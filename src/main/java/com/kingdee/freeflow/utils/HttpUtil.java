package com.kingdee.freeflow.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.lang.StringUtils;

import com.kingdee.freeflow.common.constants.FreeflowConstants;

/**
 * 说明：http get/post工具类
 * 
 * @author wu_zhu
 * 
 */
public class HttpUtil {

	/**
	 * 向指定URL发送GET方法的请求
	 * 
	 * @param url
	 *            发送请求的URL
	 * @param param
	 *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
	 * @return URL 所代表远程资源的响应结果
	 * @throws Exception
	 */
	public static String sendGet(String url, String param) throws Exception {
		String result = "";
		BufferedReader in = null;
		URL realUrl = null;
		URLConnection connection = null;
		try {
			if (StringUtils.isNotBlank(param)
					&& param.contains("authorization=")) {
				String urlNameString = url;
				realUrl = new URL(urlNameString);
				// 打开和URL之间的连接
				connection = realUrl.openConnection();
				// 设置通用的请求属性
				String authorization = param.substring(
						"authorization=".length(), param.length());
				connection.setRequestProperty("authorization", authorization);
			} else {
				String urlNameString = url + "?" + param;
				realUrl = new URL(urlNameString);
				// 打开和URL之间的连接
				connection = realUrl.openConnection();
			}
			connection.setRequestProperty("ContentType",
					"text/xml;charset=utf-8");
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");

			// 建立实际的连接
			connection.connect();
			/*
			 * // 获取所有响应头字段 Map<String, List<String>> map =
			 * connection.getHeaderFields(); // 遍历所有的响应头字段 for (String key :
			 * map.keySet()) { System.out.println(key + "--->" + map.get(key));
			 * }
			 */
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			throw e;
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
			}
		}
		return result;
	}

	/**
	 * 发送带JSON参数的http post请求
	 * 
	 * @param strUrl
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public static String sendPost(String strUrl, String params)
			throws Exception {
		StringBuffer result = new StringBuffer();

		try {
			sendHttp(result, strUrl, params);
		} catch (ConnectException e) {
			Thread.sleep(8000);
			sendHttp(result, strUrl, params);
		} catch (Exception e) {
			throw e;
		}

		return result.toString();
	}

	private static String sendHttp(StringBuffer result, String strUrl,
			String params) throws Exception {
		HttpURLConnection connection = null;
		URL url = new URL(strUrl);// 创建连接
		connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setUseCaches(false);
		connection.setInstanceFollowRedirects(true);
		connection.setRequestProperty("appkey", FreeflowConstants.GET_APP_KEY);
		connection.setRequestProperty("signature",
				FreeflowConstants.GET_SIGNATURE);

		connection.setRequestMethod("POST"); // 设置请求方式
		connection.setRequestProperty("Accept", "application/json"); // 设置接收数据的格式
		connection.setRequestProperty("Content-Type", "application/json"); // 设置发送数据的格式

		// 设置超时链接时间
		connection.setConnectTimeout(8000);

		connection.connect();

		OutputStreamWriter out = new OutputStreamWriter(
				connection.getOutputStream(), "UTF-8"); // utf-8编码
		out.append(params);
		out.flush();
		out.close();

		int HttpResult = connection.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			// 读取响应
			BufferedReader br = new BufferedReader(new InputStreamReader(
					connection.getInputStream(), "utf-8"));

			String line = null;
			while ((line = br.readLine()) != null) {
				result.append(line + "\n");
			}

			br.close();
		}else {
			result.append(HttpResult);
		}

		return result.toString();
	}
}
