package com.kingdee.freeflow.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.collections.map.ListOrderedMap;

import com.kingdee.cbos.common.utils.StringUtils;

/**
 * json格式解析工具类
 * @author kingdee
 *
 */
public class JsonUtil
{
	/**
	 * 获取json字符串key对应的内容以String[]返回
	 * @param key
	 * @param content
	 * @return
	 */
	public static  String[] getValueByKey(String key,String content){
		JSONArray jsonArray = JSONArray.fromObject(content);
		JSONObject jsonObject = null;
		String oIds[] = new String[jsonArray.size()];
		for(int i=0, l = jsonArray.size(); i < l;i++){  
	          jsonObject = (JSONObject) jsonArray.get(i);
	          oIds[i]= jsonObject.getString(key);
	    }
		return oIds;  
	}
	
	/**
	 * 保证map的存放数据的顺序
	 * @param key
	 * @param keyForVlaue
	 * @param content
	 * @return
	 */
	public static  ListOrderedMap getMapFromJsonArray(String key,String keyForVlaue,String content){
		ListOrderedMap map = null;
		try
		{
			if(!StringUtils.isEmpty(key) && !StringUtils.isEmpty(keyForVlaue) && !StringUtils.isEmpty(content)){
				JSONArray jsonArray = JSONArray.fromObject(content);
				JSONObject jsonObject = null;
				JSONArray lableArray = null,dataArray = null;
				map = new ListOrderedMap();
				for(int i=0, l = jsonArray.size(); i < l;i++){  
					jsonObject = (JSONObject) jsonArray.get(i);
					if(jsonObject.containsKey(keyForVlaue)){
						//处理日期范围类型
						//过滤图片和附件
						if(jsonObject.containsKey("type") && "filefield".equalsIgnoreCase(jsonObject.getString("type"))){
							continue;
						}
						if(jsonObject.containsKey("type") && "photofield".equalsIgnoreCase(jsonObject.getString("type"))){
							continue;
						}
						if(jsonObject.containsKey("type") && "daterangefield".equalsIgnoreCase(jsonObject.getString("type"))){
							lableArray = JSONArray.fromObject(jsonObject.getString("labels"));
							dataArray = JSONArray.fromObject(jsonObject.getString("value"));
							if(lableArray != null && dataArray != null){
								for(int a = 0, f = lableArray.size(); a < f; a++){
									if(!StringUtils.isEmpty(lableArray.get(a).toString()) && !StringUtils.isEmpty(dataArray.get(a).toString())){
										map.put(lableArray.get(a), dataArray.get(a));
									}
								}
							}
						}else{
							map.put(jsonObject.getString(key),jsonObject.getString(keyForVlaue));
						}
					}
				}
			}
		} catch (Exception e)
		{
			return map;
		}
		return  (ListOrderedMap) map;  
	}
	
	
	/**
	 * 保证map的存放数据的顺序
	 * @param key
	 * @param keyForVlaue
	 * @param content
	 * @return
	 */
	public static  ListOrderedMap getMapFromJsonArray4ExportExcel(String key,String keyForVlaue,String content){
		ListOrderedMap map = null;
		try
		{
			if(!StringUtils.isEmpty(key) && !StringUtils.isEmpty(keyForVlaue) && !StringUtils.isEmpty(content)){
				JSONArray jsonArray = JSONArray.fromObject(content);
				JSONObject jsonObject = null;
				JSONArray lableArray = null,dataArray = null;
				map = new ListOrderedMap();
				for(int i=0, l = jsonArray.size(); i < l;i++){  
					jsonObject = (JSONObject) jsonArray.get(i);
					if(jsonObject.containsKey(keyForVlaue)){
						//处理日期范围类型
						//过滤图片和附件
					/*	if(jsonObject.containsKey("type") && "filefield".equalsIgnoreCase(jsonObject.getString("type"))){
							lableArray = JSONArray.fromObject(jsonObject.getString("labels"));
							dataArray = JSONArray.fromObject(jsonObject.getString("value"));
						}*/
						if(jsonObject.containsKey("type") && "photofield".equalsIgnoreCase(jsonObject.getString("type"))){
							continue;
						}
						if(jsonObject.containsKey("type") && "daterangefield".equalsIgnoreCase(jsonObject.getString("type"))){
							lableArray = JSONArray.fromObject(jsonObject.getString("labels"));
							dataArray = JSONArray.fromObject(jsonObject.getString("value"));
							if(lableArray != null && dataArray != null){
								for(int a = 0, f = lableArray.size(); a < f; a++){
									if(!StringUtils.isEmpty(lableArray.get(a).toString()) && !StringUtils.isEmpty(dataArray.get(a).toString())){
										map.put(lableArray.get(a), dataArray.get(a));
									}
								}
							}
						}else if(jsonObject.containsKey("type") && "filefield".equalsIgnoreCase(jsonObject.getString("type"))){
							dataArray = JSONArray.fromObject(jsonObject.getString(keyForVlaue));
							if(dataArray != null && !dataArray.isEmpty()){
								String fileName = "\n";
								for (int j = 0, f = dataArray.size(); j < f; j++)
								{
									JSONObject fileObj = JSONObject.fromObject(dataArray.get(j));
									if(fileObj != null && fileObj.containsKey("fileName")){
										fileName += fileObj.getString("fileName") + "\n";
									}
								}
								
								map.put(jsonObject.getString(key),fileName);
							}
						}else{
							map.put(jsonObject.getString(key),jsonObject.getString(keyForVlaue));
						}
					}
				}
			}
		} catch (Exception e)
		{
			return map;
		}
		return  (ListOrderedMap) map;  
	}
	
	public static  List<List<String>> getArrayFromJsonArray(String key,String keyForVlaue,String content){
		List<List<String>> list=new ArrayList<List<String>>();
		try
		{
			if(!StringUtils.isEmpty(key) && !StringUtils.isEmpty(keyForVlaue) && !StringUtils.isEmpty(content)){
				JSONArray jsonArray = JSONArray.fromObject(content);
				JSONObject jsonObject = null;
				JSONArray lableArray = null,dateArray = null;
				for(int i=0, l = jsonArray.size(); i < l;i++){  
					Map<String,String> map=new HashMap<String,String>();
					JSONArray result=new JSONArray();
					jsonObject = (JSONObject) jsonArray.get(i);
					if(jsonObject.containsKey(keyForVlaue)){
						List<String> contextList=new ArrayList<String>();
						//处理日期范围类型
						//过滤图片和附件
						if(jsonObject.containsKey("type") && "filefield".equalsIgnoreCase(jsonObject.getString("type"))){
							continue;
						}
						if(jsonObject.containsKey("type") && "photofield".equalsIgnoreCase(jsonObject.getString("type"))){
							continue;
						}
						if(jsonObject.containsKey("type") && "daterangefield".equalsIgnoreCase(jsonObject.getString("type"))){
							lableArray = JSONArray.fromObject(jsonObject.getString("labels"));
							dateArray = JSONArray.fromObject(jsonObject.getString("value"));
							if(lableArray != null && dateArray != null){
								for(int a = 0, f = lableArray.size(); a < f; a++){
									if(!StringUtils.isEmpty(lableArray.get(a).toString()) && !StringUtils.isEmpty(dateArray.get(a).toString())){
										List<String> dateList=new ArrayList<String>();
										dateList.add(lableArray.getString(a));
										dateList.add(dateArray.getString(a));
										list.add(dateList);
									}
								}
							}
						}else{
							contextList.add(jsonObject.getString(key));
							contextList.add(jsonObject.getString(keyForVlaue));
							list.add(contextList);
						}
					}
					
				}
			}
		} catch (Exception e)
		{
			return list;
		}
		return  list;  
	}
	
	
	public static  HashMap<String,Object> getHashMapFromJsonArray(String key,String keyForVlaue,String content){
		Map<String,Object> map = null;
		if(!StringUtils.isEmpty(key) && !StringUtils.isEmpty(keyForVlaue) && !StringUtils.isEmpty(content)){
			JSONArray jsonArray = JSONArray.fromObject(content);
			map = new HashMap<String,Object>();
			JSONObject jsonObject = null;
			for(int i=0, l = jsonArray.size(); i < l;i++){  
				jsonObject = (JSONObject) jsonArray.get(i);
				if(jsonObject.containsKey(keyForVlaue)){
					map.put(jsonObject.getString(key),jsonObject.getString(keyForVlaue));
				}
			}
		}
		return (HashMap<String, Object>) map;  
	}
	
	
}
