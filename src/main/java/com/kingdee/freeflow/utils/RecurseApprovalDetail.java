package com.kingdee.freeflow.utils;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.kingdee.freeflow.domain.ApprovalDetail;

/**
 * 审批记录递归类
 * @author dulianbin
 *
 */
public class RecurseApprovalDetail
{
	public List<ApprovalDetail> recurse(String content){
		if(content==null) return null;
		JSONArray datas =JSONArray.fromObject(content);
		return produceApproval(datas);
	}
	
	private List<ApprovalDetail> produceApproval(JSONArray datas){
		if(datas==null||datas.size()<=0) return null;
		List<ApprovalDetail> list = new ArrayList<>();
		try {
			for (Object data : datas) {
				JSONObject item = JSONObject.fromObject(data);
				ApprovalDetail detail = new ApprovalDetail();
				detail.setuId(item.getString("id"));
				detail.setValue(item.getString("value"));
				detail.setType(item.getString("type"));
				detail.setLabel(item.getString("label"));
				if(item.containsKey("pid")){
					detail.setUpId(item.getString("pid"));
				}
				if(item.containsKey("children")){
					JSONArray children=item.getJSONArray("children");
					if(children!=null&&children.size()>0){
						produceApproval(children);
					}
					
				}
				list.add(detail);
			}
		} catch (Exception e) {
			// TODO: 处理异常   参数   和   定时调度恢复
		}
		return list;
	}
}
