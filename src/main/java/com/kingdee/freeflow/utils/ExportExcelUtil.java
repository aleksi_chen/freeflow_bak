package com.kingdee.freeflow.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.alibaba.fastjson.JSONObject;
import com.kingdee.cbos.common.utils.DateUtils;
import com.kingdee.freeflow.domain.ApprovalApply;



public class ExportExcelUtil {
	

	public static String[] header = {"单号","类型","部门","发起人","发起时间","审批人","完成时间"};

	
	
	public static void main(String[] args) throws Exception {
//		OutputStream out = new FileOutputStream("/Users/sujingyu/Desktop/a.xls");  
//		String title = "标题";
//		String[] headers = {"标题1","标题2","标题3"};
//		String[][] dataset = {{"a1","a2","a3"},{"b1","b2","b3"},{"c1","c2","c3"}};
//		exportExcel(title, headers, dataset, out);
		String x = new Date().toString();
		System.out.println(x);
		
	}
	public static String createExportFileName(String name,
			HttpServletRequest request)
	{
		String fileName = name + "_" + DateUtils.format(new Date()) + ".xls";
		String newFilename = "";
		try
		{
			newFilename = URLEncoder.encode(fileName, "UTF8");
		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		// 如果没有UA，则默认使用IE的方式进行编码
		String rtn = "filename=\"" + newFilename + "\"";
		String userAgent = request.getHeader("user-agent");
		if (userAgent != null)
		{
			userAgent = userAgent.toLowerCase();
			if (userAgent.indexOf("msie") != -1)
			{ // IE
				rtn = "filename=\"" + newFilename + "\"";
			} else if (userAgent.indexOf("opera") != -1)
			{ // Opera
				rtn = "filename*=UTF-8''" + newFilename;
			} else if (userAgent.indexOf("safari") != -1)
			{ // Safari
				try
				{
					newFilename = new String(fileName.getBytes("UTF-8"),
							"ISO8859-1");
				} catch (UnsupportedEncodingException e)
				{
					e.printStackTrace();
				}
				rtn = "filename=\"" + newFilename + "\"";
			} else if (userAgent.indexOf("applewebkit") != -1)
			{ // Chrome
				try
				{
					newFilename = new String(fileName.getBytes("gb2312"),
							"ISO8859-1");
				} catch (UnsupportedEncodingException e)
				{
					e.printStackTrace();
				}
				rtn = "filename=\"" + newFilename + "\"";
			} else if (userAgent.indexOf("mozilla") != -1)
			{ // FireFox
				rtn = "filename*=UTF-8''" + newFilename;
			}
		}
		return rtn;
	}
	
	public static void buildCustomerExportSheet(Workbook wb,
			List<ApprovalApply> details, String sheetName)
	{
		Sheet sheet = wb.createSheet(sheetName);
		// 生成表格标题
		Row row = sheet.createRow(0);
		Cell cell = null;
		int index = 0;

		cell = row.createCell(index++);
		cell.setCellValue("单号");
		sheet.setColumnWidth(index - 1, 20 * 256);	
		cell = row.createCell(index++);
		cell.setCellValue("类型");
		sheet.setColumnWidth(index - 1, 10 * 256);
		cell = row.createCell(index++);
		cell.setCellValue("部门");
		sheet.setColumnWidth(index - 1, 10 * 256);
		cell = row.createCell(index++);
		cell.setCellValue("发起人");
		sheet.setColumnWidth(index - 1, 10 * 256);

		cell = row.createCell(index++);
		cell.setCellValue("发起时间");
		sheet.setColumnWidth(index - 1, 25 * 256);

		cell = row.createCell(index++);
		cell.setCellValue("审批人");
		sheet.setColumnWidth(index - 1, 40 * 256);

		cell = row.createCell(index++);
		cell.setCellValue("完成时间");
		sheet.setColumnWidth(index - 1, 20 * 256);
		// 生成excel表格内容
		for (int i = 0; i < details.size(); i++)
		{
			row = sheet.createRow(i + 1);
			ApprovalApply model = details.get(i);

			index = 0;
			// 单号
			cell = row.createCell(index++);
			cell.setCellValue(model.getReceiptId());
			// 类型
			cell = row.createCell(index++);
			cell.setCellValue(model.getTitle());
			// 部门
			cell = row.createCell(index++);
			cell.setCellValue(model.getCreateUser().getDeptName());
			//发起人
			cell = row.createCell(index++);
			cell.setCellValue(model.getCreateUser().getName());

			// 发起时间
			cell = row.createCell(index++);
			cell.setCellValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(model.getCreateTime()));
			
			String users  = model.getApprovalUser();
			JSONArray userJsons = JSONArray.fromObject(users);  
			String approvalUsers = "";
			if(userJsons!=null){
				for(int k=0;k<userJsons.size();k++){
					JSONObject user = (JSONObject) userJsons.get(k);
					approvalUsers +=  user.getString("name")+">";
				}
				approvalUsers = approvalUsers.substring(0, approvalUsers.length()-1);
			}
			// 审批人
			cell = row.createCell(index++);
			cell.setCellValue(approvalUsers);
		
			cell = row.createCell(index++);
			// 完成时间
			if("2".equals(model.getStatus())||"3".equals(model.getStatus())){
				cell.setCellValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(model.getLastUpdateTime()));
				
			}else{
				cell.setCellValue("未完成");
			}
			
		}
	}
	
	
	public static HSSFWorkbook getExportExcel(String title, String[] headers,  
            String[][] dataset) throws Exception{
        HSSFWorkbook workbook = new HSSFWorkbook();  
        HSSFSheet sheet = workbook.createSheet(title);
        HSSFRow row = sheet.createRow(0);  
        for (int i = 0; i < headers.length; i++)  
        {  
        	if(i==0){
        		sheet.setColumnWidth(i, 20 * 256);
        	}else if(i == 3){
        		sheet.setColumnWidth(i, 40 * 256);
        	}else if(i==4){
        		sheet.setColumnWidth(i, 25 * 256);
        	}else if(i==5){
        		sheet.setColumnWidth(i, 40 * 256);
        	}else if(i==6){
        		sheet.setColumnWidth(i, 25 * 256);   
        	}
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);  
            cell.setCellValue(text);  
        }
        
        //获取cellstyle单元格样式
        CellStyle cellStyle = getCellStyle(workbook);
        
        for(int i = 0;i<dataset.length;i++){
        	HSSFRow row1 = sheet.createRow(i+1);  
            for(int j = 0;j<dataset[i].length;j++){
            	HSSFCell cell = row1.createCell(j);//.setCellValue(dataset[i][j]);
            	cell.setCellStyle(cellStyle);
            	cell.setCellValue(new HSSFRichTextString(dataset[i][j]));
            	
        	}
        }
        return workbook;
	}
	
	public static void exportExcel(String title, String[] headers,  
            String[][] dataset, OutputStream out) throws Exception{
        HSSFWorkbook workbook = new HSSFWorkbook();  
        HSSFSheet sheet = workbook.createSheet(title);
        HSSFRow row = sheet.createRow(0);  
        for (int i = 0; i < headers.length; i++)  
        {  
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);  
            cell.setCellValue(text);  
        }
        for(int i = 0;i<dataset.length;i++){
        	HSSFRow row1 = sheet.createRow(i+1);  
            for(int j = 0;j<dataset[i].length;j++){
            	row1.createCell(j).setCellValue(dataset[i][j]);
        	}
        }
		workbook.write(out);
		out.close();
	}
	
	
	 public static void exportExcel(String title, String[] headers,  
	            Collection<T> dataset, OutputStream out, String pattern){  
		// 声明一个工作薄  
	        HSSFWorkbook workbook = new HSSFWorkbook();  
	        // 生成一个表格  
	        HSSFSheet sheet = workbook.createSheet(title);  
	        // 生成一个样式  
	        HSSFCellStyle style = workbook.createCellStyle();  
	        // 设置这些样式  
	        style.setFillForegroundColor(HSSFColor.SKY_BLUE.index);  
	        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
	        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);  
	        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);  
	        style.setBorderRight(HSSFCellStyle.BORDER_THIN);  
	        style.setBorderTop(HSSFCellStyle.BORDER_THIN);  
	        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);  
	        // 生成一个字体  
	        HSSFFont font = workbook.createFont();  
	        font.setColor(HSSFColor.VIOLET.index);  
	        font.setFontHeightInPoints((short) 12);  
	        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
	        // 把字体应用到当前的样式  
	        style.setFont(font);  
	        // 生成并设置另一个样式  
	        HSSFCellStyle style2 = workbook.createCellStyle();  
	        style2.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);  
	        style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
	        style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);  
	        style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);  
	        style2.setBorderRight(HSSFCellStyle.BORDER_THIN);  
	        style2.setBorderTop(HSSFCellStyle.BORDER_THIN);  
	        style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);  
	        style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);  
	        // 生成另一个字体  
	        HSSFFont font2 = workbook.createFont();  
	        font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);  
	        // 把字体应用到当前的样式  
	        style2.setFont(font2);  
	  
	        
	        // 产生表格标题行  
	        HSSFRow row = sheet.createRow(0);  
	        for (int i = 0; i < headers.length; i++)  
	        {  
	            HSSFCell cell = row.createCell(i);
	            cell.setCellStyle(style);  
	            HSSFRichTextString text = new HSSFRichTextString(headers[i]);  
	            cell.setCellValue(text);  
	        }
	  
	        // 遍历集合数据，产生数据行  
	        Iterator<T> it = dataset.iterator();  
	        int index = 0;  
	        while (it.hasNext())  
	        {  
	            index++;  
	            row = sheet.createRow(index);  
	            T t = (T) it.next();  
	            // 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值  
	            Field[] fields = t.getClass().getDeclaredFields();  
	            for (int  i = 0; i < fields.length; i++)  
	            {  
	                HSSFCell cell = row.createCell(i);  
	                cell.setCellStyle(style2);  
	                Field field = fields[i];  
	                String fieldName = field.getName();  
	                String getMethodName = "get"  
	                        + fieldName.substring(0, 1).toUpperCase()  
	                        + fieldName.substring(1);  
	                try  
	                {  
	                    Class tCls = t.getClass();  
	                    Method getMethod = tCls.getMethod(getMethodName,  
	                            new Class[]  
	                            {});  
	                    Object value = getMethod.invoke(t, new Object[]  
	                    {});  
	                    // 判断值的类型后进行强制类型转换  
	                    String textValue = null;  
	                    // if (value instanceof Integer) {  
	                    // int intValue = (Integer) value;  
	                    // cell.setCellValue(intValue);  
	                    // } else if (value instanceof Float) {  
	                    // float fValue = (Float) value;  
	                    // textValue = new HSSFRichTextString(  
	                    // String.valueOf(fValue));  
	                    // cell.setCellValue(textValue);  
	                    // } else if (value instanceof Double) {  
	                    // double dValue = (Double) value;  
	                    // textValue = new HSSFRichTextString(  
	                    // String.valueOf(dValue));  
	                    // cell.setCellValue(textValue);  
	                    // } else if (value instanceof Long) {  
	                    // long longValue = (Long) value;  
	                    // cell.setCellValue(longValue);  
	                    // }  
	                    if (value instanceof Boolean)  
	                    {  
	                        boolean bValue = (Boolean) value;  
	                        textValue = "男";  
	                        if (!bValue)  
	                        {  
	                            textValue = "女";  
	                        }  
	                    }  
	                    else if (value instanceof Date)  
	                    {  
	                        Date date = (Date) value;  
	                        SimpleDateFormat sdf = new SimpleDateFormat(pattern);  
	                        textValue = sdf.format(date);  
	                    }  
	                    else if (value instanceof byte[])  
	                    {  
	                        // 有图片时，设置行高为60px;  
	                        row.setHeightInPoints(60);  
	                        // 设置图片所在列宽度为80px,注意这里单位的一个换算  
	                        sheet.setColumnWidth(i, (short) (35.7 * 80));  
	                        // sheet.autoSizeColumn(i);  
	                        byte[] bsValue = (byte[]) value;  
	                        HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0,  
	                                1023, 255, (short) 6, index, (short) 6, index);  
	                        anchor.setAnchorType(2);  
	                    }  
	                    else  
	                    {  
	                        // 其它数据类型都当作字符串简单处理  
	                        textValue = value.toString();  
	                    }  
	                    // 如果不是图片数据，就利用正则表达式判断textValue是否全部由数字组成  
	                    if (textValue != null)  
	                    {  
	                        Pattern p = Pattern.compile("^//d+(//.//d+)?$");  
	                        Matcher matcher = p.matcher(textValue);  
	                        if (matcher.matches())  
	                        {  
	                            // 是数字当作double处理  
	                            cell.setCellValue(Double.parseDouble(textValue));  
	                        }  
	                        else  
	                        {  
	                            HSSFRichTextString richString = new HSSFRichTextString(  
	                                    textValue);  
	                            HSSFFont font3 = workbook.createFont();  
	                            font3.setColor(HSSFColor.BLUE.index);  
	                            richString.applyFont(font3);  
	                            cell.setCellValue(richString);  
	                        }  
	                    }  
	                }  
	                catch (SecurityException e)  
	                {  
	                    e.printStackTrace();  
	                }  
	                catch (NoSuchMethodException e)  
	                {  
	                    e.printStackTrace();  
	                }  
	                catch (IllegalArgumentException e)  
	                {  
	                    e.printStackTrace();  
	                }  
	                catch (IllegalAccessException e)  
	                {  
	                    e.printStackTrace();  
	                }  
	                catch (InvocationTargetException e)  
	                {  
	                    e.printStackTrace();  
	                }  
	                finally  
	                {  
	                    // 清理资源  
	                }  
	            }  
	        }  
	        try  
	        {  
	            workbook.write(out);  
	        }  
	        catch (IOException e)  
	        {  
	            e.printStackTrace();  
	        }  
		 
		 
	    }
	 
	 
	 /**
	  * 获取工作簿单元格的样式
	  * @param workbook
	  * @return
	  */
	 public static CellStyle getCellStyle(HSSFWorkbook workbook){
		 if(workbook == null){
			return null;
		 }
		 CellStyle cellStyle = workbook.createCellStyle();
		 
		 cellStyle.setAlignment((short) 1);//0   普通   1   左对齐   2   居中   3   右对齐   4   填充   5   正当   6   居中选择   
		 
		 cellStyle.setVerticalAlignment((short)1);////单元格垂直   0   居上   1   居中   2   居下   3   正当   
		 
		 cellStyle.setWrapText(true);//单元格内换行
		 
		 return cellStyle;
	 }
}
