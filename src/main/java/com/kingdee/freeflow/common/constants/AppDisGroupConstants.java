package com.kingdee.freeflow.common.constants;


/**
 * 审批讨论组
 * @author kingdee
 *
 */
public class AppDisGroupConstants
{

	public static final String GROUP_NAME = "审批组";
	public static final String SYS_MSG_TITLE = "审批";
	public static final String LIGHT_APP_NAME = "审批";
	public static final String SYS_MSG_CONTENT_1 = "#{fromUserName}提交了#{formTemplateType}审批";  //创建审批推送消息
	//public static final String SYS_MSG_CONTENT_2 = "#{fromUserName}同意了#{user1Name}的#{formTemplateType},并转发给了#{user2Name}"; //同意并转审批
	public static final String SYS_MSG_CONTENT_2 = "#{fromUserName}同意了#{user1Name}的#{formTemplateType}审批"; //同意并转审批
	public static final String SYS_MSG_CONTENT_3 = "#{fromUserName}不同意并完成了#{user1Name}的#{formTemplateType}审批"; //不同意
	public static final String SYS_MSG_CONTENT_4 = "#{fromUserName}同意并完成了#{user1Name}的#{formTemplateType}"; //同意并完成
	public static final String SYS_MSG_CONTENT_5 = "#{fromUserName}邀请#{user1Name}参与审批#{formTemplateType}"; //新拉审批人
	
	public static final String SYS_MSG_PUBLISH_COMMENT = "#{fromUserName}对#{user1Name}的#{formTemplateType}发表了意见"; //发表意见
	
	public static final String SYS_MSG_COLOR = "#3CBAFF";//高亮显示的定义，蓝色
	
	public static final String SYS_MSG_PROCESS = "流程";
	     
	public static final String FORM_TEMPLATE_TYPE = "#{formTemplateType}";
	
	public static final String APPROVAL_GROUP_CONTENT_TITLE = "{0}的{1}({2})";//title：邓敏的合同申请流程(2016-05-22)
	
	public static final int APPROVAL_GROUP_FIELD_COUNT = 4;//显示申请单内容字段数
	
	public static final String APPROVAL_GROUP_MSG_ICON = "/freeflow/mobile/dist/static/public-group.png";//消息图标icon url
	
	public static final int APPROVAL_GROUP_FIELD_LENGTH = 10;//显示的内容长度
	
}

