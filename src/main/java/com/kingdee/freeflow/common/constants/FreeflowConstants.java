package com.kingdee.freeflow.common.constants;

public class FreeflowConstants {

	// getContext调用方法名
	public static final String GET_CONTEXT = "getcontext";
	// token
	public static final String GET_TOKEN = "token";

	public static final String APP_AUTH2 = "appAuth2";
	// pubsend
	public static final String PUSH_MSG = "pubsend";

	// getEidsByNetworkIds
	public static final String GET_EID_BY_NETWORK_IDS = "getEidsByNetworkIds";

	// shorturl/create
	public static final String SHORTURL_CREATE = "create";
	// sms/send
	public static final String SMS_SEND = "sms/send";

	// freeflow/c/form/detail.json?formId=xxx&applicationId=xxx
	public static final String FORM_DETAIL = "/c/form/detail.json";

	// lightapp redirect
	public static final String LIGHTAPP_REDIRECT = "c/redirect.json";
	// lightapp get user info
	public static final String LIGHTAPP_GET_USERINOF = "rest/application/getUserInfoByLappKey.json";

	// getHeaderByNetworkAndUserId
	public static final String GET_HEADER = "getHeaderByNetworkAndUserId";

	// getPersonsCasvirByIds
	public static final String GET_PERSONS_CASVIR_BY_IDS = "getPersonsCasvirByIds";

	// user/authbytoken
	public static final String USER_AUTH_BY_TOKEN = "user/authbytoken";

	// appkey
	public static final String GET_APP_KEY = "eHVudG9uZw";
	// signature
	public static final String GET_SIGNATURE = "Ld3dK-9E7r7HKQMZ9j7m1QOp5zCYqjWKH4xupXTaFMDl2UlJzdeQVYsWhb37scAVK-NCC6wW1A9aOYYNjzoQt-yGvup5xmOBR1SsSp690FN8aX4gUwCpxiarbesQ7Z7m9UL1fi7QUWSPBvFuD4twJNi75dOAZW287UWQHijsSqo";

	public static final String KINGDEE_NETWORKID = "383cee68-cea3-4818-87ae-24fb46e081b1";

	public static final String CODE_0 = "0";
	public static final String CODE_ALL = "all";

	public static final int CONFIRM_MSG = 0; // 确认消息类型

	public static final int QRY_GROUPMEMBER_MSG = 1; // 查询群组成员信息

	public static final int ERRORCODE_ADD_FAILER = 1; // 创建纪录失败

	public static final int MessageModelEnu_SINGLETEXT = 1; // 单条文本编排模板

	public static final int MessageModelEnu_SINGLEGRAPHIC = 2; // 单条图文混排模板

	public static final int MessageModelEnu_MULTIGRAPHIC = 3; // 多条图文混排模板

	public static final int MessageTypeEnum_TEXT = 2; // 纯文本信息

	public static final int MessageTypeEnum_LINK = 5; // 文本链接信息

	public static final int MessageTypeEnum_GRAPHIC = 6; // 图文混排信息

	// 使用场景
	public static final int USE_SCENE_CREATE = 1;// 1,创建

	public static final int USE_SCENE_TRANSFER_APPROVAL = 2; // 2, 转审批

	public static final int USE_SCENE_AGREE = 3; // 3,同意

	public static final int USE_SCENE_REJECT = 4; // 4,拒绝

	// 审批状态
	public static final int APPROVER_STATUS_INIT = 0;// 初始状态

	public static final int APPROVER_STATUS_APPROVAL = 1;// 审批中

	public static final int APPROVER_STATUS_AGREE = 2;// 同意

	public static final int APPROVER_STATUS_REJECT = 3;// 不同意

	// 表单状态(同意、驳回(重新提交表单的状态标识)、完成、审批中、已关闭(重新提交表单成功后的状态标识))
	public static final int APPROVER_FORM_STATUS_CLOSE = 0;// 关闭
	public static final int APPROVER_FORM_STATUS_APPROVAL = 1;// 审批中
	public static final int APPROVER_FORM_STATUS_FINISH = 2;// 完成
	public static final int APPROVER_FORM_STATUS_REJECT = 3;// 驳回

	// 模板状态
	public static final int FORMTEMPLATE_STATUS_OPEN = 0;// 开启
	public static final int FORMTEMPLATE_STATUS_DISABLED = 1;// 停用

	// 推送
	public static final String TITLE = "审批";

	public static final String CONTENT_C = "{2}提交了【{4}】申请，请你审批"; // 1,创建

	public static final String CONTENT_T = "{1}同意了你的【{4}】申请，等待{3}审批"; // 2, 转审批

	public static final String CONTENT_A = "{1}已经同意了你的【{4}】申请"; // 3,同意

	public static final String CONTENT_R = "{1}已经驳回了你的【{4}】申请"; // 4,拒绝
	
	public static final String LOCK_KEY="approvalapply"; //
}
