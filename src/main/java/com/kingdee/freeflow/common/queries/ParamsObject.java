package com.kingdee.freeflow.common.queries;


import com.kingdee.freeflow.enums.AppovalRecordStatus;
import com.kingdee.sns.lightapp.domain.UserIdentity;


public class ParamsObject
{

	private UserIdentity user;
	private String formTemplateId;
	private String status;
	private String approvId;
	private String receiptId; //单据编号 
	private Long startTime;
	private Long endTime;
	private String queryType;
	private String transferApproverOId;
	private String startTimeStr;  
	private String endTimeStr; 
	
	private int deptLevel;//部门负责人级别
	
	public String getStartTimeStr()
	{
		return startTimeStr;
	}
	public void setStartTimeStr(String startTimeStr)
	{
		this.startTimeStr = startTimeStr;
	}
	public String getEndTimeStr()
	{
		return endTimeStr;
	}
	public void setEndTimeStr(String endTimeStr)
	{
		this.endTimeStr = endTimeStr;
	}
	public UserIdentity getUser()
	{
		return user;
	}
	public void setUser(UserIdentity user)
	{
		this.user = user;
	}

	public String getApprovId()
	{
		return approvId;
	}
	public void setApprovId(String approvId)
	{
		this.approvId = approvId;
	}
	public Long getStartTime()
	{
		return startTime;
	}
	public void setStartTime(Long startTime)
	{
		this.startTime = startTime;
	}
	public Long getEndTime()
	{
		return endTime;
	}
	public void setEndTime(Long endTime)
	{
		this.endTime = endTime;
	}
	public String getStatus() {
		AppovalRecordStatus type = AppovalRecordStatus.getAppovalRecordStatus(status);
		if(type==null)
			this.status = "";
		else
			this.status = type.getContext();
		return status;
	}

	public void setStatus(String status) {
		AppovalRecordStatus type = AppovalRecordStatus.getAppovalRecordStatus(status);
		if(type==null)
			this.status = "";
		else
			this.status = type.getContext();
		this.status = status;
	}
	
	public void setStatus(AppovalRecordStatus status) {
		this.status = status.getContext();
	}
	
	public String getFormTemplateId()
	{
		return formTemplateId;
	}
	public void setFormTemplateId(String formTemplateId)
	{
		this.formTemplateId = formTemplateId;
	}
	public String getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}
	public String getTransferApproverOId() {
		return transferApproverOId;
	}
	public void setTransferApproverOId(String transferApproverOId) {
		this.transferApproverOId = transferApproverOId;
	}
	public String getQueryType() {
		return queryType;
	}
	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	public int getDeptLevel()
	{
		return deptLevel;
	}
	public void setDeptLevel(int deptLevel)
	{
		this.deptLevel = deptLevel;
	}
	
	
}
