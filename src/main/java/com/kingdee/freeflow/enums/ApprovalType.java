package com.kingdee.freeflow.enums;

import com.kingdee.cbos.common.utils.StringUtils;

public enum ApprovalType {
	/**
	 * 自由流
	 */
	FREE("0"),
	/**
	 * 指定条件关键审批人
	 */
	KEY("1"),
	/**
	 * 无条件关键审批人（原固定流）
	 */
	NORMAL("2");
	
	private String context;

    public String getContext() {
        return this.context;
    }

    private ApprovalType(String context) {
        this.context = context;
    }
    
    public static ApprovalType getApprovalType(String content){
    	if(StringUtils.isEmpty(content)){
    		return null;
    	}
    	for(ApprovalType approvalType:ApprovalType.values()){
    		if(approvalType.getContext().equals(content)){
    			return approvalType;
    		}
    	}
    	return null;
    }
}
