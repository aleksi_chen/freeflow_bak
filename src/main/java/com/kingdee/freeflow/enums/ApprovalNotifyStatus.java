package com.kingdee.freeflow.enums;

import com.kingdee.cbos.common.utils.StringUtils;

public enum ApprovalNotifyStatus {
	/**
	 * 通知失败
	 */
	ERROR("-1"),
	/**
	 * 通知中
	 */
	DOING("0"),
	/**
	 * 通知中成功
	 */
	FINISH("1");
	
	private String context;

    public String getContext() {
        return this.context;
    }

    private ApprovalNotifyStatus(String context) {
        this.context = context;
    }
    
    public static ApprovalNotifyStatus getApprovalNotifyStatus(String content){
    	if(StringUtils.isEmpty(content)){
    		return null;
    	}
    	for(ApprovalNotifyStatus approvalNotifyStatus:ApprovalNotifyStatus.values()){
    		if(approvalNotifyStatus.getContext().equals(content)){
    			return approvalNotifyStatus;
    		}
    	}
    	return null;
    }
}
