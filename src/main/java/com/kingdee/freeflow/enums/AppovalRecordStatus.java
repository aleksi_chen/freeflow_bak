package com.kingdee.freeflow.enums;

import com.kingdee.cbos.common.utils.StringUtils;

public enum AppovalRecordStatus {
	/**
	 * 未轮到
	 */
	FUTURE("-2"),
	/**
	 * 待处理
	 */
	WAIT("-1"),
	/**
	 * 关闭
	 */
	CLOSE("0"),
	/**
	 * 审批创建
	 */
	CREATE("1"),
	/**
	 * 审批同意
	 */
	AGREE("2"),
	/**
	 * 审批驳回
	 */
	DISAGREE("3"),
	/**
	 * 审批同意并终止
	 */
	AGREE_END("4"),
	/**
	 * 审批挂起
	 */
	HANG("5");
	
	private String context;

    public String getContext() {
        return this.context;
    }

    private AppovalRecordStatus(String context) {
        this.context = context;
    }
    
    public static AppovalRecordStatus getAppovalRecordStatus(String content){
    	if(StringUtils.isEmpty(content)){
    		return null;
    	}
    	for(AppovalRecordStatus appoverRecordStatus:AppovalRecordStatus.values()){
    		if(appoverRecordStatus.getContext().equals(content)){
    			return appoverRecordStatus;
    		}
    	}
    	return null;
    }
}
