package com.kingdee.freeflow.enums;

import com.kingdee.cbos.common.utils.StringUtils;

public enum AppovalStatus {
	/**
	 * 关闭
	 */
	CLOSE("0"),
	/**
	 * 审批中
	 */
	DOING("1"),
	/**
	 * 审批完成
	 */
	AGREE("2"),
	/**
	 * 审批驳回
	 */
	DISAGREE("3"),
	/**
	 * 审批挂起
	 */
	HANG("4");
	
	private String context;

    public String getContext() {
        return this.context;
    }

    private AppovalStatus(String context) {
        this.context = context;
    }
    
    public static AppovalStatus getAppovalStatus(String content){
    	if(StringUtils.isEmpty(content)){
    		return null;
    	}
    	for(AppovalStatus appoverStatus:AppovalStatus.values()){
    		if(appoverStatus.getContext().equals(content)){
    			return appoverStatus;
    		}
    	}
    	return null;
    }
}
