package com.kingdee.freeflow.enums;

import com.kingdee.cbos.common.utils.StringUtils;

public enum ApprovalNotifyType {
	/**
	 * 警告通知
	 */
	WARNING("0"),
	/**
	 * 普通状态通知
	 */
	NORMAL("1");
	
	private String context;

    public String getContext() {
        return this.context;
    }

    private ApprovalNotifyType(String context) {
        this.context = context;
    }
    
    public static ApprovalNotifyType getApprovalNotifyType(String content){
    	if(StringUtils.isEmpty(content)){
    		return null;
    	}
    	for(ApprovalNotifyType approvalNotifyType:ApprovalNotifyType.values()){
    		if(approvalNotifyType.getContext().equals(content)){
    			return approvalNotifyType;
    		}
    	}
    	return null;
    }
}
