package com.kingdee.freeflow.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kingdee.freeflow.dao.ApprovalRuleDAO;
import com.kingdee.freeflow.domain.ApprovalCndGroup;
import com.kingdee.freeflow.domain.ApprovalRule;
import com.kingdee.freeflow.domain.UserSimpleInfo;
import com.kingdee.freeflow.enums.ApprovalType;
import com.kingdee.freeflow.service.ApprovalCndSevice;
import com.kingdee.freeflow.service.ApprovalRuleService;
import com.kingdee.freeflow.service.ApprovalUserSevice;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.common.constants.LightappURLConstants;
import com.kingdee.sns.lightapp.dao.FormTemplateDAO;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.domain.UserNetwork;
import com.kingdee.sns.lightapp.domain.openorg.OpenPersonInfo;
import com.kingdee.sns.lightapp.domain.template.FormTemplate;
import com.kingdee.sns.lightapp.service.Tidings;
import com.kingdee.sns.lightapp.service.UserNetworkService;
import com.kingdee.sns.lightapp.service.configure.LightappConfigureService;
import com.kingdee.sns.lightapp.service.openorg.OpenorgService;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

@Service
public class ApprovalRuleServiceImpl implements ApprovalRuleService {
	private final static Logger LOGGER = LoggerFactory.getLogger(ApprovalRuleServiceImpl.class);

	@Autowired
	private ApprovalUserSevice approvalUserSevice;
	
	@Autowired
	private ApprovalCndSevice approvalCndService;
	
	@Autowired
	private FormTemplateDAO formTemplateDAO;
	
	@Autowired
	private ApprovalRuleDAO approvalRuleDAO;
	
	@Autowired
	private LightappConfigureService lightappConfigureService;
	
	@Autowired
	private UserNetworkService userNetworkService;
	
	@Autowired
	private OpenorgService openorgService;

	@Override
	public ApprovalRule getRuleByFid(String templateId) {
		if(StringUtils.isEmpty(templateId)) return null;
		try {
			return approvalRuleDAO.getRuleByFid(templateId);
		} catch (Exception e) {
			LOGGER.error("===审批规则查询失败===getApprovalRuleByFid",e);
			return null;
		}
	}

	@Override
	public Tidings saveRule(ApprovalRule approvalRule,List<ApprovalCndGroup> groups,UserIdentity user) {
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_NOTACCEPT);
		if(approvalRule==null||user==null) return msg;
		if(StringUtils.isEmpty(approvalRule.getTemplateId())) return msg;
		if(StringUtils.isEmpty(approvalRule.getId())){
			try {
				ApprovalRule rule = approvalRuleDAO.getRuleByFid(approvalRule.getTemplateId());
				if(rule!=null&&!StringUtils.isEmpty(rule.getId())){
					approvalRule.setId(rule.getId());
					msg = updateRule(approvalRule, groups, user);
					return msg;
				}
				FormTemplate formTemplate = formTemplateDAO.findOneById(approvalRule.getTemplateId());
				if(formTemplate==null)  return msg.setFlag(false).setMsg("未找到对应绑定的模板！").setCode(LightappConstants.META_CODE_NOTACCEPT);
				//设置默认自由流
				if(ApprovalType.getApprovalType(approvalRule.getApprovalType())==null) 
					approvalRule.setApprovalType(ApprovalType.FREE.getContext());
				//设置并校验关键审批人 
				boolean isAccess = buildRuleContext(approvalRule, formTemplate);
				if(!isAccess)
					return msg.setFlag(false).setMsg("非法的审批人设置！").setCode(LightappConstants.META_CODE_NOTACCEPT);
				//设置关键审批人 数据校验
				if(ApprovalType.getApprovalType(approvalRule.getApprovalType())==ApprovalType.NORMAL&&StringUtils.isEmpty(approvalRule.getContent()))
					return msg.setFlag(false).setMsg("非法的审批人设置！").setCode(LightappConstants.META_CODE_NOTACCEPT);
				//无条件关键审批人  数据校验
				if(ApprovalType.getApprovalType(approvalRule.getApprovalType())==ApprovalType.KEY&&(groups==null||groups.isEmpty()))
					return msg.setFlag(false).setMsg("非法的审批规则设置,按条件指定审批人必须指定规则！").setCode(LightappConstants.META_CODE_NOTACCEPT);
				//保存审批规则
				approvalRule.setCreateTime(new Date());
				approvalRuleDAO.save(approvalRule);
				//保存审批规则条件
				msg=approvalCndService.saveApprovalCndGroups(groups, approvalRule, user);
				if(!msg.isFlag()) {
					if(!StringUtils.isEmpty(approvalRule.getId())) approvalRuleDAO.delete(approvalRule.getId());
					return msg;
				}
				msg.setFlag(true).setMsg("操作成功！").setCode(LightappConstants.META_CODE_SAVE).put("approvalRule",approvalRule).put("approvalCndGroups", groups);
			} catch (Exception e) {
				if(!StringUtils.isEmpty(approvalRule.getId())) approvalRuleDAO.delete(approvalRule.getId());
				msg.setFlag(false).setMsg("数据库操作失败！").setCode(LightappConstants.META_CODE_ERROR);
				LOGGER.error("===审批规则保存失败===saveApprovalRule",e);
			}
		} else{
			msg = updateRule(approvalRule, groups, user);
		}
		return msg;
	}

	@Override
	public Tidings updateRule(ApprovalRule approvalRule,List<ApprovalCndGroup> groups,UserIdentity user) {
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		if(approvalRule==null||user==null||StringUtils.isEmpty(approvalRule.getId())) return msg;
		try {
			if(StringUtils.isEmpty(approvalRule.getTemplateId())) return msg;
			ApprovalRule rule = approvalRuleDAO.find(approvalRule.getId());
			if(rule==null) return msg;
			if(StringUtils.isEmpty(approvalRule.getTemplateId())) return msg;
			FormTemplate formTemplate = formTemplateDAO.findOneById(approvalRule.getTemplateId());
			if(formTemplate==null)  return msg.setFlag(false).setMsg("未找到对应绑定的模板！").setCode(LightappConstants.META_CODE_NOTACCEPT);
			//设置默认自由流
			if(ApprovalType.getApprovalType(approvalRule.getApprovalType())==null) 
				approvalRule.setApprovalType(ApprovalType.FREE.getContext());
			//设置无条件关键审批人
			boolean isAccess = buildRuleContext(approvalRule, formTemplate);
			if(!isAccess)
				return msg.setFlag(false).setMsg("非法的审批人设置！").setCode(LightappConstants.META_CODE_NOTACCEPT);
			//设置关键审批人 数据校验
			if(ApprovalType.getApprovalType(approvalRule.getApprovalType())==ApprovalType.NORMAL&&StringUtils.isEmpty(approvalRule.getContent()))
				return msg.setFlag(false).setMsg("非法的审批人设置！").setCode(LightappConstants.META_CODE_NOTACCEPT);
			//无条件关键审批人  数据校验
			if(ApprovalType.getApprovalType(approvalRule.getApprovalType())==ApprovalType.KEY&&(groups==null||groups.isEmpty()))
				return msg.setFlag(false).setMsg("非法的审批规则设置,按条件指定审批人必须指定规则！").setCode(LightappConstants.META_CODE_NOTACCEPT);
			//保存审批规则条件
			msg=approvalCndService.saveApprovalCndGroups(groups, approvalRule, user);
			if(!msg.isFlag()) return msg;
			//保存审批规则
			approvalRuleDAO.save(approvalRule);
			msg.setFlag(true).setMsg("操作成功！").setCode(LightappConstants.META_CODE_SAVE).put("approvalRule",approvalRule).put("approvalCndGroups", groups);
		} catch (Exception e) {
			msg.setFlag(false).setMsg("数据库操作失败！").setCode(LightappConstants.META_CODE_ERROR);
			LOGGER.error("===审批规则更新失败===modifyApprovalRule",e);
		}
		return msg;
	}

	@Override
	public Tidings removeRuleByFid(String templateId) {
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		try {
			approvalRuleDAO.deleteRuleByFid(templateId);
			msg.setFlag(true).setMsg("操作成功！").setCode(LightappConstants.META_CODE_REMOVE);
		} catch (Exception e) {
			msg.setFlag(false).setMsg("数据库操作失败！").setCode(LightappConstants.META_CODE_ERROR);
			LOGGER.error("===审批规则更新失败===delApprovalRuleByFid",e);
		}
		return msg;
	}

	private boolean buildRuleContext(ApprovalRule approvalRule,FormTemplate formTemplate) {
		String ruleContext = approvalRule.getContent();
		if(ApprovalType.getApprovalType(approvalRule.getApprovalType())==ApprovalType.NORMAL&&StringUtils.isEmpty(ruleContext)) return false;
		if(StringUtils.isEmpty(ruleContext)){
			approvalRule.setContent("");
			return true;
		}
		JSONArray userArray = JSONArray.fromObject(ruleContext);
		List<String> oIds = new ArrayList<String>();
		List<UserSimpleInfo> suser = new ArrayList<UserSimpleInfo>();
		for (Object o : userArray) {
			if(o==null) continue;
			JSONObject appover = JSONObject.fromObject(o);
			if(appover==null||appover.isEmpty()) return false;
			String oid = appover.getString("oId");
			if(StringUtils.isEmpty(oid)) oid = appover.getString("oid");
			if(StringUtils.isEmpty(oid)) return false;
			oIds.add(oid);
		}
		if(oIds!=null&&!oIds.isEmpty()){
			List<UserNetwork> uns = userNetworkService.getUserNetworkByIds(formTemplate.getUser().getNetworkId(),oIds,false);
			boolean isAdd = false;
			boolean sameUser = false;
			for (String oid : oIds) {
				isAdd =false;
				for (UserNetwork un : uns) {
					sameUser = oid.equals(un.getOid())||oid.equals(un.getPersonId())||oid.equals(un.getUserId())||oid.equals(un.getOpenId());
					if(sameUser&&un.getStatus().equalsIgnoreCase("ACTIVE")){
						un.setPhoto(lightappConfigureService.getKdweiboOutHost()+ LightappURLConstants.SPACE_PHOTO + "?userId=" + un.getUserId());
						suser.add(new UserSimpleInfo(un));
						isAdd=true;
						break;
					}
				}
				if(!isAdd) return false;            
			}
		}
		if(suser!=null&&!suser.isEmpty()) {
			approvalRule.setContent(JSONArray.fromObject(suser).toString());
		} else {
			if(ApprovalType.getApprovalType(approvalRule.getApprovalType())==ApprovalType.NORMAL) return false;
			approvalRule.setContent("");
		}
		return true;
	}

	
	
	/**
	 * 需要对approvalRule做处理，审批规则第一个是部门角色审批人并且审批人的部门负责人只有一个则需要做初始化显示
	 */
	@Override
	public void resetApprovalRuleForDeptRoleType(ApprovalRule approvalRule,UserIdentity user)
	{	
		if(approvalRule != null && StringUtils.isNotBlank(approvalRule.getContent())){
			JSONArray jsonArray = JSONArray.fromObject(approvalRule.getContent());
			if(jsonArray != null && !jsonArray.isEmpty()){
				JSONObject obj = (JSONObject) jsonArray.get(0);//获取第一个关键审批人
				if(obj != null && obj.containsKey("deptRoleType") && obj.containsKey("deptLevel")){
					boolean isDeptRoleTypeFlag = obj.getBoolean("deptRoleType");
					int deptLevel = obj.getInt("deptLevel");
					if(isDeptRoleTypeFlag && deptLevel == 1){//第一级部门审批人
						//需要查找该人员的信息并且填充,oid/personId/name/photo
						//根据审批人查找上级部门负责人
						/*String jsonStr = openorgService.getParentpersonByEidAndOpenId(user.getEid(), user.getOpenId());
						Set<Object> personSet = this.parseParentPersonFromJsonStr(jsonStr);//获取审批申请人的上级部门负责人
*/						
						Set<Object> personSet = openorgService.getParentPersonSet(user.getEid(), user.getOid());
						
						//如果上级部门负责人人数（默认上级，兼职上级，指定上级）有且只有一个则需要处理
						if(personSet != null && personSet.size() == 1){
							Iterator<Object> itor = personSet.iterator();
							OpenPersonInfo personInfo = (OpenPersonInfo) itor.next();
							obj.put("name", personInfo.getName());//名称
							obj.put("photo", personInfo.getPhotoUrl());//头像图片url
							jsonArray.remove(0);//清除旧的json object对象
							jsonArray.add(0, obj);//添加新的json object
						}
					}
					
				}
					
			}
		}
	}
	

	
}
