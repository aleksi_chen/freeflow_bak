package com.kingdee.freeflow.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kingdee.cbos.common.utils.DateUtils;
import com.kingdee.freeflow.dao.ApprovalApplyDAO;
import com.kingdee.freeflow.dao.ApprovalApplyRecordDAO;
import com.kingdee.freeflow.dao.ApprovalNodeUserDAO;
import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.domain.ApprovalApplyRecord;
import com.kingdee.freeflow.domain.ApprovalNodeUser;
import com.kingdee.freeflow.domain.UserSimpleInfo;
import com.kingdee.freeflow.enums.AppovalRecordStatus;
import com.kingdee.freeflow.enums.AppovalStatus;
import com.kingdee.freeflow.enums.ApprovalType;
import com.kingdee.freeflow.service.ApprovalUserSevice;
import com.kingdee.freeflow.utils.HttpUtil;
import com.kingdee.sns.lightapp.common.constants.LightappURLConstants;
import com.kingdee.sns.lightapp.domain.User;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.domain.UserNetwork;
import com.kingdee.sns.lightapp.service.UserNetworkService;
import com.kingdee.sns.lightapp.service.UserService;
import com.kingdee.sns.lightapp.service.configure.LightappConfigureService;

@Service
public class ApprovalUserSeviceImpl implements ApprovalUserSevice{
	
	public class ApprovalUserResult<E>{
		private boolean flag;
		private String msg;
		private E data;
		private List<E> datas;
		public boolean getFlag() {
			return flag;
		}
		public void setFlag(boolean flag) {
			this.flag = flag;
		}
		public String getMsg() {
			return msg;
		}
		public void setMsg(String msg) {
			this.msg = msg;
		}
		public E getData() {
			return data;
		}
		public void setData(E data) {
			this.data = data;
		}
		public List<E> getDatas() {
			return datas;
		}
		public void setDatas(List<E> datas) {
			this.datas = datas;
		}
	}
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ApprovalUserSeviceImpl.class);
	@Autowired
	private LightappConfigureService lightappConfigureService;
	@Autowired
	private UserNetworkService userNetworkService;
	@Autowired
	private ApprovalApplyDAO approvalApplyDAO;
	@Autowired
	private ApprovalApplyRecordDAO approvalApplyRecordDAO;
	@Autowired
	private ApprovalNodeUserDAO approvalNodeUserDAO;
	@Autowired
	private UserService userService;
	
	@Override
	public UserIdentity getUserInfo(String networkId, String id) {
		if(StringUtils.isEmpty(id)) return null;
		UserNetwork userNetwork=userNetworkService.getUserNetworkByUnKnowId(id, null, networkId);
		if(userNetwork==null) return null;
		userNetwork.setPhoto(lightappConfigureService.getKdweiboOutHost()+ LightappURLConstants.SPACE_PHOTO + "?userId=" + userNetwork.getUserId());
		if(userNetwork.getStatus().equalsIgnoreCase("ACTIVE"))
			return new UserIdentity(userNetwork);
		else
			return null;
	}

	@Override
	public ApprovalUserResult<ApprovalNodeUser> getNodeUsersInfo(String networkId, List<String> oIds){
		ApprovalUserResult<ApprovalNodeUser> result = new ApprovalUserResult<ApprovalNodeUser>();
		if(StringUtils.isEmpty(networkId)) {
			result.setFlag(false);
			result.setMsg("非法的参数,解析失败！！！");
			return result;
		}
		if(oIds==null||oIds.isEmpty()) {
			result.setFlag(true);
			result.setMsg("success");
			result.setDatas(null);
			return result;
		}
		List<ApprovalNodeUser> users = new ArrayList<ApprovalNodeUser>();
		List<UserNetwork> uns = userNetworkService.getUserNetworkByIds(networkId,oIds,false);
		if(uns==null||uns.isEmpty()){
			result.setFlag(false);
			result.setMsg("非法的审批人设置,解析失败！！！");
			return result;
		}
		boolean isAdd = false;
		boolean sameUser = false;
		for (String oid : oIds) {
			isAdd =false;
			for (UserNetwork un : uns) {
				sameUser = oid.equals(un.getOid())||oid.equals(un.getPersonId())||oid.equals(un.getUserId())||oid.equals(un.getOpenId());
				if(sameUser&&un.getStatus().equalsIgnoreCase("ACTIVE")){
					un.setPhoto(lightappConfigureService.getKdweiboOutHost()+ LightappURLConstants.SPACE_PHOTO + "?userId=" + un.getUserId());
					users.add(new ApprovalNodeUser(un));
					isAdd=true;
					break;
				} else if(sameUser&&!un.getStatus().equalsIgnoreCase("ACTIVE")) {
					result.setFlag(false);
					result.setMsg("关键审批人中"+un.getName()+"处于非在职状态！！！");
					isAdd=false;
					return result;
				} 
			}
			if(!isAdd) {
				result.setFlag(false);
				result.setMsg("非法的审批人设置,解析失败！！！");
				return result;
			}
		}
		result.setFlag(true);
		result.setMsg("success");
		result.setDatas(users);
		return result;
	}
	
	@Override
	public ApprovalUserResult<UserSimpleInfo> getFlowUserByRule(String ruleContext,String networkId){
		ApprovalUserResult<UserSimpleInfo> result = new ApprovalUserResult<UserSimpleInfo>();
		if(StringUtils.isEmpty(networkId)) {
			result.setFlag(false);
			result.setMsg("非法的参数,解析失败！！！");
			return result;
		}
		if(StringUtils.isEmpty(ruleContext)) {
			result.setFlag(true);
			result.setMsg("success");
			result.setDatas(null);
			return result;
		}
		try {
			List<UserSimpleInfo> suser = new ArrayList<UserSimpleInfo>();
			JSONArray userArray = JSONArray.fromObject(ruleContext);   
			List<String> oIds = new ArrayList<String>();
			for (Object o : userArray) {
				if(o==null) continue;
				JSONObject appover = JSONObject.fromObject(o);
				if(appover==null||appover.isEmpty()) continue;
				String oid = appover.getString("oId");
				if(StringUtils.isEmpty(oid)) oid = appover.getString("oid");
				if(StringUtils.isEmpty(oid)) {
					result.setFlag(false);
					result.setMsg("非法的审批规则,解析失败！！！");
					return result;
				}
				oIds.add(oid);
			}
			if(oIds!=null&&!oIds.isEmpty()){
				boolean isAdd = false;
				boolean sameUser = false;
				List<UserNetwork> uns = userNetworkService.getUserNetworkByIds(networkId,oIds,false);
				if(uns==null||uns.isEmpty()){
					result.setFlag(false);
					result.setMsg("非法的审批人设置,解析失败！！！");
					return result;
				}
				for (String oid : oIds) {
					isAdd =false;
					for (UserNetwork un : uns) {
						sameUser = oid.equals(un.getOid())||oid.equals(un.getPersonId())||oid.equals(un.getUserId())||oid.equals(un.getOpenId());
						if(sameUser&&un.getStatus().equalsIgnoreCase("ACTIVE")){
							un.setPhoto(lightappConfigureService.getKdweiboOutHost()+ LightappURLConstants.SPACE_PHOTO + "?userId=" + un.getUserId());
							suser.add(new UserSimpleInfo(un));
							isAdd=true;
							break;
						} else if(sameUser&&!un.getStatus().equalsIgnoreCase("ACTIVE")) {
							result.setFlag(false);
							result.setMsg("关键审批人中"+un.getName()+"处于非在职状态！！！");
							isAdd=false;
							return result;
						} 
					}
					if(!isAdd) {
						result.setFlag(false);
						result.setMsg("非法的审批人设置,解析失败！！！");
						return result;
					}
				}
				result.setFlag(true);
				result.setMsg("success");
				result.setDatas(suser);
				return result;
			} else{
				result.setFlag(true);
				result.setMsg("success");
				return result;
			}
		} catch (Exception e) {
			LOGGER.error("===审批规则关键人解析失败===getFlowUserByRule 非法的json",e);
			result.setFlag(false);
			result.setMsg("非法的审批规则,解析失败！！！");
			return result;
		}
	}
	
	@Override
	public boolean checkCanDoRecord(ApprovalApply approvalApply,ApprovalApplyRecord nextRecord, UserIdentity user) {
		if(approvalApply==null||nextRecord==null||user==null) return false;
		if(AppovalStatus.DOING!=AppovalStatus.getAppovalStatus(approvalApply.getStatus())) return false;
		if(!approvalApply.getNextRecordId().equals(nextRecord.getId()))  return false;
		if(nextRecord.getApprovalUser()!=null&&!user.getPersonId().equals(nextRecord.getApprovalUser().getPersonId()))  return false;
		if(approvalApply.getNextApprover()!=null&&!user.getPersonId().equals(approvalApply.getNextApprover().getPersonId()))  return false;
		
		AppovalRecordStatus nowStatus = AppovalRecordStatus.getAppovalRecordStatus(nextRecord.getStatus());
		boolean create= nowStatus==AppovalRecordStatus.CREATE;
		boolean agreeEnd= nowStatus==AppovalRecordStatus.AGREE_END;
		if(create) return false;
		if(agreeEnd) return checkCanAgreeEnd(approvalApply, nextRecord);
		return true;
	}
	
	@Override
	public boolean checkCanAgreeEnd(ApprovalApply approvalApply,ApprovalApplyRecord nextRecord){
		ApprovalType type = ApprovalType.getApprovalType(approvalApply.getApprovalType());
		String ruleText = approvalApply.getApprovalRule();
		if("2.0".equalsIgnoreCase(approvalApply.getVersion())){//新版本逻辑处理
			//获得最后待处理环节人
			List<ApprovalNodeUser> last = approvalNodeUserDAO.getNextApprovalNodeUserByApprovalId(approvalApply.getId());
			//若不存在则随时可以同意并完成
			if(last==null||last.isEmpty()) return true;
			//查询待处理节点记录
			long waitCount = approvalNodeUserDAO.countWaitByApprovalId(approvalApply.getId());
			//否则说明存在多个待处理节点 即不能同意并完成
			if(waitCount!=1) return false;
			ApprovalNodeUser lastUser= last.get(0);
			//看最后一个人是否是最后一个节点信息  是则可以同意并完成
			if(waitCount==1&&nextRecord.getApprovalUser().getPersonId().equals(lastUser.getPersonId())) return true;
		}else if(!StringUtils.isEmpty(ruleText)&&(ApprovalType.NORMAL==type||ApprovalType.KEY==type)){//旧数据兼容处理
			//获得非申请且已流转节点的全部信息 
			List<ApprovalApplyRecord> doneNodes = approvalApplyRecordDAO.findAllByApprovalId(approvalApply.getId(), true, false);
			if(doneNodes==null) doneNodes = new ArrayList<ApprovalApplyRecord>();
			//获得全部关键审批人
			ApprovalUserResult<UserSimpleInfo> reuslt = this.getFlowUserByRule(ruleText, approvalApply.getCreateUser().getNetworkId());
			if(!reuslt.getFlag()) return false;
			List<UserSimpleInfo> configUsers = reuslt.getDatas();
			if(configUsers==null||configUsers.isEmpty()) return false;
			boolean isDone = false;
			doneNodes.add(0, nextRecord);
			for (UserSimpleInfo configUser : configUsers) {
				isDone = false;
				for (ApprovalApplyRecord doneNode : doneNodes) {
					if(doneNode==null||doneNode.getApprovalUser()==null) continue;
					if(configUser.getPersonId().equals(doneNode.getApprovalUser().getPersonId())){
						isDone = true;
						break;
					}
				}
				if(!isDone) return false;//如果任意一个关键审批人没参与过那么不允许进行审批同意并完成审批
			}
		}
		return true;
	}

	@Override
	@Deprecated
	public ApprovalNodeUser getNearestUsersByFlowId(String id,UserIdentity user) {
		if(StringUtils.isBlank(id)) return null;
		ApprovalApply approvalApply = approvalApplyDAO.find(id);
		return getNearestUsersByFlowId(approvalApply,user);
	}

	@Override
	@Deprecated
	public ApprovalNodeUser getNearestUsersByFlowId(ApprovalApply approvalApply, UserIdentity user) {
		if(approvalApply==null) return null;
		if(approvalApply.getApprovalType()==null) return null;
		if(approvalApply.getNextApprover()!=null&&!approvalApply.getNextApprover().getOid().equals(user.getOid())) return null;
		if(approvalApply.getNextApprover()==null&&!approvalApply.getCreateUser().getOid().equals(user.getOid())) return null;
		//优先过得最近审批人信息若无则获得操作人上级信息
		return getLastFlowApprovalUser(approvalApply,user);
	}
	
	@Override
	public String approvalUserToJson(final String approvalUser,final String name,final String status){
		List<JSONObject> list=new ArrayList<JSONObject>();
		JSONArray array=new JSONArray();
		if(StringUtils.isEmpty(approvalUser)){
			JSONObject jsonObject=new JSONObject();
			jsonObject.put("name", name);
			jsonObject.put("status", status);
			list.add(jsonObject);
			return JSONArray.fromObject(list).toString(); 
		}else{
			array=JSONArray.fromObject(approvalUser);
			if(array!=null&&array.size()>=5) return array.toString();
			JSONObject last = array.getJSONObject((array.size()-1));
			if(last.getString("status").equals(AppovalRecordStatus.WAIT.getContext())&&last.getString("name").equals(name)){
				last.put("status", status);
			}else{
				JSONObject jsonObject=new JSONObject();
				jsonObject.put("name", name);
				jsonObject.put("status", status);
				array.add(jsonObject);
			}
		}
		return array.toString();
	}

	private ApprovalNodeUser getLastFlowApprovalUser(ApprovalApply approvalApply, UserIdentity otpUser){
		String aId= "";
		UserIdentity createUser = approvalApply.getCreateUser();
		if(approvalApply!=null) aId =approvalApply.getId();
		try {
			//获得最近一次审批的审批人信息
			if(!StringUtils.isEmpty(aId)){
				ApprovalApply flow = approvalApplyDAO.findLastFlow(aId, createUser.getNetworkId(), createUser.getUserId());
				if (flow != null) {
					String oId = flow.getFirstApproverOId();
					if(StringUtils.isBlank(oId)) return null;
					UserIdentity lastUser = getUserInfo(createUser.getNetworkId(), oId);
					if(lastUser==null) return null;
					return new ApprovalNodeUser(lastUser);
				}
			}
			//获得当前操作人的第一个上级
			List<User> users = getApprovalUserBy(otpUser.getNetworkId(), otpUser.getUserId());
			if (users != null && users.size() > 0) {
				// 按ActiveTime升序
				Collections.sort(users, new Comparator<Object>() {
					@Override
					public int compare(Object o1, Object o2) {
						Date date1 = ((User) o1).getActiveTime();
						Date date2 = ((User) o2).getActiveTime();
						if (date1 != null && date2 != null) {
							return date1.compareTo(date2);
						}
						return 0;
					}
				});
				UserIdentity leader=null;
				for (User user : users) {
					leader=null;
					if (user != null && StringUtils.isNotBlank(user.getOid()) && !user.getOid().equals(otpUser.getOid())) {
						leader = getUserInfo(otpUser.getNetworkId(), user.getOid());
						if(leader==null) continue;
						return new ApprovalNodeUser(leader);
					}
				}
			}
			return null;
		}catch (Exception e) {
			LOGGER.error("get leader failed  " + e.getMessage(), e);
		}
		return null;
	}
	
	/**
	 * 获得上级领导
	 * @param networkId
	 * @param userId
	 * @return
	 */
	private List<User> getApprovalUserBy(String networkId, String userId) {
		List<User> users = null;
		JSONObject requestParams = new JSONObject();
		requestParams.put("networkId", networkId);
		requestParams.put("userId", userId);
		String strUrl = lightappConfigureService.getOpenaccessHost()+ LightappURLConstants.OPENACCESS_GET_HEADER;
		try {
			String result = HttpUtil.sendPost(strUrl, requestParams.toString());
			JSONObject resultJson = null;
			JSONArray dataJsonArray = null;
			User user = null;
			if (!StringUtils.isEmpty(result)) {
				resultJson = JSONObject.fromObject(result);
				if (resultJson.getBoolean("success")) {
					dataJsonArray = resultJson.getJSONArray("data");
					int size = dataJsonArray.size();
					if (size > 0) {
						users = new ArrayList<User>(size);
						for (Object object : dataJsonArray) {
							JSONObject person = JSONObject.fromObject(object);
							user = new User();
							user.setId(person.getString("wbUserId"));
							if (!user.getId().equalsIgnoreCase(userId)) {
								user.setOid(person.getString("oId"));
								user.setName(person.getString("name"));
								user.setMobile(person.getString("phone"));
								user.setActiveTime(DateUtils.parse((person.getString("activeTime")),"yyyy-MM-dd HH:mm:ss"));
								users.add(user);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("getLeaderInfor()解析上级领导信息时出错：" + e.getMessage(), e);
		}
		return users;
	}
}
