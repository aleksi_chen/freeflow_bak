package com.kingdee.freeflow.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.kingdee.cbos.common.utils.StringUtils;
import com.kingdee.freeflow.dao.ApprovalCndDAO;
import com.kingdee.freeflow.dao.ApprovalCndGroupDAO;
import com.kingdee.freeflow.dao.ApprovalRuleDAO;
import com.kingdee.freeflow.domain.ApprovalCnd;
import com.kingdee.freeflow.domain.ApprovalCndGroup;
import com.kingdee.freeflow.domain.ApprovalRule;
import com.kingdee.freeflow.domain.UserSimpleInfo;
import com.kingdee.freeflow.service.ApprovalCndSevice;
import com.kingdee.freeflow.service.ApprovalUserSevice;
import com.kingdee.freeflow.service.impl.ApprovalUserSeviceImpl.ApprovalUserResult;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.service.RedisBaseService;
import com.kingdee.sns.lightapp.service.Tidings;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

@Service
@EnableAsync
public class ApprovalCndServiceImpl extends RedisBaseService<ApprovalCndGroup> implements ApprovalCndSevice{
	private final static Logger LOGGER = LoggerFactory.getLogger(ApprovalCndServiceImpl.class);

	@Autowired
	private ApprovalUserSevice approvalUserSevice;
	
	@Autowired
	private ApprovalCndGroupDAO approvalCndGroupDAO;
	
	@Autowired
	private ApprovalCndDAO approvalCndDAO;
	
	@Autowired
	private ApprovalRuleDAO approvalRuleDAO;
	
	@Override
	public long countBy(String ruleId){
		try {
			return approvalCndGroupDAO.countBy(ruleId);
		} catch (Exception e) {
			LOGGER.error("===审批记录统计失败===count",e);
			return 0;
		}
	}

	@Override
	public List<ApprovalCndGroup> getGroupsByRuleId(String ruleId, Integer start, Integer limit){
		if(StringUtils.isEmpty(ruleId)) return null;
		try {
			List<ApprovalCndGroup> groups = approvalCndGroupDAO.findCndGroupsByRuleId(ruleId, start, limit);
			return buildGroupsCnd(groups);
		} catch (Exception e) {
			LOGGER.error("===审批记录列表查询失败===getGroupsByRuleId",e);
			return null;
		}
	}
	
	@Override
	public List<ApprovalCndGroup> getAllByRuleId(String ruleId,boolean isOnlyEnabled){
		if(StringUtils.isEmpty(ruleId)) return null;
		try {
			List<ApprovalCndGroup> groups = approvalCndGroupDAO.findAllCndGroupsByRuleId(ruleId,isOnlyEnabled);
			return buildGroupsCnd(groups);
		} catch (Exception e) {
			LOGGER.error("===审批记录列表查询失败===getAllByRuleId",e);
			return null;
		}
	}

	@Override
	public Tidings saveApprovalCndGroups(List<ApprovalCndGroup> groups, ApprovalRule approvalRule,UserIdentity user) throws Exception {
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_NOTACCEPT);
		if(user==null||approvalRule==null||StringUtils.isEmpty(approvalRule.getId())) return msg;
		try {
			List<ApprovalCnd> allCnds = new ArrayList<ApprovalCnd>();
			if(groups!=null&&!groups.isEmpty()){
				for (ApprovalCndGroup group : groups) {
					group.setId(UUID.randomUUID().toString().replaceAll("-", ""));
					if(group.getStatus()==0) group.setStatus(1);//0是默认值 也就是前端没传状态  此时认为该条件组为有效条件
					group.setUser(user);
					group.setApprovalRuleId(approvalRule.getId());
					group.setCreateTime(new Date());
					if(!StringUtils.isEmpty(group.getUsersContext())){
						ApprovalUserResult<UserSimpleInfo> reuslt = approvalUserSevice.getFlowUserByRule(group.getUsersContext(), user.getNetworkId());
						if(!reuslt.getFlag()) return msg.setMsg(reuslt.getMsg()).setCode(LightappConstants.META_CODE_NOTPASS);
						List<UserSimpleInfo> users = reuslt.getDatas();
						if(users!=null&&!users.isEmpty()) group.setUsersContext(JSONArray.fromObject(users).toString());
					}
					if(group.getCnds()!=null&&!group.getCnds().isEmpty()){
						for (ApprovalCnd cnd : group.getCnds()) {
							cnd.setId(null);
							if(group.getStatus()==0) cnd.setStatus(1);
							cnd.setGroupId(group.getId());
							cnd.setCreateTime(new Date());
						}
						allCnds.addAll(group.getCnds());
					}
				}
			}
			//废弃旧条件
			List<ApprovalCndGroup> beforeGroups=approvalCndGroupDAO.findAllCndGroupsByRuleId(approvalRule.getId(),false);
			if(beforeGroups!=null&&!beforeGroups.isEmpty()) {
				List<String> gIds = new ArrayList<String>();
				for (ApprovalCndGroup approvalCndGroup : beforeGroups) {
					if(!StringUtils.isEmpty(approvalCndGroup.getId())) gIds.add(approvalCndGroup.getId());
				}
				if(gIds!=null&&!gIds.isEmpty()) approvalCndDAO.removeCnds(gIds);
			}
			//废弃旧条件组
			approvalCndGroupDAO.removeGroups(approvalRule.getId());//approvalCndGroupDAO.disableGroups(approvalRule.getId());
			//添加新条件
			if(allCnds!=null&&!allCnds.isEmpty()) approvalCndDAO.addBatch(allCnds);
			//添加新条件组
			if(groups!=null&&!groups.isEmpty()) approvalCndGroupDAO.addBatch(groups);
			msg.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_SAVE);
		} catch (Exception e) {
			LOGGER.error("===审批条件组保存失败===saveApprovalCndGroups",e);
			throw e;
		}
		return msg;
	}
	
	/**
	 * 条件组构建条件
	 * @param groups
	 * @return
	 * @throws Exception
	 */
	private List<ApprovalCndGroup> buildGroupsCnd(List<ApprovalCndGroup> groups) throws Exception {
		if(groups==null||groups.isEmpty()) return groups;
		List<String> gIds = new ArrayList<String>();
		for (ApprovalCndGroup group : groups) {
			gIds.add(group.getId());
		}
		List<ApprovalCnd> cnds = approvalCndDAO.findAllCndsByGroupIds(gIds);
		if(cnds==null||cnds.isEmpty()) return groups;
		for (ApprovalCndGroup group : groups) {
			for (ApprovalCnd cnd : cnds) {
				if(group.getId().equals(cnd.getGroupId())){
					group.addCnds(cnd);
				}
			}
		}
		return groups;
	}
}
