package com.kingdee.freeflow.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kingdee.cbos.common.utils.StringUtils;
import com.kingdee.freeflow.dao.FreeFlowUserDAO;
import com.kingdee.freeflow.domain.FreeFlowUser;
import com.kingdee.freeflow.service.OldApprovalService;
import com.kingdee.sns.lightapp.domain.UserNetwork;
import com.kingdee.sns.lightapp.service.UserNetworkService;
import com.kingdee.sns.lightapp.service.openorg.OpenorgService;

@Service
public class OldApprovalServiceImpl implements OldApprovalService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(OldApprovalServiceImpl.class);

	@Autowired
	private OpenorgService openorgService;
	@Autowired
	private UserNetworkService userNetworkService;
	@Autowired
	private FreeFlowUserDAO freeFlowUserDAO;
	
	class Task implements Runnable {

		private int page;
		
		private int all;

		public Task(final int page,final int all) {
			this.page = page;
			this.all = all;
		}
		
		private void printMsg(int index){
			int runIndex = (index+(this.page*30));
			double finnishPersent = ((double)runIndex/(double)this.all)*100;
			String msg = "=========FreeFlowUser 审批人信息补全 sum:"+ this.all + " index:" + runIndex + " 完成度：" + finnishPersent + "%";
			LOGGER.info(msg);
			System.out.println(msg);
		}

		public void run() {
			List<FreeFlowUser> freeFlowUsers = freeFlowUserDAO.findAlls(this.page,30);
			if(freeFlowUsers==null||freeFlowUsers.isEmpty()) return;
			boolean hasDept = false;
			boolean hasEid = false;
			boolean hasOpenId = false;
			boolean hasNetWork = false;
			boolean hasUserId = false;
			UserNetwork userNetWork=null;
			Map<String, Object> dept = null;
			int index=0;
			for(FreeFlowUser freeFlowUser:freeFlowUsers){
				userNetWork=null;
				hasDept =!StringUtils.isEmpty(freeFlowUser.getDeptId());
				if(hasDept){
					index++;
					printMsg(index);
					continue;
				}
				dept = null;
				hasEid =!StringUtils.isEmpty(freeFlowUser.getEid());
				hasOpenId =!StringUtils.isEmpty(freeFlowUser.getOpenId());
				hasNetWork =!StringUtils.isEmpty(freeFlowUser.getNetworkId());
				hasUserId =!StringUtils.isEmpty(freeFlowUser.getUserId());
				if(hasNetWork&&hasUserId){
					userNetWork=userNetworkService.getUserNetworkBy(freeFlowUser.getNetworkId(), freeFlowUser.getUserId(), null);
					if(userNetWork==null||StringUtils.isEmpty(userNetWork.getEid())||StringUtils.isEmpty(userNetWork.getOpenId())) continue;
				}else{
					continue;
				}
				if(hasEid&&hasOpenId){
					dept = openorgService.getDeptIdByEidAndOpenId(freeFlowUser.getEid(), freeFlowUser.getOpenId());
				}else if(hasNetWork&&hasUserId){
					dept = openorgService.getDeptIdByEidAndOpenId(userNetWork.getEid(), userNetWork.getOpenId());
				}
				try {
					freeFlowUser.setOpenId(userNetWork.getOpenId());
					freeFlowUser.setEid(userNetWork.getEid());
					freeFlowUser.setOid(userNetWork.getOid());
					freeFlowUser.setName(userNetWork.getName());
					if(dept!=null&&!dept.isEmpty()){
						if(dept.containsKey("deptId")) freeFlowUser.setDeptId(String.valueOf(dept.get("deptId")));
						if(dept.containsKey("deptName")) freeFlowUser.setDeptName(String.valueOf(dept.get("deptName")));
						if(dept.containsKey("isAdmin")) freeFlowUser.setIsAdmin((boolean)dept.get("isAdmin"));
						if(dept.containsKey("isDeptAdmin")) freeFlowUser.setIsDeptAdmin((boolean)dept.get("isDeptAdmin"));
					}
					freeFlowUserDAO.save(freeFlowUser);
				} catch (Exception e) {
					index++; printMsg(index);
					continue;
				}
				index++;
				printMsg(index);
				if((index+(page*30))==all){
					LOGGER.info("=========FreeFlowUser 审批人信息补全完成");
					System.out.println("=========FreeFlowUser 审批人信息补全完成");
				}
			}
		}
	}
	@Override
	public void updateDataPATHMain() {
		ThreadPoolExecutor executor = new ThreadPoolExecutor(30, 80, 5000,
				TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(30),
				new ThreadPoolExecutor.CallerRunsPolicy());
		int page=0;
		final int sum = (int) freeFlowUserDAO.count();
		System.out.println("=========FreeFlowUser 审批人信息补全  sum:"+sum);
		for (int i=0;i<(sum/30 + 2);i++) {
			final int key =  page;
			executor.execute(new Task(key,sum));
			page=page+1;
		}
		executor.shutdown();
	}
}
