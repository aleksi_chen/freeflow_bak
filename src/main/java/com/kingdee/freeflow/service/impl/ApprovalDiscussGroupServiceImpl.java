package com.kingdee.freeflow.service.impl;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.kingdee.cbos.common.utils.StringUtils;
import com.kingdee.freeflow.common.constants.AppDisGroupConstants;
import com.kingdee.freeflow.dao.ApprovalDiscussGroupDAO;
import com.kingdee.freeflow.dao.ApprovalNodeUserDAO;
import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.domain.ApprovalDiscussGroup;
import com.kingdee.freeflow.domain.ApprovalNodeUser;
import com.kingdee.freeflow.service.ApprovalDiscussGroupService;
import com.kingdee.freeflow.utils.JsonUtil;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.common.constants.LightappURLConstants;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.domain.xuntong.GroupMessageParam;
import com.kingdee.sns.lightapp.domain.xuntong.XunTongMessageItem;
import com.kingdee.sns.lightapp.service.FeatureService;
import com.kingdee.sns.lightapp.service.RedisBaseService;
import com.kingdee.sns.lightapp.service.Tidings;
import com.kingdee.sns.lightapp.service.UserNetworkService;
import com.kingdee.sns.lightapp.service.configure.LightappConfigureService;
import com.kingdee.sns.lightapp.service.openorg.OpenorgService;
import com.kingdee.sns.lightapp.service.xuntong.LAPPXunTongService;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

@Service
@EnableAsync
public class ApprovalDiscussGroupServiceImpl extends RedisBaseService<ApprovalApply> implements ApprovalDiscussGroupService {
	private final static Logger LOGGER = LoggerFactory.getLogger(ApprovalDiscussGroupServiceImpl.class);

	@Autowired
	private ApprovalDiscussGroupDAO approvalDiscussGroupDAO;
	
	@Autowired
	private UserNetworkService userNetworkService;
	
	@Autowired
	private OpenorgService openorgService;
	
	@Autowired
	private LightappConfigureService lightAppConfigureService;
	
	@Autowired
	private LAPPXunTongService lappXunTongService;
	
	@Autowired
	private ApprovalNodeUserDAO approvalNodeUserDAO;
	

	
	@Autowired
	private FeatureService featureService;

	@Override
	public boolean save(ApprovalDiscussGroup approvalDiscussGroup){
		boolean result = true;
		try {
			 approvalDiscussGroupDAO.save(approvalDiscussGroup);
		} catch (Exception e) {
			result = false;
			LOGGER.error(this.getClass() + " Method save failed: "
					+ e.getMessage());
		}
		return result;
	}

	@Override
	public Tidings createDiscussGroup(ApprovalApply approvalApply, UserIdentity user,Set<String> nodeUserSet){
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		JSONObject respData = null;
		Set<String> tempNodeUserSet = nodeUserSet;
		String groupId = null;
		String isOpenKey=featureService.getFeature(user.getOid(), user.getEid(), "isAutoGroupFreeFlow");
		boolean	isOpenFlag= (org.apache.commons.lang.StringUtils.isNotBlank(isOpenKey)&&Integer.valueOf(isOpenKey)==1)?true:false;
		boolean isCreatedGroup = false;
		LOGGER.info("=========创建审批会话组灰度开关========："+isOpenFlag);
		if(approvalApply != null && isOpenFlag){
			groupId = approvalApply.getGroupId();
			isCreatedGroup = StringUtils.isEmpty(groupId)?false:true;
			//approvalApply存在groupId（从会话组发起时）不再重新创建组
			if(!StringUtils.isEmpty(approvalApply.getGroupId())){
				return msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE).put("isCreatedGroup", true).put("groupId", approvalApply.getGroupId());
			}
			//获取审批人节点
			/*if(null == nodeUsers || nodeUsers.size()<2){
			    return msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE).put("isCreatedGroup", false).put("groupId", "");
			}
			tempUserIdList = new HashSet<String>();
			tempUserIdList.add(user.getPersonId());
			for(ApprovalNodeUser node :nodeUsers){
				tempUserIdList.add(node.getPersonId());
			}*/
			LOGGER.info("=========审批会话组tempUserIdList数据========："+tempNodeUserSet.toString());
			//先判断是否符合创建IM群组的条件(至少需要三个人的personId)
			if(tempNodeUserSet != null && !tempNodeUserSet.isEmpty() && tempNodeUserSet.size()>=3){
				List<String> userlist = new ArrayList<String>();
				userlist.addAll(tempNodeUserSet);
				String userIdsStr = this.builderJsonParam(userlist,approvalApply.getTitle()+AppDisGroupConstants.GROUP_NAME);//流程类型审批讨论组：如（请假申请-审批讨论组）
				String openToken = openorgService.getOpenTokenByOId(approvalApply.getCreateUser().getOid());//当前用户openToken信息
				String pushmsg_url = lightAppConfigureService.getXuntongInHost() + LightappURLConstants.XUNTONG_CREATE_GROUP;
				String responseData = lappXunTongService.createApprovalDisGroup(userIdsStr, pushmsg_url,openToken);
				LOGGER.info("=========createDiscussGroup创建审批会话组参数========userIdsStr："+userIdsStr+"====pushmsg_url:"+pushmsg_url+"====openToken:"+openToken+"====responseData:"+responseData);
				try {
					respData = JSONObject.fromObject(responseData);
					if(respData != null && respData.containsKey("success")){
						if((boolean) respData.get("success")){
							groupId=respData.getJSONObject("data").getString("groupId");
							if(!StringUtils.isEmpty(groupId))
								isCreatedGroup=true;
						}else{
							if(!"至少要有两个组员".equals(respData.getString("error"))){
								return msg.setFlag(false).setCode(LightappConstants.META_CODE_ERROR).setMsg("创建审批讨论组异常!");
							}
						}
					}
				} catch (Exception e) {
					LOGGER.error("===审批创建讨论组返回结果解析失败！===", e);
				}
			}
		}
		msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE).put("groupId", groupId).put("isCreatedGroup", isCreatedGroup);
		
		
		return msg;
	}
	
	/**
	 * {"userIds":["568b70ffe4b0e98bdb95a55f","56df98c0e4b0a5c137a04fef"],"groupName":"请假申请-审批讨论组"}
	 * 组装json参数值
	 * @param userList
	 * @return
	 */
	private String builderJsonParam(List<String> userList,String groupNameContent){
		String  startStr = "{\"userIds\":[",end = "}";
		String groupName = "\"groupName\":"+"\""+groupNameContent+"\"";
		String userIdStr = "";
		if(userList != null && !userList.isEmpty()){
			for(int i = 0, l = userList.size(); i < l ; i++){
				userIdStr += "\""+userList.get(i)+"\"";
				if(i != l -1){
					userIdStr += ",";
				}else{
					userIdStr += "],";
				}
			}
			return startStr + userIdStr +groupName+ end;
		}
		return "";
	}

	/**
	 * 發送消息
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void sendGroupMessage(final ApprovalApply approvalApply){
		LOGGER.info("-------sendGroupMessage入参------approvalApply："+(approvalApply != null ? JSONObject.fromObject(approvalApply) : ""));
		String content = "";
		if(approvalApply!=null&&StringUtils.isEmpty(approvalApply.getGroupId())) return;
		try{
				String yyyMMdd = new SimpleDateFormat("yyyy-MM-dd").format(approvalApply.getCreateTime());
				//title：邓敏的合同申请流程(2016-05-22)
				String title = MessageFormat.format(AppDisGroupConstants.APPROVAL_GROUP_CONTENT_TITLE, approvalApply.getCreateUser().getName(), approvalApply.getTitle(),yyyMMdd) +"\n";
				//获取申请单字段值内容
				try
				{
					if(approvalApply.getIsThird()==true){
						content = approvalApply.getTabloid();
					}else if(!StringUtils.isEmpty(approvalApply.getContent())){
						Map<String,Object> map = JsonUtil.getMapFromJsonArray("label", "value", approvalApply.getContent());
						if(map != null && !map.isEmpty()){
							int count = 0;
						   for (Iterator<String> it =  map.keySet().iterator();it.hasNext();)
						   {
							    Object key = it.next();
							    String value = (String) map.get(key);
							    if(!StringUtils.isEmpty(value)){
							    	value = value.length() > AppDisGroupConstants.APPROVAL_GROUP_FIELD_LENGTH ? value.substring(0, AppDisGroupConstants.APPROVAL_GROUP_FIELD_LENGTH)+"..." : value;
							    }
								content += key+":"+ value + "\n";
								count++;
								if(count >= AppDisGroupConstants.APPROVAL_GROUP_FIELD_COUNT) break;//最多只显示四个label内容
						   }
						}
					}
				} catch (Exception e)
				{
					LOGGER.info("send content message for approval apply:---->:"+e.getMessage());
				}finally{
					if(StringUtils.isEmpty(content)){content = title;}//如果字段内容是空则 以标题显示
				}
				
				// 添加url連接
				String webUrl = lightAppConfigureService.getYZJOutHost() + "/freeflow/m/approval/" + approvalApply.getId()+ "?lappName=freeflow";//&lappName=freeflow ?lappName=freeflow
				
				
				//String thumbUrl = lightAppConfigureService.getYZJOutHost() + AppDisGroupConstants.APPROVAL_GROUP_MSG_ICON;//图片缩略图
				String thumbUrl = lightAppConfigureService.getYZJOutHost() + AppDisGroupConstants.APPROVAL_GROUP_MSG_ICON;//图片缩略图
				//param消息参数
				GroupMessageParam groupMsgParam = new GroupMessageParam.GroupMessageParamBuilder(content,title,thumbUrl,webUrl)
												.setAppName(AppDisGroupConstants.LIGHT_APP_NAME)//来自：审批
												.setLightAppId(lightAppConfigureService.getAppFreeflowId())
												.setPubAccId(lightAppConfigureService.getPubaccFreeflowId())
												.setUnreadMonitor(1)
												.builder();
		
				//主体消息参数,msgType 7 是图文消息
				XunTongMessageItem messageItem = new XunTongMessageItem.XunTongMessageItemBuilder(approvalApply.getGroupId(), content, "", content.length(), "7")
												.setParam(groupMsgParam.getJsonObject())
												.builder();
						
				String pushmsg_url = lightAppConfigureService.getXuntongInHost() + LightappURLConstants.XUNTONG_SEND_MESSAGE;
				String openToken = openorgService.getOpenTokenByOId(approvalApply.getCreateUser().getOid());
				
				LOGGER.info("-------sendGroupMessage入参------pushmsg_url："+pushmsg_url+",openToken:"+openToken);
				
				boolean  success = lappXunTongService.sendMessage(messageItem, pushmsg_url,openToken);
				LOGGER.info("sendGroupMsg（）... result:{groupId"+approvalApply.getGroupId()+",success:"+success+"}");
		} catch (Exception e) {
			LOGGER.error("saveApproApply method send IM message exception---->:"+e.getMessage());
		}
	}
	
	@Override
	public Tidings addDiscussGroupUser(ApprovalApply approvalApply,Set<String> personSet) {
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		JSONObject respData = null;
		if(personSet != null && !personSet.isEmpty()){
			/*String checkPersonInGroupUrl = lightAppConfigureService.getXuntongInHost()+ LightappURLConstants.XUNTONG_PERSON_IN_GROUP;
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("personId", user.getPersonId());
			jsonObj.put("groupIds", new String[]{approvalApply.getGroupId()});*/
			
			//判断用户是否已经存在群组中
			/*boolean isPersonInGroup = lappXunTongService.isPersonInGroup(jsonObj.toString(),checkPersonInGroupUrl);
			LOGGER.info("=========该人员已经在该群组中========isPersonInGroup："+isPersonInGroup);
			if(isPersonInGroup)
				 return msg.setFlag(false).setCode(LightappConstants.META_CODE_ERROR).setMsg("该人员已经在该群组中!");*/
			
			
			
			String userIdsStr = this.builderAddGroupUserJsonParam(personSet,approvalApply.getGroupId());//流程类型审批讨论组：如（请假申请-审批讨论组）
			String openToken = openorgService.getOpenTokenByOId(approvalApply.getApprover().getOid());//当前用户openToken信息
			String pushmsg_url = lightAppConfigureService.getXuntongInHost()+ LightappURLConstants.XUNTONG_ADDUSER_GROUP;
			String responseData = lappXunTongService.addGroupUser(userIdsStr, pushmsg_url,openToken);
			LOGGER.info("=========addDiscussGroupUser创建审批会话组参数========userIdsStr："+userIdsStr+"====pushmsg_url:"+pushmsg_url+"====openToken:"+openToken+"====responseData:"+responseData);
			try {
				respData = JSONObject.fromObject(responseData);
				if(respData != null && respData.containsKey("success")){
					if(!(boolean) respData.get("success") && !("不能重复添加自己".equals(respData.getString("error")) || "至少要有两个组员".equals(respData.getString("error")))){
							return msg.setFlag(false).setCode(LightappConstants.META_CODE_ERROR).setMsg("创建审批讨论组异常!");
					}
				}
			} catch (Exception e) {
				LOGGER.error("===审批创建讨论组返回结果解析失败！===", e);
			}
		}
		msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE);
		return msg;
	}
	
	/**
	 * {"userIds":["568b70ffe4b0e98bdb95a55f","56df98c0e4b0a5c137a04fef"],"groupId":"xxx"}
	 * 组装json参数值
	 * @param userList
	 * @return
	 */
	private String builderAddGroupUserJsonParam(Set<String> personSet,String groupId){
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("userIds", personSet);
		jsonObj.put("groupId", groupId);
		
		return jsonObj.toString();
	}

	@Override
	public Tidings createDiscussGroupExcludeStatus(ApprovalApply approvalApply, UserIdentity user,Set<String> personSet){
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		JSONObject respData = null;
		String groupId = approvalApply.getGroupId();
		List<String> tempUserIdList = null;
		String isOpenKey=featureService.getFeature(user.getOid(), user.getEid(), "isAutoGroupFreeFlow");
		boolean	isOpenFlag= (org.apache.commons.lang.StringUtils.isNotBlank(isOpenKey)&&Integer.valueOf(isOpenKey)==1)?true:false;
		LOGGER.info("=========创建审批会话组灰度开关========："+isOpenFlag);
		if(isOpenFlag){//会话组灰度开关
			if(StringUtils.isEmpty(groupId)){//approvalApply沒有groupId才重新創建
				tempUserIdList=new ArrayList<String>();
				for(String personId :personSet){
					tempUserIdList.add(personId);
				}
			}else{
				return msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE).put("isCreatedGroup", true).put("groupId", approvalApply.getGroupId());
			}
			
			LOGGER.info("=========审批会话组tempUserIdList数据========："+tempUserIdList.toString());
			
			//先判断是否符合创建IM群组的条件(至少需要三个人的personId)，获取与会的personId个数
			if(tempUserIdList != null && !tempUserIdList.isEmpty()){
				//再进行业务逻辑处理
				//组装json格式的useIds字符串参数 ，userIdsStr = "{\"userIds\":[\"568b70ffe4b0e98bdb95a55f\",\"56df98c0e4b0a5c137a04fef\"]}";
				String userIdsStr = this.builderJsonParam(tempUserIdList,approvalApply.getTitle()+AppDisGroupConstants.GROUP_NAME);//流程类型审批讨论组：如（请假申请-审批讨论组）
				String openToken = openorgService.getOpenTokenByOId(user.getOid());//当前用户openToken信息
				String  pushmsg_url = lightAppConfigureService.getXuntongInHost() + LightappURLConstants.XUNTONG_CREATE_GROUP;
				String responseData = lappXunTongService.createApprovalDisGroup(userIdsStr, pushmsg_url,openToken);
				LOGGER.info("=========createDiscussGroup创建审批会话组参数========userIdsStr："+userIdsStr+"====pushmsg_url:"+pushmsg_url+"====openToken:"+openToken+"====responseData:"+responseData);
				try {
					respData = JSONObject.fromObject(responseData);
					if(respData != null && respData.containsKey("success")){
						if((boolean) respData.get("success")){
							groupId=respData.getJSONObject("data").getString("groupId");
						}else{
							System.out.println("error:"+respData.getString("error"));
							if(!"至少要有两个组员".equals(respData.getString("error"))){
								return msg.setFlag(false).setCode(LightappConstants.META_CODE_ERROR).setMsg("创建审批讨论组异常!");
							}
						}
					}
				} catch (Exception e) {
					LOGGER.error("===审批创建讨论组返回结果解析失败！===", e);
				}
			}
		}/*else{
			msg.setFlag(false).setCode(LightappConstants.META_CODE_SAVE).put("groupId", groupId);//灰度开关是关闭的状态，则不创建huihua
		}*/
		msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE).put("groupId", groupId);
		return msg;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void sendUserMessage(final ApprovalApply approvalApply,String userId){
		String content = "";
		try{
			    //避免自己给自己发送图文消息
			    if(approvalApply.getCreateUser().getPersonId().equals(userId)){
			    	return;
			    }
				String yyyMMdd = new SimpleDateFormat("yyyy-MM-dd").format(approvalApply.getCreateTime());
				//title：邓敏的合同申请流程(2016-05-22)
				String title = MessageFormat.format(AppDisGroupConstants.APPROVAL_GROUP_CONTENT_TITLE, approvalApply.getCreateUser().getName(), approvalApply.getTitle(),yyyMMdd) +"\n";
				//获取申请单字段值内容
				try
				{
					if(approvalApply.getIsThird()==true){
						content = approvalApply.getTabloid();
					}else if(!StringUtils.isEmpty(approvalApply.getContent())){
						Map<String,Object> map = JsonUtil.getMapFromJsonArray("label", "value", approvalApply.getContent());
						if(map != null && !map.isEmpty()){
							int count = 0;
						   for (Iterator<String> it =  map.keySet().iterator();it.hasNext();)
						   {
							    Object key = it.next();
							    String value = (String) map.get(key);
							    if(!StringUtils.isEmpty(value)){
							    	value = value.length() > AppDisGroupConstants.APPROVAL_GROUP_FIELD_LENGTH ? value.substring(0, AppDisGroupConstants.APPROVAL_GROUP_FIELD_LENGTH)+"..." : value;
							    }
								content += key+":"+ value + "\n";
								count++;
								if(count >= AppDisGroupConstants.APPROVAL_GROUP_FIELD_COUNT) break;//最多只显示四个label内容
						   }
						}
					}
				} catch (Exception e)
				{
					LOGGER.info("send content message for approval apply:---->:"+e.getMessage());
				}finally{
					if(StringUtils.isEmpty(content)){content = title;}//如果字段内容是空则 以标题显示
				}
				
				// 添加url連接
				String webUrl = lightAppConfigureService.getYZJOutHost() + "/freeflow/m/approval/" + approvalApply.getId()+ "?lappName=freeflow";//&lappName=freeflow ?lappName=freeflow
				
				
				String thumbUrl = lightAppConfigureService.getYZJOutHost() + AppDisGroupConstants.APPROVAL_GROUP_MSG_ICON;
				
				//param消息参数
				GroupMessageParam groupMsgParam = new GroupMessageParam.GroupMessageParamBuilder(content,title,thumbUrl,webUrl)
												.setAppName(AppDisGroupConstants.LIGHT_APP_NAME)//来自：审批
												.setLightAppId(lightAppConfigureService.getAppFreeflowId())
												.setPubAccId(lightAppConfigureService.getPubaccFreeflowId())
												.setUnreadMonitor(1)
												.builder();
		
				//主体消息参数,msgType 7 是图文消息
				XunTongMessageItem messageItem = new XunTongMessageItem.XunTongMessageItemBuilder(null, content, userId, content.length(), "7")
												.setParam(groupMsgParam.getJsonObject())
												.builder();
						
				String pushmsg_url = lightAppConfigureService.getXuntongInHost() + LightappURLConstants.XUNTONG_SEND_MESSAGE;
				String openToken = openorgService.getOpenTokenByOId(approvalApply.getCreateUser().getOid());
				boolean  success = lappXunTongService.sendMessage(messageItem, pushmsg_url,openToken);
				LOGGER.info("sendUserMessage（）... result:{userId"+userId+",success:"+success+"}");
		} catch (Exception e) {
			LOGGER.info("saveApproApply method send IM message exception---->:"+e.getMessage());
		}
	}

}
