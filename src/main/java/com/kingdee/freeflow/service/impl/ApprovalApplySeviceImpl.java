package com.kingdee.freeflow.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import redis.clients.jedis.Jedis;

import com.ibm.icu.text.SimpleDateFormat;
import com.kingdee.cbos.common.utils.StringUtils;
import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.dao.ApprovalApplyDAO;
import com.kingdee.freeflow.dao.ApprovalApplyRecordDAO;
import com.kingdee.freeflow.dao.ApprovalDetailDAO;
import com.kingdee.freeflow.dao.ApprovalNodeUserDAO;
import com.kingdee.freeflow.dao.ApprovalRuleDAO;
import com.kingdee.freeflow.dao.ReceiptNoDAO;
import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.domain.ApprovalApplyRecord;
import com.kingdee.freeflow.domain.ApprovalCnd;
import com.kingdee.freeflow.domain.ApprovalCndGroup;
import com.kingdee.freeflow.domain.ApprovalDetail;
import com.kingdee.freeflow.domain.ApprovalNodeUser;
import com.kingdee.freeflow.domain.ApprovalRule;
import com.kingdee.freeflow.domain.ReceiptNo;
import com.kingdee.freeflow.domain.UserSimpleInfo;
import com.kingdee.freeflow.enums.AppovalRecordStatus;
import com.kingdee.freeflow.enums.AppovalStatus;
import com.kingdee.freeflow.enums.ApprovalType;
import com.kingdee.freeflow.service.ApprovalApplySevice;
import com.kingdee.freeflow.service.ApprovalCndSevice;
import com.kingdee.freeflow.service.ApprovalDiscussGroupService;
import com.kingdee.freeflow.service.ApprovalMessageSevice;
import com.kingdee.freeflow.service.ApprovalUserSevice;
import com.kingdee.freeflow.service.impl.ApprovalUserSeviceImpl.ApprovalUserResult;
import com.kingdee.freeflow.utils.RecurseApprovalDetail;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.domain.template.FormTemplate;
import com.kingdee.sns.lightapp.service.RedisBaseService;
import com.kingdee.sns.lightapp.service.Tidings;
import com.kingdee.sns.lightapp.service.formtemplate.FormTemplateService;
import com.kingdee.sns.lightapp.service.openorg.OpenorgService;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

@Service("approvalApplySevice")
@EnableAsync
public class ApprovalApplySeviceImpl extends RedisBaseService<ApprovalApply> implements ApprovalApplySevice{
	private final static Logger LOGGER = LoggerFactory.getLogger(ApprovalApplySeviceImpl.class);
	@Autowired
	private ApprovalApplyDAO approvalApplyDAO;
	
	@Autowired
	private ApprovalApplyRecordDAO approvalApplyRecordDAO;
	
	@Autowired
	private ReceiptNoDAO receiptNoDAO;
	
	@Autowired
	private ApprovalDetailDAO approvalDetailDAO;
	
	@Autowired
	private ApprovalRuleDAO approvalRuleDAO;
	
	@Autowired
	private ApprovalNodeUserDAO approvalNodeUserDAO;
	
	@Autowired
	private ApprovalMessageSevice approvalMessageSevice;
	
	@Autowired
	private FormTemplateService formTemplateService;
	
	@Autowired
	private ApprovalUserSevice approvalUserSevice;
	
	@Autowired
	private ApprovalCndSevice approvalCndSevice;
	
	@Autowired
	private ApprovalDiscussGroupService approvalDiscussGroupService;
	
	@Autowired
	private OpenorgService openorgService;

	@Override
	public long count(ParamsObject paramsObejct) {
		try {
			return approvalApplyDAO.count(paramsObejct);
		} catch (Exception e) {
			LOGGER.error("===审批申请统计失败===count",e);
			return 0;
		}
	}

	@Override
	public List<ApprovalApply> findAll(ParamsObject paramsObejct,String sort, Integer start,Integer limit) {
		try {
			return approvalApplyDAO.findAll(paramsObejct,sort,start,limit);
		} catch (Exception e) {
			LOGGER.error("===审批申请列表查询失败===findAll",e);
			return null;
		}
	}
	
	@Override
	public ApprovalApply findById(String id) {
		if(StringUtils.isEmpty(id)) return null;
		try {
			return approvalApplyDAO.findOneById(id);
		} catch (Exception e) {
			LOGGER.error("===审批申请详情查询失败===findById",e);
			return null;
		}
	}
	
	@Override
	public ApprovalApply findByReceiptId(String receiptId) {
		if(StringUtils.isEmpty(receiptId)) return null;
		try {
			return approvalApplyDAO.findByReceiptId(receiptId);
		} catch (Exception e) {
			LOGGER.error("===审批申请详情查询失败===findByReceiptId",e);
			return null;
		}
	}

	private void save(ApprovalApply approvalApply) throws Exception {
		if(approvalApply==null) throw new Exception();
		try {
			approvalApply.setReceiptId(generateReceiptId(approvalApply));
			approvalApplyDAO.save(approvalApply);
		} catch (Exception e) {
			LOGGER.error("===审批申请保存失败===save",e);
			throw e;
		}
	}

	@Async
	@Override
	public void asyncSaveApprovalDetail(String id, String content, UserIdentity user){
		RecurseApprovalDetail tool=new RecurseApprovalDetail();
		approvalDetailDAO.update(id, true);
		ApprovalApply approvalApply=approvalApplyDAO.findOneById(id);
		if(StringUtils.isEmpty(content)) return;
		List<ApprovalDetail> list=tool.recurse(content);
		if(list==null||list.isEmpty()) return;
		for(ApprovalDetail app:list){
			app.setApprovalId(id);
			app.setTemplateId(approvalApply.getTenantId());
			app.setApplyUser(user);
			approvalDetailDAO.save(app);
		}
	}
	
	@Override
	public Tidings saveFlow(ApprovalApply approvalApply, UserIdentity user, List<ApprovalNodeUser> nodeUsers, String transferApproverOId){
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_NOTACCEPT);
		if(approvalApply==null) return msg;
		if(StringUtils.isEmpty(approvalApply.getId())){
			try {
				//根据模板id查询模板信息
				FormTemplate formTemplate= formTemplateService.getFormTemplateById(approvalApply.getFormTemplateId());
				if(formTemplate==null) return msg.setFlag(false).setCode(LightappConstants.META_CODE_NOTFOUND).setMsg("审批模板不存在!");
				ApprovalRule rule = approvalRuleDAO.getRuleByFid(approvalApply.getFormTemplateId());
				
				//若是无规则默认认为是自由流（兼容老数据）
 				if(rule==null){
					rule=new ApprovalRule();
					rule.setTemplateId(formTemplate.getId());
					rule.setCreateTime(new Date()); 
					rule.setApprovalType(ApprovalType.FREE.getContext());
				}
 				
 				//TODO:创建审批申请单时，下个审批人是部门负责人时才需要判断，能否获取上级信息···
				//从rule的获取第一个部门负责审批人
				boolean isFirstDeptApporverIsNull = this.checkTheFirstDeptApproverIsNull(rule,user);
				if(isFirstDeptApporverIsNull)  return msg.setFlag(false).setCode(LightappConstants.META_CODE_NODEPTAPPROVER).setMsg("你所在部门为设置部门负责人,请通知管理员设置,否则无法提交流程");
				
 				
				//设置申请基础信息
				approvalApply.setTitle(formTemplate.getTitle());
				approvalApply.setApprovalType(rule.getApprovalType());
				approvalApply.setCreateTime(new Date());
				approvalApply.setLastUpdateTime(new Date());
				approvalApply.setStatus(AppovalStatus.DOING);
				approvalApply.setApprover(user);
				approvalApply.setCreateUser(user);
				approvalApply.setVersion("2.0");
				//创建审批
				msg = createApply(approvalApply, rule, user, nodeUsers, transferApproverOId);
				if(!msg.isFlag()) return msg;
				JSONObject app=new JSONObject();
				app.put("id", approvalApply.getId());
				msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE).setMsg("操作成功").put("approvalApply", app);
			} catch (Exception e) {
				if(!StringUtils.isEmpty(approvalApply.getId())){
					approvalApplyRecordDAO.deleteByApprovalId(approvalApply.getId());
					approvalApplyDAO.delete(approvalApply.getId());
				} 
				LOGGER.error("===审批申请创建失败===saveFlow",e);
				msg.setFlag(false).setCode(LightappConstants.META_CODE_ERROR).setMsg("审批申请操作异常!");
			}
		}else{
			ApprovalApply apply=this.findById(approvalApply.getId());
			if(apply!=null){
				JSONObject app=new JSONObject();
				app.put("id", approvalApply.getId());
				msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE).setMsg("操作成功").put("approvalApply", app);
			}else{
				msg.setFlag(false).setCode(LightappConstants.META_CODE_SAVE).setMsg("参数校验不通过，非法的请求！");
			}
		}
		return msg;
	}
	
	/**
	 * 
	 * @return
	 */
	private boolean checkTheFirstDeptApproverIsNull(ApprovalRule rule,UserIdentity user){
		if(rule != null){
			//从rule获取审批人
			String personStr = rule.getContent();
			
			JSONArray personArray =  JSONArray.fromObject(personStr);
			if(personArray != null && !personArray.isEmpty() && personArray.size() >= 1){
				JSONObject personObj = (JSONObject) personArray.get(0);//获取第一个
				
				if(personObj != null && personObj.containsKey("deptRoleType") && personObj.containsKey("deptLevel")){
					boolean isDeptRoleTypeFlag = personObj.getBoolean("deptRoleType");
					int deptLevel = personObj.getInt("deptLevel");
					if(isDeptRoleTypeFlag && deptLevel == 1){//第一级部门审批人
						
						//获取当前提交审批人的上级是否为空
						Set<Object> personSet = openorgService.getParentPersonSet(user.getEid(), user.getOid());
						if(personSet != null && personSet.isEmpty())
							return true;//return msg.setFlag(Boolean.FALSE).setMsg("你所在部门为设置部门负责人,请通知管理员设置,否则无法提交流程");
					}
				}
			}
			
		}
		return false;
	}
	
	
	
	@Override
	public Tidings saveFlowForThrid(ApprovalApply approvalApply, ApprovalRule rule, UserIdentity user, List<ApprovalNodeUser> nodeUsers, String transferApproverOId){
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_NOTACCEPT);
		if(approvalApply==null) return msg;
		if(StringUtils.isEmpty(approvalApply.getId())){
			try {
				//创建审批
				msg = createApply(approvalApply, rule, user, nodeUsers, transferApproverOId);
				if(!msg.isFlag()) return msg;
				JSONObject app=new JSONObject();
				app.put("id", approvalApply.getId());
				app.put("receiptId", approvalApply.getReceiptId());
				app.put("rejectId", approvalApply.getRejectId());
				msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE).setMsg("操作成功").setData(null).put("apply", app);
			} catch (Exception e) {
				if(!StringUtils.isEmpty(approvalApply.getId())){
					approvalApplyRecordDAO.deleteByApprovalId(approvalApply.getId());
					approvalApplyDAO.delete(approvalApply.getId());
				} 
				LOGGER.error("===审批申请创建失败===saveFlow",e);
				msg.setFlag(false).setCode(LightappConstants.META_CODE_ERROR).setMsg("审批申请操作异常!").setData(null);
			}
		}else{
			ApprovalApply apply=this.findById(approvalApply.getId());
			if(apply!=null){
				JSONObject app=new JSONObject();
				app.put("id", approvalApply.getId());
				app.put("receiptId", approvalApply.getReceiptId());
				app.put("rejectId", approvalApply.getRejectId());
				msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE).setMsg("操作成功").setData(null).put("apply", app);
			}else{
				msg.setFlag(false).setCode(LightappConstants.META_CODE_SAVE).setMsg("参数校验不通过，非法的请求！").setData(null);
			}
		}
		return msg;
	}
	
	private Tidings createApply(ApprovalApply approvalApply, ApprovalRule rule, UserIdentity user, List<ApprovalNodeUser> nodeUsers, String transferApproverOId) throws Exception{
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_NOTACCEPT);
		//构建申请人的审批记录
		ApprovalApplyRecord createRecord = new ApprovalApplyRecord(null, approvalApply.getTitle(),approvalApply.getApplyTitle(),
				null,approvalApply.getFormTemplateId(), AppovalRecordStatus.CREATE.getContext(), new Date(),
				null,user, user,new Date());
		//设置下一个环节审批人
		UserIdentity nextApprovalUser = new UserIdentity();
		ApprovalApplyRecord nextApprovalRecord = new ApprovalApplyRecord();
		//校验并设置审批流转人信息
		if(nodeUsers==null) nodeUsers=new ArrayList<ApprovalNodeUser>();
		Tidings isPass = this.setFlowNode(approvalApply, rule, nodeUsers, transferApproverOId, nextApprovalUser, nextApprovalRecord);
		if(!isPass.isFlag())  return isPass;
		//验证是否合法的审批人
		if(nextApprovalUser==null
				||StringUtils.isEmpty(nextApprovalUser.getUserId())
				||nextApprovalRecord==null
				||nextApprovalRecord.getApprovalUser()==null
				||StringUtils.isEmpty(nextApprovalRecord.getApprovalUser().getUserId())) 
			return msg.setFlag(false).setCode(LightappConstants.META_CODE_NOTPASS).setMsg("非法的下一个审批人!");
		
		//获取审批节点人员
		Set<String> nodeUserSet = new LinkedHashSet<String>(); 
		nodeUserSet.add(user.getPersonId());//当前操作用户
		for(ApprovalNodeUser node :nodeUsers){//添加待审批的节点人员
			nodeUserSet.add(node.getPersonId());
		}
		
		//创建会话组
		msg = approvalDiscussGroupService.createDiscussGroup(approvalApply, user, nodeUserSet);
		if(!msg.isFlag()) return msg;
		msg.put("groupId", (String) msg.get("groupId")).put("isCreatedGroup", (boolean) msg.get("isCreatedGroup"));
		//需要将groupId更新到approvalApply
		if((boolean) msg.get("isCreatedGroup")) approvalApply.setGroupId((String)msg.get("groupId"));
		//保存申请
		this.save(approvalApply);
		//生成自己的审批记录
		createRecord.setFormId(approvalApply.getId());
		createRecord.setTransferApproverOId(nextApprovalUser.getOid());
		approvalApplyRecordDAO.save(createRecord);
		//生成下一环节的审批记录
		nextApprovalRecord.setFormId(approvalApply.getId());
		approvalApplyRecordDAO.save(nextApprovalRecord);
		//生成审批环节人记录
		createNodeUser(approvalApply, nodeUsers);
		//回写记录数据
		approvalApply.setRecordId(createRecord.getId());
		approvalApply.setNextRecordId(nextApprovalRecord.getId());
		approvalApply.setNextApprover(nextApprovalUser);
		//审批人冗余信息（存储5个用于管理页展现）
		approvalApply.setApprovalUser(approvalUserSevice.approvalUserToJson(approvalApply.getApprovalUser(), user.getName(), AppovalRecordStatus.CREATE.getContext()));
		approvalApply.setApprovalUser(approvalUserSevice.approvalUserToJson(approvalApply.getApprovalUser(), nextApprovalUser.getName(), AppovalRecordStatus.WAIT.getContext()));
		approvalApplyDAO.save(approvalApply);
		
		//发送消息给下一个节点审批者
		approvalMessageSevice.sendTodoMessage(approvalApply.getId(),approvalApply.getNextRecordId(),AppovalRecordStatus.CREATE,approvalApply.getCreateUser(),approvalApply.getNextApprover());
		//发送群组消息,如果没有群组，给单人发送消息
		if((boolean) msg.get("isCreatedGroup")){
			Set<String> personSet=new HashSet<String>();
			personSet.add(approvalApply.getCreateUser().getPersonId());//申请人的personId
			List<ApprovalNodeUser> nodeList =  approvalNodeUserDAO.getAllApprovalNodeUsersByApprovalId(approvalApply.getId(), 0, 0);
			if(null != nodeList && nodeList.size()>0){
				for(ApprovalNodeUser node:nodeList){
					personSet.add(node.getPersonId());
				}
			}
			
			approvalDiscussGroupService.addDiscussGroupUser(approvalApply, personSet);//在群组会话组发起审批时，将审批的链路人员拉入该群
			
			approvalDiscussGroupService.sendGroupMessage(approvalApply);
		}else{
			approvalDiscussGroupService.sendUserMessage(approvalApply, nextApprovalUser.getPersonId());
		}
		//发送系统消息（会话组内）
		approvalMessageSevice.sendSYSMessageForApproval(approvalApply, user, AppovalRecordStatus.CREATE);
		//生成报表信息
		if(!StringUtils.isEmpty(approvalApply.getId())) asyncSaveApprovalDetail(approvalApply.getId(),approvalApply.getContent(),approvalApply.getCreateUser());
		return msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE).setMsg("操作成功");
	}
	
	/**
	 * 生成审批环节人记录
	 * @param id
	 * @param nodeUsers
	 */
	private void createNodeUser(ApprovalApply approvalApply,List<ApprovalNodeUser> nodeUsers){
		if(!StringUtils.isEmpty(approvalApply.getId())&&nodeUsers!=null&&!nodeUsers.isEmpty()){
			//这里10000作为排序间隔是由于后续流转有可能加多个人考虑
			int num=10000;
			for (ApprovalNodeUser nodeUser : nodeUsers) {
				nodeUser.setId(null);
				nodeUser.setApprovalId(approvalApply.getId());
				nodeUser.setIsDone(false);
				nodeUser.setNumber(num);
				num+=10000;
			}
			approvalNodeUserDAO.addBatch(nodeUsers);
		}
	}
	
	/**
	 * 设置审批节点流转信息
	 * @param user
	 * @param approvalApply
	 * @param rule
	 * @param transferApproverOId
	 * @param nextApprovalUser
	 * @param recrods
	 */
	private Tidings setFlowNode(ApprovalApply approvalApply,ApprovalRule rule,List<ApprovalNodeUser> nodeUsers,String transferApproverOId,UserIdentity nextApprovalUser,ApprovalApplyRecord nextApprovalRecord) {
		Tidings msg = new Tidings().init("非法的审批人设置!",LightappConstants.META_CODE_NOTPASS);
		//初始化自定义审批人
		if(nodeUsers!=null&&!nodeUsers.isEmpty()){
			List<String> oIds = new ArrayList<String>();
			for (ApprovalNodeUser nodeUser : nodeUsers) {
				if(StringUtils.isEmpty(nodeUser.getoId())) return msg;
				oIds.add(nodeUser.getoId());
			}
			if(oIds!=null&&!oIds.isEmpty()){
				nodeUsers.clear();
				ApprovalUserResult<ApprovalNodeUser> result =approvalUserSevice.getNodeUsersInfo(approvalApply.getCreateUser().getNetworkId(), oIds);
				if(!result.getFlag()) return msg.setMsg(result.getMsg());
				nodeUsers.addAll(result.getDatas());
			}
		}
		if(ApprovalType.FREE==ApprovalType.getApprovalType(approvalApply.getApprovalType())){//自由流处理
			if(StringUtils.isEmpty(transferApproverOId)) return msg;
			UserIdentity nextUser = approvalUserSevice.getUserInfo(approvalApply.getApprover().getNetworkId(), transferApproverOId);
			if(nextUser==null) return msg;
			//设置规则
			approvalApply.setApprovalRule(null);//这里仅存关键审批人
			//设置下一个审批人
			nextApprovalUser.copy(nextUser);
			//设置下一个审批环节记录
			nextApprovalRecord.initData(approvalApply.getId(),approvalApply.getTitle(),approvalApply.getApplyTitle(),
					null,approvalApply.getFormTemplateId(),AppovalRecordStatus.WAIT.getContext(),null,
					null,approvalApply.getApprover(),nextApprovalUser,new Date());
			if(nodeUsers!=null&&!nodeUsers.isEmpty()&&!nodeUsers.get(0).getPersonId().equals(nextApprovalUser.getPersonId())){
				nodeUsers.add(0,new ApprovalNodeUser(nextApprovalUser));
			}
			return msg.setFlag(true).setCode(LightappConstants.META_CODE_GET).setMsg("操作成功");
		}else if(ApprovalType.NORMAL==ApprovalType.getApprovalType(approvalApply.getApprovalType())){//关键审批人处理
			//构建流转信息
			return this.buildNode(approvalApply, rule.getContent(), nodeUsers, transferApproverOId, nextApprovalUser, nextApprovalRecord);
		}else if(ApprovalType.KEY==ApprovalType.getApprovalType(approvalApply.getApprovalType())){//指定条件关键审批人处理
			//获得表单组件数据
			JSONArray formDATA = JSONArray.fromObject(approvalApply.getContent());
			//获得规则条件数据
			List<ApprovalCndGroup> cnds =  approvalCndSevice.getAllByRuleId(rule.getId(),true);
			//比对分析规则结果
			String rulesText = analyseCnd(approvalApply,formDATA, cnds);
			//构建流转信息
			return this.buildNode(approvalApply, rulesText, nodeUsers, transferApproverOId, nextApprovalUser, nextApprovalRecord);
		}else {
			return msg;
		}
	}
	
	/**
	 * 构建流转信息
	 * 1.设置审批规则
	 * 2.合并自定义审批人与关键审批人
	 * 3.找到符合条件的首位推荐审批人
	 * @param approvalApply
	 * @param rulesText
	 * @param nodeUsers
	 * @param transferApproverOId
	 * @param nextApprovalUser
	 * @param nextApprovalRecord
	 * @return
	 */
	private Tidings buildNode(ApprovalApply approvalApply,String rulesText,List<ApprovalNodeUser> nodeUsers,String transferApproverOId,UserIdentity nextApprovalUser,ApprovalApplyRecord nextApprovalRecord){
		Tidings msg = new Tidings().init("非法的审批人设置!",LightappConstants.META_CODE_NOTACCEPT);
		List<ApprovalNodeUser> keyUsers = new ArrayList<ApprovalNodeUser>();
		//设置 approvalApply.approvalRule 与 keyUsers
		if(!StringUtils.isEmpty(rulesText)){
			//从rulesText获取审批关键人信息，然后去查找userNetwork表，匹配每一个关键审批人是否存在并在职
			ApprovalUserResult<UserSimpleInfo> reuslt = approvalUserSevice.getFlowUserByRule(rulesText, approvalApply.getCreateUser().getNetworkId());
			if(!reuslt.getFlag()) return msg.setMsg(reuslt.getMsg());
			List<UserSimpleInfo> users = reuslt.getDatas();
			if(users!=null&&!users.isEmpty()){
				for (UserSimpleInfo user : users) {
					ApprovalNodeUser node = new ApprovalNodeUser(user);
					node.setIsKey(true);//是否是关键审批人
					keyUsers.add(node);
				}
				approvalApply.setApprovalRule(JSONArray.fromObject(users).toString());//这里仅存关键审批人
			} else {
				approvalApply.setApprovalRule("");
			}
		} else {
			approvalApply.setApprovalRule("");
		}
		if(!StringUtils.isEmpty(transferApproverOId)){//存在指定流转人
			UserIdentity nextUser = approvalUserSevice.getUserInfo(approvalApply.getApprover().getNetworkId(), transferApproverOId);
			if(nextUser==null) return msg.setMsg("转审批人不在职！！！");
			nextApprovalUser.copy(nextUser);
		}
		//合并审批节点用户   结果=指定流转人+指定新加流转节点+关键审批人
		if(keyUsers!=null&&!keyUsers.isEmpty()) nodeUsers.addAll(keyUsers);
		//若是指定流转人不是第一个流转节点人 插入首节点
		if(nextApprovalUser!=null&&!StringUtils.isEmpty(nextApprovalUser.getOid())&&(nodeUsers.isEmpty() || !nextApprovalUser.getOid().equals(nodeUsers.get(0).getoId()))) 
			nodeUsers.add(0,new ApprovalNodeUser(nextApprovalUser));
		//不存在指定流转人 则找到第一个推荐审批人作为首个待流转节点
		if(StringUtils.isEmpty(transferApproverOId)&&nodeUsers!=null&&!nodeUsers.isEmpty()){
			UserIdentity nextUser = approvalUserSevice.getUserInfo(approvalApply.getApprover().getNetworkId(), nodeUsers.get(0).getoId());
			if(nextUser==null) return msg.setMsg("转审批人不在职！！！");
			nextApprovalUser.copy(nextUser);
		}
		//设置流转节点记录
		nextApprovalRecord.initData(approvalApply.getId(),approvalApply.getTitle(),approvalApply.getApplyTitle(),
				null,approvalApply.getFormTemplateId(),AppovalRecordStatus.WAIT.getContext(),null,
				null,approvalApply.getApprover(),nextApprovalUser,new Date());
		return msg.setFlag(true).setCode(LightappConstants.META_CODE_GET).setMsg("操作成功");
	}
	
	/**
	 * 获得表单组件中可比对的值
	 * @param uId
	 * @param formDATA
	 * @return
	 */
	private String getUIValue(String uId,JSONArray formDATA){
		if(StringUtils.isEmpty(uId)) return null;
		if(formDATA==null||formDATA.isEmpty()) return null;
		for (Object ui : formDATA) {
			JSONObject uiDATA = JSONObject.fromObject(ui);
			if(uiDATA==null||uiDATA.isEmpty()) continue;
			if(!uiDATA.containsKey("type")) continue;
			if(!uiDATA.containsKey("value")) continue;
			if(!uiDATA.containsKey("require")) continue;
			if(!"textfield".equalsIgnoreCase(uiDATA.getString("type"))&&
			!"radiofield".equalsIgnoreCase(uiDATA.getString("type"))&&
			!"moneyfield".equalsIgnoreCase(uiDATA.getString("type"))&&
			!"numberfield".equalsIgnoreCase(uiDATA.getString("type"))&&
			!"textareafield".equalsIgnoreCase(uiDATA.getString("type"))&&uiDATA.getBoolean("require")!=true)  continue;
			if(uId.equalsIgnoreCase(uiDATA.getString("id"))) return uiDATA.containsKey("value")?uiDATA.getString("value"):null;
		}
		return null;
	}
	
	/**
	 * 条件分析匹配
	 * @param formDATA
	 * @param cnds
	 * @return
	 */
	private String analyseCnd(ApprovalApply approvalApply,JSONArray formDATA,List<ApprovalCndGroup> groups) {
		if(formDATA==null||formDATA.isEmpty()||groups==null||groups.isEmpty()) return null;
		boolean isPass=false;
		String uId = null;
		String config= null;
		JSONObject cndDATA = null;
		String compValue = null;
		int compareNum=0;
		for (ApprovalCndGroup group : groups) {
			if(group.getCnds()==null||group.getCnds().isEmpty()) continue;
			for (ApprovalCnd cnd : group.getCnds()) {
				isPass = false;
				uId = null;
				compValue = null;
				compareNum=0;
				config = cnd.getCndContext();
				if(StringUtils.isEmpty(config)||cnd.getStatus()!=1) continue;
				uId = cnd.getUid();
				cndDATA = JSONObject.fromObject(config);
				try {
					if(cndDATA.containsKey("eq")){
						compValue = cndDATA.getString("eq");
						isPass=compareValue(compValue, "eq", uId, formDATA);
						if(!isPass) break;
						compareNum++;
					}
					if(cndDATA.containsKey("lt")){
						compValue = cndDATA.getString("lt");
						isPass=compareValue(compValue, "lt", uId, formDATA);
						if(!isPass) break;
						compareNum++;
					}
					if(cndDATA.containsKey("lte")){
						compValue = cndDATA.getString("lte");
						isPass=compareValue(compValue, "lte", uId, formDATA);
						if(!isPass) break;
						compareNum++;
					}
					if(cndDATA.containsKey("gt")){
						compValue = cndDATA.getString("gt");
						isPass=compareValue(compValue, "gt", uId, formDATA);
						if(!isPass) break;
						compareNum++;
					}
					if(cndDATA.containsKey("gte")){
						compValue = cndDATA.getString("gte");
						isPass=compareValue(compValue, "gte", uId, formDATA);
						if(!isPass) break;
						compareNum++;
					}
				} catch (Exception e) {
					isPass=false;
					break;
				}
				if(!isPass||compareNum==0){
					isPass=false;
					break;
				}
			}
			if(isPass==true){
				approvalApply.setApprovalCndGroup(group.getId());
				return group.getUsersContext();
			}
		}
		return null;
	}
	
	/**
	 * 表单组件值比较
	 * @param compValue
	 * @param key
	 * @param uId
	 * @param formDATA
	 * @return
	 */
	private boolean compareValue(String compValue,String key,String uId,JSONArray formDATA) {
		if(!StringUtils.isEmpty(compValue)){
			String uiValue = getUIValue(uId, formDATA);
			if(StringUtils.isEmpty(uiValue))  return false;
			if("eq".equalsIgnoreCase(key)&&!uiValue.equalsIgnoreCase(compValue)){
				return false;
			}else if("eq".equalsIgnoreCase(key)&&uiValue.equalsIgnoreCase(compValue)){
				return true;
			}else {
				double uiNumber = Double.valueOf(uiValue);
				double compNumber = Double.valueOf(compValue);
				if("lt".equalsIgnoreCase(key)&&uiNumber>=compNumber){
					return false;
				}else if("lte".equalsIgnoreCase(key)&&uiNumber>compNumber){
					return false;
				}else if("gt".equalsIgnoreCase(key)&&uiNumber<=compNumber){
					return false;
				}else if("gte".equalsIgnoreCase(key)&&uiNumber<compNumber){
					return false;
				}
			}
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 *  获得审批单据id
	 * creator yongjun_su
	 * createdate 2016年7月21日
	 * modifyor 
	 * @param approvalApply
	 * @return
	 * @throws Exception
	 */
	private synchronized String generateReceiptId(ApprovalApply approvalApply) throws Exception{
		String code = null;
		String currentDay=new SimpleDateFormat("yyyyMMdd").format(new Date());
		String eid=approvalApply.getCreateUser().getEid();
		ParamsObject paramsObject=new ParamsObject();
		paramsObject.setUser(approvalApply.getCreateUser());
		paramsObject.setStartTimeStr(currentDay);
		Jedis jedis = jedisPool.getResource();
		try{
			//获取锁
			String value=jedis.get("FREEFLOW_FORMNO_LUCK_"+eid+currentDay);
			if(value==null&&StringUtils.isEmpty(value)){
				//锁为空进入，加锁
				jedis.getSet(eid+currentDay+"_approvalApply","FALSE");
				String sequence=jedis.get("FREEFLOW_FORMNO_"+eid+currentDay);
				ReceiptNo receiptNo = receiptNoDAO.findByCondition(paramsObject);
				if(!(receiptNo==null)){
					sequence=String.valueOf(receiptNo.getSequence());
				}
				if(sequence==null||StringUtils.isEmpty(sequence)){
					receiptNo=new ReceiptNo();
					receiptNo.seteId(approvalApply.getCreateUser().getEid());
					receiptNo.setNetworkId(approvalApply.getCreateUser().getNetworkId());
					receiptNo.setCurrentDay(paramsObject.getStartTimeStr());
					receiptNo.setSequence(1);
					receiptNoDAO.save(receiptNo);
					receiptNo = receiptNoDAO.findByCondition(paramsObject);
					code=receiptNo.geteId()+receiptNo.getCurrentDay()+receiptNo.getSequence();
					jedis.getSet("FREEFLOW_FORMNO_"+eid+currentDay,"1");//获取锁
				}else{
					 receiptNo.setSequence((receiptNo.getSequence()+1));
					 receiptNoDAO.update(receiptNo);
					 code=receiptNo.geteId()+receiptNo.getCurrentDay()+receiptNo.getSequence();
					 jedis.getSet("FREEFLOW_FORMNO_"+eid+currentDay,String.valueOf(receiptNo.getSequence()));
				}
				jedis.del("FREEFLOW_FORMNO_LUCK_"+eid+currentDay);
			}else{
				Thread.sleep(200);
				generateReceiptId(approvalApply);
			}
		} catch (Exception e){
			throw e;
		} finally{
			jedisPool.returnResource(jedis);
		}
		return code;
	}
}
