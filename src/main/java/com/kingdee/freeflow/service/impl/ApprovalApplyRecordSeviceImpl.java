package com.kingdee.freeflow.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.kingdee.cbos.common.utils.StringUtils;
import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.dao.ApprovalApplyDAO;
import com.kingdee.freeflow.dao.ApprovalApplyRecordDAO;
import com.kingdee.freeflow.dao.ApprovalNodeUserDAO;
import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.domain.ApprovalApplyRecord;
import com.kingdee.freeflow.domain.ApprovalNodeUser;
import com.kingdee.freeflow.domain.UserSimpleInfo;
import com.kingdee.freeflow.enums.AppovalRecordStatus;
import com.kingdee.freeflow.enums.AppovalStatus;
import com.kingdee.freeflow.service.ApprovalApplyRecordSevice;
import com.kingdee.freeflow.service.ApprovalDiscussGroupService;
import com.kingdee.freeflow.service.ApprovalMessageSevice;
import com.kingdee.freeflow.service.ApprovalUserSevice;
import com.kingdee.freeflow.service.impl.ApprovalUserSeviceImpl.ApprovalUserResult;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.service.RedisBaseService;
import com.kingdee.sns.lightapp.service.Tidings;
import com.kingdee.sns.lightapp.service.openorg.OpenorgService;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

@Service
@EnableAsync
public class ApprovalApplyRecordSeviceImpl extends RedisBaseService<ApprovalApplyRecord> implements ApprovalApplyRecordSevice{
	private final static Logger LOGGER = LoggerFactory.getLogger(ApprovalApplyRecordSeviceImpl.class);
	
	@Autowired
	private ApprovalApplyDAO approvalApplyDAO;
	
	@Autowired
	private ApprovalApplyRecordDAO approvalApplyRecordDAO;

	@Autowired
	private ApprovalNodeUserDAO approvalNodeUserDAO;
	
	@Autowired
	private ApprovalMessageSevice approvalMessageSevice;
	
	@Autowired
	private ApprovalUserSevice approvalUserSevice;
	
	@Autowired
	private ApprovalDiscussGroupService approvalDiscussGroupService;
	
	@Autowired
	private OpenorgService openorgService;
	
	@Override
	public long countBy(ParamsObject paramsObejct){
		try {
			if(paramsObejct==null) paramsObejct = new ParamsObject();
			paramsObejct.setQueryType("full");
			return approvalApplyRecordDAO.countBy(paramsObejct);
		} catch (Exception e) {
			LOGGER.error("===审批记录统计失败===count",e);
			return 0;
		}
	}

	@Override
	public List<ApprovalApplyRecord> findAll(ParamsObject paramsObejct, String sort, Integer start, Integer limit){
		try {
			if(paramsObejct==null) paramsObejct = new ParamsObject();
			paramsObejct.setQueryType("full");
			return approvalApplyRecordDAO.findAll(paramsObejct, sort, start, limit);
		} catch (Exception e) {
			LOGGER.error("===审批记录列表查询失败===findAll",e);
			return null;
		}
	}
	
	@Override
	public long countMyBy(ParamsObject paramsObejct){
		try {
			if(paramsObejct==null) paramsObejct = new ParamsObject();
			paramsObejct.setQueryType("myApprove");
			return approvalApplyRecordDAO.countBy(paramsObejct);
		} catch (Exception e) {
			LOGGER.error("===审批记录统计失败===count",e);
			return 0;
		}
	}
	
	@Override
	public List<ApprovalApplyRecord> findMy(ParamsObject paramsObejct, String sort, Integer start, Integer limit){
		try {
			if(paramsObejct==null) paramsObejct = new ParamsObject();
			paramsObejct.setQueryType("myApprove");
			return approvalApplyRecordDAO.findAll(paramsObejct, sort, start, limit);
		} catch (Exception e) {
			LOGGER.error("===审批记录列表查询失败===findAll",e);
			return null;
		}
	}
	
	@Override
	public List<ApprovalApplyRecord> findAllByApprovalId(String approvalId,boolean isDone,boolean isCreator){
		if(StringUtils.isEmpty(approvalId)) return null;
		try {
			return approvalApplyRecordDAO.findAllByApprovalId(approvalId,isDone,isCreator);
		} catch (Exception e) {
			LOGGER.error("===审批记录列表查询失败===findAllByApprovalId",e);
			return null;
		}
		
	}

	@Override
	public ApprovalApplyRecord findById(String id){
		if(StringUtils.isEmpty(id)) return null;
		try {
			return approvalApplyRecordDAO.findOneById(id);
		} catch (Exception e) {
			LOGGER.error("===审批记录详情查询失败===findById",e);
			return null;
		}
	}
	
	@Override
	public void save(ApprovalApplyRecord approvalRecord) throws Exception{
		if(approvalRecord==null) throw new Exception();
		try {
			approvalApplyRecordDAO.save(approvalRecord);
		} catch (Exception e) {
			LOGGER.error("===审批记录保存失败===save",e);
			throw e;
		}
	}
	
	/**
	 * 更新节点流转人操作状态
	 * @param record
	 * @throws Exception
	 */
	private void updateNodeUserFlag(ApprovalApplyRecord record) throws Exception {
		if(record==null) throw new Exception();
		try {
			ApprovalNodeUser nodeUser = approvalNodeUserDAO.getNearestApprovalNodeUserBy(record.getFormId(), record.getApprovalUser().getOid());
			if(nodeUser==null) return;
			nodeUser.setIsDone(true);
			approvalNodeUserDAO.save(nodeUser);
		} catch (Exception e) {
			LOGGER.error("===审批流转节点保存失败===updateNodeUserFlag",e);
			throw e;
		}
	}
	
	/**
	 * 指定新审批人插入流转人记录表
	 * @param record
	 * @throws Exception
	 */
	private void addFlowNodeUser(ApprovalApplyRecord nextRecord) throws Exception {
		if(nextRecord==null) return;
		try {
			//若转交人存在于流转人未操作记录中 不进行添加
			ApprovalNodeUser nearestNode = approvalNodeUserDAO.getNearestApprovalNodeUserBy(nextRecord.getFormId(), nextRecord.getApprovalUser().getOid());
			if(nearestNode!=null) return;
			//若转交人 不是下一个推荐审批人
			List<ApprovalNodeUser> nodeUsers = approvalNodeUserDAO.getNextApprovalNodeUserByApprovalId(nextRecord.getFormId());
			ApprovalNodeUser nodeUser = nodeUsers!=null&&!nodeUsers.isEmpty()?nodeUsers.get(0):null;
			int index = 0;
			if(nodeUser!=null&&!nextRecord.getApprovalUser().getPersonId().equals(nodeUser.getPersonId())) {
				ApprovalNodeUser doneUser = approvalNodeUserDAO.getLastApprovalNodeUserBy(nextRecord.getFormId());
				index  = doneUser.getNumber()+1;
				//位移之后的流转节点
				if(index == nodeUser.getNumber()) approvalNodeUserDAO.addAfterNumber(nextRecord.getFormId(),index);//若转交人 不是下一个推荐审批人，并且index与下一个审批节点重叠，往后的节点都需要+1更新
			}else{
				index = approvalNodeUserDAO.getMaxNumberByApprovalId(nextRecord.getFormId())+10000;
			}
			//插入流转环节
			ApprovalNodeUser user = new ApprovalNodeUser(nextRecord.getApprovalUser());
			user.setApprovalId(nextRecord.getFormId());
			user.setNumber(index);
			approvalNodeUserDAO.save(user);
		} catch (Exception e) {
			LOGGER.error("===审批流转节点保存失败===addFlowNodeUser",e);
			throw e;
		}
	}

	@Override
	public Tidings updateFlow(ApprovalApplyRecord record,UserIdentity user){
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		if(user==null||record==null||StringUtils.isEmpty(record.getId())) return msg;
		record.setApprovalUser(user);
		AppovalRecordStatus nowStatus = AppovalRecordStatus.getAppovalRecordStatus(record.getStatus());
		boolean create= nowStatus==AppovalRecordStatus.CREATE;
		boolean disAgree= nowStatus==AppovalRecordStatus.DISAGREE;
		boolean agreeEnd= nowStatus==AppovalRecordStatus.AGREE_END;
		boolean hang= nowStatus==AppovalRecordStatus.HANG;
		if(create) return msg.setFlag(false).setMsg("操作状态非法!").setCode(LightappConstants.META_CODE_NOTPASS);
		//若操作是终止操作 则不存在转审批人
		if((disAgree||agreeEnd||hang)&&!StringUtils.isEmpty(record.getTransferApproverOId())){
			return msg.setFlag(false).setMsg("终止操作,不允许指定下一个审批人!").setCode(LightappConstants.META_CODE_NOTPASS);
		}
		try {
			//待审批记录
			ApprovalApplyRecord detail = approvalApplyRecordDAO.findOneById(record.getId());
			if(detail==null) return msg;
			if(AppovalRecordStatus.WAIT!=AppovalRecordStatus.getAppovalRecordStatus(detail.getStatus())) {
				return msg.setFlag(false).setMsg("非法的流转环节!").setCode(LightappConstants.META_CODE_NOTPASS);
			}
			//审批申请
			ApprovalApply approvalApply = approvalApplyDAO.findOneById(detail.getFormId());
			if(approvalApply==null) return msg;
			//若非终止操作 则必须存在转审批人
			if(!(disAgree||agreeEnd||hang)&&StringUtils.isEmpty(record.getTransferApproverOId())){
				return msg.setFlag(false).setMsg("非终止操作,必须指定下一个审批人!").setCode(LightappConstants.META_CODE_NOTPASS);
			}
			
			//TODO:需要检验下个审批人(nextUser)是否设置部门负责人审批和存在部门负责审批人
			if(record.getDeptLevel() >= 1){
				Set<Object> parentSet = this.getParentDeptRoleApporver(approvalApply,record.getDeptLevel());
				if(parentSet != null && parentSet.isEmpty()) 
					return msg.setFlag(false).setMsg("非终止操作,必须指定下一个审批人!").setCode(LightappConstants.META_CODE_NODEPTAPPROVER);
			}
			
			//下一个审批人
			UserIdentity nextUser=approvalUserSevice.getUserInfo(user.getNetworkId(), record.getTransferApproverOId());
			
			//若存在下一个审批人id 但又找不到下一  个审批人 则验证不通过
			if(!StringUtils.isEmpty(record.getTransferApproverOId())&&nextUser==null){
				return msg.setFlag(false).setMsg("非法的下一个审批人！").setCode(LightappConstants.META_CODE_NOTACCEPT);
			}
			
			
			//验证当前人是否能操作
			boolean canDo = approvalUserSevice.checkCanDoRecord(approvalApply, record, user);
			if(!canDo) return msg.setFlag(false).setCode(LightappConstants.META_CODE_NOTPASS).setMsg("当前人员无权操作!");
			//下一个审批人 待处理记录 
			ApprovalApplyRecord nextRecord = null;
			if(nextUser!=null){
				nextRecord = new ApprovalApplyRecord(approvalApply.getId(),approvalApply.getTitle(),approvalApply.getApplyTitle(),
					null,approvalApply.getFormTemplateId(),AppovalRecordStatus.WAIT.getContext(),null,
					null,approvalApply.getCreateUser(),nextUser,new Date());
			}
			//保存当前操作记录
			detail.setApprovalTime(new Date());
			detail.setStatus(record.getStatus());
			detail.setComment(record.getComment());      
			if(nextUser!=null)  detail.setTransferApproverOId(nextUser.getOid());
			else detail.setTransferApproverOId(null);
			approvalApplyRecordDAO.save(detail);
			//如果当前操作人存在于ApprovalNodeUser中则将最近一个对应节点isDone属性更新
			updateNodeUserFlag(detail);
			if(nextRecord!=null){
				nextRecord.setStatus(AppovalRecordStatus.WAIT);
				nextRecord.setApprovalTime(null);
				//保存待处理操作记录
				approvalApplyRecordDAO.save(nextRecord);
				//更新流转节点人
				addFlowNodeUser(nextRecord);
	    		//指定下一个环节
				approvalApply.setNextRecordId(nextRecord.getId());
	    		approvalApply.setNextApprover(nextUser);
			}else{
				//指定下一个环节
				approvalApply.setNextRecordId(null);
	    		approvalApply.setNextApprover(null);
			}
			//设置审批人主表冗余信息
			if(StringUtils.isEmpty(approvalApply.getFirstApproverOId())) approvalApply.setFirstApproverOId(user.getOid());
			approvalApply.setLastApproverOId(user.getOid());
			//指定当前环节
			approvalApply.setRecordId(detail.getId());
			approvalApply.setApprover(user);
			//设置审批主表状态
			if(disAgree) approvalApply.setStatus(AppovalStatus.DISAGREE);
			if(agreeEnd) approvalApply.setStatus(AppovalStatus.AGREE);
			if(hang) approvalApply.setStatus(AppovalStatus.HANG);
			//审批人冗余信息（存储5个用于管理页展现）
			approvalApply.setApprovalUser(approvalUserSevice.approvalUserToJson(approvalApply.getApprovalUser(), user.getName(), detail.getStatus()));
			approvalApply.setLastUpdateTime(new Date());
			approvalApplyDAO.save(approvalApply);
			
			if(nextRecord !=null && nextUser!= null){
				approvalApply.setApprovalUser(approvalUserSevice.approvalUserToJson(approvalApply.getApprovalUser(), nextUser.getName(), AppovalRecordStatus.WAIT.getContext()));
				
				Set<String> personSet=this.getAllApprovalNodeUserList(approvalApply);
				//如果人数超过3人则创建组,如果已创建则将人拉入组
				if(StringUtils.isEmpty(approvalApply.getGroupId())){
					//personSet.add(nextUser.getPersonId());//添加下一个审批人的personId
					
					//建组
					//msg = approvalDiscussGroupService.createDiscussGroupExcludeStatus(approvalApply, user, personSet);
					Tidings groupMsg = approvalDiscussGroupService.createDiscussGroup(approvalApply, nextUser, personSet);
					
					LOGGER.info("-----groupMsg------"+groupMsg.toJson()+"------isCreatedGroup"+(boolean)groupMsg.get("isCreatedGroup")+"\n groupId"+(String)groupMsg.get("groupId"));
					
					if( groupMsg.isFlag() && (boolean)groupMsg.get("isCreatedGroup")){
						
					  //需要将groupId更新到approvalApply     
					  approvalApply.setGroupId((String)groupMsg.get("groupId"));
					  //建完组发送图文消息
					  approvalApplyDAO.save(approvalApply);

					  approvalDiscussGroupService.sendGroupMessage(approvalApply);
					  
					}else{//发送单人消息
						approvalDiscussGroupService.sendUserMessage(approvalApply, user.getUserId());//user.getUserId()当前操作人
					}
				}else{
					//拉人
					LOGGER.info("-----拉人------");
					//TODO 可以拉审批链路的所有人
					Tidings addGroupMsg = approvalDiscussGroupService.addDiscussGroupUser(approvalApply, personSet);
					/*if(addGroupMsg.isFlag()){
						//发送系统消息（会话组内）
						approvalMessageSevice.sendAddGroupUserSYSMessageFor(approvalApply, user);
					}*/
				}
				
				//存在下一个节点 发送待办消息
				approvalMessageSevice.sendTodoMessage(approvalApply.getId(),approvalApply.getNextRecordId(),nowStatus,approvalApply.getApprover(),approvalApply.getNextApprover());
			}else{
				//不存在下一个节点通知审批申请人
				approvalMessageSevice.sendPubaccMessage(detail.getId());
			}
			
			//发送系统消息（会话组内）
		    approvalMessageSevice.sendSYSMessageForApproval(approvalApply, user, AppovalRecordStatus.getAppovalRecordStatus(record.getStatus()));
			
			
			//处理自身待办消息
	    	approvalMessageSevice.doMessage(detail.getId(),detail.getApprovalUser().getOid(),1);
	    	List<ApprovalApplyRecord> list=findAllByApprovalId(approvalApply.getId(),false,true);
	    	msg.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_SAVE).put("approvalApplyRecord", detail).put("approvalApplyRecords", list);
		} catch (Exception e) {
			LOGGER.error("===审批流转失败===updateFlow",e);
			msg.setFlag(false).setCode(LightappConstants.META_CODE_ERROR).setMsg("审批流转操作异常!");
		}
		return msg;
	}

	@Override
	public Tidings getRecommendFlowUser(String id,UserIdentity user){
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_NOTACCEPT);
		if(StringUtils.isEmpty(id)) return msg;
		try {
			ApprovalApply approvalApply = approvalApplyDAO.findOneById(id);
			if(approvalApply==null||StringUtils.isEmpty(approvalApply.getNextRecordId())) return msg;
			ApprovalApplyRecord record = approvalApplyRecordDAO.findOneById(approvalApply.getNextRecordId());
			if(record==null||AppovalRecordStatus.WAIT!=AppovalRecordStatus.getAppovalRecordStatus(record.getStatus())) return msg;
			if(!record.getApprovalUser().getPersonId().equals(user.getPersonId())){
				return msg.setMsg("当前操作人非待审批人,无需获得推荐！").setFlag(true).setCode(LightappConstants.META_CODE_GET).setData(new HashMap<String,Object>());
			} 
			ApprovalNodeUser nodeUser = null;
			//从流转节点人信息表中获得推荐
			List<ApprovalNodeUser> nodeUsers = approvalNodeUserDAO.getNextApprovalNodeUserByApprovalId(approvalApply.getId());
			if(nodeUsers!=null&&!nodeUsers.isEmpty()){
				if(!record.getApprovalUser().getPersonId().equals(nodeUsers.get(0).getPersonId()))
					nodeUser = nodeUsers.get(0);
				else if(record.getApprovalUser().getPersonId().equals(nodeUsers.get(0).getPersonId())&&nodeUsers.size()>=2)
					nodeUser = nodeUsers.get(1);
			}
			//若获取不到推荐 则从审批规则中获得（兼容旧数据）
			if(nodeUser==null&&!StringUtils.isEmpty(approvalApply.getApprovalRule())){
				//获得规则人员
				ApprovalUserResult<UserSimpleInfo> reuslt = approvalUserSevice.getFlowUserByRule(approvalApply.getApprovalRule(), approvalApply.getCreateUser().getNetworkId());
				if(!reuslt.getFlag()) return msg.setMsg(reuslt.getMsg()).setCode(LightappConstants.META_CODE_NOTPASS);
				List<UserSimpleInfo> users = reuslt.getDatas();
				List<ApprovalApplyRecord> records = approvalApplyRecordDAO.findAllByApprovalId(approvalApply.getId(), true, false);
				boolean isHadRule = users!=null&&!users.isEmpty();
				boolean isHadRecord = records!=null&&!records.isEmpty();
				if(isHadRule&&!isHadRecord){
					if(!record.getApprovalUser().getPersonId().equals(users.get(0).getPersonId()))
						nodeUser = new ApprovalNodeUser(users.get(0));
					if(nodeUser==null&&users.size()>=2)
						nodeUser = new ApprovalNodeUser(users.get(1));
				}  else if (isHadRule&&isHadRecord){
					boolean isDone = false;
					records.add(0, record);
					for (UserSimpleInfo cuser : users) {
						isDone = false;
						for (ApprovalApplyRecord applyRecord : records) {
							if(applyRecord.getApprovalUser().getPersonId().equals(cuser.getPersonId())){//applyRecord:表示审批人对审批单的操作记录；
								isDone=true;//表示cuser当前对象已经对审批单操作过（同意|不同意|流转）
								break;
							}
						}
						if(!isDone){
							nodeUser = new ApprovalNodeUser(cuser);
							break;
						}
					}
				}
			}
			//若任然获取不到推荐 则从审批规则中获得（兼容旧数据）
			//if(nodeUser==null) nodeUser = approvalUserSevice.getNearestUsersByFlowId(approvalApply, user);
			msg.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_GET).put("recommendFlowUser", nodeUser);
		} catch (Exception e) {
			LOGGER.error("===审批推荐流转人查询失败===getRecommendFlowUser",e);
			msg.setFlag(false).setCode(LightappConstants.META_CODE_ERROR).setMsg("审批推荐流转人查询异常!");
		}
		return msg;
	}
	
	/**
	 * 获取审批单所有的审批人(去重)
	 * @param approvalApply
	 * @return
	 */
	private Set<String> getAllApprovalNodeUserList(ApprovalApply approvalApply){
		Set<String> personSet=new LinkedHashSet<String>();
		personSet.add(approvalApply.getCreateUser().getPersonId());//申请人的personId
		List<ApprovalNodeUser> nodeList =  approvalNodeUserDAO.getAllApprovalNodeUsersByApprovalId(approvalApply.getId(), 0, 0);
		if(null != nodeList && nodeList.size()>0){
			for(ApprovalNodeUser node:nodeList){
				personSet.add(node.getPersonId());
			}
		}
		
		
		List<ApprovalApplyRecord> recordList = approvalApplyRecordDAO.findAllWithoutStatus(approvalApply.getId());
		if(null == recordList || recordList.size()<1){
			return personSet;
		}
		for(ApprovalApplyRecord record:recordList){
			personSet.add(record.getApprovalUser().getPersonId());
		}
		
		//如果是老审批单要从record里面拿
		/*if(StringUtils.isEmpty(approvalApply.getVersion())||2.0 > Float.valueOf(approvalApply.getVersion())){
			List<ApprovalApplyRecord> recordList = approvalApplyRecordDAO.findAllWithoutStatus(approvalApply.getId());
			if(null == recordList || recordList.size()<1){
				return personSet;
			}
			for(ApprovalApplyRecord record:recordList){
				personSet.add(record.getApprovalUser().getPersonId());
			}
		}*/
		return personSet;
	}

	/**
	 * 获取部门推荐审批人
	 * @param formTemplateId：审批模板id
	 * @param user:当前操作用户
	 * @param deptLevel：部门级别(一级部门，二级部门)
	 * @return
	 */
	@Override
	public Tidings getRecommendDeptRoleUser(String formTemplateId,String approvalApplyId,
			UserIdentity user, int deptLevel)
	{
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_NOTACCEPT);
		//if(StringUtils.isEmpty(approvalApplyId)) return msg;
		
		//TODO:如果级别是1的，则根据审批申请单的创建人（user）找上级部门负责人
		//级别>2，则是根据上一个级别的部门负责人来找上级部门负责人
		Set<Object> personSet = new HashSet<>();
		if(deptLevel == 1){
			//String eid = "90001793",userId = "573acbf800b0a55e7b9a9a9a";
			//personSet = openorgService.getParentPersonSet(eid, userId);
			 personSet = openorgService.getParentPersonSet(user.getEid(), user.getOid());
		}else if(deptLevel >= 2){
			//TODO找到上一个已经审批过的部门负责人，获取该人的eid和openId，找到他所属的上级部门负责人
			ApprovalNodeUser approvalNodeUser = approvalNodeUserDAO.getLastApprovalNodeUserOfDeptRole(approvalApplyId, deptLevel - 1);//deptLevel - 1：找上一个级别的部门负责人
			
			if(approvalNodeUser != null){
				 personSet = openorgService.getParentPersonSet(approvalNodeUser.getEid(), approvalNodeUser.getoId());
			}
		}
		
		msg.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_GET).put("recommendDeptRoleUser", personSet);
		
		//TODO :如果存在多个则需要排序处理
		/*if(personSet != null && personSet.size() >= 2){    
			//TODO :按照拼音排序
		}*/
		
		return msg;
	}
	
	
	
	private Set<Object> getParentDeptRoleApporver(ApprovalApply approvalApply,int targetDeptLevel){
		Set<Object> personSet = new HashSet<>();
		//1.先获取approvalRule的content校验部门负责人审批设置
/*		if(approvalApply != null){
			//从rule获取审批人
			String rule = approvalApply.getApprovalRule();
			if(!StringUtils.isEmpty(rule)){
				JSONArray personArray =  JSONArray.fromObject(rule);
				
				if(personArray != null && !personArray.isEmpty() ){
					//找与user对应部门审批人openId
					for (int i = 0; i < personArray.size(); i++)
					{
						JSONObject obj = (JSONObject) personArray.get(i);
						if(obj != null && obj.containsKey("deptLevel")){
							int sourceDeptLevel = obj.getInt("deptLevel");
							
						}
					}
					
					
					if(personObj != null && personObj.containsKey("deptRoleType") && personObj.containsKey("deptLevel")){
						boolean isDeptRoleTypeFlag = personObj.getBoolean("deptRoleType");
						int deptLevel = personObj.getInt("deptLevel");
						
						//2.根据上一个人审批的人，找到他所属的上级部门负责人
						personSet = openorgService.getParentPersonSet(user.getEid(), user.getOpenId());
					}
				}
			}
		}*/
		
		
		ApprovalNodeUser approvalNodeUser = approvalNodeUserDAO.getLastApprovalNodeUserOfDeptRole(approvalApply.getId(), targetDeptLevel - 1);//deptLevel - 1：找上一个级别的部门负责人
		
		if(approvalNodeUser != null){
			 personSet = openorgService.getParentPersonSet(approvalNodeUser.getEid(), approvalNodeUser.getoId());
		}
		
		return personSet;
	}
	
	
	
	
}
