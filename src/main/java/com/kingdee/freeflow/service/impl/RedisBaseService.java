package com.kingdee.freeflow.service.impl;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;

import com.kingdee.cbos.common.utils.StringUtils;
import com.kingdee.cbos.core.framework.dal.impl.RedisBaseDAOImpl;

public class RedisBaseService<T extends Serializable> extends
		RedisBaseDAOImpl<T> {

	private Logger LOGGER = LoggerFactory.getLogger(RedisBaseService.class);

	/**
	 * @since 将Object信息存放redis中，默认有效期一天
	 * @param key
	 * @param value
	 */
	public void put(String key, T value, int seconds ,String j) {
		String json = toJson(value);
		Jedis jedis = this.jedisPool.getResource();
		if (seconds <= 0) {
			seconds = 24 * 60 * 60;
		}
		try {
			jedis.set(key, json);
			jedis.expire(key, seconds);
			this.jedisPool.returnResource(jedis);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			jedisPool.returnBrokenResource(jedis);
		}
	}

	/**
	 * @since 将Object信息存放redis中，默认有效期一天
	 * @param key
	 * @param value
	 */
	public void put(String key, T value) {
		String json = toJson(value);
		Jedis jedis = this.jedisPool.getResource();
		try {
			jedis.set(key, json);
			jedis.expire(key, 24 * 60 * 60);
			this.jedisPool.returnResource(jedis);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			jedisPool.returnBrokenResource(jedis);
		}
	}

	/**
	 * @since 从redis中删除key对应的值
	 * @param key
	 */
	public void delete(String key) {
		Jedis jedis = this.jedisPool.getResource();
		try {
			jedis.del(key);
			this.jedisPool.returnResource(jedis);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			jedisPool.returnBrokenResource(jedis);
		}
	}

	/**
	 * @since 获取key对应的值
	 * @param key
	 * @return
	 */
	public T get(String key) {
		Jedis jedis = this.jedisPool.getResource();
		T t = null;
		String value = null;
		try {
			value = jedis.get(key);
			this.jedisPool.returnResource(jedis);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			jedisPool.returnBrokenResource(jedis);
		}
		if (!StringUtils.isEmpty(value)) {
			t = fromJson(value);
		}
		return t;
	}

	/**
	 * @since 判断该key是否存在
	 * @param key
	 * @return
	 */
	public boolean exist(String key) {
		Jedis jedis = this.jedisPool.getResource();
		Boolean isExist = false;
		try {
			isExist = jedis.exists(key);
			this.jedisPool.returnResource(jedis);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			jedisPool.returnBrokenResource(jedis);
		}
		return isExist;
	}
}
