package com.kingdee.freeflow.service.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibm.icu.text.SimpleDateFormat;
import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.dao.ApprovalApplyRecordDAO;
import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.domain.ApprovalApplyRecord;
import com.kingdee.freeflow.service.ApprovalApplyRecordSevice;
import com.kingdee.freeflow.service.ApprovalApplySevice;
import com.kingdee.freeflow.service.ExcelExportService;
import com.kingdee.freeflow.utils.ExportExcelUtil;
import com.kingdee.freeflow.utils.JsonUtil;
@Service
public class ExcelExportServiceImpl implements ExcelExportService {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelExportServiceImpl.class);
	
	//public static String[] header = {"单号","类型","部门","内容","发起人","发起时间","审批人","完成时间"};
	public static String[] header = {"单号","类型","发起人","部门","内容","发起时间","审批人","完成时间"};
	public static String title = "审批数据";
	
	private static final int CellNum = 8;

	@Autowired
	private ApprovalApplySevice approvalApplySevice;
	
	@Autowired
	private ApprovalApplyRecordDAO approvalApplyRecordDAO;
	
	@Autowired
	private ApprovalApplyRecordSevice approvalApplyRecordSevice;
	
	@Override
	public HSSFWorkbook exportApproval(ParamsObject paramsObject) throws Exception {
		List<ApprovalApply> approvalApplyList = approvalApplySevice.findAll(paramsObject, "-createTime", 0, 0);
		if(null==approvalApplyList)return null;
		int p = approvalApplyList.size();
		
		LOGGER.info("-------需要导出的审批单据数量是--------"+p);
		
		String[][] dateset = new String[p][]; 
		for(int i = 0, l = p; i < l ;i++){
			ApprovalApply approvalApply = approvalApplyList.get(i);
			
			if(approvalApply != null){
				String[] row = new String[CellNum];
				row[0]=approvalApply.getReceiptId();
				row[1]=StringUtils.isBlank(approvalApply.getTitle())?"":approvalApply.getTitle() ;
				//row[2]=approvalApply.getCreateUser()==null?"":approvalApply.getCreateUser().getDeptName() ;
				row[2]=approvalApply.getCreateUser()==null?"":approvalApply.getCreateUser().getName();//发起人
				
				//row[3]= this.getApprovalApplyContent(approvalApply);
				row[3]= approvalApply.getCreateUser()==null?"":(approvalApply.getCreateUser().getDeptName() == null ? "":approvalApply.getCreateUser().getDeptName());//部门
				
				//row[4]=approvalApply.getCreateUser()==null?"":approvalApply.getCreateUser().getName();
				row[4]=this.getApprovalApplyContent(approvalApply);//内容
				
				row[5]=approvalApply.getCreateTime()==null?"":new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(approvalApply.getCreateTime());
				List<ApprovalApplyRecord> apApplyRecordsList = approvalApplyRecordSevice.findAllByApprovalId(approvalApply.getId(), false, true);;
				String approvalUsers="";
				if(null!=apApplyRecordsList){
					for (int j = 0; j < apApplyRecordsList.size(); j++)
					{
						ApprovalApplyRecord approvalApplyRecord=apApplyRecordsList.get(j);
						approvalUsers +=  approvalApplyRecord.getApprovalUser().getName()+">";
					}
					approvalUsers = approvalUsers.substring(0, approvalUsers.length()-1);
				}
				row[6] = approvalUsers;
				if("2".equals(approvalApply.getStatus())||"3".equals(approvalApply.getStatus())){
					row[7] = approvalApply.getLastUpdateTime()==null?"":new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(approvalApply.getLastUpdateTime());
				}else{
					row[7] = "未完成";
				}
				dateset[i]=row;
			}
		}
		
		HSSFWorkbook wb = ExportExcelUtil.getExportExcel(title, header, dateset);
		return wb;
	}
	
	private String getApprovalApplyContent(ApprovalApply approvalApply){
		String content = "";
		if(approvalApply.getIsThird()){//如果是第三方的话则显示title名称
			content = approvalApply.getTitle() + "\r\n";
			content += approvalApply.getTabloid();
			return content;
		}
		
		//content = approvalApply.getTitle() + "\r\n";//首先取审批单据的标题作为内容
		
		@SuppressWarnings("unchecked")
		Map<String,Object> map = JsonUtil.getMapFromJsonArray4ExportExcel("label", "value", approvalApply.getContent());//取出内容，以键值对形式显示
		if(map != null && !map.isEmpty()){
		   for (Iterator<String> it =  map.keySet().iterator();it.hasNext();)
		   {
			    Object key = it.next();
			    String value = (String) map.get(key);
			    
				content += key+":"+ value + "\r\n";
		   }
		}
		
		return content;
	}
	

}
