package com.kingdee.freeflow.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.kingdee.freeflow.common.constants.AppDisGroupConstants;
import com.kingdee.freeflow.dao.ApprovalApplyDAO;
import com.kingdee.freeflow.dao.ApprovalApplyRecordDAO;
import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.domain.ApprovalApplyRecord;
import com.kingdee.freeflow.enums.AppovalRecordStatus;
import com.kingdee.freeflow.service.ApprovalApplyRecordSevice;
import com.kingdee.freeflow.service.ApprovalMessageSevice;
import com.kingdee.freeflow.utils.JsonUtil;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.domain.pubacc.Message;
import com.kingdee.sns.lightapp.domain.xuntong.SYSMessage;
import com.kingdee.sns.lightapp.service.configure.LightappConfigureService;
import com.kingdee.sns.lightapp.service.pubacc.PubaccService;
import com.kingdee.sns.lightapp.service.xuntong.LAPPXunTongService;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

@Service
@EnableAsync
public class ApprovalMessageSeviceImpl implements ApprovalMessageSevice
{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ApprovalMessageSeviceImpl.class);
	
	private static final String APPCODE = "freeflow";
	@Autowired
	private LightappConfigureService lightappConfigureService;
	@Autowired
	private PubaccService pubaccService;
	@Autowired
	private ApprovalApplyRecordDAO approvalApplyRecordDAO;
	@Autowired
	private ApprovalApplyDAO approvalApplyDAO;
	@Autowired
	private ApprovalApplyRecordSevice approvalApplyRecordSevice;
	@Autowired
	private LightappConfigureService lightAppConfigureService;
	@Autowired
	private LAPPXunTongService lappXunTongService;
	
	@Async
	@Override
	public void sendTodoMessage(final String approvalId,final String nextRecordId,final AppovalRecordStatus type,final UserIdentity form,final UserIdentity to) {
		if (StringUtils.isBlank(approvalId)||StringUtils.isBlank(nextRecordId)||type==null|| form == null|| to==null) return;
		ApprovalApply approvalApply = approvalApplyDAO.find(approvalId);
		if (approvalApply == null) return;
		String appId = lightappConfigureService.getAppFreeflowId();
		String pubaccId = lightappConfigureService.getPubaccFreeflowId();
		String pubaccKey = lightappConfigureService.getPubaccFreeflowKey();
		String url = lightappConfigureService.getYZJOutHost() + "/freeflow/m/approval/" + approvalApply.getId() + "?lappName=freeflow";
		List<String> oIds = new ArrayList<String>();
		AppovalRecordStatus opt = type;
		
		if(opt!=AppovalRecordStatus.CREATE&&opt!=AppovalRecordStatus.AGREE) return;
		String keyWords = "";
		if(opt==AppovalRecordStatus.CREATE) keyWords="提交";
		if(opt==AppovalRecordStatus.AGREE) keyWords="同意";
		String msg = form.getName() + keyWords+"了【"+approvalApply.getCreateUser().getName()+"的"+this.getTitleForMsg(approvalApply)+"】，请你审批!";
		oIds.add(to.getOid());
		Message message = new Message(msg, url, appId, 1);
		
		LOGGER.info("=====审批待办推送=====start");
		LOGGER.info("=====审批待办推送=====id"+nextRecordId);
		LOGGER.info("=====审批待办推送=====url:"+url);
		LOGGER.info("=====审批待办推送=====msg:"+msg);
		LOGGER.info("=====审批待办推送=====Eid:"+approvalApply.getCreateUser().getEid()+" formName:"+form.getName()+" formOid:"+form.getOid()+" toName:"+to.getName()+" toOid:"+to.getOid());
		pubaccService.sendPubaccMsgByOpenIds(nextRecordId, 1,oIds, approvalApply.getCreateUser().getEid(), pubaccId, pubaccKey, message, APPCODE);
		LOGGER.info("=====审批待办推送=====End");
	}
	
	@Async
	@Override
	public void sendPubaccMessage(final String approvalRecordId){
		if (StringUtils.isBlank(approvalRecordId)) return;
		ApprovalApplyRecord approvalApplyRecord = approvalApplyRecordDAO.find(approvalRecordId);
		if (approvalApplyRecord == null) return;
		ApprovalApply approvalApply = approvalApplyDAO.find(approvalApplyRecord.getFormId());
		if (approvalApply == null) return;
		String appId = lightappConfigureService.getAppFreeflowId();
		String pubaccId = lightappConfigureService.getPubaccFreeflowId();
		String pubaccKey = lightappConfigureService.getPubaccFreeflowKey();
		String url = lightappConfigureService.getYZJOutHost()
				+ "/freeflow/m/approval/" + approvalApply.getId()
				+ "?lappName=freeflow";
		List<String> oIds = new ArrayList<String>();
		AppovalRecordStatus opt=AppovalRecordStatus.getAppovalRecordStatus(	approvalApplyRecord.getStatus());
		if(opt==AppovalRecordStatus.CREATE) return;
		String keyWords = "";
		if(opt==AppovalRecordStatus.AGREE) keyWords="同意";
		if(opt==AppovalRecordStatus.DISAGREE) keyWords="不同意并完成";
		if(opt==AppovalRecordStatus.AGREE_END) keyWords="同意并完成";
		if(opt==AppovalRecordStatus.CLOSE) keyWords="关闭";
		if(opt==AppovalRecordStatus.HANG) keyWords="挂起";
		String msg = approvalApplyRecord.getApprovalUser().getName() 
				+ keyWords+"了你的【"+this.getTitleForMsg(approvalApply)+"】,请知悉";
		Message message = new Message(msg, url, appId, 0);
		oIds.add(approvalApply.getCreateUser().getOid());
		pubaccService.sendPubaccMsgByOpenIds(approvalApplyRecord.getId(), 0,
				oIds, approvalApply.getCreateUser().getEid(), pubaccId,
				pubaccKey, message, APPCODE);
	}

	@Async
	@Override
	public void doMessage(final String id,final String oId,final int type){
		if (StringUtils.isBlank(id) || StringUtils.isBlank(oId)) return;
		List<String> oIds = new ArrayList<String>();
		oIds.add(oId);
		doMessage(id, oIds, type);
	}

	@Async
	@Override
	public void doMessage(final String approvalRecordId,final List<String> oIds,final int type){
		String appId = lightappConfigureService.getAppFreeflowId();
		String appSecret = lightappConfigureService.getAppFreeflowSecret();
		pubaccService.dealTodo(approvalRecordId, oIds, 1, type, 0, appId, appSecret);
	}

	@Async
	@Override
	public void sendMessageForUrge(final String approvalRecordId,final List<String> oIds){
		if (StringUtils.isBlank(approvalRecordId) || oIds.isEmpty()) return;
		ApprovalApplyRecord approvalApplyRecord = approvalApplyRecordDAO.find(approvalRecordId);
		if (approvalApplyRecord == null) return;
		ApprovalApply approvalApply = approvalApplyDAO.find(approvalApplyRecord.getFormId());
		if (approvalApply == null) return;
		String appId = lightappConfigureService.getAppFreeflowId();
		String pubaccId = lightappConfigureService.getPubaccFreeflowId();
		String pubaccKey = lightappConfigureService.getPubaccFreeflowKey();
		String url = lightappConfigureService.getYZJOutHost()
				+ "/freeflow/m/approval/" + approvalApply.getId()
				+ "?lappName=freeflow";
		Message message = new Message(approvalApplyRecord.getFormCreator().getName() + "提醒你尽快审批【"+this.getTitleForMsg(approvalApply)+"】", url, appId, 1);
		LOGGER.info("=====审批催办推送=====start");
		LOGGER.info("=====审批催办推送=====id"+approvalApplyRecord.getId());
		LOGGER.info("=====审批催办推送=====url:"+url);
		pubaccService.sendPubaccMsgByOpenIds(approvalApplyRecord.getId(), 1,
				oIds, approvalApplyRecord.getApprovalUser().getEid(), pubaccId,
				pubaccKey, message, APPCODE);
		LOGGER.info("=====审批催办推送=====End");
	}

	/**
	 * 发送讨论组同意消息
	 */
	@Async
	@Override
	public void sendSYSMessageForApproval(final ApprovalApply approvalApply,final UserIdentity user,final AppovalRecordStatus appovalRecordStatus){
		if(approvalApply!=null&&StringUtils.isEmpty(approvalApply.getGroupId())) return;
		SYSMessage msg = new SYSMessage();
		String appId = lightappConfigureService.getAppFreeflowId();
		msg.setAppId(appId);
		msg.setColor(AppDisGroupConstants.SYS_MSG_COLOR);
		msg.setGroupId(approvalApply.getGroupId());
		String highLightTitle = this.getTitleForMsg(approvalApply);
		msg.setKeyword(highLightTitle);//关键字做高亮显示approvalApply.getTitle()
		JSONObject dataJson = new JSONObject();
		if (appovalRecordStatus == AppovalRecordStatus.CREATE){
			dataJson.put("fromUser", user.getPersonId());// 当前操作人
			dataJson.put("user1", approvalApply.getNextApprover().getPersonId());
			//如果有标题内容则显示，否则显示approvalApply的title
			approvalApply.getContent();
			msg.setContent(AppDisGroupConstants.SYS_MSG_CONTENT_1.replace(AppDisGroupConstants.FORM_TEMPLATE_TYPE, highLightTitle));
			
		} else if (appovalRecordStatus == AppovalRecordStatus.AGREE){//同意
			dataJson.put("fromUser", user.getPersonId());// 当前操作人
			dataJson.put("user1", approvalApply.getCreateUser().getPersonId());//申请人的personId
			//dataJson.put("user2", approvalApply.getNextApprover().getPersonId());//下一个审批人
			msg.setContent(AppDisGroupConstants.SYS_MSG_CONTENT_2.replace(AppDisGroupConstants.FORM_TEMPLATE_TYPE, highLightTitle));
		} else if (appovalRecordStatus == AppovalRecordStatus.DISAGREE) {//不同意
			dataJson.put("fromUser", user.getPersonId());// 当前操作人
			dataJson.put("user1", approvalApply.getCreateUser().getPersonId());
			msg.setContent(AppDisGroupConstants.SYS_MSG_CONTENT_3.replace(AppDisGroupConstants.FORM_TEMPLATE_TYPE, highLightTitle));
		} else if (appovalRecordStatus == AppovalRecordStatus.AGREE_END) {//最终审批通过
			dataJson.put("fromUser", user.getPersonId());// 当前操作人
			dataJson.put("user1", approvalApply.getCreateUser().getPersonId());
			msg.setContent(AppDisGroupConstants.SYS_MSG_CONTENT_4.replace(AppDisGroupConstants.FORM_TEMPLATE_TYPE, highLightTitle));
		}
		String forwardUrl = lightAppConfigureService.getYZJOutHost() + "/freeflow/m/approval/" + approvalApply.getId() + "?lappName=freeflow";//设置申请详情单url
		msg.setUrl(forwardUrl);
		msg.setUsers(dataJson);
		msg.setSendType("ALL");//群组所有人能看见消息
		
		lappXunTongService.sendSysMsg(msg);
	}
	
	/**
	 * 发送评论消息
	 * @param approvalApply
	 * @param user
	 */
	@Async
	@Override
	public void sendSYSMessageForComment(final ApprovalApply approvalApply,final UserIdentity user) {
		if(approvalApply!=null&&StringUtils.isEmpty(approvalApply.getGroupId())) return;
		SYSMessage msg = new SYSMessage();
		JSONObject dataJson = new JSONObject();
		String appId = lightappConfigureService.getAppFreeflowId();
		msg.setAppId(appId);
		msg.setColor(AppDisGroupConstants.SYS_MSG_COLOR);
		msg.setGroupId(approvalApply.getGroupId());
		dataJson.put("fromUser", user.getPersonId());// 当前操作人
		dataJson.put("user1", approvalApply.getCreateUser().getPersonId());
		String highComtTitle = this.getTitleForMsg(approvalApply);
		msg.setContent(AppDisGroupConstants.SYS_MSG_PUBLISH_COMMENT.replace(AppDisGroupConstants.FORM_TEMPLATE_TYPE, highComtTitle));
		msg.setKeyword(highComtTitle);//表单title做高亮显示approvalApply.getTitle()
		String forwardUrl = lightAppConfigureService.getYZJOutHost() + "/freeflow/m/approval/" + approvalApply.getId() + "?lappName=freeflow";//设置申请详情单url
		msg.setUrl(forwardUrl);
		msg.setUsers(dataJson);
		msg.setSendType("ALL");//群组所有人能看见消息
		lappXunTongService.sendSysMsg(msg);
	}
	
	/**
	 * 如果有审批申请单有标题则优先显示title，否则显示申请类型
	 * @param sourceContent
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String getTitleForMsg(ApprovalApply approvalApply){
		String highComtTitle = "";
		try
		{
			if(approvalApply != null ){
				highComtTitle = approvalApply.getTitle();
				if(approvalApply.getIsThird()==true) return highComtTitle;
				Map<String,Object> map = JsonUtil.getMapFromJsonArray("label", "value", approvalApply.getContent());
				if(map != null && !map.isEmpty()){
					for (Iterator<String> it =  map.keySet().iterator();it.hasNext();)
					{
						Object key = it.next();
						if("标题".equalsIgnoreCase((String) key)){
							highComtTitle = (String) map.get(key);
							break;
						}
					}
				}
			}
		} catch (Exception e)
		{
			LOGGER.error("getTitleForMsg unsuccessfully:---->:"+e.getMessage());
		}
		
		return highComtTitle;
		
	}
	
	@Async
	@Override
	public void sendAddGroupUserSYSMessageFor(final ApprovalApply approvalApply,final UserIdentity user){
		if(approvalApply!=null&&StringUtils.isEmpty(approvalApply.getGroupId())) return;
		
		//如果当前操作人和下一个审批人是的personId一致(意味是同一个人的时候)，则不发审批提示消息
		if(!StringUtils.isEmpty(user.getPersonId()) && user.getPersonId().equalsIgnoreCase(approvalApply.getNextApprover().getPersonId())) return;
		
		SYSMessage msg = new SYSMessage();
		String appId = lightappConfigureService.getAppFreeflowId();
		msg.setAppId(appId);
		msg.setColor(AppDisGroupConstants.SYS_MSG_COLOR);
		msg.setGroupId(approvalApply.getGroupId());
		String highLightTitle = "";
		if(approvalApply != null ){
			highLightTitle = approvalApply.getCreateUser().getName()+"的"+approvalApply.getTitle();
		}
		msg.setKeyword(highLightTitle);//关键字做高亮显示approvalApply.getTitle()
		JSONObject dataJson = new JSONObject();
		dataJson.put("fromUser", user.getPersonId());// 当前操作人
		dataJson.put("user1", approvalApply.getNextApprover().getPersonId());
		msg.setContent(AppDisGroupConstants.SYS_MSG_CONTENT_5.replace(AppDisGroupConstants.FORM_TEMPLATE_TYPE, highLightTitle));
			
		String forwardUrl = lightAppConfigureService.getYZJOutHost() + "/freeflow/m/approval/" + approvalApply.getId() + "?lappName=freeflow";//设置申请详情单url
		msg.setUrl(forwardUrl);
		msg.setUsers(dataJson);
		msg.setSendType("ALL");//群组所有人能看见消息
		LOGGER.info("=========sendAddGroupUserSYSMessageFor========msg："+JSONObject.fromObject(msg).toString());
		lappXunTongService.sendSysMsg(msg);
	}
}