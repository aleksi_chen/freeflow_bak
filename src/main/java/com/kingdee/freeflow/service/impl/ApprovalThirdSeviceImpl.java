package com.kingdee.freeflow.service.impl;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.kingdee.cbos.common.utils.StringUtils;
import com.kingdee.freeflow.dao.ApprovalApplyDAO;
import com.kingdee.freeflow.dao.ApprovalApplyRecordDAO;
import com.kingdee.freeflow.dao.ApproveNotifyThridDAO;
import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.domain.ApprovalApplyRecord;
import com.kingdee.freeflow.domain.ApprovalNodeUser;
import com.kingdee.freeflow.domain.ApprovalRule;
import com.kingdee.freeflow.domain.ApproveApplyThrid;
import com.kingdee.freeflow.domain.ApproveNotifyThrid;
import com.kingdee.freeflow.domain.ApproveRecordThird;
import com.kingdee.freeflow.domain.ApproveUserThrid;
import com.kingdee.freeflow.enums.AppovalRecordStatus;
import com.kingdee.freeflow.enums.AppovalStatus;
import com.kingdee.freeflow.enums.ApprovalNotifyStatus;
import com.kingdee.freeflow.enums.ApprovalNotifyType;
import com.kingdee.freeflow.enums.ApprovalType;
import com.kingdee.freeflow.service.ApprovalApplySevice;
import com.kingdee.freeflow.service.ApprovalThirdSevice;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.domain.UserNetwork;
import com.kingdee.sns.lightapp.domain.comment.BizComment;
import com.kingdee.sns.lightapp.service.Tidings;
import com.kingdee.sns.lightapp.service.UserNetworkService;
import com.kingdee.sns.lightapp.service.comment.BizCommentService;
import com.kingdee.sns.lightapp.util.DateJsoLongProcessor;
import com.kingdee.sns.lightapp.util.EMPTool;
import com.kingdee.sns.lightapp.util.HttpHelper;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

@Service
@EnableAsync
public class ApprovalThirdSeviceImpl implements ApprovalThirdSevice {
	private final static Logger LOGGER = LoggerFactory.getLogger(ApprovalThirdSeviceImpl.class);

	@Autowired
	private ApprovalApplyDAO approvalApplyDAO;
	
	@Autowired
	private ApprovalApplyRecordDAO approvalApplyRecordDAO;
	
	@Autowired
	private ApproveNotifyThridDAO approveNotifyThridDAO;
	
	@Autowired
	private ApprovalApplySevice approvalApplySevice;
	
	@Autowired
	private UserNetworkService userNetworkService;
	
	@Autowired
	private BizCommentService bizCommentService;
	
	private final static String APPLY_KEY="apply";
	
	private final static String RECORDS_KEY="records";
	
	@Override
	@SuppressWarnings("unchecked")
	public Tidings saveFlow(ApproveApplyThrid apply, UserIdentity user) {
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_NOTACCEPT);
		if(apply==null||user==null||StringUtils.isEmpty(apply.getTitle())||StringUtils.isEmpty(apply.getTabloid())) return msg;
		if(StringUtils.isEmpty(apply.getId())){
			//数据初始化
			ApprovalApply approvalApply = new ApprovalApply();
			approvalApply.setTitle(apply.getTitle());
			approvalApply.setTabloid(apply.getTabloid());
			approvalApply.setBizzId(apply.getBizzId());
			approvalApply.setDetailURL(apply.getDetailURL());
			approvalApply.setNotifyURL(apply.getNotifyURL());
			approvalApply.setIsThird(true);
			approvalApply.setApprovalType(apply.getApproveType());
			approvalApply.setCreateTime(new Date());
			approvalApply.setLastUpdateTime(new Date());
			approvalApply.setStatus(AppovalStatus.DOING);
			approvalApply.setApprover(user);
			approvalApply.setCreateUser(user);
			approvalApply.setVersion("2.0");
			//数据初始化
			ApprovalRule rule = new ApprovalRule();
			rule.setCreateTime(new Date()); 
			if(ApprovalType.getApprovalType(apply.getApproveType())==null){
				rule.setApprovalType(ApprovalType.FREE.getContext());
			}else if(ApprovalType.getApprovalType(apply.getApproveType())==ApprovalType.FREE){
				rule.setApprovalType(ApprovalType.FREE.getContext());
			}else if(!StringUtils.isEmpty(apply.getApproveKey())){
				rule.setApprovalType(ApprovalType.NORMAL.getContext());
				rule.setContent(apply.getApproveKey());
			}
			//自定义审批人
			List<ApprovalNodeUser> nodeUsers = !StringUtils.isEmpty(apply.getApproveFree())?JSONArray.toList(JSONArray.fromObject(apply.getApproveFree()),ApprovalNodeUser.class):null;
			//审批申请创建
			msg = approvalApplySevice.saveFlowForThrid(approvalApply, rule, user, nodeUsers, apply.getTransferApproverOId());
		}else{
			ApprovalApply approvalApply=approvalApplySevice.findById(apply.getId());
			if(approvalApply!=null){
				JSONObject app=new JSONObject();
				app.put("id", approvalApply.getId());
				app.put("receiptId", approvalApply.getReceiptId());
				app.put("rejectId", approvalApply.getRejectId());
				msg.setFlag(true).setCode(LightappConstants.META_CODE_SAVE).setMsg("操作成功").put(APPLY_KEY, app);
			}else{
				msg.setFlag(false).setCode(LightappConstants.META_CODE_SAVE).setMsg("参数校验不通过，非法的请求！");
			}
		}
		return msg;
	}

	@Async
	@Override
	public void notifyFlow(final String applyId) {
		if(StringUtils.isEmpty(applyId)) {
			LOGGER.error("===审批第三方通知失败 参数非法===applyId："+applyId);
			return;
		}
		try {
			ApprovalApply approvalApply = approvalApplySevice.findById(applyId);
			if(approvalApply==null||StringUtils.isEmpty(approvalApply.getNotifyURL())) {
				LOGGER.error("===审批第三方通知失败 参数非法===applyId："+applyId);
				return;
			}
			String rid = approvalApply.getRecordId();
			String nrid = approvalApply.getNextRecordId();
			ApprovalApplyRecord record = approvalApplyRecordDAO.findOneById(rid);
			ApprovalApplyRecord nRecord = approvalApplyRecordDAO.findOneById(nrid);
			boolean isEmptyData = record==null;
			if(isEmptyData){
				LOGGER.error("===审批第三方通知失败 筛选数据错误===applyId："+applyId);
				throw new Exception("根据applyId："+applyId+" 筛选数据错误！");
			}
			sendNotify(approvalApply.getNotifyURL(),notifyNormal(approvalApply,record));//正常流转状态变更通知
			sendNotify(approvalApply.getNotifyURL(),notifyWarning(approvalApply,record,nRecord));//判断是否能进行正常流转 若异常则进行通知发送
		} catch (Exception e) {
			LOGGER.error("===审批第三方通知失败===applyId："+applyId, e);
		}
	}

	@Override
	public Tidings getDetails(String aid,UserIdentity user){
		Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
		try{
		  //TODO 审批表单信息
		  ApprovalApply approvalApply=approvalApplyDAO.find(aid);
		  if(null == approvalApply){
			  tiding.setFlag(true).setMsg("未找到审批！").setCode(LightappConstants.META_CODE_ERROR);
			  return tiding;
		  }
		  ApproveApplyThrid applyThrid = new ApproveApplyThrid();
		  applyThrid.setId(approvalApply.getId());
		  applyThrid.setTitle(approvalApply.getTitle());
		  applyThrid.setCreateTime(approvalApply.getCreateTime());
		  applyThrid.setLastUpdateTime(approvalApply.getLastUpdateTime());
		  applyThrid.setStatus(approvalApply.getStatus());
		  applyThrid.setReceiptId(approvalApply.getReceiptId());
		  applyThrid.setRejectId(approvalApply.getRejectId());
		  applyThrid.setDetailURL(approvalApply.getDetailURL());
		  applyThrid.setNotifyURL(approvalApply.getNotifyURL());
		  //审批申请人信息
		  ApproveUserThrid userThrid = new ApproveUserThrid();
		  userThrid.setEid(approvalApply.getCreateUser().getEid());
		  userThrid.setOid(approvalApply.getCreateUser().getOid());
		  applyThrid.setCreator(userThrid);
		  
		  //审批节点信息
		  List<ApprovalApplyRecord> recordList = approvalApplyRecordDAO.findAllByApprovalId(approvalApply.getId(), false, true);
		  if(null == recordList || recordList.isEmpty()){
			  tiding.setFlag(true).setMsg("未找到审批节点！").setCode(LightappConstants.META_CODE_ERROR);
			  return tiding;
		  }
		  List<ApproveRecordThird> recordThird= new ArrayList<ApproveRecordThird>();
		  ApproveRecordThird rThird=null;
		  ApproveUserThrid approver=null;
		  for(ApprovalApplyRecord record:recordList){
			  rThird = new ApproveRecordThird();
			  rThird.setId(record.getId());
			  rThird.setApplyId(record.getFormId());
			  rThird.setApprovalTime(record.getApprovalTime());
			  rThird.setCreateTime(record.getCreateTime());
			  rThird.setStatus(record.getStatus());
			  approver = new ApproveUserThrid();
			  approver.setEid(record.getApprovalUser().getEid());
			  approver.setOid(record.getApprovalUser().getOid());
			  rThird.setApprover(approver);
			  rThird.setTransferApproverOId(record.getTransferApproverOId());
			  rThird.setComment(record.getComment());
			  recordThird.add(rThird);
		  }
		  //获取评论
		  Map<String,String> map= new HashMap<String,String>();
		  map.put("bizId", approvalApply.getId());
		  map.put("bizType", approvalApply.getApprovalType());
		  map.put("orderType", "createTime");
		  List<BizComment> commentList = bizCommentService.getComments(JSONObject.fromObject(map), user);
		  tiding.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_GET);
		  tiding.put("apply", applyThrid).put(RECORDS_KEY, recordThird).put("detailURL", applyThrid.getDetailURL()).put("notifyURL", applyThrid.getNotifyURL()).put("comments", commentList);
		}catch(Exception e){
		  LOGGER.error("===审批查询错误===/detail/approvalApply",e);
		  tiding.setFlag(false).setMsg("操作失败,执行出现异常").setCode(LightappConstants.META_CODE_ERROR);
		}
		return tiding;
	}
	
	@SuppressWarnings("unchecked")
	private void sendNotify(String url,ApproveNotifyThrid notify){
		if(notify==null||StringUtils.isEmpty(url)) return;
		long timestamp=System.currentTimeMillis();
		Random rand = new Random();
		int noise = rand.nextInt(100000000); 
		Tidings tiding=new Tidings();
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class,new DateJsoLongProcessor());
		String msg = tiding.setFlag(true).setCode(LightappConstants.META_CODE_GET).setMsg("审批通知！").setData(JSONObject.fromObject(notify,jsonConfig)).toJson();
		JSONObject param = new JSONObject();
		param.put("timestamp", timestamp);
		param.put("noise", noise);
		param.put("msg", JSONObject.fromObject(msg));
		try {
			String paramKey = URLEncoder.encode(EMPTool.encrypt(param.toString()),"utf-8");
			Map<String, String> params = new HashMap<String, String>();
			params.put("token", paramKey);
			String result = HttpHelper.get(params, url);
			notify.setResult(result);
			notify.setStatus(ApprovalNotifyStatus.FINISH.getContext());
			approveNotifyThridDAO.save(notify);
		} catch (Exception e) {
			notify.setResult(e.getMessage());
			notify.setStatus(ApprovalNotifyStatus.ERROR.getContext());
			approveNotifyThridDAO.save(notify);
		}
	}
	
	/**
	 * 发送常规通知（流传通知）
	 * @param approvalApply
	 * @param record
	 * @return
	 */
	private ApproveNotifyThrid notifyNormal(ApprovalApply approvalApply,ApprovalApplyRecord record) {
		//已经通知成功的  不再触发通知
		if(approveNotifyThridDAO.isDoneByRecordId(record.getId(),ApprovalNotifyType.NORMAL)) return null;
		ApproveNotifyThrid last = approveNotifyThridDAO.findLastByRecordId(record.getId(), ApprovalNotifyType.NORMAL);
		//已经在5分钟内通知过了但未返回通知结果  不再触发通知
		if(last!=null&&ApprovalNotifyStatus.getApprovalNotifyStatus(last.getStatus())==ApprovalNotifyStatus.DOING
			&&(System.currentTimeMillis()-last.getTime().getTime())<300000) return null;
		ApproveNotifyThrid notify = new ApproveNotifyThrid();
		notify.setApplyId(record.getFormId());
		notify.setRecordId(record.getId());
		notify.setNowEid(record.getApprovalUser().getEid());
		notify.setNowOid(record.getApprovalUser().getOid());
		notify.setApplyStatus(approvalApply.getStatus());
		notify.setOptStatus(record.getStatus());
		notify.setTransferApproverOId(record.getTransferApproverOId());
		notify.setType(ApprovalNotifyType.NORMAL.getContext());
		notify.setStatus(ApprovalNotifyStatus.DOING.getContext());
		notify.setErrorCode(0);
		notify.setTime(new Date());
		String title= approvalApply.getTitle();
		if(!title.endsWith("申请")) title+= "申请";
		AppovalRecordStatus opt= AppovalRecordStatus.getAppovalRecordStatus(record.getStatus());
		if(opt==AppovalRecordStatus.CREATE) {
			UserNetwork userNetwork=userNetworkService.getUserNetworkByUnKnowId(record.getTransferApproverOId(), record.getApprovalUser().getEid(), record.getApprovalUser().getNetworkId());
			notify.setMsg(record.getApprovalUser().getName()+"提出了一个【"+title+"】"+"请"+userNetwork.getName()+"审批！");
		}else if(opt==AppovalRecordStatus.AGREE){
			notify.setMsg(record.getApprovalUser().getName()+"同意了"+approvalApply.getCreateUser().getName()+"的【"+title+"】");
		}else if(opt==AppovalRecordStatus.AGREE_END){
			notify.setMsg(record.getApprovalUser().getName()+"同意并完成了"+approvalApply.getCreateUser().getName()+"的【"+title+"】");
		}else if(opt==AppovalRecordStatus.DISAGREE){
			notify.setMsg(record.getApprovalUser().getName()+"不同意"+approvalApply.getCreateUser().getName()+"的【"+title+"】");
		}else if(opt==AppovalRecordStatus.HANG){
			notify.setMsg(record.getApprovalUser().getName()+"挂起了"+approvalApply.getCreateUser().getName()+"的【"+title+"】");
		}else if(opt==AppovalRecordStatus.CLOSE){
			notify.setMsg(record.getApprovalUser().getName()+"关闭了"+approvalApply.getCreateUser().getName()+"的【"+title+"】");
		}
		approveNotifyThridDAO.save(notify);
		return notify;
	}
	
	/**
	 * 发送警告通知
	 * @param approvalApply
	 * @param record
	 * @return
	 */
	private ApproveNotifyThrid notifyWarning(ApprovalApply approvalApply,ApprovalApplyRecord rd,ApprovalApplyRecord record) {
		if(AppovalStatus.DOING!=AppovalStatus.getAppovalStatus(approvalApply.getStatus())||record==null||record==rd) return null;
		UserIdentity nrUser = record.getApprovalUser();
		if(nrUser==null) return null;
		UserNetwork userNetwork=userNetworkService.getUserNetworkByUnKnowId(nrUser.getOid(), nrUser.getEid(), nrUser.getNetworkId());
		if(userNetwork==null||userNetwork.getStatus().equalsIgnoreCase("ACTIVE")) return null;
		ApproveNotifyThrid notify = new ApproveNotifyThrid();
		notify.setApplyId(record.getFormId());
		notify.setRecordId(rd.getId());
		notify.setNowEid(rd.getApprovalUser().getEid());
		notify.setNowOid(rd.getApprovalUser().getOid());
		notify.setApplyStatus(approvalApply.getStatus());
		notify.setOptStatus(rd.getStatus());
		notify.setTransferApproverOId(rd.getTransferApproverOId());
		notify.setType(ApprovalNotifyType.WARNING.getContext());
		notify.setStatus(ApprovalNotifyStatus.DOING.getContext());
		notify.setErrorCode(1);
		notify.setTime(new Date());
		String title= approvalApply.getTitle();
		if(!title.endsWith("申请")) title += "申请";
		notify.setMsg("待审批人"+nrUser.getName()+"已离职，无法对"+approvalApply.getCreateUser().getName()+"提出的【"+title+"】"+"进行审批操作！");
		approveNotifyThridDAO.save(notify);
		return notify;
	}
}
