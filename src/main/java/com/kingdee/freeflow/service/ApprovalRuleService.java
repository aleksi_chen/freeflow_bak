package com.kingdee.freeflow.service;

import java.util.List;

import com.kingdee.freeflow.domain.ApprovalCndGroup;
import com.kingdee.freeflow.domain.ApprovalRule;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.service.Tidings;

public interface ApprovalRuleService{
	/**
	 * 根据模板id 获得审批规则
	 * @param approvalRule
	 * @return
	 */
	ApprovalRule getRuleByFid(String templateId);
	/**
	 * 保存审批规则
	 * @param approvalRule
	 * @return
	 */
	Tidings saveRule(ApprovalRule approvalRule,List<ApprovalCndGroup> approvalCndGroups,UserIdentity user);
	/**
	 * 更新审批规则
	 * @param approvalRule
	 * @return
	 */
	Tidings updateRule(ApprovalRule approvalRule,List<ApprovalCndGroup> approvalCndGroups,UserIdentity user);
	/**
	 * 删除审批规则
	 * @param approvalRule
	 * @return
	 */
	Tidings removeRuleByFid(String templateId);
	
	/**
	 * @author linbu
	 * 
	 * 需要对approvalRule做处理，审批规则第一个是部门角色审批人并且审批人的部门负责人只有一个则需要做初始化显示
	 * @param approvalRule
	 */
	public void resetApprovalRuleForDeptRoleType(ApprovalRule approvalRule,UserIdentity user);
	
}
