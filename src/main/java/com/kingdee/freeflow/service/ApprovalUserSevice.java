package com.kingdee.freeflow.service;

import java.util.List;

import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.domain.ApprovalApplyRecord;
import com.kingdee.freeflow.domain.ApprovalNodeUser;
import com.kingdee.freeflow.domain.UserSimpleInfo;
import com.kingdee.freeflow.service.impl.ApprovalUserSeviceImpl.ApprovalUserResult;
import com.kingdee.sns.lightapp.domain.UserIdentity;

public interface ApprovalUserSevice{

	/**
	 * 获得指定人员信息
	 * @param networkId
	 * @param id(支持多种id)
	 * @return
	 */
	public UserIdentity getUserInfo(String networkId, String id);
	
	/**
	 * 获得指定人员信息
	 * @param networkId
	 * @param oId
	 * @return
	 */
	public ApprovalUserResult<ApprovalNodeUser> getNodeUsersInfo(String networkId, List<String> oIds);
	
	/**
	 * 解析审批规则 关键审批人
	 * @param ruleContext
	 * @param networkId
	 * @return
	 */
	public ApprovalUserResult<UserSimpleInfo> getFlowUserByRule(String ruleContext,String networkId);
	
	/**
	 * 根据审批申请id与用户信息 获得待处理人列表
	 * 
	 * 2016-08-17 废弃
	 * 该方法获取的推荐人是最近审批的转交人或者当前操作人的默认直属上级
	 * 该结果导致新的需求不一致（该结果的人不一定是在审批链路上的人）
	 * 
	 * @param Id ApprovalApply主表记录id
	 * @param user 当前操作人
	 * @return
	 */
	@Deprecated
	public ApprovalNodeUser getNearestUsersByFlowId(String id,UserIdentity user);
	
	/**
	 * 根据审批与用户信息 获得待处理人列表
	 *
	 * 2016-08-17 废弃
	 * 该方法获取的推荐人是最近审批的转交人或者当前操作人的默认直属上级
	 * 该结果导致新的需求不一致（该结果的人不一定是在审批链路上的人）
	 * 
	 *
	 * @param Id ApprovalApply主表记录id
	 * @param user 当前操作人
	 * @return
	 */
	@Deprecated
	public ApprovalNodeUser getNearestUsersByFlowId(ApprovalApply approvalApply, UserIdentity user);
	
	/**
	 * 审批人处理 用于冗余审批人信息
	 * @param approvalUser
	 * @param name
	 * @param status
	 * @return
	 */
	public String approvalUserToJson(String approvalUser,String name,String status);
	
	/**
	 * 是否允许进行审批流转
	 * @param approvalApply
	 * @param approvalApplyRecord
	 * @param user
	 * @return
	 */
	public boolean checkCanDoRecord(ApprovalApply approvalApply,ApprovalApplyRecord record, UserIdentity user);
	
	/**
	 * 是否允许进行审批同意并终止
	 * @param approvalApply
	 * @param approvalApplyRecord
	 * @param user
	 * @return
	 */
	public boolean checkCanAgreeEnd(ApprovalApply approvalApply,ApprovalApplyRecord record);
}
