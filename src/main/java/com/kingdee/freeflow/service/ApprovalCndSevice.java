package com.kingdee.freeflow.service;

import java.util.List;

import com.kingdee.freeflow.domain.ApprovalCndGroup;
import com.kingdee.freeflow.domain.ApprovalRule;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.service.Tidings;

public interface ApprovalCndSevice{
	/**
	 * 查询审批条件组数
	 * @param networkId
	 * @param userId
	 * @param approverStatus
	 * @return
	 */
	long countBy(String ruleId);
	
	/**
	 * 查询审批条件组列表记录
	 * @param networkId
	 * @param userId
	 * @param sort
	 * @param start
	 * @param limit
	 * @param approverStatus
	 * @return
	 */
	List<ApprovalCndGroup> getGroupsByRuleId(String ruleId, Integer start, Integer limit);
	
	/**
	 * 查询全部审批条件组列表记录
	 * @param ruleId 规则id
	 * @param isOnlyEnabled 是否仅查询有效条件组
	 * @return
	 */
	List<ApprovalCndGroup> getAllByRuleId(String ruleId,boolean isOnlyEnabled);
	
	/**
	 * 保存条件记录
	 * @param groups
	 * @param ruleId
	 */
	Tidings saveApprovalCndGroups(List<ApprovalCndGroup> groups, ApprovalRule approvalRule,UserIdentity user)throws Exception;
}
