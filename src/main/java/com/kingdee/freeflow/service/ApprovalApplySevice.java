package com.kingdee.freeflow.service;

import java.util.List;

import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.domain.ApprovalNodeUser;
import com.kingdee.freeflow.domain.ApprovalRule;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.service.Tidings;

public interface ApprovalApplySevice{
	/**
	 * 查询条数
	 * @param networkId
	 * @param creatorUserId
	 * @param status
	 * @return
	 */
	long count(ParamsObject paramsObejct);
	/**
	 * 查询所有记录
	 * @param networkId
	 * @param creatorUserId
	 * @param sort
	 * @param start
	 * @param limit
	 * @param status
	 * @return
	 */
	List<ApprovalApply> findAll(ParamsObject paramsObejct,String sort,Integer start,Integer limit);
	/**
	 * 根据id获得审批
	 * @param id
	 * @return
	 */
	ApprovalApply findById(String id); 
	/**
	 * 根据单据查询记录
	 */
	ApprovalApply findByReceiptId(String receiptId);
	/**
	 * 审批详情报表保存
	 * @param id
	 * @param content
	 * @param user
	 */
	void asyncSaveApprovalDetail(String id,String content,UserIdentity  user);
	/**
	 * 审批申请保存
	 * @param approvalApply
	 * @return
	 */
	Tidings saveFlow(ApprovalApply approvalApply, UserIdentity user, List<ApprovalNodeUser> nodeUsers, String transferApproverOId);
	/**
	 * 审批申请保存（第三方）
	 * @param approvalApply
	 * @return
	 */
	Tidings saveFlowForThrid(ApprovalApply approvalApply, ApprovalRule rule, UserIdentity user, List<ApprovalNodeUser> nodeUsers, String transferApproverOId);
}
