package com.kingdee.freeflow.service;

import com.kingdee.freeflow.domain.ApproveApplyThrid;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.service.Tidings;

public interface ApprovalThirdSevice {
	
	/**
	 * 第三方审批申请
	 * @param apply
	 * @param user
	 * @return
	 */
	public Tidings saveFlow(ApproveApplyThrid apply, UserIdentity user); 
	
	/**
	 * 第三方审批操作通知
	 * @param apply
	 * @param user
	 * @return
	 */
	public void notifyFlow(final String applyId);  
	
	/**
	 * 获取审批详情
	 * @param aid
	 * @return
	 */
	Tidings getDetails(String aid,UserIdentity user); 
}
