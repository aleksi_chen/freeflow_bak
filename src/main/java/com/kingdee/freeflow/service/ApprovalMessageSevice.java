package com.kingdee.freeflow.service;

import java.util.List;

import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.enums.AppovalRecordStatus;
import com.kingdee.sns.lightapp.domain.UserIdentity;

public interface ApprovalMessageSevice
{
	/**
	 * 发送待办消息（审批流转）
	 * @param approvalRecordId
	 */
	///void sendTodoMessage(String approvalRecordId);
	void sendTodoMessage(final String approvalId,final String nextRecordId,final AppovalRecordStatus type,final UserIdentity form,final UserIdentity to);
	
	/**
	 * 发送待办消息（审批催办）
	 * @param approvalRecordId
	 * @param oIds
	 * @param msg
	 */
	void sendMessageForUrge(final String approvalRecordId,final List<String> oIds);
	
	/**
	 * 发送订阅消息（审批完成）
	 * @param approvalRecordId
	 */
	void sendPubaccMessage(final String approvalRecordId);
	
	/**
	 * 消息提醒消除
	 * type 0=待办未处理;1=待办已处理
	 */
	void doMessage(final String id,final String oId,final int type);
	
	/**
	 * 消息提醒消除
	 * type 0=待办未处理;1=待办已处理
	 */
	void doMessage(final String approvalRecordId,final List<String> oIds,final int type);
	
	/**
	 * 系统消息(小组内审批流转提示)
	 * @param approvalApply
	 * @param user
	 * @param appovalRecordStatus
	 */
	void sendSYSMessageForApproval(final ApprovalApply approvalApply,final UserIdentity user,final AppovalRecordStatus appovalRecordStatus);
	
	/**
	 * 系统消息(发送评论消息)
	 * @param approvalApply
	 * @param user
	 */
	void sendSYSMessageForComment(final ApprovalApply approvalApply,final UserIdentity user);

	/**
	 * 新加审批人红包反馈消息
	 * @param approvalApply
	 * @param user
	 */
	void sendAddGroupUserSYSMessageFor(ApprovalApply approvalApply, UserIdentity user);
}