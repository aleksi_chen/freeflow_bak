package com.kingdee.freeflow.service;

import java.util.List;

import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.domain.ApprovalApplyRecord;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.service.Tidings;

public interface ApprovalApplyRecordSevice{
	/**
	 * 查询审批条数
	 * @param paramsObejct
	 * @return
	 */
	long countBy(ParamsObject paramsObejct);
	
	/**
	 * 查询我的审批条数（去除我自己创建的）
	 * @param paramsObejct
	 * @return
	 */
	long countMyBy(ParamsObject paramsObejct);
	
	/**
	 * 查询所有审批记录
	 * @param paramsObejct
	 * @param sort
	 * @param start
	 * @param limit
	 * @return
	 */
	List<ApprovalApplyRecord> findAll(ParamsObject paramsObejct, String sort, Integer start, Integer limit);
	
	/**
	 * 查询所有审批记录（去除我自己创建的）
	 * @param paramsObejct
	 * @param sort
	 * @param start
	 * @param limit
	 * @return
	 */
	List<ApprovalApplyRecord> findMy(ParamsObject paramsObejct, String sort, Integer start, Integer limit);
	
	/**
	 * 根据申请id获得审批记录
	 * @param approvalId
	 * @param isDone 是否查【仅包含】已操作的节点
	 * @param isCreator 是否【包含】创建申请节点
	 * @return
	 */
	List<ApprovalApplyRecord> findAllByApprovalId(String approvalId,boolean isDone,boolean isCreator);
	
	/**
	 * 根据id查询审批记录
	 * @param id
	 * @return
	 */
	ApprovalApplyRecord findById(String id);
	
	/**
	 * 保存审批记录
	 * @param approvalRecord
	 * @throws Exception
	 */
	void save(ApprovalApplyRecord approvalRecord) throws Exception;

	/**
	 * 审批流转
	 * @param record
	 * @param user
	 * @return
	 */
	Tidings updateFlow(ApprovalApplyRecord record,UserIdentity user);
	
	/**
	 * 获得流转推荐审批人
	 * @param recordId
	 * @param user
	 * @return
	 */
	Tidings getRecommendFlowUser(String recordId,UserIdentity user);

	
	/**
	 * 获取部门推荐审批人
	 * @param formTemplateId：审批模板id
	 * @param user:当前操作用户
	 * @param deptLevel：部门级别(一级部门，二级部门)
	 * @return
	 */
	Tidings getRecommendDeptRoleUser(String formTemplateId,String approvalApplyId, UserIdentity user,
			int deptLevel);
}