package com.kingdee.freeflow.service;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.kingdee.freeflow.common.queries.ParamsObject;

public interface ExcelExportService {
	public HSSFWorkbook exportApproval(ParamsObject paramsObject) throws Exception;
	
	
	
}
