package com.kingdee.freeflow.service;

import java.util.Set;

import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.domain.ApprovalDiscussGroup;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.service.Tidings;

public interface ApprovalDiscussGroupService
{
	/**
	 * 保存审批讨论组创建记录
	 * @param approvalRule
	 * @return
	 */
	boolean save(ApprovalDiscussGroup approvalDiscussGroup);
	
	
	/**
	 * 创建审批讨论组
	 * @param isOpenFlag 
	 */
	Tidings createDiscussGroup(ApprovalApply approvalApply, UserIdentity user,Set<String> nodeUserSet);
	

	void sendGroupMessage(ApprovalApply approvalApply);
        /**
         * 讨论组添加人
         * @param approvalApply
         * @param user
         * @return
         */
	Tidings addDiscussGroupUser(ApprovalApply approvalApply, Set<String> personSet);


	/**
	 * 一对一发送图文消息
	 * @param approvalApply
	 * @param userId
	 */
	void sendUserMessage(ApprovalApply approvalApply, String userId);

	/**
	 * 不受订单类型限制创建讨论组
	 * @param approvalApply
	 * @param user
	 * @param nodeList
	 * @return
	 */
	Tidings createDiscussGroupExcludeStatus(ApprovalApply approvalApply, UserIdentity user,Set<String> personSet);


}
