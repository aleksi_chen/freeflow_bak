package com.kingdee.freeflow.domain;

import java.util.Date;

import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;

/**
 * 审批申请(第三方使用)
 * @author S_Autumn
 * 2016.07.27
 */
public class ApproveNotifyThrid extends AbstractDynaBObject{
	private static final long serialVersionUID = 1553131033247402747L;
	
	private String applyId; 			// 申请id
	private String recordId; 			// 当前记录id
	private String nowOid; 				// 当前操作环节Oid
	private String nowEid; 				// 当前操作环节Eid
	private String applyStatus; 		// 申请状态
	private String optStatus; 			// 操作类型
	private String transferApproverOId; // 转交人oid
	private Date time;					// 触发时间
	private String status; 				// 通知状态
	private String type; 				// 通知类型
	private int errorCode; 				// 错误代码  0成功   1下一个节点离职无法流转
	private String msg; 				// 发送内容
	private String result; 				// 返回内容
	
	public String getApplyId() {
		return applyId;
	}
	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public String getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}
	public String getOptStatus() {
		return optStatus;
	}
	public void setOptStatus(String optStatus) {
		this.optStatus = optStatus;
	}
	public String getTransferApproverOId() {
		return transferApproverOId;
	}
	public void setTransferApproverOId(String transferApproverOId) {
		this.transferApproverOId = transferApproverOId;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getNowOid() {
		return nowOid;
	}
	public void setNowOid(String nowOid) {
		this.nowOid = nowOid;
	}
	public String getNowEid() {
		return nowEid;
	}
	public void setNowEid(String nowEid) {
		this.nowEid = nowEid;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
