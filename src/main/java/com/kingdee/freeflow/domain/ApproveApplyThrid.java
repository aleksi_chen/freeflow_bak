package com.kingdee.freeflow.domain;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.kingdee.freeflow.enums.AppovalStatus;
import com.kingdee.freeflow.enums.ApprovalType;

/**
 * 审批申请(第三方使用)
 * @author S_Autumn
 * 2016.07.27
 */
public class ApproveApplyThrid{
	private String id;					// 主键
	
	private String version; 			// 版本号
	
	private String bizzId;  			// 业务id
	
	private String title;				// 标题
	
	private String tabloid;				// 摘要
	
	private String detailURL;			// 详情URL
	
	private String notifyURL;			// 通知URL
	
	private String status;	 			// 申请状态	关闭:"0";申请中"1";审批完成:"2";审批驳回:"3";审批挂起:"4";
	
	private String transferApproverOId; // 指定一下个审批人用户oId
	
	private String approveType;			// 申请类型	0不指定审批人 1指定条件关键审批人 2指定无条件关键审批人
	
	private String approveFree;			// 自定义审批人	[{"oId":"","name":"","photo":"","personId":"","openId":""},...]
	
	private String approveKey;			// 关键审批人规则[{"oId":"","name":"","photo":"","personId":"","openId":""},...]
	
	private ApproveUserThrid creator;	// 审批创建人
	
	private Date createTime;			// 创建时间
	
	private Date lastUpdateTime;		// 最后的更新时间
	
	private String receiptId;           // 单据编号
	
	private String rejectId;            //驳回编号
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getBizzId() {
		return bizzId;
	}
	public void setBizzId(String bizzId) {
		this.bizzId = bizzId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTabloid() {
		return tabloid;
	}
	public void setTabloid(String tabloid) {
		this.tabloid = tabloid;
	}
	public String getDetailURL() {
		return detailURL;
	}
	public void setDetailURL(String detailURL) {
		this.detailURL = detailURL;
	}
	public String getNotifyURL() {
		return notifyURL;
	}
	public void setNotifyURL(String notifyURL) {
		this.notifyURL = notifyURL;
	}
	public String getStatus() {
		AppovalStatus type = AppovalStatus.getAppovalStatus(status);
		if(type==null)
			this.status = "";
		else
			this.status = type.getContext();
		return status;
	}
	public void setStatus(String status) {
		AppovalStatus type = AppovalStatus.getAppovalStatus(status);
		if(type==null)
			this.status = "";
		else
			this.status = type.getContext();
	}
	public ApproveUserThrid getCreator() {
		return creator;
	}
	public void setCreator(ApproveUserThrid creator) {
		this.creator = creator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public String getTransferApproverOId() {
		return transferApproverOId;
	}
	public void setTransferApproverOId(String transferApproverOId) {
		this.transferApproverOId = transferApproverOId;
	}
	public String getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}
	public String getRejectId() {
		return rejectId;
	}
	public void setRejectId(String rejectId) {
		this.rejectId = rejectId;
	}
	public String getApproveType() {
		ApprovalType type = ApprovalType.getApprovalType(approveType);
		if(type==null)
			this.approveType = "";
		else
			this.approveType = type.getContext();
		return approveType;
	}
	public void setApproveType(String approveType) {
		ApprovalType type = ApprovalType.getApprovalType(approveType);
		if(type==null)
			this.approveType = "";
		else
			this.approveType = type.getContext();
	}
	public String getApproveFree() {
		if(!StringUtils.isBlank(approveFree)){
			if(approveFree.startsWith("\"")&&approveFree.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(approveFree);
				approveFree=matcher2.replaceAll("");
				approveFree=approveFree.replaceFirst("\"", "");
			}
		}
		return approveFree;
	}
	public void setApproveFree(String approveFree) {
		if(!StringUtils.isBlank(approveFree)){
			if(approveFree.startsWith("\"")&&approveFree.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(approveFree);
				approveFree=matcher2.replaceAll("");
				approveFree=approveFree.replaceFirst("\"", "");
			}
		}
		this.approveFree = approveFree;
	}
	public String getApproveKey() {
		if(!StringUtils.isBlank(approveKey)){
			if(approveKey.startsWith("\"")&&approveKey.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(approveKey);
				approveKey=matcher2.replaceAll("");
				approveKey=approveKey.replaceFirst("\"", "");
			}
		}
		return approveKey;
	}
	public void setApproveKey(String approveKey) {
		if(!StringUtils.isBlank(approveKey)){
			if(approveKey.startsWith("\"")&&approveKey.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(approveKey);
				approveKey=matcher2.replaceAll("");
				approveKey=approveKey.replaceFirst("\"", "");
			}
		}
		this.approveKey = approveKey;
	}
}
