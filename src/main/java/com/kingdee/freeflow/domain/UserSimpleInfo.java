package com.kingdee.freeflow.domain;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.domain.UserNetwork;

public class UserSimpleInfo{	
	
	private String name;	  //审批人姓名
	private String personId;  //审批人personId
	private String oId;		  //审批人oId
	private String openId;	  //审批人openId
	private String networkId; //审批人networkId
	private String eid;	  	  //审批人eId
	private String photo;	  //审批人头像

	public UserSimpleInfo() {
		super();
	}
	
	public UserSimpleInfo(UserNetwork user) {
		super();
		if(user!=null){
			this.name = user.getName();
			this.personId = user.getPersonId();
			this.oId = user.getOid();
			this.openId =user.getOpenId();
			this.networkId = user.getNetworkId();
			this.eid = user.getEid();
			this.photo = user.getPhoto();
		}
	}
	
	public UserSimpleInfo(UserIdentity user) {
		super();
		if(user!=null){
			this.name = user.getName();
			this.personId = user.getPersonId();
			this.oId = user.getOid();
			this.openId =user.getOpenId();
			this.networkId = user.getNetworkId();
			this.eid = user.getEid();
			this.photo = user.getPhoto();
		}
	}
	
	public UserSimpleInfo(JSONObject approver) {
		super();
		if(approver!=null&&approver.isEmpty()){
			this.name = approver.getString("name");
			this.personId = approver.getString("personId");
			String oid = approver.getString("oId");
			if(StringUtils.isEmpty(oid)) oid = approver.getString("oid");
			this.oId = oid;
			this.openId =approver.getString("openId");
			this.networkId = approver.getString("networkId");
			this.eid = approver.getString("eid");
			this.photo = approver.getString("photo");
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getoId() {
		return oId;
	}

	public void setoId(String oId) {
		this.oId = oId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getEid() {
		return eid;
	}

	public void setEid(String eid) {
		this.eid = eid;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
