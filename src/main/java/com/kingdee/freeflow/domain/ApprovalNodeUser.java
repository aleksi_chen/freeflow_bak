package com.kingdee.freeflow.domain;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONObject;

import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.domain.UserNetwork;

public class ApprovalNodeUser extends AbstractDynaBObject{	
	
	private static final long serialVersionUID = -497912716197747661L;
	private String approvalId;//审批Id
	private String name;	  //审批人姓名
	private String personId;  //审批人personId
	private String oId;		  //审批人oId
	private String openId;	  //审批人openId
	private String networkId; //审批人networkId
	private String eid;	  	  //审批人eId
	private String photo;	  //审批人头像
	private int number;       //审批环节顺序
	private boolean isKey;	  //是否是关键审批人
	private boolean isDone;	  //是否完成审批操作
	private boolean isDelete; //是否已删除
	private Date createTime;  //创建时间
	
	//审批新特性-add on 2016-09-05
	private boolean isDeptRoleType;//是否是部门负责审批人
	private int deptLevel;//部门负责审批人的级别
	
	

	public ApprovalNodeUser() {
		super();
		this.createTime = new Date();
	}
	
	public ApprovalNodeUser(UserNetwork user) {
		super();
		if(user==null){
			this.createTime = new Date();
		}else{
			this.name = user.getName();
			this.personId = user.getPersonId();
			this.oId = user.getOid();
			this.openId =user.getOpenId();
			this.networkId = user.getNetworkId();
			this.eid = user.getEid();
			this.photo = user.getPhoto();
			this.createTime = new Date();
		}
	}
	
	public ApprovalNodeUser(UserIdentity user) {
		super();
		if(user==null){
			this.createTime = new Date();
		}else{
			this.name = user.getName();
			this.personId = user.getPersonId();
			this.oId = user.getOid();
			this.openId =user.getOpenId();
			this.networkId = user.getNetworkId();
			this.eid = user.getEid();
			this.photo = user.getPhoto();
			this.createTime = new Date();
		}
	}
	
	public ApprovalNodeUser(UserSimpleInfo user) {
		super();
		if(user==null){
			this.createTime = new Date();
		}else{
			this.name = user.getName();
			this.personId = user.getPersonId();
			this.oId = user.getoId();
			this.openId =user.getOpenId();
			this.networkId = user.getNetworkId();
			this.eid = user.getEid();
			this.photo = user.getPhoto();
			this.createTime = new Date();
		}
	}
	
	public ApprovalNodeUser(ApprovalApplyRecord record,int index) {
		super();
		setApprovalId(record.getFormId());
		setName(record.getApprovalUser().getName());
		setPersonId(record.getApprovalUser().getPersonId());
		setoId(record.getApprovalUser().getOid());
		setOpenId(record.getApprovalUser().getOpenId());
		setNetworkId(record.getApprovalUser().getNetworkId());
		setEid(record.getApprovalUser().getEid());
		setPhoto(record.getApprovalUser().getPhoto());
		setNumber(index);
		setIsDone(false);
		setIsKey(false);
		setCreateTime(new Date());
	}
	
	public ApprovalNodeUser(JSONObject approver) {
		super();
		if(approver==null||approver.isEmpty()){
			this.createTime = new Date();
		}else{
			this.name = approver.getString("name");
			this.personId = approver.getString("personId");
			String oid = approver.getString("oId");
			if(StringUtils.isEmpty(oid)) oid = approver.getString("oid");
			this.oId = oid;
			this.openId =approver.getString("openId");
			this.networkId = approver.getString("networkId");
			this.eid = approver.getString("eid");
			this.photo = approver.getString("photo");
			this.createTime = new Date();
		}
	}
	
	public String getApprovalId() {
		return approvalId;
	}
	public void setApprovalId(String approvalId) {
		this.approvalId = approvalId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	public String getoId() {
		return oId;
	}
	public void setoId(String oId) {
		this.oId = oId;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getEid() {
		return eid;
	}
	public void setEid(String eid) {
		this.eid = eid;
	}
	public boolean getIsKey() {
		return isKey;
	}
	public void setIsKey(boolean isKey) {
		this.isKey = isKey;
	}
	public boolean getIsDone() {
		return isDone;
	}
	public void setIsDone(boolean isDone) {
		this.isDone = isDone;
	}
	public boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public boolean isDeptRoleType()
	{
		return isDeptRoleType;
	}

	public void setDeptRoleType(boolean isDeptRoleType)
	{
		this.isDeptRoleType = isDeptRoleType;
	}

	public int getDeptLevel()
	{
		return deptLevel;
	}

	public void setDeptLevel(int deptLevel)
	{
		this.deptLevel = deptLevel;
	}
	
	
}
