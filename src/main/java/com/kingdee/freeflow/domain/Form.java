package com.kingdee.freeflow.domain;

import java.util.Date;

import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;

/**
 * 我的审批申请记录
 * @author dulianbin
 *
 */
public class Form extends AbstractDynaBObject{
	private static final long serialVersionUID = -7199716328343517994L;
	private String rejectFormId;  //驳回编号
	private String title;  //标题
	private String content; //内容
	private Integer status;  //内容
	private String networkId; //工作圈id
	private String creatorUserId; //用户id
	private String lastApproverUserId;//最后一个审批人id
	private String firstApproverUserId; //第一个审批人id
	private Date createTime;//创建时间
	private String applicationId; //应用id
	private String fileIds;  //文件id
	
	public String getRejectFormId()
	{
		return rejectFormId;
	}
	public void setRejectFormId(String rejectFormId)
	{
		this.rejectFormId = rejectFormId;
	}
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public String getContent()
	{
		return content;
	}
	public void setContent(String content)
	{
		this.content = content;
	}
	public Integer getStatus()
	{
		return status;
	}
	public void setStatus(Integer status)
	{
		this.status = status;
	}
	public String getNetworkId()
	{
		return networkId;
	}
	public void setNetworkId(String networkId)
	{
		this.networkId = networkId;
	}
	public String getCreatorUserId()
	{
		return creatorUserId;
	}
	public void setCreatorUserId(String creatorUserId)
	{
		this.creatorUserId = creatorUserId;
	}
	public String getLastApproverUserId()
	{
		return lastApproverUserId;
	}
	public void setLastApproverUserId(String lastApproverUserId)
	{
		this.lastApproverUserId = lastApproverUserId;
	}
	public String getFirstApproverUserId()
	{
		return firstApproverUserId;
	}
	public void setFirstApproverUserId(String firstApproverUserId)
	{
		this.firstApproverUserId = firstApproverUserId;
	}
	public Date getCreateTime()
	{
		return createTime;
	}
	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}
	public String getApplicationId()
	{
		return applicationId;
	}
	public void setApplicationId(String applicationId)
	{
		this.applicationId = applicationId;
	}
	public String getFileIds()
	{
		return fileIds;
	}
	public void setFileIds(String fileIds)
	{
		this.fileIds = fileIds;
	}
	
	

}
