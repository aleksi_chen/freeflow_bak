package com.kingdee.freeflow.domain;

import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;

/**
 * 我的审批申请记录
 *
 */
public class FreeFlowUser extends AbstractDynaBObject{
	private static final long serialVersionUID = -7199716328343517994L;
	
	private String openId;
	private String eid;
	private String networkId;
	private String userId;
	private String oid;
	private String name;
	private String deptId;
	private String deptName;
	private boolean isAdmin = false;
	private boolean isDeptAdmin = false;
	
	
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getEid() {
		return eid;
	}
	public void setEid(String eid) {
		this.eid = eid;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public boolean getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public boolean getIsDeptAdmin() {
		return isDeptAdmin;
	}
	public void setIsDeptAdmin(boolean isDeptAdmin) {
		this.isDeptAdmin = isDeptAdmin;
	}
}
