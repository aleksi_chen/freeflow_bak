package com.kingdee.freeflow.domain;

import java.util.Date;

import com.google.code.morphia.annotations.Embedded;
import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;
import com.kingdee.freeflow.enums.AppovalRecordStatus;
import com.kingdee.sns.lightapp.domain.UserIdentity;

/**
 * 审批记录
 * @author dulianbin
 *
 */
public class ApprovalApplyRecord extends AbstractDynaBObject {
	private static final long serialVersionUID = 8514350414441226925L;
	private String formId;//申请id 外键关联
	
	private String formTitle;//审批模板标题(冗余)
	
	private String applyTitle;//审批表单标题(冗余)
	
	private String formTemplateId; //模板id(冗余)
	
	@Embedded
	private UserIdentity formCreator;  //审批申请人的信息(冗余)
	
	private String status;//审批状态 -2未轮到  -1待处理  0关闭 1审批创建  2审批同意  3审批驳回  4审批同意并终止 5审批挂起  

	private Date approvalTime;//审批时间
	
	private String comment;//评论(留言)
	
	private String transferApproverOId;//指定一下个审批人用户oId
	
	private int deptLevel;//部门负责人的级别
	
	@Embedded
	private UserIdentity approvalUser;//当前环节用户信息
	
	private Date createTime;//创建时间
	
	public UserIdentity getFormCreator()
	{
		return formCreator;
	}

	public void setFormCreator(UserIdentity formCreator)
	{
		this.formCreator = formCreator;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public ApprovalApplyRecord()
	{
	}

	public ApprovalApplyRecord(String formId, String formTitle,String applyTitle,
			String comment, String formTemplateId, String status,
			Date approvalTime, String transferApproverOId,
			UserIdentity formCreator, UserIdentity approvalUser,Date createTime) {
		this.formId = formId;
		this.formTitle = formTitle;
		this.applyTitle = applyTitle;
		this.comment = comment;
		this.formTemplateId = formTemplateId;
		this.status = status;
		this.approvalTime = approvalTime;
		this.transferApproverOId = transferApproverOId;
		this.formCreator = formCreator;
		this.approvalUser = approvalUser;
		this.createTime=createTime;
	}
	
	public void initData(String formId, String formTitle,String applyTitle,
			String comment, String formTemplateId, String status,
			Date approvalTime, String transferApproverOId,
			UserIdentity formCreator, UserIdentity approvalUser,Date createTime) {
		this.formId = formId;
		this.formTitle = formTitle;
		this.applyTitle = applyTitle;
		this.comment = comment;
		this.formTemplateId = formTemplateId;
		this.status = status;
		this.approvalTime = approvalTime;
		this.transferApproverOId = transferApproverOId;
		this.formCreator = formCreator;
		this.approvalUser = approvalUser;
		this.createTime=createTime;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getFormTitle() {
		return formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getFormTemplateId() {
		return formTemplateId;
	}

	public void setFormTemplateId(String formTemplateId) {
		this.formTemplateId = formTemplateId;
	}

	public String getStatus() {
		AppovalRecordStatus type = AppovalRecordStatus.getAppovalRecordStatus(status);
		if(type==null)
			this.status = "";
		else
			this.status = type.getContext();
		return status;
	}

	public void setStatus(String status) {
		AppovalRecordStatus type = AppovalRecordStatus.getAppovalRecordStatus(status);
		if(type==null)
			this.status = "";
		else
			this.status = type.getContext();
		this.status = status;
	}
	
	public void setStatus(AppovalRecordStatus status) {
		this.status = status.getContext();
	}

	public Date getApprovalTime() {
		return approvalTime;
	}

	public void setApprovalTime(Date approvalTime) {
		this.approvalTime = approvalTime;
	}

	public UserIdentity getApprovalUser() {
		return approvalUser;
	}

	public void setApprovalUser(UserIdentity approvalUser) {
		this.approvalUser = approvalUser;
	}

	public String getTransferApproverOId() {
		return transferApproverOId;
	}

	public void setTransferApproverOId(String transferApproverOId) {
		this.transferApproverOId = transferApproverOId;
	}
	public String getApplyTitle()
	{
		return applyTitle;
	}

	public void setApplyTitle(String applyTitle)
	{
		this.applyTitle = applyTitle;
	}

	public int getDeptLevel()
	{
		return deptLevel;
	}

	public void setDeptLevel(int deptLevel)
	{
		this.deptLevel = deptLevel;
	}

	

}
