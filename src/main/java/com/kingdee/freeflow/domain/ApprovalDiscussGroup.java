package com.kingdee.freeflow.domain;

import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;

/**
 * 审批讨论组
 * @author bu_lin
 *
 */
public class ApprovalDiscussGroup extends AbstractDynaBObject
{

	/**
	 * 默认序列号id
	 */
	private static final long serialVersionUID = 1L;
	
	private String networkId;//工作圈Id 
	
	private String personIds;//讨论组的用户id
	
	private String creatorUserId;//创建人用户id

	private String remark;//描述说明
	
	private String createdTime;//创建时间
	
	private String sourceId;//来源（审批申请单id）

	public String getNetworkId()
	{
		return networkId;
	}

	public void setNetworkId(String networkId)
	{
		this.networkId = networkId;
	}

	public String getPersonIds()
	{
		return personIds;
	}

	public void setPersonIds(String personIds)
	{
		this.personIds = personIds;
	}

	public String getCreatorUserId()
	{
		return creatorUserId;
	}

	public void setCreatorUserId(String creatorUserId)
	{
		this.creatorUserId = creatorUserId;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public String getCreatedTime()
	{
		return createdTime;
	}

	public void setCreatedTime(String createdTime)
	{
		this.createdTime = createdTime;
	}

	public String getSourceId()
	{
		return sourceId;
	}

	public void setSourceId(String sourceId)
	{
		this.sourceId = sourceId;
	}
	


}
