package com.kingdee.freeflow.domain;

import java.util.Date;

import com.kingdee.freeflow.enums.AppovalRecordStatus;

/**
 * 审批记录(第三方使用)
 * @author S_Autumn
 * 2016.07.27
 */
public class ApproveRecordThird{
	private String id;					//记录节点id
	
	private String applyId;				//申请id
	
	private String status;				//操作状态
	
	private String transferApproverOId;	//下一个流转人oid
	
	private String comment;				//评论内容
	
	private Date approvalTime;			//操作时间
	
	private Date createTime;			//创建时间
	
	private ApproveUserThrid approver;	// 操作人

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getApplyId() {
		return applyId;
	}

	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}

	public String getStatus() {
		AppovalRecordStatus type = AppovalRecordStatus.getAppovalRecordStatus(status);
		if(type==null)
			this.status = "";
		else
			this.status = type.getContext();
		return status;
	}

	public void setStatus(String status) {
		AppovalRecordStatus type = AppovalRecordStatus.getAppovalRecordStatus(status);
		if(type==null)
			this.status = "";
		else
			this.status = type.getContext();
		this.status = status;
	}

	public String getTransferApproverOId() {
		return transferApproverOId;
	}

	public void setTransferApproverOId(String transferApproverOId) {
		this.transferApproverOId = transferApproverOId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getApprovalTime() {
		return approvalTime;
	}

	public void setApprovalTime(Date approvalTime) {
		this.approvalTime = approvalTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public ApproveUserThrid getApprover() {
		return approver;
	}

	public void setApprover(ApproveUserThrid approver) {
		this.approver = approver;
	}
}
