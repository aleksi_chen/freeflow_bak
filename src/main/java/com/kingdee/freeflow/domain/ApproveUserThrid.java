package com.kingdee.freeflow.domain;

/**
 * 审批人信息
 * @author S_Autumn
 * 2016.07.27
 */
public class ApproveUserThrid {

	private String oid;
	
	private String eid;
	
	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getEid() {
		return eid;
	}

	public void setEid(String eid) {
		this.eid = eid;
	}
}
