package com.kingdee.freeflow.domain;

import java.util.Date;

import com.google.code.morphia.annotations.Transient;
import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;

public class ApprovalRecord extends AbstractDynaBObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8514350414441226925L;
	private String networkId;// 工作圈Id
	private String formId;// 表单ID
	private String formTitle;// 表单标题（冗余）
	private String comment;// 评论(留言)
	private String approverUserId;// 审批人用户ID
	private int approvalStatus;// 审批状态（0.初始状态（当表单被重置时，对应所有审批记录都会被重置为初始化状态） 1.审批中 2.同意 3.驳回 ）
	private Date approvalTime;// 审批时间
	@Transient
	private String transferApproverUserId;// 转审批人用户ID

	private String formCreatorUserId;

	public int getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(int approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public Date getApprovalTime() {
		return approvalTime;
	}

	public void setApprovalTime(Date approvalTime) {
		this.approvalTime = approvalTime;
	}

	public String getTransferApproverUserId() {
		return transferApproverUserId;
	}

	public void setTransferApproverUserId(String transferApproverUserId) {
		this.transferApproverUserId = transferApproverUserId;
	}


	public String getFormTitle() {
		return formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getFormCreatorUserId() {
		return formCreatorUserId;
	}

	public void setFormCreatorUserId(String formCreatorUserId) {
		this.formCreatorUserId = formCreatorUserId;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getApproverUserId() {
		return approverUserId;
	}

	public void setApproverUserId(String approverUserId) {
		this.approverUserId = approverUserId;
	}

}
