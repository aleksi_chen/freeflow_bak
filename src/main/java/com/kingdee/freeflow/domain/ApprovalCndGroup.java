package com.kingdee.freeflow.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Transient;
import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;
import com.kingdee.sns.lightapp.domain.UserIdentity;

/**
 *	审批条件组
 */
public class ApprovalCndGroup extends AbstractDynaBObject{	
	private static final long serialVersionUID = -497912716197747661L;
	
	private String approvalRuleId;	//审批规则Id
	
	private String boundId;			//绑定条件id
	
	private String title;	  		//标题
	
	private String cndContext;	    //条件表达式内容
	
	private String usersContext;	//指定人信息  [{"oId":"","name":"","photo":""},{"oId":"","name":"","photo":""}]
	
	@Transient
	private List<ApprovalCnd> cnds;	//条件内容
	
	private int number;	  		    //顺序
	
	private Date createTime;	  	//创建时间
	
	private int status;	  			//状态   1有效 2失效。
	
	@Embedded
	private UserIdentity user;	  	//操作人
	
	public String getApprovalRuleId() {
		return approvalRuleId;
	}
	
	public void setApprovalRuleId(String approvalRuleId) {
		this.approvalRuleId = approvalRuleId;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	public String getCndContext() {
		if(!StringUtils.isBlank(cndContext)){
			if(cndContext.startsWith("\"")&&cndContext.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(cndContext);
				cndContext=matcher2.replaceAll("");
				cndContext=cndContext.replaceFirst("\"", "");
			}
		}
		return cndContext;
	}

	public void setCndContext(String cndContext) {
		if(!StringUtils.isBlank(cndContext)){
			if(cndContext.startsWith("\"")&&cndContext.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(cndContext);
				cndContext=matcher2.replaceAll("");
				cndContext=cndContext.replaceFirst("\"", "");
			}
		}
		this.cndContext = cndContext;
	}

	public String getUsersContext() {
		if(!StringUtils.isBlank(usersContext)){
			if(usersContext.startsWith("\"")&&usersContext.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(usersContext);
				usersContext=matcher2.replaceAll("");
				usersContext=usersContext.replaceFirst("\"", "");
			}
		}
		return usersContext;
	}

	public void setUsersContext(String usersContext) {
		if(!StringUtils.isBlank(usersContext)){
			if(usersContext.startsWith("\"")&&usersContext.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(usersContext);
				usersContext=matcher2.replaceAll("");
				usersContext=usersContext.replaceFirst("\"", "");
			}
		}
		this.usersContext = usersContext;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ApprovalCnd> getCnds() {
		return cnds;
	}

	public void setCnds(List<ApprovalCnd> cnds) {
		this.cnds = cnds;
	}
	
	public void addCnds(ApprovalCnd cnd) {
		if(this.cnds==null) this.cnds= new ArrayList<ApprovalCnd>();
		this.cnds.add(cnd);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public UserIdentity getUser() {
		return user;
	}

	public void setUser(UserIdentity user) {
		this.user = user;
	}

	public String getBoundId() {
		return boundId;
	}

	public void setBoundId(String boundId) {
		this.boundId = boundId;
	}
}
