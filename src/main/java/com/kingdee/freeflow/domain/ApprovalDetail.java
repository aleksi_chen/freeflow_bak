package com.kingdee.freeflow.domain;

import java.util.Date;

import com.google.code.morphia.annotations.Embedded;
import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;
import com.kingdee.sns.lightapp.domain.UserIdentity;

public class ApprovalDetail extends AbstractDynaBObject
{	
	private static final long serialVersionUID = 1948948641158951122L;
	private String approvalId;
	private String templateId;
	private String uId;
	private String upId;
	private String value;
	private String type;
	private String label;
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	public String getLabel()
	{
		return label;
	}
	public void setLabel(String label)
	{
		this.label = label;
	}
	private Date saveTime;
	@Embedded
	private UserIdentity applyUser;
	private boolean isDelete;
	
	public boolean getIsDelete()
	{
		return isDelete;
	}
	public void setIsDelete(boolean isDelete)
	{
		this.isDelete = isDelete;
	}
	public String getApprovalId() {
		return approvalId;
	}
	public void setApprovalId(String approvalId) {
		this.approvalId = approvalId;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public String getUpId() {
		return upId;
	}
	public void setUpId(String upId) {
		this.upId = upId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Date getSaveTime() {
		return saveTime;
	}
	public void setSaveTime(Date saveTime) {
		this.saveTime = saveTime;
	}
	public UserIdentity getApplyUser() {
		return applyUser;
	}
	public void setApplyUser(UserIdentity applyUser) {
		this.applyUser = applyUser;
	}
}
