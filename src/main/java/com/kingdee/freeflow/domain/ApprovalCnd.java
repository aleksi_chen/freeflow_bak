package com.kingdee.freeflow.domain;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;

/**
 * 审批条件
 */
public class ApprovalCnd extends AbstractDynaBObject{	
	private static final long serialVersionUID = -497912716197747661L;
	
	private String uid;	  	  	  //组件Id
	
	private String groupId;	  	  //条件组Id
	
	private String cndContext;	  //条件内容
	
	private Date createTime;	  //创建时间
	
	private int status;	  		  //状态 1为有效 2为废弃
	
	public String getGroupId() {
		return groupId;
	}
	
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Date getCreateTime() {
		return createTime;
	}
	
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getCndContext() {
		if(!StringUtils.isBlank(cndContext)){
			if(cndContext.startsWith("\"")&&cndContext.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(cndContext);
				cndContext=matcher2.replaceAll("");
				cndContext=cndContext.replaceFirst("\"", "");
			}
		}
		return cndContext;
	}

	public void setCndContext(String cndContext) {
		if(!StringUtils.isBlank(cndContext)){
			if(cndContext.startsWith("\"")&&cndContext.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(cndContext);
				cndContext=matcher2.replaceAll("");
				cndContext=cndContext.replaceFirst("\"", "");
			}
		}
		this.cndContext = cndContext;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
