package com.kingdee.freeflow.domain;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.google.code.morphia.annotations.Embedded;
import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;
import com.kingdee.freeflow.enums.AppovalStatus;
import com.kingdee.freeflow.enums.ApprovalType;
import com.kingdee.sns.lightapp.domain.UserIdentity;

/**
 * 我的审批申请记录
 */
public class ApprovalApply extends AbstractDynaBObject{
	private static final long serialVersionUID = -7199716328343517994L;
	private String version; // 版本号
	
	private String receiptId; // 单据编号
	
	private String rejectId;  //驳回编号
	
	private String formTemplateId;//表单模板ID
	
	private String status;//表单状态  关闭0  审批中1  审批完成2 审批驳回3  审批挂起4;
	
	private String title;//审批模板标题（冗余）
	
	private String applyTitle;//审批表单标题（冗余）
	
	private String tabloid;	// 摘要 （第三方使用）
	
	private String detailURL;// 详情URL（第三方使用）
	
	private String notifyURL;// 通知URL（第三方使用）
	
	private String bizzId;  // 业务id（第三方使用）
	
	private boolean isThird;  // 是否来源第三方
	
	private String content;//内容
	
	private String firstApproverOId;//第一个审批人oId
	
	private String lastApproverOId; //最后一个审批人oId
	
	@Embedded
	private UserIdentity createUser;// 创建审批用户的相关信息
	
	private Date createTime;//创建时间
	
	private Date lastUpdateTime;//最后的更新时间
	
	private String approvalType;  //审批规则类型（冗余）
	
	private String approvalRule;  //关键审批人信息
	
	private String approvalCndGroup;  //审批规则条件组ID
	
	private String recordId;  //当前环节id
	
	private UserIdentity approver;  //当前操作人
	
	private String nextRecordId;  //下一个环节id
	
	private UserIdentity nextApprover;  //下一个操作人
	
	private String groupId;//审批讨论组groupId(IM)
	
	private String approvalUser;  //操作人信息  除去创建人只保存前5个（用于审批管理查询 冗余） 
	
	
	public UserIdentity getNextApprover()
	{
		return nextApprover;
	}
	public void setNextApprover(UserIdentity nextApprover)
	{
		this.nextApprover = nextApprover;
	}
	
	public String getApprovalUser()
	{
		return approvalUser;
	}
	public void setApprovalUser(String approvalUser)
	{
		this.approvalUser = approvalUser;
	}
	public String getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}
	public String getRejectId() {
		return rejectId;
	}
	public void setRejectId(String rejectId) {
		this.rejectId = rejectId;
	}
	public String getFormTemplateId() {
		return formTemplateId;
	}
	public void setFormTemplateId(String formTemplateId) {
		this.formTemplateId = formTemplateId;
	}
	public String getStatus() {
		AppovalStatus type = AppovalStatus.getAppovalStatus(status);
		if(type==null)
			this.status = "";
		else
			this.status = type.getContext();
		return status;
	}
	public void setStatus(String status) {
		AppovalStatus type = AppovalStatus.getAppovalStatus(status);
		if(type==null)
			this.status = "";
		else
			this.status = type.getContext();
	}
	public void setStatus(AppovalStatus status) {
			this.status = status.getContext();
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		if(!StringUtils.isBlank(content)){
			if(content.startsWith("\"")&&content.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(content);
				content=matcher2.replaceAll("");
				content=content.replaceFirst("\"", "");
			}
		}
		return content;
	}
	public void setContent(String content) {
		if(!StringUtils.isBlank(content)){
			if(content.startsWith("\"")&&content.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(content);
				content=matcher2.replaceAll("");
				content=content.replaceFirst("\"", "");
			}
		}
		this.content = content;
	}
	public String getLastApproverOId() {
		return lastApproverOId;
	}
	public void setLastApproverOId(String lastApproverOId) {
		this.lastApproverOId = lastApproverOId;
	}
	public String getFirstApproverOId() {
		return firstApproverOId;
	}
	public void setFirstApproverOId(String firstApproverOId) {
		this.firstApproverOId = firstApproverOId;
	}
	public UserIdentity getCreateUser() {
		return createUser;
	}
	public void setCreateUser(UserIdentity createUser) {
		this.createUser = createUser;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getNextRecordId() {
		return nextRecordId;
	}
	public void setNextRecordId(String nextRecordId) {
		this.nextRecordId = nextRecordId;
	}
	public String getApprovalType() {
		ApprovalType type = ApprovalType.getApprovalType(approvalType);
		if(type==null)
			this.approvalType = "";
		else
			this.approvalType = type.getContext();
		return approvalType;
	}
	public void setApprovalType(String approvalType) {
		ApprovalType type = ApprovalType.getApprovalType(approvalType);
		if(type==null)
			this.approvalType = "";
		else
			this.approvalType = type.getContext();
	}
	public String getApprovalRule() {
		if(!StringUtils.isBlank(approvalRule)){
			if(approvalRule.startsWith("\"")&&approvalRule.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(approvalRule);
				approvalRule=matcher2.replaceAll("");
				approvalRule=approvalRule.replaceFirst("\"", "");
			}
		}
		return approvalRule;
	}
	public void setApprovalRule(String approvalRule) {
		if(!StringUtils.isBlank(approvalRule)){
			if(approvalRule.startsWith("\"")&&approvalRule.endsWith("\"")){
				Pattern reg2 = Pattern.compile("\"$"); 
				Matcher matcher2= reg2.matcher(approvalRule);
				approvalRule=matcher2.replaceAll("");
				approvalRule=approvalRule.replaceFirst("\"", "");
			}
		}
		this.approvalRule = approvalRule;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public UserIdentity getApprover() {
		return approver;
	}
	public void setApprover(UserIdentity approver) {
		this.approver = approver;
	}
	public String getGroupId()
	{
		return groupId;
	}
	public void setGroupId(String groupId)
	{
		this.groupId = groupId;
	}
	public String getApprovalCndGroup() {
		return approvalCndGroup;
	}
	public void setApprovalCndGroup(String approvalCndGroup) {
		this.approvalCndGroup = approvalCndGroup;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getTabloid() {
		return tabloid;
	}
	public void setTabloid(String tabloid) {
		this.tabloid = tabloid;
	}
	public String getBizzId() {
		return bizzId;
	}
	public void setBizzId(String bizzId) {
		this.bizzId = bizzId;
	}
	public boolean getIsThird() {
		return isThird;
	}
	public void setIsThird(boolean isThird) {
		this.isThird = isThird;
	}
	public String getDetailURL() {
		return detailURL;
	}
	public void setDetailURL(String detailURL) {
		this.detailURL = detailURL;
	}
	public String getNotifyURL() {
		return notifyURL;
	}
	public void setNotifyURL(String notifyURL) {
		this.notifyURL = notifyURL;
	}
	public String getApplyTitle()
	{
		return applyTitle;
	}
	public void setApplyTitle(String applyTitle)
	{
		this.applyTitle = applyTitle;
	}
	
}
