package com.kingdee.freeflow.domain;

import java.util.Date;

import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;

public class ReceiptNo extends AbstractDynaBObject
{
	
	

	private static final long serialVersionUID = -369370224194929468L;
	private String eId;        //  企业id
	private String networkId;  //  工作圈id
	private String currentDay;   //  当前日期(format=yyyyMMdd)
	private Integer sequence;  //  当前序列号
	private Date createTime;   //  创建时间(format=yyyy-MM-dd HH:mm:ss)
	
	public ReceiptNo()
	{
	}
	
	public ReceiptNo(String eId, String networkId, String currentDay,
			Integer sequence, Date createTime)
	{
		super();
		this.eId = eId;
		this.networkId = networkId;
		this.currentDay = currentDay;
		this.sequence = sequence;
		this.createTime = createTime;
	}

	public String geteId()
	{
		return eId;
	}

	public void seteId(String eId)
	{
		this.eId = eId;
	}

	public String getNetworkId()
	{
		return networkId;
	}

	public void setNetworkId(String networkId)
	{
		this.networkId = networkId;
	}

	public String getCurrentDay()
	{
		return currentDay;
	}

	public void setCurrentDay(String currentDay)
	{
		this.currentDay = currentDay;
	}

	public Integer getSequence()
	{
		return sequence;
	}

	public void setSequence(Integer sequence)
	{
		this.sequence = sequence;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}
	
	
	
	
}
