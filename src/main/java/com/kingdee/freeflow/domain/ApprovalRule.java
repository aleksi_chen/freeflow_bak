package com.kingdee.freeflow.domain;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.kingdee.cbos.core.framework.impl.AbstractDynaBObject;
import com.kingdee.freeflow.enums.ApprovalType;

public class ApprovalRule extends AbstractDynaBObject {
	private static final long serialVersionUID = -4848178467074217861L;
	
	private String templateId;	// 审批模板id

	private String approvalType;// 审批规则类型 0不指定审批人 1指定条件关键审批人 2指定无条件关键审批人

	private String content;		// 关键审批人信息  [{"oId":"","name":"","photo":""},{"oId":"","name":"","photo":""}]

	private String cndContext;	// 选中条件的表达式

	private String cndSelectId;	// 选中条件的对应组件Id

	private Date createTime;

	public String getApprovalType() {
		ApprovalType type = ApprovalType.getApprovalType(approvalType);
		if (type == null)
			this.approvalType = "";
		else
			this.approvalType = type.getContext();
		return approvalType;
	}

	public void setApprovalType(String approvalType) {
		ApprovalType appovalType = ApprovalType.getApprovalType(approvalType);
		if (appovalType == null)
			this.approvalType = "";
		else
			this.approvalType = appovalType.getContext();
	}

	public String getContent() {
		if (!StringUtils.isBlank(content)) {
			if (content.startsWith("\"") && content.endsWith("\"")) {
				Pattern reg2 = Pattern.compile("\"$");
				Matcher matcher2 = reg2.matcher(content);
				content = matcher2.replaceAll("");
				content = content.replaceFirst("\"", "");
			}
		}
		return content;
	}

	public void setContent(String content) {
		if (!StringUtils.isBlank(content)) {
			if (content.startsWith("\"") && content.endsWith("\"")) {
				Pattern reg2 = Pattern.compile("\"$");
				Matcher matcher2 = reg2.matcher(content);
				content = matcher2.replaceAll("");
				content = content.replaceFirst("\"", "");
			}
		}
		this.content = content;
	}

	public String getCndContext() {
		if (!StringUtils.isBlank(cndContext)) {
			if (cndContext.startsWith("\"") && cndContext.endsWith("\"")) {
				Pattern reg2 = Pattern.compile("\"$");
				Matcher matcher2 = reg2.matcher(cndContext);
				cndContext = matcher2.replaceAll("");
				cndContext = cndContext.replaceFirst("\"", "");
			}
		}
		return cndContext;
	}

	public void setCndContext(String cndContext) {
		if (!StringUtils.isBlank(cndContext)) {
			if (cndContext.startsWith("\"") && cndContext.endsWith("\"")) {
				Pattern reg2 = Pattern.compile("\"$");
				Matcher matcher2 = reg2.matcher(cndContext);
				cndContext = matcher2.replaceAll("");
				cndContext = cndContext.replaceFirst("\"", "");
			}
		}
		this.cndContext = cndContext;
	}

	public String getCndSelectId() {
		return cndSelectId;
	}

	public void setCndSelectId(String cndSelectId) {
		this.cndSelectId = cndSelectId;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
