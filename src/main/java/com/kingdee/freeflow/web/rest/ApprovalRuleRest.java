package com.kingdee.freeflow.web.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kingdee.freeflow.domain.ApprovalCnd;
import com.kingdee.freeflow.domain.ApprovalCndGroup;
import com.kingdee.freeflow.domain.ApprovalRule;
import com.kingdee.freeflow.enums.ApprovalType;
import com.kingdee.freeflow.service.ApprovalCndSevice;
import com.kingdee.freeflow.service.ApprovalRuleService;
import com.kingdee.sns.lightapp.authority.AuthorityType;
import com.kingdee.sns.lightapp.authority.FireAuthority;
import com.kingdee.sns.lightapp.authority.ResultTypeEnum;
import com.kingdee.sns.lightapp.common.constants.FormTemplateConstans;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.common.contexts.SecurityContext;
import com.kingdee.sns.lightapp.common.models.Context;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.domain.template.FormTemplate;
import com.kingdee.sns.lightapp.service.Tidings;
import com.kingdee.sns.lightapp.service.formtemplate.FormTemplateService;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

@Controller
@RequestMapping(value="/v1/rest/approvalRule")
public class ApprovalRuleRest extends BaseRest{
	private final static Logger LOGGER = LoggerFactory.getLogger(ApprovalRuleRest.class);

	@Autowired
	private FormTemplateService formTemplateService;
	
	@Autowired
	private ApprovalRuleService approvalRuleService;
	
	@Autowired
	private ApprovalCndSevice approvalCndSevice;
	
	@Autowired
	private SecurityContext securityContext;
	
	private static final String APPROVALRULE_KEY = "approvalRule";
	
	private static final String APPROVALCNDS_KEY = "approvalCndGroups";
	
	/**
	 * 审批规则详情
	 * @param request
	 * @param templateId
	 * @return
	 */
	@RequestMapping(value={"/detail/{id}"},method=RequestMethod.GET,produces="application/json;charset=UTF-8")
	@ResponseBody
	public String getApproversRuleDetail(HttpServletRequest request,@PathVariable String id){
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		if (StringUtils.isBlank(id)) return msg.toJson();
		FormTemplate formTemplate = formTemplateService.getFormTemplateById(id);
		ApprovalRule approvalRule = approvalRuleService.getRuleByFid(id);
		if(approvalRule==null) return msg.setFlag(false).setMsg("未找到对应详情！").setCode(LightappConstants.META_CODE_ERROR).toJson();
		List<ApprovalCndGroup> groups = approvalCndSevice.getAllByRuleId(approvalRule.getId(),false);
		msg.setFlag(true).setMsg("操作成功！").setCode(LightappConstants.META_CODE_GET)
		.put(FormTemplateConstans.TEMPLATE_KEY, formTemplate).put(APPROVALRULE_KEY, approvalRule).put(APPROVALCNDS_KEY, groups);
		LOGGER.info("===审批规则保存 /update/approvalRule 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}
	
	/**
	 * 保存一个审批规则(兼容新增与更新)
	 * @param params
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value={"/save"},method=RequestMethod.POST,produces="application/json;charset=UTF-8")
	@ResponseBody
	public String saveApproversRule(@RequestBody String params,HttpServletRequest request) throws Exception{
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		if(context.getUserNetwork()==null||user.getIsAdmin()!=true) return msg.setMsg("操作人非法！").setCode(LightappConstants.META_CODE_NOTACCEPT).toJson();
		if (StringUtils.isBlank(params)) return msg.toJson();
		try {
			JSONObject data = JSONObject.fromObject(params);
			if (data == null) return msg.toJson();
			ApprovalRule approvalRule = data.containsKey("approvalRule")?(ApprovalRule)JSONObject.toBean(data.getJSONObject("approvalRule"),ApprovalRule.class):null;
			
			Map<String, Class<?>> clazz = new HashMap<String, Class<?>>();
			clazz.put("cnds", ApprovalCnd.class);
			List<ApprovalCndGroup> approvalCndGroups = data.containsKey("approvalCndGroups")?JSONArray.toList(data.getJSONArray("approvalCndGroups"),ApprovalCndGroup.class,clazz):null;
			
			if (approvalRule==null){
				msg.setFlag(false).setMsg("指定审批规则为空！").setCode(LightappConstants.META_CODE_NOTACCEPT);	
				return msg.toJson();
			}
			if(StringUtils.isEmpty(approvalRule.getTemplateId())){
				msg.setFlag(false).setMsg("指定审批规则必须绑定模板！").setCode(LightappConstants.META_CODE_NOTACCEPT);	
				return msg.toJson();
			}
			if(ApprovalType.getApprovalType(approvalRule.getApprovalType())==null){
				msg.setFlag(false).setMsg("非法的审批类型！").setCode(LightappConstants.META_CODE_NOTACCEPT);
				return msg.toJson();
			}
			if(ApprovalType.NORMAL==ApprovalType.getApprovalType(approvalRule.getApprovalType())){
				if(StringUtils.isEmpty(approvalRule.getContent())||"[]".equals(approvalRule.getContent())||"null".equalsIgnoreCase(approvalRule.getContent())){
					msg.setFlag(false).setMsg("请至少选择一个审批人！").setCode(LightappConstants.META_CODE_NOTACCEPT);
					return msg.toJson();
				}
			}
			if(ApprovalType.KEY==ApprovalType.getApprovalType(approvalRule.getApprovalType())){
				if(approvalCndGroups==null){
					msg.setFlag(false).setMsg("请设置一个合法的审批规则！").setCode(LightappConstants.META_CODE_NOTACCEPT);
					return msg.toJson();
				}
			}
			msg = approvalRuleService.saveRule(approvalRule,approvalCndGroups,user);
		} catch (Exception e) {
			LOGGER.error("===审批规则保存 非法的参数，解析失败===saveApproversRule",e);
			msg.setFlag(false).setMsg("非法的参数，解析失败").setCode(LightappConstants.META_CODE_ERROR);
		}
		LOGGER.info("===审批规则保存 /save/approvalRule 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}
	
	/**
	 * 更新一个审批规则
	 * @param request
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value={"/update"},method=RequestMethod.POST,produces="application/json;charset=UTF-8")
	@ResponseBody
	public String updateApproversRule(HttpServletRequest request,@RequestBody String params) throws Exception {
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		if(context.getUserNetwork()==null||user.getIsAdmin()!=true) return msg.setMsg("操作人非法！").setCode(LightappConstants.META_CODE_NOTACCEPT).toJson();
		if (StringUtils.isBlank(params)) return msg.toJson();
		try {
			JSONObject data = JSONObject.fromObject(params);
			if (data == null) return msg.toJson();
			ApprovalRule approvalRule = data.containsKey("approvalRule")?(ApprovalRule)JSONObject.toBean(data.getJSONObject("approvalRule"),ApprovalRule.class):null;
			
			Map<String, Class<?>> clazz = new HashMap<String, Class<?>>();
			clazz.put("cnds", ApprovalCnd.class);
			List<ApprovalCndGroup> approvalCndGroups = data.containsKey("approvalCndGroups")?JSONArray.toList(data.getJSONArray("approvalCndGroups"),ApprovalCndGroup.class,clazz):null;
			
			if (approvalRule==null){
				msg.setFlag(false).setMsg("指定审批规则为空！").setCode(LightappConstants.META_CODE_NOTACCEPT);	
				return msg.toJson();
			}
			if(StringUtils.isEmpty(approvalRule.getTemplateId())){
				msg.setFlag(false).setMsg("指定审批规则必须绑定模板！").setCode(LightappConstants.META_CODE_NOTACCEPT);	
				return msg.toJson();
			}
			if(ApprovalType.getApprovalType(approvalRule.getApprovalType())==null){
				msg.setFlag(false).setMsg("非法的审批类型！").setCode(LightappConstants.META_CODE_NOTACCEPT);
				return msg.toJson();
			}
			if(ApprovalType.NORMAL==ApprovalType.getApprovalType(approvalRule.getApprovalType())){
				if(StringUtils.isEmpty(approvalRule.getContent())||"[]".equals(approvalRule.getContent())||"null".equalsIgnoreCase(approvalRule.getContent())){
					msg.setFlag(false).setMsg("请至少选择一个审批人！").setCode(LightappConstants.META_CODE_NOTACCEPT);
					return msg.toJson();
				}
			}
			if(ApprovalType.KEY==ApprovalType.getApprovalType(approvalRule.getApprovalType())){
				if(approvalCndGroups==null){
					msg.setFlag(false).setMsg("请设置一个合法的审批规则！").setCode(LightappConstants.META_CODE_NOTACCEPT);
					return msg.toJson();
				}
			}
			msg = approvalRuleService.updateRule(approvalRule,approvalCndGroups,user);
		} catch (Exception e) {
			LOGGER.error("===审批规则保存 非法的参数，解析失败===saveApproversRule",e);
			msg.setFlag(false).setMsg("非法的参数，解析失败").setCode(LightappConstants.META_CODE_ERROR);
		}
		LOGGER.info("===审批规则保存 /update/approvalRule 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}
}
