package com.kingdee.freeflow.web.rest;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.domain.ApprovalApplyRecord;
import com.kingdee.freeflow.domain.ApprovalNodeUser;
import com.kingdee.freeflow.enums.AppovalRecordStatus;
import com.kingdee.freeflow.enums.AppovalStatus;
import com.kingdee.freeflow.service.ApprovalApplyRecordSevice;
import com.kingdee.freeflow.service.ApprovalApplySevice;
import com.kingdee.freeflow.service.ApprovalDiscussGroupService;
import com.kingdee.freeflow.service.ApprovalMessageSevice;
import com.kingdee.freeflow.service.ApprovalThirdSevice;
import com.kingdee.freeflow.service.ApprovalUserSevice;
import com.kingdee.freeflow.service.ExcelExportService;
import com.kingdee.freeflow.service.OldApprovalService;
import com.kingdee.freeflow.utils.ExportExcelUtil;
import com.kingdee.freeflow.utils.JsonUtil;
import com.kingdee.sns.lightapp.authority.AuthorityType;
import com.kingdee.sns.lightapp.authority.FireAuthority;
import com.kingdee.sns.lightapp.authority.ResultTypeEnum;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.common.constants.LightappURLConstants;
import com.kingdee.sns.lightapp.common.contexts.SecurityContext;
import com.kingdee.sns.lightapp.common.models.Context;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.domain.UserNetwork;
import com.kingdee.sns.lightapp.domain.template.TemplateQuery;
import com.kingdee.sns.lightapp.service.Tidings;
import com.kingdee.sns.lightapp.service.UserNetworkService;
import com.kingdee.sns.lightapp.service.comment.BizCommentService;
import com.kingdee.sns.lightapp.service.configure.LightappConfigureService;
import com.kingdee.sns.lightapp.service.formtemplate.FormTemplateService;
import com.kingdee.sns.lightapp.service.openorg.OpenorgService;
import com.kingdee.sns.lightapp.service.xuntong.LAPPXunTongService;

@Controller
@RequestMapping("/v1/rest/approval")
public class ApprovalApplyRest extends BaseRest{
  private static final Logger LOGGER = LoggerFactory.getLogger(ApprovalApplyRest.class);
  
  @Autowired
  private FormTemplateService formTemplateService;
  
  @Autowired
  private ApprovalApplySevice approvalApplySevice;
  
  @Autowired
  private ApprovalApplyRecordSevice approvalApplyRecordSevice;
  
  @Autowired
  private ApprovalThirdSevice approvalThirdSevice;
  
  @Autowired
  private ApprovalUserSevice approvalUserSevice;
  
  @Autowired
  private ApprovalMessageSevice approvalMessageSevice;
  
  @Autowired
  private SecurityContext securityContext;
  
  @Autowired
  private OldApprovalService oldApprovalService;
  
  @Autowired
  private LAPPXunTongService lappXunTongService;
  
  @Autowired
  private OpenorgService openorgService;
  
  @Autowired
  private LightappConfigureService lightAppConfigureService;
  
  @Autowired
  private UserNetworkService userNetworkService;
  
  @Autowired
  private ApprovalDiscussGroupService approvalDiscussGroupService;
  
  @Autowired
  private ExcelExportService excelExportService;
  
  @Autowired
  private BizCommentService bizCommentService;
  
  /**
   * 获取我的审批申请列表 
   * @param request
   * @param params
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={"/list/myApprovals"},produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String getMyApprovals(@RequestBody String params,HttpServletRequest request){
    long startTime=System.currentTimeMillis();
    Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    try{
      if (StringUtils.isEmpty(params)) params="{}";
      JSONObject data = JSONObject.fromObject(params);
      ParamsObject paramsObject=null;
      if(data!=null&&data.containsKey("paramsObject")){
        paramsObject=(ParamsObject) JSONObject.toBean(data.getJSONObject("paramsObject"),ParamsObject.class); 
      }else{
        paramsObject= new ParamsObject();
      }
      user.setDeptId(null);
      paramsObject.setUser(user);
      String sort="status,-createTime";
      Integer start=data.containsKey("start")?data.getInt("start"):1;
      Integer limit=data.containsKey("limit")?data.getInt("limit"):10;
      List<ApprovalApply> list=approvalApplySevice.findAll(paramsObject, sort, start, limit);
      long count=approvalApplySevice.count(paramsObject);
      tiding.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_GET)
      .put("approvalApplys", list).put("total", count).put("start", start).put("limit", limit);
    }catch(Exception e){
    	LOGGER.error("===审批查询错误===/list/myApprovals",e);
      tiding.setFlag(false).setMsg("操作失败,执行出现异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("/list/approvalApplys (接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }
  
  /**
   * 获取所有审批申请 
   * @param request
   * @param params
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={"/list/approvalApplys"},produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String getApprovalApplys(@RequestBody String params,HttpServletRequest request){
    long startTime=System.currentTimeMillis();
    Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    try{
      if (StringUtils.isEmpty(params)) params="{}";
      JSONObject data = JSONObject.fromObject(params);
      ParamsObject paramsObject=null;
      if(data!=null&&data.containsKey("paramsObject")){
        paramsObject=(ParamsObject) JSONObject.toBean(data.getJSONObject("paramsObject"),ParamsObject.class); 
        if(paramsObject.getUser()!=null){
          paramsObject.getUser().setNetworkId(user.getNetworkId());
        } else {
          UserIdentity us = new UserIdentity();
          us.setNetworkId(user.getNetworkId());
          paramsObject.setUser(us);
        }
      }else{
        paramsObject= new ParamsObject();
        UserIdentity us = new UserIdentity();
        us.setNetworkId(user.getNetworkId());
        paramsObject.setUser(us);
      }
      //String sort=data.containsKey("order")?data.getString("order"):"-createTime";
      Integer start=data.containsKey("start")?data.getInt("start"):1;
      Integer limit=data.containsKey("limit")?data.getInt("limit"):10;
      List<ApprovalApply> list=approvalApplySevice.findAll(paramsObject, "-createTime", start, limit);
      long count=approvalApplySevice.count(paramsObject);
      tiding.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_GET)
      .put("approvalApplys", list).put("total", count).put("start", start).put("limit", limit);
    }catch(Exception e){
      LOGGER.error("===审批查询错误===/list/approvalApplys",e);
      tiding.setFlag(false).setMsg("操作失败,执行出现异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("/list/approvalApplys (接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }
  
  /**
   * 获取我的审批列表
   * @param request
   * @param params
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={ "/list/myApprovalRecords" },method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String getMyApprovalRecords(@RequestBody String params,HttpServletRequest request){
    long startTime=System.currentTimeMillis();
    Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    try{
      if (StringUtils.isEmpty(params)) params="{}";
      JSONObject data = JSONObject.fromObject(params);
      ParamsObject paramsObject=new ParamsObject();
      if(data!=null&&data.containsKey("paramsObject")){
        paramsObject=(ParamsObject) JSONObject.toBean(data.getJSONObject("paramsObject"),ParamsObject.class); 
      }else{
        paramsObject= new ParamsObject();
      }
      user.setDeptId(null);
      paramsObject.setUser(user);
      String sort="status,-createTime,-approvalTime";
      Integer start=data.containsKey("start")?data.getInt("start"):1;
      Integer limit=data.containsKey("limit")?data.getInt("limit"):10;
      //查询记录
      List<ApprovalApplyRecord> list=approvalApplyRecordSevice.findMy(paramsObject, sort, start, limit);
      long total=approvalApplyRecordSevice.countMyBy(paramsObject);
      tiding.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_GET)
      .put("ApprovalApplyRecords", list).put("total", total).put("start", start).put("limit", limit);
    }catch(Exception e){
      LOGGER.error("===审批查询错误===/list/myApprovalRecords",e);
      tiding.setFlag(false).setMsg("操作失败,执行操作异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("===/list/approvalRecords(接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }
  
  /**
   * 获取所有审批记录 
   * @param request
   * @param params
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={ "/list/approvalRecords" },method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String getApprovalRecords(@RequestBody String params,HttpServletRequest request){
    long startTime=System.currentTimeMillis();
    Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    try{
      if (StringUtils.isEmpty(params)) params="{}";
      JSONObject data = JSONObject.fromObject(params);
      ParamsObject paramsObject=new ParamsObject();
      if(data!=null&&data.containsKey("paramsObject")){
        paramsObject=(ParamsObject) JSONObject.toBean(data.getJSONObject("paramsObject"),ParamsObject.class); 
        if(paramsObject.getUser()!=null) {
        	paramsObject.getUser().setNetworkId(user.getNetworkId());
        } else {
            UserIdentity us = new UserIdentity();
            us.setNetworkId(user.getNetworkId());
            paramsObject.setUser(us);
          }
      }else{
        paramsObject= new ParamsObject();
        UserIdentity us = new UserIdentity();
        us.setNetworkId(user.getNetworkId());
        paramsObject.setUser(us);
      }
      String sort=data.containsKey("order")?data.getString("order"):"-createTime,-approvalTime";
      Integer start=data.containsKey("start")?data.getInt("start"):1;
      Integer limit=data.containsKey("limit")?data.getInt("limit"):10;
      //查询记录
      List<ApprovalApplyRecord> list=approvalApplyRecordSevice.findAll(paramsObject, sort, start, limit);
      long total=approvalApplyRecordSevice.countBy(paramsObject);
      tiding.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_GET)
      .put("ApprovalApplyRecords", list).put("total", total).put("start", start).put("limit", limit);
    }catch(Exception e){
      LOGGER.error("===审批查询错误===/list/approvalRecords",e);
      tiding.setFlag(false).setMsg("操作失败,执行操作异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("===/list/approvalRecords(接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }

  /**
   * 获取审批详情
   * @param id
   * @param model
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={"/detail/approvalApply/{id}"},method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String getApprovalApply(HttpServletRequest request,@PathVariable String id){
    long startTime=System.currentTimeMillis();
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    try{
      ApprovalApply approvalApply=approvalApplySevice.findById(id);
      if(approvalApply!=null){
        List<ApprovalApplyRecord> list=approvalApplyRecordSevice.findAllByApprovalId(approvalApply.getId(), false, true);
        //是否是 申请人
        boolean isCreateUser = user.getUserId().equals(approvalApply.getCreateUser().getUserId())&&user.getNetworkId().equals(approvalApply.getCreateUser().getNetworkId());
        //是否允许 审批操作
        boolean canDo = false;
        if(approvalApply.getNextApprover()!=null){
        	canDo=user.getOid().equals(approvalApply.getNextApprover().getOid())&&user.getNetworkId().equals(approvalApply.getNextApprover().getNetworkId());
        }
        //是否允许 审批同意并终止操作
        boolean isAgreeEnd = false;
        if(canDo==true){
        	ApprovalApplyRecord nextRecord = approvalApplyRecordSevice.findById(approvalApply.getNextRecordId());
        	if(nextRecord!=null)
        		isAgreeEnd=approvalUserSevice.checkCanAgreeEnd(approvalApply, nextRecord);
        	else
        		isAgreeEnd=false;
        }
        //第三方通知
        approvalThirdSevice.notifyFlow(id);
        tiding.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_GET)
        .put("approvalApply", approvalApply)
        .put("approvalApplyRecords", list)
        .put("appId", lightAppConfigureService.getPubaccFreeflowId())
        .put("lightAppId", lightAppConfigureService.getAppFreeflowId())
        .put("isCreateUser", isCreateUser)
        .put("isAgreeEnd", isAgreeEnd)
        .put("canDo", canDo);
        return tiding.toJson();
      }
      tiding.setFlag(true).setMsg("未找到审批！").setCode(LightappConstants.META_CODE_ERROR);
    }catch(Exception e){
      LOGGER.error("===审批查询错误===/detail/approvalApply",e);
      tiding.setFlag(false).setMsg("操作失败,执行出现异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("===/detail/approvalApply(接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }
  
  /**
   * 获取推荐审批人
   * @param id  审批申请id
   * @param model
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={"/recommendFlowUser/{id}"},method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String getRecommendFlowUser(HttpServletRequest request,@PathVariable String id){
    long startTime = System.currentTimeMillis();
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    Tidings tiding = new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    try{
    	tiding = approvalApplyRecordSevice.getRecommendFlowUser(id,user);
    }catch(Exception e){
      LOGGER.error("===审批查询错误===/detail/RecommendFlowUser",e);
      tiding.setFlag(false).setMsg("操作失败,执行出现异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("===/detail/RecommendFlowUser(接口响应耗时 ms):"+(endTime-startTime));    
    return tiding.toJson();
  }
  
  /**
   *  获取推荐角色审批人
   * @param request
   * @param params
   * @return
   */
  
  /**
	 * {
	 * 		"formTemplateId":"57c7e14460b2896f62c5b3cb",//审批模板id
	 *  	"approvId":"57c7e14460b2896f62c5b3c8",//审批申请单id
	 * 		"deptLevel":2,//要推荐的部门负责人级别
	 * }
	 */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={"/getRecommendDeptRoleUser"},method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String getRecommendDeptRoleUser(HttpServletRequest request,@RequestBody String params){
    long startTime = System.currentTimeMillis();
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    Tidings tiding = new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    try{
    	
    	JSONObject data = JSONObject.fromObject(params);
    	
    	String formTemplateId = "",approvalApplyId = "";
    	int deptLevel = 0;
    	
    	if(data != null ){
    		ParamsObject paramObject = (ParamsObject) JSONObject.toBean(data, ParamsObject.class);
    		if(paramObject == null) return tiding.setFlag(Boolean.FALSE).toJson();
			formTemplateId = paramObject.getFormTemplateId();
			approvalApplyId = paramObject.getApprovId();
			deptLevel = paramObject.getDeptLevel();
    	}
    	
    	tiding = approvalApplyRecordSevice.getRecommendDeptRoleUser(formTemplateId,approvalApplyId,user,deptLevel);     
    }catch(Exception e){
      LOGGER.error("===审批查询错误===/detail/RecommendFlowUser",e);
      tiding.setFlag(false).setMsg("操作失败,执行出现异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("===/detail/RecommendFlowUser(接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }
  
  
  
  
  
  
  /**
   * 保存我的审批申请
   * @param request
   * @param params
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value="/save/approvalApply/{templateId}",method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
  @ResponseBody
  @SuppressWarnings("unchecked")
  public String saveApproApply(@RequestBody String params,@PathVariable String templateId,HttpServletRequest request){
    long startTime=System.currentTimeMillis();
    Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    try{
      //参数解析
      if (StringUtils.isEmpty(params)) return tiding.toJson();
      JSONObject data = JSONObject.fromObject(params);
      if (data == null) return tiding.toJson();
      ApprovalApply approvalApply=(ApprovalApply) JSONObject.toBean(data.getJSONObject("approvalApply"),ApprovalApply.class);  
      if(approvalApply==null) return tiding.toJson();
      //默认值设置
      approvalApply.setFormTemplateId(templateId);
      //获得转审批人
      String transferApproverOId =data.containsKey("transferApproverOId")?data.getString("transferApproverOId"):"";
      //获得 自定义审批环节人信息
      List<ApprovalNodeUser> nodeUsers = data.containsKey("approvalNodeUsers")?JSONArray.toList(data.getJSONArray("approvalNodeUsers"),ApprovalNodeUser.class):null;
      //保存
      tiding=approvalApplySevice.saveFlow(approvalApply,user,nodeUsers,transferApproverOId);
      //第三方通知
      approvalThirdSevice.notifyFlow(approvalApply.getId());
    }catch(Exception e){
    	LOGGER.error("===审批保存失败===/save/approvalApply",e);
    	tiding.setFlag(false).setMsg("操作失败,执行出现异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("===/save/approvalApply(接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }
  
  /**
   * 审批处理流转
   * @param request
   * @param params
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={"/save/appApplyRecord"},method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String saveAppApplyRecord(HttpServletRequest request,@RequestBody String params){
    long startTime=System.currentTimeMillis();
    Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    try{
      if (StringUtils.isEmpty(params)) return tiding.toJson();
      JSONObject data = JSONObject.fromObject(params);
      if (data == null) return tiding.toJson();
      ApprovalApplyRecord approvalApplyRecord=(ApprovalApplyRecord) JSONObject.toBean(data.getJSONObject("approvalApplyRecord"),ApprovalApplyRecord.class);  
      if (approvalApplyRecord == null) return tiding.toJson();
      //审批流转并更新记录
      tiding=approvalApplyRecordSevice.updateFlow(approvalApplyRecord, user);
      //第三方通知
      if(!StringUtils.isEmpty(approvalApplyRecord.getId())){
    	  ApprovalApplyRecord record=approvalApplyRecordSevice.findById(approvalApplyRecord.getId());
    	  if(record!=null)  approvalThirdSevice.notifyFlow(record.getFormId());
      }
    }catch(Exception e){
    	LOGGER.error("===审批保存失败===/save/appApplyRecord",e);
    	tiding.setFlag(false).setMsg("操作失败,执行操作异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();   
    LOGGER.info("===/save/appApplyRecord(接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }
  
  /**
   * 申请催办
   * @param request
   * @param params
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={"/approvalUrge"},method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String approvalUrge(HttpServletRequest request,@RequestParam String approvalId,@RequestParam String nextApproverOid){
    long startTime=System.currentTimeMillis();
    Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    try{
      if(StringUtils.isEmpty(approvalId)||StringUtils.isEmpty(nextApproverOid))return tiding.toJson();
      ApprovalApply approvalApply=approvalApplySevice.findById(approvalId);
      if(approvalApply==null){
        return tiding.setFlag(false).setMsg("未找到申请！").setCode(LightappConstants.META_CODE_NOTFOUND).toJson();
      }
      if(AppovalStatus.DOING!=AppovalStatus.getAppovalStatus(approvalApply.getStatus())){
        return tiding.setFlag(false).setMsg("非合法的申请状态！").setCode(LightappConstants.META_CODE_NOTPASS).toJson();
      }
      if(!nextApproverOid.equals(approvalApply.getNextApprover().getOid())){
        return tiding.setFlag(false).setMsg("非合法的被催办人！").setCode(LightappConstants.META_CODE_NOTPASS).toJson();
      }
      if(!user.getNetworkId().equals(approvalApply.getCreateUser().getNetworkId())||!user.getOid().equals(approvalApply.getCreateUser().getOid())){
        return tiding.setFlag(false).setMsg("非合法的催办人！").setCode(LightappConstants.META_CODE_NOTPASS).toJson();
      }
      if(StringUtils.isEmpty(approvalApply.getNextRecordId())){
        return tiding.setFlag(false).setMsg("非合法的流程环节！").setCode(LightappConstants.META_CODE_NOTPASS).toJson();
      }
      ApprovalApplyRecord record = approvalApplyRecordSevice.findById(approvalApply.getNextRecordId());
      if(record==null){
        return tiding.setFlag(false).setMsg("非合法的流程环节！").setCode(LightappConstants.META_CODE_NOTPASS).toJson();
      }else if(AppovalRecordStatus.WAIT!=AppovalRecordStatus.getAppovalRecordStatus(record.getStatus())){
        return tiding.setFlag(false).setMsg("指定催办人已处理！").setCode(LightappConstants.META_CODE_NOTPASS).toJson();
      }
      List<String> oIds = new ArrayList<String>();
      oIds.add(nextApproverOid);
      approvalMessageSevice.sendMessageForUrge(record.getId(), oIds);
      tiding.setFlag(true).setMsg("催办成功！").setCode(LightappConstants.META_CODE_SAVE).toJson();
    }catch(Exception e){
    	LOGGER.error("===审批催办失败===/approvalUrge",e);
    	tiding.setFlag(false).setMsg("操作失败,执行操作异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("===/save/appApplyRecord(接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }
  
  /**
   * 获取我待审批记录数 
   * @param request
   * @param params
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={"/count/myWaitApproval"},produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String countMyWaitApproval(HttpServletRequest request){
    long startTime=System.currentTimeMillis();
    Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    try{
      ParamsObject paramsObject=new ParamsObject();
      paramsObject.setUser(user);
      paramsObject.setStatus(AppovalRecordStatus.WAIT);
      long count=approvalApplyRecordSevice.countBy(paramsObject);
        tiding.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_GET).put("total", count);
    }catch(Exception e){
      LOGGER.error("===我待审批记录数查询错误===/count/myWaitApproval",e);
      tiding.setFlag(false).setMsg("操作失败,执行出现异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("/count/myWaitApproval (接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }
  
  /**
   * 模板下拉数据查询
   * @param params
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={"/list/approvalTypes"},method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String getApprovalTypes(HttpServletRequest request, @RequestBody String params)throws Exception{
    long beginTime = System.currentTimeMillis();
    Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    JSONObject data = null;
    TemplateQuery query =null;
    if (StringUtils.isEmpty(params)){
      data = new JSONObject();
      query = new TemplateQuery();
      query.setUser(new UserIdentity());
      query.getUser().setNetworkId(user.getNetworkId());
    } else {
      data = JSONObject.fromObject(params);
      if (data == null) return msg.toJson();
      //参数解析
      query = (TemplateQuery) JSONObject.toBean(data.getJSONObject("query"),TemplateQuery.class);
      if(query==null) query = new TemplateQuery();
      if(query.getUser()!=null) {
        query.getUser().setNetworkId(user.getNetworkId());
      }else{
        UserIdentity quser = new UserIdentity();
        quser.setNetworkId(user.getNetworkId());
        query.setUser(quser);
      }
    }
    String order = data.containsKey("order")?data.getString("order"):null;
    order = StringUtils.isEmpty(order)?"isPublic,-createTime":order;
    Integer start = data.containsKey("start")?data.getInt("start"):null;
    start= (start==null||start<=1)?1:start;
    Integer limit =  data.containsKey("limit")?data.getInt("limit"):null;
    limit= limit==null?10:limit;
    
    //模板下拉数据查询
    msg=formTemplateService.findByNetworkId(query,start,limit,order);
    LOGGER.info("===list/operationLog 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
    return msg.toJson();
  }
  
  /**
   * 旧数据补全
   * @param request
   * @return
   */
  @RequestMapping(value={"/job/updatePATHMain"},method=RequestMethod.GET)
  @ResponseBody
  public String dataUp1(HttpServletRequest request){
    oldApprovalService.updateDataPATHMain();
    return "";
  }
  
  /**
   * 创建审批讨论组
   * @param params
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={"/create/approvalApplyDisGroup"},method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String createApprovalApplyDiscussGroup(HttpServletRequest request,@RequestBody String jsonParams){
	  long startTime = System.currentTimeMillis();

		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());

		Tidings tiding = new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
		String responseData = null;

		String pushmsg_url = lightAppConfigureService.getXuntongImHttpsHost() + LightappURLConstants.XUNTONG_CREATE_GROUP;
		try {
		    if (StringUtils.isEmpty(jsonParams)) {
			return tiding.toJson();
		    } else {
			JSONObject data = JSONObject.fromObject(jsonParams);
			// String templateId = data.getString("templateId");//
			String approvalApplyId = data.getString("id");// approvalApplyId
			if (StringUtils.isEmpty(approvalApplyId)) {
			    return tiding.toJson();
			}
			// 根据templateId获取oid->userNetWork->userId
			if (!StringUtils.isEmpty(approvalApplyId)) {

			    // 1.根据approvalApply
			    // id查询approvalApply（approvalType，approvalRule）
			    ApprovalApply approvalApply = approvalApplySevice.findById(approvalApplyId);
			    if (approvalApply != null && !"0".equalsIgnoreCase(approvalApply.getApprovalType())) {// 静态流
				String approvalRule = approvalApply.getApprovalRule();
				String oIds[] = JsonUtil.getValueByKey("oId", approvalRule);

				// 2.approvalRule->找到oid->userId
				List<String> oIdList = new ArrayList<String>();
				for (String oId : oIds) {
				    oIdList.add(oId);
				}
				// 优先根据personId取值，如果没有在根据oid匹配
				List<UserNetwork> userNetworkList = null;
				userNetworkList = userNetworkService.getUserNetworkByPersonList(oIdList);
				if (userNetworkList == null || userNetworkList.isEmpty()) {
				    userNetworkList = userNetworkService.getUserNetworkByOids(oIdList);
				}

				List<String> tempUserIdList = new ArrayList<String>();
				for (UserNetwork userNetwork : userNetworkList) {
				    tempUserIdList.add(userNetwork.getUserId());
				}
				// 3.组装json格式的useIds字符串参数
				String userIdsStr = this.builderJsonParam(tempUserIdList);
				// String openToken =
				// "7a19ca5c5df4ebc4e395d29ffaf167";//for test
				// TODO 需要身份认证获取openToken
				// String oid = "572ffcf400b041230857a444";//test
				String openToken = openorgService.getOpenTokenByOId(user.getOid());
				// userIdsStr =
				// "{\"userIds\":[\"568b70ffe4b0e98bdb95a55f\",\"56df98c0e4b0a5c137a04fef\"]}";//test
				// pushmsg_url =
				// "https://do.yunzhijia.com/xuntong/empeclite/kgiLTWl!rsQ=/ecLite/convers/createGroup.action";//test
				responseData = lappXunTongService.createApprovalDisGroup(userIdsStr, pushmsg_url, openToken);
				// TODO 如果创建成功则保存记录到ApprovalDiscussGroup

			    }
			}
		    }
		} catch (Exception e) {
		    LOGGER.error("===创建审批讨论组出错===", e.getMessage());
		    tiding.setFlag(false).setMsg("服务异常").setCode(LightappConstants.META_CODE_ERROR);
		}

		long endTime = System.currentTimeMillis();
		LOGGER.info("===/create/approvalApplyDisGroup(接口响应耗时 ms):" + (endTime - startTime));
		return responseData;
  }
  /**
   * {"userIds":["568b70ffe4b0e98bdb95a55f","56df98c0e4b0a5c137a04fef"]}
   * 组装json参数值
   * @param userList
   * @return
   */
  private String builderJsonParam(List<String> userList){
    String  startStr = "{\"userIds\":[",end = "]}";
    String userIdStr = "";
    if(userList != null && !userList.isEmpty()){
      for(int i = 0, l = userList.size(); i < l ; i++){
        userIdStr += "\""+userList.get(i)+"\"";
        if(i != l -1){
          userIdStr += ",";
        }
      }
      return startStr + userIdStr + end;
    }
    return ""; 
  }
  /***
   * 导出excel
   * @param formTemplateId
   * @param receiptId
   * @param deptId
   * @param userId
   * @param endTime
   * @param startTime
   * @param request
   * @param response
   * @throws Exception
   */
  @RequestMapping("/export")
   public void exportExcel(@RequestParam(value = "formTemplateId") String formTemplateId,
	      @RequestParam(value = "receiptId") String receiptId,
	      @RequestParam(value = "deptId") String deptId,
	      @RequestParam(value = "userId") String userId,
	      @RequestParam(value ="endTime")String endTime,
	      @RequestParam(value ="startTime")String startTime,
	      HttpServletRequest request, HttpServletResponse response) throws Exception{
	  	long operatingStartTime=System.currentTimeMillis();
	    HSSFWorkbook wb = null;
	    OutputStream ouputStream=null;
	    try{
	    	Context context =securityContext.getContext(request);
	        UserIdentity user = new UserIdentity(context.getUserNetwork());
	    	
	        ParamsObject paramsObject=new ParamsObject();
	        UserIdentity us = new UserIdentity();
	        us.setNetworkId(user.getNetworkId());
	        us.setDeptId(deptId);
	        us.setUserId(userId);
	        paramsObject.setUser(us);
	        paramsObject.setFormTemplateId(formTemplateId);
	        paramsObject.setReceiptId(receiptId);
	        if(StringUtils.isNotBlank(startTime)){
	        	paramsObject.setStartTime(Long.parseLong(startTime));
	        }
	        if(StringUtils.isNotBlank(startTime)){
	        	paramsObject.setEndTime(Long.parseLong(endTime));
	        }
	        //导出准备
	        response.setContentType("application/msexcel;");
			String fileName = ExportExcelUtil.createExportFileName("审批", request);
			response.setHeader("Content-Disposition", "attachment;" + fileName);
			wb = excelExportService.exportApproval(paramsObject);
			if(wb==null){	
			    return;
			}
			ouputStream = response.getOutputStream();
	        long operatingEndTime=System.currentTimeMillis();
	        LOGGER.info("/list/approvalRecords(接口响应耗时 ms):"+(operatingEndTime-operatingStartTime));
		   
	        wb.write(ouputStream); 
	    }catch(Exception e){
	    	LOGGER.error("导出所有审批记录失败:"+e);
	    }finally{
	    	try
			{
	    		if(ouputStream != null){
	    			ouputStream.flush();
					ouputStream.close();   
	    		}
				
			} catch (IOException e)
			{
				LOGGER.error("------导出审批excel------:"+e.getMessage());
			}    
	    }
  }
  
  /**
   * 打印审批单据
   * @param id
   * @param model
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
  @RequestMapping(value={"/print/approvalApply/{id}"},method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String printApprovalApply(HttpServletRequest request,@PathVariable String id){
    long startTime=System.currentTimeMillis();
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    String title = "";
    String netWorkName = "";
    String createTime = "";
    String printTime = "";
    String printer = "";
    Map<String,Object> formData = new LinkedHashMap<>();
    try{
      //获取审批详情
      ApprovalApply approvalApply=approvalApplySevice.findById(id); 
      if(approvalApply!=null){
        //查询所有的审批记录
        ParamsObject paramsObject=new ParamsObject();
        paramsObject.setApprovId(approvalApply.getId());
        List<ApprovalApplyRecord> recordlist=approvalApplyRecordSevice.findAllByApprovalId(approvalApply.getId(), false, true);
        com.alibaba.fastjson.JSONObject userInfo = openorgService.getUserInfoByEidAndOpenId(user.getEid(),user.getOpenId());
        title = approvalApply.getTitle();
        netWorkName = userInfo.getString("companyName");
        createTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(approvalApply.getCreateTime());
        printTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        printer = user.getName();
        formData.put("审批单号", approvalApply.getReceiptId());
        formData.put("申请人", approvalApply.getCreateUser().getName());
        formData.put("申请人部门", approvalApply.getCreateUser().getDeptName());
        formData.put("context", JsonUtil.getArrayFromJsonArray("label", "value", approvalApply.getContent()));
        //处理附件
        HashMap<String,Object> hashMap = JsonUtil.getHashMapFromJsonArray("type", "value", approvalApply.getContent());
        String fileList = (String)hashMap.get("filefield");
        String[] files = null;
        if(fileList!=null){
        	files = JsonUtil.getValueByKey("fileName", fileList);
        	formData.put("文件", files);
        }
        //拼装审批记录 把审批记录和评论结合在一起并按照时间排序
        String[][] record = getRecord(recordlist); 
        formData.put("审批记录", record);
        tiding.setFlag(true).setMsg("操作成功").setCode(LightappConstants.META_CODE_GET)
        .put("title", title)
        .put("netWorkName", netWorkName)
        .put("createTime", createTime)
        .put("printTime", printTime)
        .put("printer", printer)
        .put("formData", formData);
        return tiding.toOrderJson();
      }
      tiding.setFlag(true).setMsg("未找到审批！").setCode(LightappConstants.META_CODE_ERROR);
    }catch(Exception e){
    	LOGGER.error("===打印审批异常===",e);
      tiding.setFlag(false).setMsg("操作失败,执行出现异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("===/print/appApplyRecord(接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }
  
  public String[][] getRecord(List<ApprovalApplyRecord>recordlist){
	  String[][] result = null;
	  int index = 0 ,i = 0 ;
	  result = new String[recordlist.size()][2];
		while(i<recordlist.size()){
			ApprovalApplyRecord a = recordlist.get(i);
			result[index] = getRecord(a); 
			index ++;
			i++;
		}
	  return result;
  }
  
  /**
   * 处理审批记录对象
   * @param a
   * @return String[2]={"UserIdentity.getName() 同意审批","time"} example={"王小波 同意审批","2016-06-21 19：48：00"}
   */
  private String[] getRecord(ApprovalApplyRecord a){
	  String temp[] = new String[2]; 
	  if("1".equals(a.getStatus())){
		temp[0] = a.getApprovalUser().getName()+" 提交申请 ";
	  }else if("2".equals(a.getStatus())){
		temp[0] = a.getApprovalUser().getName()+" 同意审批 ";
	  }else if("3".equals(a.getStatus())){
		temp[0] = a.getApprovalUser().getName()+" 不同意并完成审批 ";
	  }else if("4".equals(a.getStatus())){
		temp[0] = a.getApprovalUser().getName()+" 同意并完成审批 ";
	  }else if("-1".equals(a.getStatus())){
		temp[0] = "等待 "+a.getApprovalUser().getName()+" 审批 ";
	  }
	  temp[1] = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(a.getCreateTime()); 
	  return temp;
  }
}
