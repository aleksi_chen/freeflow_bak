package com.kingdee.freeflow.web.rest;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kingdee.freeflow.domain.ApprovalCndGroup;
import com.kingdee.freeflow.domain.ApprovalRule;
import com.kingdee.freeflow.enums.ApprovalType;
import com.kingdee.freeflow.service.ApprovalCndSevice;
import com.kingdee.freeflow.service.ApprovalRuleService;
import com.kingdee.sns.lightapp.authority.AuthorityType;
import com.kingdee.sns.lightapp.authority.FireAuthority;
import com.kingdee.sns.lightapp.authority.ResultTypeEnum;
import com.kingdee.sns.lightapp.common.constants.FormTemplateConstans;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.common.contexts.SecurityContext;
import com.kingdee.sns.lightapp.common.models.Context;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.domain.operation.OperationQuery;
import com.kingdee.sns.lightapp.domain.template.FormTemplate;
import com.kingdee.sns.lightapp.domain.template.FormTemplateStatus;
import com.kingdee.sns.lightapp.domain.template.RecordTemplate;
import com.kingdee.sns.lightapp.domain.template.TemplateQuery;
import com.kingdee.sns.lightapp.service.Tidings;
import com.kingdee.sns.lightapp.service.formtemplate.FormTemplateService;
import com.kingdee.sns.lightapp.service.formtemplate.RecordTemplateService;
import com.kingdee.sns.lightapp.service.operationlog.OperationLogService;

@Controller
@RequestMapping(value = "/v1/rest/form-template")
public class FormTemplateRest extends BaseRest {
	private static final Logger LOGGER = LoggerFactory.getLogger(FormTemplateRest.class);

	@Autowired
	private ApprovalRuleService approvalRuleService;
	
	@Autowired
	private ApprovalCndSevice approvalCndSevice;

	@Autowired
	private FormTemplateService formTemplateService;

	@Autowired
	private RecordTemplateService recordTemplateService;
	
	@Autowired
	private OperationLogService operationLogService;

	@Autowired
	private SecurityContext securityContext;

	private static final String APPROVALRULE_KEY = "approvalRule";
	
	private static final String APPROVALCNDS_KEY = "approvalCndGroups";

	/**
	 * 获得模板详情接口
	 * @param request
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/detail/formTemplate/{id}" }, method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getFormTemplate(HttpServletRequest request, @PathVariable String id,@RequestParam(value="type",required=false,defaultValue="") String type) throws Exception {
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		if (StringUtils.isBlank(id)) return msg.toJson();
		//获取上下文信息
		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		//模板数据查询
		msg = formTemplateService.getTemplateById(id);
		if (!msg.isFlag()) return msg.toJson();
		//审批规则数据查询
		FormTemplate formTemplate = (FormTemplate)msg.get(FormTemplateConstans.TEMPLATE_KEY);
		ApprovalRule approvalRule = approvalRuleService.getRuleByFid(id);
		List<ApprovalCndGroup> groups = null;
		if(approvalRule==null){//自由流->没有审批规则
			approvalRule = new ApprovalRule();
			approvalRule.setTemplateId(formTemplate.getId());
			approvalRule.setCreateTime(new Date());
			approvalRule.setApprovalType(ApprovalType.FREE.getContext());
			approvalRuleService.saveRule(approvalRule,null,user);
		}else{
			boolean isOnlyEnabled= false;
			if("create".equalsIgnoreCase(type)) isOnlyEnabled=true;
			
			//TODO:需要对approvalRule做处理，审批规则第一个是部门角色审批人并且审批人的部门负责人只有一个则需要做初始化显示
			
			groups = approvalCndSevice.getAllByRuleId(approvalRule.getId(),isOnlyEnabled);
		}
		msg.put(APPROVALRULE_KEY, approvalRule).put(APPROVALCNDS_KEY, groups);
		LOGGER.info("===detail/formTemplate 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}
	
	/**
	 * 获得模板草稿详情接口
	 * @param request
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/detail/recordTemplate/{id}" }, method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getRecordTemplate(HttpServletRequest request, @PathVariable String id) throws Exception {
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		if (StringUtils.isBlank(id)) return msg.toJson();
		//模板数据查询
		msg = recordTemplateService.getRecordById(id);
		LOGGER.info("===detail/recordTemplate 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}

	/**
	 * 获取模板列表
	 * @param request
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/list/formTemplates" }, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getListForFormTemplate(HttpServletRequest request, @RequestBody String params) throws Exception {
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		JSONObject data = null;
		TemplateQuery query =null;
		if (StringUtils.isBlank(params)){
			data = new JSONObject();
			query = new TemplateQuery();
		} else {
			data = JSONObject.fromObject(params);
			if (data == null) return msg.toJson();
			query = (TemplateQuery) JSONObject.toBean(data.getJSONObject("query"),TemplateQuery.class);
			if(query==null) query = new TemplateQuery();
		}
		UserIdentity quser = new UserIdentity();
		quser.setNetworkId(user.getNetworkId());
		query.setUser(quser);
		String order = data.containsKey("order")?data.getString("order"):null;
		order = "isPublic,-createTime";
		Integer start = data.containsKey("start")?data.getInt("start"):null;
		start= (start==null||start<=1)?1:start;
		Integer limit =  data.containsKey("limit")?data.getInt("limit"):null;
		limit= limit==null?10:limit;
		boolean isTotal = data.containsKey("isTotal")?data.getBoolean("isTotal"):false;
		//模板数据查询
		msg = formTemplateService.getTemplates(query, order, start, limit,isTotal);
		LOGGER.info("===list/formTemplate 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}
	
	/**
	 * 获取模板草稿列表
	 * @param request
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/list/recordTemplates" }, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getListForRecordTemplate(HttpServletRequest request, @RequestBody String params) throws Exception {
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		JSONObject data = null;
		TemplateQuery query =null;
		if (StringUtils.isBlank(params)){
			data = new JSONObject();
			query = new TemplateQuery();
			query.setUser(new UserIdentity());
			query.getUser().setNetworkId(user.getNetworkId());
		} else {
			//参数解析
			data = JSONObject.fromObject(params);
			if (data == null) return msg.toJson();
			query = (TemplateQuery) JSONObject.toBean(data.getJSONObject("query"),TemplateQuery.class);
			if(query==null){
				query = new TemplateQuery();
			}
			if(query.getUser()!=null) {
				query.getUser().setNetworkId(user.getNetworkId());
			}else{
				query.setUser(new UserIdentity());
				query.getUser().setNetworkId(user.getNetworkId());
			}
		}
		String order = data.containsKey("order")?data.getString("order"):null;
		order = StringUtils.isBlank(order)?"-updateTime":order;
		Integer start = data.containsKey("start")?data.getInt("start"):null;
		start= (start==null||start<=1)?1:start;
		Integer limit =  data.containsKey("limit")?data.getInt("limit"):null;
		limit= limit==null?10:limit;
		boolean isTotal = data.containsKey("isTotal")?data.getBoolean("isTotal"):false;
		
		//模板数据查询
		msg = recordTemplateService.getRecords(query,order, start, limit,isTotal);
		LOGGER.info("===list/formTemplate 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}

	/**
	 * 创建草稿接口
	 * @param request
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/save/recordTemplate" }, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String saveRecordTemplate(HttpServletRequest request, @RequestBody String params) throws Exception {
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		if (StringUtils.isBlank(params)) return msg.toJson();
		JSONObject data = JSONObject.fromObject(params);
		if (data == null) return msg.toJson();
		//获取上下文信息
		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		if(context.getUserNetwork()==null||user.getIsAdmin()!=true) return msg.setMsg("操作人非法！").setCode(LightappConstants.META_CODE_NOTACCEPT).toJson();
		//存储操作
		if(!data.containsKey("id")||StringUtils.isBlank(data.getString("id"))){
			RecordTemplate record = (RecordTemplate)JSONObject.toBean(data.getJSONObject(FormTemplateConstans.RECORD_KEY),RecordTemplate.class);
			msg = recordTemplateService.addRecord(record, user);
		}else{
			msg = recordTemplateService.updateRecord(data, user);
		}
		LOGGER.info("===save/recordTemplate 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}
	
	/**
	 * 创建模板
	 * @param request
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/create/formTemplate" }, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String createFormTemplate(HttpServletRequest request,@RequestBody String params) throws Exception {
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		if (StringUtils.isBlank(params)) return msg.toJson();
		JSONObject data = JSONObject.fromObject(params);
		if (data == null) return msg.toJson();
		//获取上下文信息
		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		if(context.getUserNetwork()==null||user.getIsAdmin()!=true) return msg.setMsg("操作人非法！").setCode(LightappConstants.META_CODE_NOTACCEPT).toJson();
		//解析模板信息
		FormTemplate template = (FormTemplate) JSONObject.toBean(data.getJSONObject(FormTemplateConstans.TEMPLATE_KEY), FormTemplate.class);
		String fromId= data.containsKey("fromId")?data.getString("fromId"):null;
		if(template!=null&&!StringUtils.isEmpty(fromId)&&StringUtils.isEmpty(template.getComponents())){
			FormTemplate from = formTemplateService.getFormTemplateById(fromId);
			if(from!=null&&!StringUtils.isEmpty(from.getComponents())) 
				template.setComponents(from.getComponents());
		}
		//模板状态 0:未启用 1:已启用 -1:禁用 注意是字符串
		if (template==null) return msg.toJson();
		template.setStatus(FormTemplateStatus.ENABLED);
		template.setIsPublic(false);
		msg = formTemplateService.addTemplate(template, user);
		LOGGER.info("===create/formTemplate 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}

	/**
	 * 更新模板接口
	 * @param request
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/update/formTemplate" }, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String update(HttpServletRequest request, @RequestBody String params) throws Exception {
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings("参数不足！", LightappConstants.META_CODE_UNDO);
		if (StringUtils.isBlank(params)) return msg.toJson();
		//获取用户信息上下文
		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		if(context.getUserNetwork()==null||user.getIsAdmin()!=true) return msg.setMsg("操作人非法！").setCode(LightappConstants.META_CODE_NOTACCEPT).toJson();
		//参数解析
		JSONObject data = JSONObject.fromObject(params);
		if (data == null) return msg.toJson();
		JSONObject template = data.getJSONObject(FormTemplateConstans.TEMPLATE_KEY);
		//模板更新
		msg = formTemplateService.updateTemplate(template, user);
		LOGGER.info("===update/formTemplate 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}

	/**
	 * 批量状态更新
	 * @param request
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/update/formTemplateStatus" }, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String updates(HttpServletRequest request, @RequestBody String params)throws Exception {
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings("参数不足！", LightappConstants.META_CODE_UNDO);
		if (StringUtils.isBlank(params)) return msg.toJson();
		//获取用户信息上下文
		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		if(context.getUserNetwork()==null||user.getIsAdmin()!=true) return msg.setMsg("操作人非法！").setCode(LightappConstants.META_CODE_NOTACCEPT).toJson();
		//参数解析
		JSONObject data = JSONObject.fromObject(params);
		String templateIds = data.getString("templateIds");
		String recordIds = data.getString("recordIds");
		String[] tids = StringUtils.isEmpty(templateIds) ? null : templateIds.split(",");
		String[] rids = StringUtils.isEmpty(recordIds) ? null : recordIds.split(",");
		JSONObject updater = data.getJSONObject("updater");
		if (ArrayUtils.isEmpty(tids) && ArrayUtils.isEmpty(rids)|| updater == null) return msg.toJson();
		if (!ArrayUtils.isEmpty(tids)) {
			msg = formTemplateService.updateTemplatesStatus(tids, updater, user);
			if (!msg.isFlag())return msg.toJson();
		} else if (!ArrayUtils.isEmpty(rids)){
			msg = recordTemplateService.updateRecordsStatus(rids, updater, user);
		}
		LOGGER.info("===update/templateStatus 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}
	
	/**
	 * 操作日志
	 * @param request
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/list/operationLogs" }, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getOperationLogs(HttpServletRequest request, @RequestBody String params)throws Exception {
		long beginTime = System.currentTimeMillis();
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		if (StringUtils.isBlank(params)) return msg.toJson();
		JSONObject data = JSONObject.fromObject(params);
		if (data == null) return msg.toJson();
		//参数解析
		OperationQuery query = (OperationQuery) JSONObject.toBean(data.getJSONObject("query"),OperationQuery.class);
		String order = data.containsKey("order")?data.getString("order"):null;
		order = StringUtils.isBlank(order)?"-updateTime":order;
		Integer start = data.containsKey("start")?data.getInt("start"):null;
		start= (start==null||start<=1)?1:start;
		Integer limit =  data.containsKey("limit")?data.getInt("limit"):null;
		limit= limit==null?10:limit;
		boolean isTotal = data.containsKey("isTotal")?data.getBoolean("isTotal"):false;
		
		//模板数据查询
		msg = operationLogService.getOperationLogs(query, order, start, limit, isTotal);
		LOGGER.info("===list/operationLog 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}
	
	/**
	 * 验证模板名称
	 * @param request
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/check/templateTitle" }, method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getCheckTitle(HttpServletRequest request, @RequestParam("id") String id, @RequestParam("title") String title)throws Exception {
		long beginTime = System.currentTimeMillis();
		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		msg = formTemplateService.checkTemplateTitle(id, title, user);
		LOGGER.info("===/check/templateTitle 接口响应耗时（ms）==="+(System.currentTimeMillis()-beginTime));
		return msg.toJson();
	}
}
