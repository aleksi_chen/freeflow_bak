package com.kingdee.freeflow.web.rest;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kingdee.freeflow.domain.ApproveApplyThrid;
import com.kingdee.freeflow.service.ApprovalThirdSevice;
import com.kingdee.sns.lightapp.authority.AuthorityType;
import com.kingdee.sns.lightapp.authority.FireAuthority;
import com.kingdee.sns.lightapp.authority.ResultTypeEnum;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.common.contexts.SecurityContext;
import com.kingdee.sns.lightapp.common.models.Context;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.service.Tidings;

@Controller
@RequestMapping("/v1/third/rest")
public class ApprovalApplyThirdRest extends BaseRest{
  private static final Logger LOGGER = LoggerFactory.getLogger(ApprovalApplyThirdRest.class);
  
  @Autowired
  private ApprovalThirdSevice approvalThirdSevice;
  
  @Autowired
  private SecurityContext securityContext;
  
  /**
   * 创建审批申请
   * @param request
   * @param params
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP, AuthorityType.INTERNAL_APP })
  @RequestMapping(value="/approve/create",method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String saveApproApply(@RequestBody String params,HttpServletRequest request){
	long startTime=System.currentTimeMillis();
	Tidings tiding=new Tidings().init("参数验证不通过", LightappConstants.META_CODE_UNDO);
    Context context = securityContext.getContext(request);
    UserIdentity user = new UserIdentity(context.getUserNetwork());
    try{
      //参数解析
      if (StringUtils.isEmpty(params)) return tiding.toJson();
      JSONObject data = JSONObject.fromObject(params);
      if (data == null) return tiding.toJson();
      ApproveApplyThrid apply=(ApproveApplyThrid) JSONObject.toBean(data,ApproveApplyThrid.class);  
      if(apply==null) return tiding.toJson();
      //保存
      tiding=approvalThirdSevice.saveFlow(apply,user);
    }catch(Exception e){
    	LOGGER.error("===第三方审批申请创建失败===/approve/create",e);
    	tiding.setFlag(false).setMsg("操作失败,执行出现异常").setCode(LightappConstants.META_CODE_ERROR);
    }
    long endTime=System.currentTimeMillis();
    LOGGER.info("===/approve/create(接口响应耗时 ms):"+(endTime-startTime));
    return tiding.toJson();
  }
  
  /**
   * 获取审批详情
   * @param approAppId
   * @param request
   * @return
   */
  @FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP, AuthorityType.INTERNAL_APP })
  @RequestMapping(value="/approve/detail/{approAppId}",method=RequestMethod.GET,produces = "application/json;charset=UTF-8")
  @ResponseBody
  public String approApplyDetail(@PathVariable String approAppId,HttpServletRequest request){
	long startTime=System.currentTimeMillis();
	Context context = securityContext.getContext(request);
	UserIdentity user = new UserIdentity(context.getUserNetwork());
	LOGGER.info("===user==="+JSONObject.fromObject(user).toString());
	Tidings tiding = approvalThirdSevice.getDetails(approAppId,user);
	approvalThirdSevice.notifyFlow(approAppId);
	long endTime=System.currentTimeMillis();
	LOGGER.info("===/approve/detail/approAppId(接口响应耗时 ms):"+(endTime-startTime));
	return tiding.toJson();
  }
}
