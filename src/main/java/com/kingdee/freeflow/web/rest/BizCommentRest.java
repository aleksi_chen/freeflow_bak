package com.kingdee.freeflow.web.rest;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kingdee.freeflow.dao.ApprovalApplyDAO;
import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.freeflow.service.ApprovalApplySevice;
import com.kingdee.freeflow.service.ApprovalMessageSevice;
import com.kingdee.sns.lightapp.authority.AuthorityType;
import com.kingdee.sns.lightapp.authority.FireAuthority;
import com.kingdee.sns.lightapp.authority.ResultTypeEnum;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.common.contexts.SecurityContext;
import com.kingdee.sns.lightapp.common.models.Context;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.service.Tidings;
import com.kingdee.sns.lightapp.service.comment.BizCommentService;


@Controller
@RequestMapping(value = "/v1/rest/bizComment")
public class BizCommentRest extends BaseRest {
	@Autowired
	private BizCommentService bizCommentService;
	@Autowired
	private SecurityContext securityContext;
	@Autowired
	private ApprovalApplyDAO approvalApplyDAO;
	
	@Autowired
	private ApprovalApplySevice approvalApplySevice;
	
	@Autowired
	private ApprovalMessageSevice approvalMessageSevice;
	
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {
			AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/addComment" }, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String addComment(HttpServletRequest request, @RequestBody String params)
			throws Exception {
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		if (!StringUtils.isBlank(params)) {
			JSONObject commentJson = JSONObject.fromObject(params);
			Context context = securityContext.getContext(request);
			if(context==null||context.getUserNetwork()==null)
				context = (Context) request.getSession().getAttribute("CONTEXT");
			UserIdentity user = null;
			if(context != null){
				user = new UserIdentity(context.getUserNetwork());
				if (commentJson != null && commentJson.containsKey("bizId")) {
					String bizId = commentJson.getString("bizId");
					if(approvalApplyDAO.exist(bizId)){
						msg = bizCommentService.addComment(commentJson, user);  
						//添加IM发送消息提示
						if(msg.isFlag()){
							//找到approvalApply的是否有groupId
							ApprovalApply apply = approvalApplySevice.findById(bizId);
							if(apply != null && !StringUtils.isEmpty(apply.getGroupId())){
								approvalMessageSevice.sendSYSMessageForComment(apply, user);//发送消息
							}
						}
					} else {
						msg = new Tidings().init("操作失败，审批业务单据不存在！",LightappConstants.META_CODE_UNDO);
					}
					
				} else {
					msg = new Tidings().init("操作失败，参数bizId为空！",LightappConstants.META_CODE_UNDO);
				}
			} 
			

		} else {
			msg = new Tidings().init("操作失败，参数params为空！",LightappConstants.META_CODE_UNDO);
		}
		return msg.toJson();
	}
	
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {
			AuthorityType.LAPP, AuthorityType.WEB_LAPP })
	@RequestMapping(value = { "/findComment" }, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String findComment(HttpServletRequest request, @RequestBody String params)
			throws Exception {
		Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
		if (!StringUtils.isBlank(params)) {
			JSONObject queryJson = JSONObject.fromObject(params);
			Context context = securityContext.getContext(request);
			if(context==null||context.getUserNetwork()==null)
				context = (Context) request.getSession().getAttribute("CONTEXT");
			UserIdentity user = null;
			if(context != null){
				user = new UserIdentity(context.getUserNetwork());
			} else {
				user = new UserIdentity();
				user.setUserId("211110001test");
				user.setNetworkId("networkid");
				user.setOid("oid");
				user.setName("刘成俊");
				user.setOpenId("openid");
			}
			if (queryJson != null && queryJson.containsKey("bizId")) {
				String bizId = queryJson.getString("bizId");
				if(approvalApplyDAO.exist(bizId)){
					msg = bizCommentService.getComment(queryJson, user);
				} else {
					msg = new Tidings().init("操作失败，审批业务单据不存在！",LightappConstants.META_CODE_UNDO);
				}
			}

		} else {
			msg = new Tidings().init("操作失败，参数params为空！",LightappConstants.META_CODE_UNDO);
		}
		return msg.toJson();
	}

}
