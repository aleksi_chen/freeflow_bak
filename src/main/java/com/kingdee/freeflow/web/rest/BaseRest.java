package com.kingdee.freeflow.web.rest;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.google.gson.Gson;

/**
 * 说明：base restfull
 */
public class BaseRest {

	private static final Logger LOGGER = LoggerFactory.getLogger(BaseRest.class);

	protected Gson JSONTool = new Gson();

	@ExceptionHandler
	public String exception(HttpServletRequest request, HttpServletResponse response, Exception e) {
		try {
			PrintWriter writer = response.getWriter();
			writer.write("meta: {"+ "code: 500,"+ "status: \"failure\","+ "msg:\""+e.getMessage()+"\""+ "}");
			writer.flush();
			LOGGER.error(e.getMessage(), e);
		} catch (IOException e1) {
			LOGGER.error(e1.getMessage(), e);
		}
		return null;
	}

	
}
