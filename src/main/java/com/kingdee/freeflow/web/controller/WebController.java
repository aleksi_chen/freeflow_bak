package com.kingdee.freeflow.web.controller;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kingdee.freeflow.service.ApprovalRuleService;
import com.kingdee.sns.lightapp.authority.AuthorityType;
import com.kingdee.sns.lightapp.authority.FireAuthority;
import com.kingdee.sns.lightapp.authority.ResultTypeEnum;
import com.kingdee.sns.lightapp.common.constants.FormTemplateConstans;
import com.kingdee.sns.lightapp.common.constants.LightappConstants;
import com.kingdee.sns.lightapp.common.contexts.SecurityContext;
import com.kingdee.sns.lightapp.common.models.Context;
import com.kingdee.sns.lightapp.domain.Network;
import com.kingdee.sns.lightapp.domain.UserIdentity;
import com.kingdee.sns.lightapp.service.LightNetworkService;
import com.kingdee.sns.lightapp.service.Tidings;
import com.kingdee.sns.lightapp.service.formtemplate.FormTemplateService;
import com.kingdee.sns.lightapp.service.openorg.OpenorgService;

@Controller
@RequestMapping(value = "/web")
public class WebController {

	@Autowired
	private SecurityContext securityContext;
	
	@Autowired
	private LightNetworkService lightNetworkService;
	
	@Autowired
	private FormTemplateService formTemplateService;
	
	@Autowired
	private OpenorgService openorgService;
	
	@Autowired
	private ApprovalRuleService approvalRuleService;
	
	/**
	 * 模板管理页面
	 * @param request
	 * @param model
	 * @return
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP})
	@RequestMapping(value = { "/form-design" })
	public String formDesign(@RequestParam(defaultValue="") String id,HttpServletRequest request, Model model) {
		Context context = securityContext.getContext(request);
		if(context.getUserNetwork()==null){
			model.addAttribute("success", false);
			model.addAttribute("errorCode", 5001);
			model.addAttribute("msg", "获取用户身份失败");
			return "errorpage";
		} 
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		if(user.getIsAdmin()!=true){
			model.addAttribute("success", false);
			model.addAttribute("errorCode", 5001);
			model.addAttribute("msg", "只允许管理员配置审批模板喔~");
			return "errorpage";
		} 
		model.addAttribute("user", user);
		if(!StringUtils.isBlank(id)){
			Tidings msg = new Tidings().init("操作失败，参数验证不通过！",LightappConstants.META_CODE_UNDO);
			//模板数据查询
			msg = formTemplateService.getTemplateById(id);
			if (!msg.isFlag())  {
				model.addAttribute("success", false);
				model.addAttribute("errorCode", 5001);
				model.addAttribute("msg", msg.getMsg());				
				return "errorpage";
			} 
			//审批规则数据查询
			model.addAttribute(FormTemplateConstans.TEMPLATE_KEY, msg.get(FormTemplateConstans.TEMPLATE_KEY));
			model.addAttribute("approvalRule", approvalRuleService.getRuleByFid(id));
		}
		Network network = lightNetworkService.getNetworkById(user.getNetworkId());
		model.addAttribute("networkName", network!=null?network.getName():null);
		return "form-design";
	}
	
	/**
	 * 审批管理页面
	 * @param request
	 * @param model
	 * @return
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP})
	@RequestMapping(value = { "/approval-manage" })
	public String approvalManage(HttpServletRequest request, Model model) {
		Context context = securityContext.getContext(request);
		if(context.getUserNetwork()==null){
			model.addAttribute("success", false);
			model.addAttribute("errorCode", 5001);
			model.addAttribute("msg", "获取用户身份失败");
			return "errorpage";
		} 
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		if(user.getIsAdmin()!=true){
			model.addAttribute("success", false);
			model.addAttribute("errorCode", 5001);
			model.addAttribute("msg", "只允许管理员配置审批模板喔~");
			return "errorpage";
		} 
		model.addAttribute("user", user);
		Network network = lightNetworkService.getNetworkById(user.getNetworkId());
		model.addAttribute("networkName", network!=null?network.getName():null);
		return "approval-manage";
	}

	/**
	 * 审批管理页面
	 * @param request
	 * @param model
	 * @return
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP})
	@RequestMapping(value = { "/approval-rule" })
	public String approvalRule(HttpServletRequest request, Model model) {
		Context context = securityContext.getContext(request);
		if(context.getUserNetwork()==null){
			model.addAttribute("success", false);
			model.addAttribute("errorCode", 5001);
			model.addAttribute("msg", "获取用户身份失败");
			return "errorpage";
		} 
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		if(user.getIsAdmin()!=true){
			model.addAttribute("success", false);
			model.addAttribute("errorCode", 5001);
			model.addAttribute("msg", "只允许管理员配置审批模板喔~");
			return "errorpage";
		} 
		model.addAttribute("user", user);
		return "approval-rule";
	}
	
	/**
	 * 审批用户身份接口
	 * @param request
	 * @param model
	 * @return
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP})
	@RequestMapping(value={"/userInfo"},produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String getUserInfo(HttpServletRequest request) {
		Context context = securityContext.getContext(request);
		UserIdentity user = new UserIdentity(context.getUserNetwork());
		return JSONObject.fromObject(user).toString();
	}
	
	/**
	 * 审批WEB管理路由
	 * @param request
	 * @param model
	 * @return
	 */
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP})
	@RequestMapping(value = { "/manager/**" })
	public String managerPage() {
		return "approval-web";
	}
}
