package com.kingdee.freeflow.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kingdee.sns.lightapp.service.configure.LightappConfigureService;

@Controller
@RequestMapping(value = "/c")
public class OldMobileController {

	private static final Logger LOGGER = LoggerFactory.getLogger(OldMobileController.class);
	@Autowired
	private LightappConfigureService lightappConfigureService;

	private final static String DATAIL_URL = "/freeflow/m/approval/";

	@RequestMapping(value = { "/form/detail.json" })
	public void formDesign(HttpServletRequest request, HttpServletResponse response, @RequestParam String formId,
			@RequestParam String ticket) {
		String url = lightappConfigureService.getYZJOutHost() + DATAIL_URL + formId + "?lappName=freeflow&ticket="
				+ ticket;
		try {
			response.sendRedirect(url);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}

	}
}