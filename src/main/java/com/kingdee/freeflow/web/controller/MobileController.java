package com.kingdee.freeflow.web.controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kingdee.cbos.common.utils.StringUtils;
import com.kingdee.sns.lightapp.authority.AuthorityType;
import com.kingdee.sns.lightapp.authority.FireAuthority;
import com.kingdee.sns.lightapp.authority.ResultTypeEnum;
import com.kingdee.sns.lightapp.service.configure.LightappConfigureService;

@Controller
@RequestMapping(value = "/m")
public class MobileController {

	@Autowired
	private LightappConfigureService lightappConfigureService;
	
	
	private final static String GROUP_URL="/freeflow/m/select";
	
	@FireAuthority(resultType = ResultTypeEnum.page, authorityTypes = {AuthorityType.LAPP, AuthorityType.WEB_LAPP})
	@RequestMapping(value = { "/**" })
	public String formDesign(HttpServletRequest request, HttpServletResponse response,@RequestParam(defaultValue="")String groupId,@RequestParam(defaultValue="false")boolean isInner) {
		if(!StringUtils.isEmpty(groupId)&&!isInner){
			String query = "";
			Enumeration<String> paramKeys = request.getParameterNames();
			while (paramKeys.hasMoreElements()) {
				String key =  paramKeys.nextElement();
				if(key.equalsIgnoreCase("ticket")||key.equalsIgnoreCase("webLappToken")){
					continue;
				}
				String vlaue =  request.getParameter(key);
				query+="&"+key+"="+vlaue;
			}
			query=query.replaceFirst("&", "");
			query+="&isInner=true";
			String url=lightappConfigureService.getYZJOutHost()+GROUP_URL+"?"+query;
			try {
				response.sendRedirect(url);
				return null;
			} catch (IOException e) {
				return "errorpage";
			}
		}else{
			return "approval-mobile";
		}
	}
}