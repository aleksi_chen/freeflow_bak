package com.kingdee.freeflow.dao;

import java.util.List;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.domain.ApprovalApplyRecord;

public interface ApprovalApplyRecordDAO  extends BaseDAO<ApprovalApplyRecord>{
	/**
	 * 根据条件统计审批记录
	 * @param paramsObejct
	 * @return
	 */
	public long countBy(ParamsObject paramsObejct);

	/**
	 * 根据条件查询审批记录
	 * @param paramsObejct
	 * @return
	 */
	public List<ApprovalApplyRecord> findAll(ParamsObject paramsObejct,String sort, Integer start, Integer limit);
	
	/**
	 * 根据申请Id查询所有审批记录（非未来节点）
	 * @param approvalId
	 * @param isDone 是否仅包含已操作节点
	 * @param isCreator 是否包含申请节点
	 * @return
	 */
	public List<ApprovalApplyRecord> findAllByApprovalId(String approvalId,boolean isDone,boolean isCreator);

	/**
	 * 根据id查找审批记录
	 * @param id
	 * @return
	 */
	public ApprovalApplyRecord findOneById(String id);

	/**
	 * 根据approvalId获取所有的审批节点
	 * @param aid
	 * @return
	 */
	List<ApprovalApplyRecord> findAllWithoutStatus(String aid);
	
	/**
	 * 根据申请id 删除全部审批记录
	 * @param id
	 */
	public void deleteByApprovalId(String id);
}