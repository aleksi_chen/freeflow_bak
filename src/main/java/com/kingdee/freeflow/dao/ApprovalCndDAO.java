package com.kingdee.freeflow.dao;

import java.util.List;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.domain.ApprovalCnd;

public interface ApprovalCndDAO extends BaseDAO<ApprovalCnd>{
	/**
	 * 通过规则id 查询条件
	 * @param templateId
	 * @return
	 */
	List<ApprovalCnd> findAllCndsByGroupId(String groupId);
	
	/**
	 * 通过规则id 查询条件
	 * @param templateId
	 * @return
	 */
	List<ApprovalCnd> findAllCndsByGroupIds(List<String> groupIds);
	
	/**
	 * 批量添加审批条件
	 * @param cnds
	 * @return
	 */
	void addBatch(List<ApprovalCnd> cnds);
	
	/**
	 * 停用审批条件
	 * @param templateId
	 * @return
	 */
	void disableCnds(List<String> groupIds);
	/**
	 * 删除审批条件
	 * @param templateId
	 * @return
	 */
	void removeCnds(List<String> groupIds);
}
