package com.kingdee.freeflow.dao;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.domain.ApproveNotifyThrid;
import com.kingdee.freeflow.enums.ApprovalNotifyType;

public interface ApproveNotifyThridDAO extends BaseDAO<ApproveNotifyThrid>{
	/**
	 * 通过记录id 和返回状态查询通知记录是否存在
	 */
	boolean isDoneByRecordId(String recordId,ApprovalNotifyType type);
	
	/**
	 * 通过记录id 查询最后一次通知
	 */
	ApproveNotifyThrid findLastByRecordId(String recordId,ApprovalNotifyType type);
}
