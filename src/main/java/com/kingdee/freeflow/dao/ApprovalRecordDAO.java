package com.kingdee.freeflow.dao;

import java.util.List;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.domain.ApprovalRecord;

/**
 * 审批记录
 * @author kingdee
 *
 */
public interface ApprovalRecordDAO extends BaseDAO <ApprovalRecord>
{

	/**
	 * 查询所有旧审批记录
	 * @return
	 */
	List<ApprovalRecord> findAlls(int page,int limit);
	
	/**
	 * 根据formId查询所有审批记录
	 * @param formId
	 * @return
	 */
	List<ApprovalRecord> findAllByFormId(String formId,Integer start,Integer limit,String order);
	
	long count();
}
