package com.kingdee.freeflow.dao;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.domain.ApprovalRule;

public interface ApprovalRuleDAO extends BaseDAO<ApprovalRule>
{
	/**
	 * 通过模板id查询 审批规则
	 * @param templateId
	 * @return
	 */
	ApprovalRule getRuleByFid(String templateId);
	/**
	 * 通过模板id删除 审批规则
	 * @param templateId
	 * @return
	 */
	void deleteRuleByFid(String templateId);
}
