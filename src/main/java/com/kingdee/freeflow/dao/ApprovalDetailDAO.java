package com.kingdee.freeflow.dao;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.domain.ApprovalDetail;

public interface ApprovalDetailDAO extends BaseDAO <ApprovalDetail>
{

	void update(String id,boolean isDelete);
	
}
