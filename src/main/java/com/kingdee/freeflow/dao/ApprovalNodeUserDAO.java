package com.kingdee.freeflow.dao;

import java.util.List;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.domain.ApprovalNodeUser;

public interface ApprovalNodeUserDAO extends BaseDAO<ApprovalNodeUser>{
	/**
	 * 根据申请Id统计审批节点信息
	 * @return
	 */
	long countWaitByApprovalId(String approvalId);	
	/**
	 * 根据申请Id获得所有审批人环节信息
	 * @return
	 */
	List<ApprovalNodeUser> getAllApprovalNodeUsersByApprovalId(String approvalId,int start,int limit);
	/**
	 * 获得最短路径下的审批节点人员信息
	 * @return
	 */
	ApprovalNodeUser getNearestApprovalNodeUserBy(String approvalId,String oid);
	/**
	 * 获得下一个审批人
	 * @return
	 */
	List<ApprovalNodeUser> getNextApprovalNodeUserByApprovalId(String approvalId);
	/**
	 * 获得最后一个已处理人信息
	 * @return
	 */
	ApprovalNodeUser getLastApprovalNodeUserBy(String approvalId);
	/**
	 * 获得审批节点最大顺序
	 * @return
	 */
	int getMaxNumberByApprovalId(String approvalId);
	/**
	 * 批量添加审批人环节
	 * @param users
	 */
	void addBatch(List<ApprovalNodeUser> users);
	/**
	 * 更新index（包括自身）之后的所有节点顺序+1
	 * @param users
	 */
	void addAfterNumber(String aid,int index);
	
	/**
	 * 获得指定部门级别的已处理人部门负责人信息
	 * @param approvalId:审批申请单id
	 * @param deptLevel：部门级别
	 * @return
	 */
	ApprovalNodeUser getLastApprovalNodeUserOfDeptRole(String approvalId,int deptLevel);
}
