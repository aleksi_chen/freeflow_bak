package com.kingdee.freeflow.dao;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.domain.ReceiptNo;

public interface ReceiptNoDAO extends BaseDAO<ReceiptNo>
{

	ReceiptNo findByCondition(ParamsObject paramsObject);
	
	void update(ReceiptNo receiptNo);
	
	
}
