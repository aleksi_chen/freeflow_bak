package com.kingdee.freeflow.dao;

import java.util.List;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.domain.ApprovalCndGroup;

public interface ApprovalCndGroupDAO extends BaseDAO<ApprovalCndGroup>{
	/**
	 * 通过规则id 统计条件组数
	 * @param ruleId
	 * @return
	 */
	long countBy(String ruleId);
	
	/**
	 * 通过规则id 查询条件组(分页)
	 * @param templateId
	 * @return
	 */
	List<ApprovalCndGroup> findCndGroupsByRuleId(String ruleId,int start,int limit);
	
	/**
	 * 通过规则id 查询条件
	 * @param templateId
	 * @return
	 */
	List<ApprovalCndGroup> findAllCndGroupsByRuleId(String ruleId,boolean isOnlyEnabled);
	
	/**
	 * 批量添加审批条件组
	 * @param templateId
	 * @return
	 */
	void addBatch(List<ApprovalCndGroup> groups);
	
	/**
	 * 停用审批条件
	 * @param templateId
	 * @return
	 */
	void disableGroups(String ruleId);
	/**
	 * 删除审批条件组
	 * @param templateId
	 * @return
	 */
	void removeGroups(String ruleId);
}
