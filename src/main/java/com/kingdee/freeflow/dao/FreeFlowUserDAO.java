package com.kingdee.freeflow.dao;



import java.util.List;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.domain.FreeFlowUser;

public interface FreeFlowUserDAO extends BaseDAO <FreeFlowUser> 
{
	
	List<FreeFlowUser> findAlls(int page,int limit);
	
	long count();
	
}
