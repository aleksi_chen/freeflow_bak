package com.kingdee.freeflow.dao;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.domain.ApprovalDiscussGroup;

public interface ApprovalDiscussGroupDAO extends BaseDAO<ApprovalDiscussGroup>
{
	void save(ApprovalDiscussGroup approvalDiscussGroup);
}
