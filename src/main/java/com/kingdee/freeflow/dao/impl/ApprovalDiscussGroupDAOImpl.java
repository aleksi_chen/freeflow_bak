
package com.kingdee.freeflow.dao.impl;

import org.springframework.stereotype.Repository;

import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.dao.ApprovalDiscussGroupDAO;
import com.kingdee.freeflow.domain.ApprovalDiscussGroup;


@Repository
public class ApprovalDiscussGroupDAOImpl extends BaseDAOImpl<ApprovalDiscussGroup> implements ApprovalDiscussGroupDAO
{

}
