/**
 * @Date 2016年7月14日
 * @author S_Autumn
 *
 */
package com.kingdee.freeflow.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.kingdee.cbos.core.framework.dal.CriteriaQuery;
import com.kingdee.cbos.core.framework.dal.Results;
import com.kingdee.cbos.core.framework.dal.Updates;
import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.dao.ApprovalNodeUserDAO;
import com.kingdee.freeflow.domain.ApprovalNodeUser;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;

/**
 * @Date 2016年7月14日
 * @author S_Autumn
 */
@Repository
public class ApprovalNodeUserDAOImpl extends BaseDAOImpl<ApprovalNodeUser> implements ApprovalNodeUserDAO{

	@Override
	public long countWaitByApprovalId(String aid) {
		if(StringUtils.isEmpty(aid)) return 0;
		CriteriaQuery<ApprovalNodeUser> query = getQuery();
		query.field("approvalId").equal(aid);
		query.field("isDelete").equal(false);
		query.field("isDone").equal(false);
		query.readPreference(ReadPreference.secondaryPreferred());
		return getDAO().count(query);
	}
	
	@Override
	public List<ApprovalNodeUser> getAllApprovalNodeUsersByApprovalId(String aid, int start,int limit) {
		if(StringUtils.isEmpty(aid)) return null;
		CriteriaQuery<ApprovalNodeUser> query = getQuery();
		query.field("approvalId").equal(aid);
		query.field("isDelete").equal(false);
		if(start>0&&limit>0) query.offset((start-1)*limit).limit(limit);
		query.order("number,createTime");
		query.readPreference(ReadPreference.secondaryPreferred());
		return getDAO().query(query).asList();
	}
	
	@Override
	public ApprovalNodeUser getNearestApprovalNodeUserBy(String aid,String oid) {
		if(StringUtils.isEmpty(aid)||StringUtils.isEmpty(oid)) return null;
		CriteriaQuery<ApprovalNodeUser> query = getQuery();
		query.field("approvalId").equal(aid);
		query.field("oId").equal(oid);
		query.field("isDone").equal(false);
		query.field("isDelete").equal(false);
		query.offset(0).limit(1);
		query.order("number,createTime");
		query.readPreference(ReadPreference.secondaryPreferred());
		Results<ApprovalNodeUser> result = getDAO().query(query);
		return result.hasNext()?result.next():null;
	}
	
	@Override
	public List<ApprovalNodeUser> getNextApprovalNodeUserByApprovalId(String aid) {
		if(StringUtils.isEmpty(aid)) return null;
		CriteriaQuery<ApprovalNodeUser> query = getQuery();
		query.field("approvalId").equal(aid);
		query.field("isDone").equal(false);
		query.field("isDelete").equal(false);
		query.offset(0).limit(2);
		query.order("number,createTime");
		query.readPreference(ReadPreference.secondaryPreferred());
		return getDAO().query(query).asList();
	}
	
	@Override
	public ApprovalNodeUser getLastApprovalNodeUserBy(String aid) {
		if(StringUtils.isEmpty(aid)) return null;
		CriteriaQuery<ApprovalNodeUser> query = getQuery();
		query.field("approvalId").equal(aid);
		query.field("isDone").equal(true);
		query.field("isDelete").equal(false);
		query.offset(0).limit(1);
		query.order("-number,-createTime");
		query.readPreference(ReadPreference.secondaryPreferred());
		Results<ApprovalNodeUser> result = getDAO().query(query);
		return result.hasNext()?result.next():null;
	}

	@Override
	public int getMaxNumberByApprovalId(String approvalId) {
		CriteriaQuery<ApprovalNodeUser> query = getQuery();
		query.field("approvalId").equal(approvalId);
		query.field("isDelete").equal(false);
		query.offset(0).limit(1);
		query.order("-number,-createTime");
		query.readPreference(ReadPreference.secondaryPreferred());
		Results<ApprovalNodeUser> result = getDAO().query(query);
		ApprovalNodeUser node = result.hasNext()?result.next():null;
		if(node==null) return 0;
		return node.getNumber();
	}
	
	@Override
	public void addBatch(List<ApprovalNodeUser> users) {
		getDAO().batchAddNew(users);
	}
	
	@Override
	public void addAfterNumber(String aid,int index) {
		if(StringUtils.isEmpty(aid)) return;
		CriteriaQuery<ApprovalNodeUser> query = getQuery();
		query.field("approvalId").equal(aid);
		query.field("number").greaterThanOrEq(index);
		Updates updates = getUpdates();
		updates.inc("number", 1);
		dao.update(query, updates, false, WriteConcern.NORMAL);
	}

	@Override
	public ApprovalNodeUser getLastApprovalNodeUserOfDeptRole(
			String approvalId, int deptLevel)
	{
		if(StringUtils.isEmpty(approvalId) || StringUtils.isEmpty(deptLevel))
			return null;
		
		CriteriaQuery<ApprovalNodeUser> query = getQuery();
		query.field("approvalId").equal(approvalId);
		query.field("isDone").equal(true);
		query.field("isDeptRoleType").equal(true);
		query.field("deptLevel").equal(deptLevel);
		query.field("isDelete").equal(false);
		query.offset(0).limit(1);
		query.order("-number,-createTime");
		query.readPreference(ReadPreference.secondaryPreferred());
		Results<ApprovalNodeUser> result = getDAO().query(query);
		return result.hasNext()?result.next():null;
		
	}
}
