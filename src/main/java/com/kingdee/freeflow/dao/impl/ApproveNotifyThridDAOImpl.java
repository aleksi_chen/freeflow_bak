/**
 * @Date 2016年7月14日
 * @author S_Autumn
 *
 */
package com.kingdee.freeflow.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.kingdee.cbos.core.framework.dal.CriteriaQuery;
import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.dao.ApproveNotifyThridDAO;
import com.kingdee.freeflow.domain.ApproveNotifyThrid;
import com.kingdee.freeflow.enums.ApprovalNotifyStatus;
import com.kingdee.freeflow.enums.ApprovalNotifyType;
import com.mongodb.ReadPreference;

/**
 * @Date 2016年7月29日
 * @author S_Autumn
 */
@Repository
public class ApproveNotifyThridDAOImpl extends BaseDAOImpl<ApproveNotifyThrid> implements ApproveNotifyThridDAO{

	@Override
	public boolean isDoneByRecordId(String recordId,ApprovalNotifyType type) {
		if(StringUtils.isEmpty(recordId)) return false;
		CriteriaQuery<ApproveNotifyThrid> query = getQuery();
		query.field("recordId").equal(recordId);
		query.field("status").equal(ApprovalNotifyStatus.FINISH.getContext());
		if(type!=null) query.field("type").equal(type.getContext());
		query.readPreference(ReadPreference.secondaryPreferred());
		ApproveNotifyThrid o=getDAO().findOne(query);
		return o==null?false:true;
	}

	@Override
	public ApproveNotifyThrid findLastByRecordId(String recordId,ApprovalNotifyType type) {
		if(StringUtils.isEmpty(recordId)) return null;
		CriteriaQuery<ApproveNotifyThrid> query = getQuery();
		query.field("recordId").equal(recordId);
		if(type!=null)  query.field("type").equal(type.getContext());
		query.order("-time");
		query.offset(0).limit(1);
		query.readPreference(ReadPreference.secondaryPreferred());
		List<ApproveNotifyThrid> o = getDAO().query(query).asList();
		if(o==null||o.isEmpty()) return null;
		return o.get(0);
	}
}
