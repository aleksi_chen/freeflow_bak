package com.kingdee.freeflow.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.kingdee.cbos.core.framework.dal.CriteriaQuery;
import com.kingdee.cbos.core.framework.dal.Results;
import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.dao.ApprovalApplyDAO;
import com.kingdee.freeflow.domain.ApprovalApply;
import com.kingdee.sns.lightapp.util.DateUtils;
import com.mongodb.ReadPreference;

@Repository
public class ApprovalApplyDAOImpl extends BaseDAOImpl<ApprovalApply> implements ApprovalApplyDAO{

	@Override
	public long count(ParamsObject paramsObject){
		CriteriaQuery<ApprovalApply> query=processParam(paramsObject);
		query.readPreference(ReadPreference.secondaryPreferred());
		return getDAO().count(query);
	}

	@Override
	public List<ApprovalApply> findAll(ParamsObject paramsObject, String sort, Integer start, Integer limit){
		CriteriaQuery<ApprovalApply> query =processParam(paramsObject);
		if(StringUtils.isNotBlank(sort)) query.order(sort);
		if(start>0&&limit>0)query.offset((start-1)*limit).limit(limit);
		return getDAO().query(query).asList();
	}

	@Override
	public List<ApprovalApply> findApprovalApplys(String netWorkId, String userId) {
		if(StringUtils.isEmpty(netWorkId)||StringUtils.isEmpty(userId)) return null;
		CriteriaQuery<ApprovalApply> query = getQuery();
		query.field("createUser.networkId").equal(netWorkId);
		query.field("createUser.userId").equal(userId);
		query.order("-createTime");
		query.readPreference(ReadPreference.secondaryPreferred());
		return super.getDAO().query(query).asList();
	}

	@Override
	public ApprovalApply findOneById(String id){
		CriteriaQuery<ApprovalApply> query = getQuery();
		query.field("_id").equal(id);
		query.offset(0).limit(1);
		query.readPreference(ReadPreference.secondaryPreferred());
		Results<ApprovalApply> result = getDAO().query(query);
		return result.hasNext()?result.next():null;
	}

	@Override
	public ApprovalApply findByReceiptId(String receiptId){
		if(!StringUtils.isEmpty(receiptId)){
			CriteriaQuery<ApprovalApply> query=getQuery();
			query.filter("receiptId =", receiptId);
			query.readPreference(ReadPreference.secondaryPreferred());
			Results<ApprovalApply> result=getDAO().query(query);
			return result.hasNext()?result.next():null;
		}
		return null;
	}

	@Override
	public ApprovalApply findLastFlow(String approvalId, String netWorkId, String userId) {
		if(StringUtils.isEmpty(netWorkId)||StringUtils.isEmpty(userId)) return null;
		CriteriaQuery<ApprovalApply> query = getQuery();
		query.field("createUser.networkId").equal(netWorkId);
		query.field("createUser.userId").equal(userId);
		if(!StringUtils.isBlank(approvalId)) query.filter("_id !=",approvalId);
		query.order("-createTime");
		query.offset(0).limit(1);
		query.readPreference(ReadPreference.secondaryPreferred());
		Results<ApprovalApply> result = getDAO().query(query);
		return result.hasNext()?result.next():null;
	}

	private CriteriaQuery<ApprovalApply>  processParam(ParamsObject paramsObject){
		CriteriaQuery<ApprovalApply> query=getQuery();
		if(paramsObject!=null){
			if(paramsObject.getUser()!=null && !StringUtils.isEmpty(paramsObject.getUser().getNetworkId())){
				query.field("createUser.networkId").equal(paramsObject.getUser().getNetworkId());
			}
			if(paramsObject.getUser()!=null && !StringUtils.isEmpty(paramsObject.getUser().getUserId())){
				query.field("createUser.userId").equal(paramsObject.getUser().getUserId());
			}
			if (!StringUtils.isEmpty(paramsObject.getStatus())) {
				query.filter("status =", paramsObject.getStatus());
			}
			if(!StringUtils.isEmpty(paramsObject.getFormTemplateId())){
				//这里做特殊处理 由于第三方不使用我们的模板及规则所以 申请数据中没有FormTemplateId 所以第三方查询id由前端写死 当时 "third-party" 时该查询逻辑应该是查询第三方申请
				if("third-party".equals(paramsObject.getFormTemplateId())){
					query.field("isThird").equal(true);
				}else{
					query.field("formTemplateId").equal(paramsObject.getFormTemplateId());
				}
			}
			if(paramsObject.getStartTime()!=null){
				query.filter("createTime >=",new Date(paramsObject.getStartTime()));
			}
			if(paramsObject.getEndTime()!=null){
				query.filter("createTime <", DateUtils.addDay(paramsObject.getEndTime(), 1));
			}
			if(!StringUtils.isEmpty(paramsObject.getReceiptId())){
				query.field("receiptId").equal(paramsObject.getReceiptId());
			}
			if(paramsObject.getUser()!=null&&!StringUtils.isEmpty(paramsObject.getUser().getOid())){
				query.field("createUser.oid").equal(paramsObject.getUser().getOid());
			}
			if(paramsObject.getUser()!=null&&!StringUtils.isEmpty(paramsObject.getUser().getDeptId())){
				query.field("createUser.deptId").equal(paramsObject.getUser().getDeptId());
			}
		}
		query.readPreference(ReadPreference.secondaryPreferred());
		return query;
	}
}
