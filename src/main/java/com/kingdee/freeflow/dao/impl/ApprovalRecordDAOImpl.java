package com.kingdee.freeflow.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.kingdee.cbos.core.framework.dal.CriteriaQuery;
import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.dao.ApprovalRecordDAO;
import com.kingdee.freeflow.domain.ApprovalRecord;

@Repository
public class ApprovalRecordDAOImpl extends BaseDAOImpl<ApprovalRecord> implements ApprovalRecordDAO
{

	@Override
	public List<ApprovalRecord> findAlls(int page,int limit)
	{
		CriteriaQuery<ApprovalRecord> query=getQuery();
		query.offset(page*limit).limit(limit);
		return super.getDAO().query(query).asList();
	}

	@Override
	public List<ApprovalRecord> findAllByFormId(String formId,Integer start,Integer limit,String order)
	{
		CriteriaQuery<ApprovalRecord> query=getQuery();
		if(!StringUtils.isEmpty(formId)){
			query.field("formId").equal(formId);
		}
		if(StringUtils.isNotBlank(order)){
			query.order(order);
		}
		if(start>0&&limit>0){
			query.offset((start-1)*limit).limit(limit);
		}
		return super.getDAO().query(query).asList();
	}

	@Override
	public long count() {
		return super.getDAO().count(getQuery());
	}
	

}
