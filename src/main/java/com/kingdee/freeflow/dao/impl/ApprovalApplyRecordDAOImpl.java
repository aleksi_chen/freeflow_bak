package com.kingdee.freeflow.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.kingdee.cbos.core.framework.dal.CriteriaQuery;
import com.kingdee.cbos.core.framework.dal.Results;
import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.dao.ApprovalApplyRecordDAO;
import com.kingdee.freeflow.domain.ApprovalApplyRecord;
import com.kingdee.freeflow.enums.AppovalRecordStatus;
import com.mongodb.ReadPreference;

@Repository
public class ApprovalApplyRecordDAOImpl extends BaseDAOImpl<ApprovalApplyRecord> implements ApprovalApplyRecordDAO {
	@Override
	public long countBy(ParamsObject paramsObejct) {
		CriteriaQuery<ApprovalApplyRecord> query = getQuery();
		query = createQuery(query, paramsObejct);
		if (paramsObejct != null && StringUtils.isNotBlank(paramsObejct.getApprovId())) {
			query.field("formId").equal(paramsObejct.getApprovId());
		}
		if (paramsObejct != null && StringUtils.isNotBlank(paramsObejct.getStatus())) {
			query.filter("status =", paramsObejct.getStatus());
		}
		List<String> status = new ArrayList<String>();
		status.add("-2");
		if("myApprove".equalsIgnoreCase(paramsObejct.getQueryType())) status.add("1");
		query.field("status").hasNoneOf(status);
		query.readPreference(ReadPreference.secondaryPreferred());
		return super.getDAO().count(query);
	}

	@Override
	public List<ApprovalApplyRecord> findAll(ParamsObject paramsObejct,String sort, Integer start, Integer limit) {
		CriteriaQuery<ApprovalApplyRecord> query = getQuery();
		query = createQuery(query, paramsObejct);
		if (paramsObejct != null && StringUtils.isNotBlank(paramsObejct.getApprovId())) {
			query.field("formId").equal(paramsObejct.getApprovId());
		}
		if (paramsObejct != null && !StringUtils.isBlank(paramsObejct.getStatus())) {
			query.filter("status =", paramsObejct.getStatus());
		}
		if (StringUtils.isNotBlank(sort)) {
			query.order(sort);
		}
		if (!(start == null || start <= 0 || limit <= 0)) {
			query.offset((start - 1) * limit).limit(limit);
		}
		List<String> status = new ArrayList<String>();
		status.add("-2");
		if("myApprove".equalsIgnoreCase(paramsObejct.getQueryType())) status.add("1");
		query.field("status").hasNoneOf(status);
		query.readPreference(ReadPreference.secondaryPreferred());
		return getDAO().query(query).asList();
	}

	@Override
	public List<ApprovalApplyRecord> findAllByApprovalId(String aid,boolean isDone, boolean isCreator) {
		if (StringUtils.isEmpty(aid)) return null;
		CriteriaQuery<ApprovalApplyRecord> query = getQuery();
		query.field("formId").equal(aid);
		if (isDone) {
			if (isCreator) {
				List<String> status = new ArrayList<String>();
				status.add("0");
				status.add("1");
				status.add("2");
				status.add("3");
				status.add("4");
				status.add("5");
				query.field("status").hasAnyOf(status);
			} else {
				List<String> status = new ArrayList<String>();
				status.add("0");
				status.add("2");
				status.add("3");
				status.add("4");
				status.add("5");
				query.field("status").hasAnyOf(status);
			}
		} else {
			if (isCreator) {
				query.filter("status !=",AppovalRecordStatus.FUTURE.getContext());
			} else {
				List<String> status = new ArrayList<String>();
				status.add("-2");
				status.add("1");
				query.field("status").hasNoneOf(status);
			}
		}
		query.order("createTime,approvalTime");
		query.readPreference(ReadPreference.secondaryPreferred());
		return getDAO().query(query).asList();
	}

	@Override
	public ApprovalApplyRecord findOneById(String id) {
		if (StringUtils.isEmpty(id))
			return null;
		CriteriaQuery<ApprovalApplyRecord> query = getQuery();
		query.field("_id").equal(id);
		query.offset(0).limit(1);
		query.readPreference(ReadPreference.secondaryPreferred());
		Results<ApprovalApplyRecord> result = getDAO().query(query);
		return result.hasNext() ? result.next() : null;
	}

	@Override
	public List<ApprovalApplyRecord> findAllWithoutStatus(String aid) {
		if (StringUtils.isEmpty(aid)) return null;
		CriteriaQuery<ApprovalApplyRecord> query = getQuery();
		query.field("formId").equal(aid);
		query.readPreference(ReadPreference.secondaryPreferred());
		return getDAO().query(query).asList();
	}

	@Override
	public void deleteByApprovalId(String id) {
		if (StringUtils.isEmpty(id)) return;
		CriteriaQuery<ApprovalApplyRecord> query = getQuery();
		query.field("formId").equal(id);
		dao.delete(query);
	}
	
	private CriteriaQuery<ApprovalApplyRecord> createQuery(CriteriaQuery<ApprovalApplyRecord> query, ParamsObject paramsObejct) {
		if(paramsObejct==null) return query;
		if (paramsObejct.getUser() != null && StringUtils.isNotBlank(paramsObejct.getUser().getNetworkId())) {
			query.field("approvalUser.networkId").equal(paramsObejct.getUser().getNetworkId());
		}
		if (paramsObejct.getUser() != null && StringUtils.isNotBlank(paramsObejct.getUser().getUserId())) {
			query.field("approvalUser.userId").equal(paramsObejct.getUser().getUserId());
		}
		if (StringUtils.isNotBlank(paramsObejct.getFormTemplateId())) {
			query.field("formTemplateId").equal(paramsObejct.getFormTemplateId());
		}
		if (StringUtils.isNotBlank(paramsObejct.getTransferApproverOId())) {
			query.field("transferApproverOId").equal(paramsObejct.getTransferApproverOId());
		}
		return query;
	}
}