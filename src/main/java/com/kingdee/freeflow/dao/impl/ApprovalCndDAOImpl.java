/**
 * @Date 2016年7月14日
 * @author S_Autumn
 *
 */
package com.kingdee.freeflow.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.kingdee.cbos.core.framework.dal.CriteriaQuery;
import com.kingdee.cbos.core.framework.dal.Updates;
import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.dao.ApprovalCndDAO;
import com.kingdee.freeflow.domain.ApprovalCnd;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;

/**
 * @Date 2016年7月14日
 * @author S_Autumn
 */
@Repository
public class ApprovalCndDAOImpl extends BaseDAOImpl<ApprovalCnd> implements ApprovalCndDAO{

	@Override
	public List<ApprovalCnd> findAllCndsByGroupId(String groupId) {
		if(StringUtils.isEmpty(groupId)) return null;
		CriteriaQuery<ApprovalCnd> query = getQuery();
		query.field("groupId").equal(groupId);
		query.field("status").equal(1);
		query.order("-groupId,-createTime");
		query.readPreference(ReadPreference.secondaryPreferred());
		return getDAO().query(query).asList();
	}

	@Override
	public List<ApprovalCnd> findAllCndsByGroupIds(List<String> groupIds) {
		if(groupIds==null||groupIds.isEmpty()) return null;
		CriteriaQuery<ApprovalCnd> query = getQuery();
		query.field("groupId").hasAnyOf(groupIds);
		query.field("status").equal(1);
		query.order("-groupId,-createTime");
		query.readPreference(ReadPreference.secondaryPreferred());
		return getDAO().query(query).asList();
	}
	
	@Override
	public void addBatch(List<ApprovalCnd> cnds) {
		getDAO().batchAddNew(cnds);
	}
	
	@Override
	public void disableCnds(List<String> groupIds) {
		if(groupIds==null||groupIds.isEmpty()) return;
		CriteriaQuery<ApprovalCnd> query = getQuery();
		query.field("groupId").hasAnyOf(groupIds);
		query.field("status").equal(1);
		Updates update =  getUpdates();
		update.set("status", 2);
		dao.update(query, update, false, WriteConcern.NORMAL);
	}

	@Override
	public void removeCnds(List<String> groupIds) {
		if(groupIds==null||groupIds.isEmpty()) return;
		CriteriaQuery<ApprovalCnd> query = getQuery();
		query.field("groupId").hasAnyOf(groupIds);
		dao.delete(query);
	}
}
