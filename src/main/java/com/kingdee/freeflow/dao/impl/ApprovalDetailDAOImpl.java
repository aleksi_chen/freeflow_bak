package com.kingdee.freeflow.dao.impl;

import org.springframework.stereotype.Repository;

import com.kingdee.cbos.core.framework.dal.CriteriaQuery;
import com.kingdee.cbos.core.framework.dal.Updates;
import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.dao.ApprovalDetailDAO;
import com.kingdee.freeflow.domain.ApprovalDetail;
import com.mongodb.WriteConcern;

@Repository
public class ApprovalDetailDAOImpl extends BaseDAOImpl<ApprovalDetail> implements ApprovalDetailDAO
{

	@Override
	public void update(String id,boolean isDelete)
	{
		CriteriaQuery<ApprovalDetail> query=getQuery();
		query.field("approvalId").equal(id);
		Updates update=getUpdates(); 
		update.set("isDelete",isDelete);
		dao.update(query, update,false,WriteConcern.NORMAL);
	}
	
}
