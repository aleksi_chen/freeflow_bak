package com.kingdee.freeflow.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.kingdee.cbos.core.framework.dal.CriteriaQuery;
import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.dao.FreeFlowUserDAO;
import com.kingdee.freeflow.domain.FreeFlowUser;

@Repository
public class FreeFlowUserDAOImpl extends BaseDAOImpl<FreeFlowUser> implements FreeFlowUserDAO
{

	@Override
	public List<FreeFlowUser> findAlls(int page,int limit)
	{
		CriteriaQuery<FreeFlowUser> query=getQuery();
		query.offset(page*limit).limit(limit);
		return super.getDAO().query(query).asList();
	}

	@Override
	public long count() {
		return super.getDAO().count(getQuery());
	}
}
