package com.kingdee.freeflow.dao.impl;

import org.springframework.stereotype.Repository;

import com.kingdee.cbos.core.framework.dal.CriteriaQuery;
import com.kingdee.cbos.core.framework.dal.Results;
import com.kingdee.cbos.core.framework.dal.Updates;
import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.dao.ReceiptNoDAO;
import com.kingdee.freeflow.domain.ReceiptNo;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;



@Repository
public class ReceiptNoDAOImpl extends BaseDAOImpl<ReceiptNo> implements ReceiptNoDAO
{

	@Override
	public ReceiptNo findByCondition(ParamsObject paramsObject)
	{
		CriteriaQuery<ReceiptNo> query=getQuery();
		query.field("eId").equal(paramsObject.getUser().getEid());
		query.field("networkId").equal(paramsObject.getUser().getNetworkId());
		query.field("currentDay").equal(paramsObject.getStartTimeStr());
		query.readPreference(ReadPreference.secondaryPreferred());
		Results<ReceiptNo> result=getDAO().query(query);
		return result.hasNext()?result.next():null;
	}

	@Override
	public void update(ReceiptNo receiptNo)
	{
		CriteriaQuery<ReceiptNo> query=getQuery();
		query.field("eId").equal(receiptNo.geteId());
		query.field("networkId").equal(receiptNo.getNetworkId());
		query.field("currentDay").equal(receiptNo.getCurrentDay());
		Updates update=getUpdates(); 
		update.set("sequence", receiptNo.getSequence());
		dao.update(query, update,false,WriteConcern.NORMAL);
	}

	
}
