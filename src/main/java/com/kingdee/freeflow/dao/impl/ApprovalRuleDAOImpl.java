/**
 * @Date 2016年3月11日
 * @Desc 
 *
 */
package com.kingdee.freeflow.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.kingdee.cbos.core.framework.dal.CriteriaQuery;
import com.kingdee.cbos.core.framework.dal.Results;
import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.dao.ApprovalRuleDAO;
import com.kingdee.freeflow.domain.ApprovalRule;
import com.mongodb.ReadPreference;

/**
 * Desc:
 * @Date 2016年3月11日
 */
@Repository
public class ApprovalRuleDAOImpl extends BaseDAOImpl<ApprovalRule> implements ApprovalRuleDAO{
	
	@Override
	public ApprovalRule getRuleByFid(String templateId){
		if(StringUtils.isEmpty(templateId)) return null;
		CriteriaQuery<ApprovalRule> query = super.getQuery();
		query.filter("templateId", templateId);
		query.readPreference(ReadPreference.secondaryPreferred());
		Results<ApprovalRule> rs = getDAO().query(query);
		return rs.hasNext() ? rs.next() : null;
	}

	@Override
	public void deleteRuleByFid(String templateId){
		if(StringUtils.isEmpty(templateId)) return;
		CriteriaQuery<ApprovalRule> query = getQuery();
		query.filter("templateId",templateId);
		getDAO().delete(query);
	}
}
