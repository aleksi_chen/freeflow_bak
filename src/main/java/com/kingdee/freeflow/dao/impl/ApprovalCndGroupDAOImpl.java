/**
 * @Date 2016年7月19日
 * @author S_Autumn
 */
package com.kingdee.freeflow.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.kingdee.cbos.core.framework.dal.CriteriaQuery;
import com.kingdee.cbos.core.framework.dal.Updates;
import com.kingdee.cbos.core.framework.dal.impl.BaseDAOImpl;
import com.kingdee.freeflow.dao.ApprovalCndGroupDAO;
import com.kingdee.freeflow.domain.ApprovalCndGroup;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;

@Repository
public class ApprovalCndGroupDAOImpl extends BaseDAOImpl<ApprovalCndGroup> implements ApprovalCndGroupDAO{


	@Override
	public long countBy(String ruleId) {
		if(StringUtils.isEmpty(ruleId)) return 0;
		CriteriaQuery<ApprovalCndGroup> query = getCndGroupsQuery(ruleId);
		return getDAO().count(query);
	}
	
	@Override
	public List<ApprovalCndGroup> findCndGroupsByRuleId(String ruleId, int start,int limit) {
		if(StringUtils.isEmpty(ruleId)) return null;
		CriteriaQuery<ApprovalCndGroup> query = getCndGroupsQuery(ruleId);
		if(start>0&&limit>0) query.offset((start-1)*limit).limit(limit);
		query.order("-number,-createTime");
		return getDAO().query(query).asList();
	}
	
	
	@Override
	public List<ApprovalCndGroup> findAllCndGroupsByRuleId(String ruleId,boolean isOnlyEnabled) {
		if(StringUtils.isEmpty(ruleId)) return null;
		CriteriaQuery<ApprovalCndGroup> query = getQuery();
		query.field("approvalRuleId").equal(ruleId);
		if(isOnlyEnabled) query.field("status").equals(1);
		query.order("number,createTime");
		query.readPreference(ReadPreference.secondaryPreferred());
		return getDAO().query(query).asList();
	}
	
	@Override
	public void addBatch(List<ApprovalCndGroup> cnds) {
		getDAO().batchAddNew(cnds);
	}

	@Override
	public void disableGroups(String ruleId) {
		if(StringUtils.isEmpty(ruleId)) return;
		CriteriaQuery<ApprovalCndGroup> query = getQuery();
		query.field("approvalRuleId").equal(ruleId);
		query.field("status").equal(1);
		Updates update =  getUpdates();
		update.set("status", 2);
		dao.update(query, update, false, WriteConcern.NORMAL);
	}
	
	private CriteriaQuery<ApprovalCndGroup> getCndGroupsQuery(String ruleId){
		CriteriaQuery<ApprovalCndGroup> query = getQuery();
		query.field("approvalRuleId").equal(ruleId);
		query.field("status").equal(1);
		query.readPreference(ReadPreference.secondaryPreferred());
		return query;
	}

	@Override
	public void removeGroups(String ruleId) {
		if(StringUtils.isEmpty(ruleId)) return;
		CriteriaQuery<ApprovalCndGroup> query = getQuery();
		query.field("approvalRuleId").equal(ruleId);
		dao.delete(query);
	}
}
