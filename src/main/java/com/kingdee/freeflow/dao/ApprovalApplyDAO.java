package com.kingdee.freeflow.dao;

import java.util.List;

import com.kingdee.cbos.core.framework.dal.BaseDAO;
import com.kingdee.freeflow.common.queries.ParamsObject;
import com.kingdee.freeflow.domain.ApprovalApply;

public interface ApprovalApplyDAO extends BaseDAO <ApprovalApply> {
	/**
	 * 查询条数
	 * @param networkId
	 * @param creatorUserId
	 * @param status
	 * @return
	 */
	long count(ParamsObject paramsObejct);
	/**
	 * 查询所有记录
	 * @param networkId
	 * @param creatorUserId
	 * @param sort
	 * @param start
	 * @param limit
	 * @param status
	 * @return
	 */
	List<ApprovalApply> findAll(ParamsObject paramsObejct,String sort,Integer start,Integer limit);
	/**
	 * 根据工作区和用户id查询所有审批信息
	 * @param netWorkId
	 * @param userId
	 * @return
	 */
	List<ApprovalApply> findApprovalApplys(String netWorkId, String userId);
	/**
	 * 获得审批申请数据
	 * @param id
	 * @return
	 */
	ApprovalApply findOneById(String id);
	/**
	 * 根据单据查询记录
	 */
	ApprovalApply findByReceiptId(String receiptId);
	
	/**
	 * 查询最近一次非指定审批的审批信息
	 * @param approvalId
	 * @param netWorkId
	 * @param userId
	 * @return
	 */
	ApprovalApply findLastFlow(String approvalId, String netWorkId, String userId); 
}
