var express = require('express')
var webpack = require('webpack')
var config = require('./webpack.dev.conf')
var opn = require('opn');
var app = express()
var compiler = webpack(config)

var api = require('../src/mock')

var getToken = require('./token')
//mock api
app.get('/api/v1/account', api.account)
app.get('/api/v1/approvals', api.myApproval)
app.get('/api/v1/approval/:id', api.approvalDetail)
app.get('/api/v1/approvalRecord', api.approvalRecord)
app.post('/api/v1/approvalRecord', api.createApprovalRecord)
app.put('/api/v1/approvalRecord', api.updateApprovalRecord)
app.get('/api/v1/requests', api.myRequest)
app.get('/api/v1/approvaltpl', api.approvaltpl)
app.get('/api/v1/template', api.template)

var devMiddleware = require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath,
  stats: {
    colors: true,
    chunks: false
  }
})

var hotMiddleware = require('webpack-hot-middleware')(compiler)
// force page reload when html-webpack-plugin template changes
compiler.plugin('compilation', function (compilation) {
  compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
    hotMiddleware.publish({ action: 'reload' })
    cb()
  })
})

// handle fallback for HTML5 history API
app.use(require('connect-history-api-fallback')())
// serve webpack bundle output
app.use(devMiddleware)
// enable hot-reload and state-preserving
// compilation error display
app.use(hotMiddleware)
// serve pure static assets
app.use('/static', express.static('./static'))

//获取token

var port = 8081
new Promise(function(resolve, reject){
    app.listen(port, function (err) {
        if (err) {
            console.log(err)
            return reject(err)
        }
        resolve()
    })
}).then(getToken)
.then(token => {
        console.log(`Listening at http://localhost:${port}?lappName=freeflow&webLappToken=${token}`)
        opn(`http://localhost:${port}?lappName=freeflow&webLappToken=${token}`);
})
.catch(err => {
    console.log(`Listening at http://localhost:${port}`)
    opn(`http://localhost:${port}`);
})
