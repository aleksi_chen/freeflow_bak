import { updateComponentValue } from '../vuex/actions'

export default {
    vuex: {
        actions: {
            updateComponentValue: updateComponentValue
        }
    },
     watch: {
        'value': function(val){
           this.updateComponentValue({id: this.id, val: val})
        }
    }
}