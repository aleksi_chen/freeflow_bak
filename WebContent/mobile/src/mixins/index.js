import commonProps from './component-props'
import componentUpdate from './component-update-value'
import validate from './component-validate'

const mixins = {
    commonProps,
    componentUpdate,
    validate
}
module.exports = mixins