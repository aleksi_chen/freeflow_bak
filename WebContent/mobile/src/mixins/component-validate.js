import Vue from 'vue'
import helper from '../utils/helpers'

export default {
    methods: {
        validate() {
            if (!this.value) {
                helper.addErrorTips(this.$el)
                return false
            }
            return true
        }
    }
}