export default {
  props: {
        id: {
            type: String,
            required: true
        },
        label: {
            type: String,
            required: true
        },
        placeholder: {
            type: String,
            required: false,
            default: '请选择'
        },
        required: {
            type: Boolean,
            required: true,
            default: true 
        },
        type: {
            type: String,
            required: true
        }
    }
}