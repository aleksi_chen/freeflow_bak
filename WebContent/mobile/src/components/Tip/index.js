import Vue from 'vue'
import Tip from './Tip'

var defaultOptions = {
    msg: '操作成功'
}

function extend(defaultOptions, options) {
    if (typeof (options) !== 'object') {
        options = {}
    }
    for (var key in options) {
        if (options.hasOwnProperty(key)) {
            defaultOptions[key] = options[key]
        }
    }
    return defaultOptions
}
/*
function cloneRegExp(reg) {
    var pattern = reg.valueOf()
    var flags = ''
    flags += pattern.global ? 'g' : ''
    flags += pattern.ignoreCase ? 'i' : ''
    flags += pattern.multiline ? 'm' : ''
    return new RegExp(pattern.source, flags)
}*/

function install(Vue) {
    var TipConstructor = Vue.extend(Tip)
    var tipInstance = null

    Object.defineProperty(Vue.prototype, '$tip', {

        get: function () {

            return function (options) {

                return new Promise(function (resolve, reject) {
                    if (tipInstance) {
                        tipInstance.$destroy(true)
                    }
                    tipInstance = new TipConstructor({
                        el: document.createElement('div'),
                        data: extend(extend({}, defaultOptions), options)
                    })
                    tipInstance.$appendTo(document.body)
                    setTimeout(function () {
                        tipInstance.$destroy(true)
                    }, 1000)
                })
            }.bind(this)
        }
    })
}

export default Vue.use(install)