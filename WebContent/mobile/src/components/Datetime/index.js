import Vue from 'vue'
import DatetimePicker from './DatetimePicker'
import helpers from '../../utils/helpers'

var defaultOptions = {
    show: true
}

function cloneRegExp(reg) {
    var pattern = reg.valueOf()
    var flags = ''
    flags += pattern.global ? 'g' : ''
    flags += pattern.ignoreCase ? 'i' : ''
    flags += pattern.multiline ? 'm' : ''
    return new RegExp(pattern.source, flags)
}

function install(Vue) {
    var DatetimePickerConstructor = Vue.extend(DatetimePicker)
    var datetimePickerInstance = null

    Object.defineProperty(Vue.prototype, '$datetimePicker', {

        get: function () {

            return function (options) {
                return new Promise(function (resolve, reject) {
                    if (datetimePickerInstance) {
                        datetimePickerInstance.$destroy(true)
                    }
                    datetimePickerInstance = new DatetimePickerConstructor({
                        el: document.createElement('div'),
                        data: helpers.extend(helpers.extend({}, defaultOptions), options)
                    })
                    datetimePickerInstance.$on('cancel', function (data) {
                        reject('cancel')
                    })
                    datetimePickerInstance.$on('confirm', function (data) {
                        resolve(data)
                    })
                    datetimePickerInstance.$appendTo(document.body)
                })
            }.bind(this)
        }
    })
}

export default Vue.use(install)