import Tab from './Tab'
import TabItem from './TabItem'
import FlowItem from './FlowItem/FlowItem'
import ApplyType from './ApplyType/ApplyType'

//表单控件
import DateRangeField from './Cells/DateRangeField.vue'
import TextareaField from './Cells/TextareaField'
import PhotoField from './Cells/PhotoField'
//import AllotField from './Cells/AllotField'
import FreeAllotField from './Cells/AllotField/FreeAllotField'
import KeyAllotField from './Cells/AllotField/KeyAllotField'
import ConditionAllotField from './Cells/AllotField/ConditionAllotField'
import DateField from './Cells/DateField'
import NumberField from './Cells/NumberField'
import RadioField from './Cells/RadioField'
import TextField from './Cells/TextField'
import FileField from './Cells/FileField'
import FileFieldWeb from './Cells/FileFieldWeb'
import MoneyField from './Cells/MoneyField'
import DetailField from './Cells/DetailField'

//审批详情
import ApprovalDesc  from './ApprovalDesc/ApprovalDesc'
import ApprovalDescThird  from './ApprovalDesc/ApprovalDescThird'
import Timeline  from './ApprovalDesc/Timeline'
import ApprovalFooter  from './ApprovalDesc/ApprovalFooter'
import ApprovalDiscussInput  from './ApprovalDesc/ApprovalDiscussInput'
import ApprovalDiscuss  from './ApprovalDesc/ApprovalDiscuss'

import ApprovalPreview from './PhotoPreview/ApprovalPreview'
import PhotoPreview from './PhotoPreview/PhotoPreview'
import ConfirmApproval from './ConfirmApproval/ConfirmApproval'

import FooterButton from './FooterButton/FooterButton'
import PreviewItem from './Cells/PreviewItem'

import NoData from './NoData'

//公用组件
import FixTop from './FixTop'

//material design风格的button
import MDButton from './MDButton'

import ShowTips from './ShowTips'

const Components = {
	Tab,
	TabItem,
	FlowItem,
    ApplyType,
    DateRangeField,
    TextareaField,
    PhotoField,
    FreeAllotField,
    KeyAllotField,
    ConditionAllotField,
    DateField,
    NumberField,
    RadioField,
    TextField,
    FileField,
    FileFieldWeb,
    MoneyField,
    DetailField,
    ApprovalDesc,
    ApprovalDescThird,
    Timeline,
    ApprovalDiscuss,
    ApprovalDiscussInput,
    ApprovalFooter,
    PhotoPreview,
    PreviewItem,
    ApprovalPreview,
    ConfirmApproval,
    FooterButton,
    MDButton,
    FixTop,
    NoData,
    ShowTips
}

module.exports = Components
