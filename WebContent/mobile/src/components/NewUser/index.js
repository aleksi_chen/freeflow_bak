import Vue from 'vue'
import NewUser from './NewUser'
import helpers from '../../utils/helpers'

var defaultOptions = {
    showPopUp: true,
    isNeedKnowMore: false,
    knowMoreURL: '', //了解更多跳转URL
    picUrl: '', //弹出框背景图片
    title: '', //标题
    content: ['asda'], //内容列表
}

function install(Vue) {
    var PopupConstructor = Vue.extend(NewUser)
    var popupInstance = null;

    Object.defineProperty(Vue.prototype, '$newUserPopup', {

        get: function () {
            return function (options)  {
                    if(popupInstance){
                        popupInstance.$destroy(true)
                    }
                    popupInstance = new PopupConstructor({
                        el: document.createElement('div'),
                        data: helpers.extend({}, helpers.extend(defaultOptions, options))
                    });
                    popupInstance.$on('cancel', function(){
                        this.$destroy(true);
                    });
                    popupInstance.$appendTo(document.body);
            }.bind(this);
        }

    });
  		
}

export default Vue.use(install)