import Vue from 'vue'
    
function showTips(msg, delay) {
    var tips = document.getElementById('modaltips');
    if (!tips) {
        tips = document.createElement('div');
        tips.id = 'modaltips';
        tips.className = 'modaltips';
        document.body.appendChild(tips)
    }
    tips.addEventListener("webkitAnimationEnd", function () { //动画结束时事件
        this.className = 'modaltips';
        tips.style.display = 'none';
    }, false);

    this.showTips = function (msg, delay) {
        var tips = document.getElementById('modaltips');
        tips.innerHTML = msg;
        tips.style.display = 'block';
        if (this.tipsTimer) {
            clearTimeout(this.tipsTimer);
        }
        this.tipsTimer = setTimeout(function () {
            tips.className = 'modaltips fadeOutDown animated';
        }, delay || 500);
    }
    this.showTips(msg, delay);
}

function install() {
    Object.defineProperty(Vue.prototype, '$showTips', {
        get: function () {
            return showTips
        }
    });
}

export default Vue.use(install)