import vue from 'vue'
    
export default vue.directive('autoHeight', {
    bind: function(){
    },
    update: function(){
        this.el.style.height = "27px";
        this.el.style.height = (this.el.scrollHeight) + "px";
    }
})