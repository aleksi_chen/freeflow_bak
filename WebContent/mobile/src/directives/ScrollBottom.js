import vue from 'vue'


export default vue.directive('scroll-bottom', {
    params: ['throttle', 'distance'],
    throttle: 0,
    distance: 0,
    timer: null,
    checkBottom: function () {
            var self = this
            if (this.timer) {
                clearTimeout(self.timer)
            }
            self.timer = setTimeout(function() {
                var scrollTop = document.body.scrollTop || document.documentElement.scrollTop,
                    clientHeight = document.documentElement.clientHeight,
                    bodyHeight = document.body.scrollHeight;

                //滚动到底部
                if (bodyHeight - clientHeight - scrollTop < self.distance) {
                    self.vm && self.vm.$dispatch('scrollBottom')
                }
            }, self.throttle)

    },
    bind: function () {
        this.throttle = this.params.throttle || 300
        this.distance = this.params.distance || 30
        document.addEventListener('scroll', this.checkBottom.bind(this), false)
    },
    unbind: function () {
        console.log('unbind')
        document.removeEventListener('scroll', this.checkBottom, false)
    }
})