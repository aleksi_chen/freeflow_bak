import Root from './Root.vue'
import App from './App.vue'
import MyApply from './Tab/MyApply.vue'
import MyApproval from './Tab/MyApproval.vue'
import MyApplication from './MyApplication.vue'
import SelectApplyType from './SelectApplyType.vue'
import ApplicationDetail from './ApplicationDetail.vue'
import ApprovalRule from './ApprovalRule.vue'
import ApprovalDetail from './ApprovalDetail.vue'
import CustomRule from './CustomRule.vue'

const Pages = {
    Root,
    App,
    MyApply,
    MyApproval,
    MyApplication,
    SelectApplyType,
    ApplicationDetail,
    ApprovalRule,
    CustomRule,
    ApprovalDetail
}

module.exports = Pages
