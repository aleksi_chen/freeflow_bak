var ENV = {
    baseURL: 'http://192.168.22.144/',
    serverRootUrl: '/',
    moduleNname: 'freeflow',
    version: 'freeflow_3.0',
    environment: process.env.NODE_ENV,
    defaultLocale: 'zh',
    apiNameSpace: 'v1/rest',
    hostEndpoint: 'freeflow',
    fileUploadUrl: '/lightapp/c/file/upload.json',
    fileDownloadUrl: '/microblog/filesvr/',
    uploadUrl: '/microblog/filesvr/upload',
    photoPreviewUrl:"/lightapp/c/file/image/get.json?fileId=",
    routeRootPath: null
}

if (ENV.environment === 'develop') {
    ENV.baseURL = `http://192.168.22.144/freeflow/${ENV.apiNameSpace}`
    ENV.fileUploadUrl = 'http://kdtest.kdweibo.cn/lightapp/c/file/upload.json'
    ENV.fileDownloadUrl = 'http://kdtest.kdweibo.cn/microblog/filesvr/'
    ENV.assetsUrl = ''
    ENV.uploadUrl = `http://192.168.22.144/microblog/filesvr/upload?t=` + Date.now()
    ENV.serverRootUrl = 'http://192.168.22.144/',
    ENV.photoPreviewUrl = `http://kdtest.kdweibo.cn/lightapp/c/file/image/get.json?fileId=`
}
if (ENV.environment === 'staging') {
    ENV.baseURL = `http://192.168.22.144/freeflow/${ENV.apiNameSpace}`
    ENV.fileUploadUrl = 'http://kdtest.kdweibo.cn/lightapp/c/file/upload.json'
    ENV.fileDownloadUrl = 'http://kdtest.kdweibo.cn/microblog/filesvr/'
    ENV.assetsUrl = '/static'
}
if (ENV.environment === 'production') {
    ENV.baseURL = `/${ENV.hostEndpoint}/${ENV.apiNameSpace}`
    ENV.routeRootPath = `/${ENV.hostEndpoint}/m`
    ENV.assetsUrl = '/freeflow/mobile/dist/static'
}

export default ENV