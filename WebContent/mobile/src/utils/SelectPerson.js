/**
 * 封装选人桥,多端通用选人
 * author: haoyang_hu@kingdee.com
 */
import personSelect from "./personSelect"
import helper from "./helpers"

function app(options) {
    let people = [];
    let self = this;
    if ( helper.getOS() ) {
        XuntongJSBridge.call('selectPerson', { 'isMulti': options.multi },
            function (resp) {
                if ( resp.success === true || resp.success === 'true' ) {
                    if ( !resp.data ) {
                        return;
                    }
                    let persons = resp.data.persons;
                    //self.setSdkApprover(resp.data.persons[0])
                    //let personInfo = resp.data.persons[0]
                    //personInfo.oId = personInfo.personId;
                    //由于手机端与web端选人桥返回的数据结构差异,故采用这种方式实现
                    for (let i = 0; i < persons.length; i++) {
                        persons[i].oId = persons[i].personId;
                        people.push(persons[i])
                    }
                }
                options.success && options.success(people)
            }
        )
    } else {
        window.location.hash = '#selectPerson'
        var select = personSelect({
            multi: options.multi,
            onSelect: function (data, historyLen) {
                let keysArr = Object.keys(data);
                for (let i = 0; i < keysArr.length; i++) {
                    let key = keysArr[i];
                    var person = data[key];
                    person.personName = person.name;
                    person.openId = person.personId = person.oId;
                    //computedData.personId = computedData.oId
                    //self.setSdkApprover(computedData)
                    //self.setApprovalNodeUsers(computedData, type)
                    people.push(person)
                }
                self.toggleReturnBtn(true)
                
                if ( historyLen > 0 ) {
                    console.log('确定：' + historyLen)
                    document.getElementById("personBridge").contentWindow.history.go(-historyLen)
                }
                options.success && options.success(people)
            },
            
            onCancel: function (historyLen) {
                console.log("cancel")
                self.toggleReturnBtn(true)
                if ( historyLen > 0 ) {
                    console.log('取消：' + historyLen)
                    document.getElementById("personBridge").contentWindow.history.go(-historyLen)
                }
                options.fail && options.fail(people)
            }
        });
        this.toggleReturnBtn(false)
    }
}

export default app