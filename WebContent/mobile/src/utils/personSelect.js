//仅用作Web端选人

function extend(defaultOptions, options) {
   if (typeof (options) !== 'object') {        options = {}
    }
   for (var key in options) {
        if (options.hasOwnProperty(key)) {
            defaultOptions[key] = options[key]
        }
    }
    return defaultOptions
}

var defaultOption = {
    onSelect: null,
    onCancel: null,
    multi: false
}

function SelectPerson(option) {
    if (this instanceof SelectPerson) {
        this.options = extend(defaultOption, option)
        return this.init(this.options)
    } else {
        return new SelectPerson(option)
    }
}

SelectPerson.prototype = {

    init: function (options) {
        this.createIframe(options.multi)
        this.addEvent()
        return this
    },
    createIframe: function ( multi ) {
        var ifrm = document.createElement("iframe")
        ifrm.id = "personBridge"
        //ifrm.setAttribute("src", "/space/c/js/bridge/select/persons?multiple=false")
        //默认单选
        ifrm.setAttribute("src", "/space/c/js/bridge/select/persons?multiple=" + multi || false)
        ifrm.style.zIndex = "99999"
        ifrm.style.width = "100%"
        ifrm.style.height = "100%"
        ifrm.style.position = "fixed"
        ifrm.style.top = "50%"
        ifrm.style.left = "50%"
        ifrm.style.transform = "translate(-50%, -50%)"
        ifrm.style.border = "1px solid #ccc"
        document.body.appendChild(ifrm)
    },
    addEvent: function () {
        var self = this
        window.addEventListener('message', function handler(event){
            self.onMessage(event)
            window.removeEventListener('message', handler);
        }, false)
    },
    onMessage: function (event) {
        if (event.data.type === 'confirm') {
            this.options.onSelect && this.options.onSelect(event.data.member, event.data.historyLen)
            this.close()
        }
        if (event.data.type === 'cancel') {
            this.options.onCancel && this.options.onCancel(event.data.historyLen)
            this.close()
        }
    },
    close: function () {
        var elem = document.getElementById('personBridge')
        document.body.removeChild(elem)
    }
}

export default SelectPerson
