export default {
    required: function (value) {
        if (typeof value == 'boolean') return value;
        return !((value == null) || (value.length == 0));
    },
    numeric: function (value) {
        return (/^-?(?:0$0(?=\d*\.)|[1-9]|0)\d*(\.\d+)?$/).test(value);
    },
    integer: function (value) {
        return (/^(-?[1-9]\d*|0)$/).test(value);
    },
    digits: function (value) {
        return (/^[\d() \.\:\-\+#]+$/).test(value);
    },
    alpha: function (value) {
        return (/^[a-zA-Z]+$/).test(value);
    },
    alphaNum: function (value) {
        return !(/\W/).test(value);
    },
    email: function (value) {
        return (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(value);
    },
    url: function (value) {
        return (/^(https?|ftp|rmtp|mms):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i).test(value);
    },
    minLength: function (value, arg) {
        return value && value.length && value.length >= +arg;
    },
    maxLength: function (value, arg) {
        return value && value.length && value.length <= +arg;
    },
    length: function (value) {
        return value && value.length == +arg;
    },
    min: function (value, arg) {
        return value >= +arg;
    },
    max: function (value, arg) {
        return value <= +arg;
    },
    pattern: function (value, arg) {
        var match = arg.match(new RegExp('^/(.*?)/([gimy]*)$'));
        var regex = new RegExp(match[1], match[2]);
        return regex.test(value);
    }
};