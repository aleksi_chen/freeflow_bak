import Vue from 'vue'
module.exports = {
    //截取URL键值对
    getRequest: function () {
        var url = window.location.search;
        var theRequest = new Object();
        var strs;
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                theRequest[strs[i].split("=")[0]] = decodeURIComponent(strs[i].split("=")[1]);
            }
        }
        return theRequest;
    },
    checkBridgetVer: function (vStr) {
        var ua = navigator.userAgent,
            reg = /Qing\/([^;]+)/gi,
            match = reg.exec(ua);

        if (!match) return false;

        var versions = match[1].split('.'); //判断userAgent版本
        var vStrs = vStr.split('.');

        // 逐个判断当前版本号是否大于传入版本号
        for (var i = 0, len = versions.length; i < len; i++) {
            if (+versions[i] == +vStrs[i]) {
                continue;
            }

            return vStrs[i] ? +versions[i] > +vStrs[i] : true;
        }

        return false;
    },
    /**
     * 获取图片的base64值
     * @param ele 必须是图片元素
     * @returns {string|*}
     */
    toBase64: function (ele) {
        //var img = document.getElementById("logoImg")
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var dataURL;
        canvas.height = ele.height;
        canvas.width = ele.width;
        ctx.drawImage(ele, 0, 0);
        dataURL = canvas.toDataURL();
        return dataURL;
    },
    getOS: function () { //获取操作系统平台，IOS或安卓
        var userAgent = navigator.userAgent;
        return userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/) ? 'ios' : userAgent.match(/Android/i) ? 'android' : '';
    },
    // 判断是否桌面端
    isCloudhub: function () {
        var userAgent = navigator.userAgent;
        return /App\/cloudhub/.test(userAgent);
    },
    extend: function(defaultOptions, options) {
        if (typeof (options) !== 'object') {
            options = {}
        }
        for (var key in options) {
            if (options.hasOwnProperty(key)) {
                defaultOptions[key] = options[key]
            }
        }
        return defaultOptions
    },
    addErrorTips: function(el){
        Vue.util.addClass(el, 'error')
        setTimeout(() => {
           Vue.util.removeClass(el, 'error') 
        }, 2000)
    },
    /**
     * 判断为电脑端时，创建设置入口
     */
    createSettingMenu: function (){
        if ( this.getOS() || !(window.top.__sessionUser && window.top.__sessionUser.isAdminRole)) {
            return;
        }
        var timer = null;
        var dept = window.parent.document.querySelector('.network-name a').innerHTML
        var webLappToken = window.top.__sessionUser.webLappToken
        timer = setInterval(function () {
            if ( window.lappmask ) {
                //设置按钮
                var evtArray = [
                    {
                        text: '后台管理',
                        callBack: function () {
                            window.open('/freeflow/web/approval-manage.json?lappName=freeflow&dept=' + dept + '&webLappToken=' + webLappToken, '_blank')
                        }
                    }
                ];

                window.lappmask.createSettingPop(evtArray);
                clearInterval(timer);
            }
        }, 100)
        //跨域返回按钮
        window.parent.postMessage(JSON.stringify({
            needBack: false
        }), '*');
    },
    /**
     * 判断是否是个空对象
     * @param obj
     * @returns {boolean}
     */
    isEmptyObject: function (obj) {
        var prop;
        for (prop in obj)
            return false;
        return true
    },
}