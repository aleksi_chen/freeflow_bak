var Chance = require('chance')
var chance = new Chance()
var statusSeed = ['等待我审批...', '审批完成...', '审批不通过...']
var typeSeed = ['出差', '请假', '报销', '转正', '离职', '结婚']
var stringPool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
var componentTypes = ['textfield', 'textareafield', 'numberfield', 'datefield', 'photofield', 'radiofield', 'daterangefield']

chance.mixin({
    'approval': function() {
        return {
			id: chance.string({pool: stringPool}),
			title: chance.name() + '的【'+ typeSeed[chance.integer({min:0, max: typeSeed.length-1})] +'申请】',
			createAt: chance.date({year: 2016}),
			statusText: statusSeed[chance.integer({min:0, max: statusSeed.length-1})],
			statusCode: chance.integer({min: 0, max: 5})
		}
    }
});

chance.mixin({
    'approvalRecord': function() {
        return {
			approverUserName: chance.name(),
			action: '通过',
			createAt: chance.date({year: 2016}),
			approverId: chance.string({pool: stringPool}),
			status: chance.integer({min: 0, max: 5}),
			approverUserId: "56789f19e4b00c6e8a117566",
     		approverUserOid: "56d98538e4b042a895c0f665",
     		comment: chance.string({pool: stringPool})
		}
    }
});
chance.mixin({
    'approvaltpls': function() {
        return {
			id: chance.string({pool: stringPool}),
			title: typeSeed[chance.integer({min:0, max: typeSeed.length-1})] + '申请',
			createAt: chance.date({year: 2016}),
		}
    }
});

chance.mixin({
	'component': function(){
		return {
			id: chance.string({pool: stringPool}),
			type: componentTypes[chance.integer({min:0, max: componentTypes.length-1})],
			label: chance.string({pool: stringPool}),
			placeholder: chance.string({pool: stringPool}),
			require: true,
			options: ['选项1', '选项2', '选项3'],
			format: 'YYYY-MM-DD HH:ss',
			labels: ['开始事件', '结束时间']
		}
	}
})
chance.mixin({
	'components': function(){
		var components = []
		for(var i = 0; i < chance.integer({min: 0, max: 10}); i++){
			components.push(chance.component())
		}
		return components
	}
})
chance.mixin({
	'approvers': function(){
		var approvers = []
		for(var i = 0; i < chance.integer({min: 0, max: 10}); i++){
			approvers.push({
					id: chance.string({pool: stringPool}),
					username: chance.name(),
			})
		}
		return approvers
	}
})
chance.mixin({
	'template': function(){
		return {
			id: chance.string({pool: stringPool}),
			title: typeSeed[chance.integer({min:0, max: typeSeed.length-1})] + '申请',
			components: chance.components(),
			approvers: chance.approvers()
		}
	}
})



module.exports = {
	//我的账户信息
	account: function(req, res, next){
		var meta = {
			code: 200,
			status: "success"
		}
		var account = {
            id: 'aleksixxxxxxxx',
            username: 'aleksi',
            authentication_token: chance.string({pool: stringPool})
        }
		res.json({meta: meta, account: account})

	},
	//我的审批
	myApproval: function(req, res, next){

		var meta = {
			code: 200,
			status: "success"
		}
		var approvals = []
		for(var i = 0; i < chance.integer({min: 0, max: 30}); i++){
			approvals.push(chance.approval())
		}
		res.json({meta:meta, approvals:approvals})
	},
	myRequest: function(req, res, next){
		res.json({name:"asdad"})
	},
	approvalDetail: function(req, res, next){
		console.log('审批详情');
		console.log(req.params.id)
		var meta = {
			code: 200,
			status: "success"
		}
		var labels = []
		for(var i = 0; i < chance.integer({min: 0, max: 30}); i++){
			labels.push({title: "开始日期", value: "2016/03/12"})
		}
		var photos = []
		for(var i = 0; i < chance.integer({min: 1, max: 10}); i++){
			photos.push({
	          "fileSize": 2580638,
	          "fileId": Date.now(),
	          "fileName": "image.jpeg",
	          "fileSuffix": "jpeg",
	          "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt="+ i +"&w=150&h=150"
       		})
		}
		var approval = {
			id: req.params.id,
			title: chance.name() + '的【'+ typeSeed[chance.integer({min:0, max: typeSeed.length-1})] +'申请】',
			creatorId: chance.string({pool: stringPool}),
			creatorUserName: chance.name(),
			labels: labels,
			photos: photos,
			status: chance.integer({min: 0, max: 5}),
			nextApproverId: 'aleksixxxxxxxx',
			nextApproverUserName:  chance.name(),
			lastApproverId: 'aleksixxxxxxxx',
			lastApproverUserName: 'aleksi',
			createAt: chance.date({year: 2016}),
		}
		res.json({meta:meta, approval: approval})
	},
	approvalRecord: function(req, res, next){
		var meta = {
			code: 200,
			status: "success"
		}
		var approvalRecords = []
		var firstRecord = {
				approverUserName: chance.name(),
				action: "提交",
				createAt: chance.date({year: 2016}),
				approverId: chance.string({pool: stringPool}),
				status: chance.integer({min: 0, max: 5}),
				approverUserId: "56789f19e4b00c6e8a117566",
	     		approverUserOid: "56d98538e4b042a895c0f665",
	     		comment: chance.string({pool: stringPool})
	    }
	    approvalRecords.push(firstRecord)
		for(var i = 0; i < chance.integer({min: 1, max: 10}); i++){
			approvalRecords.push(chance.approvalRecord())
		}
		res.json({meta:meta, approvalRecords: approvalRecords})
	},
	createApprovalRecord: function(req, res, next){
		console.log(req)
		var meta = {
			code: 201,
			status: "success"
		}
		res.json(201, {
			meta:meta, 
			approvalRecord: {
				approverUserName: chance.name(),
				action: "同意",
				createAt: chance.date({year: 2016}),
				approverId: chance.string({pool: stringPool}),
				status: chance.integer({min: 0, max: 5}),
				approverUserId: "56789f19e4b00c6e8a117566",
	     		approverUserOid: "56d98538e4b042a895c0f665",
	     		comment: chance.string({pool: stringPool})
	     	}
		})
	},
	updateApprovalRecord: function(req, res, next){
		var meta = {
			code: 200,
			status: "success"
		}
		res.json({
			meta:meta, 
			approvalRecord: {
				approverUserName: chance.name(),
				action: "同意",
				createAt: chance.date({year: 2016}),
				approverId: chance.string({pool: stringPool}),
				status: chance.integer({min: 0, max: 5}),
				approverUserId: "56789f19e4b00c6e8a117566",
	     		approverUserOid: "56d98538e4b042a895c0f665",
	     		comment: chance.string({pool: stringPool})
	     	}
		})
	},
	//获取审批模版
	approvaltpl: function(req, res, next){
		var meta = {
			code: 200,
			status: "success"
		}
		var approvaltpl = []
		for(var i = 0; i < chance.integer({min: 1, max: 10}); i++){
			approvaltpl.push(chance.approvaltpls())
		}
		res.json({meta: meta, approvaltpl: approvaltpl})
	},
	template: function(req, res, next) {
		var meta = {
			code: 200,
			status: "success"
		}
		res.json({meta: meta, template: chance.template()})
	}

}