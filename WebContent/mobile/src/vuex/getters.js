//获取当前账户状态
export function myAccountInfo(state) {
    if ( window.localStorage && window.localStorage.getItem('user') && window.localStorage.getItem('user') !== "undefined" ) {
        return JSON.parse(window.localStorage.getItem('user'));
    } else if ( state.myAccount ) {
        return state.myAccount
    } else {
        return alert("用户信息获取失败");
    }
}

/**
 * 获取当前所在标签页
 * @param state
 * @returns {number}
 */
export function currentTabIndex(state) {
    return state.currentTabIndex
}

//获取我的审批
export function myApprovals(state) {
    return state.myApprovalData
}

//获取我的申请
export function myApplications(state) {
    return state.myApplicationData
}

//从state中获取审批详情数据
export function approvalDetail(state) {
    return state.approvalDetail
}
/**
 * 获取关键审批人信息
 * @param state
 * @returns {Array}
 */
export function approvalUsers(state) {
    return state.approvalUsers
}

/**
 * 获取创建审批信息
 * @param state
 * @returns {{}}
 */
export function createInfo(state) {
    let createInfo = {};
    createInfo.createTime = new Date(state.approvalDetail.createTime);
    createInfo.createUser = state.approvalDetail.createUser;
    return createInfo;
}

/**
 * 获取审批详情图片数据
 * @param state
 * @returns {Array}
 */
export function approvalDetailPhoto(state) {
    let photosInfo = [];
    state.approvalDetail.labels.forEach(function (item, index) {
        if ( item.type == "photofield" ) {
            photosInfo = item.value;
        }
    })
    /*for (let item of state.approvalDetail.labels) {
     if ( item.type == "photofield" ) {
     photosInfo = item.value;
     }
     }*/
    return photosInfo;
}

//从state中获取审批流列表数据
export function approvalRecord(state) {
    return state.approvalRecord
}

//是否显示同意审批弹窗
export function showConfirmApproval(state) {
    return state.showConfirmApproval
}

//当前用户否是当前审批人
export function isCurApprover(state) {
    //如果审批状态还没结束 暂时设置0为结束
    var canApproval = state.approvalDetail.status !== 0 ? true : false
    //如果当前我的id＝下一审批人id
    var isNextApprover = state.myAccount.id === state.approvalDetail.nextApproverId && state.myAccount.id
    return canApproval && isNextApprover
}

/**
 * 判断当前是否可以审批
 * @param state
 * @returns {boolean|*}
 */
export function isCanApproval(state) {
    //如果审批状态还没结束 暂时设置0为结束
    const canApproval = state.approvalDetail.status == "1"
    //如果当前我的id＝下一审批人id,后台已经返回该字段
    //let isNextApprover = myAccountInfo(state).oid && myAccountInfo(state).oid === state["approvalDetail"]["nextApprover"]["oid"];
    const isNextApprover = state.isNextApprover
    return canApproval && isNextApprover
}

/**
 * 判断是否是当前审批单的创建者
 * @param state
 * @returns {boolean}
 */
export function isCreateUser(state) {
    return state.isCreateUser
}

/**
 * 判断该审批单是否已经结束
 * @param state
 * @returns {boolean}
 */
export function isEndApproval(state) {
    return state.approvalDetail.status != "1"
}

//当前用户是否是最后审批人
export function canCompleteApproval(state) {
    //如果审批状态还没结束 暂时设置0为结束
    var canApproval = state.approvalDetail.status !== 0 ? true : false
    //如果当前我的id ＝ 审批详情的最后审批批人id 和下一审批人
    var isNextApprover = state.myAccount.id == state.approvalDetail.nextApproverId && state.myAccount.id == state.approvalDetail.lastApproverId
    return canApproval && isNextApprover
}

//是否显示批复窗口
export function showCommentPopup(state) {
    return state.showCommentPopup
}

//批复文字
export function approvalReplyText(state) {
    return state.approvalReplyText
}

//审批模版列表
export function approvalTpls(state) {
    return state.approvalTpls
}

//审批模版详情
export function templateDetail(state) {
    return state.template
}

//获取模板审批规则
export function approvalRule(state) {
    return state.approvalRule
}

//审批模版组件
export function components(state) {
//    if(state.template.components){
//        console.log(state.template.components)
//    }
//    return state.template.components ?  JSON.parse(state.template.components) : null
    return state.components
}

//获取选人桥中存储的审批人信息
export function getSdkApprover(state) {
    return state.sdkApprover
}

/**
 * 返回所有审批单评论信息
 * @param state
 * @returns {Array}
 */
export function approvalDiscuss(state) {
    return state.approvalDiscuss
}

/**
 * 返回已经保存的输入的评论内容
 * @param state
 * @returns {string}
 */
export function inputDiscuss(state) {
    return state.inputDiscuss
}

/**
 * 返回轻应用id
 * @param state
 * @returns {string}
 */
export function getLightAppId(state) {
    return state.lightAppId
}
/**
 * 返回appId
 * @param state
 * @returns {string}
 */
export function getAppId(state) {
    return state.appId
}
/**
 * 返回回复单据评论id
 * @param state
 * @returns {string}
 */
export function getReplyDiscussInfo(state) {
    return state.replyDiscussInfo
}
/**
 * 返回审批流转节点人
 * @param state
 * @returns {Array}
 */
export function getApprovalNodeUsers(state) {
    return state.approvalNodeUsers
}
export function getAgreeEnd(state) {
    return state.isAgreeEnd
}
export function getRecommendApprover(state) {
    return state.recommendApprover
}
export function getApprovalCndGroups(state) {
    return state.approvalCndGroups
}
export function cndApprovers(state) {
    return state.cndApprovers
}
export function curCondition(state) {
    return state.curCondition
}