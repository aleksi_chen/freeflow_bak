import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const state = {
    myAccount: {},
    currentTabIndex: 0,
    myApprovalData: [],
    myApplicationData: [],
    //审批详情
    approvalDetail: {},
    //审批流记录
    approvalRecord: [],
    //关键审批人信息
    approvalUsers: {},
    //currentApprovalIndex: 2,
    //currentApplyIndex: 2,   //默认拉取两页数据，一页5个
    showConfirmApproval: false,
    //批复内容
    approvalReplyText: '',
    //审批模版
    approvalTpls: [],
    //模版详情
    template: {},
    //组件对象列表
    components: [],
    approvalForm: [],
    //获取选人桥中存储的审批人信息
    sdkApprover: null,
    //模板审批规则
    approvalRule: {},
    isNextApprover: false,
    isCreateUser: true,
    inputDiscuss: "",
    approvalDiscuss: [],
    lightAppId: "",
    appId: "",
    replyDiscussInfo: {},
    //添加的审批人节点
    approvalNodeUsers: [],
    //是否可以结束审批字段
    isAgreeEnd: false,
    //推荐审批人
    recommendApprover: {},
    //审批规则组
    approvalCndGroups: [],
    //当前条件下的审批人
    cndApprovers: [],
    //当前符合的条件
    curCondition:{}
}

const mutations = {
    LOADMYACCOUNT: (state, account) => {
        state.myAccount = account
    },
    TOGGLETAB: (state, index) => {
        state.currentTabIndex = index
    },
    UPDATEMYAPPROVAL: (state, data) => {
        console.log('UPDATEMYAPPROVAL')
    },
    REDUCE: (state, data) => {
        state.myApprovalData.pop()
    },
    LOADMYAPPROVAL: (state, data, type) => {
        if ( type && type === "init" ) {
            state.myApprovalData = data;
        } else {
            state.myApprovalData = state.myApprovalData.concat(data)
        }
    },
    LOADMYAPPLICATION: (state, data, type) => {
        if ( type && type === "init" ) {
            state.myApplicationData = data;
        } else {
            state.myApplicationData = state.myApplicationData.concat(data)
        }
    },
    APPROVALDETAIL: (state, data) => {
        state.approvalDetail = data;
        //第三方审批isThird字段为true
        if ( !data.isThird ) {
            state.approvalDetail.labels = JSON.parse(data.content);
        }
    },
    LOADAPPROVALRECORD: (state, data) => {
        state.approvalRecord = data
    },
    TOGGLECONFIRMAPPROVAL: (state, data) => {
        state.showConfirmApproval = data
    },
    APPLYAPPROVALREPLY: (state, data) => {
        state.approvalReplyText = data
    },
    ENDAPPROVAL: (state, data) => {
        state.approvalDetail.status = 0
    },
    UPDATEAPPROVALSTATUS: (state, data)=> {
        state.approvalDetail.status = data
    },
    ADDAPPROVALRECORD: (state, approvalRecord) => {
        state.approvalRecord.pop()
        state.approvalRecord.push(approvalRecord)
    },
    UPDATEAPPROVALDETAIL: (state, data) => {
        state.approvalDetail.nextApproverId = data.nextApproverId
    },
    LOADAPPROVALTPL: (state, data) => {
        state.approvalTpls = data
    },
    ADDAPPROVALTPL: (state, data) => {
        state.approvalTpls = state.approvalTpls.concat(data)
    },
    LOADTEMPLATE: (state, data) => {
        state.template = data
        state.components = JSON.parse(data.components)
    },
    LOADAPPROVALRULE: (state, data)=> {
        state.approvalRule = data;
    },
    UPDATECURAPPROVALINDEX: (state, type, val) => {
        if ( type === "plus" ) {
            state.currentApprovalIndex += val;
        }
    },
    UPDATECURAPPLYINDEX: (state, type, val) => {
        if ( type === "plus" ) {
            state.currentApplyIndex += val;
        }
    },
    //更新组件的值
    UPDATECOMPONENTVALUE (state, { id, val }) {
        state.components.forEach((item, index) => {
            item.id === id ? item.value = val : null
        })
    },
    //设置选人桥获得的人员信息
    STORESDKAPPROVER (state, approver) {
        state.sdkApprover = approver
    },
    /**
     * 设置关键审批人信息
     * @param state
     * @param data
     * @constructor
     */
    SETAPPROVALUSERS(state, data){
        state.approvalUsers = data
    },
    /**
     * 设置自己是不是该审批单的下一个审批人
     * @param state
     * @param data
     * @constructor
     */
    SETISNEXTAPPROVER: (state, data)=> {
        state.isNextApprover = data
    },
    /**
     * 设置自己是不是改审批单的创建者
     * @param state
     * @param data
     * @constructor
     */
    SETISCREATEUSER: (state, data)=> {
        state.isCreateUser = data
    },
    /**
     * 设置审批单评论
     * @param state
     * @param data
     * @constructor
     */
    SETAPPROVALDISCUSS: (state, data)=> {
        state.approvalDiscuss = data
    },
    /**
     * 在现有评论添加一条新评论,时间倒序排列
     * @param state
     * @param data
     * @constructor
     */
    ADDAPPROVALDISCUSS: (state, data)=> {
        state.approvalDiscuss.unshift(data)
    },
    /**
     * 设置当前输入的审批单评论
     * @param state
     * @param data
     * @constructor
     */
    SETINPUTDISCUSS: (state, data)=> {
        state.inputDiscuss = data
    },
    /**
     * 设置轻应用id
     * @param state
     * @param data
     * @constructor
     */
    SETLIGHTAPPID: (state, data)=> {
        state.lightAppId = data
    },
    /**
     * 设置appId
     * @param state
     * @param data
     * @constructor
     */
    SETAPPID: (state, data)=> {
        state.appId = data
    },
    /**
     * 设置回复的评论id
     * @param state
     * @param data
     * @constructor
     */
    SETREPLYDISCUSSINFO: (state, data)=> {
        state.replyDiscussInfo = data
    },
    RESTCOMPONENT: (state) => {
        state.components = []
    },
    /**
     * 设置审批流转节点人
     * @param state
     * @param data
     * @constructor
     */
    SETAPPROVALNODEUSERS: (state, data, type)=> {
        if ( type && type == "init" ) {
            state.approvalNodeUsers = data
        } else if ( type && type == "delete" ) {
            state.approvalNodeUsers.splice(data, 1)
        } else if ( type && type == "push" ) {
            state.approvalNodeUsers.push(data)
        } else {
            state.approvalNodeUsers.unshift(data)
        }
    },
    SETAGREEEND: (state, data)=> {
        state.isAgreeEnd = data
    },
    SETRECOMMENDAPPROVER: (state, data)=> {
        state.recommendApprover = data
    },
    /**
     * 设置审批规则组
     * @param state
     * @param data
     * @constructor
     */
    SETAPPROVALCNDGROUPS: (state, data)=> {
        state.approvalCndGroups = data
    },
    SETCNDAPPROVERS: (state, data)=> {
        state.cndApprovers = data
    },
    SETCURCONDITION: (state, data)=> {
        state.curCondition = data
    }
};

var store = new Vuex.Store({
    state,
    mutations,
    strict: debug
})

export default store