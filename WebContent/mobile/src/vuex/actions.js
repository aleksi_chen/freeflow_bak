//import * as types from './mutation-types'
import http from '../api/http'

//获取我的账户信息
//export const loadMyAccount = ({dispatch, state}) => {
//    //如果localstorage有就存进state, 没有则请求后台
//    if(window.localStorage && window.localStorage.getItem('user')&&window.localStorage.getItem('user')!=="undefined"){
//        dispatch('LOADMYACCOUNT', JSON.parse(window.localStorage.getItem('user')))
//    }else {
//        http.fetch('lightapp/rest/application/getUserInfo.json', {
//            type: 'GET',
//            urlAssemble: false,
//        }).then(function(res){
//            dispatch('LOADMYACCOUNT', res.body.data)
//            window.localStorage.setItem('user',JSON.stringify(res.body.data))
//        })
//    }
//}

export const toggleTab = ( { dispatch, state }, index ) => {
    dispatch('TOGGLETAB', index)
}

//获取我的审批
export const loadMyApproval = ( { dispatch, state }, options ) => {
    return new Promise(function ( resolve, reject ) {
        http.fetch('approval/list/myApprovalRecords', {
            type: 'POST',
            loading: true,
            data: options && options["type"] === "more" ? {
                start: options["start"],
                limit: 5
            } : ""
        }).then(function ( res ) {
            if ( options && options["type"] === "more" ) {
                if ( res["body"] && res["body"]["data"] ) {
                    resolve(res.body.data);
                    dispatch('LOADMYAPPROVAL', res.body.data.ApprovalApplyRecords)
                } else {
                    reject(res);
                }
            } else {
                dispatch('LOADMYAPPROVAL', res.body.data.ApprovalApplyRecords, options && options["type"] || "")
            }
        })
    });
};

//获取我的申请
export const loadMyApplication = ( { dispatch, state }, options ) => {
    return new Promise(function ( resolve, reject ) {
        http.fetch('approval/list/myApprovals', {
            type: 'POST',
            loading: true,
            data: options && options["type"] === "more" ? {
                start: options["start"],
                limit: 5
            } : ""
        }).then(function ( res ) {
            if ( options && options["type"] === "more" ) {
                if ( res["body"] && res["body"]["data"] ) {
                    resolve(res.body.data);
                    dispatch('LOADMYAPPLICATION', res.body.data.approvalApplys)
                } else {
                    reject(res);
                }
            } else {
                dispatch('LOADMYAPPLICATION', res.body.data.approvalApplys, options && options["type"] || "")
            }
        })
    })
};

//获取审批详情
export const loadApprovalDetail = ( { dispatch, state }, id ) => {
    return http.fetch(`approval/detail/approvalApply/${id}`, { loading: true }).then(function ( res ) {
        dispatch('APPROVALDETAIL', res.body.data.approvalApply)
        dispatch('LOADAPPROVALRECORD', res.body.data.approvalApplyRecords)
        dispatch('SETAPPROVALUSERS', res.body.data.approvalUsers)
        dispatch('SETISNEXTAPPROVER', res.body.data.canDo)
        dispatch('SETISCREATEUSER', res.body.data.isCreateUser)
        dispatch('SETLIGHTAPPID', res.body.data.lightAppId)
        dispatch('SETAPPID', res.body.data.appId)
        dispatch('SETAGREEEND', res.body.data.isAgreeEnd)
    })
}

export const loadRecommendApprover = ({ dispatch }, id) => {
    http.fetch('approval/recommendFlowUser/' + id, {
        type: 'GET',
        loading: true
    }).then(function (res) {
        dispatch('SETRECOMMENDAPPROVER', res.body.data && res.body.data.recommendFlowUser)
    })
}

//获取审批流列表
export const loadApprovalRecord = ( { dispatch, state }, id ) => {
    http.fetch('approvalrecord', { loading: true }).then(function ( res ) {
        dispatch('LOADAPPROVALRECORD', res.body.approvalRecords)
    })
}

/**
 * 获取审批流转要传的数据
 * @param dispatch
 * @param state
 * @param options
 * @returns {options.data|{status}}
 */
export const getModifyData = ( { dispatch, state }, options )=> {
    let comment = state.approvalReplyText;
    //这里的id指的是审批记录单id，与审批单id不同
    let id = state.approvalDetail.nextRecordId;
    let data = options.data;
    data.comment = comment;
    data.id = id;
    data.formCreatorUserId = state.approvalDetail.createUser.oid;
    data.transferApproverOId = data.transferApproverOId ? data.transferApproverOId : (state.approvalDetail.nextApprover.oid || "");
    if ( options.type === "onlyConfirm" || options.type === "end" || options.type === "reject") {
        data.transferApproverOId = ""
    }
    return data
}
/**
 * 修改审批流状态
 * @param dispatch
 * @param state
 * @param options
 */
export const modifyApproval = ( { dispatch, state }, options ) => {
    let data = getModifyData({ dispatch, state }, options)
    return http.fetch('approval/save/appApplyRecord', {
        type: 'POST',
        data: {
            approvalApplyRecord: data
        },
        loading: false
    }).then(function ( res ) {
        //dispatch('ADDAPPROVALRECORD', res.body.data.approvalApplyRecord)
        dispatch('LOADAPPROVALRECORD', res.body.data.approvalApplyRecords)
        switch ( options.type ) {
            case "reject":
                dispatch('UPDATEAPPROVALSTATUS', options.data.status);
                break;
            case "confirm":
            case "end":
                dispatch('TOGGLECONFIRMAPPROVAL', !state.showConfirmApproval)
                dispatch('ENDAPPROVAL', res.body.data.approvalApplyRecords)
                break;
            case "onlyConfirm":
                dispatch('ENDAPPROVAL', res.body.data.approvalApplyRecords)
                break;
        }
    })
}

//结束审批
export const endApproval = ( { dispatch, state }, record ) => {
    let options={
        data:{
            status:"4"
        },
        type:"end"
    };
    return modifyApproval({ dispatch, state },options);
    /*http.fetch('approval/save/appApplyRecord', {
        type: 'POST',
        data: record,
        loading: false
    })
        .then(function ( res ) {
            dispatch('ENDAPPROVAL', res.body.approvalRecord)
            //添加新的操作记录
            dispatch('ADDAPPROVALRECORD', res.body.approvalRecord)
            //关闭窗口
            dispatch('TOGGLECONFIRMAPPROVAL', ! state.showConfirmApproval)
        })*/
}

/**
 * 催办下一个审批人
 * @param dispatch
 * @param state
 * @param options
 */
export const urgeApproval = ( { dispatch, state }, options ) => {
    let data={
        nextApproverOid:state.approvalDetail.nextApprover.oid,
        approvalId:state.approvalDetail.id
    };
    return http.fetch('/approval/approvalUrge', {
        type: 'GET',
        data: data,
        loading: false
    })
}

//切换同意审批/选人窗口
export const toggleConfirmApproval = ( { dispatch, state } ) => {
    dispatch('TOGGLECONFIRMAPPROVAL', ! state.showConfirmApproval)
}

//确认审批批复文字
export const applyApprovalReply = ( { dispatch }, text ) => {
    dispatch('APPLYAPPROVALREPLY', text)
}

//选择下一审批人
export const nextApprover = ( { dispatch, state }, options ) => {
    return modifyApproval({ dispatch, state },options);
    /*http.fetch('approvalrecord', {
        type: 'POST',
        data: approver,
        loading: true
    })
    .then(function (res) {
        //添加新的操作记录
        dispatch('ADDAPPROVALRECORD', res.body.approvalRecord)
            //更新审批详细信息
        dispatch('UPDATEAPPROVALDETAIL', res.body.approvalRecord)
            //关闭窗口
        dispatch('TOGGLECONFIRMAPPROVAL', !state.showConfirmApproval)
    })*/
}

//获取审批模版列表
export const loadApprovalTpl = ( { dispatch }, params) => {

    return new Promise((resolve, reject) => {
        http.fetch('approval/list/approvalTypes', {
            type: 'POST',
            loading: true,
            data: params
        })
        .then(function ( res ) {
            if(res.body.meta.success == true){
                if(params.start === 1){
                    dispatch('LOADAPPROVALTPL', res.body.data.approvalTypes)
                }else{
                    dispatch('ADDAPPROVALTPL', res.body.data.approvalTypes)
                }
                resolve(res.body.data)
            }else {
                reject()
            }
        })
    })
    
}

//获取审批模版详情
export const loadTemplateDetail = ( { dispatch }, id ) => {
    http.fetch('form-template/detail/formTemplate/' + id + "?type=create", {
        type: 'GET',
        /*
         data: { id: id },
         */
        loading: true
    }).then(function (res) {
        dispatch('LOADTEMPLATE', res.body.data.template);
        dispatch('LOADAPPROVALRULE', res.body.data.approvalRule)
        dispatch('SETAPPROVALCNDGROUPS', res.body.data.approvalCndGroups)
    })
}

//提交审批申请
export const submitApproval = ( { dispatch }, {id, params}) => {
    return http.fetch(`approval/save/approvalApply/${id}`, {
        type: 'POST',
        loading: true,
        data: params
    })
}

//把表单控件的值更新到state的components里面
export const updateComponentValue = ( { dispatch }, data ) => {
    dispatch('UPDATECOMPONENTVALUE', data)
}

//存储选人桥获得的人员信息
export const setSdkApprover = ( { dispatch }, data ) => {
    dispatch('STORESDKAPPROVER', data)
}

export const setInputDiscuss = ( { dispatch,state }, data ) => {
    dispatch('SETINPUTDISCUSS', data)
}

export const setApprovalDiscuss = ( { dispatch,state }, data ) => {
    dispatch('SETAPPROVALDISCUSS', data)
}

export const addApprovalDiscuss = ( { dispatch }, data ) => {
    dispatch('ADDAPPROVALDISCUSS', data)
}

/*export const submitDiscuss=({dispatch,state},options)=>{
    let data = {
        bizId: options.id,
        bizType: 'freeflow_qingjia',
        content: options.content,
        reCommentId: state.replyDiscussInfo && state.replyDiscussInfo.id ? state.replyDiscussInfo.id : ""
    }
    return http.fetch('lightapp/rest/bizComment/addComment', {
        type: 'POST',
        data: data,
        loading: true,
        urlAssemble: false
    }).then(function ( res ) {
        addApprovalDiscuss( { dispatch }, res.body.data.bizComment )
    })
}*/

/**
 * 提交评论，用这个接口，有验证
 * @param dispatch
 * @param state
 * @param options
 * @returns {Promise.<TResult>|*}
 */
export const submitDiscuss=({dispatch,state},options)=>{
    let data = {
        bizId: options.id,
        bizType: 'freeflow_qingjia',
        content: options.content,
        reCommentId: state.replyDiscussInfo && state.replyDiscussInfo.id ? state.replyDiscussInfo.id : ""
    }
    return http.fetch('bizComment/addComment', {
        type: 'POST',
        data: data,
        loading: true
    }).then(function ( res ) {
        addApprovalDiscuss( { dispatch }, res.body.data.bizComment )
    })
}

/**
 * 载入审批单评论，用这个接口，有验证
 * @param dispatch
 * @param data
 */
export const loadApprovalDiscuss = ( { dispatch, state }, data )=> {
    return http.fetch('bizComment/findComment', {
        type: 'POST',
        data: {
            bizId: data,
            bizType: 'freeflow_qingjia'
        },
        loading: false
    }).then(function ( res ) {
        setApprovalDiscuss({ dispatch, state }, res.body.data.bizComments);
    })
}

/*export const loadApprovalDiscuss = ( { dispatch, state }, data )=> {
    http.fetch('lightapp/rest/bizComment/findComment', {
        type: 'POST',
        data: {
            bizId: data,
            bizType: 'freeflow_qingjia'
        },
        loading: false,
        urlAssemble: false
    }).then(function ( res ) {
        setApprovalDiscuss({ dispatch, state }, res.body.data.bizComments);
    })
}*/

export const updateReplyDiscussInfo = ( { dispatch, state }, data )=> {
    dispatch('SETREPLYDISCUSSINFO', data)
}

export const resetComponentInfo = ( { dispatch, state } )=> {
    dispatch('RESTCOMPONENT')
}

export const setApprovalNodeUsers = ({ dispatch, state }, data, type)=> {
    dispatch('SETAPPROVALNODEUSERS', data, type)
}
export const setIsAgreeEnd = ({ dispatch, state }, data)=> {
    dispatch('SETAGREEEND', data)
}
export const setRecommendApprover = ({ dispatch, state }, data)=> {
    dispatch('SETRECOMMENDAPPROVER', data)
}
export const setCndApprovers = ({ dispatch, state }, data)=> {
    dispatch('SETCNDAPPROVERS', data)
}
export const setCurCondition = ({ dispatch, state }, data)=> {
    dispatch('SETCURCONDITION', data)
}
