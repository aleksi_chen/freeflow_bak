import Vue from 'vue'
import VueRouter from 'vue-router'
import Fastclick from 'fastclick'
import config from './config'
import qingjs from './lib/qingjs'
import helpers from './utils/helpers'

import {
    Root,
    App,
    MyApply,
    MyApproval,
    MyApplication,
    SelectApplyType,
    ApplicationDetail,
    ApprovalDetail
} from './pages'

import { getRequest } from './utils/helpers'
import { ApprovalPreview, ApprovalDiscussInput } from './components'

//本地开发环境下为每个请求添加webLappToken进行通信
if ( process.env.NODE_ENV === 'develop' ) {
    const query = getRequest()
    if ( query.lappName && query.webLappToken ) {
        const authInfo = {
            lappName: query.lappName,
            webLappToken: query.webLappToken
        }
        localStorage.setItem('auth', JSON.stringify(authInfo))
    }
}

const query = getRequest()
    if ( query.lappName && query.webLappToken ) {
        const authInfo = {
            lappName: query.lappName,
            webLappToken: query.webLappToken
    }
    localStorage.setItem('auth', JSON.stringify(authInfo))
}

//忘记下面这参数干嘛的了
Vue.config.debug = true

Vue.use(VueRouter)

const router = new VueRouter({
    history: true ,
    root: config.routeRootPath
})

router.map({
    '/': {
        component: App,
        subRoutes: {
            '/myapproval': {
                name: 'myapproval',
                component: MyApproval
            },
            '/myapply': {
                name: 'myapply',
                component:  MyApply
            }
        }
    },
    '/myApply': {
        component: MyApply
    },
    '/select': {
        component: SelectApplyType  //选择申请的审批类型
    },
    '/applydetail/:id': {
        name: 'applicationDetail',
        component: ApplicationDetail //申请详情
    },
    '/approval/:id': {
        name: 'approvalDetail',
        component: ApprovalDetail, //审批单详情
        subRoutes: {
            '/photo/:index': {
                name: 'preview',
                component: ApprovalPreview
            },
            '/discuss': {
                name: 'discuss',
                component: ApprovalDiscussInput
            }
        }
    }

})

router.start(Root, '#container')

Fastclick.attach(document.body)

helpers.createSettingMenu()