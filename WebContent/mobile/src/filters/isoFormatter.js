import Vue from 'vue'

export default Vue.filter('isoFormatter', function (value) {
    if(!value){
        return value
    }

    const d = new Date(value)
    
    const dateString = `${d.getFullYear()}-${("0"+(d.getMonth()+1)).slice(-2)}-${("0" + d.getDate()).slice(-2)} ${("0" + d.getHours()).slice(-2)}:${("0" + d.getMinutes()).slice(-2)}`
    
    return dateString
})