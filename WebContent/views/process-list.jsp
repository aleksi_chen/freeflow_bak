<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html >
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="format-detection" content="telephone=no" />
	<title>流程审批</title>
	<%@ include  file="common-settings.jsp"%>
	<script>
		currUserId = '${CONTEXT.userNetwork.userId}';
	</script>
</head>
<body>
	<section class="process-list-wrap">
		<div class="group-btn list-nav">
			<button class="selected">我的申请</button>
			<button class="btn-yes">我的审批</button>
		</div>
		<section class="list-wrap">
			<ul class="myrequest-wrap"></ul>
			<ul class="myprocess-wrap"></ul>
		</section>	
		<div class="group-btn" >
			<button class="btn-full-w request-btn" data-active="active-grey">我要申请</button>
		</div>
		<div class="loading">
			<div class="circle-container container1 vertical-middle">
				<div class="circle1"></div>
				<div class="circle2"></div>
				<div class="circle3"></div>
				<div class="circle4"></div>
			</div>
			<div class="circle-container container2 vertical-middle">
				<div class="circle1"></div>
				<div class="circle2"></div>
				<div class="circle3"></div>
				<div class="circle4"></div>
			</div>
		</div>
		<div class="bottom-tips">
			<div class="load-more">
				<span class="circle1"></span>
				<span class="circle2"></span>
				<span class="circle3"></span>
			</div>
			<div class="request-tips">
				<div class="no-more-process none">没有更多了哦~</div>
				<div class="load-more-fail none">出错了&gt;_&lt;，下拉重试</div>
				<div class="no-process none">
					<img src="/freeflow/images/no-data.png" alt="">
					<span>没有审批记录哦~</span>
			    </div>
			</div>	
			<div class="process-tips">
				<div class="no-more-process none">没有更多了哦~</div>
				<div class="load-more-fail none">出错了&gt;_&lt;，下拉重试</div>
				<div class="no-process none">没有审批记录哦~</div>
			</div>	
		</div>
	</section>
	<script>
    	processList.init()
    </script>
</body>
</html>
