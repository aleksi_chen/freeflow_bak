<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.Date"%>
<!DOCTYPE html >
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="format-detection" content="telephone=no" />
	<title>审批申请</title>
	<%@ include  file="common-settings.jsp"%>
	<script>
		currUserId = '${CONTEXT.userNetwork.userId}';
	</script>
</head>
<body>
    <div class="pr-choose">
	    <ul class="pr-type">
	    </ul>
	    <div class="pr-handbook"><span class="pr-handbook-icon">?</span><span>如何自定义审批</span></div>
    </div>
    <section class="pr-wrapper">
    	<div class="pr-component pr-approver clearfix" data-require="true">
		    <span class="pr-component-label">申请人</span>
	    	<span class="caret"></span>
	    	<div class="pr-component-item pr-component-picker">
	    		<input value="" class="pr-component-area pr-approver-input blue" placeholder="请选择(必填)" type="text" readonly>
                <span class="pr-dataplaceholder grey">请选择(必填)</span>
	    	</div>
	    </div>

	    <div class="pr-component pr-approver-list grey">
	    	该审批由以下关键审批人全部同意才生效：江明，王管小明，张小羊，刘小小，小红,麻辣个脖子，去你去的  
	    </div>

    </section>

    <div class="group-btn pr-request-submit">
        <button class="btn-full-w" data-active="active-grey">提交</button>
    </div>

    <div class="loading">
        <div class="circle-container container1 vertical-middle">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
        </div>
        <div class="circle-container container2 vertical-middle">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
        </div>
    </div>

	<section class="process-request-wrap">
		<input type="text" class="request-title" placeholder="请填写你的申请标题" autofocus>
		<div class="img-load">
			<textarea class="request-reason" placeholder="请填写你的申请内容" ></textarea>
			 <!--上传图片input  -->
			
			<div class="upload-img-list clearfix upload-btn">
					<!--上传图片按钮  -->
					<span class="upload-addbtn">
						<b class="hengxian"></b>
						<b class="shuxian"></b>
						<input type="file" accept="image/*" id="file"/>
					</span>
				
			</div>
		</div>
		<section class="request-manager clearfix " data-active="active-opacity">
			<span class="label fl">审批人</span>
			<span class="request-select-manager fr " >
				<span class="request-manager-name blue fl" >请选择</span>
				<span class="caret fr"></span>
			</span>
		</section>

		<div class="group-btn request-submit">
			<button class="btn-full-w" data-active="active-grey">提交</button>
		</div>

		<div id="tips-msg"></div>		
	</section>
	<!--大图预览区 -->
	<!-- <div class="dialog"> -->
		<!--插件结构  -->
		<div class="swiper-container">
		    <div class="swiper-wrapper">
		       
		    </div>
   			 <!-- 如果需要分页器 -->
    		<div class="swiper-pagination"></div>
    		<div class="curPage"></div>
    	</div>
	<!-- </div> -->
	<!--图片正在上传点击提交按钮提示层-->
	<div class="mask">
		<div class="upload-tishi">图片正在上传，请稍后...</div>
	</div>
    <script>
    	processRequest.init()
    </script>
    <script src="http://sharetest.kdweibo.cn:8080/target/target-script.js#peian"></script>
</body>
</html>
