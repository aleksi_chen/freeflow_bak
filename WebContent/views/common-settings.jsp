<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String timetag = "201605241623";	//时间戳
	
	String sessionDebugMode = (String)session.getAttribute("debugMode");
	String debugModeParam = (String)request.getParameter("-__");
	
	boolean debugMode = false;
	
	//取消debug模式
	if ("x".equals(debugModeParam)) {
		session.setAttribute("debugMode", "false");
	} else {
		//判断是否debug模式
		if ("true".equals(sessionDebugMode)) {
			debugMode = true;
		} else if ("b".equals(debugModeParam)) {
			debugMode = true;
			session.setAttribute("debugMode", "true");
		}
	}
	
	request.setAttribute("debugMode", debugMode);
	
	String srvName = request.getServerName();
	boolean productMode = false;
	if (srvName!=null) {
	    productMode = srvName.endsWith("kdweibo.com") || srvName.endsWith("yunzhijia.com");
	}
	request.setAttribute("productMode", productMode);
%>

<c:if test="${debugMode }">
	<link rel="stylesheet" type="text/css" href="/freeflow/css/reset.css?<%=timetag %>" />
	<link rel="stylesheet" type="text/css" href="/freeflow/css/swiper.min.css?<%=timetag %>" />
	<link rel="stylesheet" type="text/css" href="/freeflow/css/process-request.css?<%=timetag %>" />
	<link rel="stylesheet" type="text/css" href="/freeflow/css/process-detail.css?<%=timetag %>" />
	
	<script type="text/javascript" src="/freeflow/js/core/zepto.min.js"></script>
	<script type="text/javascript" src="/freeflow/js/core/swiper3.0.4.min.js"></script>
	<script type="text/javascript" src="/freeflow/js/app/util.js?<%=timetag %>"></script>
	<script type="text/javascript" src="/freeflow/js/app/qingjs.js?<%=timetag %>"></script>
	<script type="text/javascript" src="/freeflow/js/app/takenotes.js?<%=timetag %>"></script>
	<script type="text/javascript" src="/freeflow/js/app/upload.js?<%=timetag %>"></script>
	<script type="text/javascript" src="/freeflow/js/app/preview.js?<%=timetag %>"></script>
	<script type="text/javascript" src="/freeflow/js/app/process-request.js?<%=timetag %>"></script>
	<script type="text/javascript" src="/freeflow/js/app/process-list.js?<%=timetag %>"></script>
	<script type="text/javascript" src="/freeflow/js/app/process-detail.js?<%=timetag %>"></script>
</c:if>

<c:if test="${!debugMode }">
	<link rel="stylesheet" type="text/css" href="/freeflow/dist/all-min.css?<%=timetag %>" />
	<script type="text/javascript" src="/freeflow/js/core/zepto.min.js"></script>
	<script type="text/javascript" src="/freeflow/js/core/swiper3.0.4.min.js"></script>
	<script type="text/javascript" src="/freeflow/dist/all-min.js?<%=timetag %>"></script>
</c:if>

<c:if test="${!productMode || debugMode}">
	<!-- weinre调试用 -->
	<!-- <script src="http://sharetest.kdweibo.cn:8080/target/target-script.js#process"></script> -->
</c:if>