<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="format-detection" content="telephone=no" />
	<title>审批申请</title>
    <link rel="stylesheet" type="text/css" href="/freeflow/css/reset.css" />
	<script type="text/javascript" src="/freeflow/js/app/qingjs.js"></script>
</head>
<body>
	<p style="margin-top: 50px; text-align: center;">获取你的用户信息失败，请<a class="close" href="javascript:;">关闭页面</a>重新进入！</p>
	<script>
	   document.querySelector('.close').onclick = function() {
	   		XuntongJSBridge && XuntongJSBridge.call('close');
	   }
	</script>
</body>
</html>
