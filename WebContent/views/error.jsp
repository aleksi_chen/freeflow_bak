<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<head>
	<meta charset="UTF-8">
	<meta property="qc:admins" content="231167026453463475127636777232507247" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="format-detection" content="telephone=no" />
	<title>错误</title>
    <link rel="stylesheet" type="text/css" href="/freeflow/css/error/reset.css" />
</head>
<body>
	<div class="nothing">
		<div class="nothing-icon"></div>
		<p class="nothing-tips" style="font-size:14px;">
			<c:choose>
                <c:when test='${msg != null && msg!="" }'>
                    ${msg}
                </c:when>
                <c:otherwise>
                   	 抱歉，当前页面已损坏~
                </c:otherwise>
            </c:choose>
		</p>
	</div>
</body>
</html>
