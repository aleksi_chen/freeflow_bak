<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="format-detection" content="telephone=no" />
	<title>审批申请</title>
	
	<%@ include  file="common-settings.jsp"%>
	
	<script>
		__processData = {
			appId: '${CONTEXT.application.mcloudAppId}',
			pubaccId: '${CONTEXT.application.pubacc}',
			applicationId: '${CONTEXT.application.id}',
			networkId: '${CONTEXT.userNetwork.networkId}',
			currUserId: '${CONTEXT.userNetwork.userId}'
		};
	</script>
</head>
<body>
	<section class="process-detail-wrap">
		<script class="process-detail-wrap" type="text/html" id="template">
		<#
			var form = this.form;				<%--审批表单内容--%>
			var replyList = this.replyList;		<%--审批列表数据--%>
			var currUserId = this.currUserId;	<%--当前登陆用户的userId--%>
			var isMyProcess = this.isMyProcess; <%--当前登陆用户是否是申请人--%>
			var isApprover = this.isApprover;	<%--当前登陆用户是否是当前审批人--%>
			var canReply = isApprover && form.status === 1;	<%--是否可以审批，表单状态（0.关闭，1.审批中，2.同意，3.不同意  ）--%>
		#>
		<div class="top">
			<div class="process-detail">
				<span class="applicant-name"><# form.creatorUserName #>&nbsp;</span>
				<span class="process-type">审批申请</span>
				<p class="process-title"><# form.title #></p> 
				<span class="patch"></span>
				  <%--审批结果显示  --%>
	 			 
					<# 
						if(form.status === 2){ 
					#>
						<div class="approval-result approval-result-agree result-animate"></div>	
					<#
						}else if(form.status === 3){
					#>
						<div class="approval-result approval-result-refuse result-animate"></div>	
					<#		
						}
					#>
						
				
			</div>
			<div class="process-content">
				<p class="content-text"><# form.content #></p>
				<div class="addimg-wrapper">
				<div class="upload-img-list  clearfix">
					<%--遍历上传缩略图图片 --%>
					<#	
						for(var i = 0;i < form.fileIds.length;i++){
					#>
							
							<div class="addimg-slide fl pic-preview">
								<img src="http://192.168.22.144/freeflow/c/file/image/get.json?fileId=<#form.fileIds[i]#>"&w=120&h=120/>
								<span class="progress" style="display:none;"></span>
							</div>
						
					<#	
						}
					#>
				</div>
				</div>
			</div>
		</div>
		<div class="reply-list">
			<ul>
				<li class="reply-item clearfix" data-active="active-grey">
					<div class="reply-info fl">
						<span class="time fl">
							<span class="time-inner">
								<# if (Util.isToday(form.createTime)) { #>
									今天 <# new Date(form.createTime).format('hh:mm') #>
								<# } else { #>
									<# new Date(form.createTime).format('MM/dd hh:mm') #>
								<# } #>
							</span>
						</span>
						<img class="photo fl" src="/space/c/photo/load?userId=<# form.creatorUserId #>" alt="头像" />
					</div>
					<div class="text fl view-profile" data-openid="<# form.creatorUserOid #>">
						<div class="text-inner">
							<span class="text-title"><# isMyProcess ? '我' : form.creatorUserName + ' ' #>提交流程</span>
						</div>
					</div>
				</li>
				<%--遍历审批列表 --%>
				<# 
					var item, isLast, isReplyed, isMe, decide;
					for (var i = 0, len = replyList.length; i < len; i++) {
						item = replyList[i];
						isLast = i == len -1;					<%--是否最后一条--%>
						isReplyed = item.status != 1;	<%--审批状态（1.审批中 2.同意 3.不同意）--%>
						isMe = currUserId == item.approverUserId;	<%--当前用户是否该审批记录的审批人--%>
						decide = !isReplyed ? 'waite' : (item.status == 2 ? 'agree' : 'disagree');
				#>
									
				<li class="reply-item clearfix" data-active="active-grey">
					<div class="reply-info fl">
						<span class="time fl">
							<span class="time-inner">
								<# if (isReplyed) {#>
									<# if (Util.isToday(item.createTime)) { #>
										今天 <# new Date(item.createTime).format('hh:mm') #>
									<# } else { #>
										<# new Date(item.createTime).format('MM/dd hh:mm') #>
									<# } #>
								<# } #>
							</span>
						</span>
				<%-- <#
					不是最后一条（肯定不是待审核状态）
					if (!isLast) {
				#>
						<img class="photo fl" src="/space/c/photo/load?userId=<# item.approverUserId #>" alt="头像" />
				<# 
					最后一条
					} else {
						如果未审批且当前用户不是审批人（显示头像）
						if (!isReplyed && !isApprover) {
				#>     
						<img class="photo fl" src="/space/c/photo/load?userId=<# item.approverUserId #>" alt="头像" />

				<#
						} else {
				#>
						<span class="decide-icon <# decide #>-icon"></span>
						<img class="photo fl" src="/space/c/photo/load?userId=<# item.approverUserId #>" alt="头像" />
				<#
						}
					}
				#> --%>
						<img class="photo fl" src="/space/c/photo/load?userId=<# item.approverUserId #>" alt="头像" />
					</div>
					<div class="text view-profile fl<# item.comment || (isApprover && !isReplyed) ? ' has-content' : '' #><# canReply && isMe && isLast ? ' can-reply' : '' #>" data-openid="<# item.approverUserOid #>" data-approverid="<# item.approverId #>">
						<div class="text-inner">
				<#
					<%--已审批 --%>
					if (isReplyed) {
				#>
							<span class="text-title <# isLast ? decide : '' #>">
								<# isMe ? '我' : item.approverUserName + ' ' #><# item.status == 2 ? '已同意' : '不同意' #>
							</span>
							<p class="text-content"><# item.comment #></p>
				<#
					<%--未审批 --%>
					} else {
						<%--是当前审批人 --%>
						if (isApprover) {
				#>
							<span class="text-title waite">等待我审批</span>
							<p class="text-content reply-content">点击添加批复</p>
				<#
						} else {
				#>
							<span class="text-title">等待<span class="blue">&nbsp;<# item.approverUserName #>&nbsp;</span>审批</span>
				<#
						}
					}
				#>
						</div>
					</div>
				</li>
				<# } #>
			</ul>
		</div>
		
		<div class="group-btn <# canReply ? 'choose-btn' : 'share-btn' #>">
		<#
			<%--我的请假已被拒绝 --%>
			if (isMyProcess && form.status == 3) {
		#>
			<a href="/freeflow/c/form/process-request.json?formId=<# form.id #>" class="btn-resubmit btn-full-w">重新提交</a>
		<#
			} else if (canReply) {
		#>
			<button class="btn-disagree btn-no" data-active="active-grey">不同意</button>
			<button class="btn-agree btn-yes" data-active="active-grey">同意</button>
		<#
			} else {
		#>
			<button class="btn-share btn-full-w" data-active="active-grey">知会同事</button>
		<#
			}
		#>
		</div>

		<# if (canReply) { #>
			<%-- 批复弹层 --%>
			<div class="dialog reply-dialog">
				<div class="dialog-main">
					<p class="title">请输入批复</p>
					<input type="text" class="reply-input" />
				</div>
				<div class="group-btn">
					<button class="btn-no">取消</button>
					<button class="btn-yes">确定</button>
				</div>
			</div>
			
			<%-- 转审批弹层 --%>
			<div class="dialog add-approver-dialog">
				<div class="dialog-main">
					<p class="title">还需要其他人审批吗？</p>
				</div>
				<div class="group-btn">
					<button class="btn-yes btn-add">添加审批人</button>
					<button class="btn-yes btn-finish">完成审批</button>
				</div>
			</div>
		<# } #>
		</script>
		<div id="tips-msg"></div>
		<div class="overlayer"></div>
		<div class="loading">
			<div class="circle-container container1 vertical-middle">
				<div class="circle1"></div>
				<div class="circle2"></div>
				<div class="circle3"></div>
				<div class="circle4"></div>
			</div>
			<div class="circle-container container2 vertical-middle">
				<div class="circle1"></div>
				<div class="circle2"></div>
				<div class="circle3"></div>
				<div class="circle4"></div>
			</div>
		</div>
		
		<!--大图预览插件结构  -->
		<div class="swiper-container">
		    <div class="swiper-wrapper">
		       
		    </div>
   			 <!-- 如果需要分页器 -->
    	<!-- 	<div class="swiper-pagination"></div> -->
    		<div class="curPage"></div>
    	</div>
	  	
	    
	</section>
	<script>
		processDetail.init();
	</script>
</body>
</html>
