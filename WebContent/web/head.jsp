<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<%
    String timestamp = "201607271636";  //时间戳
    request.setAttribute("timestamp", timestamp);
    
    String sessionDebugMode = (String)session.getAttribute("debugMode");
    String debugModeParam = (String)request.getParameter("-__");
    
    boolean debugMode = false;
    
    //取消debug模式
    if ("x".equals(debugModeParam)) {
        session.setAttribute("debugMode", "false");
    } else {
        //判断是否debug模式
        if ("true".equals(sessionDebugMode)) {
            debugMode = true;
        } else if ("b".equals(debugModeParam)) {
            debugMode = true;
            session.setAttribute("debugMode", "true");
        }
    }
    
    request.setAttribute("debugMode", debugMode);
    
    String srvName = request.getServerName();
    boolean productMode = false;
    if (srvName!=null) {
        productMode = srvName.endsWith("kdweibo.com") || srvName.endsWith("yunzhijia.com");
    }
    request.setAttribute("productMode", productMode);
    
    request.setAttribute("projectPath", "freeflow");
%>

<%--debug模式 --%>
<c:if test="${debugMode }">
    <link rel="stylesheet" href="/${projectPath}/web/My97DatePicker/skin/WdatePicker.css?${timestamp}" />
    <link rel="stylesheet" href="/${projectPath}/web/css/normalize.css?${timestamp}" />
    <link rel="stylesheet" href="/${projectPath}/web/css/common.css?${timestamp}" />
	<link rel="stylesheet" href="/${projectPath}/web/css/form-design.css?${timestamp}" />
	<link rel="stylesheet" href="/${projectPath}/web/css/approval-manage.css?${timestamp}" />
	<link rel="stylesheet" href="/${projectPath}/web/css/person-select.css?${timestamp}" />
    <link rel="stylesheet" href="/${projectPath}/web/css/freeflow.css?${timestamp}" />
    <link rel="stylesheet" href="/${projectPath}/web/css/dialog.css?${timestamp}" />
</c:if>

<%--非debug模式 --%>
<c:if test="${!debugMode }">
<!--
	<link rel="stylesheet" href="/${projectPath}/dist/css/web-all-min.css?${timestamp}" />
    <link rel="stylesheet" href="/${projectPath}/web/My97DatePicker/skin/WdatePicker.css?${timestamp}" />
-->
    <link rel="stylesheet" href="/${projectPath}/web/My97DatePicker/skin/WdatePicker.css?${timestamp}" />
    <link rel="stylesheet" href="/${projectPath}/web/css/normalize.css?${timestamp}" />
    <link rel="stylesheet" href="/${projectPath}/web/css/common.css?${timestamp}" />
	<link rel="stylesheet" href="/${projectPath}/web/css/form-design.css?${timestamp}" />
	<link rel="stylesheet" href="/${projectPath}/web/css/approval-manage.css?${timestamp}" />
	<link rel="stylesheet" href="/${projectPath}/web/css/person-select.css?${timestamp}" />
    <link rel="stylesheet" href="/${projectPath}/web/css/freeflow.css?${timestamp}" />
    <link rel="stylesheet" href="/${projectPath}/web/css/dialog.css?${timestamp}" />
</c:if>