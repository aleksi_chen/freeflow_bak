/**
 * 弹窗组件，默认提示框自动关闭，确认框不自动关闭
 * @author muqin_deng@kingdee.com
 * @time 2014/10/08
 */

;(function ($, Widget) {
	var zIndex = 9999;

	function Tips () {
		this._timer = null;
		this._$elem = $('');	//要弹出的对话框
	}

	Tips.prototype = $.extend({}, new Widget(), {

		//初始化配置项
		initCfg: function (cfg) {
			this.cfg = {
				title: '',
				content: '',
				cls: '',
				confirmBtnText: '确定',
				cancelBtnText: '取消',
				backDrop: false,	//是否支持点击外部关闭弹出框
				hasMask: false, 	//是否需要朦层
				showTime: 500,		//展示的动画时间
				waiteTime: 1000,	//关闭的等待时间
				hideTime: 400,		//关闭的动画时间
				startTop: 50,		//最初弹出框距离顶部的位置
				endTop: 100,		//最终弹出框距离顶部的位置
				autoClose: null,
				height: null,
				onClose: null,
				onConfirm: null,
				onReady: null,
				onCancel: null
			}

			if (cfg.type == 'confirm') {
				this.cfg.title = '确认';
			}

			$.extend(this.cfg, cfg);

			if (typeof this.cfg.autoClose != 'boolean') {
				if (this.cfg.type === 'confirm') {
					this.cfg.autoClose = false;
				} else if (this.cfg.type === 'alert') {
					this.cfg.autoClose = true;
				}
			}

			//之前弹出的对话框(不包括dialog类型)
			this._$showElem = $('.ui-tips.alert').add('.ui-tips.confirm');	
			this._$mask = $('');
		},

		//生成dom节点
		renderUI: function () {

			this._$elem = $(
				 '<div class="ui-tips ui-tips-shadow ' + (this.cfg.type ? this.cfg.type : '') + ' ' + this.cfg.cls+'">'
				+	'<div class="content-wrap">'
				+		'<div class="content"></div>'
				+	'</div>'
				+'</div>'
			);

			this._$elem.find('.content').append(this.cfg.content);

			if (!this.cfg.autoClose) {
				this._$elem.find('.content-wrap').before('<span class="close" title="关闭">×</span>');
			}

			if (this.cfg.title != '') {
				this._$elem.find('.content-wrap').before(
					 '<div class="title-wrap">'
					+	'<h3 class="title">' + this.cfg.title + '</h3>'
					+'</div>'
				);
			}

			if (this.cfg.type === 'confirm') {
				this._$elem.append(
					 '<div class="foot-wrap">'
					+	'<button class="btn btn-info btn-sm yes">' + this.cfg.confirmBtnText + '</button>'
					+	'<button class="btn btn-default btn-sm no">' + this.cfg.cancelBtnText + '</button>'
					+'</div>'
				);
			}

			if (this.cfg.hasMask) {
				var $body = $(document.body);

				$body.addClass('ui-tips-open');

				if (this._isOverflow()) {
					$body.addClass('padding');
				}

				this._$elem = $('<div class="ui-tips-wrap mask"></div>').append(this._$elem);

				//朦层
				this._$mask = $('<div class="ui-tips-mask"></div>').css({'display': 'none'});
				this._$mask.css('z-index', zIndex++);
				$body.append(this._$mask);

				this._$mask.fadeIn(200);
			}

			this._$elem.css('z-index', zIndex++);
		},

		//绑定事件
		bindUI: function () {
			var me = this,
				$tar = this._$elem;

			if (this.cfg.hasMask) {
				$tar = this._$elem.find('.ui-tips');
			}

			$tar.on('click', '.close', function (e) {
				me.on('hide', function () {
					me.fire('close');
				});
				me.destroy();
			});

			if (this.cfg.type == 'confirm') {
				$tar.on('click', '.yes', function (e) {
					me.on('hide', function () {
						me.fire('confirm');
					});
					me.destroy();
				})
				.on('click', '.no', function (e) {
					me.on('hide', function () {
						me.fire('cancel');
						me.fire('close');
					});
					me.destroy();
				});
			}
			
			if (this.cfg.onClose) {
				this.on('close', this.cfg.onClose);
			}
			if (this.cfg.onConfirm) {
				this.on('confirm', this.cfg.onConfirm);
			}
			if (this.cfg.onCancel) {
				this.on('cancel', this.cfg.onCancel);
			}
			if (this.cfg.onReady) {
				this.on('ready', this.cfg.onReady);
			}
			if (this.cfg.onDestroy) {
				this.on('destroy', this.cfg.onDestroy)
			}
			//是否支持点击非弹窗部分关闭弹窗
			if (this.cfg.backDrop && !this.cfg.autoClose) {
				$(document).off('click', me.destroy).click($.proxy(me.destroy, me));
				$tar.on('click', function (e) {
					e.stopPropagation();
				});	
			}
		},

		//同步设置初始样式
		syncUI: function () {
			var $tar = this._$elem;

			if (this.cfg.hasMask) {
				$tar = this._$elem.find('.ui-tips');
			};

			if (typeof this.cfg.height != 'null') {
				$tar.css('height', this.cfg.height + 'px');
			}

			$tar.css('top', this.cfg.startTop);
		},

		//实例化后的操作
		ready: function () {
			var $tar = this._$elem,
				width;
				
			if (this.cfg.hasMask) {
				$tar = this._$elem.find('.ui-tips');
			};

			width = this.cfg.width ? this.cfg.width : $tar.width();

			$tar.css({
				'width': width + 'px'
			});

			if (!this.cfg.hasMask) {
				width = -width/2; 
				if ($('.ui-tips-mask').length) {
					 width -= 17/2;
				}

				$tar.css('margin-left', width + 'px');
			}
				
			this.show();
		},

		//销毁函数
		destroy: function () {
			var me = this;

			this.on('hide', function () {
				me.cfg.backDrop && !me.cfg.autoClose && $(document).off('click', me.destroy);

				me._timer && window.clearTimeout(me._timer);

				if (this.cfg.hasMask) {
					if (me._isOverflow()) { 
						$('body').removeClass('padding'); 
					}
					$('body').removeClass('ui-tips-open');
					me._$mask.fadeOut(200, function () {
						$(this).remove();
						me.fire('destroy');
					});
					me._$elem.find('.ui-tips').off();
				} else {
					me.fire('destroy');
				}
				
				me._$elem.off().remove();

			});
			this.hide();
		},

		//自动隐藏
		_autoHide: function () {
			var me = this;

			this._timer =  window.setTimeout(function () {
				me.on('hide', function () {
					me.fire('close');
				});
				me.destroy();
			}, this.cfg.waiteTime + this.cfg.showTime);
		},

		//展示动画效果
		show: function () {
			var me = this,
				$tar = this._$elem;

			if (this.cfg.hasMask) {
				$tar = this._$elem.find('.ui-tips');
			};

			if (this._$showElem.length > 0) {
				this._$showElem.each(function () {
					if ($(this).is(':hidden')) {
						$(this).remove();
					} else {
						$(this).hide();
					}
				});
			}
			
			this._timer && window.clearTimeout(this._timer);

			this.cfg.autoClose && this._autoHide();

			$tar.stop(true)
						.animate({
							'top': me.cfg.endTop + 'px',
							'opacity': 1
						}, this.cfg.showTime, function () {
							me.fire('ready');
						});

		},

		//隐藏弹窗
		hide: function () {
			var me = this,
				$tar = this._$elem;

			if (this.cfg.hasMask) {
				$tar = this._$elem.find('.ui-tips');
			};

			$tar.animate({
				'top': me.cfg.startTop + 'px',
				'opacity': 0
			}, this.cfg.hideTime, function(){
				me.fire('hide');
			})
		},

		//取得所有对话框的最高zIndex值
		_getMaxZIndex: function () {
			var arr = [0];
			$('.ui-tips').add('.ui-tips-mask').add('.ui-tips-wrap').each(function () {
				arr.push($(this).css('z-index') || 1);
			});
			//倒序排序
			arr.sort(function (a, b) {
				return b-a;
			});
			return +arr[0];
		},

		//将传入配置项转换
		_tranceCfg: function (cfg) {
			var config = {};
			if (typeof cfg === 'string') {
				config = {content: cfg};
			} else if (typeof cfg === 'object') {
				config = cfg;
			} else {
				return false;
			}
			return config;
		},

		//判断是否给body添加padding
		_isOverflow: function () {
			return $(document).height() > $(window).height();
		},

		//提示框
		alert: function (cfg) {			
			var config = this._tranceCfg(cfg);

			config.type = 'alert';
			this.render(config);
			return this;
		},

		//确认框
		confirm: function (cfg) {
			var config = this._tranceCfg(cfg);

			config.type = 'confirm';
			this.render(config);
			return this;
		},

		//通用框
		dialog: function (cfg, container) {
			var config = this._tranceCfg(cfg);

			config.type = 'dialog';
			this.render($.extend({
				autoClose: false,
				hasMask: true
			}, config), container);
			return this;
		}
	});

	window.Tips = Tips;
})(jQuery, F.d.Widget);
