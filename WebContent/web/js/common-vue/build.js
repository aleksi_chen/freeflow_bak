/**
 * Created by yuce_wang on 2016/8/10.
 * r.js测试环境打包配置
 * 执行：
 * if !r.js
 *  npm install requirejs -g
 * r.js -o build.js
 * 2016-07-20
 * 参考：https://github.com/requirejs/r.js/blob/master/build/example.build.js
 */
({
    baseUrl: '.',
    paths: {
        'Vue': '../lib/vue.min',
        'Vuex': '../lib/vuex.min'
    },

    generateSourceMaps: true,
    preserveLicenseComments: false,

    name: "approval-rule",
    exclude: [
        'Vue',
        'Vuex'
    ],
    findNestedDependencies: true,
    out: 'approval-rule.min.js',
    logLevel: 0
})