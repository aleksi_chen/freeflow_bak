define(['Vue', 'Vuex'], function (vue, Vuex) {
    vue.use(Vuex)
    var state = {
        templateId: "",//审批模板id
        templateName: "",//审批模板名称
        approvalStep: 0,//当前审批步骤
        approvalStepCount: 2,//审批步骤总数
        personRuleType: 0,//当前审批规则配置类型
        personSelectList: {},//不限条件审批人列表对象
        openDialog: false,//悬浮框状态
        conditionSelect: 0,//按条件审批人选中组件id
        conditionList: [],//按条件审批人 条件数据列表
        conditionCount: 0,//按条件审批人 符合展现的条件数量
        conditionNumberData: {},//按条件审批人 数字输入条件区间数据
        conditionPersonSelect: false,//按条件审批人 是否展现 选择审批人
        conditionPersonData: {},//按条件审批人 条件区间对应选中审批人数据
        conditionComplete: {}//按条件审批人 对应条件是否已设置审批人
    };

    var mutations = {
        SETTEMPLATEID: function (state, data) {
            state.templateId = data;
        },
        SETTEMPLATENAME: function (state, data) {
            state.templateName = data;
        },
        SETAPPROVALSTEP: function (state, data) {
            state.approvalStep = data;
        },
        SETPERSONRULETYPE: function (state, data) {
            state.personRuleType = data;
        },
        SETPERSONSELECTLIST: function (state, data) {
            state.personSelectList = JSON.parse(JSON.stringify(data))
        },
        TOGGLEOPENDIALOG: function (state) {
            state.openDialog = !state.openDialog;
        },
        SETCONDITIONSELECT: function (state, data) {
            state.conditionSelect = data;
        },
        SETCONDITIONLIST: function (state, data) {
            state.conditionList = data;
        },
        SETCONDITIONCOUNT: function (state, data) {
            state.conditionCount = data;
        },
        SETCONDITIONNUMBERDATA: function (state, data) {
            state.conditionNumberData = vue.util.extend({}, data);
        },
        SETONECONDITIONNUMBER: function (state, id, data) {
            state.conditionNumberData[id] = data;
        },
        SETCONDITIONPERSONSELECT: function (state, data) {
            state.conditionPersonSelect = !state.conditionPersonSelect;
        },
        SETCONDITIONPERSONDATA: function (state, id, data) {
            state.conditionPersonData[id] = data.slice(0)
            state.conditionPersonData = vue.util.extend({}, state.conditionPersonData)
        },
        SETCONDITIONCOMPLETE: function (state, id, data) {
            state.conditionComplete[id] = data;
            state.conditionComplete = vue.util.extend({}, state.conditionComplete)
        }
    }

    var store = new Vuex.Store({
        state: state,
        mutations: mutations,
        strict: false
    })
    return store;
});