/**
 * Created by yuce_wang on 2016/7/21.
 */
define(['Vue', 'vuex/store', 'vuex/getter', 'vuex/action'], function (vue, store, getter, action) {
    var template = [
        '<div class="ff-terms-item" @click="setConditionSelect(index)">',
        '<i class="ff-item-radio" :class={\'ff-item-radio-active\':select}></i>',
        '<div class="ff-item-title">{{condition.label}}{{fail?\'（失效）\':\'\'}}</div>',
        '<div v-show="select"><div class="ff-leave-type">',
        '<span v-for="option in condition.options">{{option}}</span>',
        '</div>',
        '<div><button class="ff-btn"  @click.stop="conditionSet">设置审批人</button></div>',
        '</div></div>'].join('')
    return vue.extend({
        template: template,
        vuex: {
            getters: {
                conditionList: getter.conditionList,
                conditionSelect: getter.conditionSelect,
                conditionPersonData: getter.conditionPersonData,
                conditionComplete: getter.conditionComplete
            },
            actions: {
                setConditionSelect: action.setConditionSelect,
                conditionPersonSet: action.conditionPersonSet
            }
        },
        props: {
            index: {
                type: Number,
                required: true
            }
        },
        computed: {
            condition: function () {
                return this.conditionList[this.index]
            },
            select: function () {
                return this.conditionSelect == this.index && this.conditionSelect != undefined
            },
            fail: function () {
                return this.conditionComplete && this.conditionComplete[this.condition.id] && !this.select
            }
        },
        methods: {
            conditionSet: function () {
                var data = {
                    conditionList: this.conditionList,
                    conditionSelect: this.conditionSelect,
                    conditionPersonData: this.conditionPersonData,
                }
                this.conditionPersonSet(data)
            }
        }
    })
})