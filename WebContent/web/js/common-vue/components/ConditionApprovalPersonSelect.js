/**
 * Created by yuce_wang on 2016/7/25.
 */
define(['Vue', 'vuex/store', 'vuex/getter', 'vuex/action'], function (vue, store, getter, action) {
    var template = [
        '<div class="ff-type-set-admin">',
        '<template v-for="condition in conditionPerson"><div class="ff-interva-item">',
        '<div class="ff-interva-item-title">{{{condition.showTitle}}}</div>',
        '<div class="ff-addadmin-container">',
        '<template v-for="person in condition.usersContext">',
        '<div class="ff-admin-item"><img class="ff-admin-avatar" :src="person.photoUrl"><div class="ff-admin-name ellipsis">{{person.userName}}</div></div>',
        '</template>',
        '<div class="ff-addadmin-item" @click="addConditionPersonList(id,$index,conditionPerson)"><div class="ff-add-icon"></div><div class="ellipsis">添加审批人</div></div></div></div></template>',
        '</div>'].join('')
    return vue.extend({
        template: template,
        vuex: {
            getters: {
                conditionSelect: getter.conditionSelect,
                conditionList: getter.conditionList,
                conditionPersonData: getter.conditionPersonData
            },
            actions: {
                addConditionPersonList: action.addConditionPersonList
            }
        },
        computed: {
            id: function () {
                return this.conditionList[this.conditionSelect] && this.conditionList[this.conditionSelect].id
            },
            conditionPerson: function () {
                return this.conditionPersonData[this.id]
            }
        }
    })
})