/**
 * Created by yuce_wang on 2016/7/21.
 */
define(['Vue', 'vuex/store', 'vuex/getter', 'vuex/action', 'components/ConditionRadioSet', 'components/ConditionNumberSet', 'components/ConditionApprovalPersonSelect'], function (vue, store, getter, action, ConditionRadioSet, ConditionNumberSet, ConditionApprovalPersonSelect) {
    var template = [
        '<div v-show="personRuleType==1">',
        '<div v-show="!conditionPersonSelect&&conditionCount!=0" class="ff-terms-container">',
        '<template v-for="condition in conditionList">',
        '<condition-radio-set  v-if="condition.type==\'radiofield\'&&condition.require==true" :index="$index"></condition-radio-set>',
        '<condition-number-set v-if="(condition.type==\'numberfield\'||condition.type==\'moneyfield\')&&condition.require==true" :index="$index"></condition-number-set>',
        '</template></div>',
        '<condition-approval-person-select v-show="conditionPersonSelect"></condition-approval-person-select>',
        '<div class="ff-warn-container" v-if="conditionCount==0">',
        '<div class="ff-warn-icon"></div>',
        '<p class="ff-warn-content"><span>您的&nbsp;"{{templateName}}"&nbsp;中没有可以作为审批条件的控件</span>',
        '<small>“数字输入框”、“金额”、“单选框”控件,并且设置为必填项后,可以做为审批条件。</small>',
        '</p></div></div>'].join('')
    return vue.extend({
        template: template,
        vuex: {
            getters: {
                templateName:getter.templateName,
                personRuleType: getter.personRuleType,
                conditionList: getter.conditionList,
                conditionCount: getter.conditionCount,
                conditionPersonSelect: getter.conditionPersonSelect
            },
            actions: {}
        },
        components: {
            ConditionRadioSet: ConditionRadioSet,
            ConditionNumberSet: ConditionNumberSet,
            ConditionApprovalPersonSelect: ConditionApprovalPersonSelect
        }
    })
})