/**
 * Created by yuce_wang on 2016/7/19.
 */
define(['Vue', 'vuex/store', 'vuex/getter', 'vuex/action'], function (vue, store, getter, action) {
    var template = [
        '<div class="fd-design-step"  :class="{\'fd-design-active\': approvalStep+1==step}">',
        '<span class="fd-design-step-icon">{{step}}</span>',
        '<p class="fd-design-step-text">{{name}}</p>',
        '</div>'].join('')
    return vue.extend({
        template: template,
        vuex: {
            getters: {
                approvalStep: getter.approvalStep,
            },
            actions: {}
        },
        props: {
            step: {
                type: String,
                required: true
            },
            name: {
                type: String,
                required: true
            }
        }
    })
})