/**
 * Created by yuce_wang on 2016/7/21.
 */
define(['Vue', 'vuex/store', 'vuex/getter', 'vuex/action'], function (vue, store, getter, action) {
    var template = [
        '<div class="ff-terms-item" @click="setConditionSelect(index)">',
        '<i class="ff-item-radio" :class={\'ff-item-radio-active\':select}></i>',
        '<div class="ff-item-title">{{condition.label}}{{fail?\'（失效）\':\'\'}}</div>',
        '<div v-show="select"><div class="ff-leave-number">',
        '<div class="ff-leave-explainr">请输入“{{condition.label}}”的分隔条件，我们将把数值区域作为审批条件</div>',
        '<div class="ff-leave-day">',
        '<template  v-for="option in numbers" track-by="$index">',
        '<div class="ff-leave-interval">',
        '<input type="text" :class="{\'error\': errArr.indexOf($index)>-1}" @mouseover="mouseover($index)" @mouseout="mouseout" @input="input($index,$event)" v-model="numbers[$index]">',
        '<span class="ff-interval-text">≤</span>',
        '<i class="ff-interval-clear" :style="{ opacity: showIndex==$index?1:0 }" @mouseover="mouseover($index)" @mouseout="mouseout" @click="del($index)"></i>',
        '</div></template>',
        '<div class="ff-leave-add"  @click="add()"><span></span></div></div></div>',
        '<div><button class="ff-btn" @click.stop="conditionSet">设置审批人</button></div>',
        '</div></div>'].join('')
    return vue.extend({
        template: template,
        vuex: {
            getters: {
                conditionList: getter.conditionList,
                conditionSelect: getter.conditionSelect,
                conditionNumberData: getter.conditionNumberData,
                conditionPersonData: getter.conditionPersonData,
                conditionComplete: getter.conditionComplete
            },
            actions: {
                setConditionSelect: action.setConditionSelect,
                conditionPersonSet: action.conditionPersonSet,
                setOneConditionNumber: action.setOneConditionNumber,
                numberConditionSort: action.numberConditionSort
            }
        },
        data: function () {
            return {
                showIndex: null,//当前删除图标显示的index
                errArr: [],//验证错误数组
                numbers:[] //数字条件数组
            }
        },
        props: {
            index: {
                type: Number,
                required: true
            }
        },
        created:function () {
            this.numbers=[].concat(this.conditionNumberData[this.condition.id])
        },
        computed: {
            condition: function () {
                return this.conditionList[this.index]
            },
            select: function () {
                return this.conditionSelect == this.index && this.conditionSelect != undefined
            },
            fail: function () {
                return this.conditionComplete && this.conditionComplete[this.condition.id] && !this.select
            }
        },
        methods: {
            conditionSet: function () {
                var result = this.numberConditionSort(this.numbers, this.condition.type)
                data = {
                    conditionList: this.conditionList,
                    conditionSelect: this.conditionSelect,
                    conditionPersonData: this.conditionPersonData,
                    conditionNumberData: this.conditionNumberData
                }
                if (result.success) {
                    this.numbers=result.arr.slice(0)
                    this.setOneConditionNumber(this.condition.id, result.arr)
                    this.conditionPersonSet(data)
                } else {
                    this.errArr = result.errArr
                    this.$msg(result.msg)
                }
            },
            input: function ($index,$event) {
                var idx = this.errArr.indexOf($index)
                idx > -1 && this.errArr.splice(idx, 1)
                var reg=new RegExp(),condition=this.condition
                if(condition.type=='numberfield'){
                    if(condition.decimal===false){
                        reg=/^\d{0,15}/  //不支持负数 //最大位数限制
                    }else{
                        reg=/(^\d{0,15}\.\d{0,15}|^\d{0,15}\.|^\d{0,15})/
                    }
                }else if(condition.type=='moneyfield'){
                    // reg=/(^-*\d{0,9}\.\d{1,2}|^-*\d{0,9}\.|^-*\d{0,9})/
                    reg=/(^\d{0,9}\.\d{1,2}|^\d{0,9}\.|^\d{0,9})/
                }
                var value=$event.target.value
                this.numbers.splice($index,1,reg.exec(value)[0])
            },
            mouseover: function ($index) {
                this.showIndex = $index
            },
            mouseout: function () {
                this.showIndex = null
            },
            del: function ($index) {
                if (this.numbers.length > 1) {
                    this.numbers.splice($index, 1)
                }
            },
            add: function () {
                this.numbers.push(0)
            }
        }
    })
})