/**
 * Created by yuce_wang on 2016/7/20.
 */
define(['Vue', 'vuex/store', 'vuex/getter', 'vuex/action'], function (vue, store, getter, action) {
    var template = [
        '<div v-show="personRuleType==2" class="ff-addadmin-container">',
        '<div class="ff-admin-item" v-for="person in personSelectList">',
        '<img class="ff-admin-avatar" :src="person.photoUrl">',
        '<div class="ellipsis">{{person.userName}}</div></div>',
        '<div class="ff-addadmin-item" @click="addPersonSelectList(personSelectList)">',
        '<div class="ff-add-icon"></div>',
        '<div class="ellipsis">添加审批人</div>',
        '</div></div>'].join('')
    return vue.extend({
        template: template,
        vuex: {
            getters: {
                personRuleType: getter.personRuleType,
                personSelectList: getter.personSelectList
            },
            actions: {
                addPersonSelectList: action.addPersonSelectList
            }
        }
    })
})