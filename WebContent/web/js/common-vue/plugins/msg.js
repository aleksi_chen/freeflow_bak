/**
 * Created by yuce_wang on 2016/7/28.
 */
define(['Vue'
], function (vue) {
    var alertInstance = null;

    function encode(txt) {
        if (typeof txt != "string") return "";
        txt = txt.replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/\"/g, "&quot;")
            .replace(/\'/g, "&#39;")
            .replace(/ /g, "&nbsp;")
        return txt;
    }

    function msg() {
        var template = ['<div class="q-bg" :style="{display:display?\'block\':\'none\'}"><div class="q-alert"><div class="q-alert-heard">',
            '<span class="q-alert-closebtn" @click="dismiss"></span>',
            '</div><div class="q-alert-body">',
            '<h1 class="q-alert-title text-center">提示</h1>',
            '<p class="q-alert-text text-center">{{message}}</p>',
            '</div><div class="q-alert-button-group text-center">',
            '<a class="q-btn btn-default" v-if="!onlyAlert" @click="cancle">取消</a>',
            '<a class="q-btn btn-primary" @click="confirm">确定</a>',
            '</div></div></div>'].join('')
        return vue.extend({
            template: template,
            methods: {
                confirm: function () {
                    this.display = false
                    this.$dispatch('confirm', {
                        action: 'confirm'
                    })
                },
                cancle: function () {
                    this.display = false
                    this.$dispatch('cancel', {
                        action: 'cancel'
                    })
                },
                dismiss: function () {
                    this.display = false
                    this.$dispatch('dismiss', {
                        action: 'dismiss'
                    })
                }
            }
        })
    }

    function install() {
        var AlertConstructor = msg();
        Object.defineProperty(vue.prototype, '$msg', {
            get: function () {
                return function (data) {
                    var options = {
                        message: '',
                        confirm: function () {
                        },
                        cancel: function () {
                        },
                        dismiss: function () {
                        },
                        onlyAlert: true
                    }
                    if (typeof data == 'string') {
                        options.message = data
                    } else {
                        vue.util.extend(options, data)
                    }

                    if (alertInstance) {
                        alertInstance.$destroy(true)
                    }
                    alertInstance = new AlertConstructor({
                        el: document.createElement('div'),
                        data: function () {
                            return {
                                message: encode(options.message),
                                display: true,
                                onlyAlert: options.onlyAlert
                            }
                        }
                    });
                    alertInstance.$on('cancel', function () {
                        options.cancel('cancel')
                    })
                    alertInstance.$on('confirm', function () {
                        options.confirm('confirm')
                    })
                    alertInstance.$on('dismiss', function () {
                        options.dismiss('dismiss')
                    })
                    alertInstance.$appendTo(document.body)
                }
            }
        });
    }

    vue.use(install)
});