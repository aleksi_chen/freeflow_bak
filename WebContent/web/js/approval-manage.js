;
(function (window, doc, factory) {
    factory = factory(window, doc);

    window.F.d.Manage = window.F.d.Manage || factory;

})(window, document, function (window, doc) {
    var Paginator = F.d.Paginator;
    var OrgTree = F.d.OrgTree;
    var urlParam = 'webLappToken=' + webUtil.getRequestKey()['webLappToken'] + '&lappName=freeflow';
    var data = {
        //createTplUrl: '/freeflow/web/form-design.json?' + urlParam,
        createTplUrl: '/freeflow/web/manager/formdesign?' + urlParam,
        operationLogUrl: '/freeflow/v1/rest/form-template/list/operationLogs?' + urlParam, //操作日志  OK
        tabListRemoveUrl: '/freeflow/v1/rest/form-template/update/formTemplate?' + urlParam, //表格列表删除  OK
        checkApprovalNameUrl: '/freeflow/v1/rest/form-template/check/templateTitle?' + urlParam, //检查审批名称是否已经存在
        createApproval: '/freeflow/v1/rest/form-template/create/formTemplate?' + urlParam,//新建模板
        //审批类型
        queryApprovalType: {
            url: '/freeflow/v1/rest/approval/list/approvalTypes?' + urlParam,
            limit: 10,
            pageIndex: 1,
            first: true
        },
        //审批模板配置--表格
        approvalTemplateConfiguration: {
            url: '/freeflow/v1/rest/form-template/list/formTemplates?' + urlParam,
            limit: 10,
            pageIndex: 1,
            totalSize: null
        },
        //审批数据导出--表格
        approvalDataExport: {
            url: '/freeflow/v1/rest/approval/list/approvalApplys?' + urlParam,
            limit: 10,
            pageIndex: 1,
            first: true,
            totalSize: null
        },
        approvalTemplatePaginator: null, //审批模板分页
        approvalDataExportPaginator: null, //审批数据分页

        operationLogType: ['删除了', '创建了', '修改了'], //操作日志类型
        status: {
            blockUp: '-1', //停用
            notEnabled: '0', //未启用
            alreadyEnabled: '1' //已启用
        },
        //审批打印接口
        approvalPrint: '/freeflow/v1/rest/approval/print/approvalApply/',
        //查询数据
        checkData: {
            userName: null, //发起人姓名
            userId: '', //发起人id                    
            deptId: '', //查询部门id
            limit: 10,
            pageIndex: 1,
            totalSize: null
        },
        viewDetailUrl: '/freeflow/m/approval/', //审批详情接口

        checkDataIsClick: false, //是否点击过查询
        isSelectPageClick: false, //是否点击页码
        isStartData: true, //控制ajax请求 启用
        isStopData: true ////控制ajax请求 停用
    };

    var tipMsg = {
        stopSuc: '已停用！',
        activeSuc: '已启用！',
        updateFail: '操作失败，请稍后再试！',
        delSuc: '删除成功！',
        delFail: '删除失败，请稍后再试！',
        approvalType: '获取审批类型失败，请稍后再试！',
        noData: '<tr><td class="am-manage-no-data" colspan="5">无数据</td></tr>',
        checkDateFail: '查询失败，请稍后再试！'
    };

    //生成数据模板
    function templateEngine(html, options) {
        var re = /<%(.*?)%>/g,
            reExp = /(^(\s|\t|\n|\r)*(var|if|for|else|switch|case|break|{|}))(.*)?/g,
            code = 'var r=[];\n',
            cursor = 0;

        html = html.replace(/\n|\r|\t/g, '');

        var add = function (line, js) {
            js ? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
                (code += line !== '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
            return add;
        };

        while (match = re.exec(html)) {
            var noJsStr = html.slice(cursor, match.index);
            add(/[^\s]/g.test(noJsStr) ? noJsStr : '')(match[1], true);
            cursor = match.index + match[0].length;
        }
        add(html.substr(cursor, html.length - cursor));
        code += 'return r.join("");';

        return new Function(code.replace(/[\r\t\n]/g, '')).apply(options ? options : {});
    }

    //生成表格列表数据
    function renderUITabList(curPage, limit) {
        var paramStr = {
            query: {
                //"title":"", //标题
                //"code":"",  //类型
                //"user":{}// 简单用户信息
                //"ltSaveTime":22222222,// 大于保存时间
                //"gtSaveTime":11111111// 小于保存时间
                //"isPublic": false,// 是否公开   
                "isDelete": false, // 是否删除
                "appName": "freeflow"
            },
            "order": "-createTime ",
            "start": curPage,
            "limit": limit,
            "isTotal": true
        };
        var optData = {
            url: data.approvalTemplateConfiguration.url,
            param: JSON.stringify(paramStr)
        }

        getTabListAjax(optData).done(function (resp) {
            //console.log(resp);
            data.approvalTemplateConfiguration.totalSize = resp.data.total;
            var dataArr = resp.data.templates;
            var html = '';
            if (!dataArr.length) {
                noData('.am-manage-tbody', tipMsg.noData);
                return;
            }
            for (var i = 0, len = dataArr.length; i < len; i++) {
                //表格数据

                var tpl = [
                    '<tr data-status="<% this.status %>" data-isPublic="<% this.isPublic %>" data-id="<% this.id %>">',
                    '<td><% this.title %></td>',
                    '<% if(this.status == this.statusNum.alreadyEnabled) { %>',
                    '<td class="am-skyblue">已启用</td>',
                    '<% } else if(this.status == this.statusNum.blockUp) { %>',
                    '<td class="am-red">已停用</td>',
                    '<% } else{ %>',
                    '<td>未启用</td>',
                    '<% } %>',
                    '<td><% this.userName %></td>',
                    //'<td><% this.createTime %></td>',
                    '<td>',
                    '<% if(this.isPublic) { %>',
                    //'<a class="am-edit-btn" href="javascript:void(0)">编辑</a>',
                    //'<a class="approval-person-set-btn am-skyblue" href="javascript:void(0)">审批人设置</a>',
                    '<% if(this.status == this.statusNum.alreadyEnabled) { %>',
                    '<a class="am-stop-btn am-red" href="javascript:void(0)">停用</a>',
                    '<% } else {%>',
                    '<a href="javascript:;" class="am-start-btn am-skyblue">启用</a>',
                    //'<a class="am-del-btn am-red" href="javascript:void(0)">删除</a>',
                    '<% } %>',
                    '<% } else { %>',
                    '<a class="am-edit-btn am-skyblue" href="javascript:void(0)">编辑</a>',
                    //'<a class="approval-person-set-btn am-skyblue" href="javascript:void(0)">审批人设置</a>',
                    '<% if(this.status == this.statusNum.alreadyEnabled) { %>',
                    '<a class="am-stop-btn am-red" href="javascript:void(0)">停用</a>',
                    '<% } else {%>',
                    '<a href="javascript:void(0) am-skyblue" class="am-start-btn">启用</a>',
                    '<a class="am-del-btn am-red" href="javascript:void(0)">删除</a><% } %>',
                    '<% } %>',
                    '</td>',
                    '</tr>'
                ].join('');
                //am-skyblue
                //resp[i] = $.extend(dataArr[i], {createTime: dateFormat(dataArr[i].datetime)});
                html += templateEngine(tpl, {
                    'status': dataArr[i].status,
                    'createTime': dateFormat(dataArr[i].createTime),
                    'isPublic': dataArr[i].isPublic,
                    'title': dataArr[i].title,
                    'userName': dataArr[i].user ? dataArr[i].user.name : '',
                    'id': dataArr[i].id,
                    'statusNum': data.status
                });
            }
            $('.am-manage-tbody').html('');
            $('.am-manage-tbody').html(html);

            if (data.hasInitPaginatorF != true) {
                //分页
                var opt = {
                    'target': '.approval-template-pagiantion',
                    'pageSize': data.approvalTemplateConfiguration.limit,
                    'currentPage': data.approvalTemplateConfiguration.pageIndex,
                    'itemsTotal': data.approvalTemplateConfiguration.totalSize
                };
                initPaginatorTabF(opt);
                data.hasInitPaginatorF = true;
            }

        }).fail(function () {
            console.log('生成表格列表数据失败')
        })
    }

    //生成公用模板数据
    function renderComList(curPage, limit) {
        var paramStr = {
            query: {
                //"title":"", //标题
                //"code":"",  //类型
                //"user":{}// 简单用户信息
                //"ltSaveTime":22222222,// 大于保存时间
                //"gtSaveTime":11111111// 小于保存时间
                "isGlobal": true, // 是否为原始模板
                "isPublic": true, // 是否公开
                "isDelete": false, // 是否删除
                "appName": "freeflow"
            },
            "order": "-createTime",
            "start": curPage,
            "limit": limit,
            "isTotal": true
        };
        var optData = {
            url: data.approvalTemplateConfiguration.url,
            param: JSON.stringify(paramStr)
        }

        getTabListAjax(optData).done(function (resp) {
            //console.log(resp);
            data.approvalTemplateConfiguration.totalSize = resp.data.total;
            var dataArr = resp.data.templates;
            var html = '';
            if (!dataArr.length) {
                //noData('.am-manage-tbody', tipMsg.noData);
                return;
            }
            for (var i = 0, len = dataArr.length; i < len; i++) {
                //公共模板数据
                var tpl = [
                    '<li class="am-popbox-item" id="<% this.id %>">',
                    '<img src="../../lightapp/c/file/image/get.json?fileId=<% this.photo %>" >',
                    '<p><% this.title %></p>',
                    '</li>'
                ].join('');

                //resp[i] = $.extend(dataArr[i], {createTime: dateFormat(dataArr[i].datetime)});
                html += templateEngine(tpl, {
                    'title': dataArr[i].title,
                    'id': dataArr[i].id,
                    'photo': dataArr[i].photo
                });
            }
            //			$('.am-popbox-list').html('');
            //			$('.am-popbox-list').html(html);

            $('.am-blank').nextAll().remove();
            $('.am-blank').after(html);

            if (data.hasInitPaginatorF != true) {
                //分页
                var opt = {
                    'target': '.approval-template-pagiantion',
                    'pageSize': data.approvalTemplateConfiguration.limit,
                    'currentPage': data.approvalTemplateConfiguration.pageIndex,
                    'itemsTotal': data.approvalTemplateConfiguration.totalSize
                };
                initPaginatorTabF(opt);
                data.hasInitPaginatorF = true;
            }

        }).fail(function () {
            console.log('生成公用模板数据失败')
        })
    }

    //tab1 分页  ----   审批模板
    function initPaginatorTabF(opt) {
        data.approvalTemplatePaginator = new Paginator({
            'target': opt.target,
            'className': '',
            'edgeNum': 2,
            'middleNum': 7,
            'pageSize': opt.pageSize,
            'currentPage': opt.currentPage,
            'itemsTotal': opt.itemsTotal
        });
        data.approvalTemplatePaginator.on('selectPage', function (page) {
            //console.log(page);
            //点击页码刷新数据
            renderUITabList(page, data.approvalTemplateConfiguration.limit);

        });
    }

    //tab2 分页  ---- 审批数据导出
    function initPaginatorTabS(opt) {
        data.approvalDataExportPaginator = new Paginator({
            'target': opt.target,
            'className': '',
            'edgeNum': 2,
            'middleNum': 7,
            'pageSize': opt.pageSize,
            'currentPage': opt.currentPage,
            'itemsTotal': opt.itemsTotal
        });
        var a = 1;
        data.approvalDataExportPaginator.on('selectPage', function (page) {
            //console.log(page);
            //点击页码刷新数据
            //第一次加载全部注册页码点击事件
            data.isSelectPageClick = true;
            approvalDataExport(page, data.approvalDataExport.limit, getCheckData());

        });
    }

    //操作日志列表
    function operationLog() {
        var optData = {
            url: data.operationLogUrl,
            param: {
                query: {
                    //"operation":"", //操作类型
                    //"traget":"", //关联主体
                    //"tragetId":"",  //关联id
                    //"user":{}// 简单用户信息
                    //"ltSaveTime":22222222,// 大于保存时间
                    //"gtSaveTime":11111111// 小于保存时间
                    "isDelete": false // 是否删除
                },
                order: "createTime",
                start: 1,
                limit: 10,
                isTotal: true
            }
        }
        optData.param = JSON.stringify(optData.param);


        getTabListAjax(optData).done(function (resp) {
            //console.log(resp)
            var html = '';
            var dataArr = resp.data.templates;
            if (!dataArr.length) {
                return;
            }
            //operation 0 ---- 删除  1 -----  创建   2 ----- 修改
            for (var i = 0; i < dataArr.length; i++) {
                var tpl = [
                    '<li>',
                    '<span class="operation-log-username"><% this.userName %>&nbsp;</span>',
                    '<span class="operation-log-create-time">于<b class="<% this.createTimeTitle %>"><% this.createTime %>&nbsp;</b></span>',
                    '<span class="operation-log-type<% this.className %>"><b><% this.createType %></b>审批模板</span>',
                    '<span class="operation-log-title">【<% this.title %>】</span>',
                    '</li>'
                ].join('');

                html += templateEngine(tpl, {
                    'userName': dataArr[i].user ? dataArr[i].user.name : '',
                    'createTime': dateFormat(dataArr[i].createTime),
                    'createTimeTitle': dateFormat(dataArr[i].createTime, true),
                    'createType': data.operationLogType[dataArr[i].operation],
                    'title': dataArr[i].content,
                    'className': dataArr[i].operation
                });
            }

            $('.am-log-list').html(html);
        }).fail(function () {
            console.log('操作日志列表请求失败')
        })
    }

    //异步取数据ajax封装
    function getTabListAjax(optData) {
        var url = optData.url;
        var param = optData.param;
        var deferred = $.Deferred();

        $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                contentType: 'application/json;charset=UTF-8',
                data: param
            })
            .done(function (resp) {
                if (resp.meta.success == true) {
                    deferred.resolve(resp);
                } else {
                    deferred.reject();
                }
            })
            .fail(function () {
                deferred.reject();
            });

        return deferred.promise();
    }

    function bindUI() {
        var $overlay = $('.am-overlay'),
            $popbox = $('.am-popbox'),
            $approvalNameBox = $('#approvalNameBox');

        $('body').on('click', '.am-create-approval', function () {
                renderComList(0, 8);
                $overlay.show();
                $popbox.show();
            })
            .on('click', '.am-popbox-close', function () {
                $overlay.hide();
                $popbox.hide();
            })
            .on('click', '.am-popbox-item', function () {
                var id = $(this).closest('li').attr('id') || '';
                var text = $(this).closest('li').find('p').text();
                text = text == '空白新模板' ? '' : text
                $approvalNameBox.find('input').val(text)
                //选择玩模版后需要输入审批名字，获取审批id 
                $popbox.hide();
                $('.q-hint-item').hide()
                $approvalNameBox.data('id', id)
                $approvalNameBox.show();

                // if (typeof id == "undefined") {
                //     window.location.href = data.createTplUrl;
                // } else {
                //     window.location.href = data.createTplUrl + '&formId=' + id + '&common=true';
                // }

            })
            // .on('click', '.creatNew', function() {
            //     var id = $(this).closest('li').attr('id');
            //     if (typeof id == "undefined") {
            //         window.location.href = data.createTplUrl;
            //     } else {
            //         window.location.href = data.createTplUrl + '?formId=' + id + '&common=true';
            //     }
            // })
            .on('click', '.am-nav button', function () {
                var index = $(this).index();
                $('.am-nav button').removeClass('am-curr');
                $(this).addClass('am-curr');
                if (index === 0) {
                    $('.am-export-content').hide();
                    $('.am-manage-content').show();
                    $('.approval-btn').hide();
                    $('.am-create-approval').show();
                } else {
                    $('.am-manage-content').hide();
                    $('.am-export-content').show();
                    $('.approval-btn').show();
                    $('.am-create-approval').hide();

                    //获取审批类型
                    if (data.queryApprovalType.first) {
                        queryApprovalType();
                    }

                    //加载第二个tab页
                    if (data.approvalDataExport.first) {
                        approvalDataExport(data.approvalDataExport.pageIndex, data.approvalDataExport.limit, getCheckData());
                    }

                }
            })
            //查看单据
            .on('click', '.view-details-btn', function () {
                var url = data.viewDetailUrl + $(this).data('id') + '?' + urlParam;
                $('.view-details-frame-content').attr('src', url);
                $overlay.show();
                $('.view-details-frame').show();
            })
            //打印
            .on('click', '.print-details-btn', function () {
                var thisDid = $(this).data('id');
                console.log('thisDid');
                showPrintPup(thisDid);
                $('.view-print-frame').show();
                $('.view-print-frame-mask').show();
            })
            .on('click', '#startPrint', function () {
                printHtml();
            })
            .on('click', '.view-print-cancel', function () {
                $('.view-print-frame').hide();
                $('.view-print-frame-mask').hide();
            })
            .on('click', '#closePrint', function () {
                $('.view-print-frame').hide();
                $('.view-print-frame-mask').hide();
            })
        //查看单据 打印 取消
        $('.view-details-cancel').on('click', function () {
            $overlay.hide();
            $('.view-details-frame').hide();
        })

        $('.am-manage-tbody').on('click', '.am-edit-btn', function (e) {
            var id = $(this).closest('tr').data('id');
            window.location.href = data.createTplUrl + '&formId=' + id + '&' + urlParam;
        });

        //启用
        $('.am-manage-tbody').on('click', '.am-start-btn', function () {
            if (!data.isStartData) {
                return false;
            }

            data.isStartData = false;

            var $me = $(this),
                $tr = $me.closest('tr'),
                id = $tr.data('id');
            //status = $tr.data('status');
            var paramStr = {
                //"approvalRule":{}
                "template": {
                    "id": id,
                    "status": data.status.alreadyEnabled
                }
            }
            var optData = {
                url: data.tabListRemoveUrl,
                param: JSON.stringify(paramStr)
            }

            getTabListAjax(optData).done(function () {
                    data.isStartData = true;
                    $tr.find('td').eq(1).addClass('am-skyblue').text('已启用');
                    var html = '<a class="am-stop-btn am-red" href="javascript:void(0)">停用</a>';
                    $tr.find('.am-start-btn').remove();
                    $tr.find('.am-del-btn').remove();
                    $tr.find('td:last').append(html);
                    notify(tipMsg.activeSuc);
                })
                .fail(function () {
                    notify(tipMsg.updateFail);
                    data.isStartData = true;
                });
        })

        //停用
        $('.am-manage-tbody').on('click', '.am-stop-btn', function (e) {
            if (!data.isStopData) {
                return false;
            }
            data.isStopData = false;
            var $me = $(this),
                $tr = $me.closest('tr'),
                id = $tr.data('id');

            var paramStr = {
                //"approvalRule":{}
                "template": {
                    "id": id,
                    "status": data.status.blockUp
                }
            }
            var optData = {
                url: data.tabListRemoveUrl,
                param: JSON.stringify(paramStr)
            }

            getTabListAjax(optData).done(function (resp) {
                    $tr.find('td').eq(1).removeClass('am-skyblue').addClass('am-red').text('已停用');
                    $tr.data('id', resp.data.template.id);
                    var isPublic = $tr.data('ispublic'),
                        html = '<a href="javascript:;" class="am-start-btn am-skyblue">启用</a><a class="am-del-btn am-red" href="javascript:void(0)">删除</a>',
                        htmlPublic = '<a href="javascript:;" class="am-start-btn am-skyblue">启用</a>';
                    $tr.find('.am-stop-btn').remove();
                    if (isPublic == true) {
                        $tr.find('td:last').append(htmlPublic);
                        $tr.data("id", resp.data.template.id);
                        $tr.find('td:nth-last-child(2)').html(resp.data.template.user.name);
                    } else {
                        $tr.find('td:last').append(html);
                    }

                    data.isStopData = true;
                    notify(tipMsg.stopSuc);
                })
                .fail(function () {
                    notify(tipMsg.updateFail);
                    data.isStopData = true;
                });
        });

        //删除
        $('.am-manage-tbody').on('click', '.am-del-btn', function (e) {
            var $me = $(this),
                $tr = $me.closest('tr'),
                id = $tr.data('id');
            //status = $tr.data('status');
            var paramStr = {
                //"approvalRule":{}
                "template": {
                    "id": id,
                    "isDelete": true
                }
            }
            var optData = {
                url: data.tabListRemoveUrl,
                param: JSON.stringify(paramStr)
            }
            var mes = confirm("删除后将无法使用，无法查看数据");
            if (mes == true) {
                getTabListAjax(optData).done(function () {
                        $tr.remove();
                        notify(tipMsg.delSuc);
                        if ($('.am-manage-tbody tr').length == 0) {
                            document.location.reload();
                        }
                    })
                    .fail(function () {
                        notify(tipMsg.updateFail);
                    });
            }
        });
        $('.q-form-input').on('input',function () {
            var $hint =$('.q-hint-item')
            if($(this).val().replace(/[^\x00-\xff]/g, '**').length>20){
                $hint.text('审批名称最多10个字').show()
            }else{
                $hint.hide()
            }
        })
        //点击审批名称弹框的关闭
        $('.q-dialog').on('click', '.q-dialog-closebtn', function () {
            $('#approvalNameBox').hide()
            $('.am-overlay').hide()
        });
        //点击审批名称弹框的下一步，将名字传到后端进行验证，获得审批id
        $('.q-dialog').on('click', '.q-btn', function () {
            var name = $('.q-form-input').val()
            var fromId = $('#approvalNameBox').data('id'), id
            if (!name) {
                $('.q-hint-item').text('审批名称不能为空').show()
                return
            }else if(name.replace(/[^\x00-\xff]/g, '**').length>20){
                $('.q-hint-item').text('审批名称最多10个字').show()
                return
            }
            var param = {
                template: {
                    appName: 'freeflow',//应用名称 必传
                    title: name,//标题
                    components: ''//组件内容
                },
                fromId: fromId
            }
            $.ajax({
                url: data.createApproval,
                type: 'POST',
                data: JSON.stringify(param),
                contentType: 'application/json'
            }).then(function (rsp) {
                // console.log(rsp)
                if (rsp.meta.success) {
                    id = rsp.data.template.id
                    window.location.href = data.createTplUrl + '&formId=' + id;
                } else {
                    $('.q-hint-item').text(rsp.meta.msg).show()
                    // notify(rsp.meta.msg);
                }

            })
        });


    }

    function initTpl() {
        /*var tpl = '',
         compiled = '';

         getTplData().done(function(resp) {
         for(var i = 0, len = resp.length; i < len; i++) {
         tpl = [
         '<tr data-status="<%= status %>" data-isPublic="<%= isPublic %>">',
         '<td><%= templateName %></td>',
         '<% if(status == 0) { %><td>已启用</td><% } else { %><td class="am-red">已停用</td><% } %>',
         '<td><%= updateName %></td>',
         '<td><%= createTime %></td>',
         '<td>',
         '<% if(isPublic) { %><a class="am-edit-btn" href="javascript:void(0)">编辑</a>',
         '<a class="am-stop-btn" href="javascript:void(0)"><% if(this.status == 0) { %>停用<% } else {%>启用<% } %></a>',
         '<a class="am-del-btn" href=""></a>',
         '<% } else { %><a class="am-edit-btn" href="javascript:void(0)">编辑</a>',
         '<a class="am-stop-btn" href="javascript:void(0)"><% if(this.status == 0) { %>停用<% } else {%>启用<% } %></a>',
         '<a class="am-del-btn" href="javascript:void(0)">删除</a><% } %>',
         '</td>',
         '</tr>'
         ].join(''); 
         resp[i] = $.extend(resp[i], {createTime: dateFormat(resp[i].datetime)});
         compiled += _.template(tpl)(resp[i]);
         }
         $('.am-manage-tbody').append(compiled);
         });*/


    }

    function getTplData() {
        var deferred = $.Deferred();

        $.ajax({
                url: data.tplListUrl,
                type: 'POST',
                dataType: 'JSON',
                // contentType: 'application/json;charset=UTF-8',
                data: {}
            })
            .done(function (resp) {
                if (resp.success) {
                    deferred.resolve(resp.data.details);
                } else {
                    deferred.reject();
                }
            })
            .fail(function (resp) {
                deferred.reject();
            });

        return deferred.promise();
    }

    function showOperateLog() {
        var tpl = '',
            compiled = '';

        getLogData().done(function (resp) {
            for (var i = 0, len = resp.length; i < len; i++) {
                tpl = [
                    '<li class="am-log-item" data-action="<%= remark %>"><span class="am-log-col"><%= updateName %></span>',
                    '于<%= updateDate %>',
                    '<% if(remark == 1) { %>',
                    '<span class="am-blue">创建了',
                    '<% } else if(remark == 2) { %>',
                    '<span class="am-yellow">修改了',
                    '<% } else { %>',
                    '<span class="am-red">删除了<% } %>',
                    '</span>审批模板<span class="am-log-col">【<%= templateName %>】</span></li>'
                ].join('');
                resp[i] = $.extend(resp[i], {updateDate: dateFormat(resp[i].updateDate)});
                compiled += _.template(tpl)(resp[i]);
            }
            $('.am-log-list').append(compiled);
        });
    }

    function getLogData() {
        var deferred = $.Deferred();

        $.ajax({
                url: data.operationLogUrl,
                type: 'POST',
                dataType: 'JSON',
                // contentType: 'application/json;charset=UTF-8',
                data: {}
            })
            .done(function (resp) {
                if (resp.success) {
                    deferred.resolve(resp.data.details);
                } else {
                    deferred.reject();
                }
            })
            .fail(function (resp) {
                deferred.reject();
            });

        return deferred.promise();
    }

    function apdateAjax(status) {
        var deferred = $.Deferred();

        $.ajax({
                url: data.tplListUrl,
                type: 'POST',
                dataType: 'JSON',
                // contentType: 'application/json;charset=UTF-8',
                data: {}
            })
            .done(function (resp) {
                if (resp.success) {
                    deferred.resolve();
                } else {
                    deferred.reject();
                }
            })
            .fail(function () {
                deferred.reject();
            });

        return deferred.promise();
    }

    function notify(msg) {
        var tpl = '<div class="notify">' + msg + '</div>';
        $('body').prepend(tpl);
        var $notify = $('.notify');
        $notify.fadeIn(500, function () {
            _.delay(function () {
                $notify.fadeOut(500, function () {
                    $notify.remove();
                });
            }, 1000)
        })
    }

    function dateFormat(str, needYear) {
        var d = new Date(parseInt(str)),
            year = d.getFullYear(),
            month = d.getMonth() < 9 ? '0' + parseInt(d.getMonth() + 1) : d.getMonth() + 1,
            day = d.getDate() < 10 ? '0' + d.getDate() : d.getDate(),
            hour = d.getHours() < 10 ? '0' + d.getHours() : d.getHours(),
            minute = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes(),
            second = d.getSeconds() < 10 ? '0' + d.getSeconds() : d.getSeconds();
        if (needYear) {
            return year + '年' + month + '月' + day + '日 ' + hour + ':' + minute;
        } else {
            return month + '月' + day + '日 ' + hour + ':' + minute;
        }

    }

    //选择部门
    function selectorDepartment() {
        var eid = __global.eid;
        var orgId; //__global.orgId; 


        orgId = '';


        $.extend(OrgTree.prototype, {
            _postData: function (orgId, isRoot) {
                var data = {
                    deptId: orgId
                };

                if (isRoot) {
                    data.isFirst = true
                }

                return data;
            },

            _tranformData: function (resp) {
                var data = resp.openDeptDetail;
                var newData = {
                    id: data ? data.id : '',
                    name: data ? data.name : '',
                    person: [],
                    children: data ? data.subDept : []
                };

                return newData;
            },
        });

        var orgTree = new OrgTree({
            'url': '/attendance/rest/querydept',
            'target': '.fd-selectappr-org',
            'orgId': orgId,
            'needSelectDept': true,
            'needPerson': false,
            'multiSelect': false
        });

        //查询部门
        $('.am-department').on('click', function () {
            $('.am-overlay').fadeIn();
            $('.selector-department').fadeIn();
        })

        //关闭 取消按钮
        $('.selector-department-guanbi').add('.selector-department-quxiao').on('click', function () {
            $('.am-overlay').hide();
            $('.selector-department').hide();
        });

        //点击确定选择部门信息
        $('.selector-department-true').on('click', function () {
            var depts = orgTree.getSelectedDepts();
            var dept = depts[0];
            if (dept) {
                $('.am-department input').val(dept.name);
                data.checkData.deptId = dept.id;
                /*me.data.isInclude = $('.selector-department-children input[type=checkbox]').prop('checked');*/
                $('.am-department b').show();
            } else {
                $('.selector-department-tips').show();
                setTimeout(function () {
                    $('.selector-department-tips').hide();
                }, 1500)
                return false;
            }
            $('.selector-department').hide();
            $('.am-overlay').hide();
        });


        //选择部门清空按钮
        $('.am-department b').on('click', function (e) {
            e.stopPropagation();
            $('.am-department input').val('');
            $(this).hide();
            data.checkData.deptId = '';
        })
    }

    //查询审批类型
    function queryApprovalType() {
        var paramStr = {
            "order": "isPublic,-createTime",
            "start": data.queryApprovalType.pageIndex,
            // "limit": data.queryApprovalType.limit,
            "limit": -1
        };
        var optData = {
            url: data.queryApprovalType.url,
            param: JSON.stringify(paramStr)
        }

        getTabListAjax(optData).done(function (resp) {
            data.queryApprovalType.first = false;
            var dataArr = resp.data.approvalTypes;
            var html = '<option value="">全部</option>';
            if (!dataArr.length) {
                data.checkData.approvalType = '';
                html += '<option value="">暂无类型</option>';
            }
            else{
                for (var i = 0; i < dataArr.length; i++) {
                    html += '<option value="' + dataArr[i].id + '">' + dataArr[i].title + '</option>';
                }
            }
            html += '<option value="third-party">第三方</option>';
            $('.am-approve-type select').html(html);
        }).fail(function () {
            notify(tipMsg.approvalType);
        })
    }


    //搜索框
    function userNameSerch() {
        $('.am-promoter input').autoComplete({
            searchUrl: '/im/web/searchUser.do',
            data: {
                count: 10
            },

            onBefore: function () {
                $('.job-log-report-serchk').show();
                $('.job-log-report-serchk div').show();
            },
            onNoSearchVal: function () {
                $('.job-log-report-serchk ul').html('');
                $('.job-log-report-serchk').hide();
                data.checkData.userName = '';
                data.checkData.userId = '';
            },
            onSuccess: function (respObj) {
                var dataArr = respObj.data.data.list;
                var html = '';
                $.each(dataArr, function (key, val) {
                    if (val.phones == '') {
                        val.phones = val.emails;
                    }
                    html += '<li data-userid="' + val.wbUserId + '" data-username="' + val.name + '"><i style="padding-right:5px;">' + val.name + '</i><i>' + val.phones + '</i></li>';
                });

                $('.job-log-report-serchk div').hide();
                $('.job-log-report-serchk ul').html(html);
                if (!dataArr || !dataArr.length) {
                    $('.job-log-report-serchk ul').html('搜索无结果');
                    data.checkData.userName = '';
                    data.checkData.userId = '';
                }
            },
            onError: function () {
                $('.job-log-report-serchk ul').html('搜索无结果');
            },
            onAlways: function () {

            }
        })
    }

    //搜索框鼠标划过
    function userNameSerchHover() {
        var me = this;
        //鼠标划过
        $('.job-log-report-serchk').on('mouseover', 'li', function () {
            $('.job-log-report-serchk .hover').removeClass('hover');
            $(this).addClass('hover');
        })

        //点击选人
        $('.job-log-report-serchk').on('click', 'li', function () {
            data.checkData.userId = $(this).data('userid');
            data.checkData.userName = $(this).data('username');
            $('.job-log-report-serchk').hide();
            $('.am-promoter input').val(data.checkData.userName);
        })

        //失去焦点
        $('.am-promoter input').on('click', function (e) {
            e.stopPropagation();
        }).on('focus', function () {
            if (this.value) {
                $('.job-log-report-serchk').show();
            }
        });

        $(document).click(function () {
            $('.job-log-report-serchk').hide();
        });
    }

    //生成打印页面
    function printHtml() {
        var html = '<div class="pdf-box">' + $('.view-print-content').html() + '</div>';
        var contentWindow = document.getElementById('print-frame').contentWindow;
        $(contentWindow.document).find('.pdf-box').html(html);
        contentWindow.print();
    }

    //预览打印页面
    function showPrintPup(dID) {
        $.ajax({
                url: data.approvalPrint + dID,
                type: 'GET',
                dataType: 'json',
            })
            .done(function (resp) {
                var dataArr = resp.data;
                var html = '';
                //表格数据
                console.log("预览打印页面数据", dataArr.formData);
                var tpl = [
                    '<div class="pdf-con">',
                    '<div class="pdf">',
                    '<div class="header">',
                    '<h2><% this.title %></h2>',
                    '</div>',
                    '<div class="con-body">',
                    '<p class="comp-a-time">',
                    '<span class="comp">公司：<% this.netWorkName %></span>',
                    '<span class="atime">申请日期：<% this.createTime %></span>',
                    '</p>',
                    '<table class="con-table">',
                    '<tbody>',
                    '<% for(var key in this.formData){ %>',
                    '<% if(key != "审批记录"){ %>',
                    '<% if(key == "context") { %>', //是控件类型
                    '<% for(var i = 0; i < this.formData[key].length; i++) { %>',
                    '<tr>',
                    '<td><span class="t-title"><% this.formData[key][i][0] %></span></td>',
                    '<td><span class="t-con"><% this.formData[key][i][1] %></td>',
                    '</tr>',
                    '<% } %>',
                    '<% }else { %>',
                    '<tr>',
                    '<td><span class="t-title"><% key %></span></td>',
                    '<% if(this.formData[key] instanceof Array){ %>',
                    '<td>',
                    '<ul class="t-ul">',
                    '<% for(var i = 0; i < this.formData[key].length; i++) { %>',
                    '<li class="t-li"><span><% this.formData[key][i] %></span></li>',
                    '<% } %>',
                    '</ul>',
                    '</td>',
                    '<% }else{ %>',
                    '<td><span class="t-con"><% this.formData[key] %></td>',
                    '<% } %>',
                    '</tr>',
                    '<% } %>',
                    '<% }else{ %>',
                    '<tr>',
                    '<td><span class="t-title"><% key %></span></td>',
                    '<td>',
                    '<ul class="t-ul">',
                    '<% for(var i = 0; i < this.formData[key].length; i++){ %>',
                    '<li class="t-li">',
                    '<span class="s-name"><% this.formData[key][i][0] %></span>',
                    //'<span class="suggest">同意审批</span>',
                    '<span class="s-time"><% this.formData[key][i][1] %></span>',
                    '</li>',
                    '<% } %>',
                    '</ul>',
                    '</td>',
                    '</tr>',
                    '<% } %>',
                    '<% } %>',
                    '</tbody>',
                    '</table>',
                    '<p class="ptime-ppeop">',
                    '<span class="p-time">打印时间：<% this.printTime %></span>',
                    '<span class="p-peop">打印人：<% this.printer %></span>',
                    '</p>',
                    '</div>',
                    '</div>',
                    '</div>'
                ].join('');

                //resp[i] = $.extend(dataArr[i], {createTime: dateFormat(dataArr[i].datetime)});
                html += templateEngine(tpl, {
                    'title': dataArr.title, //审批名称
                    'createTime': dataArr.createTime, //申请时间
                    'printer': dataArr.printer, //打印者
                    'netWorkName': dataArr.netWorkName, //部门名
                    'printTime': dataArr.printTime, //打印时间
                    'formData': dataArr.formData //信息数组
                });
                $('.view-print-content').html('');
                $('.view-print-content').html(html);
                console.log(html);
            })
            .fail(function () {
                console.log('审批数据导出--表格失败！');

                notify(tipMsg.checkDateFail);
            })
    }

    //审批数据导出--表格
    function approvalDataExport(curPage, limit, checkDataParam) {

        var paramStr = {
            "paramsObject": checkDataParam,
            "order": "updateTime",
            "start": curPage,
            "limit": limit
        };

        var optData = {
            url: data.approvalDataExport.url,
            param: JSON.stringify(paramStr)
        }

        getTabListAjax(optData).done(function (resp) {
            //console.log(resp);
            data.approvalDataExport.first = false;
            data.approvalDataExport.totalSize = resp.data.total;
            var dataArr = resp.data.approvalApplys;
            var html = '';

            //分页
            if (!data.isSelectPageClick) {
                var opt = {
                    'target': '.approval-data-export-pagiantion',
                    'pageSize': data.approvalDataExport.limit,
                    'currentPage': data.approvalDataExport.pageIndex,
                    'itemsTotal': data.approvalDataExport.totalSize
                };
                initPaginatorTabS(opt);
            }
            //无数据
            if (!dataArr.length) {
                noData('.am-export-tbody', '<tr><td class="am-manage-no-data" colspan="8">无数据</td></tr>');
                return;
            }

            for (var i = 0; i < dataArr.length; i++) {
                //表格数据
                // console.log(dataArr[i].approvalUser);
                var tpl = [
                    '<tr>',
                    '<td><% this.receiptId %></td>',
                    '<td class="am-skyblue"><% this.title %></td>',
                    '<td><% this.deptName %></td>',
                    '<td><% this.userName %></td>',
                    '<td title="<% this.createTimeTitle %>"><% this.createTime %></td>',
                    '<td>',
                    '<% for(var i = 0;i < this.approvalUser.length;i ++){ %>',
                    '<% if(this.approvalUser[i].name == undefined){continue;} %>',
                    '<% if(i == this.approvalUser.length - 1){ %>',
                    '<% this.approvalUser[i].name %>',
                    '<% }else{ %>',
                    '<% this.approvalUser[i].name %>' + '>',
                    '<% } %>',
                    '<% } %>',
                    '</td>',
                    '<% if(this.status == "2" || this.status == "3"){ %> ',
                    '<td title="<% this.lastUpdateTimeTitle %>"><% this.lastUpdateTime %></td>',
                    '<% }else{ %>',
                    '<td>未完成</td>',
                    '<% } %>',
                    '<td>',
                    '<a class="am-blue view-details-btn" href="javascript:;" data-typeid="<% this.formTemplateId %>" data-id="<% this.id %>">查看单据</a>',
                    '<a class="am-blue print-details-btn" href="javascript:;" data-id="<% this.id %>">打印</a>',
                    '</td>',
                    '</tr>'
                ].join('');

                //resp[i] = $.extend(dataArr[i], {createTime: dateFormat(dataArr[i].datetime)});
                html += templateEngine(tpl, {
                    'receiptId': dataArr[i].receiptId, //单号
                    'formTemplateId': dataArr[i].formTemplateId, //类型id
                    'title': dataArr[i].title, //类型名
                    'deptName': dataArr[i].createUser ? dataArr[i].createUser.deptName : '无部门', //部门名
                    'userName': dataArr[i].createUser ? dataArr[i].createUser.name : '', //发起人
                    'createTime': dateFormat(dataArr[i].createTime), //发起时间
                    'createTimeTitle': dateFormat(dataArr[i].createTime, true),
                    'approvalUser': dataArr[i].approvalUser, //审批人
                    'status': dataArr[i].status, //审批状态
                    'lastUpdateTime': dataArr[i].lastUpdateTime ? dateFormat(dataArr[i].lastUpdateTime) : '', //审批完成时间
                    'lastUpdateTimeTitle': dataArr[i].lastUpdateTime ? dateFormat(dataArr[i].lastUpdateTime, true) : '',
                    'id': dataArr[i].id //当前审批id
                });
            }
            $('.am-export-tbody').html('');
            $('.am-export-tbody').html(html);

        }).fail(function () {
            console.log('审批数据导出--表格失败！');

            notify(tipMsg.checkDateFail);

        })
    }

    //无数据
    function noData(ele, noDataText) {
        $(ele).html('');
        $(ele).html(noDataText);
    }

    //将时间转换为毫秒数
    function getTime(timeStr) {
        if (timeStr) {
            var time;
            var y = timeStr.substr(0, 4);
            var m = timeStr.substr(timeStr.indexOf('-') + 1, 2) - 1;
            var d = timeStr.substr(timeStr.lastIndexOf('-') + 1);
            time = +new Date(y, m, d);
            //console.log(y + ',' + m + ',' + d);
            return time;
        }
    }

    //获取查询参数
    function getCheckData() {
        var formTemplateId = $('.am-approve-type select').val(), //类型id
            receiptId = $('.am-approve-number input').val(), //单号
            startTime = getTime($('#d12').val()) || null,
            endTime = getTime($('#d13').val()) || null;

        var checkDataParam = {
            formTemplateId: formTemplateId,
            user: {
                deptId: data.checkData.deptId,
                userId: data.checkData.userId
            },
            startTime: startTime,
            endTime: endTime,
            receiptId: receiptId
        };

        return checkDataParam;

    }

    //查询按钮
    function checkMessage() {

        $('.approval-data-chaxun').on('click', function () {
            data.isSelectPageClick = false;
            //console.log(getCheckData());
            //查询刷新页面
            approvalDataExport(data.checkData.pageIndex, data.checkData.limit, getCheckData());

        })
        //导出按钮
        $('.am-export-btn').on('click', function () {
            //				data.isSelectPageClick = false;
            //				$('#params').val(JSON.stringify(getCheckData()).replaceAll("\"","\'"));
            var formTemplateId = $('.am-approve-type select').val(), //类型id
                receiptId = $('.am-approve-number input').val(), //单号
                deptId = data.checkData.deptId,
                userId = data.checkData.userId,
                startTime = getTime($('.am-approve-date #d12').val()) || null,
                endTime = getTime($('.am-approve-date #d13').val()) || null;
            // console.log("开始时间", startTime);
            $('#formTemplateId').val(formTemplateId);
            $('#receiptId').val(receiptId);
            $('#deptId').val(deptId);
            $('#userId').val(userId);
            $('#startTime').val(startTime);
            $('#endTime').val(endTime);
            $('#exportForm').attr('action', '/freeflow/v1/rest/approval/export');
            $('#exportForm').submit();
            //查询刷新页面
            //				approvalDataExport(data.checkData.pageIndex, data.checkData.limit, getCheckData());
        })
    }

    return {
        init: function () {
            //设置云之家头部
            /* if(webUtil.getRequestKey()['dept']){
             document.querySelector('.job-log-report-name').innerHTML = webUtil.getRequestKey()['dept'] + '<i>—</i>审批';
             }*/
            bindUI();
            showOperateLog();
            initTpl();

            //生成表格列表
            renderUITabList(data.approvalTemplateConfiguration.pageIndex, data.approvalTemplateConfiguration.limit)

            //操作日志列表
            operationLog();

            //发起人搜索框
            userNameSerch();

            //搜索框鼠标划过
            userNameSerchHover();

            //选择部门
            selectorDepartment();

            //查询按钮
            checkMessage();

        }
    };
});
