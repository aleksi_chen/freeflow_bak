/*
 * 选择审批人
 * @author muqin_deng
 * @time 2015/10/13
 */
;(function (factory) {
	factory = factory(jQuery, F.d.Widget, webUtil, _, F.d.OrgTree);

	webUtil.namespace('F.d.SelectApprover', factory);

})(function ($, Widget, Util, _, OrgTree) {
	//主函数
	function SelectApprover(cfg) {
		Widget.call(this);
		return this.render(cfg);
	}

	SelectApprover.prototype = $.extend({}, Widget.prototype, {
		constructor: SelectApprover,
		
		initCfg: function (cfg) {
			this.cfg = $.extend({
				'switchSearch': false,
				'multiSelect': false
			}, cfg);
		},

		renderUI: function () {
			var tpl = new Util.Template();

			tpl._('<div class="fd-dialog fd-select-approver zoomin">')
			       ._('<div class="fd-dialog-hd">')
			           ._('<span class="fd-selectappr-title">设置关键审批人</span>')
			           ._('<span class="fd-dialog-closebtn"></span>')
			       ._('</div>')
			       ._('<div class="fd-dialog-bd">')
			       	   ._('<div class="fd-selectappr-check">')
				           ._('<label class="fd-checkbox-label">')
				               ._('<input type="radio" class="fd-checkbox" name="need-search" value="0"/>')
				               ._('<span class="fd-checkbox-face"></span>')
				               ._('<span class="fd-label-text">需申请人的直接上司</span>')
				           ._('</label>')
				           ._('<label class="fd-checkbox-label">')
				               ._('<input type="radio" class="fd-checkbox" name="need-search" value="1" <%= switchSearch ? "checked" : "" %> />')
				               ._('<span class="fd-checkbox-face"></span>')
				               ._('<span class="fd-label-text">精确查找审批人</span>')
				           ._('</label>')
				       ._('</div>')
				       ._('<div class="fd-selectappr-search">')
				           ._('<div class="fd-selectappr-searchgroup">')
				                ._('<span class="fd-selectappr-icon fd-search-icon"></span>')
				                ._('<input type="text" class="fd-selectappr-searchinput" placeholder="输入员工姓名/手机号"/>')
				                ._('<span class="fd-selectappr-icon fd-search-closebtn"></span>')
				           ._('</div>')
				           ._('<div class="fd-selectappr-org" data-switchpanel="0">')
				               ._('<p class="fd-selectappr-orgtitle">组织架构</p>')
				           ._('</div>')
				           ._('<div class="fd-selectappr-searchresult none" data-switchpanel="1">')
				               ._('<p class="fd-selectappr-searchloading">')
				                   ._('<img src="/freeflow/web/images/loading.gif">')
				               ._('</p>')
				               ._('<p class="fd-selectappr-searchmsg none"></p>')
				               ._('<div class="fd-selectappr-searchlist"></div>')
				           ._('</div>')
				       ._('</div>')
			       ._('</div>')
			       ._('<div class="fd-dialog-ft">')
			           ._('<div class="fd-dialog-btngroup">')
			               ._('<button class="fd-btn fd-btn-primary fd-dialog-confirmbtn">确定</button>')
			               ._('<button class="fd-btn fd-btn-default fd-dialog-cancelbtn">取消</button>')
			           ._('</div>')
			       ._('</div>')
			   ._('</div>');

			tpl = _.template(tpl.toString())(this.cfg);
			
			this._$elem = $(tpl).before();
		},

		bindUI: function () {
			var me = this;

			this._$elem.on('click', '.fd-dialog-closebtn, .fd-dialog-cancelbtn', function () {
				me.destroy();
				me.fire('cancel');
			})
			.on('click', '.fd-dialog-confirmbtn', function () {
				me._onConfirm();
			})
			// .on('change', 'input[name="need-search"]', function () {
			// 	me.showSearchPanel($(this).val() == 1);
			// })
			// 关闭搜索结果
//			.on('click', '.fd-search-closebtn', function (e) {
//				me._onCloseSearch(e);
//			})
			// 选中搜索结果项
			.on('change', '.fd-searchitem-checkbox', function (e) {
				me._onSearchItemChange(e);
			});

			this.eachBind({
				'confirm': 'onConfirm',
				'cancel': 'onCancel'
			});
		},

		ready: function () {
			var me = this;
			this._createOverlayer();
			this._initSearcher();
			this._initOrgTree();
			// this.showSearchPanel(this.get('switchSearch'), true);
		},

		_createOverlayer: function () {
			var $overlayer = $('<div class="fd-overlayer fd-selectappr-overlayer fadein">');
			this._$overlayer = $overlayer;
			this._$elem.before($overlayer);

			return $overlayer;
		},

		// 确定按钮
		_onConfirm: function () {
			var $checkbox = this.find('.fd-selectappr-check input[type="radio"]:checked');
			var returnData = {
				isSelected: false,
				needDeptHead: false,
				person: null
			};

			if ($checkbox.length) {
				returnData.isSelected = true;

				// 申请人直接上司
				if ($checkbox.val() == 0) {
					returnData.needDeptHead = true;
				// 精确查找
				} else if ($checkbox.val() == 1) {
					returnData.person = this.getSearchSelected();
				}
			}

			this.fire('confirm', returnData);
			this.destroy();
		},

		// 获取搜索或组织架构选中的人
		getSearchSelected: function () {
			var person = null;
			// 搜索面板
			if (this.find('.fd-selectappr-searchresult').is(':visible')) {
				var $checkbox = this.find('.fd-searchitem input[type="checkbox"]:checked');

				if ($checkbox.length) {
					var $searchitem = $checkbox.closest('.fd-searchitem');
					var userId = $checkbox.val();
					var name = $searchitem.find('.fd-searchitem-name').text();
					var photoUrl = $searchitem.find('img').attr('src');
					person = {
						userId: userId,
						name: name,
						photoUrl: photoUrl
					};
				}
			// 组织架构
			} else {
				var orgTree = this.get('orgTree');
				var persons = orgTree.getSlectedPersons();
				if (persons.length) {
					person = persons[0];
				}
			}

			return person;
		},

		// showSearchPanel: function (show, isInit) {
		// 	if (show === false) {
		// 		$('.fd-selectappr-search')[isInit ? 'hide' : 'slideUp']();
		// 	} else {
		// 		$('.fd-selectappr-search')[isInit ? 'show' : 'slideDown']();
		// 	}
		// },

		_initSearcher: function () {
			var me = this;
			var $searchInput = this.find('.fd-selectappr-searchinput');

			$searchInput.autoComplete({
				searchUrl: '/im/web/searchUser.do',
				throttle: 200,
				closeBtn: '.fd-search-closebtn',
				onBefore: function () {
					$searchInput.addClass('searching');
					me._switchPanel(1);
					me._searchLoading();
					me._showSearList(false);
					me._showSearchMsg(false);
				},
				onNoSearchVal: function () {
					me._onCloseSearch();
				},
				onSuccess: function (respObj) {
					me._onSearchSuccess(respObj);
				},
				onError: function () {
					me._showSearchMsg('搜索出错了 >_<');
				},
				onAlways: function () {
					me._searchLoading(false);
				}
			});

			this.set('$searchInput', $searchInput);
		},

		_onSearchSuccess: function (respObj) {
			var resp = respObj.data;
			if (resp.success === true && resp.data.count > 0) {
				var listTpl = this._tplSearchList(resp.data.list);
				this.find('.fd-selectappr-searchlist').html(listTpl);
				this._showSearList();
				// 保存人员信息对象
				this.set('searchDataCache', resp.data.list);
			} else {
				this._showSearchMsg('没有找到结果 >_<');
				this._showSearList(false);
			}
		},

		_tplSearchList: function (list) {
			var t = new Util.Template();

			t._('<% _.each(list, function (item) { %>')
				._('<label class="fd-checkbox-label fd-searchitem">')
					._('<div class="fd-searchitem-cbgroup">')
						._('<input type="checkbox" class="fd-checkbox fd-searchitem-checkbox" value="<%= item.wbUserId %>"/>')
				    	._('<span class="fd-checkbox-face"></span>')
				    ._('</div>')
				    ._('<div class="fd-searchitem-profile">')
				    	._('<img src="<%= item.photoUrl || "/space/c/photo/load?id=" %>" />')
				    	._('<div class="fd-searchitem-info ellipsis">')
				    		._('<p>')
				    			._('<span class="fd-searchitem-name"><%= item.name %></span>')
				    			._('<span class="fd-searchitem-jobtitle"><%= item.jobTitle%></span>')
				    		._('</p>')
				    		._('<span class="fd-searchitem-phone"><%= item.phones || item.emails %></span>')
				    	._('</div>')
				    ._('</div>')
				._('</label>')
			 ._('<% }) %>');

			return _.template(t.toString())({
				list: list
			});
		},

		_onSearchItemChange: function (e) {
			var multiSelect = this.get('multiSelect');
			var $target = $(e.target);

			if (!multiSelect && $target.prop('checked')) {
				this.find('.fd-searchitem-checkbox:checked')
				    .not(e.target)
				    .prop('checked', false);
			}
		},

		// 结束搜索
		_onCloseSearch: function () {
//			this.get('$searchInput').removeClass('searching').val('');
			this.find('.fd-selectappr-searchlist').html('');
			this._switchPanel(0);
		},

		_searchLoading: function (show) {
			if (show === false) {
				this.find('.fd-selectappr-searchloading').hide();
			} else {
				this.find('.fd-selectappr-searchloading').show();
			}
		},

		_showSearList: function (show) {
			if (show === false) {
				this.find('.fd-selectappr-searchlist').hide();
			} else {
				this.find('.fd-selectappr-searchlist').show();
			}
		},

		_showSearchMsg: function (msg) {
			if (msg === false) {
				$('.fd-selectappr-searchmsg').html('').hide();
			} else {
				$('.fd-selectappr-searchmsg').html(msg).show();
			}
		},

		// 初始化组织架构
		_initOrgTree: function () {
			var orgTree = new OrgTree({
				'target': '.fd-selectappr-org',
				'eid': '441170',
				'multiSelect': false
			});

			this.set('orgTree', orgTree);
		},

		// 切换搜索结果跟组织架构面板
		_switchPanel: function (index) {
			this.find('[data-switchpanel]').hide();
			this.find('[data-switchpanel=' + index + ']').show();
		},

		destroy: function () {
			this._$elem.off().remove();

			var $overlayer = this._$overlayer;
			$overlayer.remove();
		}

	});

	return SelectApprover;
	
});
