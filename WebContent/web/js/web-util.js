;(function (window, doc, factory) {
	factory = factory(window, doc);

	window.webUtil = window.webUtil || factory;

})(window, document, function (window, doc) {
	// 工具集
	var webUtil = {
		// 根据命名空间执行init方法
		run: function (namespaceStr) {
			if (!namespaceStr) {return ;}

			var namespaceArr = namespaceStr.split('.'),
				obj = window;

			for (var i = 0, len = namespaceArr.length; i < len; i++) {
				if (!obj[namespaceArr[i]]) {
					return ;
				}

				obj = obj[namespaceArr[i]];
			}

			typeof obj.init === 'function' && obj.init();
		},

		// 创建命名空间
		namespace: function (namespaceStr, newObj) {
			if (!namespaceStr) {return ;}

			var namespaceArr = namespaceStr.split('.'),
				obj = window;

			for (var i = 0, len = namespaceArr.length; i < len; i++) {
				if (!obj[namespaceArr[i]]) {
					if (newObj !== undefined && i == len - 1) {
						obj = obj[namespaceArr[i]] = newObj;
						break;
					}

					obj[namespaceArr[i]] = {};
				}

				obj = obj[namespaceArr[i]];
			}

			return obj;
		},

		// 获取url键值对
        getUrlKeyValObj: function () {
        	var url = window.location.search,
				arr, i, len,
				paramsObj = {};	
        	
        	if (this.getUrlKeyValObj.urlKeyValObj) {
        		return this.getUrlKeyValObj.urlKeyValObj;
        	}
	
			arr = url.substring(1).split('&');

			if (!arr.length) {
				return paramsObj;
			}
			
			for (i = 0, len = arr.length; i < len; i++) {
				var reg = /(.*)\=(.*)/g,
					match = reg.exec(arr[i]);
	
				if (match && match[1]) {
					paramsObj[decodeURIComponent(match[1])] = decodeURIComponent(match[2]);
				}
			}
			
			this.getUrlKeyValObj.urlKeyValObj = paramsObj;
			
			return paramsObj;
        },
        getRequestKey: function () {
               var url = window.location.search;
               var theRequest = new Object();
               var strs;
               if (url.indexOf("?") != -1) {
                   var str = url.substr(1);
                   strs = str.split("&");
                   for (var i = 0; i < strs.length; i++) {
                       theRequest[strs[i].split("=")[0]] = decodeURIComponent(strs[i].split("=")[1]);
                   }
               }
               return theRequest;
           },
        
        // 获取url的对应参数的值
		getUrlValue: function (param) {
			if (!param) {
				return '';
			}
			
			var paramsObj = this.getUrlKeyValObj();

			if (paramsObj.hasOwnProperty(param)) {
				return paramsObj[param];
			} else {
				return '';
			}
		},

		/*
		 * 函数节流
		 * @param {function} method	要进行节流的函数
		 * @param {number} delay 延时时间(ms)
		 * @param {number} duration 经过duration时间(ms)必须执行函数method
		 */
		throttle: function (method, delay, duration) {
			var timer = null,
				begin = null;
            return function () {
                var context = this,
                	args = arguments,
                	current = new Date();
                if (!begin) {
                	begin = current;
                }
                if (timer) {
                	window.clearTimeout(timer);
                }
                if (duration && current - begin >= duration) {
                     method.apply(context,args);
                     begin = null;
                }else {
                    timer = window.setTimeout(function () {
                        method.apply(context, args);
                        begin = null;
                    }, delay);
               }
            };
		},

		uuid: function (len, radix) {
		    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
		    var uuid = [], i;
		    radix = radix || chars.length;
		 
		    if (len) {
		      // Compact form
		      for (i = 0; i < len; i++) {
		      	uuid[i] = chars[0 | Math.random() * radix];
		      }
		    } else {
		      var r;
		 
		      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
		      uuid[14] = '4';

		      for (i = 0; i < 36; i++) {
		        if (!uuid[i]) {
		          r = 0 | Math.random() * 16;
		          uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
		        }
		      }
		    }
		 
		    return uuid.join('');
		},

		// 获取字节长度
		getByteLength: function(str){
            return str.replace(/[^\x00-\xff]/g, '**').length;
        },

        /**
		 * 根据字节长度截取字符串
		 * @param {string} str 目标字符串
		 * @param {number} num 需要截取的长度
		 * @return {string} 返回截取后的字符串
		 */
		getByteVal: function(str, num){
			var len = 0,
				returnValue = '';
			for (var i = 0, l = str.length; i < l; i++) {
				if (str[i].match(/[^\x00-\xff]/ig) != null) { //全角
		            len += 2;
		        } else {
		            len += 1;
				}
				if (len > num) {
					break;
				}
				returnValue += str[i];
			}
			return returnValue;
		},

		// 转义特殊字符<,>,",&
		escape: function (str) {
			if (!str) {
				return '';
			}

			var escape = {
				'<': '&lt;',
				'>': '&gt;',
				'\"': '&quot;',
				'&': '&amp;'
			};

			return str.replace(/[&<>"]/g, function (match) {
				return escape[match] || match;
			});
		},

		tab: function (panelWrap, activeClass) {
			var $wrap = $(panelWrap);

			$wrap.on('click', '[data-panel]', function () {
				activeClass && $wrap.find('[data-panel]').removeClass(activeClass);
				$wrap.find('[data-pindex]').removeClass(activeClass).hide();

				var index = $(this).addClass(activeClass).data('panel');
				$wrap.find('[data-pindex=' + index + ']').addClass(activeClass).show();
			});
		},

		tips: function (msg) {
			$('.ui-tips').remove();

			var $tips = $('<div class="ui-tips floatin"></div>');
			$tips.text(msg).appendTo('body');
		},

		animationEnd: function (el, cb, remove) {
			var eventType = whichAnimationEvent(el);

			el.addEventListener(eventType, handler, false);

			function handler() {
		    	if (remove !== false) {
		    		el.removeEventListener(eventType, handler, false);
		    	}

		    	var args = [].slice.call(arguments);
		    	cb.apply(null, args);
		    }

			function whichAnimationEvent(el) {
				var animations = {
					'animation':'animationend',
					'OAnimation':'oAnimationEnd',
					'MozAnimation':'animationend',
					'WebkitAnimation':'webkitAnimationEnd'
				}

				for(var a in animations){
				   if( el.style[a] !== undefined ){
				       return animations[a];
				   }
				}
		    }
		},

		delay: function (time) {
			var deferred = $.Deferred();

			setTimeout(function () {
				deferred.resolve();
			}, time || 0);

			return deferred.promise();
		},

		// 简单模板拼装
		Template: function () {
			this.arr = [];
			this._pushAll(arguments);
		},
        //截取URL键值对
        getRequest : function(){ 
            var url = window.location.search;
            var theRequest = new Object();
            if (url.indexOf("?") != -1) {
                var str = url.substr(1);
                strs = str.split("&");
                for (var i = 0; i < strs.length; i++) {
                    theRequest[strs[i].split("=")[0]] = decodeURIComponent(strs[i].split("=")[1]);
                }
            }
            return theRequest;
        }
	};
    

	webUtil.Template.prototype = {
		constructor: webUtil.Template,

		_: function () {
			this._pushAll(arguments);
			return this;
		},

		toString: function () {
			return this.arr.join('');
		},

		_pushAll: function (arguments) {
			var args = [].slice.call(arguments);
			this.arr = this.arr.concat(args);
		},

		clean: function () {
			this.arr = [];
			return this;
		}
	};

	return webUtil;
});