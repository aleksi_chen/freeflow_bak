/**
 * 搜索框组件
 * 依赖jQuery
 * @author muqin_deng@kingdee.com
 * @time 2015/10/12
 */

 ;(function (factory) {
	factory = factory(jQuery, webUtil);

	webUtil.namespace('F.d.autocomplete', factory);

})(function ($, Util) {

	var defaultSettings = {
		searchUrl: '',	//搜索路径

		key: 'word', //搜索内容对应的键值

		data: {},	//搜索请求中携带的额外数据

		cache: true,	//是否换成搜索记录

		throttle: 300,

		closeBtn: '',	// 关闭按钮如果有

		onBefore: function () {}, //发送搜索请求前的回调

		onNoSearchVal: function () {}, //搜索框值为空的回调

		onSuccess: function (resp) {},	//搜索成功回调

		onError: function (resp) {},	//搜索失败回调

		onAlways: function (resp) {}	//搜索或失败都会调用

	};

	$.fn.autoComplete = function (settings) {
		var cfg = $.extend({}, defaultSettings, settings);

		return this.each(function () {
			var ac = new AutoComplete(cfg);

			ac.init(this);
		});
	};

	function AutoComplete (cfg) {
		this.cfg = cfg;
		this._db = {};
	}

	AutoComplete.prototype = {
		constructor: AutoComplete,

		init: function (elem) {
			this._$elem = $(elem);
			this.bindUI();
		},

		//绑定事件
		bindUI: function () {
			var me = this;

			this._$elem.on('keyup', Util.throttle(function (e) {	//搜索输入框事件
					me._search(e);
				}, this.cfg.throttle)
			);
			
			if (this.cfg.closeBtn) {
				$(this.cfg.closeBtn).on('click', function () {
					me._$elem.val('');
					me._empty();
				});
			}
		},

		//搜索操作
		_search: function (e) {
			var $tar = this._$elem,
				searchCount = $tar.data('search-count') || 0,
				oldVal = $tar.data('old-val'),
				newVal = Util.escape($tar.val().trim()),
				reg = /\S+/g;	//非空格

			//按下退格键重新设置old-val为新的值
			if (e.keyCode == 8) {
				$tar.data('old-val', newVal);
			}

			//如果搜索框为空
			if (!reg.test(newVal)) {
				this._empty();
				return ;
			}
			//屏蔽ctrl/shift/enter/capsLock等非输入按键，减少发送请求
			if (oldVal == newVal) {
				return ;
			}

			//更新搜索次数
			$tar.data('search-count', searchCount += 1);

			this.cfg.onBefore.call(this);

			//发送查询请求
			this._searchGet(newVal, this._searchSucc(newVal, searchCount));
		},

		//搜索人员成功
		_searchSucc: function (newVal, searchCount) {
			var me = this,
				$tar = this._$elem;

			/* 使用闭包保留请求的顺序
			 * 保证是最后一次请求的回调才进行html拼装渲染（慢网速）
			 */
			return function (resp) {
				var reg = /\S+/g;	//非空格

				if (searchCount != $tar.data('search-count')) {
					return ;
				}
				//如果搜索框为空
				if (!reg.test($tar.val())) {
					return ;
				}

				$tar.data('old-val', newVal);

				me.cfg.onSuccess.call(me, {data: resp, keyVal: newVal});
			}
		},

		_empty: function () {
			this._$elem.data('old-val', '');
			this.cfg.onNoSearchVal.call(this);
		},

		//发送搜索请求
		_searchGet: function (word, fn) {
			var me = this,
				data = {};

			data['' + this.cfg.key] = word;

			if (this.cfg.data) {
				$.extend(data, this.cfg.data);
			}

			if (this.cfg.cache && this._db[word]) {
				fn && fn(this._db[word]);
				me.cfg.onAlways.call(me, {data: this._db[word], keyVal: word});
				return ;
			}

			$.ajax({
				type: "GET",
				url: this.cfg.searchUrl,
				dataType: 'json',
				data: data
			})
			.done(function (resp) {
				fn && fn(resp);

				if (me.cfg.cache) {
					me._db[word] = resp;
				}
			})
			.fail(function (resp) {
				me.cfg.onError.call(me, {data: resp, keyVal: word});
			})
			.always(function (resp) {
				me.cfg.onAlways.call(me, {data: resp, keyVal: word});
			});
		}

	};

});
