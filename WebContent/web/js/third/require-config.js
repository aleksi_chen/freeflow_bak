/**
 * Created by yuce_wang on 2016/7/19.
 */
define(function () {
    var basePath="/freeflow/web/js/third",
    version='1.0'
    return{
        urlArgs: 'v=' + version,
        baseUrl: basePath,
        paths: {
            'Vue': '../lib/vue.min',
            // 'Vue': '../common-vue/vue',
            'Vuex': '../lib/vuex.min'
        },
        waitSeconds: 15 // 最多等待15s
    }
})