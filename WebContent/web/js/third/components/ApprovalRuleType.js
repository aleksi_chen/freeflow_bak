/**
 * Created by yuce_wang on 2016/7/20.
 */
define(['Vue','vuex/store', 'vuex/getter', 'vuex/action'], function ( vue, store, getter, action ) {
    var template=[
        '<div class="ff-config-item" @click="setPersonRuleTypr(step)">',
        '<i class="ff-item-check fl" :class="{\'ff-item-check-active\': personRuleType==step}""></i>',
        '<div class="ff-item-row">',
        '<span class="ff-item-title">{{title}}</span>',
        '<span class="ff-title-explain" v-show="step!=1||!conditionPersonSelect">{{explain}}</span>',
        '<button class="ff-btn ff-back-terms" v-show="step==1&&conditionPersonSelect" @click="setConditionPersonSelect">返回选择条件</button>',
        '</div></div>'].join('')
    return vue.extend({
        template: template,
        vuex: {
            getters: {
                personRuleType:getter.personRuleType,
                conditionPersonSelect:getter.conditionPersonSelect
            },
            actions: {
                setPersonRuleTypr:action.setPersonRuleTypr,
                setConditionPersonSelect:action.setConditionPersonSelect,
            }
        },
        props:{
            title: {
                type: String,
                required: true
            },
            explain: {
                type: String,
                required: false
            },
            step: {
                type: String,
                required: true
            }
        }
    })
})