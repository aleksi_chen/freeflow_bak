/**
 * Created by yuce_wang on 2016/7/19.
 */
define(['Vue','vuex/store', 'vuex/getter', 'vuex/action','components/ApprovalStep'], function ( vue, store, getter, action,ApprovalStep ) {
var template=['<div class="fd-bd-header" v-if="!isThirdCall">',
'<div class="fd-header-l fl">',
'<a href="javascript:;" class="back-manager" @click="back"></a>',
'<span class="line"></span>',
'<approval-step  step="1" name="审批设计器"></approval-step>',
'<span class="fd-design-step-arrow"></span>',
'<approval-step  step="2" name="审批规则配置"></approval-step>',
'</div>',
'<div class="fr">',
'<button class="fd-btn fd-btn-default" v-show="approvalStep>0" @click="last">上一步</button>',
'<button class="fd-btn fd-btn-default" v-show="approvalStep<approvalStepCount-1" @click="next">下一步</button>',
'<button class="fd-btn fd-btn-primary" v-show="approvalStep==approvalStepCount-1" @click="submit">保存并启用</button>',
'</div>',
'</div>'].join('')
    return vue.extend({
        template: template,
        vuex: {
            getters: {
                isThirdCall:getter.isThirdCall,
                templateId:getter.templateId,
                personRuleType:getter.personRuleType,
                personSelectList:getter.personSelectList,
                conditionSelect:getter.conditionSelect,
                conditionList:getter.conditionList,
                conditionNumberData:getter.conditionNumberData,
                conditionPersonData:getter.conditionPersonData,
                conditionComplete:getter.conditionComplete,
                approvalStep:getter.approvalStep,
                approvalStepCount:getter.approvalStepCount
            },
            actions: {
                setApprovalStep:action.setApprovalStep,
                submitApprovalRule:action.submitApprovalRule
            }
        },
        data:function () {
            return {
                isClicked:false,
                search:'?webLappToken=' + webUtil.getRequestKey()['webLappToken'] + '&lappName=freeflow'
            }
        },
        created: function () {
            this.setApprovalStep(1);
        },
        methods: {
            back:function () {
                window.location.href = "/freeflow/web/approval-manage.json"+this.search
            },
            last:function () {
                //this.setApprovalStep(this.approvalStep-1);
                var self=this
                if(!this.isClicked){
                    self.isClicked=true
                    var data={
                        templateId:this.templateId,
                        personRuleType:this.personRuleType,
                        personSelectList:this.personSelectList,
                        conditionSelect:this.conditionSelect,
                        conditionList:this.conditionList,
                        conditionNumberData:this.conditionNumberData,
                        conditionPersonData:this.conditionPersonData,
                        conditionComplete:this.conditionComplete
                    }
                    this.submitApprovalRule(data).then(function (rsp) {
                        console.log(rsp)
                        self.isClicked=false
                        if(rsp.meta&&rsp.meta.success){
                            // self.$alert(rsp.meta.msg)
                            window.location.href='/freeflow/web/form-design.json'+self.search+'&formId='+self.templateId
                        } else{
                            self.$alert(rsp.msg||rsp.meta.msg)
                        }
                    })
                }
            },
            next:function () {
                this.setApprovalStep(this.approvalStep+1);
            },
            submit:function () {
                var self=this
                if(!this.isClicked){
                    self.isClicked=true
                    var data={
                        templateId:this.templateId,
                        personRuleType:this.personRuleType,
                        personSelectList:this.personSelectList,
                        conditionSelect:this.conditionSelect,
                        conditionList:this.conditionList,
                        conditionNumberData:this.conditionNumberData,
                        conditionPersonData:this.conditionPersonData,
                        conditionComplete:this.conditionComplete
                    }
                    this.submitApprovalRule(data).then(function (rsp) {
                        console.log(rsp)
                        self.isClicked=false
                        if(rsp.meta&&rsp.meta.success){
                            // self.$alert(rsp.meta.msg)
                            window.location.href='/freeflow/web/approval-manage.json'+self.search
                        } else{
                            self.$alert(rsp.msg||rsp.meta.msg)
                        }
                    })
                }
            }
        },components:{
            ApprovalStep:ApprovalStep
        }
    })
})