/**
 * Created by yuce_wang on 2016/8/5.
 */
define(['Vue', 'vuex/store', 'vuex/getter', 'vuex/action'], function (vue, store, getter, action) {
    var template = [
        '<div class="ff-btn-content" v-if="isThirdCall"><button class="fd-btn fd-btn-default" @click="cancleThirdCall">取消</button><button class="fd-btn fd-btn-primary" @click="submit">确定</button></div>'].join('')
    return vue.extend({
        template: template,
        vuex: {
            getters: {
                isThirdCall:getter.isThirdCall,
                templateId:getter.templateId,
                personRuleType:getter.personRuleType,
                personSelectList:getter.personSelectList,
                conditionSelect:getter.conditionSelect,
                conditionList:getter.conditionList,
                conditionNumberData:getter.conditionNumberData,
                conditionPersonData:getter.conditionPersonData,
                conditionComplete:getter.conditionComplete
            },
            actions: {
                cancleThirdCall:action.cancleThirdCall,
                submitApprovalRule:action.submitApprovalRule
            }
        },
        data: function(){
            return {
                isClicked:false
            }
        },
        methods:{
            submit:function () {
                if(!this.isClicked){
                    this.isClicked=true
                    var data={
                        isThirdCall:this.isThirdCall,
                        templateId:this.templateId,
                        personRuleType:this.personRuleType,
                        personSelectList:this.personSelectList,
                        conditionSelect:this.conditionSelect,
                        conditionList:this.conditionList,
                        conditionNumberData:this.conditionNumberData,
                        conditionPersonData:this.conditionPersonData,
                        conditionComplete:this.conditionComplete
                    }
                    this.submitApprovalRule(data)
                }
            }
        }
    })
})