/**
 * Created by yuce_wang on 2016/7/19.
 */
define(['Vue','vuex/store', 'vuex/getter', 'vuex/action','components/ApprovalRuleType','components/ApprovalPersonSelect','components/ConditionApprovalPerson','components/ThirdCallBtn'], function ( vue, store, getter, action,ApprovalRuleType,ApprovalPersonSelect,ConditionApprovalPerson,ThirdCallBtn ) {
    var template = [
        '<div class="fd-step2">',
        '<div class="ff-design-title">审批规则配置</div>',
        '<approval-rule-type title="不指定审批人" explain="由发起人或审批人来选择下级或多个下级审批人，任何审批人都可以结束流程。" step="0"></approval-rule-type>',
        '<approval-rule-type title="不限条件指定关键审批人" explain="由管理员指定关键审批人，该审批必须由关键审批人全部审批结束后才能完成审批。" step="2"></approval-rule-type>',
        '<approval-person-select></approval-person-select>',
        '<approval-rule-type title="按条件指定关键审批人" explain="由管理员按条件指定关键审批人，该审批必须相应条件的关键审批人全部审批结束后才能完成审批。" step="1"></approval-rule-type>',
        '<condition-approval-person></condition-approval-person>',
        '<third-call-btn></third-call-btn>',
        '</div>'].join('')
    return vue.extend({
        template: template,
        vuex: {
            getters: {
            },
            actions: {
                getApprovalRule:action.getApprovalRule
            }
        },
        methods:{
        },
        created:function () {
            this.getApprovalRule()
        },
        components:{
            ApprovalRuleType:ApprovalRuleType,
            ApprovalPersonSelect:ApprovalPersonSelect,
            ConditionApprovalPerson:ConditionApprovalPerson,
            ThirdCallBtn:ThirdCallBtn
        }
    })
})