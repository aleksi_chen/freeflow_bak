define([], function () {
    return {
        isThirdCall:function (state) {
            return state.isThirdCall
        },
        templateId:function (state) {
            return state.templateId
        },
        approvalStep: function (state) {
            return state.approvalStep;
        },
        approvalStepCount: function (state) {
            return state.approvalStepCount;
        },
        personRuleType: function (state) {
            return state.personRuleType;
        },
        personSelectList: function (state) {
            return state.personSelectList;
        },
        openDialog: function (state) {
            return state.openDialog;
        },
        conditionSelect: function (state) {
            return state.conditionSelect;
        },
        conditionList: function (state) {
            return state.conditionList;
        },
        conditionCount: function (state) {
            return state.conditionCount;
        },
        conditionNumberData: function (state) {
            return state.conditionNumberData;
        },
        conditionPersonSelect: function (state) {
            return state.conditionPersonSelect;
        },
        conditionPersonData: function (state) {
            return state.conditionPersonData;
        },
        conditionComplete: function (state) {
            return state.conditionComplete;
        }
    }
});