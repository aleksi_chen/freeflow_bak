/**
 * Created by yuce_wang on 2016/7/19.
 */
require(['require-config'],function(config){
    require.config(config)
    require(['Vue','vuex/store', 'vuex/getter', 'vuex/action','components/MenuContainer','components/ApprovalRuleSet','plugins/alert'],
        function(Vue, store,getter,action, MenuContainer, ApprovalRuleSet,alert){
        console.log("vue init")
        var template=['<div id="approvalRule" :class="{blur:openDialog}">',
            '<menu-container></menu-container>',
            '<approval-rule-set></approval-rule-set>',
            '</div>'].join('');
        var app = new Vue({
            store: store,
            el: '#approvalRule',
            vuex: {
                getters: {
                    openDialog:getter.openDialog
                },
                actions: {
                }
            },
            template: template,
            components: {
                MenuContainer: MenuContainer,
                ApprovalRuleSet:ApprovalRuleSet
            }
        });
    })
})
