/**
 * Created by yuce_wang on 2016/7/28.
 */
define(['Vue'
], function ( vue ) {
    var alertInstance = null;

    function encode ( txt ) {
        if ( typeof txt != "string" ) return "";
        txt = txt.replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/\"/g, "&quot;")
            .replace(/\'/g, "&#39;")
            .replace(/ /g, "&nbsp;")
        return txt;
    }

    function msg ( name, type ) {
        var template = ['<div class="q-bg" v-show="display"><div class="q-alert"><div class="q-alert-heard">',
            '<span class="q-alert-closebtn" @click="destroy"></span>',
            '</div><div class="q-alert-body">',
            '<h1 class="q-alert-title text-center">提示</h1>',
            '<p class="q-alert-text text-center">{{message}}</p>',
            '</div><div class="q-alert-button-group text-center">',
            '<a class="q-btn btn-primary" @click="destroy">确定</a>',
            '</div></div></div>'].join('')
        return vue.extend({
            template: template,
            methods:{
                destroy:function () {
                    this.display=false
                }
            }
        })
    }

    function install () {
        var AlertConstructor = msg();
        Object.defineProperty(vue.prototype, '$alert', {
            get: function () {
                return  function(message){
                    if (alertInstance){
                        alertInstance.$destroy(true)
                    };
                    alertInstance = new AlertConstructor({
                        el: document.createElement('div'),
                        data:function(){
                            return {
                                message: encode(message),
                                display:true
                            };
                        }
                    });
                    alertInstance.$appendTo(document.body);
                };
            }
        });
    }

    vue.use(install)
});