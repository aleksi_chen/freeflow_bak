;(function (factory) {
    factory = factory(window, document);

    webUtil.namespace('F.d.controller', factory);

})(function (window, doc) {
    var Designer = F.d.Designer;
    var SelectApprover = F.d.SelectApprover;
    var __Global_Approve_Arr;//有审批人数据时的全局变量
    var __Global_Approve_Arr_Flag = true;//有审批人数据时的开关
    var urlParam = 'webLappToken=' + webUtil.getRequestKey()['webLappToken'] + '&lappName=freeflow';
    var controller = {
        data: {

            getFormUrl: '/freeflow/v1/rest/form-template/detail/formTemplate',	// 获取表单设置控件url
            // saveUrl: '/freeflow/v1/rest/form-template/create/formTemplate.json?' + urlParam,
            createApproval: '/freeflow/v1/rest/form-template/create/formTemplate?' + urlParam,//保存模板
            saveUrl: '/freeflow/v1/rest/form-template/save/recordTemplate',
            reviseUrl: '/freeflow/v1/rest/form-template/update/formTemplate?' + urlParam,
            formArea: null,
            selectApprover: null,
            approverData: {},
            addKeyApproverArr: [],//指定审批人信息
            addApproverArr: [],//指定审批人信息
            curApprType: 0  // 0为不指定审批人， 1为审批人， 2为普通审批人
        },

        init: function () {
            var me = this;
            this.initFormArea();	// 初始化控件拖放
            this.bindUI();			// 绑定事件
            this.setAuthToken();    //为每个请求的url添加授权token

            webUtil.tab('.fd-widgetpanel', 'curr');	// tab切换

            var formId = webUtil.getUrlValue('formId');

            //编辑将之前选择模板生成
            if (formId) {
                this.getFormDataPost(formId).done(function (resp) {
                    // console.log(resp)
                    var title = resp.data.template.title;
                    var status = resp.data.template.status;
                    // $('.fd-title').text(title);
                    // console.log(resp.data)
                    if (resp.data.template.components) {
                        me.renderForm(resp.data, status,title);
                    } else {//空白模板
                        me.renderFormWithTitle(status,title);
                    }
                });
            }
        },
        addArea:function (contentArr,title) {
            var arr=contentArr||[]
            // if(arr.length===0||arr[0].type!=='titlefield'){
            //     arr.unshift({
            //         "id": "titlefield-" + webUtil.uuid(),
            //         "type": "titlefield",
            //         "label": "标题",
            //         "placeholder": "请输入",
            //         "require": true
            //     })
            // }
             arr.unshift({
                "id": "namefield",
                "type": "namefield",
                "label":title||"审批名称",
                "require": true
            })
            return arr
        },
        renderForm: function (data, status,title) {
            //第一步内容加载
            var content = JSON.parse(data.template.components);
            var selectId = data.approvalRule.cndSelectId
            if (selectId && data.approvalRule.approvalType == '1') {//添加作为审批条件的控件选中的selected表示
                content.forEach(function (e) {
                    if (e.id === selectId) {
                        e.selected = true
                    }
                });
            }
            content=this.addArea(content,title)
            this.data.formArea.renderForm(content, status);
            //第二步内容加载
            // if(data.approvalRule.content != null){
            // 	var numbers = !data.approvalRule.content ? [] : JSON.parse(data.approvalRule.content);
            // 	this.data.formArea.renderFormStep2(numbers);
            // 	__Global_Approve_Arr = numbers ;
            // }
            /*var props = content.props;
             var settingTpl = Designer.getSettingTpl(props);
             $('.fd-settingpanel .fd-panel-body').html(settingTpl);*/
        },
        renderFormWithTitle: function (status,title) {
            var content = [{
                "id": "titlefield-" + webUtil.uuid(),
                "type": "titlefield",
                "label": "标题",
                "placeholder": "请输入",
                "require": true
            }];
            content=this.addArea(content,title)
            this.data.formArea.renderForm(content, status);
        },
        // 初始化控件拖放区域
        initFormArea: function () {
            var formArea = new F.d.FormArea({
                'target': '.fd-designer',
                'className': 'fd-designer-inner',
                'dragTarget': '.fd-widgetitem'	// 拖动元素的选择器
            })
                .on('dragStart', this._onDragStart.bind(this))
                .on('draging', this._onDraging.bind(this))
                .on('dragEnd', this._onDragEnd.bind(this))
                // 选择控件的监听
                .on('selectComp', this._onSelectComp.bind(this))
                // 删除控件的监听
                .on('delComp', this._onDelComp.bind(this));

            this.data.formArea = formArea;
        },

        _onSelectComp: function (component) {
            var props = component.props;

            var settingTpl = Designer.getSettingTpl(props);

            $('.fd-settingpanel .fd-panel-body').html(settingTpl);
            // 校验数据
            this.validate();
        },

        _onDelComp: function (component) {
            if (component.isActive) {
                $('.fd-settingpanel .fd-panel-body').html('');
            }
        },

        _onDragStart: function (e, component) {
            var type = component.props ? component.props.type : $(component.srcElem).data('type');

            $('html').addClass('fd-cursor-move');
            this.showDraingMask(e, type);
        },

        _onDraging: function (e, component) {
            var $mask = $('.fd-draging-mask'),
                mtouch = e.mTouchEvent;

            var translateStr = 'translate3d(' + mtouch.moveX + 'px,' + mtouch.moveY + 'px,0)'

            $mask.css({
                'transform': translateStr,
                '-webkit-transform': translateStr,
                '-moz-transform': translateStr,
                '-o-transform': translateStr,
                '-ms-transform': translateStr
            });
        },

        _onDragEnd: function (e) {
            $('html').removeClass('fd-cursor-move');
            this.hideDraginMask();
        },

        // 绑定事件
        bindUI: function () {
            var me = this;
            $('.back-manager').on('click', function () {
                window.location.href = "/freeflow/web/approval-manage.json?" + urlParam;
            })
            // 标题输入
            $('body').on('input', '.fd-setting-label input', function (e) {
                    me._onLabelChange('label', this.value, 20);
                })
                // 提示语输入
                .on('input', '.fd-setting-placeholder input', function (e) {
                    me._onLabelChange('placeholder', this.value, 40);
                })
                // 时间区间的标题
                .on('input', '.fd-daterange-input1, .fd-daterange-input2', function () {
                    var index = $(this).hasClass('fd-daterange-input1') ? 0 : 1;
                    me._onDrLabelChange(index, this.value);
                })
                // 单选框选项
                .on('input', '.fd-setting-options input', function () {
                    var index = $('.fd-setting-options input').index(this);
                    me._onOptionsChange(index, this.value, 40);
                })
                // 添加选项
                .on('click', '.fd-option-add', function (e) {
                    me._optionItemHandler(e, 'add');
                })
                // 删除选项
                .on('click', '.fd-option-del', function (e) {
                    me._optionItemHandler(e, 'del');
                })
                // 日期格式
                .on('change', 'input[name="dateformat"]', function () {
                    var component = me.getCurrComponent();	// 当前选中控件和属性

                    component.props.format = this.value;
                })
                // require是否必填  decimal是否允许小数
                .on('change', '.fd-setting-require input', function () {
                    var component = me.getCurrComponent();	// 当前选中控件和属性
                    var $placeholder = component.$comp.find('.fd-component-placeholder');
                    var oldVal = $placeholder.html();
                    var name=this.className
                    var obj={
                        require:{
                            title:'(必填)',
                            regex:/\(必填\)(?=\(允许小数\)|$)/g
                        },
                        decimal:{
                            title:'(允许小数)',
                            regex:/\(允许小数\)(?=\(必填\)|$)/g
                        },
                    }
                    component.props[name] = this.checked;
                    $placeholder.html(this.checked ? oldVal + obj[name].title : oldVal.replace(obj[name].regex,''));
                })
                // 选择是否需要审批人
                // .on('change', 'input[name=need-approver]', function (e) {
                // me._onNeedApprover(e);
                // })
                // 保存
                // .on('click', '.fd-btn-save', function (e) {
                // 	me._save(e);
                // })
                //下一步
                .on('click', '.fd-btn-next', function (e) {
                    // me._nextPage(e)
                    me._save(e)
                })
                // 修改审批名称
                // .on('click', '.fd-title', function (e) {
                //     me.setTitleArea();
                // })
                // .on('input', '.title-input', function (e) {
                //     me.checkAndSetTitle(e);
                // })
            //上一页
            // .on('click', '.fd-btn-pre', function(e){
            //     var self = this;
            //     $('.fd-bd-main').css({
            //         "transform": "translateX(0)"
            //     });
            //     $('.fd-bd-nextPage').css({
            //         "transform": "translateX(110%)"
            //     });
            //     setTimeout(function(){
            //         $(self).hide();
            //         $('.fd-btn-next').show();
            //         $('.step-two').removeClass('fd-design-active');
            //        $('.step-one').addClass('fd-design-active');
            //     }, 0)
            //     //隐藏保存按钮
            //     $('.fd-btn-save').hide();
            // })
            //切换选择审批人tab
            // .on('click', '.fd-appr-type .tab', function(e){
            //     $('.fd-appr-type .tab').removeClass('active');
            //     $('.fd-appr-con').hide();
            //     $(this).addClass('active');
            //     $('.fd-appr-con').eq($(this).index()).show();
            //     me.data.curApprType = $(this).index();
            // })
            //添加审批人
            // .on('click', '#addKeyApprover', function(e){
            // 	var ele = $(this);
            //
            //     me.showOrganizational(ele, me.data.curApprType);
            // })
            // .on('click', '#addApprover', function(e){
            //    var ele = $(this);
            //    console.log(me.data)
            //    me.showOrganizational(ele, me.data.curApprType);
            // })
        },
        //切换到下一步
        // _nextPage: function(){
        //     var me = this;
        // 	var formArea = this.data.formArea;
        // 	var allComponents = formArea.getAllComponents();
        // 	if (!allComponents.length) {
        // 		$('.fd-widgetpanel [data-panel="1"]').click();
        // 		webUtil.tips('请先添加表单控件');
        // 		return ;
        // 	}else{
        // 		for (var i = 0, len = allComponents.length; i < len; i++) {
        // 			var component = allComponents[i];
        // 			var errors = $(component.srcElem).data('errors');
        // 			if (errors && errors.length) {
        // 				// 选中有错的控件
        // 				formArea.selectComp(component.srcElem);
        // 				return ;
        // 			}
        // 		}
        // 		// $('.fd-bd-main').css({
        // //          "transform": "translateX(-110%)"
        // //          });
        // //          $('.fd-bd-nextPage').css({
        // //          "transform": "translateX(0)"
        // //          });
        // //          setTimeout(function(){
        // //              $('.fd-btn-next').hide();
        // //              $('.fd-btn-pre').show();
        // //              $('.step-one').removeClass('fd-design-active');
        // //              $('.step-two').addClass('fd-design-active');
        // //          }, 0)
        // //          //显示保存按钮
        // //          $('.fd-btn-save').show();
        // 			window.location.href = window.location.href;
        // 	}
        // },
        //添加审批人调出组织架构
        // showOrganizational: function(ele, curApprType){
        // var me = this;
        // $('#body').addClass('blur');
        // var selectedPersonArr = [];
        // if(curApprType == 1){
        // 	if(__Global_Approve_Arr_Flag && typeof(__Global_Approve_Arr) != "undefined" && __Global_Approve_Arr != ""){
        // 		selectedPersonArr = __Global_Approve_Arr;
        // 		__Global_Approve_Arr_Flag = false;
        // 	}else{
        // 		selectedPersonArr = me.data.addKeyApproverArr;
        // 	}
        // }
        // else if(curApprType == 2){
        // 	selectedPersonArr = me.data.addApproverArr;
        // }
        //
        // new LappPersonSelect({
        // 	parentElement: 'body',
        // 	//appId: '10103',
        // 	//lightAppId: '',
        // 	eId: __global.eid,
        // 	selectedMembers: me.selectedApprovalPerson(selectedPersonArr), //记录上次选了哪些人
        // 	//openId: '5638397000b0e5cbba2743c1',
        // 	cancelCallBack: cancel,	          //取消回调
        // 	sureCallBack: sureCallBack,       //确定回调
        // 	//removeCallBack: removeCallBack,    //删除回调
        // 	existingSessionIsNeed: false
        // 	})
        //
        //
        // //取消回调
        // function cancel(resp){
        // 	console.log(resp);
        // 	$('#body').removeClass('blur');
        // 	__Global_Approve_Arr_Flag = true;
        // 	//$('input[name="need-approver"][value="0"]').prop('checked', true);
        // }
        //
        // //确定回调
        // function sureCallBack(resp){
        // 	__Global_Approve_Arr = "";
        // 	var selectedArr = [];
        // 	console.log(resp);
        // 	$('#body').removeClass('blur');
        // 	var html = '';
        // 	var eleId = $(ele)[0].id;
        // 	$.each(resp, function(key, val){
        // 		var obj = {
        // 			oId: val.userId,
        // 			name: val.userName,
        // 			photo: val.photoUrl
        // 		};
        //
        // 		selectedArr.push(obj);
        //
        // 		html += '<div class="approval-person-photo"><img src="'+ val.photoUrl +'"/><p>'+ val.userName +'</p></div>';
        // 	})
        //
        // 	if(curApprType == 1){
        // 		me.data.addKeyApproverArr = selectedArr;
        // 	}
        // 	else if(curApprType == 2){
        // 		me.data.addApproverArr = selectedArr;
        // 	}
        //
        // 	html += '<div class="add-btn" id="'+ eleId +'"><span class="hengxian"></span><span class="shuxian"></span></div>';
        // 	$(ele).parent().html(html);
        // }

        //删除回调
        /*function removeCallBack(resp){
         arr = [];
         $.each(resp, function(key, val){
         var obj = {
         oId: val.userId,
         name: val.userName,
         photo: val.photoUrl
         };

         arr.push(obj);
         })
         }*/
        // },

        //记录已选择审批人信息
        // selectedApprovalPerson: function(arr){
        // 	var obj = {};
        // 	if(arr.length){
        // 		$.each(arr, function(index, val){
        // 			obj[val.oId] = {
        // 				userId: val.oId,
        // 				photoUrl: val.photo,
        // 				userName: val.name
        // 			};
        // 		})
        // 	}
        //
        // 	return obj;
        // },

        // 保存操作
        _save: function (e) {
            var me = this;
            var formArea = this.data.formArea;

            // 先校验审批设置数据 
            // if (!this._checkFormSetting()) {
            // 	return ;
            // }

            var allComponents = formArea.getAllComponents();


            var componentsArr=[],title
            for (var i = 0, len = allComponents.length; i < len; i++) {
                var component = allComponents[i];
                var errors = $(component.srcElem).data('errors');
                if (errors && errors.length) {
                    // 选中有错的控件
                    formArea.selectComp(component.srcElem);
                    return;
                }
                if(component.props.type!=='namefield'){
                    componentsArr.push(component)
                }else{
                    title= component.props.label
                }
            }

            if (!componentsArr.length) {
                $('.fd-widgetpanel [data-panel="1"]').click();
                webUtil.tips('请先添加表单控件');
                return;
            }

            // 提交数据
            var data = this._getData(componentsArr);
            //审批人数据
            // var approvalRule;
            // var appContent;
            // if(JSON.stringify(this.data.addKeyApproverArr) == "[]"){
            // 	appContent = JSON.stringify(__Global_Approve_Arr);
            // }else{
            // 	appContent = JSON.stringify(this.data.addKeyApproverArr);
            // }
            // if(this.data.curApprType === 0) {
            //     approvalRule = {
            //         "approvalType": "0"
            //     };
            // }
            // if(this.data.curApprType === 1){
            //     approvalRule = {
            //         "approvalType": "2",
            //         "content": appContent
            //     };
            // }
            // if (this.data.curApprType === 2){
            //     approvalRule = {
            //         "approvalType": "2",
            //         "content": JSON.stringify(this.data.addApproverArr)
            //     };
            // }

            var formId = webUtil.getRequestKey().formId,
            // common = webUtil.getRequestKey().common,
                params;
            //保存
            if (!title) {
                webUtil.tips('模板名称不能为空');
                return
            }
            if (formId) {
                params = {
                    template: {
                        id: formId,
                        appName: "freeflow",//应用名称 必传
                        title: title,//标题
                        components: JSON.stringify(data.components)
                    }
                }
            } else {
                webUtil.tips('模板ID为空');
            }
            // //是修改
            // if(formId && !common){
            // 	params = {
            //        "template": {
            //        	"id": formId,
            //        	"appName":"freeflow",//应用名称必传 有问题找小周
            //            "title": $('.fd-step2 .fd-form-name').val(),
            //            "components": JSON.stringify(data.components),
            //            "status": "1"
            //        },
            //        "approvalRule": approvalRule
            //    }
            // }
            //
            // //新建保存
            // else{
            // 	params = {
            //        "template": {
            //        	"appName":"freeflow",//应用名称必传 有问题找小周
            //            "title": $('.fd-step2 .fd-form-name').val(),
            //            "components": JSON.stringify(data.components),
            //            "status": "1"
            //        },
            //        "approvalRule": approvalRule
            //    }
            // }
            this._createFormPost(params);
        },

        // 获取数据
        _getData: function (allComponents) {
            // var title = $('.fd-form-name').val().trim();
            // var needApprover = $('input[name="need-approver"]:checked').val();

            var components = [];

            allComponents.forEach(function (item) {
                delete item.props.selected
                components.push(item.props);
            });

            return {
                // title: title,
                // approverInfo: this.data.approverData,
                components: components
            };
        },

        // 创建表单配置
        _createFormPost: function (data) {
            var deferred = $.Deferred();
            var formId = webUtil.getUrlValue('formId');
            // var common = webUtil.getUrlValue('common');
            var url = this.data.createApproval;
            // if(formId && !common){
            // 	url = this.data.reviseUrl;
            // }else{
            // 	url = this.data.saveUrl;
            // }
            $.ajax({
                url: url,
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                timeout: 3000
            }).then(function (resp) {
                // console.log(resp, data);
                if (resp.meta.success === true) {
                    deferred.resolve(resp);
                    webUtil.tips('保存成功');
                    window.location.href = '/freeflow/web/approval-rule.json?detail=' + formId + '&' + urlParam
                } else {
                    deferred.reject(resp.errorMsg || '保存失败了');
                    webUtil.tips(resp.meta.msg);
                }
            }, function (resp) {
                deferred.reject('保存失败了');
//				if(status=='timeout'){//超时,status还有success,error等值的情况
//		 　　　　　 ajaxTimeoutTest.abort();
//		　　　　　  alert("超时");
//			　　}
            });

            return deferred.promise();
        },

        // 校验审批设置数据
        // _checkFormSetting: function () {
        // 	var $formName = $('.fd-step2 .fd-form-name');
        //
        // 	if (!$formName.val().trim()) {
        // 		webUtil.tips('请填写审批名称');
        // 		$('.fd-widgetpanel [data-panel="0"]').click();
        // 		$formName.focus();
        // 		return false;
        // 	}
        //
        // 	return true;
        // },

        // 输入内容改变监听
        _onLabelChange: function (type, value, valLengh) {
            var component = this.getCurrComponent();	// 当前选中控件和属性

            var placeholder = type == 'placeholder' && component.props.require ? '(必填)' : ''
            placeholder+= type == 'placeholder'&& component.props.decimal ? '(允许小数)' : '';

            component.props[type] = value;
            component.$comp.find('.fd-component-' + type)
                .html(webUtil.getByteVal(value, valLengh) + placeholder);

            var errors = component.$comp.data('errors') || [];

            this.validate(type).done(function () {
                if (errors.indexOf(type) > -1) {
                    //重置状态
                    var $elem = $('.fd-settings').find('.fd-setting-' + type);
                    $elem.find('input').removeClass('error');

                    var $itemInfo = $elem.find('.fd-setting-iteminfo');
                    $itemInfo.removeClass('error').html($itemInfo.data('tips'));

                    errors.splice(errors.indexOf(type), 1);
                    component.$comp.data('errors', errors);
                }
            }).fail(function () {
                if (errors.indexOf(type) == -1) {
                    errors.push(type);
                    component.$comp.data('errors', errors);
                }
            });
        },
        // 输入内容改变监听
        _onOptionsChange: function (index, value, valLengh) {
            var component = this.getCurrComponent();	// 当前选中控件和属性
            component.props.options[index] = value;
            // webUtil.getByteVal(value, valLengh)
            var type='options'+index
            var errors = component.$comp.data('errors') || [];

            this.validate( 'options' ,index).done(function () {
                if (errors.indexOf(type) > -1) {
                    //重置状态
                    var $elem = $('.fd-setting-options').find('.fd-setting-itembody:eq('+index+')')
                    $elem.find('input').removeClass('error');
                    errors.splice(errors.indexOf(type), 1);
                    component.$comp.data('errors', errors);
                }
            }).fail(function () {
                if (errors.indexOf(type) == -1) {
                    errors.push(type);
                    component.$comp.data('errors', errors);
                }
            });
        },
        // 时间区间标题改变
        _onDrLabelChange: function (index, value) {
            var component = this.getCurrComponent();	// 当前选中控件和属性
            var checkerType = index === 0 ? 'starttime' : 'endtime';

            component.props.labels[index] = value;
            component.$comp.find('.fd-component-label')
                .eq(index)
                .html(webUtil.getByteVal(value, 20));

            var errors = component.$comp.data('errors') || [];

            this.validate(checkerType).done(function () {
                if (errors.indexOf(checkerType) > -1) {
                    //重置状态
                    var $daterangeLabel = $('.fd-setting-daterangelabel');
                    $daterangeLabel.find('input').eq(index).removeClass('error');

                    var $itemInfo = $daterangeLabel.find('.fd-setting-iteminfo').eq(index)
                    $itemInfo.removeClass('error').html($itemInfo.data('tips'));

                    errors.splice(errors.indexOf(checkerType), 1);
                    component.$comp.data('errors', errors);
                }
            }).fail(function () {
                if (errors.indexOf(checkerType) == -1) {
                    errors.push(checkerType);
                    component.$comp.data('errors', errors);
                }
            });
        },

        // 新增或删除选项
        _optionItemHandler: function (e, type) {
            var component = this.getCurrComponent();	// 当前选中控件和属性
            var options = component.props.options;
            var isAdd = type == 'add';
            var $target = $(e.target);
            var $settingOptions = $target.closest('.fd-setting-options');

            if (isAdd) {
                if (options.length >= 50) {
                    return;
                }

                if (options.length == 49) {
                    $settingOptions.addClass('nomore-add');
                }
                $settingOptions.removeClass('limit-del');
            } else {
                if (options.length <= 1) {
                    return;
                }

                if (options.length == 2) {
                    $settingOptions.addClass('limit-del');
                }
                $settingOptions.removeClass('nomore-add');
            }

            var $itembody = $target.closest('.fd-setting-itembody');
            var index = $itembody.index();

            // 添加选项
            if (isAdd) {
                // 确定新的选项名字
                for (var i = 1, len = options.length; i <= len; i++) {
                    if (options.indexOf('选项' + i) == -1) {
                        break;
                    }
                }
                var newOption = '选项' + i;

                var $newItemBody = $itembody.clone();
                $newItemBody.find('input').val(newOption);

                // 插入新的选项
                options.splice(index + 1, 0, newOption);
                $itembody.after($newItemBody);
            } else {
                options.splice(index, 1);
                $itembody.remove();
            }
        },

        // 数据校验
        validate: function (checkerType,args) {
            var component = this.getCurrComponent();
            var type = component.props.type;
            var promise = Designer[type].validator.handler(component, checkerType,args);

            return promise;
        },

        getCurrComponent: function () {
            var currComponent = this.data.formArea.get('currComponent');

            if (!currComponent) {
                return;
            }

            return {
                props: currComponent.props,
                $comp: $(currComponent.srcElem)
            }
        },

        // 选择是否需要审批人
        // _onNeedApprover: function (e) {
        // 	var me = this;
        // 	var $target =  $(e.target);
        //
        // 	if ($target.val() == 1) {
        // 		/*this.data.selectApprover = new SelectApprover({
        // 			'switchSearch': false
        // 		})*/
        //
        // 		//取消回调
        // 		/*var cancel = function(resp){
        // 			console.log(resp);
        // 			$('#body').removeClass('blur');
        // 			$('input[name="need-approver"][value="0"]').prop('checked', true);
        // 		}
        //
        // 		//确定回调
        // 		var sureCallBack = function(resp){
        // 			console.log(resp);
        // 			$('#body').removeClass('blur');
        // 		}
        // 		new LappPersonSelect({
        // 									parentElement: 'body',
        // 									//appId: '10103',
        // 									//lightAppId: '',
        // 									eId: '1708351',
        // 									//openId: '5638397000b0e5cbba2743c1',
        // 									cancelCallBack: cancel,
        // 									sureCallBack: sureCallBack,
        // 									existingSessionIsNeed: false
        // 			        	})*/
        // 		/*$('#body').removeClass('blur');
        // 		.on('confirm', function (data) {
        //
        // 			if (!data.isSelected || !data.needDeptHead && !data.person) {
        // 				$('input[name="need-approver"][value="0"]').prop('checked', true);
        // 				return ;
        // 			}
        //
        // 			me.data.approverData = {
        // 				deptLeader: data.needDeptHead ? true : false,
        // 				openIds: [data.person.oId]
        // 			};
        // 		})
        // 		.on('cancel', function () {
        // 			$('#body').removeClass('blur');
        // 			$('input[name="need-approver"][value="0"]').prop('checked', true);
        // 		});*/
        //
        // 		//$('#body').addClass('blur');
        // 	} else {
        //
        // 	}
        // },

        // 显示拖动的朦层
        showDraingMask: function (e, type) {
            var $mask = $('.fd-draging-mask');

            $mask.html($('.fd-widgetitem[data-type=' + type + ']').html());

            var width = $mask.outerWidth(),
                height = $mask.outerHeight();

            $mask.css({
                'left': (e.mTouchEvent.startX - width / 2) + 'px',
                'top': (e.mTouchEvent.startY - height / 2) + 'px'
            }).show();
        },

        // 隐藏拖动的朦层
        hideDraginMask: function () {
            $('.fd-draging-mask').hide();
        },

        // 获取表单数据
        getFormDataPost: function (formId) {
            var deferred = $.Deferred();

            $.ajax({
                    'url': this.data.getFormUrl + '/' + formId,
                    'type': 'GET',
                    'contentType': 'application/json;charset=UTF-8'
                })
                .always(function (resp) {
                    if (resp && resp.meta.success === true) {
                        deferred.resolve(resp);
                    } else {
                        deferred.reject(resp);
                    }
                });

            return deferred.promise();
        },
        setAuthToken: function () {
            //全局请求设置-->为每个请求带上授权token
            //获取url query
            var req = webUtil.getRequest();
            if (req.lappName && req.webLappToken) {
                $.ajaxSetup({
                    beforeSend: function (e, data) {
                        console.log(e, data)
                        data.url += '?lappName=' + req.lappName + '&webLappToken=' + req.webLappToken;
                        return true;
                    }
                });
            }
        }
    };

    return controller;
});
