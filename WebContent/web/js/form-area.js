;
(function(factory) {
    factory = factory(window, document, jQuery, mTouch);

    webUtil.namespace('F.d.FormArea', factory);

})(function(window, doc, $, mTouch) {
    var Widget = F.d.Widget;
    var Designer = F.d.Designer;

    function FormArea(cfg) {
        Widget.call(this);
        return this.render(cfg);
    }

    FormArea.prototype = $.extend({}, Widget.prototype, {
        constructor: FormArea,

        initCfg: function(cfg) {
            this.cfg = $.extend({
                'dragTarget': '', // 拖动元素的选择器
                'onInAear': function() {}, // 拖动元素进入设计器区域的监听
                'onLeaveAear': function() {}, // 拖动元素离开设计器区域的监听
                'onDragStart': function() {}, // 拖动开始的监听
                'onDraging': function() {}, // 拖动中的监听
                'onDragEnd': function() {}, // 拖动开始的监听
                'onSelectComp': function() {}, // 选择控件的监听
                'onDelComp': function() {} // 删除控件的监听
            }, cfg);
        },

        ready: function() {
            var me = this;
            var occupyLine = document.createElement('div');
            occupyLine.className = 'fd-occupyline';
            // 占位线
            this.set('occupyLine', occupyLine);
        },

        // 根据表单数据初始化表单控件
        renderForm: function(components,status) {
            var me = this;
            var $components = $();
            components.forEach(function(item) {
                $components = $components.add(me._tplComponent$(item.type, item));
            });

            this.set('componentStatus', status);
            this._$elem.append($components);
            this.checkAreaLen()

            //默认显示第一个
            var currElem = this._$elem.find('.fd-component').eq(0);
            currElem.addClass('active');
            var currComponent = this._packageComponent(currElem);
            this.set('currComponent', currComponent);
            this.fire('selectComp', currComponent);
        },
        //检测条件数量来是否显示提示
        checkAreaLen:function () {
            var keys = Object.keys(this._components)
            if(keys.length>2){
                $('.fd-add-field').hide()
            }else{
                $('.fd-add-field').show()
            }
        },
        renderFormStep2: function(contentStr) {
            if(!contentStr.length){
                return;
            }
            var me = this;
            var contentList = '';
            var content = contentStr;
            for (x in content){ 
            	contentList += '<div class="approval-person-photo person-photo" id="'+ content[x].oId +'"><img src="'+ content[x].photo +'"/><p>'+ content[x].name +'</p></div>';
            }
			$('#addKeyApprover').before(contentList);
			$('.fd-appr-type li:nth-child(2)').click();
        },

        renderUI: function() {
            var tpl = '<div class="<%= className %>">';

            this._$elem = $(_.template(tpl)({
                'className': this.get('className')
            }))
        },

        // 绑定外部事件
        _bindHandler: function() {
            this.eachBind({
                'inAear': 'onInAear',
                'leaveAear': 'onLeaveAear',
                'dragStart': 'onDragStart',
                'draging': 'onDraging',
                'dragEnd': 'onDragEnd',
                'selectComp': 'onSelectComp',
                'delComp': 'onDelComp'
            });
        },

        bindUI: function() {
            var me = this;

            this._bindHandler();

            this._$elem.on('mouseenter', '.fd-component', function() {
                    $(this).addClass('hover');
                })
                .on('mouseleave', '.fd-component', function() {
                    $(this).removeClass('hover');
                })
                .on('mousedown', '.fd-component-remove', function() {
                    return false;
                })
                .on('click', '.fd-component-remove', function(e) {
                    me._removeComponent(this);
                });
            $('body').on('click','.cancle',function () {
                $('.q-bg').hide()
            }).on('click','.q-alert-closebtn',function () {
                $('.q-bg').hide()
            })

            // 无限延长长按事件的触发
            mTouch.config({
                'longTapDelay': 1000000000
            });

            // 如果有拖动的目标元素，绑定拖动事件
            if (this.get('dragTarget')) {
                mTouch('body').on('swipestart', this.get('dragTarget'), function(e) {
                    me._swipeStart(e, this, true);
                });
            }

            mTouch('body').on('swipestart', '.fd-component', function(e) {

                    me._swipeStart(e, this, false);
                    me.selectComp(this);
                })
                .on('swiping', function(e) {
                    if (!me.get('_start')) {
                        return;
                    }

                    if (Math.abs(e.mTouchEvent.moveX) > 2 || Math.abs(e.mTouchEvent.moveY) > 2) {
                        me.listenDraging(e);
                    }

                })
                .on('swipeend', function(e) {
                    // 拖动结束
                    me._swipeEnd(e);
                });
        },

        // 删除控件
        _removeComponent: function(elem) {
            var $parent = $(elem).closest('.fd-component'),
                id = $parent.data('id');
            if(this._components[id].selected){
                webUtil.tips('该控件已经作为审批条件，不可删除')
                return
            }
            var self=this

            // if(this.get('componentStatus')=='1'){ //删除已启用模板控件提示隐藏
            //     $('.q-bg').show()
            //     $('.q-bg').find('.confirm').off('click').on('click',function () {
            //         self._remove(elem);
            //         $('.q-bg').hide()
            //     })
            //     return
            // }
            this._remove(elem);
        },
        _remove:function (elem) {
            var $parent = $(elem).closest('.fd-component'),
                currElem = $parent[0],
                id = $parent.data('id');

            if ($parent.hasClass('active')) {
                if ($parent.nextAll('.fd-component').length) {
                    this.selectComp($parent.nextAll('.fd-component')[0])
                } else if ($parent.prevAll('.fd-component').length) {
                    this.selectComp($parent.prevAll('.fd-component')[0])
                } else {
                    var firstComponent = this.find('.fd-component')[0];
                    firstComponent ? this.selectComp(firstComponent) : this.set('currComponent', null);
                }
            }

            $parent.off().remove();
            this.delComponentProps(id);
            this.checkAreaLen()
            this.fire('delComp', this._packageComponent(currElem));
        },
        // 监听控件在区域内拖动
        listenDraging: function(e) {
            var myPosition = $(this.get('target'))[0].getBoundingClientRect(),
                mtouch = e.mTouchEvent,
                drag
            this._dragStart(e);
            drag={
                top:myPosition.top+88,//undrag区域高度 未计算直接写的高度
                left:myPosition.left,
                width:myPosition.width,
                height:myPosition.height-88
            }
            var checkIn=function (touch,clientRect) {
                return  touch.pageX >= clientRect.left && touch.pageX <= clientRect.left + clientRect.width && touch.pageY >= clientRect.top && touch.pageY <= clientRect.top + clientRect.height
            }

            var dragElem = this.get('_dragElem').elem;
            this.fire('draging', e, this._packageComponent(dragElem));

            // 判断元素是否拖动进入设计框区域
            if (checkIn(mtouch,drag)) {
               this._inAear(e);
            } else {
                this._leaveAear(e);
            }
        },

        // 进入区域
        _inAear: function(e) {
            if (!this.get('_isInAear')) {
                this.set('_isInAear', true);
                this.fire('inAear');
                this._showOccupyLine(component);
            }

            // 根据拖动坐标获取坐标最近的控件节点
            var component = this._getComponentByPos(e.clientX, e.clientY);

            if (this.get('_component') === component) {
                return;
            }

            this._showOccupyLine(component);
            this.set('_component', component);
        },

        // 离开区域
        _leaveAear: function(e) {
            if (this.get('_isInAear')) {
                this.set('_isInAear', false);
                this._$elem.removeClass('dragin');
                this._hideOccupyLine();
                this.fire('leaveAear');
            }

            this.set('_component', null);
        },

        // 移动开始
        _dragStart: function(e) {
            if (!this.get('_dragStart')) {
                var dragElemObj = this.get('_dragElem');

                this.set('_dragStart', true);
                this.fire('dragStart', e, this._packageComponent(dragElemObj.elem));

                this._$elem.addClass('draging');
                if (dragElemObj.isNew === false) {
                    $(dragElemObj.elem).addClass('draging');
                }
            }
        },

        // 按钮鼠标开始
        _swipeStart: function(e, elem, isNew) {
            // 不是鼠标左键
            if($(elem).hasClass('disdrag')){//namefield不能拖动
                return
            }
            if (e.button !== 0) {
                return;
            }
            e.preventDefault();
            this.set('_start', true);
            this.set('_dragElem', {
                elem: elem,
                isNew: isNew
            });
        },

        _swipeEnd: function(e) {
            if (!this.get('_start')) {
                return;
            }

            if (this.get('_isInAear')) {
                var dragElemObj = this.get('_dragElem');
                var dragElem = dragElemObj.elem;

                // 需要添加新的控件
                if (dragElemObj.isNew) {
                    var dragElemType = dragElem.getAttribute('data-type');
                    var filefieldLen = $(".fd-designer-inner").find(".fd-compnent-filefield").length;
                    var photofieldLen = $(".fd-designer-inner").find(".fd-compnent-photofield").length;
                    if(filefieldLen > 0 && dragElemType == 'filefield'){
                    	webUtil.tips('只能设置一个文件上传控件');
                    	this.fire('dragEnd', e);
                    	this._hideOccupyLine();
                    	return;
                    }
                    if(photofieldLen > 0 && dragElemType == 'photofield'){
                    	webUtil.tips('只能设置一个图片上传控件');
                    	this.fire('dragEnd', e);
                    	this._hideOccupyLine();
                    	return;
                    }
                    dragElem = this._tplComponent$(dragElemType)[0];
                }

                var component = this.get('_component');

                if (component === null) {
                    this._$elem.append(dragElem);
                } else {
                    $(component).before(dragElem);
                }

                this.selectComp(dragElem);
                this.checkAreaLen()
                this._hideOccupyLine();
            }

            this._$elem.removeClass('draging');
            $(this.get('currComponent').srcElem).removeClass('draging');

            this.resetStatus();

            this.fire('dragEnd', e);
        },

        resetStatus: function() {
            this.set('_start', false);
            this.set('_dragElem', null);
            this.set('_dragStart', false);
            this.set('_component', null);
            this.set('_isInAear', false);
        },

        // 选中控件
        selectComp: function(currElem) {
            this.find('.active').removeClass('active');
            $(currElem).addClass('active');

            var currComponent = this._packageComponent(currElem);
            this.set('currComponent', currComponent);
            this.fire('selectComp', currComponent);
        },

        // 显示占位线
        _showOccupyLine: function(component) {
            var occupyLine = this.get('occupyLine');
            occupyLine.style.display = 'block';

            if (!component) {
                this._$elem.append(occupyLine);
            } else {
                component.parentNode.insertBefore(occupyLine, component);
            }
        },

        // 隐藏占位线
        _hideOccupyLine: function() {
            this.get('occupyLine').style.display = 'none';
        },

        // 根据拖动坐标获取坐标最近的控件节点
        _getComponentByPos: function(x, y) {
            var components = this._$elem.children('.fd-component');

            if (!components.length) {
                return null;
            }

            var firstPos = components[0].getBoundingClientRect();

            for (var i = 0, len = components.length; i < len; i++) {
                // 第一个
                if (y <= (firstPos.top + firstPos.height / 2)) {
                    return components[0];
                }

                // 最后一个
                if (i == len - 1) {
                    return null;
                }

                var pos = components[i].getBoundingClientRect(),
                    nextPos = components[i + 1].getBoundingClientRect();

                if (y >= (pos.top + pos.height / 2) && y <= (nextPos.top + nextPos.height / 2)) {
                    return components[i + 1];
                }
            }
        },

        // 拼装控件html
        _tplComponent$: function(componentType, props) {
            if (Designer[componentType]) {
                var props = $.extend(true, {}, props || Designer[componentType].props);
                var $component = $(Designer.getWidgetTpl(props));
                var id = webUtil.uuid(8, 32);

                if (!props.id) {
                    /*id = props.id.replace(props.type + '-', '');
                } else {*/
                    props.id = id = props.type + '-' + id;
                }

                //添加不可删除属性
                // if(props){
                //     console.log(props)
                // }
                $component.data('id', id);
                this.addComponentProps(id, props);
            }

            return $component;
        },

        _packageComponent: function(elem) {
            var $elem = $(elem);

            return {
                srcElem: elem,
                props: this.getComponentProps($elem.data('id')),
                isActive: $elem.hasClass('active')
            }
        },

        addComponentProps: function(id, props) {
            if (!this._components) {
                this._components = {};
            }

            this._components[id] = props;
        },

        getComponentProps: function(id) {
            return (id && this._components[id]);
        },

        delComponentProps: function(id) {
            delete this._components[id];
        },

        getAllComponents: function() {
            var $components = this.find('.fd-component');
            var arr = [],
                me = this;

            $components.each(function() {
                arr.push(me._packageComponent(this));
            });

            return arr;
        }
    });

    return FormArea;
});