/**
 * 组织架构树组件
 * 依赖jQuery、webUtil.js、widget.js、underscre.js
 * @author muqin_deng@kingdee.com
 * @time 2015/10/19
 */
;(function (factory) {
    factory = factory(jQuery, webUtil, F.d.Widget, _);

    webUtil.namespace('F.d.OrgTree', factory);
    
})(function ($, Util, Widget, _) {

    function OrgTree (cfg) {
        Widget.call(this);
        return this.render(cfg);
    }

    OrgTree.prototype = $.extend({}, Widget.prototype, {
        constructor: OrgTree,

        initCfg: function (cfg) {
            this.cfg = $.extend({
                'url': '/im/web/treeOrg.do',
                'eid': '',
                'orgId': '',
                'needSelectDept': false,
                'needPerson': true,
                'multiSelect': false    // 是否多选
            }, cfg);
        },
        //生成dom节点
        renderUI: function () {
            var t = new Util.Template();

            t._('<ul class="org-tree"></ul>');

            this._$elem = $(t.toString());
        },

        //绑定事件
        bindUI: function () {
            var me = this;

            // 点击非叶子节点事件
            this._$elem.on('click', '.orgtree-node-name', function (e) {    
                e.stopPropagation();
                me._onTreeNodeClick(e, this);
            })
            .on('click', '.orgtree-node-name label', function (e) {
                e.stopPropagation();
            })
            .on('change', '.orgtree-leaf input[type=checkbox]', function (e) {
                e.stopPropagation();
                me._onLeafChange(e, this);
            })
            .on('change', '.orgtree-node-name input[type=checkbox]', function (e) {
                e.stopPropagation();
                me._onTreeNodeChange(e, this);
            });

            // 绑定自定义事件
            this.eachBind({
                'expandNode': 'onExpandNode',
                'collNode': 'onCollNode',
                'selectPerson': 'onSelectPerson'
            });
        },

        //同步设置初始样式
        syncUI: function () {
            if (this.cfg.cls) {
                this._$elem.addClass(this.cfg.cls);
            }

        },

        ready: function () {
            this._tplTree(true, this.get('orgId'));

            this.fire('ready');
        },

        _onLeafChange: function (e, elem) {
            var $elem = $(elem);
            var checked = $elem.prop('checked');
            var $leaf = $elem.closest('.orgtree-leaf');
            var $parentCheckbox = $leaf.closest('.orgtree-node')
                                       .find('.orgtree-node-name input[type="checkbox"]');

            // 不支持多选
            if (!this.get('multiSelect')) {
                if (checked) {
                    this.find('.orgtree-leaf input[type="checkbox"]')
                        .not(elem)
                        .prop('checked', false);
                }
            // 支持多选
            } else {
                // 选中
                if (checked && $parentCheckbox.length && !$parentCheckbox.prop('checked')) {
                    var $leafs = $leaf.parent().find('input[type="checkbox"]');
                    var $checkedLeafs = $leafs.filter(':checked');
                    if ($leafs.length == $checkedLeafs.length) {
                        $parentCheckbox.prop('checked', true);
                    }
                // 取消选中
                } else if (!checked && $parentCheckbox.length && $parentCheckbox.prop('checked')) {
                    $parentCheckbox.prop('checked', false);
                }
            }

            // 触发选中事件
            if (checked) {
                this.fire('selectPerson', this.getPersonInfo($leaf));
            } else {
                this.fire('unselectPerson', this.getPersonInfo($leaf));
            }
            
        },

        // 非叶子结点复选框change事件
        _onTreeNodeChange: function (e, elem) {
            var $elem = $(elem);
            var persons = $elem.data('info');

            if (!persons || !persons.length) {return ;}

            var checked = $elem.prop('checked');
            var $checkboxs = $elem.closest('.orgtree-node')
                                  .find('.orgtree-leaf input[type=checkbox]');

            var i = 0, len = persons.length;
            if (checked) {
                $checkboxs.prop('checked', true);
                for (; i < len; i++) {
                    this.fire('selectPerson', persons[i])
                }
            } else {
                $checkboxs.prop('checked', false);
                for (; i < len; i++) {
                    this.fire('unselectPerson', persons[i]);
                }
            }
        },

        // 点击节点，加载下一级节点
        _onTreeNodeClick: function (e, elem) {
            var $node = $(elem).closest('.orgtree-node');
            var checked = $node.hasClass('selected');
            var needSelectDept = this.get('needSelectDept');

            // if ($node.hasClass('orgtree-root')) {
            //     return ;
            // }          

            var id = $node.data('id');
            var name = $node.children('.orgtree-node-name').attr('title');
            var level = $node.data('level');

            if (!$node.data('loaded')) {
                this._tplTree(false, id, $node, level);
            }

            // 需要选择部门
            if (needSelectDept) {
                // 是否多选
                if (this.get('multiSelect')) {
                    $node.toggleClass('selected');
                } else {
                    if (checked) {
                        $node.removeClass('selected');
                    } else {
                        this.find('.orgtree-node.selected').removeClass('selected');
                        $node.addClass('selected');
                    }
                }

                // 折叠
                if ($node.hasClass('orgtree-node-expand') && checked) {
                    $node.removeClass('orgtree-node-expand');
                    this.fire('collNode', id, name);
                } else if (!$node.hasClass('orgtree-node-expand') && !checked) {
                    $node.addClass('orgtree-node-expand');
                    this.fire('expandNode', id, name);
                }

            // 不需要选择部门
            } else {
                if ($node.hasClass('orgtree-node-expand')) {
                    $node.removeClass('orgtree-node-expand');
                    this.fire('collNode', id, name);
                } else {
                    $node.addClass('orgtree-node-expand');
                    this.fire('expandNode', id, name);
                }
            }

        },

        _tplTreeRoot: function (data) {
            var t = new Util.Template();

            t._('<li class="orgtree-node orgtree-root" data-id="<%= id %>" data-level="0">')
                ._('<span class="orgtree-node-name ellipsis" title="<%= name %>"><%= name %></span>')
            ._('</li>');

            return _.template(t.toString())(data);
        },

        _tplTreeWrap: function (node) {
            var t = new Util.Template();
            var $wrap;

            t._('<ul class="orgtree-nodelist">')
                ._('<p class="orgtree-loading">')
                    ._('<img src="/workreport/web/images/loading.gif" />')
                ._('</p>')
             ._('</ul>');

            $wrap = $(t.toString());
            $(node).data('loaded', true).append($wrap);

            return $wrap;
        },

        // 生成子树
        _tplTree: function (isRoot, orgId, node, level) {
            var me = this;
            var $treeWrap;
            level = level || 0;

            if (isRoot) {
                me._$elem.append('<p class="orgtree-loading"><img src="/workreport/web/images/loading.gif" /></p>');
            } else {
                $treeWrap = me._tplTreeWrap(node);
            }

            return this.requestSubTree(orgId, isRoot)
                .then(function (resp) {
                    resp.person = resp.person || [];
                    resp.children = resp.children || [];
                    resp._level = level + 1;

                    if (isRoot) {
                        // 创建树根节点
                        node = $(me._tplTreeRoot(resp));
                        // 插入树根节点
                        me._$elem.html(node);
                        // 树根节点插入子节点wrap
                        $treeWrap = me._tplTreeWrap(node);
                    }

                    // 插入子节点列表
                    $treeWrap.html(me._tplItems(resp));

                    // 支持多选且没有子部门，只有人员
                    if (me.get('multiSelect') 
                        && me.get('needPerson') 
                        && !resp.children.length 
                        && resp.person.length
                    ) {
                        var $checker = $('<label>'+
                                            '<input type="checkbox" class="fd-checkbox"/>'+
                                            '<span class="fd-checkbox-face"></span>'+
                                        '</label>');
                        $treeWrap.parent()
                                 .find('.orgtree-node-icon')
                                 .after($checker);

                        var arr = [];
                        var person = resp.person;
                        for (var i = 0, len = person.length; i < len; i++) {
                            var item = person[i];
                            arr.push({
                                id: item.id,
                                userId: item.wbUserId,
                                name: item.name,
                                photoUrl: item.photoUrl || '/space/c/photo/load?id='
                            });
                        }
                        $checker.children('input').data('info', arr);
                    // 没有人员且没有子部门
                    } else if (!resp.person.length && !resp.children.length) {
                        node.addClass('no-children');
                    }

                }, function (errMsg) {
                    $(node).data('loaded', false);
                    // alert(errMsg);
                });
        },

        _tplItems: function (resp) {
            var t = new Util.Template();

            if (this.get('needPerson')) {
                // 人员部分
                t._('<% _.each(person, function (item) { %>')
                    ._('<li class="orgtree-leaf" data-userid="<%= item.wbUserId %>" data-id="<%= item.id %>">')
                        ._('<label class="fd-checkbox-label ellipsis">')
                            ._('<% for(var i = 0; i < _level; i++){ %>')
                                ._('<b class="orgtree-space"></b>')
                            ._('<% } %>')
                            ._('<input type="checkbox" class="fd-checkbox"/>')
                            ._('<span class="fd-checkbox-face"></span>')
                            ._('<img src="<%= item.photoUrl || "/space/c/photo/load?id="%>" />')
                            ._('<span class="orgtree-person-name" title="<%= item.name %>"><%= item.name %></span>')
                        ._('</label>')
                    ._('</li>')
                ._('<% }); %>');
            }
            
            // 部门部分
            t._('<% _.each(children, function (item) { %>')
                ._('<li class="orgtree-node" data-id="<%= item.id %>" data-level="<%= _level %>">')
                    ._('<span  class="orgtree-node-name ellipsis" title="<%= item.name %>">')
                        ._('<% for(var i = 0; i < _level; i++){ %>')
                            ._('<b class="orgtree-space"></b>')
                        ._('<% } %>')
                        ._('<span class="orgtree-node-icon"></span>')
                        ._('<%= item.name %>')
                    ._('</span>')
                ._('</li>')
            ._('<% }); %>')
            ._('<% if (!person.length && !children.length && needPerson) { %>')
                ._('<li class="orgtree-empty">没有数据！</li>')
            ._('<% } %>');

            return _.template(t.toString())($.extend({
                'needPerson': this.get('needPerson')
            }, resp));
        },

        // 包装请求成功后返回的数据格式
        _tranformData: function (resp) {
            return resp.data;
        },

        // 包装请求的数据格式
        _postData: function (orgId, isRoot, begin, count) {
            var data = {
                eid: this.get('eid'),
                orgId: orgId || '',
                begin: begin || 0,
                count: count || 10000
            };

            return data;
        },

        requestSubTree: function (orgId, isRoot) {
            var deferred = $.Deferred();
            var data = this._postData(orgId, isRoot);
            var me = this;

            $.ajax({
                'url': this.get('url'),
                'type': 'POST',
                'dataType': 'json',
                'data': data
            }).then(function (resp) {
                if (resp && resp.success === true) {
                    resp = me._tranformData(resp);
                    deferred.resolve(resp);
                } else {
                    deferred.reject((resp && resp.error) || '请求组织架构失败');
                }
            }, function () {
                deferred.reject('请求组织架构失败');
            });

            return deferred.promise();
        },

        getPersonInfo: function ($leaf) {
            var info = {
                'id': $leaf.data('id'),
                'userId': $leaf.data('userid'),
                'name': $leaf.find('.orgtree-person-name').text(),
                'photoUrl': $leaf.find('img').attr('src')
            };

            return info;
        },

        getSelectedPersons: function () {
            var $checkeds = this._$elem.find('.orgtree-leaf input[type="checkbox"]:checked');
            var persons = [], me = this;

            $checkeds.each(function (ind, item) {
                var $leaf = $(this).closest('.orgtree-leaf');
                persons.push(me.getPersonInfo($leaf));
            });

            return persons;
        },

        getSelectedDepts: function () {
            var $selected = this.find('.orgtree-node.selected');
            var depts = [];

            $selected.each(function (i, item) {
                var $item = $(item);
                var deptId = $item.data('id');
                var deptName = $item.children('.orgtree-node-name').attr('title');
                depts.push({
                    'id': deptId,
                    'name': deptName
                });
            });

            return depts;
        }

    });

    return OrgTree;
});
