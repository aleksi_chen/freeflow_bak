/*
 * 分页组件
 * 依赖jQuery
 * @author muqin_deng
 * @time 2015/03/29
 */
;(function(factory) {
    factory = factory(window, document, jQuery, mTouch);

    webUtil.namespace('F.d.Paginator', factory);

})(function(window, doc, $, mTouch) {
    var Widget = F.d.Widget;

    //主函数
    function Paginator(settings) {
        Widget.call(this);

        return this.init(settings);
    }

    /*Paginator.prototype = $.extend({}, Widget.prototype, {
        constructor: Paginator,

        initCfg: function (settings) {
            this.cfg = {
                'target': 'body',
                'className': '',
                'pageSize': 10,     //一页多少条数据
                'middleNum': 5,     //中间显示的页码数
                'edgeNum': 1,       //左右两边要显示的页码数
                'itemstotal': 0,    //数据总条数
                'currentPage': 1,   //当前页
                'ellipsis': '...',  //省略内容
                'onSelectPage': null    //选中页回调
            };

            $.extend(this.cfg, settings);

            this.cfg.pageTotal = this._getPageTotal();
        },

        init: function (settings) {         
            this.initCfg(settings);

            //只有一页不显示分页
            if (this.get('pageTotal') <= 1) {
                return ;
            }

            this.renderUI();
            this.bindUI();
            this.syncUI();
            
            this.fire('ready');

            return this;
        },

        renderUI: function () {
            this._$elem = $('<ul></ul>');

            var html = this._tpl(this.get('currentPage'), this.get('pageTotal'));

            this._$elem.html(html);

            $(this.get('target')).html(this._$elem);
        },

        _tpl: function (currentPage, pageTotal) {
            var html = '';
            if(pageTotal > 1){
                //上一页
                html += '<li' + (currentPage == 1 ? ' class="disabled"' : '') + '>'
                     +      '<a href="#" class="pre">上一页</a>'
                     +  '</li>';

                var displayPages = this._getDisplayPages(), i, len = displayPages.length;

                for (i = 0; i < len; i++) {
                    displayPage = displayPages[i];

                    //省略部分
                    if (displayPage == '.') {
                        html += '<li class="disabled"><a href="#">' + this.get('ellipsis') + '</a></li>';
                    //页码部分
                    } else {
                        html += '<li' + (currentPage == displayPage ? ' class="active"' : '') + '>'
                             +      '<a href="#" data-page="' + displayPage + '">' + displayPage + '</a>'
                             +  '</li>';
                    }
                }

                //下一页
                html += '<li' + (currentPage == pageTotal ? ' class="disabled"' : '') + '>'
                     +      '<a href="#" class="next"><span>下一页</span></a>'
                     +  '</li>';

            }
                
            return html;
        },

        bindUI: function () {
            var me = this;

            this._$elem.off('click').on('click', 'li a', function (e) {
                var $this =  $(this),
                    $li = $this.closest('li'),
                    currentPage =  me.get('currentPage'),
                    dataPage = $this.data('page');

                e.preventDefault();

                if ($li.hasClass('disabled') || $li.hasClass('active')) {
                    return ;
                }

                if ($this.hasClass('pre')) {
                    currentPage -= 1;
                } else if ($this.hasClass('next')) {
                    currentPage += 1;
                } else if (dataPage) {
                    currentPage = dataPage;
                }
                me.set('currentPage', currentPage);

                //重新生成页码栏
                me._reDraw(currentPage, me.get('pageTotal'));

                me.fire('selectPage', currentPage);
            });

            //绑定选中页码回调
            this.eachBind({
                'selectPage': 'onSelectPage',
                'ready': 'onReady'
            });
            
        },

        syncUI: function () {
            this._$elem.addClass(this.get('className'));
        },

        _reDraw: function (currPage, totalPage) {
            this._$elem.html(this._tpl(currPage, totalPage));
        },

        resetPageTotal: function(totalPage){
            this.cfg.pageTotal = Math.ceil(totalPage / this.get('pageSize'));;
        },

        //获取左边页码数
        _getLeftEdge: function () {
            return this.get('edgeNum');
        },

        //获取中间页码数
        _getMiddles: function () {
            var curr = this.get('currentPage'),
                middleNum = this.get('middleNum'),
                pageTotal = this.get('pageTotal'),
                begin = curr - Math.floor((middleNum - 1) /2),
                end;

            if (begin < 1) {begin = 1;}

            end = begin + middleNum - 1;

            if (end > pageTotal) {end = pageTotal;}

            return [begin, end];
        },

        //获取右边开始页码
        _getRightEdge: function () {
            return this.get('pageTotal') - this.get('edgeNum') + 1;
        },

        //获取需要展示的页码
        _getDisplayPages: function () {
            var edgeNum = this.get('edgeNum'), middleNum = this.get('middleNum'),
                pageTotal = this.get('pageTotal'), i, pages = [];

            //总页码小于设定要展示的页码（两边 + 中间页码）
            if (middleNum + edgeNum * 2 >= pageTotal) {
                for (i = 1; i <= pageTotal; i++ ) {
                    pages.push(i);
                }
                return pages;
            }

            var left = this._getLeftEdge(), right = this._getRightEdge(),
                middles = this._getMiddles(), begin;
            //存入左边页码
            for (i = 1; i <= left; i++) {
                pages.push(i);
            }

            //判断中间开始页码是否比左边页码大
            if (middles[0] > left + 1) {
                pages.push('.');    //省略标记
                begin = middles[0];
            } else {
                begin = left + 1;
            }
            //存入中间页码
            for (i = begin; i <= middles[1]; i++) {
                pages.push(i);
            }

            //判断右边开始页码是否比中间结束页码大
            if (right > middles[1] + 1) {
                pages.push('.');
                begin = right;
            } else {
                begin = middles[1] + 1;
            }
            //存入右边页码
            for (i = begin; i <= pageTotal; i++) {
                pages.push(i);
            }

            return pages;
        },

        //计算页码总数
        _getPageTotal: function () {
            return Math.ceil(this.get('itemsTotal') / this.get('pageSize'));
        },
        
        getCurrentPage: function () {
            return this.get('currentPage');
        }
        
    });*/
Paginator.prototype = $.extend({}, Widget.prototype, {
        constructor: Paginator,

        initCfg: function (settings) {
            this.cfg = {
                'target': 'body',
                'className': '',
                'pageSize': 10,     //一页多少条数据
                'middleNum': 5,     //中间显示的页码数
                'edgeNum': 1,       //左右两边要显示的页码数
                'itemstotal': 0,    //数据总条数
                'currentPage': 1,   //当前页
                'ellipsis': '...',  //省略内容
                'onSelectPage': null    //选中页回调
            };

            $.extend(this.cfg, settings);

            this.cfg.pageTotal = this._getPageTotal();
        },

        init: function (settings) {         
            this.initCfg(settings);

            //只有一页不显示分页
            if (this.get('pageTotal') <= 1) {
                $(this.get('target')).html('');
                return ;
            }

            this.renderUI();
            this.bindUI();
            this.syncUI();
            
            this.fire('ready');

            return this;
        },

        renderUI: function () {
            this._$elem = $('<ul></ul>');

            var html = this._tpl();

            this._$elem.append(html);

            $(this.get('target')).html(this._$elem);
        },

        _tpl: function () {
            var currentPage = this.get('currentPage'),
                pageTotal = this.get('pageTotal'),
                html = '';

            //上一页
            html += '<li' + (currentPage == 1 ? ' class="disabled"' : '') + '>'
                 +      '<a href="#" class="pre">上一页</a>'
                 +  '</li>';

            var displayPages = this._getDisplayPages(), i, len = displayPages.length;

            for (i = 0; i < len; i++) {
                displayPage = displayPages[i];

                //省略部分
                if (displayPage == '.') {
                    html += '<li class="disabled"><a href="#">' + this.get('ellipsis') + '</a></li>';
                //页码部分
                } else {
                    html += '<li' + (currentPage == displayPage ? ' class="active"' : '') + '>'
                         +      '<a href="#" data-page="' + displayPage + '">' + displayPage + '</a>'
                         +  '</li>';
                }
            }

            //下一页
            html += '<li' + (currentPage == pageTotal ? ' class="disabled"' : '') + '>'
                 +      '<a href="#" class="next"><span>下一页</span></a>'
                 +  '</li>';

            return html;
        },

        bindUI: function () {
            var me = this;

            this._$elem.off('click').on('click', 'li a', function (e) {
                var $this =  $(this),
                    $li = $this.closest('li'),
                    currentPage =  me.get('currentPage'),
                    dataPage = $this.data('page');

                e.preventDefault();

                if ($li.hasClass('disabled') || $li.hasClass('active')) {
                    return ;
                }

                if ($this.hasClass('pre')) {
                    currentPage -= 1;
                } else if ($this.hasClass('next')) {
                    currentPage += 1;
                } else if (dataPage) {
                    currentPage = dataPage;
                }

                me.set('currentPage', currentPage);

                //重新生成页码栏
                me._reDraw();

                me.fire('selectPage', currentPage);
            });

            //绑定选中页码回调
            this.eachBind({
                'selectPage': 'onSelectPage',
                'ready': 'onReady'
            });
            
        },

        syncUI: function () {
            this._$elem.addClass(this.get('className'));
        },

        _reDraw: function () {
            this._$elem.html(this._tpl());
        },

        //获取左边页码数
        _getLeftEdge: function () {
            return this.get('edgeNum');
        },

        //获取中间页码数
        _getMiddles: function () {
            var curr = this.get('currentPage'),
                middleNum = this.get('middleNum'),
                pageTotal = this.get('pageTotal'),
                begin = curr - Math.floor((middleNum - 1) /2),
                end;

            if (begin < 1) {begin = 1;}

            end = begin + middleNum - 1;

            if (end > pageTotal) {end = pageTotal;}

            return [begin, end];
        },

        //获取右边开始页码
        _getRightEdge: function () {
            return this.get('pageTotal') - this.get('edgeNum') + 1;
        },

        //获取需要展示的页码
        _getDisplayPages: function () {
            var edgeNum = this.get('edgeNum'), middleNum = this.get('middleNum'),
                pageTotal = this.get('pageTotal'), i, pages = [];

            //总页码小于设定要展示的页码（两边 + 中间页码）
            if (middleNum + edgeNum * 2 >= pageTotal) {
                for (i = 1; i <= pageTotal; i++ ) {
                    pages.push(i);
                }
                return pages;
            }

            var left = this._getLeftEdge(), right = this._getRightEdge(),
                middles = this._getMiddles(), begin;
            //存入左边页码
            for (i = 1; i <= left; i++) {
                pages.push(i);
            }

            //判断中间开始页码是否比左边页码大
            if (middles[0] > left + 1) {
                pages.push('.');    //省略标记
                begin = middles[0];
            } else {
                begin = left + 1;
            }
            //存入中间页码
            for (i = begin; i <= middles[1]; i++) {
                pages.push(i);
            }

            //判断右边开始页码是否比中间结束页码大
            if (right > middles[1] + 1) {
                pages.push('.');
                begin = right;
            } else {
                begin = middles[1] + 1;
            }
            //存入右边页码
            for (i = begin; i <= pageTotal; i++) {
                pages.push(i);
            }

            return pages;
        },

        //计算页码总数
        _getPageTotal: function () {
            return Math.ceil(this.get('itemsTotal') / this.get('pageSize'));
        },
        
        getCurrentPage: function () {
            return this.get('currentPage');
        }
        
    });


    return Paginator;
    
});