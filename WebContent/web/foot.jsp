<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%--debug模式 --%>
<c:if test="${debugMode}">
    <script type="text/javascript" src="/${projectPath }/js/core/underscore-min.js"></script>
    <script type="text/javascript" src="/${projectPath }/js/core/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/${projectPath }/js/core/mTouch.js?${timestamp}"></script>
    <script type="text/javascript" src="/${projectPath }/web/js/web-util.js?${timestamp}"></script>
    <script type="text/javascript" src="/${projectPath }/web/js/widget.js?${timestamp}"></script>
    <script type="text/javascript" src="/${projectPath }/web/js/paginator.js?${timestamp}"></script>
    <script type="text/javascript" src="/${projectPath }/web/My97DatePicker/WdatePicker.js?${timestamp}"></script>
    
    
	<script type="text/javascript" src="/${projectPath }/web/js/autocomplete.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/org-tree.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/select-approver.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/designer.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/form-area.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/form-controller.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/approval-manage.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/person-select.js?${timestamp}"></script>
</c:if>

<%--非debug模式 --%>
<c:if test="${!debugMode}">
<!--
    <script type="text/javascript" src="/${projectPath }/js/core/underscore-min.js"></script>
    <script type="text/javascript" src="/${projectPath }/js/core/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/${projectPath }/dist/js/base-min.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/dist/js/web-all-min.js?${timestamp}"></script>
    <script type="text/javascript" src="/${projectPath }/web/My97DatePicker/WdatePicker.js?${timestamp}"></script>
-->
    <script type="text/javascript" src="/${projectPath }/js/core/underscore-min.js"></script>
    <script type="text/javascript" src="/${projectPath }/js/core/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/${projectPath }/js/core/mTouch.js?${timestamp}"></script>
    <script type="text/javascript" src="/${projectPath }/web/js/web-util.js?${timestamp}"></script>
    <script type="text/javascript" src="/${projectPath }/web/js/widget.js?${timestamp}"></script>
    <script type="text/javascript" src="/${projectPath }/web/js/paginator.js?${timestamp}"></script>
    <script type="text/javascript" src="/${projectPath }/web/My97DatePicker/WdatePicker.js?${timestamp}"></script>
    
    
	<script type="text/javascript" src="/${projectPath }/web/js/autocomplete.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/org-tree.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/select-approver.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/designer.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/form-area.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/form-controller.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/approval-manage.js?${timestamp}"></script>
	<script type="text/javascript" src="/${projectPath }/web/js/person-select.js?${timestamp}"></script>
</c:if>