<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="云之家,审批,审批自定义,申请,移动办公" />
    <title>审批管理</title>
    <script>
		__global = {
			eid: '${user.eid}',
			orgId: '${user.deptId}',
			networkName: '${networkName}'
		}
    </script>
    <%@ include file="head.jsp"%>
</head>
<body>
	<!-------- 云之家头部  ------>
	<div class="job-log-report-header">
		<div class="job-log-report-header2">
			<a href="/im/xiaoxi" class="job-log-report-logo">
				<img class="fl" src="/res/css/n/skin-default/new_images/new-logo.png">
				<div class="job-log-report-name fl">
					${networkName}<i>—</i>审批
				</div>
			</a>
		</div>
	</div>
	<!-------- 云之家头部  ------>
	<div id="body">
		<div class="am-header">审批管理</div>
		<div class="am-bd">
			<div class="am-nav">
				<button class="am-curr">审批模板配置</button>
				<button>审批数据导出</button>
				<div class="approval-btn fr">
					<span class="approval-data-chaxun">查询</span>
					<div class="btn-blue am-export-btn">导出</div>
				</div>
				<div class="btn-blue am-create-approval">新建审批模版</div>
			</div>
		
			<div class="am-manage-content">
				<table class="am-table" border="1px" >
					<tr class="am-table-header">
						<th>审批模板名称</th>
						<th>状态</th>
						<th>创建人</th>
						<!--<th>创建时间</th>-->
						<th width="260">操作</th>
					</tr>
					<tbody class="am-manage-tbody"></tbody>
				</table>
				
				<div class="am-operate-log">
					<div class="am-log-title">操作日志</div>
					<ul class="am-log-list"></ul>
				</div>
				<!-------------  分页      -------------->
				<div class="approval-template-pagiantion"></div>
			</div>
		
			<div class="am-export-content">
				<div class="am-input-group">
				    <div class="am-input-row clearfix">
				    	<span class="am-department fl">
							<label for="">部门</label>
							<input type="text" readonly placeholder="选择部门"/>
							<b>×</b>
						</span>
						<span class="am-promoter fl">
							<label for="">发起人</label>
							<input type="text" placeholder="请输入姓名、拼音"/>
							<div class="font12 job-log-report-serchk">
								<div><span></span></div>
								<ul></ul>
							</div>
						</span>
						<span class="am-approve-type fl">
							<label for="">类型</label>
							<select name="" id="">
							</select>
						</span>
						<span class="am-approve-date fl">
							<label for="d12">发起时间</label>
							<input  type="text" id="d12" onclick="WdatePicker({el:'d12',isShowClear:true,highLineWeekDay:false})" readonly />
							<label for="d13">至</label>
							<input type="text" id="d13" onclick="WdatePicker({el:'d13',isShowClear:true,highLineWeekDay:false})" readonly />
						</span>
						<span class="am-approve-number fl">
							<label for="">单号</label>
							<input type="text">
						</span>
					</div>
					<table class="am-table" border="1px">
						<tbody>
						<tr class="am-table-header">
							<th width="11%">单号</th>
							<th width="8%">类型</th>
							<th width="12%">部门</th>
							<th width="6%">发起人</th>
							<th width="15%">发起时间</th>
							<th width="18%">审批人</th>
							<th width="15%">完成时间</th>
							<th width="15%">操作</th>
						</tr>
						</tbody>
						<tbody class="am-export-tbody">
							<!-- <tr> 
								<td>2015091901</td>
								<td class="am-skyblue">请假</td>
								<td>开发部</td>
								<td>周恒颖</td>
								<td>2015-09-11 09:11</td>
								<td class="am-approver">周恒颖 > 周周 > 张三</td>
								<td>2015-09-13 15:00</td>
								<td>
									<a class="am-blue view-details-btn" href="javascript:;">查看单据</a>
									<a class="am-blue print-details-btn" href="javascript:;">打印</a>
								</td>
							</tr>
							<tr>
								<td>2015091901</td>
								<td class="am-skyblue">请假</td>
								<td>开发部</td>
								<td>周恒颖</td>
								<td>2015-09-11 09:11</td>
								<td class="am-approver">周恒颖 > 周周 > 张三 > 赵小青</td>
								<td>2015-09-13 15:00</td>
								<td>
									<a class="am-blue view-details-btn" href="javascript:;">查看单据</a>
									<a class="am-blue print-details-btn" href="javascript:;">打印</a>
								</td>
							</tr> -->
						</tbody>
					</table>
				</div>


				<!-------------  分页      -------------->
				<div class="approval-data-export-pagiantion"></div>
			</div>
		
		</div>
		<div class="am-overlay fadein"></div>
		<div class="am-popbox zoomin">
			<div class="am-popbox-header">
				<span>创建新审批</span>
				<span class="am-popbox-close">×</span>
			</div>
			<!--<a class="creatNew" href="javascript:;">+ 从审批设计器创建</a>-->
			<!--<h3>公共模板</h3>-->
			<ul class="am-popbox-list">
				<li class="am-popbox-item am-blank">
					<p>空白新模板</p>
				</li>
			</ul>
		</div>
	
		<!-------------  选择部门      -------------->
		<div class="selector-department">
			<h2>
				<span class="fl">选择部门</span>
				<a href="javascript:;" class="fr selector-department-guanbi"></a>
			</h2>
			<div class="selector-department-children">
				<label><input type="checkbox"/>包含下级部门</label>
			</div>
			<div class="fd-selectappr-org"></div>
			<div class="selector-department-bottom">
				<a href="javascript:;" class="selector-department-true">确认</a>
				<a href="javascript:;" class="selector-department-quxiao">取消</a>
			</div>
			<div class="selector-department-tips">
				<b></b><span>请选择相应部门！</span>
			</div>
		</div>
		
		<!-------------  查看单据  -------------->
		<div class="view-details-frame">
			<div class="view-details-frame-header">
				审批
				<span class="view-details-cancel"></span>
			</div>
			<iframe src="" class="view-details-frame-content"></iframe>
			<div class="view-details-frame-footer"></div>
		</div>

		<!-------------  预览打印  -------------->

		<!-- 遮罩层 -->
		<div class="view-print-frame-mask"></div>
		<div class="view-print-frame">
			<div class="view-print-frame-header">
				审批
				<span class="view-print-cancel"></span>
			</div>
			<div class="view-print-frame-body">
				<div class="view-print-content"></div>
				<div class="view-print-frame-footer">
					<a href="javascript:;" class="quxiao" id="closePrint">取消</a>
					<a href="javascript:;" class="dayin" id="startPrint">打印</a>
				</div>
			</div>
		</div>
		<iframe src="show-pdf.html" id="print-frame"></iframe>
	</div>
	<!-- start 输入审批名称 -->
	<div class="q-dialog" id="approvalNameBox">
		<div class="q-dialog-heard">
		    <span>输入审批名称</span>
		    <span class="q-dialog-closebtn"></span>
		</div>
		<div class="q-dialog-body">
		    <div class="q-form-row">
		        <input type="text" class="q-form-input" placeholder="请填写审批名称，如出差,报销等"/>
		        <div class="q-hint-list"><span class="q-hint-item">审批名称已存在</span></div>
		    </div>
		    <div class="q-dialog-button-group">
		        <a class="q-btn btn-primary fr">下一步</a>
		    </div>
		</div>
	</div>
	<!-- end 输入审批名称 -->

    <%@ include file="foot.jsp"%>
    <script>
        $(function () {
            webUtil.run('F.d.Manage');
        });
    </script>
    <form id="exportForm" method="post"  >
    	<!--  <input type="hidden" name="params" id="params" value=''>-->
    	 <input type="hidden" name="formTemplateId" id="formTemplateId" value=''>
    	 <input type="hidden" name="receiptId" id="receiptId" value=''>
    	 <input type="hidden" name="deptId" id="deptId" value=''>
    	 <input type="hidden" name="userId" id="userId" value=''>
    	 <input type="hidden" name="startTime" id="startTime" value="">
    	 <input type="hidden" name="endTime" id="endTime" value="">
    </form>
</body>
</html>
