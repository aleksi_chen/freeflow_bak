<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="云之家,审批,审批自定义,申请,移动办公" />
    <title>审批管理</title>
    <script>
		__global = {
			eid: '${user.eid}',
			orgId: '${user.deptId}'
		}
    </script>
    <%@ include file="head.jsp"%>
</head>
<body class="fd-bg">
    <div class="approval-rule" id="body">
        <div id="approvalRule"></div>
    </div>
    <%@ include file="foot.jsp"%>
    <script src="/${projectPath }/web/js/lib/requirejs.js?${timestamp}" data-main="/${projectPath }/web/js/common-vue/approval-rule.min.js" ></script>
</body>
</html>