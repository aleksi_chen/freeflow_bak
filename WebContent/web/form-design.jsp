<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html >
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="云之家,审批,审批自定义,申请,移动办公" />
    <title>审批设计器</title>
        <script>
		__global = {
			eid: '${user.eid}',
			orgId: '${user.deptId}'
		}
    </script>
    <%@ include file="head.jsp"%>
</head>
<body class="fd-bg">
	<div id="body">
		<div class="fd-bd">
			<div class="fd-bd-header">
				<div class="fd-header-l fl">
					<a href="javascript:;" class="back-manager"></a>
					<span class="line"></span>
					<div class="fd-design-step step-one fd-design-active">
					   <span class="fd-design-step-icon">1</span>
					   <p class="fd-design-step-text">审批设计器</p>
					</div>
					<span class="fd-design-step-arrow"></span>
					<div class="fd-design-step step-two">
					   <span class="fd-design-step-icon">2</span>
					   <p class="fd-design-step-text">审批规则匹配</p>
					</div>
				</div>
				<div class="fd-header-r fr">
				    <button class="fd-btn fd-btn-primary fd-btn-pre" style="display:none">上一步</button>
                    <button class="fd-btn fd-btn-primary fd-btn-next">下一步</button>
					<!--<button class="fd-btn fd-btn-black fd-btn-preview">预览</button>-->
					<button class="fd-btn fd-btn-primary fd-btn-save" style="display:none">保存并启用</button>
				</div>
			</div>
            <!--上一步-->
			<div class="fd-bd-main">
				<div class="fd-bd-l">
					<div class="fd-widgetpanel">
						<div class="fd-panel-tab">
							<a href="javascript:;" class="fd-tabitem" data-panel="0">审批设置</a>
							<a href="javascript:;" class="fd-tabitem curr" data-panel="1">表单控件</a>
						</div>
						<div class="fd-panel-body">
							<div class="fd-panel-item fd-form-settings clearfix  none" data-pindex="0">
								<div class="fd-formsetting-item fd-formsetting-name">
									<div class="fd-formsetting-itemtitle">审批名称</div>
									<div class="fd-formsetting-itembody">
										<input type="text" class="fd-form-name">
									</div>
								</div>
								<div class="fd-formsetting-item fd-formsetting-approver">
									<div class="fd-formsetting-itemtitle">审批人设置</div>
									<div class="fd-formsetting-itembody">
										<label class="fd-checkbox-label">
											<input type="radio" class="fd-checkbox" name="need-approver" value="0" checked autocomplete="off"/>
											<span class="fd-checkbox-face"></span>
											<span class="fd-label-text">不需要审批人</span>
										</label>
										<label class="fd-checkbox-label">
											<input type="radio" class="fd-checkbox" name="need-approver" value="1" autocomplete="off"/>
											<span class="fd-checkbox-face"></span>
											<span class="fd-label-text">选择审批人</span>
										</label>
									</div>
								</div>
							</div>
							<div class="fd-panel-item clearfix" data-pindex="1">
								<div class="fd-widgetitem fl" data-type="textfield">
									<label>单行文本</label>
									<i class="widgeticon">Aa</i>
								</div>
								<div class="fd-widgetitem fl" data-type="textareafield">
									<label>多行输入框</label>
									<i class="widgeticon">Aa</i>
								</div>
								<div class="fd-widgetitem fl" data-type="numberfield">
									<label>数字输入框</label>
									<i class="widgeticon">123</i>
								</div>
								<div class="fd-widgetitem fl" data-type="moneyfield">
									<label>金额输入框</label>
									<i class="widgeticon">123</i>
								</div>
								<div class="fd-widgetitem fl" data-type="radiofield">
									<label>单选框</label>
									<i class="widgeticon"></i>
								</div>
								<div class="fd-widgetitem fl" data-type="datefield">
									<label>日期选择</label>
									<i class="widgeticon"></i>
								</div>
								<div class="fd-widgetitem fl" data-type="daterangefield">
									<label>日期区间</label>
									<i class="widgeticon"></i>
								</div>
								<div class="fd-widgetitem fl" data-type="photofield">
									<label>图片上传</label>
									<i class="widgeticon"></i>
								</div>
								<div class="fd-widgetitem fl" data-type="filefield">
									<label>文件上传</label>
									<i class="widgeticon"></i>
								</div>
								<div class="fd-widgetitem fl">
									<label>明细</label>
									<i class="widgeticon"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="fd-bd-c">
					<div class="fd-phone">
						<div class="fd-phone-header"></div>
						<div class="fd-phone-footer"></div>
					</div>
					<div class="fd-designer">
					</div>
					<div class="fd-add-field">
						<div class="fd-add-field-icon"></div>
						<div class="fd-add-info">请从左边表单控件拖动控件来创建你的审批</div>
					</div>
				</div>
				<div class="fd-bd-r">
					<div class="fd-settingpanel">
						<span class="fd-settingpanel-title">控件设置</span>
						<div class="fd-panel-body">
							
						</div>
					</div>
				</div>
			</div>
         
			<div class="fd-draging-mask"></div>
			<div class="q-bg">
				<div class="q-alert">
					<div class="q-alert-heard">
						<span class="q-alert-closebtn"></span>
					</div>
					<div class="q-alert-body">
						<h1 class="q-alert-title text-center">确定要删除该控件吗?</h1>
						<p class="q-alert-text text-center">删除后该控件对应的数据也会删除，建议先导出数据备份。 如需保留数据，请尽量沿用原控件</p>
					</div>
					<div class="q-alert-button-group text-center">
						<a class="q-btn btn-default cancle">取消</a>
						<a class="q-btn btn-primary confirm">确定</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="foot.jsp"%>
	<script>
	    $(function () {
	        webUtil.run('F.d.controller');
	    });
	</script>
</body>
</html>