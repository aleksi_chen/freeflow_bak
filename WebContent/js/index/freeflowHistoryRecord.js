var snsDevDB=db.getSisterDB("snsDev");
var ossDevDB=db.getSisterDB("ossDev");
snsDevDB.T_ApprovalRecord.find().forEach(function(item){
	
	 //查询审批人信息
	 var approvalUser="{}";
	 ossDevDB.T_UserNetwork.find({"networkId":item.networkId,"userId":item.approverUserId}).forEach(function(result){
		 //TODO 旧数据部门信息(部门id，部门名称),怎么处理?
		 approvalUser={
				  "userId":result.userId,
				  "networkId":result.networkId,
				  "oid":result.oid,
				  "eid":result.eid,
				  "orgInfoId":result.orgInfoId,
				  "openId":result.openId,
				  "name":result.name
		  };
	 });
	 
	 
	 
	 var formCreator="{}";
	  ossDevDB.T_UserNetwork.find({"networkId":item.networkId,"userId":item.formCreatorUserId}).forEach(function(result){
		//TODO 旧数据部门信息(部门id，部门名称),怎么处理?
		  formCreator={
				  "userId":result.userId,
				  "networkId":result.networkId,
				  "oid":result.oid,
				  "eid":result.eid,
				  "orgInfoId":result.orgInfoId,
				  "openId":result.openId,
				  "name":result.name
		  };
	  });
	  
	//审批申请人的信息
	  
	var approvalApplyRecord={
			"formId":item.formId,
			"formTitle":item.formTitle,
			"comment":item.comment,
			"approvalTime":item.approvalTime,
			"status":"\""+item.approvalStatus+"\"", //TODO 状态不匹配怎么处理?
			"approvalUser":approvalUser,
			"formCreator":formCreator,
			"transferApproverOId":item.transferApproverUserId,
			"createTime":item.createTime
	};
	
	snsDevDB.T_ApprovalApplyRecord.save(approvalApplyRecord);
});