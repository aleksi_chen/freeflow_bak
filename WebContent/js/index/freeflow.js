var snsDevDB=db.getSisterDB("snsDev");
snsDevDB.T_ApprovalApply.ensureIndex({"formTemplateId":1},{"background":1});
snsDevDB.T_ApprovalApply.ensureIndex({"formTemplateId":1,"status":1},{"background":1});
snsDevDB.T_ApprovalApply.ensureIndex({"createUser.oid":1,"createUser.deptId":1},{"background":1});
snsDevDB.T_ApprovalApply.ensureIndex({"receiptId":1},{"background":1});
snsDevDB.T_ApprovalApply.ensureIndex({"formTemplateId":1,"status":1,"receiptId":1,"createTime":1,"createUser.oid":1,"createUser.deptId":1},{"background":1});

snsDevDB.T_ApprovalApplyRecord.ensureIndex({"formId":1},{"background":1});
snsDevDB.T_ApprovalApplyRecord.ensureIndex({"formTemplateId":1},{"background":1});
snsDevDB.T_ApprovalApplyRecord.ensureIndex({"formTemplateId":1,"status":1,"formId":1},{"background":1});

snsDevDB.T_ApprovalDetail.ensureIndex({"templateId":1},{"background":1});
snsDevDB.T_ApprovalDetail.ensureIndex({"approvalId":1},{"background":1});
snsDevDB.T_ApprovalDetail.ensureIndex({"templateId":1,"approvalId":1},{"background":1});

snsDevDB.T_ApprovalRule.ensureIndex({"templateId":1},{"background":1});
snsDevDB.T_ApprovalRule.ensureIndex({"createTime":1},{"background":1});
snsDevDB.T_ApprovalRule.ensureIndex({"approvalType":1,"templateId":1,"createTime":1},{"background":1});

var lightappDB=db.getSisterDB("lightappDB");
lightappDB.T_FormTemplate.ensureIndex({"user.oid":1,"user.networkId":1},{"background":1});
lightappDB.T_FormTemplate.ensureIndex({"user.userId":1,"user.networkId":1},{"background":1});
lightappDB.T_FormTemplate.ensureIndex({"createTime":1,"updateTime":1},{"background":1});
lightappDB.T_FormTemplate.ensureIndex({"title":1,"status":1,"appName":1,"isDelete":1},{"background":1});
lightappDB.T_FormTemplate.ensureIndex({"title":1,"status":1,"user.oid":1,"user.networkId":1,"createTime":1,"updateTime":1,"appName":1,"isDelete":1},{"background":1});
lightappDB.T_FormTemplate.ensureIndex({"title":1,"status":1,"user.userId":1,"user.networkId":1,"createTime":1,"updateTime":1,"appName":1,"isDelete":1},{"background":1})

lightappDB.T_RecordTemplate.ensureIndex({"user.oid":1,"user.networkId":1},{"background":1});
lightappDB.T_RecordTemplate.ensureIndex({"user.userId":1,"user.networkId":1},{"background":1});
lightappDB.T_RecordTemplate.ensureIndex({"saveTime":1},{"background":1});
lightappDB.T_RecordTemplate.ensureIndex({"title":1,"status":1,"appName":1,"isDelete":1},{"background":1});
lightappDB.T_RecordTemplate.ensureIndex({"title":1,"status":1,"user.oid":1,"user.networkId":1,"saveTime":1,"appName":1,"isDelete":1},{"background":1});
lightappDB.T_RecordTemplate.ensureIndex({"title":1,"status":1,"user.userId":1,"user.networkId":1,"saveTime":1,"appName":1,"isDelete":1},{"background":1});

lightappDB.T_OperationLog.ensureIndex({"user.oid":1,"user.networkId":1},{"background":1});
lightappDB.T_OperationLog.ensureIndex({"user.userId":1,"user.networkId":1},{"background":1});
lightappDB.T_OperationLog.ensureIndex({"createTime":1},{"background":1});
lightappDB.T_OperationLog.ensureIndex({"targetId":1,"operation":1,"target":1,"isDelete":1},{"background":1});
lightappDB.T_OperationLog.ensureIndex({"user.oid":1,"user.networkId":1,"createTime":1,"targetId":1,"operation":1,"target":1,"isDelete":1},{"background":1});
lightappDB.T_OperationLog.ensureIndex({"user.userId":1,"user.networkId":1,"createTime":1,"targetId":1,"operation":1,"target":1,"isDelete":1},{"background":1});
