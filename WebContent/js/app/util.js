/* 
 * 通用工具集
 * @author muqin_deng
 * @time 2015/03/16
 */
(function (window, undefined) {
	var util = {
		data: {
			ONE_DATE: 24 * 60 * 60 * 1000
		},
		
		userAgent: navigator.userAgent,
		
		
		//判断是否是今天
		isToday: function (time) {
			var today = new Date();
			
			time = new Date(time);
			
			return today.toLocaleDateString() == time.toLocaleDateString();
		},
		
		isTomorrow: function (time) {
			time = +new Date(time) - this.data.ONE_DATE;
			
			return this.isToday(time);
		},
		
		//根据手机品牌判断是否支持scale动画
		isSupperScale: function () {
			return !/XiaoMi|M\S+\sBuild/ig.test(this.userAgent);
		},
		
		loading: function (show) {
			var $loading = $('.loading');
			
			if (show === false) {
				$loading.hide();
				return ;
			}
			
			$loading.show();
		},
		
        //时间补0
        timeAddZero: function (time) {
        	if (time < 10) {
                return '0' + time;
            } else {
                return '' + time;
            }
        },
		
		//显示消息提示
		showTips : function(tips, time){
			var me = this,
				$userinfoPs = $("#tips-msg"),
				closeTime = time || 1000;
			
			if (tips =='HIDE') {
				$userinfoPs.fadeOut();
				return ;
			}
			
			$userinfoPs.html(tips).fadeIn(function(){
				if (closeTime == 'NOHIDE') {
					return ;
				}
				
				me.delay(closeTime).done(function () {
    				$userinfoPs.fadeOut();
    			});
    		});
 		},
		
		//延时函数
        delay: function (t) {
        	var deferred = $.Deferred();

        	setTimeout(function () {
        		deferred.resolve();
        	}, t);

        	return deferred.promise();
        },
        
        //转义特殊字符<,>,",'
		escapeToHtmlEntity: function (str) {
			if (!str) {
				return '';
			}

			var escape = {
				'<': '&#60;',
				'>': '&#62;',
				'\"': '&#34;',
				'\'': '&#39;'
			};

			return str.replace(/[<>"']/g, function (match) {
				return escape[match] || match;
			});
		},
        
        //元素滚动到可见区域
        scrollIntoView: function (elem, always) {
        	if (always === true) {
        		elem.scrollIntoView();
        		return ;
        	}
        	elem.scrollIntoViewIfNeeded();
        },

        //获取根路径（可含项目路径）
        getRootPath: function (needProjectPath) {
        	var href = window.location.href,			//完整路径
				pathName = window.location.pathname,	//主机后面的路径
				pos = href.indexOf(pathName),
				localhostPath = href.substring(0, pos),	//主机路径
				projectName = /\/[^\/]+\//.exec(pathName)[0];
        	
        	if (needProjectPath) {
        		return (localhostPath + projectName);
        	}
			
        	return localhostPath;
        },        
        
        //判断移动端系统
        checkMobileSystem: function (type) {
        	var regObj = {
	        		'ios': /\(i[^;]+;( U;)? CPU.+Mac OS X/,
	        		'android': /Android/i
	        	};
        	
        	if (!regObj[type]) {
        		return ;
        	}
        	
        	return regObj[type].test(this.userAgent);
        },

        //获取url键值对
        getUrlKeyValObj: function () {
        	var url = window.location.href,
				arr, i, len,
				paramsObj = {};	
        	
        	if (this.urlKeyValObj) {
        		return this.urlKeyValObj;
        	}
	
			arr = url.substring(url.indexOf('?')+1).split('&');
			
			for (i = 0, len = arr.length; i < len; i++) {
				var reg = /(.*)\=(.*)/g,
					match = reg.exec(arr[i]);
	
				if (match && match[1]) {
					paramsObj[decodeURIComponent(match[1])] = decodeURIComponent(match[2]);
				}
			}
			
			this.urlKeyValObj = paramsObj;
			
			return paramsObj;
        },
        
        //获取url的对应参数的值
		getUrlValue: function (param) {
			if (!param) {
				return '';
			}
			
			var paramsObj = this.getUrlKeyValObj();

			if (paramsObj.hasOwnProperty(param)) {
				return paramsObj[param];
			} else {
				return '';
			}
		},
		
		/*
		 * 函数节流
		 * @param {function} method	要进行节流的函数
		 * @param {number} delay 延时时间(ms)
		 * @param {number} duration 经过duration时间(ms)必须执行函数method
		 */
		throttle: function (method, delay, duration) {
			var timer = null,
				begin = null;
            return function () {
                var context = this,
                	args = arguments,
                	current = new Date();
                if (!begin) {
                	begin = current;
                }
                if (timer) {
                	window.clearTimeout(timer);
                }
                if (duration && current - begin >= duration) {
                     method.apply(context,args);
                     begin = null;
                }else {
                    timer = window.setTimeout(function () {
                        method.apply(context, args);
                        begin = null;
                    }, delay);
               }
            };
		},
    	
    	/*
		 * 页面模板引擎解析
		 * @param {string} html 要进行解析的html字符串（在<##>里面的转换成js来执行）
		 * @param {object} options html中解析传入的对象
		 * @return {string} 返回解析后的html字符串
		 */
		templateEngine: function(html, options) {
		    var re = /<#(.*?)#>/g, 
		    	reExp = /(^(\s|\t|\n|\r)*((var|if|for|else|switch|case|break)\b|{|}))(.*)?/g, 
		    	code = 'var r=[];\n', 
		    	cursor = 0;
		    
		    html = html.replace(/\n|\r|\t/g, '');

		    var add = function(line, js) {
		        js? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
		            (code += line !== '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
		        return add;
		    };
		    
		    while(match = re.exec(html)) {
		    	var noJsStr = html.slice(cursor, match.index);
		        add(/[^\s]/g.test(noJsStr) ? noJsStr : '')(match[1], true);
		        cursor = match.index + match[0].length;
		    }
		    add(html.substr(cursor, html.length - cursor));
		    code += 'return r.join("");';
		    return new Function(code.replace(/[\r\t\n]/g, '')).apply(options ? options : {});
		},

        getBytes: function(str) { 
            var len = 0; 
            for (var i=0; i < str.length; i++) { 
                if (str.charCodeAt(i) > 127 || str.charCodeAt(i) == 94) { 
                    len += 2; 
                } else { 
                    len ++; 
                } 
            } 
            return len; 
        }

	};

	if (typeof define == 'function') {
		define(['Zepto'], function ($) {
			return util;
		});
	}
	
	window.Util = window.Util || util;
	
	/*** 时间格式化相关 ***/
	var format = function(formatStr) {
        var year, month, day, hour, minute, second, 
            reg, rule, afterFormat;

        if (!formatStr) {
            return this.getTime();
        }

        year = this.getFullYear();
        month = this.getMonth() + 1;
        day = this.getDate();
        hour = this.getHours();
        minute = this.getMinutes();
        second = this.getSeconds();

        rule = {
            'yy': year - 2000,
            'yyyy': year,
            'M': month,
            'MM': util.timeAddZero(month),
            'd': day,
            'dd': util.timeAddZero(day),
            'h': hour,
            'hh': util.timeAddZero(hour),
            'm': minute,
            'mm': util.timeAddZero(minute),
            's': second,
            'ss': util.timeAddZero(second)
        };

        reg = /y{2,4}|M{1,2}|d{1,2}|h{1,2}|m{1,2}|s{1,2}/g;

        afterFormat = formatStr.replace(reg, function($) {
            if ($ in rule) {
                return rule[$];
            } else {
                return $;
            }
        });

       return afterFormat;

    };
    
    Date.prototype.format = format;
    
    
    var dateCompare = function (compareTime) {
    	var time = this.getTime();
    	
    	compareTime = +new Date(compareTime);
    	
    	if (isNaN(compareTime)) {
    		return NaN;
    	}
    	
    	return time > compareTime ? 1 : time == compareTime ? 0 : -1;
    };
    
    Date.prototype.compare = dateCompare;
    
  //按钮需要在点击的时候添加样式->初始化
    $(function () {
    	$('body').on('touchstart', '[data-active]', function (e) {
    		var $this = $(this);
    		$this.addClass($this.data('active'));
    	})
    	.on('touchend', '[data-active]', cancel).on('touchcancel', '[data-active]', cancel);
    	
    	function cancel() {
    		var $this = $(this);
    		$this.removeClass($this.data('active'));
    	}
    });
    	
})(window);