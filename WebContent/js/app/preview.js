$(function(){
	function Preview (cfg) {
		this.$target = $(cfg.target);	
		this.$previewCont = $(cfg.previewCont);
		this.picSelector = cfg.picSelector;
		this.imgUrlArrs = cfg.imgUrlArrs;
		this.autoBindUI = cfg.autoBindUI === false ? false : true;	//是否自动绑定点击小图预览的控制
		this.hash = cfg.hash || '#perview';
		return this.init();
	}
	
	Preview.prototype = {
		init: function () {
			var me = this;
			
			this.renderUI();	//生成页面
			
			//插件SWIPER初始化
			var mySwiper = new Swiper(this.$previewCont, {
				// 如果需要分页器
			    //pagination: '.swiper-pagination',
			    onSlideChangeEnd: function(swiper){
			    	
			    	$('.curPage').html(mySwiper.activeIndex + 1 + '/' + me.imgUrlArrs.length);
			    
			    }
			  });
			
			this.mySwiper = mySwiper;

			this.bindUI();
			
			return this;
		},
		
		renderUI: function () {
			var imgUrlArrs = this.imgUrlArrs;
			
			var html = '';
			
			for (var i = 0, len = imgUrlArrs.length; i < len; i++) {
				html += '<div class="swiper-slide"><img src="' + imgUrlArrs[i] + '"/></div>';
				
			}
			
			this.$previewCont.find('.swiper-wrapper').html(html);
		},
		
		bindUI: function () {
			var me = this;
			
			//点击退出全屏预览
			this.$previewCont.on('click', function(){
				me.hide();
				if (location.hash !== undefined) {
					history.go(-1);
				}
			});
			
			//需要自动绑定点击预览大图事件
			if (this.autoBindUI) {
				this.bindPreview();
			}
			
			//用过hash控制点击返回键关掉图片预览
			if (location.hash !== undefined) {
				window.addEventListener('hashchange', function (e) {
					if (e.oldURL.indexOf(me.hash) != -1) {
						me.hide();
					}
				});
			}
		},
		
		bindPreview: function () {
			var me = this;
			
			this.$target.on('click', this.picSelector, function () {
				var curIndex = $(this).index();
				me.preview(curIndex, false);
			});
		},
		
		//预览函数
		preview: function (index, needUpdate) {
			//需要更新预览
			if (needUpdate) {
				this.renderUI();
			}

			this.$previewCont.show()
						   .find('.curPage')
						   .html(index + 1 + '/' + this.imgUrlArrs.length);
			
			this.mySwiper.update();

			this.mySwiper.slideTo(index, 10, false);
			
			location.hash = this.hash;
		},
		
		hide: function () {
			this.$previewCont.hide();
		}
	};
	
	window.Preview = window.Preview || Preview;
});