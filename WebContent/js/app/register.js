$(function(){

	var applicationId;
	toJson = function(a){
		var o = {};
		$.each(a,function(){
			console.log(this);
			o[this.name] = this.value || '';
		})
		return o;
	};
	function tipCover(msg){
		$('.tip').text(msg).attr('style','margin-left:200px;').show();
		setTimeout(function(){
			$('.tip').hide();
		}, 2000)
	}

	// function moduleTabCtrl(){
	// 	$('#module-tabs li').click(function(){

	// 		console.log(this)
	// 		var index = $(this).index();

	// 		$('#module-tabs li').removeClass('active');
	// 		$(this).addClass('active')
	// 		$('#module-form>div').hide().eq(index).show();
	// 	})
	// } 
	// moduleTabCtrl();

	// var moduleOpenCtrl = (function(){
	// 	var open = false;
	// 	$('#module-open').click(function(){
	// 		if( open ){
	// 			open = false;
	// 			$('#module-tabs,#module-form').hide();
	// 			$('#module-open span').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
	// 		}
	// 		else{
	// 			open = true;
	// 			$('#module-tabs,#module-form').show();
	// 			$('#module-open span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
	// 		}	
	// 	})
	// })()

	$('#register-form').validate({
		debug : true,
		messages : {
			mcloudAppId : {
				required : '请输入appID'
			},
			mcloudAppSecret : {
				required : '请输入mcloudAppSecret'
			},
			pubaccKey : {
				required : '请输入pubaccKey'
			},
			mcloudAppSecret : {
				required : '请输入mcloudAppSecret'
			},
			pubacc : {
				required : '请输入pubacc'
			},
			name : {
				required : '请输入名称'
			},
			accessAddress : {
				required : '请输入访问地址'
			}
		},
		submitHandler : function(form){
			console.log($('#register-form').serialize())
			var data = toJson($('#register-form').serializeArray())

			if( data.isOpenPubacc == 'true' ){
				data.isOpenPubacc = true;
			}
			else{
				data.isOpenPubacc = false;
			}
			if( data.isOpenSms == 'true' ){
				data.isOpenSms = true;
			}
			else{
				data.isOpenSms = false;
			}

			$.ajax({
				contentType : 'application/json;charset=UTF-8',
				url: '/freeflow/rest/application/create.json',
				type: 'POST',
				dataType: 'json',
				data: JSON.stringify( data )
			})
			.done(function( data ) {
				console.log("success");
				if( data.success ){
					tipCover('增加成功');
					// applicationId = data.data.applicationId;
					// $('#module-btn').click();
				}
				else{
					tipCover(data.errorMsg)
				}
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			return false;
		}
	})

	// $('#module-form').validate({
	// 	debug : true,
	// 	messages : {
	// 		title : {
	// 			required : '请输入标题'
	// 		},
	// 		content : {
	// 			required : '请输入内容'
	// 		}
	// 	},
	// 	submitHandler : function(form){
	// 		alert(1);
	// 		console.log($('#module-form').serialize())
	// 		var data = toJson($('#module-form').serializeArray())
	// 		data.applicationId = applicationId;
	// 		console.log(data)
	// 		console.log(JSON.stringify( data ))
	// 		// form.submit(function(){
	// 		// 	return false;
	// 		// });
	// 		// return false;
	// 		$.ajax({
	// 			contentType : 'application/json;charset=UTF-8',
	// 			url: '/freeflow/rest/application/msg-template/create.json',
	// 			type: 'POST',
	// 			dataType: 'json',
	// 			data: JSON.stringify( data )
	// 		})
	// 		.done(function() {
	// 			console.log("success");
	// 		})
	// 		.fail(function() {
	// 			console.log("error");
	// 		})
	// 		.always(function() {
	// 			console.log("complete");
	// 		});
	// 		return false;
	// 	}
	// });
	$('#submit-btn').click(function(){
		if( $('#register-form').valid() ){
			// if( $('#module-form').valid() ){
				// $('#register-btn').click();
			// }
			// else{
			// 	$('#module-btn').click();
			// }
		}
		// else{
		// 	$('#register-btn').click();
		// }
		
		// $('#module-btn').click();
	})
	
})
