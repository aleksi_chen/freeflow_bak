(function (window, $, undefined) {

	var validator = new Validator();

	var widgetTpl = {
		textfield: '<div class="pr-component pr-textfield clearfix" data-require=<%= require %>>'
			    		+ '<span class="pr-component-label"><%= label %></span>'
			    		+ '<div class="pr-component-item">'
			    			+ '<input type="type" class="pr-component-area pr-textfield-input" placeholder="<%= placeholder %><%= require ? "(必填)" : "" %>">'
			    		+ '</div>'
			    	+ '</div>',

		textareafield: '<div class="pr-component pr-textareafield clearfix" data-require=<%= require %> >'
			    		+ '<span class="pr-component-label"><%= label %></span>'
			    		+ '<div class="pr-component-item">'
			    			+ '<div class="pr-component-area pr-textareafield-area">'
			                    + '<div class="pr-textarea-inner"></div>'
			                    + '<textarea class="pr-textareafield-textarea" placeholder="<%= placeholder %><%= require ? "(必填)" : "" %>"></textarea>'
			                + '</div>'
			    		+ '</div>'
			    		+ '<div class="pr-wordcontrol">'
			    			+ '<span class="pr-currword">120</span>/<span class="pr-totalword">200</span>'
			    		+ '</div>'
			    	+ '</div>',

		numberfield: '<div class="pr-component pr-numberfield clearfix" data-require=<%= require %>>'
			    		 + '<span class="pr-component-label"><%= label %></span>'
			    		 + '<div class="pr-component-item">'
			    			 + '<input type="number" class="pr-component-area pr-numberfield-input" placeholder="<%= placeholder %><%= require ? "(必填)" : "" %>">'
			    		 + '</div>'
			    	 + '</div>', 

		radiofield: '<div class="pr-component pr-radiofield" data-require=<%= require %>>'
			    		+ '<span class="pr-component-label"><%= label %></span>'
			            + '<div class="pr-component-item">'
			                + '<span class="caret"></span>'
			                + '<div class="pr-component-radioselect">'
			                + '<select name="" class="pr-component-radioarea">'
			                	+ '<% _.each(otherProps.options, function(item) { %>'
			                    + '<option value="<%= item %>"><%= item %></option>'
			                    + '<% }) %>'
			                + '</select>'
			                + '<span class="pr-dataplaceholder grey">请选择<%= require ? "(必填)" : "" %></span>'
			                + '</div>'
			            + '</div>'
			    	+ '</div>',

		datefield: '<div class="pr-component pr-datefield clearfix" data-require=<%= require %>>'
				    	+ '<span class="pr-component-label"><%= label %></span>'
				    	+ '<span class="caret"></span>'
				    	+ '<div class="pr-component-item pr-component-picker">'
				    		+ '<input value="" class="pr-component-area pr-datefield-input blue" type=<%= (otherProps.format == "yyyy/MM/dd") ? "date" : "datetime-local" %>>'
				    		+ '<span class="pr-dataplaceholder grey">请选择<%= require ? "(必填)" : "" %></span>'
				    	+ '</div>'
					+ '</div>',

		daterangefield: '<div class="pr-component pr-daterangefield clearfix" data-require=<%= require %>>'
					    	+ '<div class="pr-startpicker">'
						    	+ '<span class="pr-component-label"><%= otherProps.labels[0] %></span>'
						    	+ '<span class="caret"></span>'
						    	+ '<div class="pr-component-item pr-component-picker">'
						    		+ '<input value="" class="pr-component-area pr-startpicker-input blue" type=<%= (otherProps.format == "yyyy/MM/dd") ? "date" : "datetime-local" %>>'
						    		+ '<span class="pr-dataplaceholder grey">请选择<%= require ? "(必填)" : "" %></span>'
						    	+ '</div>'
						    + '</div>'
						    + '<div class="pr-endpicker">'
						    	+ '<span class="pr-component-label"><%= otherProps.labels[1] %></span>'
						    	+ '<span class="caret"></span>'
						    	+ '<div class="pr-component-item pr-component-picker">'
						    		+ '<input value="" class="pr-component-area pr-endpicker-input blue" type=<%= (otherProps.format == "yyyy/MM/dd") ? "date" : "datetime-local" %>>'
						    		+ '<span class="pr-dataplaceholder grey">请选择<%= require ? "(必填)" : "" %></span>'
						    	+ '</div>'
						    + '</div>'
				    	+ '</div>',

		photofield: '<div class="pr-component pr-photofield" data-require=<%= require %>>'
						+ '<div class="pr-component-uploadlabel"><%= label %><%= require ? "(必填)" : "" %></div>'
						+ '<div class="pr-upload-addbtn">'
					    	+ '<b class="horizontal-line"></b>'
					    	+ '<b class="vertical-line"></b>'
					    	+ '<input type="file" accept="image/*" class="pr-photofield-input" id="file"></span>'
				    	+ '</div>'
					+ '</div>',
    
		filefield: '<div class="pr-component pr-photofield" data-require=<%= require %>>'
						+ '<div class="pr-component-uploadlabel"><%= label %><%= require ? "(必填)" : "" %></div>'
						+ '<div class="pr-upload-addbtn">'
					    	+ '<b class="horizontal-line"></b>'
					    	+ '<b class="vertical-line"></b>'
					    	+ '<input type="file" accept="image/*" class="pr-photofield-input" id="file"></span>'
				    	+ '</div>'
					+ '</div>',
	};

	// 控件集合
	var processTpl = {

		'textfield': {
			getWidgetTpl: widgetTpl.textfield,
			init: initTextfield
		},

		'textareafield': {
			getWidgetTpl: widgetTpl.textareafield,
			init: initTextareafield
		},

		'numberfield': {
			getWidgetTpl: widgetTpl.numberfield,
			init: initNumberfield
		},

		'radiofield': {
			getWidgetTpl: widgetTpl.radiofield,
			init: initRadiofield
		},

		'datefield': {
			getWidgetTpl: widgetTpl.datefield,
			init: initDatefield
		},

		'daterangefield': {
			getWidgetTpl: widgetTpl.daterangefield,
			init: initDaterangefield
		},

		'photofield': {
			getWidgetTpl: widgetTpl.photofield,
			init: initPhotofield
		},
		
		'filefield': {
			getWidgetTpl: widgetTpl.filefield,
			init: initFilefield
		}
	
	};

	function initTextfield(data, $elem) {
		if(data['require']) {
			validator.add($elem.children('.pr-textfield-input'), [{
				strategy: 'isNotEmpty',
				errorMsg: data.label + '不能为空'
			}]);
		}

	}

	function initTextareafield(data, $elem) {
		if(data['require']) {
			validator.add($elem.children('.pr-textareafield-textarea'), [{
				strategy: 'isNotEmpty',
				errorMsg: data.label + '不能为空'
			}]);
		}

		$elem.on('input', '.pr-textareafield-textarea', function() {
			var val = $(this).val().replace(/\n/g, '<br>');
			$('.pr-textarea-inner').html(val);
		});
	}

	function initNumberfield(data, $elem) {
		if(data['require']) {
			validator.add($elem.children('.pr-numberfield-input'), [{
				strategy: 'isNotEmpty',
				errorMsg: data.label + '不能为空'
			}]);
		}

		validator.add($elem.children('.pr-numberfield-input'), [{
			strategy: 'isNumber',
			errorMsg: data.label + '不能有非数字字符'
		}]);
	}

	function initRadiofield(data, $elem) {
		if(data['require']) {
			validator.add($elem.children('.pr-component-radioarea'), [{
				strategy: 'isNotEmpty',
				errorMsg: data.label + '不能为空'
			}]);
		}

		if(Util.checkMobileSystem('ios')) { 

			$elem.on('tap', function() {
				$(this).find('.pr-component-radioarea').trigger('mousedown');
			});

			$elem.find('.pr-component-radioarea').on('change', function() {
				$elem.find('.pr-dataplaceholder').text($(this).val()).addClass('blue'); 
			});

		} else {

			var val = $elem.find('.pr-dataplaceholder').text();

			$elem.find('.pr-component-radioarea').mobiscroll().select({
			       theme: 'ios',
			       lang: 'zh',
			       display: 'bottom',
			       minWidth: 200
			});

			
			$elem.find('.pr-dataplaceholder').hide();
			$elem.find('input').val('').attr('placeholder', val);

			$elem.find('.pr-component-radioarea').on('change', function() {
				$elem.find('.pr-dataplaceholder').text($(this).val()).addClass('blue'); 
			});
		}

	}

	function initDatefield(data, $elem) {
		if(data['require']) {
			validator.add($elem.children('.pr-datefield-input'), [{
				strategy: 'isNotEmpty',
				errorMsg: data.label + '不能为空'
			}]);
		}

		if(Util.checkMobileSystem('ios')) { 
			$elem.on('tap', function() {
				$(this).find('.pr-datefield-input').trigger('mousedown');
			});

			$elem.on('change', '.pr-datefield-input', function() {
				var optDate = $(this).val().replace(/T/g, /' '/);
				$elem.find('.pr-dataplaceholder').text(optDate).addClass('blue');
			});

		} else {
			$elem.find('.pr-datefield-input').attr('type', 'text');
			var now = new Date();

			if(data['otherProps']['format'] == 'yyyy/MM/dd') {
				$elem.find('.pr-datefield-input').mobiscroll().date({
				       theme: 'ios',
				       lang: 'zh',
				       display: 'bottom',
				       minDate: new Date(now.getFullYear() - 5, now.getMonth(), now.getDate()),
	        		   maxDate: new Date(now.getFullYear() + 5, now.getMonth(), now.getDate())
				});
			} else {
				$elem.find('.pr-datefield-input').mobiscroll().datetime({
				       theme: 'ios',
				       lang: 'zh',
				       display: 'bottom',
				       minDate: new Date(now.getFullYear() - 5, now.getMonth(), now.getDate()),
	        		   maxDate: new Date(now.getFullYear() + 5, now.getMonth(), now.getDate())
				});
			}
	
			$elem.on('tap', function() {
				$(this).find('input').trigger('mousedown');
			});

			$elem.on('change', '.pr-datefield-input', function() {
				$elem.find('.pr-dataplaceholder').text($(this).val()).addClass('blue');
			});
			
		}

	}

	function initDaterangefield(data, $elem) {
			if(data['require']) {
				validator.add($elem.children('.pr-startpicker-input'), [{
					strategy: 'isNotEmpty',
					errorMsg: data.label + '不能为空'
				}]);
				validator.add($elem.children('.pr-endpicker-input'), [{
					strategy: 'isNotEmpty',
					errorMsg: data.label + '不能为空'
				}]);
			}

			if(Util.checkMobileSystem('ios')) { 
				$elem.on('tap', '.pr-startpicker, .pr-endpicker', function() {
					$(this).find('input').trigger('mousedown');
				});

				$elem.on('change', '.pr-startpicker-input, .pr-endpicker-input', function() {
					var optDate = $(this).val().replace(/T/g, /' '/);
					$(this).siblings('.pr-dataplaceholder').text(optDate).addClass('blue');
				});

			} else {
				var now = new Date();
				$elem.find('.pr-startpicker-input, .pr-endpicker-input').attr('type', 'text');

				if(data['otherProps']['format'] == 'yyyy/MM/dd') {
					$elem.find('.pr-startpicker-input, .pr-endpicker-input').mobiscroll().date({
					       theme: 'ios',
					       lang: 'zh',
					       display: 'bottom',
					       minDate: new Date(now.getFullYear() - 5, now.getMonth(), now.getDate()),
		        		   maxDate: new Date(now.getFullYear() + 5, now.getMonth(), now.getDate())
					});
				} else {
					$elem.find('.pr-startpicker-input, .pr-endpicker-input').mobiscroll().datetime({
					       theme: 'ios',
					       lang: 'zh',
					       display: 'bottom',
					       minDate: new Date(now.getFullYear() - 5, now.getMonth(), now.getDate()),
		        		   maxDate: new Date(now.getFullYear() + 5, now.getMonth(), now.getDate())
					});
				}

				$elem.on('tap', '.pr-startpicker, .pr-endpicker', function() {
					$(this).find('input').trigger('mousedown');
				});

				$elem.on('change', '.pr-startpicker-input, .pr-endpicker-input', function() {
					$(this).siblings('.pr-dataplaceholder').text($(this).val()).addClass('blue');
				});
				
			}
	}

	function initPhotofield(data, $elem) {
		if(data['require']) {
			validator.add($elem.children('.pr-photofield-input'), [{
				strategy: 'isNotEmpty',
				errorMsg: data.label + '不能为空'
			}]);
		}
		
		$elem.find('#file').upload({
			uploadUrl: '/freeflow/c/file/upload.json',
			previewWrap: '.upload-img-list',
			
			previewUrl: '/freeflow/c/file/image/get.json?fileId=' + '{id}&w=120&h=120',		//图片预览路径
			
			//上传开始
			onUploadStart: function (xhr) {
				var $progress = this.find('.progress', true);
				//保存进度节点
				$progress.html('0%').addClass('uploading');
				//上传图片限制为4张
				if($('.pic-preview').length >= 4){
					$('.upload-addbtn').hide();
				}
			},
			//上传成功
			onUploadSuccess: function (fileInfo) {
				//alert('上传成功了，' + fileInfo.id);
				
				this.getRoot$().data('fileId', fileInfo.id)
							   .addClass('success');
				//隐藏进度
				this.find('.progress', true).html('').removeClass('uploading');
				//显示删除按钮
				this.find('.del', true).fadeIn();
				
				//实例化预览
				// me.initPreveiw();
			},
			//上传进度
			onUploadProgress: function (loaded, total) {
				console.log('共：' + total + '，上传了：' + loaded);
				
				var $progress = this.find('.progress', true);
				//修改进度
				$progress.html(Math.floor(loaded / total * 100) + '%');
				
			},
			//上传失败
			onUploadFail: function (xhr) {
				var $progress = this.find('.progress', true);
				$progress.removeClass('uploading');
			},
			onDelImage: function () {
				//上传图片小于4张
				if($('.pic-preview').length <= 4){
					$('.upload-addbtn').show();
				}
			}
		});
	}
	
	function initFilefield(data, $elem) {
		if(data['require']) {
			validator.add($elem.children('.pr-photofield-input'), [{
				strategy: 'isNotEmpty',
				errorMsg: data.label + '不能为空'
			}]);
		}
		
		$elem.find('#file').upload({
			uploadUrl: '/freeflow/c/file/upload.json',
			previewWrap: '.upload-img-list',
			
			previewUrl: '/freeflow/c/file/image/get.json?fileId=' + '{id}&w=120&h=120',		//图片预览路径
			
			//上传开始
			onUploadStart: function (xhr) {
				var $progress = this.find('.progress', true);
				//保存进度节点
				$progress.html('0%').addClass('uploading');
				//上传图片限制为4张
				if($('.pic-preview').length >= 4){
					$('.upload-addbtn').hide();
				}
			},
			//上传成功
			onUploadSuccess: function (fileInfo) {
				//alert('上传成功了，' + fileInfo.id);
				
				this.getRoot$().data('fileId', fileInfo.id)
							   .addClass('success');
				//隐藏进度
				this.find('.progress', true).html('').removeClass('uploading');
				//显示删除按钮
				this.find('.del', true).fadeIn();
				
				//实例化预览
				// me.initPreveiw();
			},
			//上传进度
			onUploadProgress: function (loaded, total) {
				console.log('共：' + total + '，上传了：' + loaded);
				
				var $progress = this.find('.progress', true);
				//修改进度
				$progress.html(Math.floor(loaded / total * 100) + '%');
				
			},
			//上传失败
			onUploadFail: function (xhr) {
				var $progress = this.find('.progress', true);
				$progress.removeClass('uploading');
			},
			onDelImage: function () {
				//上传图片小于4张
				if($('.pic-preview').length <= 4){
					$('.upload-addbtn').show();
				}
			}
		});
	}


	window.processTpl = window.processTpl || processTpl;
})(window, Zepto);