$(function(){
	function getLocalStorage( key ){
		var val = localStorage.getItem(key),
			rObj = /^{.*}$/;
		if( !val ){
			return false;
		}
		if( rObj.test(val) ){
			return JSON.parse(val);
		}
		else{
			return val;
		}

	}
	function insertInputVal( obj, formEle ){
		var inputEle = formEle.find( 'input[type!="radio"],textarea');
		$.each( inputEle, function( index, val){
			var $val = $(val);
			var inputName = $val.attr('name');
			$val.val(obj[inputName] || '')
		} )
	}
	function insertSelectVal( obj, formEle ){
		var selectEle = formEle.find( 'select');
		$.each( selectEle, function( index, val){
			var $val = $(val);
			var inputName = $val.attr('name');
			$val.find('option[value = '+obj[inputName] +' ]').attr('selected', true)
		} )
	}
	function insertRadioVal( obj, formEle ){
		var radioEle = formEle.find( 'input[type = "radio"]');
		$.each( radioEle, function( index, val){
			var $val = $(val);
			var inputName = $val.attr('name');
			if( obj[inputName] == $val.attr('value') ){
				$val[0].checked = true;
			}
		} )
	}
	function insertForm( obj, formEle ){
		insertInputVal( obj, formEle );
		insertSelectVal( obj, formEle );
		insertRadioVal( obj, formEle );
	}
	var freeflowObj = getLocalStorage( 'freeflow' );
	console.log(freeflowObj)
	$.ajax({
		contentType : 'application/json;charset=UTF-8',
		url: '/freeflow/rest/application/get.json',
		type: 'post',
		dataType: 'json',
		data : JSON.stringify( {id:freeflowObj.id} )
	})
	.done(function( data ) {
		if( data.success && data.data ){
			insertForm( data.data, $('#details-form') );
		}
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
})