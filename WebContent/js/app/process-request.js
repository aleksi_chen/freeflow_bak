(function (window, $, undefined) {
	var processRequest = {
		data: {
			submitProcessUrl: '/freeflow/rest/form/create.json',		//提交请求的路径
			successGoToUrl: '/freeflow/c/form/list.json',	//请求成功后的跳转路径
			getLeaderUrl: '/freeflow/rest/form/get-leader.json',
			getFormDataUrl: '/freeflow/rest/form/get.json',

			getComponentUrl: '/freeflow/data/tpl.txt',
			getProcessTypeUrl: '/freeflow/data/process-type.txt', 

			picUrl: '/freeflow/c/file/image/get.json?fileId=',
			formId: '',
			sendingText: '提交中...',
			submitBtnText: '提交'
		},

		init: function() {
			document.documentElement.style.fontSize = document.documentElement.clientWidth / 6.4 + 'px';
			XuntongJSBridge && XuntongJSBridge.call('hideOptionMenu');	//隐藏云之家右上角按钮		

			this.renderUI();
			this.bindUI();	
			
			this.getFormData();
			this.getLeader();
			this.initSelectManager();
			this.initSubmitBtn();
			this.upload();
		},

		renderUI: function() {
			this.getProcessType().done(function(resp) {
				var tpl = '';
				for(var i = 0, len = resp.length; i < len; i++) {
					tpl += '<li class="pr-item" data-id="' + resp[i].id + '">' + resp[i].name + '</li>';
				}
				$('.pr-type').html(tpl);
			}).fail(function() {
				alert("获取审批类型失败！");
			});
		},

		getProcessType: function() {
			var deferred = $.Deferred();

			$.ajax({
				url: this.data.getProcessTypeUrl,
				type: 'POST',
				dataType: 'JSON',
				// contentType: 'application/json;charset=UTF-8',
				data: {

				}
			})
			.done(function(resp) {
				resp = JSON.parse(resp);
				if(resp.success) {
					 deferred.resolve(resp.data.detail);
				} else {
					 deferred.reject();
				}
			})
			.fail(function(resp) {
				 deferred.reject();
			});

			return deferred.promise();
		},
		
		bindUI: function() {

			var me = this;

			$('body').on('tap', '.pr-item', function() {
				$('.loading').css('display', 'block');
				var typeID = $(this).data('id');
				me.getProcessComponent(typeID).done(function(resp) {

					var templateTpl = '',
						content = resp.data.content;

					var $empty = $(), $temp;

					for(var i = 0, len = content.length; i < len; i++) {
						templateTpl = _.template(processTpl[content[i]['type']].getWidgetTpl)(content[i]);
						$temp = $(templateTpl);
						$empty = $empty.add($temp);
						processTpl[content[i]['type']].init(content[i], $temp);
					}

					$('.pr-choose').hide();
					$('.pr-wrapper').prepend($empty).show();
					$('.pr-request-submit').css('display', 'block');

					var str = me.showMoreBtn($.trim($('.pr-approver-list').text()));
					$('.pr-approver-list').html(str);

				}).fail(function() {
					alert("获取审批模板失败！");
				}).always(function() {
					$('.loading').css('display', 'none');
				});

			});
		},

		getProcessComponent: function(typeID) {
			var deferred = $.Deferred();

			$.ajax({
				url: this.data.getComponentUrl,
				type: 'POST',
				dataType: 'JSON',
				// contentType: 'application/json;charset=UTF-8',
				data: {

				}
			})
			.done(function(resp) {
				resp = JSON.parse(resp);
				if(resp.success) {
					 deferred.resolve(resp);
				} else {
					 deferred.reject();
				}
			})
			.fail(function(resp) {
				 deferred.reject();
			});

			return deferred.promise();
		},

		showMoreBtn: function(str) {
			var rowChineseLen = Math.floor(($('.pr-approver-list').innerWidth() - parseFloat($('.pr-approver-list').css('padding-left')) * 2) / 18);
			var strChineseLen = Math.ceil(Util.getBytes(str) / 2);

			if(strChineseLen > rowChineseLen * 2) {
				str = str.substr(0, (rowChineseLen * 2 - 4)) + '...<span class="pr-more-btn blue">更多</span>';
			}

			return str;

		},


		//上传相关
		upload: function () {
			var me = this;
			
			//上传图片插件调用
			$('#file').upload({
				uploadUrl: '/freeflow/c/file/upload.json',
				previewWrap: '.upload-img-list',
				
				previewUrl: this.data.picUrl + '{id}&w=120&h=120',		//图片预览路径
				
				//上传开始
				onUploadStart: function (xhr) {
					var $progress = this.find('.progress', true);
					//保存进度节点
					$progress.html('0%').addClass('uploading');
					//上传图片限制为4张
					if($('.pic-preview').length >= 4){
						$('.upload-addbtn').hide();
					}
				},
				//上传成功
				onUploadSuccess: function (fileInfo) {
					//alert('上传成功了，' + fileInfo.id);
					
					this.getRoot$().data('fileId', fileInfo.id)
								   .addClass('success');
					//隐藏进度
					this.find('.progress', true).html('').removeClass('uploading');
					//显示删除按钮
					this.find('.del', true).fadeIn();
					
					//实例化预览
					me.initPreveiw();
				},
				//上传进度
				onUploadProgress: function (loaded, total) {
					console.log('共：' + total + '，上传了：' + loaded);
					
					var $progress = this.find('.progress', true);
					//修改进度
					$progress.html(Math.floor(loaded / total * 100) + '%');
					
				},
				//上传失败
				onUploadFail: function (xhr) {
					var $progress = this.find('.progress', true);
					$progress.removeClass('uploading');
				},
				onDelImage: function () {
					//上传图片小于4张
					if($('.pic-preview').length <= 4){
						$('.upload-addbtn').show();
					}
				}
			});
			
			//上传图片按钮点击选择图片
		/*	$('.upload-addbtn').on('click',function(){
				return $('#file').click();
			});*/
		},
		
		//实例化预览
		initPreveiw: function () {
			var me = this, oldSuccess, imgIds, preview;
			if (!this.initPreveiw.inited) {
				imgIds = me.getImgIdArrs();
				oldSuccess = imgIds.length;
				
				preview = new Preview({
					'previewCont': '.swiper-container',
					'imgUrlArrs': getImgUrlArrs(imgIds),
					'autoBindUI': false
					
				});
				
				this.initPreveiw.inited = true;
				
				$('.upload-img-list').on('click', '.success', function (e) {
					var $this = $(this);
					
					imgIds = me.getImgIdArrs();
					
					//判断是否需要更新大图预览
					var needUpdate = false,
						newSuccess = $('.upload-img-list').children('.success').length;
					if (oldSuccess < newSuccess) {
						needUpdate = true;
						oldSuccess = newSuccess;
					}
					
					//console.log(needUpdate);

					var imgArrs = getImgUrlArrs(imgIds);
					
					preview.imgUrlArrs = imgArrs;
					preview.preview(imgIds.indexOf($this.data('fileId')), true);
					
					//return imgArrs;
				});
				
			}
			
			function getImgUrlArrs(imgIds) {
				imgUrlArrs = [];
			
				imgIds.forEach(function (value) {
					imgUrlArrs.push(me.data.picUrl + value);
				});
				
			
				return imgUrlArrs;
				
			}
		},

		getImgIdArrs: function () {
			
			var imgArr = [];
			$('.pic-preview').each(function(){
				var imgId = $(this).data('fileId');
				
				if (imgId) {
					imgArr.push(imgId);
				}
			});
			return imgArr;
		},

		getFormData: function() {
			var me = this;
			this.data.formId = Util.getUrlValue('formId') || '';

			if(me.data.formId === '') {
				return;
			}

			takenotes('_', '_', '8459', '重新提交的点击次数', '0');

			$.ajax({
				type: 'POST',
				url: me.data.getFormDataUrl,
				contentType: 'application/json; utf-8',
				data: JSON.stringify({
					id: me.data.formId
				})
			})
			.done(function (resp) {
				if (resp.success === true) {
					$('.request-title').val(resp.data.title);
					$('.request-reason').val(resp.data.content);
					me.data.leaderId = resp.data.firstApproverUserOid;
				    me.data.leaderName = resp.data.firstApproverUserName;
				    $('.request-manager-name').html(resp.data.firstApproverUserName);
				}
			})
			.fail(function (resp) {
				
			});

		},

		getLeader: function() {
			var me = this;

			if(me.data.formId !== '') {
				return;
			}

			$.ajax({
				type: 'POST',
				contentType: 'application/json; utf-8',
				url: me.data.getLeaderUrl
			})
			.done(function (resp) {
				//console.log(resp);
				if (resp.success === true) {
					
					$('.request-manager-name').html(resp.data.name);
					me.data.leaderId = resp.data.oid;
					me.data.leaderName = resp.data.name;
				}
			})
			.fail(function (resp) {
				
			});
		},

		//选择审批人
		initSelectManager: function () {
			var me = this;
			
			$('.request-manager').tap(function () {
				me.selectUserByApp();
			});
		},
		
		//调用云之家选人接口
		selectUserByApp: function () {
			var me = this,
				$managerName = $('.request-manager-name'),
				managerId = this.data.leaderId,
				param = {
					'isMulti': false,	//是否多选 默认false
					'blacklist': [currUserId]	//黑名单，排除人员的openId
					//'whitelist': []	//白名单，包含人员的openId(默认选中)
				};
			
			if (managerId) {
				param.whitelist = [managerId];
			}
				
			XuntongJSBridge.call('selectPersons', param, function (resp) {
					var person;			
					if (resp.success === true || resp.success == 'true') {
						person = resp.data.persons[0];
						$managerName.html(person.name);
						me.data.leaderId = person.openId;
						me.data.leaderName = person.name;
					}
			});
		},

		//提交按钮
		initSubmitBtn: function () {
			var me = this;
			
			$('.request-submit').tap(function () {
				var $this= $(this), data;
				//判断图片是否正在上传2015/9/14
				var imgLoadUp = true;
				
				$('.pic-preview').find('.progress').each(function(){
					if($(this).hasClass('uploading')){
						imgLoadUp = false;
							//提示用户图片正在上传
							$('.mask').show().css('opacity','0.6');
							Util.delay(1500).done(function () {
								$('.mask').hide();
								isLoading = true;
							});
						
						return false;
					}
				});
				if(!imgLoadUp){
					
					return;
				}
				if ($this.hasClass('sending')) {
					return ;
				}
				
				data = me.getData();		//获取填写的数据
				if (!me.checkData(data)) {	//校验数据
					return ;
				}
				
				
				$this.addClass('sending');
				//提交请求
				me.submitProcessPost(data)
				  .done(function (resp) {
					  me.onSubmitSuccess(resp);
				  })
				  .fail(function (resp) {
					  Util.showTips('不好意思，提交出错了，请稍后重试');
				  })
				  .always(function () {
					 $this.removeClass('sending');
				  });
				
				
		
			});
		},
		
		//提交成功
		onSubmitSuccess: function (resp) {
			var me = this;
			Util.showTips('提交成功');

			if(me.data.formId !== '') {
				takenotes('_', '_', '8460', '重新提交后成功发起的次数', '0');
			} 
			
			window.location.href = me.data.successGoToUrl;
		},
		
		//提交请求
		submitProcessPost: function (data) {
			var me = this,
				deferred = $.Deferred(),
				$submitText = $('.request-submit button');
			$submitText.html(this.data.sendingText);
				
			$.ajax({
				type: 'POST',
				url: me.data.submitProcessUrl,
				contentType: 'application/json; utf-8',
				data: JSON.stringify(data)
			})
			.done(function (resp) {
				if (resp.success === true) {
					deferred.resolve(resp);
				} else {
					deferred.reject(resp);
				}
			})
			.fail(function (resp) {
				deferred.reject(resp);
			})
			.always(function () {
				$submitText.html(me.data.submitBtnText);
			});
			
			return deferred.promise();
		},
		
		//校验数据
		checkData: function (data) {
			var $content = $('.request-reason'),
			    $title = $('.request-title'); 

			//申请内容是否为空
			if (!data.title) {
 				$title.addClass('error');
				Util.delay(1200).done(function () {
					$title.focus();
					$title.removeClass('error');
				});
				return false;
			}

			//申请内容是否为空
			if (!data.content) {
 				$content.addClass('error');
				Util.delay(1200).done(function () {
					$content.focus();
					$content.removeClass('error');
				});
				return false;
			}

			//是否选了部门负责人
			if (!this.data.leaderId) {
				this.selectUserByApp();
				return false;
			} 
			
			return true;
		},
		
		//获取提交数据
		getData: function () {
			var data = {
					fileIds: this.getImgIdArrs(),
					rejectFormId: this.data.formId,
					firstApproverUserId: this.data.leaderId,				//最新审批人用户ID
					title: Util.escapeToHtmlEntity($('.request-title').val().trim()).replace(/\n|\r/g, '<br>'),    // 标题
					content: Util.escapeToHtmlEntity($('.request-reason').val().trim()).replace(/\n|\r/g, '<br>')	// 内容
				};
			
			return data;
		}

	};

	window.processRequest = window.processRequest || processRequest;
})(window, Zepto);