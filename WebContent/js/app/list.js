$(function(){
	
	var ffObj = {
		theData : '',
		tabMsg : {
			useScene : 1,
			tabNow : 'details'
		},
		listCtr : (function(){
			var index = 1,
				pageCount = 0,
				searchAppName = '',
				searchAppType = '',
				ajaxData = {
					start : 0,
					limit : 10,
					needTotal : true
				};

			function insertData( data ){
				var apps = data.apps,
					len = apps.length,
					list = $('#list tr:gt(0)');

				list.find('td').text('')
				list.hide();

				for( var i = 0; i < len; i++ ){
					var td = list.eq(i).show().find('td');
					td.eq(0).text( apps[i].mcloudAppId );
					td.eq(1).text( apps[i].name )
					td.eq(2).text( apps[i].type );
				}
			}
			function getList(  ){
				ajaxData.start = index;
				ajaxData.name = $('#search-name').val();
				ajaxData.type = $('#search-type').val();
				if( index == 1 ){
					ajaxData.needTotal = true;
				}
				else{
					ajaxData.needTotal = false;
				}
				$.ajax({
					contentType : 'application/json;charset=UTF-8',
					url: '/freeflow/rest/application/get-list.json',
					type: 'post',
					dataType: 'json',
					data : JSON.stringify( ajaxData )
				})
				.done(function( data ) {
					console.log("success");
					console.log(data)
					if( data.success && data.data && data.data.apps ){
						theData = data.data;
						var apps = data.data.apps,
							count;
						if( data.data.count ){

						 	count = data.data.count%10 == 0 ? data.data.count/10 : Math.floor( data.data.count/10 ) + 1;
						}
						pageCount = count || pageCount;

						insertData( data.data );

						pageCtr.setPage( index, pageCount );
					}
					else{
						pageCtr.setPageInit( $('#page li') );
					}
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			}

			$('#search-btn').click(function(){
				getList();
			})

			//页码控制
			var pageCtr = {

				//页码、上一页、下一页的控制
				pageObj : (function(){
					var pageNumArray = [];
			        for( var i = 0; i < 10; i++ ){
			            pageNumArray[i] = {
			                num : null,
			                active : false
			            }
			        }
					return {
			            pre : false,
			            next : false,
			            pageNumArray : pageNumArray
			        }
				})(),

				//初始化页码对象
				initPageObj : function(){
					var pageObj = this.pageObj;
					pageObj.pre = false;
		            pageObj.next = false;
		            for( var i = 0; i < 10; i++ ){
		                pageObj.pageNumArray[i].num = null;
		                pageObj.pageNumArray[i].active = false;
		            }
				},

				//回复页码的初始设置
				setPageInit : function( pageLi ){
					$('#page-wrap').hide();
					$('#page li:gt(0):lt(10)').hide().removeClass('active');
		            pageLi.eq(0).removeClass('disabled');
		            pageLi.eq(11).removeClass('disabled');

		            this.initPageObj();
				},

				//根据pageObj来设置页码
				setPageNum : function(){
					$('#page-wrap').show();

					var pageLi = $('#page li'),
		            	pageA = $('#page a');

					var pageObj = this.pageObj;
					 for( var i = 0; i < 10; i++ ){
		            	if( pageObj.pageNumArray[i].num ){
							pageA.eq(i+1).text(pageObj.pageNumArray[i].num);
							pageLi.eq(i+1).show();
							if( pageObj.pageNumArray[i].active ){
								pageLi.eq(i+1).addClass('active');
							}
		            	}
		            	else{
		            		break;
		            	}
		            }

		            if( !pageObj.pre ){
		            	pageLi.eq(0).addClass('disabled');
		            	pageA.eq(0).off('click')
		            }
		            else{
		            	pageA.eq(0).off().click(function(){
		            		index--;
							getList(  );
		            	})
		            }
		            if( !pageObj.next ){
		            	pageLi.eq(11).addClass('disabled');
		            	pageA.eq(11).off('click')
		            }
		            else{
						pageA.eq(11).off().click(function(){
							index++;
							getList(  );
		            	})
		            }
				},
				clickPageNum : (function(){
					$('#page a:lt(11):gt(0)').click(function(){
			            	index = $(this).text();
			            	getList()
			        })
				})(),

				//根据页码和页数设置pageObj
				setPageObj : function( index, pageCount ){
					var start,end,active,pageObj = this.pageObj;

					//总页数大于10
		            if( pageCount > 10 ){

		                pageObj.pre = true;
		                pageObj.next = true;
		                if( index <= 5 ){
		                    start = 1
		                    end = pageCount

		                    for( var i = 0; i < 9; i++){
		                        pageObj.pageNumArray[i].num = start + i;
		                    }
		                    pageObj.pageNumArray[page].active = true;
		                    pageObj.pageNumArray[9].num = pageCount;

		                    if( index === 1){
		                        pageObj.pre = false;
		                    }
		                }

		                else if( pageCount - page >= 5 ){
		                    start = index - 3;
		                    end = index + 4;

		                    for( var i = 1; i < 9; i++){
		                        pageObj.pageNumArray[i].num = start + i - 1;
		                    }
		                    pageObj.pageNumArray[0].num = 1;
		                    pageObj.pageNumArray[4].active = true;
		                    pageObj.pageNumArray[9].num = pageCount;

		                }
		                else{
		                    if( pageCount == index ){
		                        pageObj.next = false;
		                    }

		                    start = pageCount - 8;
		                    end = pageCount;
		                    active = page - start + 1;

		                    for( var i = 1; i < 10; i++){
		                        pageObj.pageNumArray[i].num = start + i - 1;
		                    }
		                    pageObj.pageNumArray[0].num = 1;
		                    pageObj.pageNumArray[active].active = true;
		                }
		            }

		            //>0  <10
		            else if( pageCount > 0 ){
		                if( index > 1 ){
		                    pageObj.pre = true;
		                }
		                if( index < pageCount ){
		                    pageObj.next = true;
		                }

		                for( var i = 0; i < pageCount; i++ ){
		                    pageObj.pageNumArray[i].num = i + 1;
		                }

		                pageObj.pageNumArray[ index -1 ].active = true;
		            }
				},

				setPage : function ( index, pageCount ){
		            var pageLi = $('#page li'),
		            	pageA = $('#page a');

		            //回复页码的初始状态
		            this.setPageInit( pageLi );

		            this.setPageObj( index, pageCount );
					
					// 设置新的页码
		            this.setPageNum( );
           		},
			}
			return {
				getList : getList
			}
		})(),
		detailsCtr : function(){ 
			(function(listCtr){
				console.log(listCtr)
				var freeflowId,
					useScene = 1,
					AppMsg = {
						isOpenSms : false,
						isOpenPubacc : false,
						moduleList : [],
					};
					console.log(this)

				var toJson = function(a){//jquery 的 serializeArray 的返回值转 JSON
					var o = {};
					$.each(a,function(){
						o[this.name] = this.value || '';
					})
					return o;
				}
				
				function tipCover(){//提示信息层
					$('.tip').show();
					setTimeout(function(){
						$('.tip').hide();
					}, 2000)
				}
		
				// 新增模板验证提交
				$('.detail-create-module-form').validate({
					debug : true,
					messages : {
						title : {
							required : '请输入标题'
						},
						content : {
							required : '请输入内容'
						}
					},
					submitHandler : function(form){
						var data = {
							applicationId : freeflowId,
							useScene : useScene,
							title : $('.detail-create-module-form input[type="text"]').eq(useScene - 1).val(),
							content : $('.detail-create-module-form textarea').eq(useScene - 1).val(),
							msgType : $('.detail-create-module-form input[name="msgType"]:checked').eq(useScene - 1).val()
						}

						$.ajax({
							contentType : 'application/json;charset=UTF-8',
							url: '/freeflow/rest/application/msg-template/create.json',
							type: 'POST',
							dataType: 'json',
							data: JSON.stringify( data )
						})
						.done(function( data ) {
							console.log("success");
							if( data.success ){
								// showModule( useScene, $('.detail-module-form').eq(useScene - 1))
								getModuleList( freeflowId );

								tipCover('增加成功');

							}
							else{
								tipCover(data.errorMsg)
							}
						});
						return false;
					}
				})

				//修改模板验证提交
				$('.module-detail-wrap form').validate({
					debug : true,
					messages : {
						title : {
							required : '请输入标题'
						},
						content : {
							required : '请输入内容'
						}
					},
					submitHandler : function(form){
						var data = {
							applicationId : freeflowId,
							useScene : useScene,
							title : $('.detail-module-form input[type="text"]').eq(useScene - 1).val(),
							content : $('.detail-module-form textarea').eq(useScene - 1).val(),
							msgType : $('.detail-module-form input[type="radio"]:checked').eq(useScene - 1).val()
						}

						$.ajax({
							contentType : 'application/json;charset=UTF-8',
							url: '/freeflow/rest/application/msg-template/update.json',
							type: 'POST',
							dataType: 'json',
							data: JSON.stringify( data )
						})
						.done(function( data ) {
							console.log("success");
							if( data.success ){
								tipCover('增加成功');

								var form = $('.detail-module-form').parent().eq(useScene - 1).addClass('hide-animation');
								setTimeout(function(){
									form.find('.module-detail-ctrl,.module-update').show();
									form.find('.detail-module-form').hide();
									
								},500)
								setTimeout(function(){
									form.removeClass('hide-animation');
									
								},1000)

								showModule( useScene, $('.detail-module-form').eq(useScene - 1))


							}
							else{
								tipCover(data.errorMsg)
							}
						});
						return false;
					}
				})

				var upDataModule = function(){
					var getVal = function(){

					}
				}

				function insertModuleDetail( module ){
					var useScene = module.useScene,
						type = '',
						form = $('.module-detail-wrap').eq( useScene - 1 ),
						dd = form.find('dd');

					form.show();

					switch( module.msgType  ){
						case 1:
							type = 'pubaccmsg';
							break;
						case 2:
							type = '短信';
							break;
						case 3:
							type = '同时推送';
							break;
					}
					
					dd.eq(1).text( module.title );
					dd.eq(2).text( module.content );
					dd.eq(3).text( type );
				}

				function createModuleTabShow(){
					var list = AppMsg.moduleList,
						li = $('#module-tabs li'),
						form = $('.detail-create-module-form'),
						active = false;
					for( var i = 0; i < 4; i++ ){
						if( list[ i ] ){

							li.eq(i).removeClass('active').hide();
							form.eq(i).hide();
						}
						else{
							li.eq(i).show();
							if( !active ){
								li.eq(i).addClass('active');
								active = true;
								useScene = i + 1;
								form.eq(i).show();
							}
							else{
								li.eq(i).removeClass('active');
							}
						}
					}
				}

				//获取已经设置的模板
				function getModuleList( id ){
					var me = this;
					$.ajax({
						contentType : 'application/json;charset=UTF-8',
						url: '/freeflow/rest/application/msg-template/get-list.json',
						type: 'POST',
						dataType: 'json',
						data: JSON.stringify( {applicationId : id} )
					})
					.done(function( data ) {
						if( data.success && data.data ){
							var list = data.data,
								listLen = list.length;
								for( var i = 0; i < 4; i++ ){
									AppMsg.moduleList[ i ] = false;
								}
							if( listLen > 0 ){
								for( var i = 0; i < listLen; i++ ){

									//插入模板的详情
									insertModuleDetail( list[i] );

									//修改模板列表对象
									AppMsg.moduleList[ list[i].useScene - 1 ] = true;

									
								}
							}
							// 控制新建模板的tab
								createModuleTabShow();

						}
					});
				}

				//新增模板
				// function createModule( useScene, id, form){
				// 	var data = toJson(form.serializeArray());
				// 	data.applicationId = id;
				// 	data.useScene = useScene;
				// 	$.ajax({
				// 		contentType : 'application/json;charset=UTF-8',
				// 		url: '/freeflow/rest/application/create.json',
				// 		type: 'POST',
				// 		dataType: 'json',
				// 		data: JSON.stringify( data )
				// 	})
				// 	.done(function( data ) {
				// 		if( data.success ){
				// 			tipCover();
				// 		}
				// 		else{

				// 		}
				// 	});
				// }

				function showModule( index, form ){
					var data = {
						applicationId : freeflowId,
						useScene : index
					}
					$.ajax({
						contentType : 'application/json;charset=UTF-8',
						url: '/freeflow/rest/application/msg-template/get.json',
						type: 'post',
						dataType: 'json',
						data : JSON.stringify( data )
					})
					.done(function( data ) {
						if( data.success && data.data ){
							form.find('input[type="text"]').val(data.data.title);
							form.find('textarea').val(data.data.content);
							var type = data.data.msgType;
							if( type == 1 ){
								form.find('input[type="radio"]').eq(0)[0].checked = true;
							}
							else if(type == 2){
								form.find('input[type="radio"]').eq(1)[0].checked = true;
							}
							else if(type == 3){
								form.find('input[type="radio"]').eq(2)[0].checked = true;
							}

							insertModuleDetail( data.data )
						}
						console.log("success");
					});
				}

				!(function moduleTabCtrl( tabMsg ){//创建模板的tab控制
					$('#module-tabs li').click(function(){

						console.log(this)
						var index = $(this).index();
						useScene = index + 1;

						$('#module-tabs li').removeClass('active');
						$(this).addClass('active')
						$('.detail-create-module-form').hide().eq(index).show();
					})
				})( this.tabMsg );

				(function mainTabCtrl(){//主要tab的控制
					$('#main-tabs li').click(function(){

						console.log(this)
						var index = $(this).index();

						$('#main-tabs li').removeClass('active');
						$(this).addClass('active')
						$('.rabDiv').hide().eq(index).show();						

					})
				})()

				// 模板详情的修改事件
				!(function(){
					$('.module-update').click(function(){
						var index = $(this).index('.module-detail-wrap .module-update') + 1;
						useScene = index;
						showModule( index, $('.detail-module-form').eq(index-1) );
						var form = $(this).parent().addClass('hide-animation');
						setTimeout(function(){
							form.find('.module-detail-ctrl,.module-update').hide();
							form.find('.detail-module-form').show();
							
						},500)
						setTimeout(function(){
							form.removeClass('hide-animation');
							
						},1000)
					})
				})()

				// var moduleOpenCtrl = (function(){
				// 	var open = false;
				// 	$('#module-open').click(function(){
				// 		if( open ){
				// 			open = false;
				// 			$('#module-tabs,#details-module-form').hide();
				// 			$('#module-open span').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
				// 		}
				// 		else{
				// 			open = true;
				// 			$('#module-tabs,#details-module-form').show();
				// 			$('#module-open span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
				// 		}	
				// 	})
				// })()

				function msgTypeCtrl( data ){
					var openSms = data.openSms || data.isOpenSms,
						openPubacc = data.openPubacc || data.isOpenPubacc;
					

					if( !openSms && !openPubacc ){
						$('#main-tabs li').eq(1).hide();
						
					}
					else if( !openSms ){
						$('#main-tabs li').eq(1).show();
						$('.pull-way input').parent().parent().hide();
						$('.pull-way input[value="1"]').parent().parent().show();

						$.each( $('.pull-way input[value="1"]'),function(index, ele){
							console.log(ele)
							ele.checked = true;
						} )
						
						getModuleList( freeflowId );
					}
					else if( !openPubacc ){
						$('#main-tabs li').eq(1).show();
						$('.pull-way input').parent().parent().hide();
						$('.pull-way input[value="2"]').parent().parent().show();

						$.each( $('.pull-way input[value="2"]'),function(index, ele){
							console.log(ele)
							ele.checked = true;
						} )

						getModuleList( freeflowId );
					}
					else{
						$('#main-tabs li').eq(1).show();
						$('.pull-way input').parent().parent().show();

						$.each( $('.pull-way input[value="1"]'),function(index, ele){
							ele.checked = true;
						} )

						getModuleList( freeflowId );
					}
					
					
				}

				function bindBtnClick(){//绑定按钮事件
					
					// 绑定详情事件
					$('#list tr:gt(0)').click(function(){
						var index = $(this).index() - 1;
						freeflowId = theData.apps[index].id;
						msgTypeCtrl( theData.apps[index] );

						getDetails();
						$('#details-cover').show();
						
					})

					$('#submit-btn').click(function(){
						var data = toJson($('#details-form').serializeArray());
						data.id = freeflowId;
						if( data.isOpenPubacc == 'true' ){
							data.isOpenPubacc = true;
						}
						else{
							data.isOpenPubacc = false;
						}
						if( data.isOpenSms == 'true' ){
							data.isOpenSms = true;
						}
						else{
							data.isOpenSms = false;
						}

						msgTypeCtrl( data )
						$.ajax({
							contentType : 'application/json;charset=UTF-8',
							url: '/freeflow/rest/application/update.json',
							type: 'POST',
							dataType: 'json',
							data: JSON.stringify( data )
						})
						.done(function( data ) {
							if( data.success ){
								tipCover();
								listCtr.getList();
							}
							else{

							}
						});
						return false;
					})

					$('.close').click(function(){
						$('#details-cover').hide();

						$('.module-detail-ctrl,.module-update').show();
						$('.detail-module-form').hide();
						$('.module-detail-wrap').removeClass('hide-animation').hide();

						$('#main-tabs li').eq(0).click();
					})
				}

				function insertInputVal( obj, formEle ){
					var inputEle = formEle.find( 'input[type!="radio"],textarea');
					$.each( inputEle, function( index, val){
						var $val = $(val);
						var inputName = $val.attr('name');
						$val.val(obj[inputName] || '')
					} )
				}
				function insertSelectVal( obj, formEle ){
					var selectEle = formEle.find( 'select');
					$.each( selectEle, function( index, val){
						var $val = $(val);
						var inputName = $val.attr('name');
						$val.find('option[value = '+obj[inputName] +' ]').attr('selected', true)
					} )
				}
				function insertRadioVal( obj, formEle ){
					var radioEle = formEle.find( 'input[type = "radio"]');
					$.each( radioEle, function( index, val){
						var $val = $(val);
						var inputName = 'o' + $val.attr('name').substr(3);
						if( obj[inputName].toString() == $val.attr('value') ){
							$val[0].checked = true;
						}
					} )
				}
				function insertForm( obj, formEle ){
					insertInputVal( obj, formEle );
					insertSelectVal( obj, formEle );
					insertRadioVal( obj, formEle );
				}

				function getDetails(){
					$.ajax({
						contentType : 'application/json;charset=UTF-8',
						url: '/freeflow/rest/application/get.json',
						type: 'post',
						dataType: 'json',
						data : JSON.stringify( {id:freeflowId} )
					})
					.done(function( data ) {
						if( data.success && data.data ){
							insertForm( data.data, $('#details-form') );
						}
						console.log("success");
					});
				}

				bindBtnClick();
			})(this.listCtr);
		},
		init : function(){
			this.listCtr.getList();
			this.detailsCtr();
		}
	}

	ffObj.init();
})