(function (window, $, undefined) {

	var strategies = {

		isNotEmpty: function(value, errorMsg) {
			if(/[^\s]+/g.test(value)) {
				return errorMsg;
			}
		},

		isNumber: function(value, errorMsg) {
			if(!/\d+/g.test(value)) {
				return errorMsg;
			}
		}
	};

	var Validator = function() {
		this.cache = [];
	};

	Validator.prototype.add = function(dom, rules) {

		var me = this;

		for(var i = 0, rule; rule = rules[i++]; ) {
			(function(rule) {
				var strategyAry = rule.strategy.split(':');
				var errorMsg = rule.errorMsg

				me.cache.push(function() {
					var strategy = strategyAry.shift();
					strategyAry.unshift(dom.value || dom.innerHTML);
					strategyAry.push(errorMsg);
					return strategies[strategy].apply(dom, strategyAry);
				});
			})(rule)
		}
		
	};

	Validator.prototype.start = function() {
		for(var i = 0, validatorFunc; validatorFunc = this.cache[i++]; ) {
			var errorMsg = validatorFunc();
			if(errorMsg) {
				return errorMsg;
			}
		}
	};


	window.Validator = window.Validator || Validator;
})(window, Zepto);