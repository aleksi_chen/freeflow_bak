/* 
 * 发起请假模块
 * @author muqin_deng
 * @time 2015/03/16
 */
(function (window, $, undefined) {
	var processDetail = {
		data: {
			formId: Util.getUrlValue('formId'),
			submitChooseUrl: '/freeflow/rest/approvalrecord/update.json',
			getProcessFormUrl: '/freeflow/rest/form/get.json',
			getReplyListUrl: '/freeflow/rest/approvalrecord/get-list.json',
			transferApprovalOId: '',	//需要转审批的下一个审批人
			loading: false,
			detailData: null 	//流程详情数据
		},
		
		init: function () {
			XuntongJSBridge && XuntongJSBridge.call('hideOptionMenu');	//隐藏云之家右上角按钮
			Util.loading();
			//初始化页面
			this.initPage().done(function () {
				//初始化布局位置
				// this.initPosition();
				//绑定事件
				this.bindUI();
				this.takenotes();
			}.bind(this)).fail(function () {
				$('.process-detail-wrap').html('<p class="load-error">加载数据出错，点击<a href="">刷新重试</a></p>');
			}).always(function () {
				Util.loading(false);
			});
		},
		
		//初始化埋点
		takenotes: function () {
			var type = Util.getUrlValue('type');
			
			if (!type)  {return ;}
			
			if (type == 'request') {
				takenotes('_', '_', '8460', '我的申请列表点进详情页', '0');
			} else if (type == 'process') {
				takenotes('_', '_', '8461', '我的审批列表点进详情页', '0');
			}
		},
		
		//初始化布局位置
		initPosition: function () {
			$('.reply-list').css({
				'margin-top': $('.top').height() + 'px'
			});
		},

		initPage: function () {
			var deferred = $.Deferred();

			//拉取初始化数据
			$.when(this.getProcessFormPost({'id': this.data.formId}), this.getReplyListPost({'formId': this.data.formId}))
			 .done(function (formData, listData) {
			 	if (formData && listData.approvalRecords && listData.approvalRecords.length) {
			 		//渲染页面
				 	this.renderPage(formData, listData.approvalRecords);
				 	deferred.resolve();
			 	} else {
			 		deferred.reject(1);
			 	}
			 	
			 }.bind(this))
			 .fail(function () {
			 	deferred.reject(2);
			 });

			 return deferred.promise();
		},

		//初始化渲染页面
		renderPage: function (formData, listData) {
			var tplDom = document.getElementById('template'),
				tpl = tplDom.innerHTML,
				currUserId = __processData.currUserId,
				renderData = {
					form: formData,
					replyList: listData,
					currUserId: currUserId,									//当前登陆用户id
					isMyProcess: currUserId == formData.creatorUserId,		//当前登陆用户是否是申请人
					isApprover: false/*currUserId == formData.lastApproverUserId*/	//当前登陆用户是否是当前审批人
				};

			renderData.form.id = this.data.formId;
			
			/*if (listData.length && listData[listData.length - 1].approverUserId == currUserId) {

			    renderData.isApprover = true;
			}*/
			if(listData.length){
				$.each(listData, function(index, val){
					if(val.approverStatus == 1 && val.approverUserId == currUserId){
						renderData.isApprover = true;
						return false;
					}
				})
			}
				

			this.data.detailData = renderData;

			tplDom.outerHTML = Util.templateEngine(tpl, renderData);
		},
		
		//绑定事件
		bindUI: function () {
			var me = this;
			
			//（1.审批中 2.同意 3.驳回）
			$('.group-btn').on('tap', '.btn-agree', function (e) {
				me.doMyChoose(2);
			})
			.on('tap', '.btn-disagree', function () {
				me.doMyChoose(3);
				//埋点
				takenotes('_', '_', '8454', '不同意的点击次数', '0');
			})
			//知会
			.on('tap', '.btn-share', function () {
				me.shareProcess();
			});
			
			//查看用户详情（js桥）
			$('.reply-list').on('tap', '.view-profile', function () {
				//点击查看用户详情埋点
				takenotes('_', '_', '8455', '点击查看用户详情', '0');
				
				var openId = $(this).data('openid');
				XuntongJSBridge.call('personInfo', {'openId': openId}, function (result) {});
			})
			//点击添加批复
			.on('tap', '.can-reply', function () {
				me.showReplyDialog($(this).find('.reply-content'));
			});

		},
		
		//点击同意或不同意执行
		//status（1.审批中 2.同意 3.驳回）
		doMyChoose: function (status) {
			if (this.data.loading === true) {
				return ;
			}

			//同意的情况
			if (status == 2) {
				this.showAddApprDialog(status);
				return ;
			}
			
			//不同意
			this.handlerChoose(status);
		},
		
		handlerChoose: function (status) {
			var data = this.getData(status);
			
			this.submitMyChoosePost(data)
				.done(function () {
					location.reload();
				})
				.fail(function () {
					Util.showTips('Oh No，出错了~');
				});
		},
		
		//显示添加批复弹窗
		showReplyDialog: function ($replyCont) {
			var $overLayer = $('.overlayer'),
				$replyDialog = $('.reply-dialog'),
				content = $replyCont.data('content'),
				$replyInput = $replyDialog.find('.reply-input');

			if (!$replyCont || !$replyCont.length) {
				return ;
			}
			
			$replyDialog.fadeIn();

			if (Util.isSupperScale()) {
				$overLayer.fadeIn();
			} else {
				$overLayer.show();
			}

			Util.delay(500).done(function () {
				$replyInput.focus();
			});
			
			//重新编辑批复
			$replyInput.val(content ? content : '');
			
			if (this.showReplyDialog.isBind === true) {
				return ;
			}
			
			var cancelHandler = function () {
				$overLayer.hide();
				$replyDialog.hide();
				$replyInput.blur();
			};
			
			$replyDialog.on('tap', '.btn-yes', function () {
				var inputCont = $replyInput.val().trim();
				
				$replyCont.data('content', inputCont).html(inputCont ? inputCont : '点击添加批复');
				
				cancelHandler();
			})
			.on('tap', '.btn-no', cancelHandler);
			
			this.showReplyDialog.isBind = true;
		},
		
		//显示转审批弹窗
		showAddApprDialog: function (status) {
			var me = this,
				$overLayer = $('.overlayer'),
				$apprDialog = $('.add-approver-dialog');
			
			$apprDialog.fadeIn();
			if (Util.isSupperScale()) {
				$overLayer.fadeIn();
			} else {
				$overLayer.show();
			}
			
			if (this.showAddApprDialog.isBind === true) {
				return ;
			}
			
			var cancelHandler =  function () {
				$overLayer.hide();
				$apprDialog.hide();
				me.handlerChoose(status);
			};
			
			$apprDialog.on('tap', '.btn-add', function () {
				//调用js桥选人
				me.selectUserByApp().then(function (openId) {
					me.data.transferApprovalOId = openId;
					cancelHandler();
					//转审批行为埋点
					takenotes('_', '_', '8458', '转审批行为', '0');
				}, function () {
					Util.showTips('未选择审批人');
				});
			})
			.on('tap', '.btn-finish', cancelHandler);
			
			this.showAddApprDialog.isBind = true;
		},
		
		//选择转审批人
		selectUserByApp: function () {
			var deferred = $.Deferred(),
				param = {
					'isMulti': false,	//是否多选 默认false
					'blacklist': []	//黑名单，排除人员的openId
					//'whitelist': []	//白名单，包含人员的openId(默认选中)
				};
			
			XuntongJSBridge.call('selectPersons', param, function (resp) {
				if (resp.success === true || resp.success == 'true') {
					var person = resp.data.persons[0];
					deferred.resolve(person.openId);
				} else {
					deferred.reject();
				}
			});
			
			return deferred.promise();
		},
		
		//请假知会
		shareProcess: function () {
			var data = this.data.detailData,
				title = data.form.creatorUserName + '的审批知会【' + (data.form.title.length > 5 ? data.form.title.substr(0, 4) : data.form.title) + '】',
				content = data.form.content,//this.getShareContent(data),
				params = ['formId=' + this.data.formId, 'applicationId=' + __processData.applicationId].join('&'),
				webpageUrl = Util.getRootPath(true) + 'c/form/detail.json?' + params,
				thumbData = 'iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAIAAAD/gAIDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAcGSURBVHja7NxriFxXHQDw//+c+5rXnZnNbjb7mC100sTS+EipICiCIRDq+qG2XypCwUoLviAVaz/YQptVa9SUFr+IItH2gxB8VcTWYhEk0goVurGhMZpks5PurLvZzGNn7vucvx8mKe5mN81072zuTs6fZT/M7hzm/uY8/ufccy7uPbEAKq4vmCJQWApLYSkshaWwFIHCUlgKS2EpLIWlCBSWwlJYCqs/Q+tFoSFRIEHcoEviAAYDHXELYHmSyiY/ULR2mhpuuhQB/MePXq55Z3yRYphoLFfSgYL5xGhum34DG7h5/0Bq6p3lVxp+vF5xXlJAULa0p8ZvrBQAwKDODpVyZUsLCBKLRZMFM88TMWjkOZssmAFRQrEkQdnSIDFRtjSZ2JoFAAgJitg/TMxYlCQsSjiWyuBVKKwkY9GWKnbz5oZXp/WPV5ZPumEv5msh0Z6UPlXKxT65uTFYC6H8bc0jANaDeiABT3viazsyt5i8H7BGDPbgUOotJ9J78N1HBHektRGD9UkzNBAfH8upDl6NhioUlsJSWAqrD2KT1urqkawJ6kWKTQBFjgWtX/KsWiQfOFM/60d6DxYHQ6BbTe35cqHYe6/NwGoLOusJl8jvQeES6Iwn2oKKWl/UrFGTf2cid9qNtN5Md3altNHeTww3CYsB3FO0oKhGQ5U6qFBYCkthKSw13bm++Ec7PO+LrvIsQbBDZx/LGXhTYc0H4uGz9aVI8m7u7kgii+Hvdg/sTsxmk01JShFNhgy7bPMIBiJffzpZF/JSSC0pJYCOmOM4rDMTcWtjbdfZz8uFuUB0W7OGdL7TWj2PWQzlq03/b8vBjC8WQ7kspSAwEAsa7tD57Sltv21+NKtbDNkW7bN2WdquDbemi5F8YdF9seZWQ9nZeNX5RQARgRNQNZBvtsNfXXL3pPQvbk8LinlRKEF7z64df274R6qtM57oNOdBne22tN0pbVBjOqIj5VwoTzrhOV84kqad8LHZ5qjOMhxvOqyfLTjPzbciAoaw0+L3DaT2580xna+6Yx8QnXCiP9S8l+teQ1AlkPGuc2wBrJ8uOM9UWwxAZ/j5bamHhtPFdbatGoh3ZfS7Mvq9A9aRavv1VsAxzg4/6UnpH+v+s/MtBMhw/G4p983RbPE6Nvh+KK3/5Nb8/dtSroxzB26isSqBODzXkgQGwlTJnixY1/9eE/HJ8dx9A5Yj6KbA+vF/nblAEMHDw9kDebPbtyPAE2O5O7O6G9Om5eRinXKjP9V9hrA3oz84lHp/hVgMHx3JphjK/sZ6qe7VheSIXxhKW11uVHvbjV6seSERANyZ0ffZph9H5UooVkDw12aAADtN/gnb6Oq900741ZnGwfONY0te55V7BiwNY9hIl1Csc35UDSUA7M3o6W6q1XQ7emSmWfGjImcjxuWp0gfT+qihbbyjTyjWrC8cSQxhT1pfMWEEeNuN1rvqE0508HyjEogcZ1Pj9r4rVdLmWLZ4uOEsIqFYS5H0JCHA2MoDZj+ab997+tL35lq0ptRMoxJENsdD4/Zk0fz/YXFIY31bs3xJAggB0isndxcC4Ug6uuis8pp2ooMzjdkgsjlbJfXusNi3fZbBkAESgLdyFPvWWO7ugiWJji46T79z2Wu6HT5yRerbpdzVUh197FesImedulANVmRIBY6HJ+z9eYuInr/oPFNtvdEKvjHbrARRnrOpkv3ptbJ8AliK5MYXIBKKVTJ5moEkeMuNVv0pz/HwhL0vb0mCFy66X5lpXgiEzdmhkj1ZWDvLbwk664mNH1lIKFbZ5Nt1DgBvtgP/qlEsz/H7E/a+vOEIagqZZvjU+lKdHLUSiL6tWRbDj2cNAviXJ/7eCq/+hzzHH96S/+yANaixQyX7M4VrzRx/X/MCSl6fFWNxdxesLMeA6OjC2gsHWYZPT9i/3jUweU2pU270SsMz4zjZEycWAtQiGVdpH85on7JNInh9Ofjlkrvep99+zZP+AuAH1VZTEE9YVQAN4S/NIMYCvzycHtQYIDw333ptrcb4nnFkrnV8OUjHdGCMxdvRHF8Ojq1TC95H3GZpXx/NMgBH0GOzjePLXXwTBPDsfOsXi04qvoVlPvKlR2NshgTwWitoShjQWI5jRLDBnw9YWkPSSTfyJLza9BHwjrSmvdf1n/PFkxdax5Zcg2GMxxAx9kecSwKfKM/juQ3V+QI6XaEgCIg+ktE/ty31SdsorLUY/28veqnu/+aSOx/KFIv59jT26HnwgmI7iIkA77ITQGcZb9zge9L67SltWGc6YlvKSiD/6YSn3GgpkiZiLzb74lZ8eD4BREQRgSCQV0AZAgfQesN0eQSDLRgIoCPqm74XSW1mU1gKS2EpLIWlQmEpLIWlsBSWwlKhsBSWwlJYCqtP438DAMt/9URmsTrNAAAAAElFTkSuQmCC';
			 
			var config = {
				'shareType': '4',
				'appId': __processData.pubaccId,	//公共号id
				'lightAppId': __processData.appId,	//轻应用id 
				'appName': '流程审批',
				'thumbData': thumbData,		//应用logo，Base64格式
				'theme': title,			//主题（可选），如传入，创建组时以此命名组名称
				'title': title,			//分享弹窗时的标题
				'content': content,		//分享弹窗界面内容
				'cellContent': content,	//聊天界面显示的内容
				'webpageUrl': webpageUrl,	//分享内容的跳转链接
				'sharedObject': 'all'	//分享的对象：all（所有），group（组），person（人）
			};
			
			//埋点
			//如果是审批人
			if (data.isApprover) {
				takenotes('_', '_', '8456', '审批人点击知会', '0');
			} else {
				takenotes('_', '_', '8457', '非审批人点击知会', '0');
			}
						
			//调用云之家app分享接口
			XuntongJSBridge.call('share', config, function (resp) {
				
			});
		},
		
		//获取分享的内容
		// getShareContent: function (data) {
		// 	return data.form.title;
		// },
		
		getData: function (status) {
			var comment = $('.reply-content').data('content');

			if (comment) {
				comment = Util.escapeToHtmlEntity(comment.trim()).replace(/\n|\r/g, '<br>');
			}

			return {
				formId: this.data.formId,
				transferApprovalUserId: this.data.transferApprovalOId,	//下一个审批人
				approverStatus: status,							//审批状态
				approverUserId: __processData.currUserId,		//审批人id
				comment: comment,
				networkId: __processData.networkId,
				formTitle: this.data.detailData.form.title,		//表单标题
				formCreatorUserId: this.data.detailData.form.creatorUserId,	//表单创建者userId
				applicationId: __processData.applicationId,
				id: $('.can-reply').last().data('approverid')			//当前审批记录的唯一id
			};
		},
		
		//获取审批单详情
		getProcessFormPost: function (data) {
			return this.requestService({
				url: this.data.getProcessFormUrl,
				data: data
			});
		},

		//获取审批流程详情
		getReplyListPost: function (data) {
			return this.requestService({
				url: this.data.getReplyListUrl,
				data: data
			});

		},
		
		//提交选择
		submitMyChoosePost: function (data) {
			Util.loading();
			this.data.loading = true;

			return this.requestService({
				url: this.data.submitChooseUrl,
				data: data
			})
			.always(function () {
				Util.loading(false);
				this.data.loading = false;
			}.bind(this));
		},

		requestService: function (obj) {
			var deferred = $.Deferred();

			$.ajax({
				url: obj.url,
				type: 'POST',
				dataType:'json',
				contentType: 'application/json;charset=UTF-8',
				data: JSON.stringify(obj.data)
			})
			.done(function (resp) {
				if (resp.success === true) {
					deferred.resolve(resp.data);
				} else {
					deferred.reject(resp);
				}
			})
			.fail(function (resp) {
				deferred.reject(resp);
			});

			return deferred.promise();
		}
	
	};
	
	window.processDetail = window.processDetail || processDetail;
})(window, Zepto);