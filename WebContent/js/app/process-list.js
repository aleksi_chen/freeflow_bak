(function (window, $, undefined) {
	var processList = {
		data: {
			myrequestUrl: '/freeflow/rest/form/get-list.json',		
			myprocessUrl: '/freeflow/rest/approvalrecord/myapproval-list.json',
			jumpUrl: '/freeflow/c/form/process-request.json',
			myrequestPageIndex: 1,
			myprocessPageIndex: 1,
			pageIndex: 1,
			pageSize: 10,
			isFirstSwitch: true,
			needApprovalTotal: true,
			requestNoData: false,
			processNoData: false,
			currType: 'request'
		},

		init: function() {
			XuntongJSBridge && XuntongJSBridge.call('hideOptionMenu');	//隐藏云之家右上角按钮
			Util.loading();
			this.initList('request');
			this.initMoreProcess();
			this.switchTap();	
			this.jumpEvt();	
		},

		initList: function(type) {
			var me = this;

			var data = me.getAjaxData(type);

			this.getMyrequest(data).done(function(resp) {
				if(type == 'request') {
				    me.data.myrequestPageIndex++;
				    me.data.needApprovalTotal = false;

				    if(resp.approvalTotal !== 0) {
				    	$('.list-wrap').css('marginTop', '6.2rem');
				    	$('.list-nav').show();
				    }
				} else {
					me.data.myprocessPageIndex++;
					me.data.isFirstSwitch = false;
				}

				if (resp.details.length === 0) {
					me.onInitNoData(type);
					return ;
				}
				
				if (resp.details.length < me.data.pageSize) {
					if (type == 'request') {
						me.data.requestNoData = true;
					} else {
						me.data.processNoData = true;
					}
					
					// me.initMoreProcess(type);
				}

				me.renderUI(resp.details, type);
				me.bindEvt();
			}).fail(function() {
				me.onInitNoData(type);
			}).always(function() {
				Util.loading(false);
			});
		},

		renderUI: function(data, type) {
			for(var i = 0, len = data.length; i < len; i++) {
				var statusText = '',
				    time = '',
				    overprintCls = '',
				    lastApproverUserName = '',
				    listTitle = '',
				    approverUserId = '',
				    approvalName = '';

				time = new Date(data[i].createTime).format('MM/dd hh:mm'); 
				if(type === 'request') {
					if(data[i].lastApproverUserId === currUserId) {
						lastApproverUserName = '我';
					} else {
						lastApproverUserName = data[i].lastApproverUserName;
					}

					if(data[i].status === 1) { //审批中
						statusText = '等待' + lastApproverUserName + '审批';
					} else if(data[i].status === 3){ //驳回
						statusText = lastApproverUserName + '驳回';
						overprintCls = 'disagree-overprint';
					} else {
						statusText = '审批完成';
						overprintCls = 'agree-overprint';
					}
					listTitle = data[i].title;
				} else {
					if(data[i].status === 1) { //审批中
						statusText = '等待我审批';
					} else if(data[i].status === 3){ //驳回
						statusText = '我已驳回';
						overprintCls = 'disagree-overprint';
					} else {
						statusText = '我已同意';
						overprintCls = 'agree-overprint';
					}
					approvalName = (data[i].approverUserId === approverUserId) ? '我' : data[i].formCreatorUserName;
					listTitle = approvalName + '的申请【' + this.InterceptStr(data[i].title, 20) + '】';
				}
		
				var delayTime = Math.sqrt(i) * 2 * 100,
					itemStyle = '-webkit-animation-delay:' + (delayTime || 1) + 'ms;'	
							  + 'animation-delay:'+ (delayTime || 1) + 'ms;';

				var tpl = '<li class="list-item leftin" style="' 
								+ itemStyle
				                + '" data-formId=' + data[i].formId + '>'
								+ '<p class="list-content">' + listTitle +'</p>'
								+ '<p class="request-time">' + time +'</p>'
								+ '<p class="list-process-state">' + statusText + '</p>'
								+ '<div class="overprint ' + overprintCls + '"></div>'
						   +'</li>';
			    if(type === 'request') {
			    	$('.list-wrap ul').eq(0).append(tpl);
			    } else {
			    	$('.list-wrap ul').eq(1).append(tpl);
			    }
			}
		},

		bindEvt: function() {
			$('.list-wrap ul').on('tap', 'li', function(e){
				var formId = $(this).data('formid'),
				    type = '';
				if($(this).closest('.myrequest-wrap').length !== 0) {
					type = 'request';
				} else {
					type = 'process';
				}

				window.location.href = '/freeflow/c/form/detail.json?formId=' + formId + '&type=' + type;
			});
		},

		//初始化假单数据为空
		onInitNoData: function (type) {
			if(type === 'request') {
				$('.request-tips .no-process').show();
			} else {
				$('.process-tips .no-process').show();
			}
		},
		
		//初始化滚动加载更多
		initMoreProcess: function () {
			var me = this;
			
			$(document, 'body').on('scroll', Util.throttle(function () {
					if (me.data.currType == 'request' && me.data.requestNoData) {
						return ;
					}
					if (me.data.currType == 'process' && me.data.processNoData) {
						return ;
					}

					var scrollTop = document.body.scrollTop || document.documentElement.scrollTop,
						windowHeight = $(window).height(),
						bodyHeight = $('body').height();
						
					//滚动到底部
					if (bodyHeight - windowHeight - scrollTop < 20) {
						me.getMoreProcess(me.data.currType);
					}
				}, 300)
			);
		},

		getMoreProcess: function (type) {
			var me = this;
			
			this.getMoreLoading();
			//加载更多数据
			var data = me.getAjaxData(type);
			this.getMyrequest(data).done(function (data) {
					if(type == 'request') {
					    me.data.myrequestPageIndex++;
					} else {
						me.data.myprocessPageIndex++;
					}
				
					me.renderUI(data.details, type);
					if (data.details.length < me.data.pageSize) {
						me.noMoreProcess(type);
					}
				})
				.fail(function () {
					me.getMoreFail(type);
				})
				.always(function () {
					me.getMoreLoading(false);
				});
		},
		
		//加载更多时的loading
		getMoreLoading: function (show) {
			var $loadMore = $('.load-more');
			
			if (show === false) {
				$loadMore.hide();
				return;
			}
			
			$('.load-more-fail').hide();	//隐藏加载更多错误提示
			$loadMore.show();
		},
		
		getMoreFail: function (type) {
			if(type === 'request') {
				$('.request-tips .load-more-fail').show();
			} else {
				$('.process-tips .load-more-fail').show();
			}
		},
		
		noMoreProcess: function (type) {
			// $(document, 'body').off('scroll');

			if(type === 'request') {
				$('.request-tips .no-more-process').show();
				this.data.requestNoData = true;
			} else {
				$('.process-tips .no-more-process').show();
				this.data.processNoData = true;
			}
		},

		getMyrequest: function(data) {
			var me = this;
			var deferred = $.Deferred();
			
			$.ajax({
				type: 'POST',
				contentType: 'application/json; utf-8',
				dataType: 'json',
				url: data.url,
				data: JSON.stringify(data.param)
			})
			.done(function (resp) {
				if (resp.success === true) {
					deferred.resolve(resp.data);
				} else {
					deferred.reject(resp);
				}
			})
			.fail(function (resp) {
				deferred.reject(resp);
			});
			
			return deferred.promise();
		},

		getAjaxData: function(type) {
			var	data = {};

			if(type === 'request') {
				data.param = {
					start: this.data.myrequestPageIndex,
					limit: this.data.pageSize,
					needApprovalTotal: this.data.needApprovalTotal
				};
				data.url = this.data.myrequestUrl;
			} else {
				data.param = {
					start: this.data.myprocessPageIndex,
					limit: this.data.pageSize,
					needTotal: false
				};
				data.url = this.data.myprocessUrl;
			}

			return data;
		},

		switchTap: function() {
			var me = this;

			$('.list-nav button').tap(function() {
				if($(this).hasClass('selected')) {
					return;
				}
				$('.selected').removeClass('selected');
				$(this).addClass('selected');

				var index = $(this).index();
				$('.list-wrap ul').css('display', 'none');
				$('.list-wrap ul').eq(index).css('display', 'block');
				if(index === 0) {
					$('.request-tips').show();
					$('.process-tips').hide();
					// if(me.data.requestNoData === false || me.data.processNoData === true) {
					// 	// me.initMoreProcess('request');
					// } 
					me.data.currType = 'request';
				} else {
					$('.request-tips').hide();
					$('.process-tips').show();
					// if(me.data.requestNoData === true || me.data.processNoData === false) {
					// 	// me.initMoreProcess('process');
					// }
					me.data.currType = 'process';
				}

				if(me.data.isFirstSwitch) {
					me.initList('process');
				}
			})
		},

		jumpEvt: function() {
			var me = this;

			$('.request-btn').tap(function() {
				window.location.href = me.data.jumpUrl;
			});
		},

		InterceptStr: function(str, len) {
			for(var i=str.length, n=0; i--; ){
			        n += str.charCodeAt(i) > 255 ? 2 : 1;
			}
			if(n <= len) return str;
			return str.replace(/.{2}$/,'...');
		}

	
	};

	window.processList = window.processList || processList;
})(window, Zepto);