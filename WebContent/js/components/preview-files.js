/**
 * 图片、文件预览组件
 * 依赖 Preview
 * 依赖 Mtouch
 * 继承 Widget	
 * @author muqin_deng@kingdee.com
 * @time 2015/12/21
 */
;(function (global, Preview, mTouch, factoryFn) {
    var factory = factoryFn(global, Preview, mTouch);
    
    global.PreviewFiles = global.PreviewFiles || factory;
    
})(window, Preview, mTouch, function (global, Preview, mTouch) {
    
    function isMobile () {
        var regObj = {
            'ios': /\(i[^;]+;( U;)? CPU.+Mac OS X/,
            'android': /Android/i,
            'wp': /Windows\s+Phone/
        };
        
        for (var key in regObj) {
            if (regObj.hasOwnProperty(key) && regObj[key].test(navigator.userAgent)) {
                return true;
            }
        }
        return false;
    }
    
    function isCloudHub () {
        return /cloudhub 10204/.test(navigator.userAgent);
    }
    
    // 主函数
    function PreviewFiles(cfg) {
        Widget.call(this);
        
        return this.render(cfg);
    }
    
    PreviewFiles.prototype = Widget.extend({
        constructor: PreviewFiles,

        // 初始化配置项
        initCfg: function (cfg) {
            this.cfg = this._$.extend(true, {
                'imgCfg': {
                    'target': '',
                    'previewCont': '',
                    'picSelector': '',
                    'imgUrlArrs': [],
                    'autoBindUI': false,
                    'hash': undefined
                },
                'fileCfg': {
                    'target': '',
                    'fileSelector': ''
                }
            }, cfg);
        },

        // 绑定事件
        bindUI: function () {
            var imgCfg = this.get('imgCfg');
            
            if (imgCfg && imgCfg.target) {
                this._bindImgPreview(imgCfg);
            }
            
            var fileCfg = this.get('fileCfg');
            
            if (fileCfg && fileCfg.fileSelector) {
                this._bindFilePreview(fileCfg.target, fileCfg.fileSelector);
            }
        },
        
        _bindImgPreview: function (imgCfg) {
            var imgPreview = new Preview(imgCfg);
            this.set('imgPreveiwInstance', imgPreview);
        },
        
        _bindFilePreview: function (target, fileSelector) {
            var $ = this._$;
            var me = this;
            
            mTouch(target || 'body').on('tap', fileSelector, function () {
                var fileInfo = me.getFileInfo(this);
                
                if (isMobile()) {
                    me.previewFile(fileInfo);
                } else {
                    me.downLoadFile(fileInfo);
                }
            });
        },
        
        previewFile: function (fileInfo) {
            XuntongJSBridge && XuntongJSBridge.call('showFile', fileInfo, function () {});
        },
        
        downLoadFile: function (fileInfo) {
            if (isCloudHub()) {
                XuntongJSBridge && XuntongJSBridge.call('downloadFile', fileInfo, function () {

                });

            } else {
                var downLoadUrl = '/microblog/filesvr/' + fileInfo.fileId + '?orginal';
                var iframe = document.createElement('iframe');
                iframe.style.display = 'none';
                document.querySelector('body').appendChild(iframe);
                iframe.src = downLoadUrl;
            }
        },
        
        getFileInfo: function (elem) {
            var $elem = this._$(elem);
            var fileId = $elem.data('fileid');
            var fileName = $elem.data('filename');
            var fileExt = $elem.data('fileext');
            var fileSize = $elem.data('filesize');
            
            return {
                'fileId': fileId,
                'fileName': fileName,
                'fileExt': fileExt,
                'fileSize': fileSize,
                'fileTime': ''
            };
        },
        
        imgPreview: function (index, needUpdate) {
            this.get('imgPreveiwInstance').preview(index, needUpdate);
        },
        
        getImgUrlArrs: function () {
            return this.get('imgPreveiwInstance').getImgUrlArrs();
        },
        
        setImgUrlArrs: function (arrs) {
            return this.get('imgPreveiwInstance').setImgUrlArrs(arrs);
        },

        ready: function () {

            this.fire('ready');
        },

    });
    
    return PreviewFiles;
});