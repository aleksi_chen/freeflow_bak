;(function(window, $, undefined) {
    var defaultSettings = {
        isNeedKnowMore: false,

        konwMoreURL: '', //了解更多跳转URL

        picUrl: '', //弹出框背景图片

        title: '', //标题

        content: [], //内容列表

        localStorageKey: ''
    };

    function PopupBox(cfg) {
        this._$elem = $('');
        this._tpl = '';
        return this.init(cfg);
    }

    PopupBox.prototype = {
        constructor: PopupBox,

        init: function(cfg) {
            this.initCfg(cfg);
            
            if(window.localStorage && window.localStorage.getItem(cfg.localStorageKey) != 'true') {
                window.localStorage.setItem(cfg.localStorageKey, 'true');
                
                this.tpl();

                this.render();

                this.bindUI();
            }
            
            return this;
        },
        
        initCfg: function (cfg) {
            this.cfg = $.extend({}, defaultSettings, cfg);
        },

        tpl: function() {
            var html = ['<div class="model-layer">',
                             '<div class="newuser-wrapper">',
                                '<div class="newuser-content">',
                                   '<img src="<# this.picUrl #>" alt="">',
                                   '<p class="newuser-title"><# this.title #></p>',
                                   '<ul class="newuser-list">',
                                        '<# for(var i = 0; i < this.content.length; i++) { var item = this.content[i]; #>',
                                        '<li><# item #></li>',
                                        '<# } #>',
                                    '</ul>',
                                    '<div class="popbox-group-btn border border-t">',
                                        '<# if(this.isNeedKnowMore) { #>',
                                            '<button class="cancel-btn">知道啦</button>',
                                            '<button class="sure-btn">了解更多</button>',
                                        '<# } else { #>',
                                            '<button class="cancel-btn btn-full-w">知道啦</button>',
                                        '<# } #>',
                                    '</div>',
                                '</div>',
                            '</div>',
                        '</div>'].join('');
            this._tpl = html;
        },

        render: function() {
            this._$elem = $(this.templateEngine(this._tpl, this.cfg));
            this._$elem.appendTo('body');
        },

        bindUI: function() {
            var me = this;

            this._$elem.on('click', '.cancel-btn', function() {
                me._$elem.css('display', 'none');
            });

            if (this.cfg.isNeedKnowMore) {
                this._$elem.on('click', '.sure-btn', function() {
                    window.location.href = me.cfg.konwMoreURL;
                });
            }
        },

        /*
         * 页面模板引擎解析
         * @param {string} html 要进行解析的html字符串（在<##>里面的转换成js来执行）
         * @param {object} options html中解析传入的对象
         * @return {string} 返回解析后的html字符串
         */
        templateEngine: function(html, options) {
            var re = /<#(.*?)#>/g, 
                reExp = /(^(\s|\t|\n|\r)*(var|if|for|else|switch|case|break|{|}))(.*)?/g, 
                code = 'var r=[];\n', 
                cursor = 0;
            
            html = html.replace(/\n|\r|\t/g, '');

            var add = function(line, js) {
                js? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
                    (code += line !== '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
                return add;
            };
            
            while(match = re.exec(html)) {
                var noJsStr = html.slice(cursor, match.index);
                add(/[^\s]/g.test(noJsStr) ? noJsStr : '')(match[1], true);
                cursor = match.index + match[0].length;
            }
            add(html.substr(cursor, html.length - cursor));
            code += 'return r.join("");';
            
            return new Function(code.replace(/[\r\t\n]/g, '')).apply(options ? options : {});
        }
    };

    if (typeof define === 'function' && (define.amd || define.cmd)) {
        define(['Zepto'], function ($) {
            return PopupBox;
        });
    } else {
        window.PopupBox = window.PopupBox || PopupBox;
    }
})(window, Zepto);