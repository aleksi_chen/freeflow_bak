/**
 * 图片上传UI组件
 * 依赖 upload
 * 依赖 mTouch
 * 继承 Widget	
 * @author muqin_deng@kingdee.com
 * @time 2015/12/15
 */
;(function (global, Upload, mTouch, factoryFn) {
    var factory = factoryFn(global, Upload, mTouch, navigator.userAgent);
    
    global.UploaderUI = global.UploaderUI || factory;
    
})(window, Upload, mTouch, function (global, Upload, mTouch, ua) {
    var ua = navigator.userAgent;
    var util = {
        isMobile: function () {
            var regObj = {
                'ios': /\(i[^;]+;( U;)? CPU.+Mac OS X/,
                'android': /Android/i,
                'wp': /Windows\s+Phone/
            };
            
            for (var key in regObj) {
                if (regObj.hasOwnProperty(key) && regObj[key].test(ua)) {
                    return true;
                }
            }
            return false;
        },
        
        // 是否桌面端
        isCloudHub: function () {
            return /cloudhub 10204/.test(ua);
        },
        
        /*
         * 页面模板引擎解析
         * @param {string} html 要进行解析的html字符串（在<##>里面的转换成js来执行）
         * @param {object} options html中解析传入的对象
         * @return {string} 返回解析后的html字符串
         */
        templateEngine: function(html, options) {
            var re = /<#(.*?)#>/g, 
                reExp = /(^(\s|\t|\n|\r)*((var|if|for|else|switch|case|break)\b|{|}))(.*)?/g, 
                code = 'var r=[];\n', 
                cursor = 0;
            
            html = html.replace(/\n|\r|\t/g, '');

            var add = function(line, js) {
                js? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
                    (code += line !== '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
                return add;
            };
            
            while(match = re.exec(html)) {
                var noJsStr = html.slice(cursor, match.index);
                add(/[^\s]/g.test(noJsStr) ? noJsStr : '')(match[1], true);
                cursor = match.index + match[0].length;
            }
            add(html.substr(cursor, html.length - cursor));
            code += 'return r.join("");';
            return new Function(code.replace(/[\r\t\n]/g, '')).apply(options ? options : {});
        }
    };
    
    // 主函数
    function UploaderUI(cfg) {
        Widget.call(this);
        
        return this.render(cfg);
    }
    
    UploaderUI.prototype = Widget.extend({
        constructor: UploaderUI,

        // 初始化配置项
        initCfg: function (cfg) {
            this.cfg = $.extend(true, {
                'target': '',
                // 上传图片配置项
                'imgConfig': {  
                    target: '',         // 选择器
                    count: -1           // 允许上传个数
                },    
                // 上传文件配置项
                'fileConfig': {
                    target: '',         // 选择器
                    count: -1           // 允许上传个数
                },
                'onOverImgCount': function () {},   // 超出上传图片张数
                'onOverFileCount': function () {},  // 超出上传文件张数
                'onUploadImgStart': function () {},     // 开始上传图片
                'onUploadFileStart': function () {},    // 开始上传文件
                'onUploadImgSuccess': function () {},   // 上传图片成功
                'onUploadFileSuccess': function () {},  // 上传文件成功
                'onDelImg': function () {},
                'onDelFile': function () {},
                'onClickImgItem': function () {},
                'onClickFileItem': function () {},
                'uploadUrl':  undefined, // 上传路径
                // 私有数据
                'pData': {
                    img: {
                        'items': [], 
                        'count': 0,
                        'successCount': 0,
                        'failCount': 0
                    },
                    file: {
                        'items': [], 
                        'count': 0,
                        'successCount': 0,
                        'failCount': 0
                    }
                }
            }, cfg);
        },
        
        renderUI: function () {
            var tpl = [
                '<div class="upload-img-preview"></div>',
                '<div class="upload-file-preview"></div>'
            ].join('');
            
            return tpl;
        },

        // 绑定事件
        bindUI: function () {
            this.eachBind({
                'overImgCount': 'onOverImgCount',
                'overFileCount': 'onOverFileCount',
                'uploadImgStart': 'onUploadImgStart',
                'uploadFileStart': 'onUploadFileStart',
                'uploadImgSuccess': 'onUploadImgSuccess',
                'uploadFileSuccess': 'onUploadFileSuccess',
                'delImg': 'onDelImg',
                'delFile': 'onDelFile',
                'clickImgItem': 'onClickImgItem',
                'clickFileItem': 'onClickFileItem'
            });
            
            var $ = this._$;
            var me = this;
            var imgCfg = this.get('imgConfig');
            var fileCfg = this.get('fileConfig');
            
            // 绑定图片上传选择图片事件
            if (imgCfg.target) {
                $('body').on('change', imgCfg.target, function (e) {
                    me._fireUpload(e, this, 'img');                    
                });
            }
            
            if (fileCfg.target) {
                if (util.isMobile()) {
                    $(fileCfg.target).on('click', function (e) {e.preventDefault();})
                    mTouch(fileCfg.target).on('tap', function (e) {
                        me._selectFiles(e, this);
                    });
                } else {
                    $('body').on('change', fileCfg.target, function (e) {
                        me._fireUpload(e, this, 'file');
                    });
                }
            }

            // 上传结束后的操作事件
            mTouch(this._$elem[0]).on('tap', '.reupload', function (e) {
                me._onReUploadBtnClick(e, this);
                return false;
            })
            .on('tap', '.upload-del-btn', function (e) {
                me._onDelBtnClick(e, this);
                return false;
            });
            
            this._$elem.on('click', '.upload-item.upload-success', function (e) {
                e.preventDefault();
                e.stopPropagation();
                me._onClickUploadItem(e, this);
            });
        },
        
        // 点击重新上传
        _onReUploadBtnClick: function (e, elem) {
            var $item = $(elem).closest('.upload-item');
            var upload = $item.data('upload');
            var type = $item.data('type');
            var pDataCfg = this.get('pData')[type];
            
            pDataCfg.failCount -= 1;
            
            upload.reUpload();
        },
        
        // 点击删除按钮
        _onDelBtnClick: function (e, elem) {
            var $elem = $(elem);
            var $item = $elem.closest('.upload-item');
            
            if ($item.data('isDel')) {
                return ;
            }
            
            $item.data('isDel', true);
            
            var type = $item.data('type');
            var id = $item.data('id');
            var pDataCfg = this.get('pData')[type];
            
            pDataCfg.count -= 1;
            
            var fileInfo;
            if (id) {
                pDataCfg.successCount -= 1;
                fileInfo = this._delFile(pDataCfg.items, id);
            } else {
                pDataCfg.failCount -= 1;
            }       
            
            this.fire((type == 'img' ? 'delImg' : 'delFile'), fileInfo ? fileInfo : undefined);
            
            $item.fadeOut(200, function () {
                $elem.removeData();
                $item.removeData().remove();
            });
        },
        
        _onClickUploadItem: function (e, elem) {
            var $item = $(elem);
            var type = $item.data('type');
            var id = $item.data('id');
            var pDataCfg = this.get('pData')[type];
            var fileInfo = pDataCfg.items[id];
            
            this.fire((type == 'img' ? 'clickImgItem' : 'clickFileItem'), fileInfo);
        },
        
        // 触发上传
        _fireUpload: function (e, elem, type) {
            var me = this;
            var files = e.target.files; 
            var len = files.length;
            var checkCount = this._checkOverCount(type, len);
            
            // 检查是否超出文件个数限制
            if(checkCount < len) {
                len = checkCount;
            }
            
            var pDataCfg = this.get('pData')[type];
                        
            for (var i = 0; i < len; i++) {
                pDataCfg.count += 1;  // 上传总个数
                // 上传开始
                this._uploadFile(files[i], type, function (resp) {
                        var fileInfo = me._parseFileInfo(resp.fileInfo);
                        var $item = resp.$item;
                        
                        pDataCfg.successCount += 1;
                        me._addFile(pDataCfg.items, fileInfo);
                        
                        me.fire((type == 'img' ? 'uploadImgSuccess' : 'uploadFileSuccess'), fileInfo, $item);
                        
                        if (type == 'img') {
                            $item.find('img')
                                 .on('load', function () {
                                     $item.removeClass('uploading')
                                          .find('.upload-progress').fadeOut(300);
                                 })
                                 .attr('src', '/lightapp/c/file/image/get.json?fileId=' + fileInfo.id + '&w=120&h=120');
                        } else {
                            var fileIconType = me._getFileType(fileInfo.ext);
                            
                            $item.removeClass('uploading')
                                 .find('.file-icon')
                                 .addClass('file-' + fileIconType + '-icon');
                        }
                        
                    }, function (fileInfo) {
                        pDataCfg.failCount += 1;
                    });
            }
            
            var elemClone = elem.cloneNode();
            elem.parentNode.insertBefore(elemClone, elem);
            elem.parentNode.removeChild(elem);
        },
        
        // 调用js桥选择文件
        _selectFiles: function () {
            if (!XuntongJSBridge) {
                return ;
            }
            
            var me = this;
            
            XuntongJSBridge.call('selectFile', {}, function (resp) {
                if (resp.success == true || resp.success == 'true') {
                    var files = resp.data ? resp.data.files : [];
                    
                    if (files && files.length) {
                        var len = files.length;
                        var checkCount = me._checkOverCount('file', len);
                        
                        // 检查是否超出文件个数限制
                        if(checkCount < len) {
                            len = checkCount;
                        }
                        
                        var pDataCfg = me.get('pData').file;
                        
                        for (var i = 0; i < len; i++) {
                            var fileInfo = me._parseFileInfo(files[i]);
                            me._addFile(pDataCfg.items, fileInfo);
                            
                            pDataCfg.count += 1;  // 上传总个数
                            pDataCfg.successCount += 1;
                            
                            var $item = me._$(me._tplItem(fileInfo, 'file'));
                            me.find('.upload-file-preview').append($item);
                            
                            me.fire('uploadFileStart', $item);
                            
                            $item.find('.upload-progress').remove();
                            $item.data('id', fileInfo.id)
                                 .removeClass('uploading')
                                 .addClass('upload-success');   // 存放上传实例
                            
                            var fileIconType = me._getFileType(fileInfo.ext);
                            $item.find('.file-icon')
                                 .addClass('file-' + fileIconType + '-icon');
                            
                            me.fire('uploadFileSuccess', fileInfo, $item);
                        }
                    }
                }
            });
        },
        
        // 检查是否超出文件个数限制
        _checkOverCount: function (type, uploadCount) {
            var config = this.get(type + 'Config');
            var count = config.count;
            
            if (count > -1) {
                var pData = this.get('pData');
                var pDataCfg = pData[type];
                var readyCount = pDataCfg.count;
                
                // 在上传的个数已经达到设置的最大上传个数
                if (readyCount >= count) {
                    this.fire((type == 'img' ? 'overImgCount' : 'overFileCount'), readyCount);
                    return 0;
                } else {
                    return count - readyCount;
                }
            } else {
                return uploadCount;
            }
        },
        
        // 上传开始
        _uploadFile: function (file, type, successCb, failCb) {
            var timerId, step = 1;
            var deferred = this._$.Deferred();
            var $item = this._$(this._tplItem(file, type));
            this.find('.upload-' + type + '-preview').append($item);
            
            this.fire((type == 'img' ? 'uploadImgStart' : 'uploadFileStart'), $item);
            
            var upload = new Upload({
                'file': file,
                'onUploadStart': function () {
                    $item.addClass('uploading')
                         .find('.upload-progress')
                         .removeClass('reupload')
                         .find('span')
                         .html('0%');
                },
                // 上传成功
                'onUploadSuccess': function (e, fileInfo) {
                    clearTimeout(timerId);
                    
                    $item.find('.upload-progress span').html('99%');
                    
                    $item.data('id', fileInfo.id)
                         .addClass('upload-success');
                    
                    successCb && successCb({fileInfo: fileInfo, $item: $item});
                }, 
                // 上传进度
                'onUploadProgress': function (e, loaded, total) {
                    var percent = Math.floor(loaded / total * 100);
                    
                    if (percent > 96) {
                        if (timerId) return ;
                        // 下载缩略图期间模拟进度
                        timerId = setInterval(function () {
                            if (step > 2) {
                                clearTimeout(timerId);
                                return ;
                            }
                            $item.find('.upload-progress span').html((96 + step++) + '%');
                        }, 1500);
                        return ;
                    }
                    
                    $item.find('.upload-progress span')
                         .html(percent + '%');
                },
                // 上传失败
                'onUploadFail': function (e, fileInfo) {
                    clearTimeout(timerId);
                    $item.removeClass('uploading')
                         .find('.upload-progress')
                         .addClass('reupload')
                         .find('span')
                         .html('重新上传');
                    
                    failCb && failCb(fileInfo);
                } 
            });
            
            $item.data('upload', upload);   // 存放上传实例            
        },
        
        // 拼装图片上传预览
        _tplItem: function (file, type) {
            var tpl = [
                '<div class="upload-item upload-' + type + '-item uploading" data-type="' + type + '">',
                    '<div class="upload-item-preview">',
                        '<span class="upload-del-btn">×</span>',
                        '<div class="upload-progress"><span>0%</span></div>',
                        (type == 'img' ? '<img src="" />' : '<span class="file-icon"></span>'),
                    '</div>',
                    '<p class="upload-item-name ellipsis">' + file.name + '</p>',
                '</div>'
            ].join('');
            
            return tpl;
        },

        ready: function () {
            this.fire('ready');
        },
                
        _addFile: function (itemsObj, fileObj) {
            itemsObj[fileObj.id] = fileObj;
            return itemsObj;
        },
        
        _delFile: function (itemsObj, id) {
            try {
                var val = itemsObj[id];
                delete itemsObj[id];
                return val;
            } catch (e) {
                return false;
            }
        },
        
        // 转换文件信息格式
        _parseFileInfo: function (fileInfo) {
            return {
                id: fileInfo.id || fileInfo.fileId,
                name: fileInfo.fullName || fileInfo.fileName,
                size: fileInfo.size || fileInfo.fileSize,
                ext: fileInfo.ext || fileInfo.fileExt
            };
        },
        
        _getFileType: function (ext) {
            if (!ext) {
                return 'unknown';
            }
            var fileTypeCfg = {
                jpg: 'img',
                jpeg: 'img',
                png: 'img',
                gif: 'img',
                bmp: 'img',
                docx: 'word',
                doc: 'word',
                xlsx: 'excel',
                xls: 'excel',
                pptx: 'ppt',
                ppt: 'ppt',
                pptm: 'ppt',
                pdf: 'pdf',
                zip: 'zip',
                rar: 'zip',
                mp3: 'music',
                ogg: 'music',
                wma: 'music',
                wav: 'music',
                aac: 'music',
                wmv: 'video',
                asf: 'video',
                rm: 'video',
                rmvb: 'video',
                mov: 'video',
                avi: 'video',
                dat: 'video',
                mpg: 'video',
                mpeg: 'video',
                mp4: 'video',
                txt: 'txt'
            };
            
            return fileTypeCfg[ext.toLocaleLowerCase()] || 'unknown';
        },
        
        getUploading: function () {
            var pData = this.get('pData');
            
            var imgLoadingCount = pData.img.count - pData.img.successCount - pData.img.failCount;
            var fileLoadingCount = pData.file.count - pData.file.successCount - pData.file.failCount;
            
            if (imgLoadingCount <= 0 && fileLoadingCount <= 0) {
                return false;
            }
            
            return {
                imgLoadingCount:  imgLoadingCount,
                fileLoadingCount: fileLoadingCount
            };
        },
        
        _getUploadedItems: function (type) {
            var me = this;
            var items = this.get('pData')[type].items;
            var arr = [];
            var $uploadElems = this.find('.upload-' + type + '-item.upload-success');
            
            $uploadElems.each(function (i, item) {
                var $item = me._$(item);
                var isDel =  $item.data('isDel');
                var id = $item.data('id');
                !isDel && arr.push(items[id]);
            });
            
            return arr;
        },
        
        getUploadedImgs: function () {
            return this._getUploadedItems('img');
        },
        
        getUploadedFiles: function () {
            return this._getUploadedItems('file');
        }
    });
    
    return UploaderUI;
});