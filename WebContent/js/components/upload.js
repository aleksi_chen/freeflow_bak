/**
 * 图片上传UI组件
 * 继承 Widget	
 * @author muqin_deng@kingdee.com
 * @time 2015/12/15
 */
;(function (global, factoryFn) {
    var factory = factoryFn(global);
    
    global.Upload = global.Upload || factory;
    
})(window, function (global) {
    // 主函数
    function Upload(cfg) {
        Widget.call(this);
        
        return this.render(cfg);
    }
    
    Upload.prototype = Widget.extend({
        constructor: Upload,

        // 初始化配置项
        initCfg: function (cfg) {
            this.cfg = this._$.extend({
                'file': null,   // 需上传的文件对象
                'fileSize': -1, // 允许上传的最大文件大小
                'fileType': '',  // 允许的文件类型
                'uploadUrl': '/lightapp/c/file/upload.json',
                'formData': {}, // 上传额外携带的数据，可配置需要携带name,size等参数 
                'onCheckError': function () {}, // 检测截断出错
                'onUploadStart': function () {},    // 上传开始
                'onUploadSuccess': function () {},  // 上传成功
                'onUploadProgress': function () {}, // 上传进度
                'onUploadFail': function () {} // 上传失败
            }, cfg);
        },

        // 绑定事件
        bindUI: function () {
            this.eachBind({
                'checkError': 'onCheckError',
                'uploadStart': 'onUploadStart',
                'uploadSuccess': 'onUploadSuccess',
                'uploadProgress': 'onUploadProgress',
                'uploadFail': 'onUploadFail',
                'reUpload': function () {
                    this.reUpload();
                }
            });
        },

        ready: function () {
            this.reUpload();

            this.fire('ready');
        },

        _check: function () {
            var file = this.get('file'),
                type = this.get('fileType'),
                size = this.get('fileSize'),
                uploadUrl = this.get('uploadUrl');
            
            if (!file) {    //文件不存在
                this.fire('checkError', '没有传入文件');
                return false;
            }

            if (type && type != file.type) {
                this.fire('checkError', '文件格式不正确');
                return false;
            }

            if (size != -1 && size < file.size) {
                this.fire('checkError', '文件大小过大');
                return false;
            }

            if (!uploadUrl) {
                this.fire('checkError', '没有上传路径');
                return false;
            }

            return true;
        },

        // 上传
        _upload: function () {
            var me = this,
                file = this.get('file'),
                xhr = new XMLHttpRequest(),
                fd = new FormData();    //表单数据对象
            
            fd.append('name', 'Html 5 File API/FormData');
            fd.append('uploadfile', file, file.name);   //表单添加文件
            
            // 上传开始
            xhr.onloadstart = function (e) {
                me.fire('uploadStart', e, me._getFileInfo());
            };
            // 上传结束
            xhr.onload = function (e) {me._uploadComplete(e, xhr); };
            //上传进度
            // xhr.onprogress = function (e) { me._uploadProgress(e, xhr); };
            xhr.upload.onprogress = function (e) { me._uploadProgress(e, xhr); };
            // 上传错误
            xhr.onerror = function (e) { 
                me.fire('uploadFail', e, me._getFileInfo());
            };
            
            //发送文件和表单自定义参数
            xhr.open('post', this.get('uploadUrl'));
            xhr.send(fd);
            
            this.set('xhr', xhr);
        },
        
        // 上传进度
        _uploadProgress: function (e, xhr) {
            // 如果进度可用
            if (event.lengthComputable) {
                this.fire('uploadProgress', e, e.loaded, e.total);
            }
        },

        // 上传结束
        _uploadComplete: function (e, xhr) {
            var fileInfo = this._getFileInfo();

            if (xhr.status == 200 || xhr.status == 304) {
                var resp = JSON.parse(xhr.responseText);                
                
                fileInfo.id = resp.data;
                
                if (resp && resp.success === true) {
                    // 上传成功回调
                    this.fire('uploadSuccess', e, fileInfo);
                } else {
                    // 上传失败回调
                    this.fire('uploadFail', e, fileInfo);
                }
                
            } else {
                // 上传失败回调
                this.fire('uploadFail', e, fileInfo);
            }

            xhr.abort();
        },


        // 重新上传
        reUpload: function () {
            if (this._check()) {
                this._upload();
            }
        },

        // 包装上传图片信息
        _getFileInfo: function (id) {
            var file = this.get('file');
            var fullName = file.name;
            var name = fullName;
            var ext = '';
            var index = fullName.lastIndexOf('.');
            
            if (index > -1) {
                name = fullName.substring(0, index);
                ext = fullName.substring(index + 1, fullName.length);
            }
            
            var fileInfo = {
                id: id || '',
                fullName: fullName,
                name: name,
                size: file.size,
                type: file.type,
                ext: ext
            };
            
            return fileInfo;
        }
    });
    
    return Upload;
});