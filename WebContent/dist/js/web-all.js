;(function (window, doc, factory) {
	factory = factory(window, doc);

	window.webUtil = window.webUtil || factory;

})(window, document, function (window, doc) {
	// 工具集
	var webUtil = {
		// 根据命名空间执行init方法
		run: function (namespaceStr) {
			if (!namespaceStr) {return ;}

			var namespaceArr = namespaceStr.split('.'),
				obj = window;

			for (var i = 0, len = namespaceArr.length; i < len; i++) {
				if (!obj[namespaceArr[i]]) {
					return ;
				}

				obj = obj[namespaceArr[i]];
			}

			typeof obj.init === 'function' && obj.init();
		},

		// 创建命名空间
		namespace: function (namespaceStr, newObj) {
			if (!namespaceStr) {return ;}

			var namespaceArr = namespaceStr.split('.'),
				obj = window;

			for (var i = 0, len = namespaceArr.length; i < len; i++) {
				if (!obj[namespaceArr[i]]) {
					if (newObj !== undefined && i == len - 1) {
						obj = obj[namespaceArr[i]] = newObj;
						break;
					}

					obj[namespaceArr[i]] = {};
				}

				obj = obj[namespaceArr[i]];
			}

			return obj;
		},

		// 获取url键值对
        getUrlKeyValObj: function () {
        	var url = window.location.search,
				arr, i, len,
				paramsObj = {};	
        	
        	if (this.getUrlKeyValObj.urlKeyValObj) {
        		return this.getUrlKeyValObj.urlKeyValObj;
        	}
	
			arr = url.substring(1).split('&');

			if (!arr.length) {
				return paramsObj;
			}
			
			for (i = 0, len = arr.length; i < len; i++) {
				var reg = /(.*)\=(.*)/g,
					match = reg.exec(arr[i]);
	
				if (match && match[1]) {
					paramsObj[decodeURIComponent(match[1])] = decodeURIComponent(match[2]);
				}
			}
			
			this.getUrlKeyValObj.urlKeyValObj = paramsObj;
			
			return paramsObj;
        },
        
        // 获取url的对应参数的值
		getUrlValue: function (param) {
			if (!param) {
				return '';
			}
			
			var paramsObj = this.getUrlKeyValObj();

			if (paramsObj.hasOwnProperty(param)) {
				return paramsObj[param];
			} else {
				return '';
			}
		},

		/*
		 * 函数节流
		 * @param {function} method	要进行节流的函数
		 * @param {number} delay 延时时间(ms)
		 * @param {number} duration 经过duration时间(ms)必须执行函数method
		 */
		throttle: function (method, delay, duration) {
			var timer = null,
				begin = null;
            return function () {
                var context = this,
                	args = arguments,
                	current = new Date();
                if (!begin) {
                	begin = current;
                }
                if (timer) {
                	window.clearTimeout(timer);
                }
                if (duration && current - begin >= duration) {
                     method.apply(context,args);
                     begin = null;
                }else {
                    timer = window.setTimeout(function () {
                        method.apply(context, args);
                        begin = null;
                    }, delay);
               }
            };
		},

		uuid: function (len, radix) {
		    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
		    var uuid = [], i;
		    radix = radix || chars.length;
		 
		    if (len) {
		      // Compact form
		      for (i = 0; i < len; i++) {
		      	uuid[i] = chars[0 | Math.random() * radix];
		      }
		    } else {
		      var r;
		 
		      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
		      uuid[14] = '4';

		      for (i = 0; i < 36; i++) {
		        if (!uuid[i]) {
		          r = 0 | Math.random() * 16;
		          uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
		        }
		      }
		    }
		 
		    return uuid.join('');
		},

		// 获取字节长度
		getByteLength: function(str){
            return str.replace(/[^\x00-\xff]/g, '**').length;
        },

        /**
		 * 根据字节长度截取字符串
		 * @param {string} str 目标字符串
		 * @param {number} num 需要截取的长度
		 * @return {string} 返回截取后的字符串
		 */
		getByteVal: function(str, num){
			var len = 0,
				returnValue = '';
			for (var i = 0, l = str.length; i < l; i++) {
				if (str[i].match(/[^\x00-\xff]/ig) != null) { //全角
		            len += 2;
		        } else {
		            len += 1;
				}
				if (len > num) {
					break;
				}
				returnValue += str[i];
			}
			return returnValue;
		},

		// 转义特殊字符<,>,",&
		escape: function (str) {
			if (!str) {
				return '';
			}

			var escape = {
				'<': '&lt;',
				'>': '&gt;',
				'\"': '&quot;',
				'&': '&amp;'
			};

			return str.replace(/[&<>"]/g, function (match) {
				return escape[match] || match;
			});
		},

		tab: function (panelWrap, activeClass) {
			var $wrap = $(panelWrap);

			$wrap.on('click', '[data-panel]', function () {
				activeClass && $wrap.find('[data-panel]').removeClass(activeClass);
				$wrap.find('[data-pindex]').removeClass(activeClass).hide();

				var index = $(this).addClass(activeClass).data('panel');
				$wrap.find('[data-pindex=' + index + ']').addClass(activeClass).show();
			});
		},

		tips: function (msg) {
			$('.ui-tips').remove();

			var $tips = $('<div class="ui-tips floatin"></div>');
			$tips.text(msg).appendTo('body');
		},

		animationEnd: function (el, cb, remove) {
			var eventType = whichAnimationEvent(el);

			el.addEventListener(eventType, handler, false);

			function handler() {
		    	if (remove !== false) {
		    		el.removeEventListener(eventType, handler, false);
		    	}

		    	var args = [].slice.call(arguments);
		    	cb.apply(null, args);
		    }

			function whichAnimationEvent(el) {
				var animations = {
					'animation':'animationend',
					'OAnimation':'oAnimationEnd',
					'MozAnimation':'animationend',
					'WebkitAnimation':'webkitAnimationEnd'
				}

				for(var a in animations){
				   if( el.style[a] !== undefined ){
				       return animations[a];
				   }
				}
		    }
		},

		delay: function (time) {
			var deferred = $.Deferred();

			setTimeout(function () {
				deferred.resolve();
			}, time || 0);

			return deferred.promise();
		},

		// 简单模板拼装
		Template: function () {
			this.arr = [];
			this._pushAll(arguments);
		},
        //截取URL键值对
        getRequest : function(){ 
            var url = window.location.search;
            var theRequest = new Object();
            if (url.indexOf("?") != -1) {
                var str = url.substr(1);
                strs = str.split("&");
                for (var i = 0; i < strs.length; i++) {
                    theRequest[strs[i].split("=")[0]] = decodeURIComponent(strs[i].split("=")[1]);
                }
            }
            return theRequest;
        }
	};
    

	webUtil.Template.prototype = {
		constructor: webUtil.Template,

		_: function () {
			this._pushAll(arguments);
			return this;
		},

		toString: function () {
			return this.arr.join('');
		},

		_pushAll: function (arguments) {
			var args = [].slice.call(arguments);
			this.arr = this.arr.concat(args);
		},

		clean: function () {
			this.arr = [];
			return this;
		}
	};

	return webUtil;
});
/*
 * 父级组件
 * @author muqin_deng
 * @time 2015/09/25
 */
;(function (factory) {
	factory = factory(window, document);

	webUtil.namespace('F.d.Widget', factory);

})(function () {
	//主函数
	function Widget() {
		this._$elem = null;
		this.cfg = {};			//配置项
		this._handlers = {};
	}

	Widget.prototype = {
		constructor: Widget,
		
		//绑定自定义事件
		on: function (type, handler) {
			if (!this._handlers.hasOwnProperty(type)) {
				this._handlers[type] = [];
			}
			this._handlers[type].push(handler);
			return this;
		},
		
		//触发自定义事件
		fire: function (type, data) {
			//判断是否为数组
			if (Object.prototype.toString.call(this._handlers[type]) == '[object Array]') {
				var handlers = this._handlers[type];
				
				for (var i = 0, len = handlers.length; i < len; i++) {
					handlers[i].apply(this, Array.prototype.slice.call(arguments).slice(1));
				}
			}
			return this;
		},
		
		//统一绑定外部事件 {type: funcName}
		eachBind: function (typeObj) {
			var val;

			if (!typeObj) {
				return ;
			}

			for (key in typeObj) {
				if (typeObj.hasOwnProperty(key)) {
					val = typeObj[key];

					//如果是字符串类型且配置项中有值
					if (typeof val === 'string' && typeof this.get(val) === 'function') {
						this.on(key, this.get(val));
					//如果是函数则直接绑定
					} else if (typeof val === 'function') {
						this.on(key, val);
					} 
				}
			}
		},
		
		//设置cfg的值
		set: function (key, value) {
			this.cfg[key] = value;
			return this;
		},
		
		//取cfg的值
		get: function (key) {
			return this.cfg[key];
		},


		init: function () {},	//初始化（接口）

		initCfg: function () {},	//初始化配置项（接口）
		
		renderUI: function () {},	//生成dom节点（接口）
		
		bindUI: function () {},	//绑定事件（接口）
		
		syncUI: function () {},	//同步设置初始样式（接口）
		
		ready: function () {}, //组件实例化完成后调用（接口）

		//总体渲染入口
		render: function (cfg) {
			this.init();
			this.initCfg(cfg);
			this.renderUI();
			this.bindUI();
			this.syncUI();

			var $container = $(this.get('target') || document.body);

			if (this.get('isPrepend')) {
				$container.prepend(this._$elem);
			} else {
				$container.append(this._$elem);
			}

			this.ready();

			return this;
		},

		//销毁
		destroy: function () {
			this._$elem.off().remove();
			
			this.fire('destroy');
		},
		
		find: function (selector) {
			return this._$elem.find(selector);
		},
		
		//获取弹窗的节点对象(jq)
		getElem: function () {
			return this._$elem;
		}

	};

	return Widget;
});

/*
 * 分页组件
 * 依赖jQuery
 * @author muqin_deng
 * @time 2015/03/29
 */
;(function(factory) {
    factory = factory(window, document, jQuery, mTouch);

    webUtil.namespace('F.d.Paginator', factory);

})(function(window, doc, $, mTouch) {
    var Widget = F.d.Widget;

    //主函数
    function Paginator(settings) {
        Widget.call(this);

        return this.init(settings);
    }

    Paginator.prototype = $.extend({}, Widget.prototype, {
        constructor: Paginator,

        initCfg: function (settings) {
            this.cfg = {
                'target': 'body',
                'className': '',
                'pageSize': 10,     //一页多少条数据
                'middleNum': 5,     //中间显示的页码数
                'edgeNum': 1,       //左右两边要显示的页码数
                'itemstotal': 0,    //数据总条数
                'currentPage': 1,   //当前页
                'ellipsis': '...',  //省略内容
                'onSelectPage': null    //选中页回调
            };

            $.extend(this.cfg, settings);

            this.cfg.pageTotal = this._getPageTotal();
        },

        init: function (settings) {         
            this.initCfg(settings);

            //只有一页不显示分页
            if (this.get('pageTotal') <= 1) {
                return ;
            }

            this.renderUI();
            this.bindUI();
            this.syncUI();
            
            this.fire('ready');

            return this;
        },

        renderUI: function () {
            this._$elem = $('<ul></ul>');

            var html = this._tpl(this.get('currentPage'), this.get('pageTotal'));

            this._$elem.html(html);

            $(this.get('target')).html(this._$elem);
        },

        _tpl: function (currentPage, pageTotal) {
            var html = '';
            if(pageTotal > 1){
                //上一页
                html += '<li' + (currentPage == 1 ? ' class="disabled"' : '') + '>'
                     +      '<a href="#" class="pre">上一页</a>'
                     +  '</li>';

                var displayPages = this._getDisplayPages(), i, len = displayPages.length;

                for (i = 0; i < len; i++) {
                    displayPage = displayPages[i];

                    //省略部分
                    if (displayPage == '.') {
                        html += '<li class="disabled"><a href="#">' + this.get('ellipsis') + '</a></li>';
                    //页码部分
                    } else {
                        html += '<li' + (currentPage == displayPage ? ' class="active"' : '') + '>'
                             +      '<a href="#" data-page="' + displayPage + '">' + displayPage + '</a>'
                             +  '</li>';
                    }
                }

                //下一页
                html += '<li' + (currentPage == pageTotal ? ' class="disabled"' : '') + '>'
                     +      '<a href="#" class="next"><span>下一页</span></a>'
                     +  '</li>';

            }
                
            return html;
        },

        bindUI: function () {
            var me = this;

            this._$elem.on('click', 'li a', function (e) {
                var $this =  $(this),
                    $li = $this.closest('li'),
                    currentPage =  me.get('currentPage'),
                    dataPage = $this.data('page');

                e.preventDefault();

                if ($li.hasClass('disabled') || $li.hasClass('active')) {
                    return ;
                }

                if ($this.hasClass('pre')) {
                    currentPage -= 1;
                } else if ($this.hasClass('next')) {
                    currentPage += 1;
                } else if (dataPage) {
                    currentPage = dataPage;
                }

                me.set('currentPage', currentPage);

                //重新生成页码栏
                me._reDraw(currentPage, me.get('pageTotal'));

                me.fire('selectPage', currentPage);
            });

            //绑定选中页码回调
            this.eachBind({
                'selectPage': 'onSelectPage',
                'ready': 'onReady'
            });
            
        },

        syncUI: function () {
            this._$elem.addClass(this.get('className'));
        },

        _reDraw: function (currPage, totalPage) {
            this._$elem.html(this._tpl(currPage, totalPage));
        },

        //获取左边页码数
        _getLeftEdge: function () {
            return this.get('edgeNum');
        },

        //获取中间页码数
        _getMiddles: function () {
            var curr = this.get('currentPage'),
                middleNum = this.get('middleNum'),
                pageTotal = this.get('pageTotal'),
                begin = curr - Math.floor((middleNum - 1) /2),
                end;

            if (begin < 1) {begin = 1;}

            end = begin + middleNum - 1;

            if (end > pageTotal) {end = pageTotal;}

            return [begin, end];
        },

        //获取右边开始页码
        _getRightEdge: function () {
            return this.get('pageTotal') - this.get('edgeNum') + 1;
        },

        //获取需要展示的页码
        _getDisplayPages: function () {
            var edgeNum = this.get('edgeNum'), middleNum = this.get('middleNum'),
                pageTotal = this.get('pageTotal'), i, pages = [];

            //总页码小于设定要展示的页码（两边 + 中间页码）
            if (middleNum + edgeNum * 2 >= pageTotal) {
                for (i = 1; i <= pageTotal; i++ ) {
                    pages.push(i);
                }
                return pages;
            }

            var left = this._getLeftEdge(), right = this._getRightEdge(),
                middles = this._getMiddles(), begin;
            //存入左边页码
            for (i = 1; i <= left; i++) {
                pages.push(i);
            }

            //判断中间开始页码是否比左边页码大
            if (middles[0] > left + 1) {
                pages.push('.');    //省略标记
                begin = middles[0];
            } else {
                begin = left + 1;
            }
            //存入中间页码
            for (i = begin; i <= middles[1]; i++) {
                pages.push(i);
            }

            //判断右边开始页码是否比中间结束页码大
            if (right > middles[1] + 1) {
                pages.push('.');
                begin = right;
            } else {
                begin = middles[1] + 1;
            }
            //存入右边页码
            for (i = begin; i <= pageTotal; i++) {
                pages.push(i);
            }

            return pages;
        },

        //计算页码总数
        _getPageTotal: function () {
            return Math.ceil(this.get('itemsTotal') / this.get('pageSize'));
        },
        
        getCurrentPage: function () {
            return this.get('currentPage');
        }
        
    });

    return Paginator;
    
});
/**
 * 搜索框组件
 * 依赖jQuery
 * @author muqin_deng@kingdee.com
 * @time 2015/10/12
 */

 ;(function (factory) {
	factory = factory(jQuery, webUtil);

	webUtil.namespace('F.d.autocomplete', factory);

})(function ($, Util) {

	var defaultSettings = {
		searchUrl: '',	//搜索路径

		key: 'word', //搜索内容对应的键值

		data: {},	//搜索请求中携带的额外数据

		cache: true,	//是否换成搜索记录

		throttle: 300,

		closeBtn: '',	// 关闭按钮如果有

		onBefore: function () {}, //发送搜索请求前的回调

		onNoSearchVal: function () {}, //搜索框值为空的回调

		onSuccess: function (resp) {},	//搜索成功回调

		onError: function (resp) {},	//搜索失败回调

		onAlways: function (resp) {}	//搜索或失败都会调用

	};

	$.fn.autoComplete = function (settings) {
		var cfg = $.extend({}, defaultSettings, settings);

		return this.each(function () {
			var ac = new AutoComplete(cfg);

			ac.init(this);
		});
	};

	function AutoComplete (cfg) {
		this.cfg = cfg;
		this._db = {};
	}

	AutoComplete.prototype = {
		constructor: AutoComplete,

		init: function (elem) {
			this._$elem = $(elem);
			this.bindUI();
		},

		//绑定事件
		bindUI: function () {
			var me = this;

			this._$elem.on('keyup', Util.throttle(function (e) {	//搜索输入框事件
					me._search(e);
				}, this.cfg.throttle)
			);
			
			if (this.cfg.closeBtn) {
				$(this.cfg.closeBtn).on('click', function () {
					me._$elem.val('');
					me._empty();
				});
			}
		},

		//搜索操作
		_search: function (e) {
			var $tar = this._$elem,
				searchCount = $tar.data('search-count') || 0,
				oldVal = $tar.data('old-val'),
				newVal = Util.escape($tar.val().trim()),
				reg = /\S+/g;	//非空格

			//按下退格键重新设置old-val为新的值
			if (e.keyCode == 8) {
				$tar.data('old-val', newVal);
			}

			//如果搜索框为空
			if (!reg.test(newVal)) {
				this._empty();
				return ;
			}
			//屏蔽ctrl/shift/enter/capsLock等非输入按键，减少发送请求
			if (oldVal == newVal) {
				return ;
			}

			//更新搜索次数
			$tar.data('search-count', searchCount += 1);

			this.cfg.onBefore.call(this);

			//发送查询请求
			this._searchGet(newVal, this._searchSucc(newVal, searchCount));
		},

		//搜索人员成功
		_searchSucc: function (newVal, searchCount) {
			var me = this,
				$tar = this._$elem;

			/* 使用闭包保留请求的顺序
			 * 保证是最后一次请求的回调才进行html拼装渲染（慢网速）
			 */
			return function (resp) {
				var reg = /\S+/g;	//非空格

				if (searchCount != $tar.data('search-count')) {
					return ;
				}
				//如果搜索框为空
				if (!reg.test($tar.val())) {
					return ;
				}

				$tar.data('old-val', newVal);

				me.cfg.onSuccess.call(me, {data: resp, keyVal: newVal});
			}
		},

		_empty: function () {
			this._$elem.data('old-val', '');
			this.cfg.onNoSearchVal.call(this);
		},

		//发送搜索请求
		_searchGet: function (word, fn) {
			var me = this,
				data = {};

			data['' + this.cfg.key] = word;

			if (this.cfg.data) {
				$.extend(data, this.cfg.data);
			}

			if (this.cfg.cache && this._db[word]) {
				fn && fn(this._db[word]);
				me.cfg.onAlways.call(me, {data: this._db[word], keyVal: word});
				return ;
			}

			$.ajax({
				type: "GET",
				url: this.cfg.searchUrl,
				dataType: 'json',
				data: data
			})
			.done(function (resp) {
				fn && fn(resp);

				if (me.cfg.cache) {
					me._db[word] = resp;
				}
			})
			.fail(function (resp) {
				me.cfg.onError.call(me, {data: resp, keyVal: word});
			})
			.always(function (resp) {
				me.cfg.onAlways.call(me, {data: resp, keyVal: word});
			});
		}

	};

});

/**
 * 组织架构树组件
 * 依赖jQuery、webUtil.js、widget.js、underscre.js
 * @author muqin_deng@kingdee.com
 * @time 2015/10/19
 */
;(function (factory) {
    factory = factory(jQuery, webUtil, F.d.Widget, _);

    webUtil.namespace('F.d.OrgTree', factory);
    
})(function ($, Util, Widget, _) {

    function OrgTree (cfg) {
        Widget.call(this);
        return this.render(cfg);
    }

    OrgTree.prototype = $.extend({}, Widget.prototype, {
        constructor: OrgTree,

        initCfg: function (cfg) {
            this.cfg = $.extend({
                'url': '/im/web/treeOrg.do',
                'eid': '',
                'orgId': '',
                'needSelectDept': false,
                'needPerson': true,
                'multiSelect': false    // 是否多选
            }, cfg);
        },
        //生成dom节点
        renderUI: function () {
            var t = new Util.Template();

            t._('<ul class="org-tree"></ul>');

            this._$elem = $(t.toString());
        },

        //绑定事件
        bindUI: function () {
            var me = this;

            // 点击非叶子节点事件
            this._$elem.on('click', '.orgtree-node-name', function (e) {    
                e.stopPropagation();
                me._onTreeNodeClick(e, this);
            })
            .on('click', '.orgtree-node-name label', function (e) {
                e.stopPropagation();
            })
            .on('change', '.orgtree-leaf input[type=checkbox]', function (e) {
                e.stopPropagation();
                me._onLeafChange(e, this);
            })
            .on('change', '.orgtree-node-name input[type=checkbox]', function (e) {
                e.stopPropagation();
                me._onTreeNodeChange(e, this);
            });

            // 绑定自定义事件
            this.eachBind({
                'expandNode': 'onExpandNode',
                'collNode': 'onCollNode',
                'selectPerson': 'onSelectPerson'
            });
        },

        //同步设置初始样式
        syncUI: function () {
            if (this.cfg.cls) {
                this._$elem.addClass(this.cfg.cls);
            }

        },

        ready: function () {
            this._tplTree(true, this.get('orgId'));

            this.fire('ready');
        },

        _onLeafChange: function (e, elem) {
            var $elem = $(elem);
            var checked = $elem.prop('checked');
            var $leaf = $elem.closest('.orgtree-leaf');
            var $parentCheckbox = $leaf.closest('.orgtree-node')
                                       .find('.orgtree-node-name input[type="checkbox"]');

            // 不支持多选
            if (!this.get('multiSelect')) {
                if (checked) {
                    this.find('.orgtree-leaf input[type="checkbox"]')
                        .not(elem)
                        .prop('checked', false);
                }
            // 支持多选
            } else {
                // 选中
                if (checked && $parentCheckbox.length && !$parentCheckbox.prop('checked')) {
                    var $leafs = $leaf.parent().find('input[type="checkbox"]');
                    var $checkedLeafs = $leafs.filter(':checked');
                    if ($leafs.length == $checkedLeafs.length) {
                        $parentCheckbox.prop('checked', true);
                    }
                // 取消选中
                } else if (!checked && $parentCheckbox.length && $parentCheckbox.prop('checked')) {
                    $parentCheckbox.prop('checked', false);
                }
            }

            // 触发选中事件
            if (checked) {
                this.fire('selectPerson', this.getPersonInfo($leaf));
            } else {
                this.fire('unselectPerson', this.getPersonInfo($leaf));
            }
            
        },

        // 非叶子结点复选框change事件
        _onTreeNodeChange: function (e, elem) {
            var $elem = $(elem);
            var persons = $elem.data('info');

            if (!persons || !persons.length) {return ;}

            var checked = $elem.prop('checked');
            var $checkboxs = $elem.closest('.orgtree-node')
                                  .find('.orgtree-leaf input[type=checkbox]');

            var i = 0, len = persons.length;
            if (checked) {
                $checkboxs.prop('checked', true);
                for (; i < len; i++) {
                    this.fire('selectPerson', persons[i])
                }
            } else {
                $checkboxs.prop('checked', false);
                for (; i < len; i++) {
                    this.fire('unselectPerson', persons[i]);
                }
            }
        },

        // 点击节点，加载下一级节点
        _onTreeNodeClick: function (e, elem) {
            var $node = $(elem).closest('.orgtree-node');
            var checked = $node.hasClass('selected');
            var needSelectDept = this.get('needSelectDept');

            // if ($node.hasClass('orgtree-root')) {
            //     return ;
            // }          

            var id = $node.data('id');
            var name = $node.children('.orgtree-node-name').attr('title');
            var level = $node.data('level');

            if (!$node.data('loaded')) {
                this._tplTree(false, id, $node, level);
            }

            // 需要选择部门
            if (needSelectDept) {
                // 是否多选
                if (this.get('multiSelect')) {
                    $node.toggleClass('selected');
                } else {
                    if (checked) {
                        $node.removeClass('selected');
                    } else {
                        this.find('.orgtree-node.selected').removeClass('selected');
                        $node.addClass('selected');
                    }
                }

                // 折叠
                if ($node.hasClass('orgtree-node-expand') && checked) {
                    $node.removeClass('orgtree-node-expand');
                    this.fire('collNode', id, name);
                } else if (!$node.hasClass('orgtree-node-expand') && !checked) {
                    $node.addClass('orgtree-node-expand');
                    this.fire('expandNode', id, name);
                }

            // 不需要选择部门
            } else {
                if ($node.hasClass('orgtree-node-expand')) {
                    $node.removeClass('orgtree-node-expand');
                    this.fire('collNode', id, name);
                } else {
                    $node.addClass('orgtree-node-expand');
                    this.fire('expandNode', id, name);
                }
            }

        },

        _tplTreeRoot: function (data) {
            var t = new Util.Template();

            t._('<li class="orgtree-node orgtree-root" data-id="<%= id %>" data-level="0">')
                ._('<span class="orgtree-node-name ellipsis" title="<%= name %>"><%= name %></span>')
            ._('</li>');

            return _.template(t.toString())(data);
        },

        _tplTreeWrap: function (node) {
            var t = new Util.Template();
            var $wrap;

            t._('<ul class="orgtree-nodelist">')
                ._('<p class="orgtree-loading">')
                    ._('<img src="/workreport/web/images/loading.gif" />')
                ._('</p>')
             ._('</ul>');

            $wrap = $(t.toString());
            $(node).data('loaded', true).append($wrap);

            return $wrap;
        },

        // 生成子树
        _tplTree: function (isRoot, orgId, node, level) {
            var me = this;
            var $treeWrap;
            level = level || 0;

            if (isRoot) {
                me._$elem.append('<p class="orgtree-loading"><img src="/workreport/web/images/loading.gif" /></p>');
            } else {
                $treeWrap = me._tplTreeWrap(node);
            }

            return this.requestSubTree(orgId, isRoot)
                .then(function (resp) {
                    resp.person = resp.person || [];
                    resp.children = resp.children || [];
                    resp._level = level + 1;

                    if (isRoot) {
                        // 创建树根节点
                        node = $(me._tplTreeRoot(resp));
                        // 插入树根节点
                        me._$elem.html(node);
                        // 树根节点插入子节点wrap
                        $treeWrap = me._tplTreeWrap(node);
                    }

                    // 插入子节点列表
                    $treeWrap.html(me._tplItems(resp));

                    // 支持多选且没有子部门，只有人员
                    if (me.get('multiSelect') 
                        && me.get('needPerson') 
                        && !resp.children.length 
                        && resp.person.length
                    ) {
                        var $checker = $('<label>'+
                                            '<input type="checkbox" class="fd-checkbox"/>'+
                                            '<span class="fd-checkbox-face"></span>'+
                                        '</label>');
                        $treeWrap.parent()
                                 .find('.orgtree-node-icon')
                                 .after($checker);

                        var arr = [];
                        var person = resp.person;
                        for (var i = 0, len = person.length; i < len; i++) {
                            var item = person[i];
                            arr.push({
                                id: item.id,
                                userId: item.wbUserId,
                                name: item.name,
                                photoUrl: item.photoUrl || '/space/c/photo/load?id='
                            });
                        }
                        $checker.children('input').data('info', arr);
                    // 没有人员且没有子部门
                    } else if (!resp.person.length && !resp.children.length) {
                        node.addClass('no-children');
                    }

                }, function (errMsg) {
                    $(node).data('loaded', false);
                    // alert(errMsg);
                });
        },

        _tplItems: function (resp) {
            var t = new Util.Template();

            if (this.get('needPerson')) {
                // 人员部分
                t._('<% _.each(person, function (item) { %>')
                    ._('<li class="orgtree-leaf" data-userid="<%= item.wbUserId %>" data-id="<%= item.id %>">')
                        ._('<label class="fd-checkbox-label ellipsis">')
                            ._('<% for(var i = 0; i < _level; i++){ %>')
                                ._('<b class="orgtree-space"></b>')
                            ._('<% } %>')
                            ._('<input type="checkbox" class="fd-checkbox"/>')
                            ._('<span class="fd-checkbox-face"></span>')
                            ._('<img src="<%= item.photoUrl || "/space/c/photo/load?id="%>" />')
                            ._('<span class="orgtree-person-name" title="<%= item.name %>"><%= item.name %></span>')
                        ._('</label>')
                    ._('</li>')
                ._('<% }); %>');
            }
            
            // 部门部分
            t._('<% _.each(children, function (item) { %>')
                ._('<li class="orgtree-node" data-id="<%= item.id %>" data-level="<%= _level %>">')
                    ._('<span  class="orgtree-node-name ellipsis" title="<%= item.name %>">')
                        ._('<% for(var i = 0; i < _level; i++){ %>')
                            ._('<b class="orgtree-space"></b>')
                        ._('<% } %>')
                        ._('<span class="orgtree-node-icon"></span>')
                        ._('<%= item.name %>')
                    ._('</span>')
                ._('</li>')
            ._('<% }); %>')
            ._('<% if (!person.length && !children.length && needPerson) { %>')
                ._('<li class="orgtree-empty">没有数据！</li>')
            ._('<% } %>');

            return _.template(t.toString())($.extend({
                'needPerson': this.get('needPerson')
            }, resp));
        },

        // 包装请求成功后返回的数据格式
        _tranformData: function (resp) {
            return resp.data;
        },

        // 包装请求的数据格式
        _postData: function (orgId, isRoot, begin, count) {
            var data = {
                eid: this.get('eid'),
                orgId: orgId || '',
                begin: begin || 0,
                count: count || 10000
            };

            return data;
        },

        requestSubTree: function (orgId, isRoot) {
            var deferred = $.Deferred();
            var data = this._postData(orgId, isRoot);
            var me = this;

            $.ajax({
                'url': this.get('url'),
                'type': 'POST',
                'dataType': 'json',
                'data': data
            }).then(function (resp) {
                if (resp && resp.success === true) {
                    resp = me._tranformData(resp);
                    deferred.resolve(resp);
                } else {
                    deferred.reject((resp && resp.error) || '请求组织架构失败');
                }
            }, function () {
                deferred.reject('请求组织架构失败');
            });

            return deferred.promise();
        },

        getPersonInfo: function ($leaf) {
            var info = {
                'id': $leaf.data('id'),
                'userId': $leaf.data('userid'),
                'name': $leaf.find('.orgtree-person-name').text(),
                'photoUrl': $leaf.find('img').attr('src')
            };

            return info;
        },

        getSelectedPersons: function () {
            var $checkeds = this._$elem.find('.orgtree-leaf input[type="checkbox"]:checked');
            var persons = [], me = this;

            $checkeds.each(function (ind, item) {
                var $leaf = $(this).closest('.orgtree-leaf');
                persons.push(me.getPersonInfo($leaf));
            });

            return persons;
        },

        getSelectedDepts: function () {
            var $selected = this.find('.orgtree-node.selected');
            var depts = [];

            $selected.each(function (i, item) {
                var $item = $(item);
                var deptId = $item.data('id');
                var deptName = $item.children('.orgtree-node-name').attr('title');
                depts.push({
                    'id': deptId,
                    'name': deptName
                });
            });

            return depts;
        }

    });

    return OrgTree;
});

/*
 * 选择审批人
 * @author muqin_deng
 * @time 2015/10/13
 */
;(function (factory) {
	factory = factory(jQuery, F.d.Widget, webUtil, _, F.d.OrgTree);

	webUtil.namespace('F.d.SelectApprover', factory);

})(function ($, Widget, Util, _, OrgTree) {
	//主函数
	function SelectApprover(cfg) {
		Widget.call(this);
		return this.render(cfg);
	}

	SelectApprover.prototype = $.extend({}, Widget.prototype, {
		constructor: SelectApprover,
		
		initCfg: function (cfg) {
			this.cfg = $.extend({
				'switchSearch': false,
				'multiSelect': false
			}, cfg);
		},

		renderUI: function () {
			var tpl = new Util.Template();

			tpl._('<div class="fd-dialog fd-select-approver zoomin">')
			       ._('<div class="fd-dialog-hd">')
			           ._('<span class="fd-selectappr-title">设置关键审批人</span>')
			           ._('<span class="fd-dialog-closebtn"></span>')
			       ._('</div>')
			       ._('<div class="fd-dialog-bd">')
			       	   ._('<div class="fd-selectappr-check">')
				           ._('<label class="fd-checkbox-label">')
				               ._('<input type="radio" class="fd-checkbox" name="need-search" value="0"/>')
				               ._('<span class="fd-checkbox-face"></span>')
				               ._('<span class="fd-label-text">需申请人的直接上司</span>')
				           ._('</label>')
				           ._('<label class="fd-checkbox-label">')
				               ._('<input type="radio" class="fd-checkbox" name="need-search" value="1" <%= switchSearch ? "checked" : "" %> />')
				               ._('<span class="fd-checkbox-face"></span>')
				               ._('<span class="fd-label-text">精确查找审批人</span>')
				           ._('</label>')
				       ._('</div>')
				       ._('<div class="fd-selectappr-search">')
				           ._('<div class="fd-selectappr-searchgroup">')
				                ._('<span class="fd-selectappr-icon fd-search-icon"></span>')
				                ._('<input type="text" class="fd-selectappr-searchinput" placeholder="输入员工姓名/手机号"/>')
				                ._('<span class="fd-selectappr-icon fd-search-closebtn"></span>')
				           ._('</div>')
				           ._('<div class="fd-selectappr-org" data-switchpanel="0">')
				               ._('<p class="fd-selectappr-orgtitle">组织架构</p>')
				           ._('</div>')
				           ._('<div class="fd-selectappr-searchresult none" data-switchpanel="1">')
				               ._('<p class="fd-selectappr-searchloading">')
				                   ._('<img src="/freeflow/web/images/loading.gif">')
				               ._('</p>')
				               ._('<p class="fd-selectappr-searchmsg none"></p>')
				               ._('<div class="fd-selectappr-searchlist"></div>')
				           ._('</div>')
				       ._('</div>')
			       ._('</div>')
			       ._('<div class="fd-dialog-ft">')
			           ._('<div class="fd-dialog-btngroup">')
			               ._('<button class="fd-btn fd-btn-primary fd-dialog-confirmbtn">确定</button>')
			               ._('<button class="fd-btn fd-btn-default fd-dialog-cancelbtn">取消</button>')
			           ._('</div>')
			       ._('</div>')
			   ._('</div>');

			tpl = _.template(tpl.toString())(this.cfg);
			
			this._$elem = $(tpl).before();
		},

		bindUI: function () {
			var me = this;

			this._$elem.on('click', '.fd-dialog-closebtn, .fd-dialog-cancelbtn', function () {
				me.destroy();
				me.fire('cancel');
			})
			.on('click', '.fd-dialog-confirmbtn', function () {
				me._onConfirm();
			})
			// .on('change', 'input[name="need-search"]', function () {
			// 	me.showSearchPanel($(this).val() == 1);
			// })
			// 关闭搜索结果
//			.on('click', '.fd-search-closebtn', function (e) {
//				me._onCloseSearch(e);
//			})
			// 选中搜索结果项
			.on('change', '.fd-searchitem-checkbox', function (e) {
				me._onSearchItemChange(e);
			});

			this.eachBind({
				'confirm': 'onConfirm',
				'cancel': 'onCancel'
			});
		},

		ready: function () {
			var me = this;
			this._createOverlayer();
			this._initSearcher();
			this._initOrgTree();
			// this.showSearchPanel(this.get('switchSearch'), true);
		},

		_createOverlayer: function () {
			var $overlayer = $('<div class="fd-overlayer fd-selectappr-overlayer fadein">');
			this._$overlayer = $overlayer;
			this._$elem.before($overlayer);

			return $overlayer;
		},

		// 确定按钮
		_onConfirm: function () {
			var $checkbox = this.find('.fd-selectappr-check input[type="radio"]:checked');
			var returnData = {
				isSelected: false,
				needDeptHead: false,
				person: null
			};

			if ($checkbox.length) {
				returnData.isSelected = true;

				// 申请人直接上司
				if ($checkbox.val() == 0) {
					returnData.needDeptHead = true;
				// 精确查找
				} else if ($checkbox.val() == 1) {
					returnData.person = this.getSearchSelected();
				}
			}

			this.fire('confirm', returnData);
			this.destroy();
		},

		// 获取搜索或组织架构选中的人
		getSearchSelected: function () {
			var person = null;
			// 搜索面板
			if (this.find('.fd-selectappr-searchresult').is(':visible')) {
				var $checkbox = this.find('.fd-searchitem input[type="checkbox"]:checked');

				if ($checkbox.length) {
					var $searchitem = $checkbox.closest('.fd-searchitem');
					var userId = $checkbox.val();
					var name = $searchitem.find('.fd-searchitem-name').text();
					var photoUrl = $searchitem.find('img').attr('src');
					person = {
						userId: userId,
						name: name,
						photoUrl: photoUrl
					};
				}
			// 组织架构
			} else {
				var orgTree = this.get('orgTree');
				var persons = orgTree.getSlectedPersons();
				if (persons.length) {
					person = persons[0];
				}
			}

			return person;
		},

		// showSearchPanel: function (show, isInit) {
		// 	if (show === false) {
		// 		$('.fd-selectappr-search')[isInit ? 'hide' : 'slideUp']();
		// 	} else {
		// 		$('.fd-selectappr-search')[isInit ? 'show' : 'slideDown']();
		// 	}
		// },

		_initSearcher: function () {
			var me = this;
			var $searchInput = this.find('.fd-selectappr-searchinput');

			$searchInput.autoComplete({
				searchUrl: '/im/web/searchUser.do',
				throttle: 200,
				closeBtn: '.fd-search-closebtn',
				onBefore: function () {
					$searchInput.addClass('searching');
					me._switchPanel(1);
					me._searchLoading();
					me._showSearList(false);
					me._showSearchMsg(false);
				},
				onNoSearchVal: function () {
					me._onCloseSearch();
				},
				onSuccess: function (respObj) {
					me._onSearchSuccess(respObj);
				},
				onError: function () {
					me._showSearchMsg('搜索出错了 >_<');
				},
				onAlways: function () {
					me._searchLoading(false);
				}
			});

			this.set('$searchInput', $searchInput);
		},

		_onSearchSuccess: function (respObj) {
			var resp = respObj.data;
			if (resp.success === true && resp.data.count > 0) {
				var listTpl = this._tplSearchList(resp.data.list);
				this.find('.fd-selectappr-searchlist').html(listTpl);
				this._showSearList();
				// 保存人员信息对象
				this.set('searchDataCache', resp.data.list);
			} else {
				this._showSearchMsg('没有找到结果 >_<');
				this._showSearList(false);
			}
		},

		_tplSearchList: function (list) {
			var t = new Util.Template();

			t._('<% _.each(list, function (item) { %>')
				._('<label class="fd-checkbox-label fd-searchitem">')
					._('<div class="fd-searchitem-cbgroup">')
						._('<input type="checkbox" class="fd-checkbox fd-searchitem-checkbox" value="<%= item.wbUserId %>"/>')
				    	._('<span class="fd-checkbox-face"></span>')
				    ._('</div>')
				    ._('<div class="fd-searchitem-profile">')
				    	._('<img src="<%= item.photoUrl || "/space/c/photo/load?id=" %>" />')
				    	._('<div class="fd-searchitem-info ellipsis">')
				    		._('<p>')
				    			._('<span class="fd-searchitem-name"><%= item.name %></span>')
				    			._('<span class="fd-searchitem-jobtitle"><%= item.jobTitle%></span>')
				    		._('</p>')
				    		._('<span class="fd-searchitem-phone"><%= item.phones || item.emails %></span>')
				    	._('</div>')
				    ._('</div>')
				._('</label>')
			 ._('<% }) %>');

			return _.template(t.toString())({
				list: list
			});
		},

		_onSearchItemChange: function (e) {
			var multiSelect = this.get('multiSelect');
			var $target = $(e.target);

			if (!multiSelect && $target.prop('checked')) {
				this.find('.fd-searchitem-checkbox:checked')
				    .not(e.target)
				    .prop('checked', false);
			}
		},

		// 结束搜索
		_onCloseSearch: function () {
//			this.get('$searchInput').removeClass('searching').val('');
			this.find('.fd-selectappr-searchlist').html('');
			this._switchPanel(0);
		},

		_searchLoading: function (show) {
			if (show === false) {
				this.find('.fd-selectappr-searchloading').hide();
			} else {
				this.find('.fd-selectappr-searchloading').show();
			}
		},

		_showSearList: function (show) {
			if (show === false) {
				this.find('.fd-selectappr-searchlist').hide();
			} else {
				this.find('.fd-selectappr-searchlist').show();
			}
		},

		_showSearchMsg: function (msg) {
			if (msg === false) {
				$('.fd-selectappr-searchmsg').html('').hide();
			} else {
				$('.fd-selectappr-searchmsg').html(msg).show();
			}
		},

		// 初始化组织架构
		_initOrgTree: function () {
			var orgTree = new OrgTree({
				'target': '.fd-selectappr-org',
				'eid': '441170',
				'multiSelect': false
			});

			this.set('orgTree', orgTree);
		},

		// 切换搜索结果跟组织架构面板
		_switchPanel: function (index) {
			this.find('[data-switchpanel]').hide();
			this.find('[data-switchpanel=' + index + ']').show();
		},

		destroy: function () {
			this._$elem.off().remove();

			var $overlayer = this._$overlayer;
			$overlayer.remove();
		}

	});

	return SelectApprover;
	
});

;(function (window, doc, _, factory) {
	factory = factory(window, doc, _);

	webUtil.namespace('F.d.Designer', factory);

})(window, document, _, function (window, doc, _) {
	// 获取控件属性
	var getProps = function (type, label, placeholder, otherProps) {
		var obj = {
			'id': '',
			'type': type || '',
			'label': label || '',
			'placeholder': placeholder || '',
			'require': false
		};

		if (otherProps) {
			$.extend(obj, otherProps);
		}

		return obj;
	};

	// 生成每个控件的默认属性
	var textfieldProps = getProps('textfield', '单行输入框', '请输入'),
		textareafieldProps = getProps('textareafield', '多行输入框', '请输入'),
		numberfieldProps = getProps('numberfield', '数字输入框', '请输入'),
		datefieldProps = getProps('datefield', '日期', '请选择', {'format': 'yyyy/MM/dd'}),
		photofieldProps = getProps('photofield', '图片', '请选择'),
		filefieldProps = getProps('filefield', '文件', '请选择'),
		radiofieldProps = getProps('radiofield', '单选框', '请选择', {
			'options': ['选项1', '选项2', '选项3']
		}),
		daterangefieldProps = getProps('daterangefield', '日期区间', '请选择', {
			'labels': ['开始日期', '结束日期'],
			'format': 'yyyy/MM/dd'
		});

	// 校验器的校验规则
	var rules = {
		'notEmpty': function (val) {
			return /[^\s]+/g.test(val);
		},
		'nomoreTen': function (val) {
			return webUtil.getByteLength(val) <= 20;
		},
		'nomoreTwenty': function (val) {
			return webUtil.getByteLength(val) <= 40;
		}
	};

	var labelChecker = {
		'type': 'label',
		'rules': ['notEmpty', 'nomoreTen'],
		'tips': ['标题不能为空'],
		'value': 'label'
	};

	var placeholderChecker = {
		'type': 'placeholder',
		'rules': ['nomoreTwenty'],
		'value': 'placeholder'
	};

	var drfieldStartChecker = {
		'type': 'starttime',
		'rules': ['notEmpty', 'nomoreTen'],
		'tips': ['标题不能为空'],
		'value': 'labels'
	};

	var drfieldEndChecker = {
		'type': 'endtime',
		'rules': ['notEmpty', 'nomoreTen'],
		'tips': ['标题不能为空'],
		'value': 'labels'
	};

	// 控件集合
	var designer = {
		'textfield': {
			props: textfieldProps,
			validator: {
				checkers: [labelChecker, placeholderChecker],
				handler: getHandler(errorHandler)
			}
		},

		'textareafield': {
			props: textareafieldProps,
			validator: {
				checkers: [labelChecker, placeholderChecker],
				handler: getHandler(errorHandler)
			}
		},

		'numberfield': {
			props: numberfieldProps,
			validator: {
				checkers: [labelChecker, placeholderChecker],
				handler: getHandler(errorHandler)
			}
		},

		'radiofield': {
			props: radiofieldProps,
			validator: {
				checkers: [labelChecker],
				handler: getHandler(errorHandler)
			}
		},

		'datefield': {
			props: datefieldProps,
			validator: {
				checkers: [labelChecker],
				handler: getHandler(errorHandler)
			}
		},

		'daterangefield': {
			props: daterangefieldProps,
			validator: {
				checkers: [drfieldStartChecker, drfieldEndChecker],
				handler: getHandler(drfieldErrHandler)
			}
		},

		'photofield': {
			props: photofieldProps,
			getWidgetTpl: getWidgetTpl,
			validator: {
				checkers: [labelChecker],
				handler: getHandler(errorHandler)
			}
		},
		
		'filefield': {
			props: filefieldProps,
			getWidgetTpl: getWidgetTpl,
			validator: {
				checkers: [labelChecker],
				handler: getHandler(errorHandler)
			}
		},

		// 控件模板
		'getWidgetTpl': getWidgetTpl,

		// 设置模板
		'getSettingTpl': getSettingTpl
	};

	// 获取校验器处理函数
	function getHandler(errorHandler) {
		return function (compProps, checkerType) {
			var deferred = $.Deferred();
			var checkers = this.checkers;
			var checker, result;
			// 遍历校验器
			for (var i = 0, len = checkers.length; i < len; i++) {
				checker = checkers[i];
				if (!checkerType || (checkerType && checkerType == checker.type)) {
					// 遍历校验规则
					for (var j = 0, l = checker.rules.length; j < l; j++) {
						result = errorHandler(j, compProps, checker);
						if (result === false) {
							deferred.reject();
							break;
						}
					}
				}
			}

			deferred.resolve();
			return deferred.promise();
		};
	}

	// 通用的错误处理
	function errorHandler(index, compProps, checker) {
		if (!rules[checker.rules[index]].call(null, compProps.props[checker.value])) {
			var $elem = $('.fd-settings').find('.fd-setting-' + checker.type);
			$elem.find('input').addClass('error');
			$elem.find('.fd-setting-iteminfo')
				 .addClass('error')
				 .html(checker.tips ? checker.tips[index] : undefined);

			return false;
		}

		return true;
	}

	// 时间区间的错误处理
	function drfieldErrHandler(index, compProps, checker) {
		var labelIndex = checker.type == 'starttime' ? 0 : 1;

		if (!rules[checker.rules[index]].call(null, compProps.props.labels[labelIndex])) {
			var $daterangeLabel = $('.fd-setting-daterangelabel');
			$daterangeLabel.find('input').eq(labelIndex).addClass('error');
			$daterangeLabel.find('.fd-setting-iteminfo')
						   .eq(labelIndex)
				 		   .addClass('error')
				 		   .html(checker.tips ? checker.tips[index] : undefined);

			return false;
		}

		return true;
	}

	// 控件基础html模板
	function getWidgetTpl(data) {
		// 是否需要“进入”右箭头
		var needEnterIcon = {
			'radiofield': 1,
			'datefield': 1,
			'daterangefield': 1,
			'photofield': 1,
			'filefield': 1
		};

		data = $.extend({needEnterIcon: needEnterIcon}, data);

		var baseTpl = [
			'<div class="fd-component fd-compnent-<%= type %>">',
				'<span class="fd-component-remove"></span>',
				'<div class="fd-component-overlayer"></div>',
				// 日期区间类型
				'<% if (type == "daterangefield") { %>',
					'<div class="fd-component-group">',
						'<label class="fd-component-label"><%= labels[0] %></label>',
						'<span class="fd-component-placeholder ellipsis blue">',
							'<%= placeholder + (require ? "(必填)" : "") %>',
						'</span>',
						'<i class="fd-component-entericon"></i>',
					'</div>',
					'<div class="fd-component-group">',
						'<label class="fd-component-label"><%= labels[1] %></label>',
						'<span class="fd-component-placeholder ellipsis blue">',
							'<%= placeholder + (require ? "(必填)" : "") %>',
						'</span>',
						'<i class="fd-component-entericon"></i>',
					'</div>',
					
				// 非日期期间类型
				'<% } else { %>',
					'<div class="fd-component-group">',
						'<label class="fd-component-label"><%= label %></label>',
						'<span class="fd-component-placeholder ellipsis<%= needEnterIcon[type] ? " blue" : "" %>">',
							'<%= placeholder + (require ? "(必填)" : "") %>',
						'</span>',
						'<% if (needEnterIcon[type]) { %>',
							'<i class="fd-component-entericon"></i>',
						'<% } %>',
					'</div>',
				'<% } %>',
			'</div>'
		].join('');

		return _.template(baseTpl)(data);
	}

	// 设置区域的html模板
	function getSettingTpl(data) {
		// 需要提示语的类型
		var needPlaceholderType = {
			'textfield': 1,
			'textareafield': 1,
			'numberfield': 1
		};

		data = $.extend({'needPlaceholderType': needPlaceholderType}, data); 

		var settingTpl = [
			'<div class="fd-settings">',
				// 设置项的标题
				'<% if (type != "daterangefield") { %>',
					'<div class="fd-setting-item fd-setting-label">',
						'<div class="fd-setting-itemtitle">',
							'<span>标题</span>',
							'<span class="fd-setting-iteminfo" data-tips="最多10个字">最多10个字</span>',
						'</div>',
						'<div class="fd-setting-itembody">',
							'<input type="text" value="<%= label %>"/>',
						'</div>',
					'</div>',
				'<% } else { %>',
					'<div class="fd-setting-item fd-setting-daterangelabel">',
						'<div class="fd-setting-itemtitle">',
							'<span>标题1</span>',
							'<span class="fd-setting-iteminfo" data-tips="最多10个字">最多10个字</span>',
						'</div>',
						'<div class="fd-setting-itembody">',
							'<input type="text" class="fd-daterange-input1" value="<%= labels[0] %>"/>',
						'</div>',
						'<div class="fd-setting-itemtitle">',
							'<span>标题2</span>',
							'<span class="fd-setting-iteminfo" data-tips="最多10个字">最多10个字</span>',
						'</div>',
						'<div class="fd-setting-itembody">',
							'<input type="text" class="fd-daterange-input2" value="<%= labels[1] %>"/>',
						'</div>',
					'</div>',
				'<% } %>',

				// 需要配置提示语
				'<% if (needPlaceholderType[type]) { %>',
					'<div class="fd-setting-item fd-setting-placeholder">',
						'<div class="fd-setting-itemtitle">',
							'<span>提示语</span>',
							'<span class="fd-setting-iteminfo" data-tips="最多20个字">最多20个字</span>',
						'</div>',
						'<div class="fd-setting-itembody">',
							'<input type="text" value="<%= placeholder %>"/>',
						'</div>',
					'</div>',
				'<% } %>',

				// 日期类型，需要选择日期格式
				'<% if (type == "datefield" || type == "daterangefield") { %>',
					'<div class="fd-setting-item fd-setting-dateformat">',
						'<div class="fd-setting-itemtitle">',
							'<span>日期类型</span>',
							'<span class="fd-setting-iteminfo"></span>',
						'</div>',
						'<label class="fd-setting-itembody">',
							'<input type="radio" name="dateformat" value="yyyy/MM/dd" <%= format == "yyyy/MM/dd" ? "checked" : "" %>/>',
							'<span>年/月/日</span>',
						'</label>',
						'<label class="fd-setting-itembody">',
							'<input type="radio" name="dateformat" value="yyyy/MM" <%= format == "yyyy/MM" ? "checked" : "" %>/>',
							'<span>月/日</span>',
						'</label>',
					'</div>',
				'<% } %>',

				// 单选框，提供选项
				'<% if (type == "radiofield") { %>',
					'<div class="fd-setting-item fd-setting-options">',
						'<div class="fd-setting-itemtitle">',
							'<span>选项</span>',
							'<span class="fd-setting-iteminfo" data-tips="最多50项，每项最多20个字">最多50项，每项最多20个字</span>',
						'</div>',
						'<div class="">',
							'<% _.each(options, function (option) { %>',
								'<div class="fd-setting-itembody">',
									'<input type="text" value="<%= option %>" maxlength="20"/>',
									'<span class="fd-option-del">&minus;</span>',
									'<span class="fd-option-add">+</span>',
								'</div>',
							'<% }); %>',
						'</div>',
					'</div>',
				'<% } %>',
				// 设置项是否必填部分
				'<div class="fd-setting-item fd-setting-require">',
					'<label class="fd-setting-itembody">',
						'<input type="checkbox" value="1" <%= require === true ? "checked" : "" %>/>',
						'<span>必填</span>',
					'</label>',
				'</div>',
			'</div>'
		].join('');

		return _.template(settingTpl)(data);
	};

	return designer;
});
;
(function(factory) {
    factory = factory(window, document, jQuery, mTouch);

    webUtil.namespace('F.d.FormArea', factory);

})(function(window, doc, $, mTouch) {
    var Widget = F.d.Widget;
    var Designer = F.d.Designer;

    function FormArea(cfg) {
        Widget.call(this);
        return this.render(cfg);
    }

    FormArea.prototype = $.extend({}, Widget.prototype, {
        constructor: FormArea,

        initCfg: function(cfg) {
            this.cfg = $.extend({
                'dragTarget': '', // 拖动元素的选择器
                'onInAear': function() {}, // 拖动元素进入设计器区域的监听
                'onLeaveAear': function() {}, // 拖动元素离开设计器区域的监听
                'onDragStart': function() {}, // 拖动开始的监听
                'onDraging': function() {}, // 拖动中的监听
                'onDragEnd': function() {}, // 拖动开始的监听
                'onSelectComp': function() {}, // 选择控件的监听
                'onDelComp': function() {} // 删除控件的监听
            }, cfg);
        },

        ready: function() {
            var me = this;
            var occupyLine = document.createElement('div');
            occupyLine.className = 'fd-occupyline';
            // 占位线
            this.set('occupyLine', occupyLine);
        },

        // 根据表单数据初始化表单控件
        renderForm: function(components) {
            var me = this;
            var $components = $();

            components.forEach(function(item) {
                $components = $components.add(me._tplComponent$(item.type, item));
            });

            this._$elem.append($components);
            
            //默认显示第一个
            var currElem = this._$elem.find('.fd-component').eq(0);
            currElem.addClass('active');
            var currComponent = this._packageComponent(currElem);
            this.set('currComponent', currComponent);
            this.fire('selectComp', currComponent);
        },
        
        renderFormStep2: function(content) {
            var me = this;
            var contentList = '';
            
            for (x in content){
            	contentList += '<div class="approval-person-photo person-photo" id="'+ content[x].oId +'"><img src="'+ content[x].photo +'"/><p>'+ content[x].name +'</p></div>';
            }
			
			$('#addKeyApprover').before(contentList);
        },

        renderUI: function() {
            var tpl = '<div class="<%= className %>">';

            this._$elem = $(_.template(tpl)({
                'className': this.get('className')
            }))
        },

        // 绑定外部事件
        _bindHandler: function() {
            this.eachBind({
                'inAear': 'onInAear',
                'leaveAear': 'onLeaveAear',
                'dragStart': 'onDragStart',
                'draging': 'onDraging',
                'dragEnd': 'onDragEnd',
                'selectComp': 'onSelectComp',
                'delComp': 'onDelComp'
            });
        },

        bindUI: function() {
            var me = this;

            this._bindHandler();

            this._$elem.on('mouseenter', '.fd-component', function() {
                    $(this).addClass('hover');
                })
                .on('mouseleave', '.fd-component', function() {
                    $(this).removeClass('hover');
                })
                .on('mousedown', '.fd-component-remove', function() {
                    return false;
                })
                .on('click', '.fd-component-remove', function(e) {
                    me._removeComponent(e, this);
                });

            // 无限延长长按事件的触发
            mTouch.config({
                'longTapDelay': 1000000000
            });

            // 如果有拖动的目标元素，绑定拖动事件
            if (this.get('dragTarget')) {
                mTouch('body').on('swipestart', this.get('dragTarget'), function(e) {
                    me._swipeStart(e, this, true);
                });
            }

            mTouch('body').on('swipestart', '.fd-component', function(e) {
                    me._swipeStart(e, this, false);
                    me.selectComp(this);
                })
                .on('swiping', function(e) {
                    if (!me.get('_start')) {
                        return;
                    }

                    if (Math.abs(e.mTouchEvent.moveX) > 2 || Math.abs(e.mTouchEvent.moveY) > 2) {
                        me.listenDraging(e);
                    }

                })
                .on('swipeend', function(e) {
                    // 拖动结束
                    me._swipeEnd(e);
                });
        },

        // 删除控件
        _removeComponent: function(e, elem) {
            var $parent = $(elem).closest('.fd-component'),
                currElem = $parent[0],
                id = $parent.data('id');

            if ($parent.hasClass('active')) {
                if ($parent.nextAll('.fd-component').length) {
                    this.selectComp($parent.nextAll('.fd-component')[0])
                } else if ($parent.prevAll('.fd-component').length) {
                    this.selectComp($parent.prevAll('.fd-component')[0])
                } else {
                    var firstComponent = this.find('.fd-component')[0];
                    firstComponent ? this.selectComp(firstComponent) : this.set('currComponent', null);
                }
            }

            $parent.off().remove();
            this.delComponentProps(id);

            this.fire('delComp', this._packageComponent(currElem));
        },

        // 监听控件在区域内拖动
        listenDraging: function(e) {
            var myPosition = $(this.get('target'))[0].getBoundingClientRect(),
                mtouch = e.mTouchEvent;

            this._dragStart(e);


            var dragElem = this.get('_dragElem').elem;
            this.fire('draging', e, this._packageComponent(dragElem));

            // 判断元素是否拖动进入设计框区域
            if (mtouch.pageX >= myPosition.left && mtouch.pageX <= myPosition.left + myPosition.width && mtouch.pageY >= myPosition.top && mtouch.pageY <= myPosition.top + myPosition.height) {
                this._inAear(e);
            } else {
                this._leaveAear(e);
            }
        },

        // 进入区域
        _inAear: function(e) {
            if (!this.get('_isInAear')) {
                this.set('_isInAear', true);
                this.fire('inAear');
                this._showOccupyLine(component);
            }

            // 根据拖动坐标获取坐标最近的控件节点
            var component = this._getComponentByPos(e.clientX, e.clientY);

            if (this.get('_component') === component) {
                return;
            }

            this._showOccupyLine(component);
            this.set('_component', component);
        },

        // 离开区域
        _leaveAear: function(e) {
            if (this.get('_isInAear')) {
                this.set('_isInAear', false);
                this._$elem.removeClass('dragin');
                this._hideOccupyLine();
                this.fire('leaveAear');
            }

            this.set('_component', null);
        },

        // 移动开始
        _dragStart: function(e) {
            if (!this.get('_dragStart')) {
                var dragElemObj = this.get('_dragElem');

                this.set('_dragStart', true);
                this.fire('dragStart', e, this._packageComponent(dragElemObj.elem));

                this._$elem.addClass('draging');
                if (dragElemObj.isNew === false) {
                    $(dragElemObj.elem).addClass('draging');
                }
            }
        },

        // 按钮鼠标开始
        _swipeStart: function(e, elem, isNew) {
            // 不是鼠标左键
            if (e.button !== 0) {
                return;
            }
            e.preventDefault();
            this.set('_start', true);
            this.set('_dragElem', {
                elem: elem,
                isNew: isNew
            });
        },

        _swipeEnd: function(e) {
            if (!this.get('_start')) {
                return;
            }

            if (this.get('_isInAear')) {
                var dragElemObj = this.get('_dragElem');
                var dragElem = dragElemObj.elem;

                // 需要添加新的控件
                if (dragElemObj.isNew) {
                    var dragElemType = dragElem.getAttribute('data-type');
                    var filefieldLen = $(".fd-designer-inner").find(".fd-compnent-filefield").length;
                    var photofieldLen = $(".fd-designer-inner").find(".fd-compnent-photofield").length;
                    if(filefieldLen > 0 && dragElemType == 'filefield'){
                    	webUtil.tips('只能设置一个文件上传控件');
                    	this.fire('dragEnd', e);
                    	this._hideOccupyLine();
                    	return;
                    }
                    if(photofieldLen > 0 && dragElemType == 'photofield'){
                    	webUtil.tips('只能设置一个图片上传控件');
                    	this.fire('dragEnd', e);
                    	this._hideOccupyLine();
                    	return;
                    }
                    dragElem = this._tplComponent$(dragElemType)[0];
                }

                var component = this.get('_component');

                if (component === null) {
                    this._$elem.append(dragElem);
                } else {
                    $(component).before(dragElem);
                }

                this.selectComp(dragElem);

                this._hideOccupyLine();
            }

            this._$elem.removeClass('draging');
            $(this.get('currComponent').srcElem).removeClass('draging');

            this.resetStatus();

            this.fire('dragEnd', e);
        },

        resetStatus: function() {
            this.set('_start', false);
            this.set('_dragElem', null);
            this.set('_dragStart', false);
            this.set('_component', null);
            this.set('_isInAear', false);
        },

        // 选中控件
        selectComp: function(currElem) {
            this.find('.active').removeClass('active');
            $(currElem).addClass('active');

            var currComponent = this._packageComponent(currElem);
            this.set('currComponent', currComponent);
            this.fire('selectComp', currComponent);
        },

        // 显示占位线
        _showOccupyLine: function(component) {
            var occupyLine = this.get('occupyLine');
            occupyLine.style.display = 'block';

            if (!component) {
                this._$elem.append(occupyLine);
            } else {
                component.parentNode.insertBefore(occupyLine, component);
            }
        },

        // 隐藏占位线
        _hideOccupyLine: function() {
            this.get('occupyLine').style.display = 'none';
        },

        // 根据拖动坐标获取坐标最近的控件节点
        _getComponentByPos: function(x, y) {
            var components = this._$elem.children('.fd-component');

            if (!components.length) {
                return null;
            }

            var firstPos = components[0].getBoundingClientRect();

            for (var i = 0, len = components.length; i < len; i++) {
                // 第一个
                if (y <= (firstPos.top + firstPos.height / 2)) {
                    return components[0];
                }

                // 最后一个
                if (i == len - 1) {
                    return null;
                }

                var pos = components[i].getBoundingClientRect(),
                    nextPos = components[i + 1].getBoundingClientRect();

                if (y >= (pos.top + pos.height / 2) && y <= (nextPos.top + nextPos.height / 2)) {
                    return components[i + 1];
                }
            }
        },

        // 拼装控件html
        _tplComponent$: function(componentType, props) {
            if (Designer[componentType]) {
                var props = $.extend(true, {}, props || Designer[componentType].props);
                var $component = $(Designer.getWidgetTpl(props));
                var id = webUtil.uuid(8, 32);

                if (!props.id) {
                    /*id = props.id.replace(props.type + '-', '');
                } else {*/
                    props.id = id = props.type + '-' + id;
                }

                $component.data('id', id);
                this.addComponentProps(id, props);
            }

            return $component;
        },

        _packageComponent: function(elem) {
            var $elem = $(elem);

            return {
                srcElem: elem,
                props: this.getComponentProps($elem.data('id')),
                isActive: $elem.hasClass('active')
            }
        },

        addComponentProps: function(id, props) {
            if (!this._components) {
                this._components = {};
            }

            this._components[id] = props;
        },

        getComponentProps: function(id) {
            return (id && this._components[id]);
        },

        delComponentProps: function(id) {
            delete this._components[id];
        },

        getAllComponents: function() {
            var $components = this.find('.fd-component');
            var arr = [],
                me = this;

            $components.each(function() {
                arr.push(me._packageComponent(this));
            });

            return arr;
        }
    });

    return FormArea;
});
;(function (factory) {
	factory = factory(window, document);

	webUtil.namespace('F.d.controller', factory);

})(function (window, doc) {
	var Designer = F.d.Designer;
	var SelectApprover = F.d.SelectApprover;
    var __Global_Approve_Arr;//有审批人数据时的全局变量
	var controller = {
		data: {
            
			getFormUrl: '/freeflow/v1/rest/form-template/detail/formTemplate?',	// 获取表单设置控件url
			urlParam:  'webLappToken=RTXcirUwxXeNjfmsL%2BRqCRmDhx5TESR0a2PDRAY2fJoL%2B1YXjGBviOsFIDkq50RS8CacivV474WoWy2mrH530BJa14DvsiDc9Sp8Bjrzkvw%3D&lappName=freeflow',
			saveUrl: '/freeflow/v1/rest/form-template/create/formTemplate.json',
			reviseUrl: '/freeflow/v1/rest/form-template/update/formTemplate',
			formArea: null,
			selectApprover: null,
			approverData: {

			},
			addKeyApproverArr: [],//指定关键审批人信息
			addApproverArr: [],//指定审批人信息
            curApprType: 0  // 0为不指定审批人， 1为关键审批人， 2为普通审批人
		},

		init: function () {
			var me = this;

			this.initFormArea();	// 初始化控件拖放
			this.bindUI();			// 绑定事件
            //this.setAuthToken();    //为每个请求的url添加授权token
            
			webUtil.tab('.fd-widgetpanel', 'curr');	// tab切换

			var formId = webUtil.getUrlValue('formId');

			//编辑将之前选择模板生成
			if (formId) {
				this.getFormDataPost(formId).done(function (resp) {
					me.renderForm(resp.data);
				});
			}
		},

		renderForm: function (data) {
			//第一步内容加载
			var title = data.template.title;
			$('.fd-form-name').val(title);
			var content = JSON.parse(data.template.components);
			this.data.formArea.renderForm(content);
			//第二步内容加载
			var numbers = data.approvalRule.content;
			this.data.formArea.renderFormStep2(numbers);
			__Global_Approve_Arr = data.approvalRule.content;
			//
			/*var props = content.props;
			var settingTpl = Designer.getSettingTpl(props);
			$('.fd-settingpanel .fd-panel-body').html(settingTpl);*/
		},

		// 初始化控件拖放区域
		initFormArea: function () {
			var formArea = new F.d.FormArea({
				'target': '.fd-designer',
				'className': 'fd-designer-inner',
				'dragTarget': '.fd-widgetitem'	// 拖动元素的选择器
			})
			.on('dragStart', this._onDragStart.bind(this))
			.on('draging', this._onDraging.bind(this))
			.on('dragEnd', this._onDragEnd.bind(this))
			// 选择控件的监听
			.on('selectComp', this._onSelectComp.bind(this))
			// 删除控件的监听
			.on('delComp', this._onDelComp.bind(this));

			this.data.formArea = formArea;
		},

		_onSelectComp: function (component) {
			var props = component.props;

			var settingTpl = Designer.getSettingTpl(props);

			$('.fd-settingpanel .fd-panel-body').html(settingTpl);
			// 校验数据
			this.validate();
		},

		_onDelComp: function(component) {
			if (component.isActive) {
				$('.fd-settingpanel .fd-panel-body').html('');
			}
		},

		_onDragStart: function (e, component) {
			var	type = component.props ? component.props.type : $(component.srcElem).data('type');

			$('html').addClass('fd-cursor-move');
			this.showDraingMask(e, type);
		},

		_onDraging: function (e, component) {
			var $mask = $('.fd-draging-mask'),
				mtouch = e.mTouchEvent;

			var translateStr = 'translate3d(' + mtouch.moveX + 'px,' + mtouch.moveY + 'px,0)'

			$mask.css({
				'transform': translateStr,
				'-webkit-transform': translateStr,
				'-moz-transform': translateStr,
				'-o-transform': translateStr,
				'-ms-transform': translateStr
			});
		},

		_onDragEnd: function (e) {
			$('html').removeClass('fd-cursor-move');
			this.hideDraginMask();
		},

		// 绑定事件
		bindUI: function () {
			var me = this;

			// 标题输入
			$('body').on('input', '.fd-setting-label input', function (e) {
				me._onLabelChange('label', this.value, 20);
			})
			// 提示语输入
			.on('input', '.fd-setting-placeholder input', function (e) {
				me._onLabelChange('placeholder', this.value, 40);
			})
			// 时间区间的标题
			.on('input', '.fd-daterange-input1, .fd-daterange-input2', function () {
				var index = $(this).hasClass('fd-daterange-input1') ? 0 : 1;
				me._onDrLabelChange(index, this.value);
			})
			// 单选框选项
			.on('input', '.fd-setting-options input', function () {
				var component = me.getCurrComponent();	// 当前选中控件和属性
				var index = $('.fd-setting-options input').index(this);

				component.props.options[index] = this.value;
			})
			// 添加选项
			.on('click', '.fd-option-add', function (e) {
				me._optionItemHandler(e, 'add');
			})
			// 删除选项
			.on('click', '.fd-option-del', function (e) {
				me._optionItemHandler(e, 'del');
			})
			// 日期格式
			.on('change', 'input[name="dateformat"]', function () {
				var component = me.getCurrComponent();	// 当前选中控件和属性

				component.props.format = this.value;
			})
			// 是否必填
			.on('change', '.fd-setting-require input', function () {
				var component = me.getCurrComponent();	// 当前选中控件和属性
				var $placeholder = component.$comp.find('.fd-component-placeholder');
				var oldVal = $placeholder.html();

				component.props.require = this.checked;

				$placeholder.html(this.checked ? oldVal + '(必填)' : oldVal.replace(/(\(必填\))$/g, ''));
			})
            // 选择是否需要审批人
			.on('change', 'input[name=need-approver]', function (e) {
				me._onNeedApprover(e);
			})
			// 保存
			.on('click', '.fd-btn-save', function (e) {
				me._save(e);
			})
            //下一步
            .on('click', '.fd-btn-next', function(e){
               me._nextPage(e)
            })
            //上一页
            .on('click', '.fd-btn-pre', function(e){
                var self = this;
                $('.fd-bd-main').css({
                    "transform": "translateX(0)"
                });
                $('.fd-bd-nextPage').css({
                    "transform": "translateX(110%)"
                });
                setTimeout(function(){
                    $(self).hide();
                    $('.fd-btn-next').show();
                    $('.step-two').removeClass('fd-design-active');
	                $('.step-one').addClass('fd-design-active');
                }, 0)
                //隐藏保存按钮
                $('.fd-btn-save').hide();
            })
            //切换选择审批人tab
            .on('click', '.fd-appr-type .tab', function(e){
                $('.fd-appr-type .tab').removeClass('active');
                $('.fd-appr-con').hide();
                $(this).addClass('active');
                $('.fd-appr-con').eq($(this).index()).show();
                me.data.curApprType = $(this).index();
            })
            //添加关键审批人
            .on('click', '#addKeyApprover', function(e){
            	var ele = $(this);
           
                me.showOrganizational(ele, me.data.curApprType);
            })
            .on('click', '#addApprover', function(e){
               var ele = $(this);
               console.log(me.data)
               me.showOrganizational(ele, me.data.curApprType);
            })
		},
        //切换到下一步
        _nextPage: function(){
            var me = this;
			var formArea = this.data.formArea;
			var allComponents = formArea.getAllComponents();
			if (!allComponents.length) {
				$('.fd-widgetpanel [data-panel="1"]').click();
				webUtil.tips('请先添加表单控件');
				return ;
			}else{
				for (var i = 0, len = allComponents.length; i < len; i++) {
					var component = allComponents[i];
					var errors = $(component.srcElem).data('errors');
					if (errors && errors.length) {
						// 选中有错的控件
						formArea.selectComp(component.srcElem);
						return ;
					}
				}
				$('.fd-bd-main').css({
	            "transform": "translateX(-110%)"
	            });
	            $('.fd-bd-nextPage').css({
	            "transform": "translateX(0)"
	            });
	            setTimeout(function(){
	                $('.fd-btn-next').hide();
	                $('.fd-btn-pre').show();
	                $('.step-one').removeClass('fd-design-active');
	                $('.step-two').addClass('fd-design-active');
	            }, 0)
	            //显示保存按钮
	            $('.fd-btn-save').show();
			}
        },
        //添加审批人调出组织架构
		showOrganizational: function(ele, curApprType){
			var me = this;
			$('#body').addClass('blur');
			var selectedPersonArr = [];
			var flag = true;
			if(curApprType == 1){
				if(flag && typeof(__Global_Approve_Arr) != "undefined"){
					selectedPersonArr = __Global_Approve_Arr;
					flag = false;
				}else{
					selectedPersonArr = me.data.addKeyApproverArr;
				}
			}
			else if(curApprType == 2){
				selectedPersonArr = me.data.addApproverArr;
			}

			new LappPersonSelect({
				parentElement: 'body',	
				//appId: '10103',
				//lightAppId: '',
				eId: __global.eid,
				selectedMembers: me.selectedApprovalPerson(selectedPersonArr), //记录上次选了哪些人
				//openId: '5638397000b0e5cbba2743c1',
				cancelCallBack: cancel,	          //取消回调
				sureCallBack: sureCallBack,       //确定回调
				//removeCallBack: removeCallBack,    //删除回调
				existingSessionIsNeed: false		
        	})
        	

			//取消回调
			function cancel(resp){
				console.log(resp);
				$('#body').removeClass('blur');
				//$('input[name="need-approver"][value="0"]').prop('checked', true);
			}

			//确定回调
			function sureCallBack(resp){

				var selectedArr = [];
				console.log(resp);
				$('#body').removeClass('blur');
				var html = '';
				var eleId = $(ele)[0].id;
				$.each(resp, function(key, val){
					var obj = {
						oId: val.userId,
						name: val.userName,
						photo: val.photoUrl
					};

					selectedArr.push(obj);
					
					html += '<div class="approval-person-photo"><img src="'+ val.photoUrl +'"/><p>'+ val.userName +'</p></div>';
				})

				if(curApprType == 1){
					me.data.addKeyApproverArr = selectedArr;
				}
				else if(curApprType == 2){
					me.data.addApproverArr = selectedArr;
				}

				html += '<div class="add-btn" id="'+ eleId +'"><span class="hengxian"></span><span class="shuxian"></span></div>';
				$(ele).parent().html(html);
			}

			//删除回调
			/*function removeCallBack(resp){
				arr = [];
				$.each(resp, function(key, val){
					var obj = {
						oId: val.userId,
						name: val.userName,
						photo: val.photoUrl
					};

					arr.push(obj);
				})
			}*/
		},

		//记录已选择审批人信息
		selectedApprovalPerson: function(arr){
			var obj = {};
			if(arr.length){
				$.each(arr, function(index, val){
					obj[val.oId] = {
						userId: val.oId,
						photoUrl: val.photo,
						userName: val.name
					};
				})
			}
				
			return obj;
		},

		// 保存操作
		_save: function (e) {
			var me = this;
			var formArea = this.data.formArea;

			// 先校验审批设置数据 
			if (!this._checkFormSetting()) {
				return ;
			}
			
			var allComponents = formArea.getAllComponents();

			if (!allComponents.length) {
				$('.fd-widgetpanel [data-panel="1"]').click();
				webUtil.tips('请先添加表单控件');
				return ;
			}

			for (var i = 0, len = allComponents.length; i < len; i++) {
				var component = allComponents[i];
				var errors = $(component.srcElem).data('errors');
				if (errors && errors.length) {
					// 选中有错的控件
					formArea.selectComp(component.srcElem);
					return ;
				}
			}
			// 提交数据
			var data = this._getData(allComponents);
            //审批人数据
            var approvalRule;
            if(this.data.curApprType === 0) {
                approvalRule = {
                    "approvalType": "0"
                };
            }
            if(this.data.curApprType === 1){
                approvalRule = {
                    "approvalType": "1",
                    "content": JSON.stringify(this.data.addKeyApproverArr)
                };
            }
            if (this.data.curApprType === 2){
                approvalRule = {
                    "approvalType": "2",
                    "content": JSON.stringify(this.data.addApproverArr)
                };
            }

            var formId = webUtil.getUrlValue('formId'),
            	common = webUtil.getUrlValue('common'),
            	params;

            //是修改
            if(formId && !common){
            	params = {
	                "template": {
	                	"id": formId,
	                	"appName":"freeflow",//应用名称必传 有问题找小周
	                    "title": $('.fd-step2 .fd-form-name').val(),
	                    "components": JSON.stringify(data.components)
	                },
	                "approvalRule": approvalRule
	            }
            }
            //新建保存
            else{
            	params = {
	                "template": {
	                	"appName":"freeflow",//应用名称必传 有问题找小周
	                    "title": $('.fd-step2 .fd-form-name').val(),
	                    "components": JSON.stringify(data.components)
	                },
	                "approvalRule": approvalRule
	            }
            }
			this._createFormPost(params);
		},

		// 获取数据
		_getData: function (allComponents) {
			var title = $('.fd-form-name').val().trim();
			var needApprover = $('input[name="need-approver"]:checked').val();

			var components = [];

			allComponents.forEach(function (item) {
				components.push(item.props);
			});

			return {
				title: title,
				approverInfo: this.data.approverData,
				components: components
			};
		},

		// 创建表单配置
		_createFormPost: function (data) {
			var deferred = $.Deferred();
			var formId = webUtil.getUrlValue('formId');
			var url = '';
			if(formId){
				url = this.data.reviseUrl;
			}else{
				url = this.data.saveUrl;
			}
			$.ajax({
				'type': 'POST',
				'url': url,
				'contentType': 'application/json;charset=UTF-8',
				'data': JSON.stringify(data)
			}).then(function (resp) {
				console.log(resp);
				if (resp.meta.status === 'success') {
					deferred.resolve(resp);
					alert('保存成功');
					location.href = "approval-manage.json";
				} else {
					deferred.reject(resp.errorMsg || '保存失败了');
					alert(resp.errorMsg);
				}
			}, function (resp) {
				deferred.reject('保存失败了');
			});

			return deferred.promise();
		},

		// 校验审批设置数据
		_checkFormSetting: function () {
			var $formName = $('.fd-step2 .fd-form-name');

 			if (!$formName.val().trim()) {
 				webUtil.tips('请填写审批名称');
				$('.fd-widgetpanel [data-panel="0"]').click();
				$formName.focus();
 				return false;
 			}

			return true;
		},

		// 输入内容改变监听
		_onLabelChange: function (type, value, valLengh) {
			var component = this.getCurrComponent();	// 当前选中控件和属性
			var placeholder = type == 'placeholder' && component.props.require ? '(必填)' : '';

			component.props[type] = value;
			component.$comp.find('.fd-component-' + type)
						   .html(webUtil.getByteVal(value, valLengh) + placeholder);

			var errors = component.$comp.data('errors') || [];

			this.validate(type).done(function () {
				if (errors.indexOf(type) > -1) {
					//重置状态
					var $elem = $('.fd-settings').find('.fd-setting-' + type);
					$elem.find('input').removeClass('error');

					var $itemInfo =  $elem.find('.fd-setting-iteminfo');
					$itemInfo.removeClass('error').html($itemInfo.data('tips'));

					errors.splice(errors.indexOf(type), 1);
					component.$comp.data('errors', errors);
				}
			}).fail(function () {
				if (errors.indexOf(type) == -1) {
					errors.push(type);
					component.$comp.data('errors', errors);
				}
			});
		},

		// 时间区间标题改变
		_onDrLabelChange: function (index, value) {
			var component = this.getCurrComponent();	// 当前选中控件和属性
			var checkerType = index === 0 ? 'starttime': 'endtime';

			component.props.labels[index] = value;
			component.$comp.find('.fd-component-label')
						   .eq(index)
						   .html(webUtil.getByteVal(value, 20));

			var errors = component.$comp.data('errors') || [];

			this.validate(checkerType).done(function () {
				if (errors.indexOf(checkerType) > -1) {
					//重置状态
					var $daterangeLabel = $('.fd-setting-daterangelabel');
					$daterangeLabel.find('input').eq(index).removeClass('error');

					var $itemInfo =  $daterangeLabel.find('.fd-setting-iteminfo').eq(index)
					$itemInfo.removeClass('error').html($itemInfo.data('tips'));

					errors.splice(errors.indexOf(checkerType), 1);
					component.$comp.data('errors', errors);
				}
			}).fail(function () {
				if (errors.indexOf(checkerType) == -1) {
					errors.push(checkerType);
					component.$comp.data('errors', errors);
				}
			});
		},

		// 新增或删除选项
		_optionItemHandler: function (e, type) {
			var component = this.getCurrComponent();	// 当前选中控件和属性
			var options = component.props.options;
			var isAdd = type == 'add';
			var $target = $(e.target);
			var $settingOptions = $target.closest('.fd-setting-options');

			if (isAdd) {
				if (options.length >= 50) {
					return ;
				}

				if (options.length == 49) {
					$settingOptions.addClass('nomore-add');
				}
				$settingOptions.removeClass('limit-del');
			} else {
				if (options.length <= 1) {
					return ;
				}

				if (options.length == 2) {
					$settingOptions.addClass('limit-del');
				}
				$settingOptions.removeClass('nomore-add');
			}

			var $itembody = $target.closest('.fd-setting-itembody');
			var index = $itembody.index();

			// 添加选项
			if (isAdd) {
				// 确定新的选项名字
				for (var i = 1, len = options.length; i <= len; i++) {
					if (options.indexOf('选项' + i) == -1) {
						break;
					}
				}
				var newOption = '选项' + i;

				var $newItemBody = $itembody.clone();
				$newItemBody.find('input').val(newOption);

				// 插入新的选项
				options.splice(index + 1, 0, newOption);
				$itembody.after($newItemBody);
			} else {
				options.splice(index, 1);
				$itembody.remove();
			}
		},

		// 数据校验
		validate: function (checkerType) {
			var component = this.getCurrComponent();
			var type = component.props.type;

			var promise = Designer[type].validator.handler(component, checkerType);

			return promise;
		},

		getCurrComponent: function () {
			var currComponent = this.data.formArea.get('currComponent');

			if (!currComponent) {
				return ;
			}

			return {
				props: currComponent.props,
				$comp: $(currComponent.srcElem)
			}
		},

		// 选择是否需要审批人
		_onNeedApprover: function (e) {
			var me = this;
			var $target =  $(e.target);

			if ($target.val() == 1) {
				/*this.data.selectApprover = new SelectApprover({
					'switchSearch': false
				})*/

				//取消回调
				/*var cancel = function(resp){
					console.log(resp);
					$('#body').removeClass('blur');
					$('input[name="need-approver"][value="0"]').prop('checked', true);
				}

				//确定回调
				var sureCallBack = function(resp){
					console.log(resp);
					$('#body').removeClass('blur');
				}
				new LappPersonSelect({
											parentElement: 'body',	
											//appId: '10103',
											//lightAppId: '',
											eId: '1708351',
											//openId: '5638397000b0e5cbba2743c1',
											cancelCallBack: cancel,	
											sureCallBack: sureCallBack,
											existingSessionIsNeed: false		
					        	})*/
				/*$('#body').removeClass('blur');
				.on('confirm', function (data) {
					
					if (!data.isSelected || !data.needDeptHead && !data.person) {
						$('input[name="need-approver"][value="0"]').prop('checked', true);
						return ;
					}
					
					me.data.approverData = {
						deptLeader: data.needDeptHead ? true : false,
						openIds: [data.person.oId]
					};
				})
				.on('cancel', function () {
					$('#body').removeClass('blur');
					$('input[name="need-approver"][value="0"]').prop('checked', true);
				});*/

				//$('#body').addClass('blur');
			} else {

			}
		},

		// 显示拖动的朦层
		showDraingMask: function (e, type) {
			var $mask = $('.fd-draging-mask');

			$mask.html($('.fd-widgetitem[data-type=' + type + ']').html());

			var width = $mask.outerWidth(),
				height = $mask.outerHeight();

			$mask.css({
				'left': (e.mTouchEvent.startX - width / 2) + 'px',
				'top': (e.mTouchEvent.startY - height / 2) + 'px'
			}).show();
		},

		// 隐藏拖动的朦层
		hideDraginMask: function () {
			$('.fd-draging-mask').hide();
		},

		// 获取表单数据
		getFormDataPost: function (formId) {
			var deferred = $.Deferred();

			$.ajax({
				'url': this.data.getFormUrl + this.data.urlParam,
				'type': 'POST',
				'contentType': 'application/json;charset=UTF-8',
				'data': JSON.stringify({id: formId})
			})
			.always(function (resp) {
				if (resp && resp.meta.status === 'success') {
					deferred.resolve(resp);
				} else {
					deferred.reject(resp);
				}
			});

			return deferred.promise();
		},
        setAuthToken: function () {
            //全局请求设置-->为每个请求带上授权token
            //获取url query
            var req = webUtil.getRequest();
            if (req.lappName && req.webLappToken) {
                $.ajaxSetup({
                    beforeSend: function (e, data) {
                        console.log(e, data)
                        data.url += '?lappName=' + req.lappName + '&webLappToken=' + req.webLappToken;
                        return true;
                    }
                });
            }
        }
	};

	return controller;
});
;(function (window, doc, factory) {
	factory = factory(window, doc);

	window.F.d.Manage = window.F.d.Manage || factory;

})(window, document, function (window, doc) {
	var Paginator = F.d.Paginator;
	var  OrgTree =  F.d.OrgTree;
	var data = {
		// operationLogUrl: '/rest/template/operationlog/get-list.json',
		//operationLogUrl: '/freeflow/web/data/log.txt?' + (+new Date()),
		// updateUrl: '/rest/form-template/update.json',
		updateUrl: '/freeflow/web/data/log.txt',
		// tplListUrl: '/rest/form-template/get-list.json',
		//tplListUrl: '/freeflow/web/data/data.txt?' + (+new Date()),
		createTplUrl: '/freeflow/web/form-design.json',
		
		operationLogUrl: '/freeflow/v1/rest/form-template/list/operationLogs?'  + (+new Date()),  //操作日志
		tabListRemoveUrl: '/freeflow/v1/rest/form-template/update/formTemplate?' + (+new Date()), //表格列表删除
		
		//审批类型
		queryApprovalType: {
			url: '/freeflow/v1/rest/approval/list/approvalTypes?',
			limit: 10,
			pageIndex: 1,
			first: true
		},
		//审批模板配置--表格
		approvalTemplateConfiguration: {
			url: '/freeflow/v1/rest/form-template/list/formTemplates?'  + (+new Date()),
			limit: 10,
			pageIndex: 1,
			totalSize: null
		},
		//审批数据导出--表格
		approvalDataExport: {
			url: '/freeflow/v1/rest/approval/list/approvalApplys?' + (+new Date()),
			limit: 10,
			pageIndex: 1,
			first: true,
			totalSize: null
		},
		approvalTemplatePaginator: null,                      //审批模板分页
		approvalDataExportPaginator: null,                   //审批数据分页
		urlParam:  '&webLappToken=RTXcirUwxXeNjfmsL%2BRqCRmDhx5TESR0a2PDRAY2fJoL%2B1YXjGBviOsFIDkq50RS8CacivV474WoWy2mrH530BJa14DvsiDc9Sp8Bjrzkvw%3D&lappName=freeflow',
		operationLogType: ['删除了', '创建了', '修改了'], //操作日志类型
		status: {
			blockUp: '-1',                  //停用
			notEnabled: '0',				//未启用
			alreadyEnabled: '1'			   //已启用
		},

		//查询数据
		checkData: {
			userName: null,             //发起人姓名
			userId: '',               //发起人id                    
			deptId: '' ,                    //查询部门id
			limit: 10,
			pageIndex: 1,
			totalSize: null
		},
		viewDetailUrl: '/freeflow/m/approval/',    //审批详情接口
		
	};

	var tipMsg = {
		stopSuc: '已停用！',
		activeSuc: '已启用！',
		updateFail: '操作失败，请稍后再试！',
		delSuc: '删除成功！',
		delFail: '删除失败，请稍后再试！',
		approvalType: '获取审批类型失败，请稍后再试！',
		noData: '<tr><td class="am-manage-no-data" colspan="5">无数据</td></tr>',
		checkDateFail: '查询失败，请稍后再试！'
	};

	//生成数据模板
	function templateEngine(html, options) {
		    var re = /<%(.*?)%>/g, 
		    	reExp = /(^(\s|\t|\n|\r)*(var|if|for|else|switch|case|break|{|}))(.*)?/g, 
		    	code = 'var r=[];\n', 
		    	cursor = 0;
		    
		    html = html.replace(/\n|\r|\t/g, '');

		    var add = function(line, js) {
		        js? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
		            (code += line !== '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
		        return add;
		    };
		    
		    while(match = re.exec(html)) {
		    	var noJsStr = html.slice(cursor, match.index);
		        add(/[^\s]/g.test(noJsStr) ? noJsStr : '')(match[1], true);
		        cursor = match.index + match[0].length;
		    }
		    add(html.substr(cursor, html.length - cursor));
		    code += 'return r.join("");';
		    
		    return new Function(code.replace(/[\r\t\n]/g, '')).apply(options ? options : {});
	}

	//生成表格列表数据
	function renderUITabList(curPage, limit){
		var paramStr = {
			    query:{
			       //"title":"", //标题
			       //"code":"",  //类型
			       //"user":{}// 简单用户信息
			       //"ltSaveTime":22222222,// 大于保存时间
			       //"gtSaveTime":11111111// 小于保存时间
			       //"isPublic": true,// 是否公开
			       "isDelete": false,// 是否删除
			       "appName": "freeflow"
			    },
			    "order": "-createTime ",
			    "start": curPage,
			    "limit": limit, 
			    "isTotal": true
		};
		var optData = {
			url: data.approvalTemplateConfiguration.url + data.urlParam,
			param: JSON.stringify(paramStr)
		}
		
		getTabListAjax(optData).done(function(resp){
			//console.log(resp);
			data.approvalTemplateConfiguration.totalSize = resp.data.total;
			var dataArr = resp.data.templates;
			var html = '';
			if(!dataArr.length){
				noData('.am-manage-tbody', tipMsg.noData);
				return;
			}
			for(var i = 0, len = dataArr.length; i < len; i++) {
				//表格数据
				
				var tpl = [
				 		'<tr data-status="<% this.status %>" data-isPublic="<% this.isPublic %>" data-id="<% this.id %>">',
					 		'<td><% this.title %></td>',
					 		'<% if(this.status == this.statusNum.alreadyEnabled) { %>',
					 			'<td class="am-skyblue">已启用</td>',
					 		'<% } else if(this.status == this.statusNum.blockUp) { %>',
					 			'<td class="am-red">已停用</td>',
					 		'<% } else{ %>',
					 			'<td>未启用</td>',
					 		'<% } %>',
					 		'<td><% this.userName %></td>',
					 		'<td><% this.createTime %></td>',
					 		'<td>',
					 			'<% if(this.isPublic) { %>',
					 				'<a class="am-edit-btn am-skyblue" href="javascript:void(0)">编辑</a>',
					 				'<a class="approval-person-set-btn am-skyblue" href="javascript:void(0)">审批人设置</a>',
						 			'<% if(this.status == this.statusNum.alreadyEnabled) { %>',
						 				'<a class="am-stop-btn am-red" href="javascript:void(0)">停用</a>',
						 			'<% } else {%>',
						 				'<a href="javascript:;" class="am-start-btn am-skyblue">启用</a>',
						 				'<a class="am-del-btn am-red" href="javascript:void(0)">删除</a>',
						 			'<% } %>',
								'<% } else { %>',
									'<a class="am-edit-btn" href="javascript:void(0) am-skyblue">编辑</a>',
									'<a class="approval-person-set-btn am-skyblue" href="javascript:void(0)">审批人设置</a>',
					 				'<% if(this.status == this.statusNum.alreadyEnabled) { %>',
					 					'<a class="am-stop-btn am-red" href="javascript:void(0)">停用</a>',
					 				'<% } else {%>',
					 					'<a href="javascript:void(0)" class="am-start-btn am-skyblue">启用</a><% } %>',
					 					'<a class="am-del-btn am-red" href="javascript:void(0)">删除</a>',
									'<% } %>',
							'</td>',
						'</tr>'
				        ].join(''); 

				//resp[i] = $.extend(dataArr[i], {createTime: dateFormat(dataArr[i].datetime)});
        	    html += templateEngine(tpl, {
					'status': dataArr[i].status,
					'createTime': dateFormat(dataArr[i].createTime),
					'isPublic': dataArr[i].isPublic,
					'title': dataArr[i].title,
					'userName': dataArr[i].user ? dataArr[i].user.name : '',
					'id': dataArr[i].id,
					'statusNum': data.status
				});
			}
			$('.am-manage-tbody').html('');
			$('.am-manage-tbody').html(html);

			if(data.hasInitPaginatorF != true){
				//分页
				var opt = {
					'target': '.approval-template-pagiantion',
					'pageSize': data.approvalTemplateConfiguration.limit,
					'currentPage': data.approvalTemplateConfiguration.pageIndex,
					'itemsTotal': data.approvalTemplateConfiguration.totalSize
				};
				initPaginatorTabF(opt);
				data.hasInitPaginatorF = true;
			}

		}).fail(function(){
			console.log('生成表格列表数据失败')
		})
	}
	
	//生成公用模板数据
	function renderComList(curPage, limit){
		var paramStr = {
			    query:{
			       //"title":"", //标题
			       //"code":"",  //类型
			       //"user":{}// 简单用户信息
			       //"ltSaveTime":22222222,// 大于保存时间
			       //"gtSaveTime":11111111// 小于保存时间
			       "isPublic": true,// 是否公开
			       "isDelete": false,// 是否删除
			       "appName": "freeflow"
			    },
			    "order": "-createTime",
			    "start": curPage,
			    "limit": limit, 
			    "isTotal": true
		};
		var optData = {
			url: data.approvalTemplateConfiguration.url + data.urlParam,
			param: JSON.stringify(paramStr)
		}
		
		getTabListAjax(optData).done(function(resp){
			//console.log(resp);
			data.approvalTemplateConfiguration.totalSize = resp.data.total;
			var dataArr = resp.data.templates;
			var html = '';
			if(!dataArr.length){
				//noData('.am-manage-tbody', tipMsg.noData);
				return;
			}
			for(var i = 0, len = dataArr.length; i < len; i++) {
				//公共模板数据
				var tpl = [
						'<li class="am-popbox-item" id="<% this.id %>">',
						  '<img src="<% this.photo %>" >',
						  '<p><% this.title %></p>',
						'</li>'
				        ].join(''); 

				//resp[i] = $.extend(dataArr[i], {createTime: dateFormat(dataArr[i].datetime)});
        	    html += templateEngine(tpl, {
					'title': dataArr[i].title,
					'id': dataArr[i].id,
					'photo': dataArr[i].photo
				});
			}
			$('.am-blank').nextAll().remove();
			$('.am-blank').after(html);

			if(data.hasInitPaginatorF != true){
				//分页
				var opt = {
					'target': '.approval-template-pagiantion',
					'pageSize': data.approvalTemplateConfiguration.limit,
					'currentPage': data.approvalTemplateConfiguration.pageIndex,
					'itemsTotal': data.approvalTemplateConfiguration.totalSize
				};
				initPaginatorTabF(opt);
				data.hasInitPaginatorF = true;
			}

		}).fail(function(){
			console.log('生成公用模板数据失败')
		})
	}

	//tab1 分页  ----   审批模板
	function initPaginatorTabF(opt){
		data.approvalTemplatePaginator = new Paginator({
			'target': opt.target,
			'className': '',
			'edgeNum': 2,
			'middleNum': 7,
			'pageSize': opt.pageSize,
			'currentPage': opt.currentPage,
			'itemsTotal': opt.itemsTotal
		});
		data.approvalTemplatePaginator.on('selectPage', function (page) {
			//console.log(page);
			//点击页码刷新数据
			renderUITabList(page, data.approvalTemplateConfiguration.limit);

		});
	}

	//tab2 分页  ---- 审批数据导出
	function initPaginatorTabS(opt){
		data.approvalDataExportPaginator = new Paginator({
			'target': opt.target,
			'className': '',
			'edgeNum': 2,
			'middleNum': 7,
			'pageSize': opt.pageSize,
			'currentPage': opt.currentPage,
			'itemsTotal': opt.itemsTotal
		});
		data.approvalDataExportPaginator.on('selectPage', function (page) {
			//console.log(page);
			//点击页码刷新数据
			approvalDataExport(page, data.approvalDataExport.limit);

		});
	}

	//操作日志列表
	function operationLog(){
		var optData = {
			url: data.operationLogUrl + data.urlParam,
			param: {
				query: {
				   //"operation":"", //操作类型
				   //"traget":"", //关联主体
				   //"tragetId":"",  //关联id
				 //"user":{}// 简单用户信息
				 //"ltSaveTime":22222222,// 大于保存时间
				 //"gtSaveTime":11111111// 小于保存时间
				   "isDelete": false// 是否删除
				},
				order: "createTime",
				start: 1,
				limit: 10, 
				isTotal: true
			}
		}
		optData.param = JSON.stringify(optData.param);

		
		getTabListAjax(optData).done(function(resp){
			//console.log(resp)
			var html = '';
			var dataArr = resp.data.templates;
			if(!dataArr.length){
				return;
			}
			//operation 0 ---- 删除  1 -----  创建   2 ----- 修改
			for(var i = 0; i < dataArr.length; i ++){
				var tpl = [
							'<li>',
								'<span class="operation-log-username"><% this.userName %>&nbsp;</span>',
								'<span class="operation-log-create-time">于<b><% this.createTime %>&nbsp;</b></span>',
								'<span class="operation-log-type<% this.className %>"><b><% this.createType %></b>审批模板</span>',
								'<span class="operation-log-title">【<% this.title %>】</span>',
						    '</li>'
					].join('');
				
				html += templateEngine(tpl, {
					'userName': dataArr[i].user ? dataArr[i].user.name : '',
					'createTime': dateFormat(dataArr[i].createTime),
					'createType': data.operationLogType[dataArr[i].operation],
					'title': dataArr[i].content,
					'className': dataArr[i].operation
				});
			}

			$('.am-log-list').html(html);
		}).fail(function(){
			console.log('操作日志列表请求失败')
		})
	}

	//异步取数据ajax封装
	function getTabListAjax(optData){
		var url = optData.url;
		var param = optData.param;
		var deferred = $.Deferred();

		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'JSON',
			contentType: 'application/json;charset=UTF-8',
			data: param
		})
		.done(function(resp) {
			if(resp.meta.status == 'success') {
				 deferred.resolve(resp);
			} else {
				 deferred.reject();
			}
		})
		.fail(function() {
			 deferred.reject();
		});

		return deferred.promise();
	}
	function bindUI() {
		var $overlay = $('.am-overlay'),
		    $popbox = $('.am-popbox');

		$('body').on('click', '.am-create-approval', function() {
			renderComList(0,8);
			$overlay.show();
			$popbox.show();
		})
		.on('click', '.am-popbox-close', function() {
			$overlay.hide();
			$popbox.hide();
		})
		.on('click', '.am-popbox-item', function() {
			var id = $(this).closest('li').attr('id');
			if(typeof id == "undefined"){
				window.location.href = data.createTplUrl;
			}else{
				window.location.href = data.createTplUrl + '?formId=' + id + '&common=true';
			}
		})
		.on('click', '.am-nav button', function() {
			var index = $(this).index();
			$('.am-nav button').removeClass('am-curr');
			$(this).addClass('am-curr');
			if(index === 0) {
				$('.am-export-content').hide();
				$('.am-manage-content').show();
				$('.approval-btn').hide();
				$('.am-create-approval').show();
			} else {
				$('.am-manage-content').hide();
				$('.am-export-content').show();
				$('.approval-btn').show();
				$('.am-create-approval').hide();

				//获取审批类型
				if(data.queryApprovalType.first){
					queryApprovalType();
				}

				//加载第二个tab页
				if(data.approvalDataExport.first){
					approvalDataExport(data.approvalDataExport.pageIndex, data.approvalDataExport.limit);
				}
				
			}
		})
		//查看单据
		.on('click', '.view-details-btn', function(){
			var url = data.viewDetailUrl +　  $(this).data('id') + '?' + (+new Date()) + data.urlParam;
			$('.view-details-frame-content').attr('src', url);
			$overlay.show();
			$('.view-details-frame').show();



		})
		//打印
		.on('click', '.print-details-btn', function(){

		})
		//查看单据 打印 取消
		$('.view-details-cancel').on('click', function(){
			$overlay.hide();
			$('.view-details-frame').hide();
		})

		$('.am-manage-tbody').on('click', '.am-edit-btn', function(e) {
			var id = $(this).closest('tr').data('id');
			window.location.href = data.createTplUrl + '?formId=' + id;
		});

		//启用
		$('.am-manage-tbody').on('click', '.am-start-btn', function(){
			var $me = $(this),
			    $tr = $me.closest('tr'),
			    id = $tr.data('id');
			    //status = $tr.data('status');
		    var paramStr = {
			    //"approvalRule":{}
			    "template":{
			        "id": id,
			        "status": data.status.alreadyEnabled
			    }
			}
			var optData = {
				url: data.tabListRemoveUrl + data.urlParam,
				param: JSON.stringify(paramStr)
			}

			getTabListAjax(optData).done(function() {
				$tr.find('td').eq(1).addClass('am-skyblue').text('已启用');
				var html = '<a class="am-stop-btn am-red" href="javascript:void(0)">停用</a>';
				$tr.find('.am-start-btn').remove();
				$tr.find('.am-del-btn').remove();
				$tr.find('td:last').append(html);
				notify(tipMsg.activeSuc);
			})
			.fail(function() {
				notify(tipMsg.updateFail);
			});
		})

		//停用
		$('.am-manage-tbody').on('click', '.am-stop-btn', function(e) {
			var $me = $(this),
			    $tr = $me.closest('tr'),
			    id = $tr.data('id');

			var paramStr = {
			    //"approvalRule":{}
			    "template":{
			        "id": id,
			        "status": data.status.blockUp
			    }
			}
			var optData = {
				url: data.tabListRemoveUrl + data.urlParam,
				param: JSON.stringify(paramStr)
			}

			getTabListAjax(optData).done(function() {
				$tr.find('td').eq(1).removeClass('am-skyblue').addClass('am-red').text('已停用');
				var html = '<a href="javascript:;" class="am-start-btn am-skyblue">启用</a><a class="am-del-btn am-red" href="javascript:void(0)">删除</a>';
				$tr.find('.am-stop-btn').remove();
				$tr.find('td:last').append(html);
				notify(tipMsg.stopSuc);
			})
			.fail(function() {
				notify(tipMsg.updateFail);
			});
		});

		//删除
		$('.am-manage-tbody').on('click', '.am-del-btn', function(e) {
			var $me = $(this),
			    $tr = $me.closest('tr'),
			    id = $tr.data('id');
			    //status = $tr.data('status');
		    var paramStr = {
			    //"approvalRule":{}
			    "template":{
			        "id": id,
			        "isDelete": true
			    }
			}
			var optData = {
				url: data.tabListRemoveUrl + data.urlParam,
				param: JSON.stringify(paramStr)
			}

			getTabListAjax(optData).done(function() {
				$tr.remove();
				notify(tipMsg.delSuc);
				if($('.am-manage-tbody tr').length == 0　){
					document.location.reload();
				}
			})
			.fail(function() {
				notify(tipMsg.updateFail);
			});
		});

		
	}

	function initTpl() {
		/*var tpl = '',
			compiled = '';

		getTplData().done(function(resp) {
			for(var i = 0, len = resp.length; i < len; i++) {
				 tpl = [
				 		'<tr data-status="<%= status %>" data-isPublic="<%= isPublic %>">',
					 		'<td><%= templateName %></td>',
					 		'<% if(status == 0) { %><td>已启用</td><% } else { %><td class="am-red">已停用</td><% } %>',
					 		'<td><%= updateName %></td>',
					 		'<td><%= createTime %></td>',
					 		'<td>',
					 			'<% if(isPublic) { %><a class="am-edit-btn" href="javascript:void(0)">编辑</a>',
					 			'<a class="am-stop-btn" href="javascript:void(0)"><% if(this.status == 0) { %>停用<% } else {%>启用<% } %></a>',
								'<a class="am-del-btn" href=""></a>',
								'<% } else { %><a class="am-edit-btn" href="javascript:void(0)">编辑</a>',
					 			'<a class="am-stop-btn" href="javascript:void(0)"><% if(this.status == 0) { %>停用<% } else {%>启用<% } %></a>',
								'<a class="am-del-btn" href="javascript:void(0)">删除</a><% } %>',
							'</td>',
						'</tr>'
				        ].join(''); 
				resp[i] = $.extend(resp[i], {createTime: dateFormat(resp[i].datetime)});
        	    compiled += _.template(tpl)(resp[i]);
			}
		    $('.am-manage-tbody').append(compiled);
		});*/
		

		
	}

	function getTplData() {
		var deferred = $.Deferred();

		$.ajax({
			url: data.tplListUrl,
			type: 'POST',
			dataType: 'JSON',
			// contentType: 'application/json;charset=UTF-8',
			data: {

			}
		})
		.done(function(resp) {
			if(resp.success) {
				 deferred.resolve(resp.data.details);
			} else {
				 deferred.reject();
			}
		})
		.fail(function(resp) {
			 deferred.reject();
		});

		return deferred.promise();
	}

	function showOperateLog() {
		var tpl = '',
			compiled = '';

		getLogData().done(function(resp) {
			for(var i = 0, len = resp.length; i < len; i++) {
				 tpl = [
				 		 '<li class="am-log-item" data-action="<%= remark %>"><span class="am-log-col"><%= updateName %></span>',
				         '于<%= updateDate %>',
				         '<% if(remark == 1) { %>',
				         '<span class="am-blue">创建了',
				         '<% } else if(remark == 2) { %>',
				         '<span class="am-yellow">修改了',
				         '<% } else { %>',
				         '<span class="am-red">删除了<% } %>',
				         '</span>审批模板<span class="am-log-col">【<%= templateName %>】</span></li>'
				        ].join(''); 
				resp[i] = $.extend(resp[i], {updateDate: dateFormat(resp[i].updateDate)});
        	    compiled += _.template(tpl)(resp[i]);
			}
		    $('.am-log-list').append(compiled);
		});
	}

	function getLogData() {
		var deferred = $.Deferred();

		$.ajax({
			url: data.operationLogUrl,
			type: 'POST',
			dataType: 'JSON',
			// contentType: 'application/json;charset=UTF-8',
			data: {

			}
		})
		.done(function(resp) {
			if(resp.success) {
				 deferred.resolve(resp.data.details);
			} else {
				 deferred.reject();
			}
		})
		.fail(function(resp) {
			 deferred.reject();
		});

		return deferred.promise();
	}

	function apdateAjax(status) {
		var deferred = $.Deferred();

		$.ajax({
			url: data.tplListUrl,
			type: 'POST',
			dataType: 'JSON',
			// contentType: 'application/json;charset=UTF-8',
			data: {

			}
		})
		.done(function(resp) {
			if(resp.success) {
				 deferred.resolve();
			} else {
				 deferred.reject();
			}
		})
		.fail(function() {
			 deferred.reject();
		});

		return deferred.promise();
	}

	function notify(msg) {
		var tpl = '<div class="notify">' + msg + '</div>';
		$('body').prepend(tpl);
		var $notify = $('.notify'); 
		$notify.fadeIn(500, function() {
			_.delay(function() {
				$notify.fadeOut(500, function() {
					$notify.remove();
				});
			}, 1000)
		})
	}

	function dateFormat(str){
	        var d = new Date(parseInt(str)),
	        year = d.getFullYear(),
	        month = d.getMonth() < 9  ? '0'+ parseInt(d.getMonth() + 1) : d.getMonth() + 1,
	        day = d.getDate() < 10  ? '0' + d.getDate() : d.getDate(),
	        hour = d.getHours() < 10  ? '0' + d.getHours() : d.getHours(),
	        minute = d.getMinutes()  < 10  ? '0' + d.getMinutes() : d.getMinutes(),
	        second = d.getSeconds()  < 10  ? '0' + d.getSeconds() : d.getSeconds();
	             
            return year + '年' + month + '月' + day + '日 ' + hour + ':' + minute;
	}

	//选择部门
	function selectorDepartment(){
		var eid = __global.eid;
		var orgId = __global.orgId;//'dfc2889d-77c5-4074-ab36-8db18886cc03';

		$.extend(OrgTree.prototype, {
			_postData: function (orgId, isRoot) {
				var data = {
					deptId: orgId
				};

				if (isRoot) {
					data.isFirst = true
				}

				return data;
			},

			_tranformData: function (resp) {
				var data = resp.openDeptDetail;
				var newData = {
					id: data ? data.id : '',
					name: data ? data.name : '',
					person: [],
					children: data ? data.subDept : []
				};

				return newData;
			},
		});

		var orgTree = new OrgTree({
			'url': '/attendance/rest/querydept',
			'target': '.fd-selectappr-org',
			'orgId': orgId,
			'needSelectDept': true,
            'needPerson': false,
			'multiSelect': false
		});

		//查询部门
		$('.am-department').on('click', function(){
			$('.am-overlay').fadeIn();
			$('.selector-department').fadeIn();
		})

		//关闭 取消按钮
		$('.selector-department-guanbi').add('.selector-department-quxiao').on('click',function(){
			$('.am-overlay').hide();
			$('.selector-department').hide();
		});

		//点击确定选择部门信息
		$('.selector-department-true').on('click',function(){
			var depts = orgTree.getSelectedDepts();
			var dept = depts[0];
			if(dept){
				$('.am-department input').val(dept.name);
				data.checkData.deptId = dept.id;
				/*me.data.isInclude = $('.selector-department-children input[type=checkbox]').prop('checked');*/
				$('.am-department b').show();
			}else{
				$('.selector-department-tips').show();
				setTimeout(function(){
					$('.selector-department-tips').hide();
				},1500)
				return false;
			}
			$('.selector-department').hide();
			$('.am-overlay').hide();
		});


		//选择部门清空按钮
		$('.am-department b').on('click',function(e){
			e.stopPropagation();
			$('.am-department input').val('');
			$(this).hide();
			data.checkData.deptId = '';
		})
	}

	//查询审批类型
	function queryApprovalType(){
		var paramStr = {
			    "order": "-updateTime",
		        "start": data.queryApprovalType.pageIndex,
		        "limit": data.queryApprovalType.limit,
		};
		var optData = {
			url: data.queryApprovalType.url + data.urlParam,
			param: JSON.stringify(paramStr)
		}
		
		getTabListAjax(optData).done(function(resp){
			data.queryApprovalType.first = false;
			var dataArr = resp.data.approvalTypes;
			var html = '<option value="">全部</option>';
			if(!dataArr.length){
				data.checkData.approvalType = '';
				html += '<option value="">暂无类型</option>';
				$('.am-approve-type select').html(html);
				return;
			}
			for(var i = 0;i < dataArr.length; i ++){
				html += '<option value="' + dataArr[i].id + '">' + dataArr[i].title + '</option>';
			}
			$('.am-approve-type select').html(html);
		}).fail(function(){
			notify(tipMsg.approvalType);
		})
	}


	//搜索框
	function userNameSerch(){
			$('.am-promoter input').autoComplete({
				searchUrl: '/im/web/searchUser.do',
				data: {
					count: 10
				},

				onBefore: function () {
					$('.job-log-report-serchk').show();
					$('.job-log-report-serchk div').show();
				},
				onNoSearchVal: function () {
					$('.job-log-report-serchk ul').html('');
					$('.job-log-report-serchk').hide();
					data.checkData.userName = '';
					data.checkData.userId = '';
				},
				onSuccess: function (respObj) {
					var data = respObj.data.data.list;
					var html = '';
					$.each(data,function(key,val){
						if(val.phones == ''){
							val.phones = val.emails;
						}
						html += '<li data-userid="'+ val.wbUserId +'" data-username="' + val.name + '"><i style="padding-right:5px;">' + val.name + '</i><i>' + val.phones + '</i></li>';
					}) ;

					$('.job-log-report-serchk div').hide();
					$('.job-log-report-serchk ul').html(html);
					if(!data || !data.length){
						$('.job-log-report-serchk ul').html('搜索无结果');
						data.checkData.userName = '';
						data.checkData.userId = '';
					}
				},
				onError: function () {
					$('.job-log-report-serchk ul').html('搜索无结果');
				},
				onAlways: function () {
					
				}
			})
		}

		//搜索框鼠标划过
		function userNameSerchHover(){
			var me = this;
			//鼠标划过
			$('.job-log-report-serchk').on('mouseover','li',function(){
				$('.job-log-report-serchk .hover').removeClass('hover');
				$(this).addClass('hover');
			})

			//点击选人
			$('.job-log-report-serchk').on('click','li',function(){
				data.checkData.userId = $(this).data('userid');
				data.checkData.userName = $(this).data('username');
				$('.job-log-report-serchk').hide();
				$('.am-promoter input').val(data.checkData.userName);
			})

			//失去焦点
			$('.am-promoter input').on('click',function(e){
				e.stopPropagation();
			}).on('focus', function () {
				if (this.value) {
					$('.job-log-report-serchk').show();
				}
			});

			$(document).click(function () {
				$('.job-log-report-serchk').hide();
			});
		}

		//审批数据导出--表格
		function approvalDataExport(curPage, limit, isCheckData, checkDataParam){
			var paramStr = {
			    "order": "updateTime",
			    "start": curPage,
			    "limit": limit
			};
			if(isCheckData){
				paramStr = {
					"paramsObject": checkDataParam,
				    "order": "updateTime",
				    "start": curPage,
				    "limit": limit
				};
			}
			var optData = {
				url: data.approvalDataExport.url + data.urlParam,
				param: JSON.stringify(paramStr)
			}
			
			getTabListAjax(optData).done(function(resp){
				//console.log(resp);
				data.approvalDataExport.first = false;
				data.approvalDataExport.totalSize = resp.data.total;
				var dataArr = resp.data.approvalApplys;
				var html = '';

				//如果是查询 分页栏刷新
				if(isCheckData && data.hasInitPaginatorTabS && resp.data.total > limit){
					data.approvalDataExportPaginator._reDraw(1, resp.data.total);
				}
				//无数据
				if(!dataArr.length){
					noData('.am-export-tbody', '<tr><td class="am-manage-no-data" colspan="8">无数据</td></tr>');
					return;
				}

				for(var i = 0; i < dataArr.length; i ++){
									//表格数据
						console.log(dataArr[i].approvalUser);			
						var tpl = [
						 			'<tr>',
						 				'<td><% this.receiptId %></td>',
						 				'<td class="am-skyblue"><% this.title %></td>',
						 				'<td><% this.deptName %></td>',
						 				'<td><% this.userName %></td>',
						 				'<td><% this.createTime %></td>',
						 				'<td>',
						 					'<% for(var i = 0;i < this.approvalUser.length;i ++){ %>',
						 							'<% if(this.approvalUser[i].name == undefined){continue;} %>',
						 							'<% if(i == this.approvalUser.length - 1){ %>',
						 								'<% this.approvalUser[i].name %>',
						 							'<% }else{ %>',
						 								'<% this.approvalUser[i].name %>'  + '>',
						 							'<% } %>',
						 							 
						 					'<% } %>',
						 				'</td>',
						 				'<td>',
						 					'<% if(this.status == "2"){ %> ',
						 							'<% this.createTime %>',
						 					'<% }else{ %>',
						 							'未完成',
						 					'<% } %>',
						 				'</td>',
						 				'<td>',
											'<a class="am-blue view-details-btn" href="javascript:;" data-typeid="<% this.formTemplateId %>" data-id="<% this.id %>">查看单据</a>',
											'<a class="am-blue print-details-btn" href="javascript:;">打印</a>',
										'</td>',
						 			'</tr>'
						        ].join(''); 

						//resp[i] = $.extend(dataArr[i], {createTime: dateFormat(dataArr[i].datetime)});
		        	    html += templateEngine(tpl, {
							'receiptId': dataArr[i].receiptId,                           //单号
							'formTemplateId': dataArr[i].formTemplateId,                 //类型id
							'title': dataArr[i].title,								     //类型名
							'deptName': dataArr[i].createUser.deptName,       //部门名
							'userName': dataArr[i].createUser.name,                      //发起人
							'createTime': dateFormat(dataArr[i].createTime),                         //发起时间
							'approvalUser': dataArr[i].approvalUser,                     //审批人
							'status': dataArr[i].status,                                 //审批状态
							'lastUpdateTime': dataArr[i].lastUpdateTime,                 //审批完成时间
							'id': dataArr[i].id                                          //当前审批id
						});
				}
					$('.am-export-tbody').html('');
					$('.am-export-tbody').html(html);

					//分页
					if(data.hasInitPaginatorTabS != true){
						var opt = {
							'target': '.approval-data-export-pagiantion',
							'pageSize': data.approvalDataExport.limit,
							'currentPage': data.approvalDataExport.pageIndex,
							'itemsTotal': data.approvalDataExport.totalSize
						};
						initPaginatorTabS(opt);
						data.hasInitPaginatorTabS = true;
					}

					
			}).fail(function(){
				console.log('审批数据导出--表格失败！');
				if(isCheckData){
					notify(tipMsg.checkDateFail);
				}
			})
		}

		//无数据
		function noData(ele, noDataText){
			$(ele).html('');
			$(ele).html(noDataText);
		}
		//将时间转换为毫秒数
		function getTime(timeStr){
			if(timeStr){
				var time;
				var y = timeStr.substr(0,4);
				var	m = timeStr.substr(timeStr.indexOf('-') + 1, 2) - 1;
				var	d = timeStr.substr(timeStr.lastIndexOf('-') + 1);
				time = +new Date(y, m, d);
				//console.log(y + ',' + m + ',' + d);
				return time;
			}
		}
		//获取查询参数
		function getCheckData(){
			var formTemplateId = $('.am-approve-type select').val(),   //类型id
				receiptId = $('.am-approve-number input').val(),      //单号
				startTime = getTime($('#d12').val()) || null,
				endTime =getTime($('#d13').val()) || null;

			var checkDataParam = {
					formTemplateId:　formTemplateId,
					user: {
						deptId: data.checkData.deptId,
						userId: data.checkData.userId
					},
					startTime: startTime,
					endTime: endTime,
					receiptId: receiptId
			};

			return checkDataParam;

		}
		//查询按钮
		function checkMessage(){
			

			$('.approval-data-chaxun').on('click', function(){
				console.log(getCheckData());
				//查询刷新页面
				approvalDataExport(data.checkData.pageIndex, data.checkData.limit, true, getCheckData());
			})
		}

	return {
		init: function() {
			bindUI();
			showOperateLog();
			initTpl();

			//生成表格列表
			renderUITabList(data.approvalTemplateConfiguration.pageIndex, data.approvalTemplateConfiguration.limit)
			
			//操作日志列表
			operationLog();

			//发起人搜索框
			userNameSerch();
			
			//搜索框鼠标划过
			userNameSerchHover();

			//选择部门
			selectorDepartment();

			//查询按钮
			checkMessage();

		}
	};
});
(function(window, $, undefined){
	function LappPersonSelect(cfg){
		this.selectedCount = 0;
		this.xhrList = [];
		this.selectedMembers = cfg.selectedMembers;
		this.selectedGroupId = '';
		this.orgPath = [];
		this.orgCache = {};
		this._$root = null;
		this._$parentElement = null;
		this.contactList = [];				  // 会话列表数据
		this.sessionLoading = true;         // 是否需要显示会话loading
		this.loadContactsComplete = false;    // 判断会话列表是否加载完成
		this.historyLen = 0;                  // 选人组件的历史记录数
		this.scrollIndex = 1;               
		this.cfg = {
			parentElement: 'body',	
			appId: '',				// 公众号id
			lightAppId: '', 		// 轻应用id	
			eId: '',				// 企业id
			openId: '',				// 用户id
			selectedMembers: '',	//记录上次选了哪些人			
			appName: '',            // 应用名称
			theme: '',			    // 主题，如传入，创建组时以此命名组名称
			title: '',			    // 分享弹窗时的标题，也是会话组消息的标题
			content: '',		    // 分享弹窗界面内容
			cellContent: '',	    // 聊天界面显示的内容
			thumbData : '',	    	// 分享的图标（base64）
			webpageUrl : '',		// 分享内容的跳转链接
			callbackUrl : '',      	// 创建会话组分享成功后的回调地址
			callbackParam: {},		// 回调地址传参
			successCallback: ''	,	// 回调成功后的处理函数
			existingSessionIsNeed: true, //是否需要已有会话 
			cancelCallBack: function(resp){
				console.log(resp)
			}, //取消回调
			sureCallBack: function(resp){
				console.log(resp)
			} ,//确定回调
			removeCallBack: function(resp){

			}
		};

		this.init(cfg);
	}

	LappPersonSelect.prototype = {
		init: function(cfg){
			this.initCfg(cfg);
			this.renderUI();
			this.initContactList(0, 150);	
			this.bindEvt();
			this.initFirstData(cfg.selectedMembers)
		},

		initCfg: function(cfg){
			$.extend(this.cfg, cfg);
		},
		//上次如果选了人
		initFirstData: function(selectedMembers){
			var self = this;
			$.each(selectedMembers, function(key, val){
				var userId = key,
					photoUrl = val.photoUrl,
					userName = val.userName;

				$('.lapp-person-select-info ul').append(self.selectedPersonTpl(userId, photoUrl, userName));
				$('.lapp-person-select-wrap .contact-detail-ul').find('li[data-userid="' + userId + '"]').addClass('checked');
				$('.no-select').hide();
			})

			
		},
		renderUI: function(){
			this._$parentElement = $(this.cfg.parentElement);
			this.setParentEleOverflow();
			window.location.hash='#lapp-contact-list';

			var $tpl = $(this.tpl());
			this._$root = $tpl;
			this._$parentElement.append($tpl);
		},

		initContactList: function(offset, count){
			var self = this;

            var jqxhr = $.ajax({
            	type: 'get',
            	cache: false,
            	url: '/im/web/updatelistGroup.do',
            	data: {
            		offset: offset, 
                	count: count
            	},
            	dataType: 'json',
            	success: function(resp){
            		$('.lapp-person-select-wrap .contact-list-wrap .lapp-search-loading').hide();
            		self.sessionLoading = false;
            		$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-search-loading').hide();

            		if(resp.list && resp.list.length > 0){
	                    self.contactList = self.contactList.concat(resp.list);
	                    self.renderContactList(resp.list);
	                    self.renderSessionList(resp.list);
	                }

	                if(resp.more){
	                	self.initContactList(offset + count, count);
	                }
	                else{
	                	self.loadContactsComplete = true;
	                	// 联系人列表为空
	                	if($('.lapp-person-select-wrap .contact-list-wrap .contact-detail-ul li').length === 0){
	                		$('.lapp-person-select-wrap .contact-list-wrap .lapp-result-none').show();
	                	}
	                }
            	}
            });

			this.xhrList.push(jqxhr);
		},

		// initSessionList: function(offset, count){
		// 	var self = this;

  //           var jqxhr = $.ajax({
  //           	type: 'get',
  //           	cache: false,
  //           	url: '/im/web/updatelistGroup.do',
  //           	data: {
  //           		offset: offset, 
  //               	count: count
  //           	},
  //           	dataType: 'json',
  //           	success: function(resp){
  //           		$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-search-loading').hide();
  //           		if(resp.list && resp.list.length > 0){
	 //                    self.renderSessionList(resp.list);
	 //                }

	 //                if(resp.more){
	 //                	self.initSessionList(offset + count, count);
	 //                }
	 //                else{
	 //                	// 已有会话为空
	 //                	if($('.lapp-person-select-wrap .person-select-exist-sessions .contact-detail-ul li').length === 0){
	 //                		$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-result-none').show();
	 //                	}
	 //                } 
  //           	}
  //           });

  //           this.xhrList.push(jqxhr);
		// },

		bindEvt: function(){
			this.bindSearchEvt();
			this.bindSearchSession();
			this.bindContactListEvt();
			this.bindLinkEvt();
			this.bindHashChangeEvt();
			this.bindSelectedPersonsEvt();
			this.bindOrgTreeEvt();
			this.bindButtonEvt();
			this.bindScrollEvt();

			//清空选人
			this.clearEmpty();
		},

		//清空选人
		clearEmpty: function(){
			var me = this;
			$('body').on('click', '.clear-empty', function(){
				$('.lapp-person-select-info ul').html('');
				$('.contact-detail-ul li').removeClass('checked');
				$('.no-select').show();
				me.selectedMembers = {};
			})
		},

		// 联系人列表点击事件
		bindContactListEvt: function(){
			var self = this;
			// 点击用户
			$('.lapp-person-select-wrap').on('click', '.contact-detail-ul li.contact-detail', function(e){
				var $li = $(e.target).closest('li'),
					userId = $li.data('userid'),
            		photoUrl = $li.find('img').attr('src'),
            		groupId = $li.data('groupid'),
            		userName = $li.find('h3').text();

            	if($li.hasClass('checked')){
            		//return;
            		

            		$('.lapp-person-select-wrap .contact-detail-ul').find('li[data-userid="' + userId + '"]').removeClass('checked');
            		if(self.selectedMembers[userId]){
            			//console.log(self.selectedMembers);
            			delete self.selectedMembers[userId];
            			//self.cfg.removeCallBack(self.selectedMembers);
            		}

            		$('.lapp-person-select-wrap .selected-persons-wrap').find('li[data-userid="' + userId + '"]').remove();
            		$('.lapp-person-select-info li[data-userid="' + userId + '"]').remove();

            		if($('.lapp-person-select-info ul').html() == ''){
            			$('.no-select').show();
            		}
            		self.selectedCount--;

            		// 选择人员数目小于6，隐藏左右滚动箭头
					if(self.selectedCount == 5){
						$('.lapp-person-select-wrap .person-move-left').hide();
	            		$('.lapp-person-select-wrap .person-move-right').hide();
	            		$('.lapp-person-select-wrap .selected-persons-scroll-wrap').removeClass('selected-persons-four');
	            		$('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': '0px'}, 300);
	            		self.scrollIndex = 1;
					}
					// 选择删除最后一版元素
					else if(self.selectedCount >= 6 && self.scrollIndex == self.selectedCount - 2){
						var marginLeft = -(self.scrollIndex - 2) * 46 + 'px';
						$('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': marginLeft}, 300);
						self.scrollIndex--;
					}
            	}
            	else{
            		$('.lapp-person-select-info p.no-select').hide();
            		$('.lapp-person-select-wrap .contact-detail-ul').find('li[data-userid="' + userId + '"]').addClass('checked');
            		if(self.selectedMembers[userId]){
            			return;
            		}
            		// 如果groupId为空，表明是在搜索列表或组织架构列表，则去常用联系人列表中尝试获取groupId
					if(!groupId){
						groupId = $('.lapp-person-select-wrap .contact-list-wrap .contact-detail-ul').find('li[data-userid="' + userId + '"]').data('groupid');
					}

            		var obj = {
            			userId: userId, 
            			photoUrl: photoUrl,
            			groupId: groupId,
            			userName: userName
            		};
            		self.selectedMembers[obj.userId] = obj;

            		$('.lapp-person-select-info ul').append(self.selectedPersonTpl(userId, photoUrl, userName));
            		
            		self.selectedCount++;

            		// 如果选择的人数大于5，显示左右滚动箭头
	            	if(self.selectedCount == 6){
	            		$('.lapp-person-select-wrap .person-move-left').show();
	            		$('.lapp-person-select-wrap .person-move-right').show();
	            		$('.lapp-person-select-wrap .selected-persons-scroll-wrap').addClass('selected-persons-four');
	            	}	            	
            	}	
            	self.showSelectedCount();
            	
			});	
				
			// 点击已有会话
			$('.lapp-person-select-wrap').on('click', '.contact-detail-ul li.group-detail', function(e){
				$(this).addClass('selected');
				$('.lapp-person-select-wrap .content-box').fadeIn();
				$('.lapp-person-select-wrap .person-select-overlay').fadeIn();
				// 标识为点击已有会话打开
				$('.lapp-person-select-wrap .content-box').attr('openSource', '1');
				self.selectedGroupId = $(this).data('groupid');
			});
		},

		// 已选择的人员列表点击事件
		bindSelectedPersonsEvt: function(){
			var self = this;
			$('.lapp-person-select-info').on('click', '.delete-selected-person', function(e){
				var $li = $(e.target).closest('li'),
					userId = $li.data('userid');
				$('.lapp-person-select-wrap li.contact-detail[data-userid="' + userId + '"]').removeClass('checked');
				if(self.selectedMembers[userId]){
					delete self.selectedMembers[userId];
					//self.cfg.removeCallBack(self.selectedMembers);
				}

				$li.remove();
				self.selectedCount--;
				self.showSelectedCount();

				/*// 选择人员数目小于6，隐藏左右滚动箭头
				if(self.selectedCount < 6){
					$('.lapp-person-select-wrap .person-move-left').hide();
            		$('.lapp-person-select-wrap .person-move-right').hide();
            		$('.lapp-person-select-wrap .selected-persons-scroll-wrap').removeClass('selected-persons-four');
            		$('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': '0px'}, 300);
            		self.scrollIndex = 1;
				}
				// 选择删除最后一版元素
				else if(self.scrollIndex == self.selectedCount - 2){
					var marginLeft = -(self.scrollIndex - 2) * 46 + 'px';
					$('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': marginLeft}, 300);
					self.scrollIndex--;
				}*/
				if($('.lapp-person-select-info ul').html() == ''){
					$('.lapp-person-select-info p.no-select').show();
				}
			});
		},

		// 搜索用户
		bindSearchEvt: function(){
			var	self = this;
			$('.person-search-wrap .search-input').on('input propertychange', function(e){
				var word = htmlEntities($(e.target).val().trim()),
					me = this;

				clearTimeout(this.searchTimeout);
		        this.searchTimeout = setTimeout(function(){
		        	$('.lapp-person-select-wrap .search-list-wrap .contact-detail-ul').empty();
		            if(word === '') {
		                $('.lapp-person-select-wrap .contact-list-wrap').show();
		                $('.lapp-person-select-wrap .search-list-wrap').hide();
		            } 
		            else {
		                $('.lapp-person-select-wrap .contact-list-wrap').hide();
		                $('.lapp-person-select-wrap .search-list-wrap').show();
		                $('.lapp-person-select-wrap .search-list-wrap .lapp-result-none').hide();
		                $('.lapp-person-select-wrap .search-list-wrap .lapp-search-loading').show();

		                var reqData = {
		                	protocol: 'get',
		                	url: '/im/web/searchUser.do',
		                	param: { word: word }
		                };
		                self.commonRequest(reqData).done(function (resp) {
		                	$('.lapp-person-select-wrap .search-list-wrap .lapp-search-loading').hide();
		                	if(resp.data.list && resp.data.list.length > 0){
	                            self.renderSearchList(resp.data.list, word);
	                        }
	                        else{
	                        	$('.lapp-person-select-wrap .search-list-wrap .lapp-result-none').show();
	                        }
						}).fail(function () {
							$('.lapp-person-select-wrap .search-list-wrap .lapp-search-loading').hide();
							$('.lapp-person-select-wrap .search-list-wrap .lapp-result-none').show();
						}).always(function() {

						});
		            }

		            clearTimeout(me.searchTimeout);
		            me.searchTimeout = null;
		        }, 500);
			});			
		},

		// 搜索会话组
		bindSearchSession: function () {
			var	self = this,
				timerId,
				$result = $('.person-select-exist-sessions .lapp-result-none');

			$('.session-search-wrap .search-input').on('input propertychange', function(){
				var $ul = $('.person-select-exist-sessions .contact-detail-ul'),
					$li = $ul.find('li');

				if ($li.length == 0) {
					return ;
				}

				var word = $(this).val().trim(),
					me = this;

				clearTimeout(timerId);
		        timerId = setTimeout(function() {
		        	
		            if (word === '') {
		            	$ul.show();
		            	$li.show();
		            	$result.hide();
		            } else {
		            	var $searchList = $ul.find('li[data-groupname*="' + word + '"]');

		            	// 搜索无结果
		                if ($searchList.length == 0) {
		                	$ul.hide();
		                	$result.show();
		                } else {
		                	$ul.show();
		                	$result.hide();
		                	$li.hide();
		                	$searchList.show();
		                }
		            }
		        }, 300);
			});			
		},

		bindButtonEvt: function(){
			var self = this;

			/*// 取消
			$('.lapp-person-select-wrap .content-box-footer .content-box-cancel').click(function(){
				// 去掉选中会话组的效果
				var openSource = $('.lapp-person-select-wrap .content-box').attr('openSource');
				if(openSource === '1'){
					var groupId = self.selectedGroupId;
					$('.lapp-person-select-wrap .person-select-exist-sessions li[data-groupid="' + groupId + '"]').removeClass('selected');
				}

				$('.lapp-person-select-wrap .content-box').fadeOut();
				$('.lapp-person-select-wrap .person-select-overlay').fadeOut();
			});

			// 发送
			$('.lapp-person-select-wrap .content-box-footer .content-box-confirm').click(function(){
				$('.lapp-person-select-wrap .content-box').fadeOut();

				var openSource = $('.lapp-person-select-wrap .content-box').attr('openSource');
				if(openSource === '0'){
					var selectedMembers = self.selectedMembers;

					// 单人，执行回调后发送消息
					if(self.selectedCount == 1){
						var groupId = '', userId = '';
						for(var id in selectedMembers){
							userId = id;
							groupId = selectedMembers[id]['groupId'];
							break;
						}
						self.handleCallback(groupId, userId);						
					}
					// 多人，创建新会话，执行回调，发送消息
					else{
						var userIds = [],
							groupName = self.cfg.theme;

						for(var id in selectedMembers){
							if(!selectedMembers.hasOwnProperty(id)){
								continue;
							}
							userIds.push(id);
						}

						$.ajax({
							type: 'post',
							traditional: true,
							url: '/im/web/createGroup.do',
							data: {
								groupName: groupName,
								userIds: userIds
							},
							dataType: 'json',
							success: function(resp){
								if(resp.success === true){
									var	userId = '',
										groupId = resp.data && resp.data.groupId;
									self.handleCallback(groupId, userId);
								}
								else{
									self.showTips('发送失败', 1000, function(){	
										$('.lapp-person-select-wrap .person-select-overlay').fadeOut();
									});
								}
							}
						});
					}
				}
				// 选择已有会话
				else if(openSource === '1'){
					var groupId = self.selectedGroupId,
					    userId = '';

					self.handleCallback(groupId, userId);
				}
			});*/

			// 点击开始按钮
			$('.select-organizational-container .select-persons-button-start').click(function(){
				/*if(!$(this).hasClass('selected')){
					return;
				}*/
				//点击确定回调
				self.cfg.sureCallBack(self.selectedMembers);
				self.destroy();
				self.resetHistory();
				/*$('.lapp-person-select-wrap .content-box').fadeIn();
				$('.lapp-person-select-wrap .person-select-overlay').fadeIn();*/
				// 标识为点击开始按钮打开
				$('.select-organizational-container .content-box').attr('openSource', '0');
			});

			// 关闭
			$('.select-organizational-container .select-persons-button-close').click(function(){
				self.cfg.cancelCallBack(self.selectedMembers);
				self.destroy();
				self.resetHistory();
			});
		},

		bindLinkEvt: function(){
			//点击组织架构
			$('.lapp-person-select-wrap .link-list .organization-link').click(function(){
				window.location.hash = '#lapp-organization';
			});

			// 点击已有会话
			$('.lapp-person-select-wrap .link-list .exist-sessions-link').click(function(){
				window.location.hash = '#lapp-exist-sessions';
			});

		},

		bindScrollEvt: function(){
			var self = this;
			// 已选择人员列表向右滚动
			$('.lapp-person-select-wrap .person-move-left').click(function(){
				var index = self.scrollIndex;
				if(index == 1){
					return;
				}
				var marginLeft = -(index - 2) * 46 + 'px';
				$('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': marginLeft}, 300);
				self.scrollIndex--;				
			});

			// 已选择人员列表向左滚动
			$('.lapp-person-select-wrap .person-move-right').click(function(){
				var index = self.scrollIndex;
				if(index == self.selectedCount - 3){
					return;
				}
				var marginLeft = -index * 46 + 'px';
				$('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': marginLeft}, 300);
				self.scrollIndex++;
			});
		},

		bindHashChangeEvt: function(){
			this.newHashChangeHandler = this.hashChangeHandler.bind(this);
			$(window).on('hashchange', this.newHashChangeHandler);
		},

		unbindHashChangEvt: function(){
			$(window).off('hashchange', this.newHashChangeHandler);
		},

		hashChangeHandler: function(){
			var hash = window.location.hash;

			if(hash === '#lapp-exist-sessions'){
				$('.lapp-person-select-wrap .person-select-container').hide();
				$('.lapp-person-select-wrap .person-select-exist-sessions').show();
				$('.lapp-person-select-wrap .selected-persons-toolbar-left').hide();
				$('.lapp-person-select-wrap .select-persons-button-start').hide();

				if (this.loadContactsComplete === true) {
					// 已有会话为空
					if($('.lapp-person-select-wrap .person-select-exist-sessions .contact-detail-ul li').length === 0){
						$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-result-none').show();
					}
				}

				if (this.sessionLoading === true) {
					$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-search-loading').show();

					this.sessionLoading = false;

					// if(this.loadContactsComplete === true){
					// 	this.renderSessionList(this.contactList);

					// 	// 已有会话为空
	    //             	if($('.lapp-person-select-wrap .person-select-exist-sessions .contact-detail-ul li').length === 0){
	    //             		$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-result-none').show();
	    //             	}
					// }
					// else{
					// 	$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-search-loading').show();
			  //           this.initSessionList(0, 150);
					// }						
				} /*else {
					$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-search-loading').hide();
				}*/

				this.historyLen = 2;
			} else {
				var $searchInput = $('.person-select-exist-sessions .search-input');
				if ($searchInput.val().trim() != '') {
					$searchInput.val('').trigger('input');
				}

				if(hash === '#lapp-organization'){
					$('.lapp-person-select-wrap .person-select-container').hide();
					$('.lapp-person-select-wrap .person-select-organization').show();
					$('.lapp-person-select-wrap .selected-persons-toolbar-left').show();
					$('.lapp-person-select-wrap .select-persons-button-start').show();

					this.renderOrgTree();
					this.historyLen = 2;
				}
				else if(hash === '#lapp-contact-list'){
					$('.lapp-person-select-wrap .person-select-container').hide();
					$('.lapp-person-select-wrap .person-select-contacts').show();
					$('.lapp-person-select-wrap .selected-persons-toolbar-left').show();
					$('.lapp-person-select-wrap .select-persons-button-start').show();

					this.historyLen = 1;
				}
				else{
					this.destroy();
					this.historyLen = 0;
				}
			}
		},

		bindOrgTreeEvt: function(){
			var self = this,
				timeout = null;

			// 进入下一级组织
			$('.lapp-person-select-wrap .person-select-organization').on('click', 'li.department', function(){
				var $ul = $(this).closest('ul'),
					$li = $(this).closest('li'),
                    id = $li.data('id'),
                    name = $.trim($li.text());
                    node = self.orgCache[id];

                if(timeout){
                	clearTimeout(timeout);
                }

                timeout = setTimeout(function(){
                	if(node) {
                        node.parent = name;
                        self.orgPath.push(node);

                        var	tpl = self.orgTreeTpl(node), $tpl = $(tpl);
		    			$('.lapp-person-select-wrap .person-select-organization .contact-detail-wrap').append($tpl);

	                    $ul.animate({
		                    'left': '-100%'
		                }, 300);

		                $tpl.animate({
		    				'left': 0
		    			}, 300);
	                }
                    else{
                    	var eid = self.cfg.eId || '',
                    		orgId = id,
			    			reqData = {
			                	protocol: 'post',
			                	url: '/im/web/treeOrg.do',
			                	param: {
			                		eid: eid,
				                    orgId: orgId,
				                    begin: 0,
				                    count: 10000
			                	}
			                };

			            self.commonRequest(reqData).done(function (resp){
			            	self.orgCache[orgId] = resp.data;
			            	node = { 'parent': name };
			            	$.extend(node, resp.data);           
			            	self.orgPath.push(node);

			            	var	tpl = self.orgTreeTpl(node), $tpl = $(tpl);
			    			$('.lapp-person-select-wrap .person-select-organization .contact-detail-wrap').append($tpl);

		                    $ul.animate({
			                    'left': '-100%'
			                }, 300);

			                $tpl.animate({
			    				'left': 0
			    			}, 300);			            	  	
			            });
                    };            	
                }, 400);
			});

			// 返回上一级组织
			$('.lapp-person-select-wrap .person-select-organization').on('click', 'li.parent-org', function(){
				var $ul = $(this).closest('ul'),
					$prevUl = $ul.prev(),
					last = self.orgPath.length - 1;

				$ul.animate({
					'left': '100%'
				}, 300, function(){
					self.orgPath.splice(last, 1);    
                    $ul.remove();
				});

				$prevUl.animate({
					'left': 0
				}, 300);
			});
		},

		tpl: function(){
			var t = new SimpleTemplate();

			t._('<div class="select-organizational-container clearfix">')
				._('<div class="lapp-person-select-info bsizing fl">')
					._('<div class="lapp-person-select-head1">')
						._('已选择审批人')
						._('<span class="clear-empty">清空</span>')
					._('</div>')
					._('<ul class="clearfix"></ul>')
					._('<p class="no-select center">请从右边选择审批人</p>')
				._('</div>')

				._('<div class="lapp-person-select-wrap fl">')
					._('<div class="lapp-person-select-head2">')
						._('角色')
						._('<span></span>')
					._('</div>')
					._('<div class="lapp-person-select-title">所有员工</div>')
					._('<div class="person-select-contacts person-select-container">')
						._('<div class="person-search-div">')
							._('<div class="person-search-wrap">')
								._('<span class="search-icon"></span>')
								._('<input class="search-input" autocomplete="off" placeholder="姓名/拼音/电话"></input>')
							._('</div>')
						._('</div>')
						._('<div class="contact-list-wrap">')
							._('<div class="link-list">')
								._('<div class="organization-link link">')
									._('<img src="/lightapp/images/organization-icon.png">')
									._('<h3>组织架构</h3>')
								._('</div>');
							//是否需要已有会话
							if(this.cfg.existingSessionIsNeed){
								t._('<div class="exist-sessions-link link clearfix">')
									._('<img src="/lightapp/images/exist-sessions-icon.png">')
									._('<h3>已有会话</h3>')
								._('</div>');
							}
									
							t._('</div>')
							._('<h3>常用联系人</h3>');
							if(this.cfg.existingSessionIsNeed){
								t._('<div class="contact-list">');
							}
							else{
								t._('<div class="contact-list no-need-session">');
							};
							
								t._('<div class="contact-detail-wrap">')
									._('<ul class="contact-detail-ul">')
									._('</ul>')
								._('</div>')
								._('<div class="lapp-search-loading"><img src="/lightapp/images/loading.gif"></div>')
								._('<div class="lapp-result-none">')
									._('<img src="/lightapp/images/result-none.png">')
									._('<span>暂无联系人</span>')
								._('</div>')							
							._('</div>')
						._('</div>')
						._('<div class="search-list-wrap">')
							._('<div class="contact-detail-wrap">')
								._('<ul class="contact-detail-ul">')
								._('</ul>')
							._('</div>')
							._('<div class="lapp-search-loading"><img src="/lightapp/images/loading.gif"></div>')
							._('<div class="lapp-result-none">')
								._('<img src="/lightapp/images/result-none.png">')
								._('<span>无结果</span>')
							._('</div>')
						._('</div>')
					._('</div>')
					._('<div class="person-select-exist-sessions person-select-container">')
						._('<div class="contact-detail-wrap">')
							._('<div class="session-search-wrap"><span class="search-icon">')
								._('</span><input class="search-input" autocomplete="off" placeholder="会话组名">')
							._('</div>')
							._('<ul class="contact-detail-ul">')
							._('</ul>')
						._('</div>')
						._('<div class="lapp-search-loading"><img src="/lightapp/images/loading.gif"></div>')
						._('<div class="lapp-result-none">')
							._('<img src="/lightapp/images/result-none.png">')
							._('<span>暂无会话</span>')
						._('</div>')
					._('</div>')
					._('<div class="person-select-organization person-select-container">')
						._('<div class="contact-detail-wrap clearfix">')
						._('</div>')
						._('<div class="lapp-search-loading"><img src="/lightapp/images/loading.gif"></div>')
						._('<div class="lapp-result-none">')
							._('<img src="/lightapp/images/result-none.png">')
							._('<span>没有组织架构</span>')
						._('</div>')
					._('</div>')
					
					._('<div class="person-select-overlay"></div>')
					._('<div class="content-box" openSource="0">')
						._('<h3 class="ellipsis">' + this.cfg.title + '</h3>')
						._('<div class="content-box-wrap">')
							._('<div class="content-box-main clearfix">')
								._('<img src="' + this.cfg.thumbData + '">')
								._('<p>' + escapeContent(this.cfg.content) + '</p>')
							._('</div>')
							._('<div class="content-box-source">')
								._('<span>来自：</span>')
								._('<span class="app_name">' + this.cfg.appName + '</span>')
							._('</div>')
						._('</div>')
						._('<div class="content-box-footer clearfix">')
							._('<button class="content-box-cancel">取消</button>')
							._('<button class="content-box-confirm">发送</button>')
						._('</div>')
					._('</div>')
					._('<div class="person-select-prompt-msg"></div>')
				._('</div>')
				._('<div class="selected-persons-toolbar clearfix">')
				._('<div class="select-persons-button-list clearfix">')
					._('<button class="select-persons-button-start">确定</button>')
					._('<button class="select-persons-button-close">取消</button>')
				._('</div>')
			._('</div>')
			._('</div>')

			return t.toString();
		},

		//显示开始按钮上的数字
		showSelectedCount: function(){
			var $confirmButton = $('.select-organizational-container .select-persons-button-start');
	        if(this.selectedCount > 0){
	            $confirmButton.text('开始 (' + this.selectedCount +')');
	            if(!$confirmButton.hasClass('selected')){
	            	$confirmButton.addClass('selected');
	            }
	        }
	        else{
	            $confirmButton.removeClass('selected').text('开始');
	        }
	    },

	    selectedPersonTpl: function(userId, photoUrl, userName){
	    	var t = new SimpleTemplate();
	    	t._('<li class="bsizing" data-userid="' + userId + '">')
	    		._('<div class="select-persons-info">')
		    		._('<img src="' + photoUrl + '">')
		    		._('<span class="delete-selected-person"></span>')
		    	._('</div>')
		    	._('<p class="select-persons-info-username">'+ userName +'</p>')
	    	 ._('</li>');

	    	return t.toString();
	    },

	   	//搜索框列表
	    renderSearchList: function(data, keyword){
	    	var t = new SimpleTemplate(),
            	photoUrl,
            	additional_message,
            	defaultUrl = 'http://kdweibo.com/space/c/photo/load?id=';
            for(var i = 0, len = data.length; i < len; i++){
            	photoUrl = data[i].photoUrl || defaultUrl;
            	isSelectedCls = this.selectedMembers[data[i].id] ? ' checked' : '';
            	var fullPinyin = data[i].fullPinyin,
            		phone = data[i].defaultPhone;
            	additional_message = phone.indexOf(keyword) > -1 ? phone : fullPinyin.indexOf(keyword) > -1 ? fullPinyin : phone;

            	t._('<li class="contact-detail clearfix' + isSelectedCls + '" data-userid="' + data[i].id + '">')
            		._('<div class="contact-detail-inner clearfix">')
	            		._('<span class="contact-checkbox"></span>')
	            		._('<img src="' + photoUrl + '">')
	            		._('<div class="contact-content">')
	            			._('<div class="clearfix">')
	            				._('<h3 class="search-name-text ellipsis">' + data[i].name + '</h3>')
	            				._('<h3 class="search-department-text ellipsis">' + data[i].department + '</h3>')
	            			._('</div>')
	            			._('<p>' + additional_message + '</p>')
	            		._('</div>')
	            	._('</div>')
            	._('</li>');
            }

            $('.lapp-person-select-wrap .search-list-wrap .contact-detail-ul').append(t.toString());
	    },

	    //联系人列表
	    renderContactList: function(data){
	    	for(var i = 0, len = data.length; i < len; i++){
	    		if(data[i].type === 1){
	    			$('.lapp-person-select-wrap .contact-list-wrap .contact-detail-ul').append(this.contactDetailTpl(data[i]));
	    		}
	    	}
	    },

	    //会话列表
	    renderSessionList: function(data){
	    	for(var i = 0, len = data.length; i < len; i++){
	    		if(data[i].type === 2){
	    			$('.lapp-person-select-wrap .person-select-exist-sessions .contact-detail-ul').append(this.groupDetailTpl(data[i]));
	    		}
	    	}
	    },

	    contactDetailTpl: function(data){
	    	var t = new SimpleTemplate(),
            	defaultUrl = 'http://kdweibo.com/space/c/photo/load?id=',
            	jobTitle = data.participant[5][0] || '职员',
            	userId = data.participant[0][0],
            	userName = data.groupName,
            	groupId = data.groupId,
            	photoUrl = data.participant[2][0] || defaultUrl,
				isSelectedCls = this.selectedMembers[userId] ? ' checked' : ''; 

            t._('<li class="contact-detail' + isSelectedCls + '" data-userid="' + userId + '" data-groupid="' + groupId + '">')
            	._('<div class="contact-detail-inner clearfix">')
	        		._('<span class="contact-checkbox"></span>')
	        		._('<img src="' + photoUrl + '">')
	        		._('<div class="contact-content">')
	        			._('<h3>' + userName + '</h3>')
	        			._('<p>' + jobTitle + '</p>')
	        		._('</div>')
	        	._('</div>')
        	._('</li>');

            return t.toString();
	    },

	    groupDetailTpl: function(data){
	    	var t = new SimpleTemplate(),
	            photoUrl,
	            photoCount,
	            defaultUrl = 'http://kdweibo.com/xtweb/pub/img/default_man.png';

	        var searchVal = $('.person-select-exist-sessions .search-input').val().trim();
	        var style = '';

			if (searchVal && data.groupName.indexOf(searchVal) == -1) {
				style = 'style="display: none;"';
			}

	        t._('<li class="group-detail" ' + style + ' data-groupid="' + data.groupId + '" data-groupname="' + data.groupName + '">')
	        	._('<div class="contact-detail-inner clearfix">')
	        		._('<div class="group-avatar clearfix">');

	       	photoCount = data.participant[2].length > 4 ? 4 : data.participant[2].length;
            for(var j = 0; j < photoCount; j++){
                photoUrl = data.participant[2][j] || defaultUrl;
                t._('<img src="' + photoUrl + '">');
            }

            t._('</div>')
            ._('<div class="group-content ellipsis">' + data.groupName + '(' + (parseInt(data.participant[2].length) + 1) + '人)</div></div>')
            ._('</li>');

           	return t.toString();
	    },

	    renderOrgTree: function(){
	    	var orgPath = this.orgPath,
	    		self = this;
	    	if(orgPath && orgPath.length > 0){
	    		if(orgPath.length > 1){
	    			orgPath.splice(1);
	    		}

	    		var tpl = this.orgTreeTpl(orgPath[0], 'org-index');
	    		$('.lapp-person-select-wrap .person-select-organization .contact-detail-wrap').empty().append(tpl);

	    		// 组织架构为空
            	if($('.lapp-person-select-wrap .person-select-organization .contact-detail-ul li').length === 0){
            		$('.lapp-person-select-wrap .person-select-organization .lapp-result-none').show();
            	}
	    	}
	    	else{
	    		var eid = this.cfg.eId || '',
	    			reqData = {
	                	protocol: 'post',
	                	url: '/im/web/treeOrg.do',
	                	param: {
	                		eid: eid,
		                    orgId: '',
		                    begin: 0,
		                    count: 10000
	                	}
	                };

	            this.commonRequest(reqData).done(function (resp){
	            	$('.lapp-person-select-wrap .person-select-organization .lapp-search-loading').hide();
	            	var key = 'root';
	            	resp.data.id = key;
	            	self.orgCache[key] = resp.data;
                	orgPath.push(resp.data);

                	var tpl = self.orgTreeTpl(orgPath[0], 'org-index');
	    			$('.lapp-person-select-wrap .person-select-organization .contact-detail-wrap').empty().append(tpl);

	    			// 组织架构为空
	            	if($('.lapp-person-select-wrap .person-select-organization .contact-detail-ul li').length === 0){
	            		$('.lapp-person-select-wrap .person-select-organization .lapp-result-none').show();
	            	}
	            });
	    	}
	    },

	    orgTreeTpl: function(data, type){
	    	var t = new SimpleTemplate();

	    	if(type === 'org-index'){
	    		t._('<ul class="contact-detail-ul contact-detail-ul-org-index" data-id="' + data.id + '">');
	    	}
	    	else{
	    		t._('<ul class="contact-detail-ul" data-id="' + data.id + '">');
	    	}	    	

	    	if(data.parent){
	    		t._('<li class="parent-org">')
	    			._('<div class="contact-detail-inner">')
		    			._('<span class="prev-arrow"></span>')
		    			._('<a href="javascript:void(0)" class="parent-name ellipsis" title="' + data.parent + '">' + data.parent + '</a>')
		    		._('</div>')
	    		._('</li>');
	    	}

	    	for(var i = 0, len = data.children.length; i < len; i++){
	    		var department = data.children[i];
	    		t._('<li class="department" data-id="' + department.id + '">')
	    			._('<div class="contact-detail-inner">')
		    			._('<a class="dep-name ellipsis" href="javascript:void(0)" title="' + department.name + '">' + department.name + '</a>')
		    			._('<span class="next-arrow"></span>')
		    		._('</div>')
	    		._('</li>');
	    	}

	    	for(var j = 0, len = data.person.length; j < len; j++){
	    		var user = data.person[j],
		    		defaultUrl = 'http://kdweibo.com/space/c/photo/load?id=',
	            	photoUrl = user.photoUrl || defaultUrl,
	            	userId = user.id,
					isSelectedCls = this.selectedMembers[userId] ? ' checked' : ''; 
	    			user.jobTitle = user.jobTitle || '职员';
	    		t._('<li class="contact-detail' + isSelectedCls + '" data-userid="' + userId + '">')
	    			._('<div class="contact-detail-inner clearfix">')
		        		._('<span class="contact-checkbox"></span>')
		        		._('<img src="' + photoUrl + '">')
		        		._('<div class="contact-content">')
		        			._('<h3>' + user.name + '</h3>')
		        			._('<p>' + user.jobTitle + '</p>')
		        		._('</div>')
		        	._('</div>')
	        	._('</li>');
	    	}

	    	t._('</ul>');

	    	return t.toString();
	    },

	    destroy: function(){
	    	this.clearXhr();
	    	this.unbindHashChangEvt();
	    	this.resetParentEleOverflow();
	    	
	    	if(this._$root){
	    		this._$root.remove();
	    		this._$root = null;
	    	}
	    },

	    clearXhr: function () {
	    	for (var i = 0, len = this.xhrList.length; i < len; i++) {
	    		this.xhrList[i].abort();
	    	}
	    },
	   
        commonRequest: function(data){
			var deferred = $.Deferred();							
			$.ajax({
				type: data.protocol || 'get',
				cache: false,
				dataType: 'json',
				url: data.url,
				data: data.param
			}).done(function (resp) {
				if (resp.success === true) {
					deferred.resolve(resp);
				} 
				else {
					deferred.reject(resp);
				}
			}).fail(function (resp) {
				deferred.reject(resp);
			});		
			return deferred.promise();
		},

		addUrlParam: function(url, str){
			if(url.indexOf('?') <= 0){
				url = url + '?';
			}
			else{
				url = url + '&';
			}
			return url + str;
		},

		handleCallback: function(groupId, userId){
			var self = this,
				callbackUrl = self.cfg.callbackUrl;				

			if(groupId == undefined){
				groupId = '';
			}

			// 执行回调
			if(callbackUrl != ''){
				var callbackParam = self.cfg.callbackParam;
				$.extend(callbackParam, {
					'eId': self.cfg.eId,
					'openId': self.cfg.openId,
					'groupId': groupId || userId
				});

				$.ajax({
					type: 'post',
					url: callbackUrl,
					data: callbackParam,
					dataType: 'json',
					success: function(resp){
						if(resp.success === true){
							var callbackData = resp.data,
								param = {
								'appName': self.cfg.appName,
								'lightAppId': self.cfg.lightAppId,
								'pubAccId': self.cfg.appId,
								'thumbUrl': self.cfg.thumbData,
								'title': self.cfg.title,
								'content': self.cfg.content,
								'webpageUrl': callbackData ? self.addUrlParam(self.cfg.webpageUrl, callbackData) : self.cfg.webpageUrl
							};

							$.ajax({
								type: 'post',
								url: '/im/web/sendMessage.do',						
								data: {
									groupId: groupId,
									toUserId: userId,
									msgType: 7,
									content: self.cfg.title,
									param: JSON.stringify(param)
								},
								dataType: 'json',
								success: function(resp){
									if(resp.success === true){
										self.showTips('发送成功', 1000, function(){	
											$('.lapp-person-select-wrap .person-select-overlay').fadeOut();
											var callback = self.cfg.successCallback;
											if(callback && typeof callback === 'function'){
												callback.call(null, callbackData);
											}
											else{
												self.destroy();
												self.resetHistory();
											}
										});
									}							
									else{
										self.showTips('发送失败', 1000, function(){	
											$('.lapp-person-select-wrap .person-select-overlay').fadeOut();
										});
									}
								}
							});
						}
						else{
							self.showTips('发送失败', 1000, function(){	
								$('.lapp-person-select-wrap .person-select-overlay').fadeOut();
							});
						}
					}
				});
			}
			// 不执行回调，直接发送消息
			else{
				var param = {
					'appName': self.cfg.appName,
					'lightAppId': self.cfg.lightAppId,
					'pubAccId': self.cfg.appId,
					'thumbUrl': self.cfg.thumbData,
					'title': self.cfg.title,
					'content': self.cfg.content,
					'webpageUrl': self.cfg.webpageUrl
				};

				$.ajax({
					type: 'post',
					url: '/im/web/sendMessage.do',						
					data: {
						groupId: groupId,
						toUserId: userId,
						msgType: 7,
						content: self.cfg.title,
						param: JSON.stringify(param)
					},
					dataType: 'json',
					success: function(resp){
						if(resp.success === true){
							self.showTips('发送成功', 1000, function(){
								$('.lapp-person-select-wrap .person-select-overlay').fadeOut();
								
								var callback = self.cfg.successCallback;
                                if(callback && typeof callback === 'function'){
                                    callback.call(null);
                                }
                                
								self.destroy();
								self.resetHistory();
							});
						}
						else{
							self.showTips('发送失败', 1000, function(){	
								$('.lapp-person-select-wrap .person-select-overlay').fadeOut();
							});
						}									
					}
				});
			}
		},

		// 设置父元素overflow为hidden，使得选人组件铺满父窗口
		setParentEleOverflow: function(){
			this.parentEleOverflow = this._$parentElement.css('overflow');
			this._$parentElement.css('overflow', 'hidden');
		},

		resetParentEleOverflow: function(){
			this._$parentElement.css('overflow', this.parentEleOverflow);
		},

		resetHistory: function(){
			if(this.historyLen > 0){
				window.history.go(-this.historyLen );
			}
		},

		showTips: function(tips, time, callback){
			var $promptMsg = $(".lapp-person-select-wrap .person-select-prompt-msg"),
                fadeOutTime = time || 500;

            $promptMsg.html(tips).fadeIn('slow', function(){
            	$promptMsg.fadeOut(fadeOutTime, function(){
            		if(callback){
            			setTimeout(callback, 300);
            		}
            	});
            });
		}

	};

	function SimpleTemplate () {
        this.parts = [];
        this._pushAll(arguments);
    }

    SimpleTemplate.prototype = {
        _: function () {
            this._pushAll(arguments);
            return this;
        },

        toString: function () {
            return this.parts.join('');
        },

        _pushAll: function (arr) {
            var i, n = arr.length;
            for (i = 0; i < n; i++) {
                this.parts.push(arr[i]);
            }
        }
    };

    function htmlEntities(str) {
        if (typeof str === 'undefined') return '';
        return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }

    function escapeContent(content){
		return content.replace(/\n|\r/g, '<br>').replace(/ /g, '&nbsp;');
	}

	window.LappPersonSelect = window.LappPersonSelect || LappPersonSelect;
})(window, window.Zepto || window.jQuery);