//     Underscore.js 1.8.2
//     http://underscorejs.org
//     (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.
(function(){function n(n){function t(t,r,e,u,i,o){for(;i>=0&&o>i;i+=n){var a=u?u[i]:i;e=r(e,t[a],a,t)}return e}return function(r,e,u,i){e=d(e,i,4);var o=!w(r)&&m.keys(r),a=(o||r).length,c=n>0?0:a-1;return arguments.length<3&&(u=r[o?o[c]:c],c+=n),t(r,e,u,o,c,a)}}function t(n){return function(t,r,e){r=b(r,e);for(var u=null!=t&&t.length,i=n>0?0:u-1;i>=0&&u>i;i+=n)if(r(t[i],i,t))return i;return-1}}function r(n,t){var r=S.length,e=n.constructor,u=m.isFunction(e)&&e.prototype||o,i="constructor";for(m.has(n,i)&&!m.contains(t,i)&&t.push(i);r--;)i=S[r],i in n&&n[i]!==u[i]&&!m.contains(t,i)&&t.push(i)}var e=this,u=e._,i=Array.prototype,o=Object.prototype,a=Function.prototype,c=i.push,l=i.slice,f=o.toString,s=o.hasOwnProperty,p=Array.isArray,h=Object.keys,v=a.bind,g=Object.create,y=function(){},m=function(n){return n instanceof m?n:this instanceof m?void(this._wrapped=n):new m(n)};"undefined"!=typeof exports?("undefined"!=typeof module&&module.exports&&(exports=module.exports=m),exports._=m):e._=m,m.VERSION="1.8.2";var d=function(n,t,r){if(t===void 0)return n;switch(null==r?3:r){case 1:return function(r){return n.call(t,r)};case 2:return function(r,e){return n.call(t,r,e)};case 3:return function(r,e,u){return n.call(t,r,e,u)};case 4:return function(r,e,u,i){return n.call(t,r,e,u,i)}}return function(){return n.apply(t,arguments)}},b=function(n,t,r){return null==n?m.identity:m.isFunction(n)?d(n,t,r):m.isObject(n)?m.matcher(n):m.property(n)};m.iteratee=function(n,t){return b(n,t,1/0)};var x=function(n,t){return function(r){var e=arguments.length;if(2>e||null==r)return r;for(var u=1;e>u;u++)for(var i=arguments[u],o=n(i),a=o.length,c=0;a>c;c++){var l=o[c];t&&r[l]!==void 0||(r[l]=i[l])}return r}},_=function(n){if(!m.isObject(n))return{};if(g)return g(n);y.prototype=n;var t=new y;return y.prototype=null,t},j=Math.pow(2,53)-1,w=function(n){var t=n&&n.length;return"number"==typeof t&&t>=0&&j>=t};m.each=m.forEach=function(n,t,r){t=d(t,r);var e,u;if(w(n))for(e=0,u=n.length;u>e;e++)t(n[e],e,n);else{var i=m.keys(n);for(e=0,u=i.length;u>e;e++)t(n[i[e]],i[e],n)}return n},m.map=m.collect=function(n,t,r){t=b(t,r);for(var e=!w(n)&&m.keys(n),u=(e||n).length,i=Array(u),o=0;u>o;o++){var a=e?e[o]:o;i[o]=t(n[a],a,n)}return i},m.reduce=m.foldl=m.inject=n(1),m.reduceRight=m.foldr=n(-1),m.find=m.detect=function(n,t,r){var e;return e=w(n)?m.findIndex(n,t,r):m.findKey(n,t,r),e!==void 0&&e!==-1?n[e]:void 0},m.filter=m.select=function(n,t,r){var e=[];return t=b(t,r),m.each(n,function(n,r,u){t(n,r,u)&&e.push(n)}),e},m.reject=function(n,t,r){return m.filter(n,m.negate(b(t)),r)},m.every=m.all=function(n,t,r){t=b(t,r);for(var e=!w(n)&&m.keys(n),u=(e||n).length,i=0;u>i;i++){var o=e?e[i]:i;if(!t(n[o],o,n))return!1}return!0},m.some=m.any=function(n,t,r){t=b(t,r);for(var e=!w(n)&&m.keys(n),u=(e||n).length,i=0;u>i;i++){var o=e?e[i]:i;if(t(n[o],o,n))return!0}return!1},m.contains=m.includes=m.include=function(n,t,r){return w(n)||(n=m.values(n)),m.indexOf(n,t,"number"==typeof r&&r)>=0},m.invoke=function(n,t){var r=l.call(arguments,2),e=m.isFunction(t);return m.map(n,function(n){var u=e?t:n[t];return null==u?u:u.apply(n,r)})},m.pluck=function(n,t){return m.map(n,m.property(t))},m.where=function(n,t){return m.filter(n,m.matcher(t))},m.findWhere=function(n,t){return m.find(n,m.matcher(t))},m.max=function(n,t,r){var e,u,i=-1/0,o=-1/0;if(null==t&&null!=n){n=w(n)?n:m.values(n);for(var a=0,c=n.length;c>a;a++)e=n[a],e>i&&(i=e)}else t=b(t,r),m.each(n,function(n,r,e){u=t(n,r,e),(u>o||u===-1/0&&i===-1/0)&&(i=n,o=u)});return i},m.min=function(n,t,r){var e,u,i=1/0,o=1/0;if(null==t&&null!=n){n=w(n)?n:m.values(n);for(var a=0,c=n.length;c>a;a++)e=n[a],i>e&&(i=e)}else t=b(t,r),m.each(n,function(n,r,e){u=t(n,r,e),(o>u||1/0===u&&1/0===i)&&(i=n,o=u)});return i},m.shuffle=function(n){for(var t,r=w(n)?n:m.values(n),e=r.length,u=Array(e),i=0;e>i;i++)t=m.random(0,i),t!==i&&(u[i]=u[t]),u[t]=r[i];return u},m.sample=function(n,t,r){return null==t||r?(w(n)||(n=m.values(n)),n[m.random(n.length-1)]):m.shuffle(n).slice(0,Math.max(0,t))},m.sortBy=function(n,t,r){return t=b(t,r),m.pluck(m.map(n,function(n,r,e){return{value:n,index:r,criteria:t(n,r,e)}}).sort(function(n,t){var r=n.criteria,e=t.criteria;if(r!==e){if(r>e||r===void 0)return 1;if(e>r||e===void 0)return-1}return n.index-t.index}),"value")};var A=function(n){return function(t,r,e){var u={};return r=b(r,e),m.each(t,function(e,i){var o=r(e,i,t);n(u,e,o)}),u}};m.groupBy=A(function(n,t,r){m.has(n,r)?n[r].push(t):n[r]=[t]}),m.indexBy=A(function(n,t,r){n[r]=t}),m.countBy=A(function(n,t,r){m.has(n,r)?n[r]++:n[r]=1}),m.toArray=function(n){return n?m.isArray(n)?l.call(n):w(n)?m.map(n,m.identity):m.values(n):[]},m.size=function(n){return null==n?0:w(n)?n.length:m.keys(n).length},m.partition=function(n,t,r){t=b(t,r);var e=[],u=[];return m.each(n,function(n,r,i){(t(n,r,i)?e:u).push(n)}),[e,u]},m.first=m.head=m.take=function(n,t,r){return null==n?void 0:null==t||r?n[0]:m.initial(n,n.length-t)},m.initial=function(n,t,r){return l.call(n,0,Math.max(0,n.length-(null==t||r?1:t)))},m.last=function(n,t,r){return null==n?void 0:null==t||r?n[n.length-1]:m.rest(n,Math.max(0,n.length-t))},m.rest=m.tail=m.drop=function(n,t,r){return l.call(n,null==t||r?1:t)},m.compact=function(n){return m.filter(n,m.identity)};var k=function(n,t,r,e){for(var u=[],i=0,o=e||0,a=n&&n.length;a>o;o++){var c=n[o];if(w(c)&&(m.isArray(c)||m.isArguments(c))){t||(c=k(c,t,r));var l=0,f=c.length;for(u.length+=f;f>l;)u[i++]=c[l++]}else r||(u[i++]=c)}return u};m.flatten=function(n,t){return k(n,t,!1)},m.without=function(n){return m.difference(n,l.call(arguments,1))},m.uniq=m.unique=function(n,t,r,e){if(null==n)return[];m.isBoolean(t)||(e=r,r=t,t=!1),null!=r&&(r=b(r,e));for(var u=[],i=[],o=0,a=n.length;a>o;o++){var c=n[o],l=r?r(c,o,n):c;t?(o&&i===l||u.push(c),i=l):r?m.contains(i,l)||(i.push(l),u.push(c)):m.contains(u,c)||u.push(c)}return u},m.union=function(){return m.uniq(k(arguments,!0,!0))},m.intersection=function(n){if(null==n)return[];for(var t=[],r=arguments.length,e=0,u=n.length;u>e;e++){var i=n[e];if(!m.contains(t,i)){for(var o=1;r>o&&m.contains(arguments[o],i);o++);o===r&&t.push(i)}}return t},m.difference=function(n){var t=k(arguments,!0,!0,1);return m.filter(n,function(n){return!m.contains(t,n)})},m.zip=function(){return m.unzip(arguments)},m.unzip=function(n){for(var t=n&&m.max(n,"length").length||0,r=Array(t),e=0;t>e;e++)r[e]=m.pluck(n,e);return r},m.object=function(n,t){for(var r={},e=0,u=n&&n.length;u>e;e++)t?r[n[e]]=t[e]:r[n[e][0]]=n[e][1];return r},m.indexOf=function(n,t,r){var e=0,u=n&&n.length;if("number"==typeof r)e=0>r?Math.max(0,u+r):r;else if(r&&u)return e=m.sortedIndex(n,t),n[e]===t?e:-1;if(t!==t)return m.findIndex(l.call(n,e),m.isNaN);for(;u>e;e++)if(n[e]===t)return e;return-1},m.lastIndexOf=function(n,t,r){var e=n?n.length:0;if("number"==typeof r&&(e=0>r?e+r+1:Math.min(e,r+1)),t!==t)return m.findLastIndex(l.call(n,0,e),m.isNaN);for(;--e>=0;)if(n[e]===t)return e;return-1},m.findIndex=t(1),m.findLastIndex=t(-1),m.sortedIndex=function(n,t,r,e){r=b(r,e,1);for(var u=r(t),i=0,o=n.length;o>i;){var a=Math.floor((i+o)/2);r(n[a])<u?i=a+1:o=a}return i},m.range=function(n,t,r){arguments.length<=1&&(t=n||0,n=0),r=r||1;for(var e=Math.max(Math.ceil((t-n)/r),0),u=Array(e),i=0;e>i;i++,n+=r)u[i]=n;return u};var O=function(n,t,r,e,u){if(!(e instanceof t))return n.apply(r,u);var i=_(n.prototype),o=n.apply(i,u);return m.isObject(o)?o:i};m.bind=function(n,t){if(v&&n.bind===v)return v.apply(n,l.call(arguments,1));if(!m.isFunction(n))throw new TypeError("Bind must be called on a function");var r=l.call(arguments,2),e=function(){return O(n,e,t,this,r.concat(l.call(arguments)))};return e},m.partial=function(n){var t=l.call(arguments,1),r=function(){for(var e=0,u=t.length,i=Array(u),o=0;u>o;o++)i[o]=t[o]===m?arguments[e++]:t[o];for(;e<arguments.length;)i.push(arguments[e++]);return O(n,r,this,this,i)};return r},m.bindAll=function(n){var t,r,e=arguments.length;if(1>=e)throw new Error("bindAll must be passed function names");for(t=1;e>t;t++)r=arguments[t],n[r]=m.bind(n[r],n);return n},m.memoize=function(n,t){var r=function(e){var u=r.cache,i=""+(t?t.apply(this,arguments):e);return m.has(u,i)||(u[i]=n.apply(this,arguments)),u[i]};return r.cache={},r},m.delay=function(n,t){var r=l.call(arguments,2);return setTimeout(function(){return n.apply(null,r)},t)},m.defer=m.partial(m.delay,m,1),m.throttle=function(n,t,r){var e,u,i,o=null,a=0;r||(r={});var c=function(){a=r.leading===!1?0:m.now(),o=null,i=n.apply(e,u),o||(e=u=null)};return function(){var l=m.now();a||r.leading!==!1||(a=l);var f=t-(l-a);return e=this,u=arguments,0>=f||f>t?(o&&(clearTimeout(o),o=null),a=l,i=n.apply(e,u),o||(e=u=null)):o||r.trailing===!1||(o=setTimeout(c,f)),i}},m.debounce=function(n,t,r){var e,u,i,o,a,c=function(){var l=m.now()-o;t>l&&l>=0?e=setTimeout(c,t-l):(e=null,r||(a=n.apply(i,u),e||(i=u=null)))};return function(){i=this,u=arguments,o=m.now();var l=r&&!e;return e||(e=setTimeout(c,t)),l&&(a=n.apply(i,u),i=u=null),a}},m.wrap=function(n,t){return m.partial(t,n)},m.negate=function(n){return function(){return!n.apply(this,arguments)}},m.compose=function(){var n=arguments,t=n.length-1;return function(){for(var r=t,e=n[t].apply(this,arguments);r--;)e=n[r].call(this,e);return e}},m.after=function(n,t){return function(){return--n<1?t.apply(this,arguments):void 0}},m.before=function(n,t){var r;return function(){return--n>0&&(r=t.apply(this,arguments)),1>=n&&(t=null),r}},m.once=m.partial(m.before,2);var F=!{toString:null}.propertyIsEnumerable("toString"),S=["valueOf","isPrototypeOf","toString","propertyIsEnumerable","hasOwnProperty","toLocaleString"];m.keys=function(n){if(!m.isObject(n))return[];if(h)return h(n);var t=[];for(var e in n)m.has(n,e)&&t.push(e);return F&&r(n,t),t},m.allKeys=function(n){if(!m.isObject(n))return[];var t=[];for(var e in n)t.push(e);return F&&r(n,t),t},m.values=function(n){for(var t=m.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=n[t[u]];return e},m.mapObject=function(n,t,r){t=b(t,r);for(var e,u=m.keys(n),i=u.length,o={},a=0;i>a;a++)e=u[a],o[e]=t(n[e],e,n);return o},m.pairs=function(n){for(var t=m.keys(n),r=t.length,e=Array(r),u=0;r>u;u++)e[u]=[t[u],n[t[u]]];return e},m.invert=function(n){for(var t={},r=m.keys(n),e=0,u=r.length;u>e;e++)t[n[r[e]]]=r[e];return t},m.functions=m.methods=function(n){var t=[];for(var r in n)m.isFunction(n[r])&&t.push(r);return t.sort()},m.extend=x(m.allKeys),m.extendOwn=m.assign=x(m.keys),m.findKey=function(n,t,r){t=b(t,r);for(var e,u=m.keys(n),i=0,o=u.length;o>i;i++)if(e=u[i],t(n[e],e,n))return e},m.pick=function(n,t,r){var e,u,i={},o=n;if(null==o)return i;m.isFunction(t)?(u=m.allKeys(o),e=d(t,r)):(u=k(arguments,!1,!1,1),e=function(n,t,r){return t in r},o=Object(o));for(var a=0,c=u.length;c>a;a++){var l=u[a],f=o[l];e(f,l,o)&&(i[l]=f)}return i},m.omit=function(n,t,r){if(m.isFunction(t))t=m.negate(t);else{var e=m.map(k(arguments,!1,!1,1),String);t=function(n,t){return!m.contains(e,t)}}return m.pick(n,t,r)},m.defaults=x(m.allKeys,!0),m.clone=function(n){return m.isObject(n)?m.isArray(n)?n.slice():m.extend({},n):n},m.tap=function(n,t){return t(n),n},m.isMatch=function(n,t){var r=m.keys(t),e=r.length;if(null==n)return!e;for(var u=Object(n),i=0;e>i;i++){var o=r[i];if(t[o]!==u[o]||!(o in u))return!1}return!0};var E=function(n,t,r,e){if(n===t)return 0!==n||1/n===1/t;if(null==n||null==t)return n===t;n instanceof m&&(n=n._wrapped),t instanceof m&&(t=t._wrapped);var u=f.call(n);if(u!==f.call(t))return!1;switch(u){case"[object RegExp]":case"[object String]":return""+n==""+t;case"[object Number]":return+n!==+n?+t!==+t:0===+n?1/+n===1/t:+n===+t;case"[object Date]":case"[object Boolean]":return+n===+t}var i="[object Array]"===u;if(!i){if("object"!=typeof n||"object"!=typeof t)return!1;var o=n.constructor,a=t.constructor;if(o!==a&&!(m.isFunction(o)&&o instanceof o&&m.isFunction(a)&&a instanceof a)&&"constructor"in n&&"constructor"in t)return!1}r=r||[],e=e||[];for(var c=r.length;c--;)if(r[c]===n)return e[c]===t;if(r.push(n),e.push(t),i){if(c=n.length,c!==t.length)return!1;for(;c--;)if(!E(n[c],t[c],r,e))return!1}else{var l,s=m.keys(n);if(c=s.length,m.keys(t).length!==c)return!1;for(;c--;)if(l=s[c],!m.has(t,l)||!E(n[l],t[l],r,e))return!1}return r.pop(),e.pop(),!0};m.isEqual=function(n,t){return E(n,t)},m.isEmpty=function(n){return null==n?!0:w(n)&&(m.isArray(n)||m.isString(n)||m.isArguments(n))?0===n.length:0===m.keys(n).length},m.isElement=function(n){return!(!n||1!==n.nodeType)},m.isArray=p||function(n){return"[object Array]"===f.call(n)},m.isObject=function(n){var t=typeof n;return"function"===t||"object"===t&&!!n},m.each(["Arguments","Function","String","Number","Date","RegExp","Error"],function(n){m["is"+n]=function(t){return f.call(t)==="[object "+n+"]"}}),m.isArguments(arguments)||(m.isArguments=function(n){return m.has(n,"callee")}),"function"!=typeof/./&&"object"!=typeof Int8Array&&(m.isFunction=function(n){return"function"==typeof n||!1}),m.isFinite=function(n){return isFinite(n)&&!isNaN(parseFloat(n))},m.isNaN=function(n){return m.isNumber(n)&&n!==+n},m.isBoolean=function(n){return n===!0||n===!1||"[object Boolean]"===f.call(n)},m.isNull=function(n){return null===n},m.isUndefined=function(n){return n===void 0},m.has=function(n,t){return null!=n&&s.call(n,t)},m.noConflict=function(){return e._=u,this},m.identity=function(n){return n},m.constant=function(n){return function(){return n}},m.noop=function(){},m.property=function(n){return function(t){return null==t?void 0:t[n]}},m.propertyOf=function(n){return null==n?function(){}:function(t){return n[t]}},m.matcher=m.matches=function(n){return n=m.extendOwn({},n),function(t){return m.isMatch(t,n)}},m.times=function(n,t,r){var e=Array(Math.max(0,n));t=d(t,r,1);for(var u=0;n>u;u++)e[u]=t(u);return e},m.random=function(n,t){return null==t&&(t=n,n=0),n+Math.floor(Math.random()*(t-n+1))},m.now=Date.now||function(){return(new Date).getTime()};var M={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;"},N=m.invert(M),I=function(n){var t=function(t){return n[t]},r="(?:"+m.keys(n).join("|")+")",e=RegExp(r),u=RegExp(r,"g");return function(n){return n=null==n?"":""+n,e.test(n)?n.replace(u,t):n}};m.escape=I(M),m.unescape=I(N),m.result=function(n,t,r){var e=null==n?void 0:n[t];return e===void 0&&(e=r),m.isFunction(e)?e.call(n):e};var B=0;m.uniqueId=function(n){var t=++B+"";return n?n+t:t},m.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};var T=/(.)^/,R={"'":"'","\\":"\\","\r":"r","\n":"n","\u2028":"u2028","\u2029":"u2029"},q=/\\|'|\r|\n|\u2028|\u2029/g,K=function(n){return"\\"+R[n]};m.template=function(n,t,r){!t&&r&&(t=r),t=m.defaults({},t,m.templateSettings);var e=RegExp([(t.escape||T).source,(t.interpolate||T).source,(t.evaluate||T).source].join("|")+"|$","g"),u=0,i="__p+='";n.replace(e,function(t,r,e,o,a){return i+=n.slice(u,a).replace(q,K),u=a+t.length,r?i+="'+\n((__t=("+r+"))==null?'':_.escape(__t))+\n'":e?i+="'+\n((__t=("+e+"))==null?'':__t)+\n'":o&&(i+="';\n"+o+"\n__p+='"),t}),i+="';\n",t.variable||(i="with(obj||{}){\n"+i+"}\n"),i="var __t,__p='',__j=Array.prototype.join,"+"print=function(){__p+=__j.call(arguments,'');};\n"+i+"return __p;\n";try{var o=new Function(t.variable||"obj","_",i)}catch(a){throw a.source=i,a}var c=function(n){return o.call(this,n,m)},l=t.variable||"obj";return c.source="function("+l+"){\n"+i+"}",c},m.chain=function(n){var t=m(n);return t._chain=!0,t};var z=function(n,t){return n._chain?m(t).chain():t};m.mixin=function(n){m.each(m.functions(n),function(t){var r=m[t]=n[t];m.prototype[t]=function(){var n=[this._wrapped];return c.apply(n,arguments),z(this,r.apply(m,n))}})},m.mixin(m),m.each(["pop","push","reverse","shift","sort","splice","unshift"],function(n){var t=i[n];m.prototype[n]=function(){var r=this._wrapped;return t.apply(r,arguments),"shift"!==n&&"splice"!==n||0!==r.length||delete r[0],z(this,r)}}),m.each(["concat","join","slice"],function(n){var t=i[n];m.prototype[n]=function(){return z(this,t.apply(this._wrapped,arguments))}}),m.prototype.value=function(){return this._wrapped},m.prototype.valueOf=m.prototype.toJSON=m.prototype.value,m.prototype.toString=function(){return""+this._wrapped},"function"==typeof define&&define.amd&&define("underscore",[],function(){return m})}).call(this);
//# sourceMappingURL=underscore-min.map
/*
 * 移动端 ( 兼容pc端) 手势操作库
 * @author muqin_deng
 * @descript https://github.com/DMQ/mTouch
 */

(function (global, doc, factory) {
	factory = factory(global, doc);

	//对外提供的接口
	if (typeof global.define === 'function' && (define.amd || define.cmd)) {
		define(function () {
			return factory;
		});
	} else {
		global.mTouch = global.mTouch || factory;
	}

})(window || this, document, function (global, doc) {

	var util = {
		//是否具有touch事件
		hasTouch: !!('ontouchstart' in window),
		/**
		 * 判断节点是否是事件代理的目标
		 * @param {object} el dom节点对象
		 * @param {string} proxyStr 事件委托的选择器
		 */
		isProxyTarget: function (el, proxyStr) {
			//class代理
			if (proxyStr.indexOf('.') === 0) {
				return new RegExp('(\\s|^)' + proxyStr.substring(1) + '(\\s|$)').test(el.className);
			//id代理
			} else if (proxyStr.indexOf('#') === 0) {
				return el.id == proxyStr.substring(1);
			//标签代理
			} else {
				return el.tagName.toLocaleLowerCase() == proxyStr;
			}
		},

		/**
		 * 获取滑动方向
		 * @param {number} x1 滑动开始的x坐标
		 * @param {number} y1 滑动开始的y坐标
		 * @param {number} x2 滑动结束的x坐标
		 * @param {number} y2 滑动结束的y坐标
		 */
		swipeDirection: function (x1, y1, x2, y2) {
			return Math.abs(x1 - x2) >= Math.abs(y1 - y2) ? (x1 > x2 ? 'LEFT' : 'RIGHT') : (y1 > y2 ? 'UP' : 'DOWN');
		},

		/**
		 * 触发事件，支持事件委托冒泡处理
		 * @param {string} eventType 事件类型
		 * @param {object} el 绑定了touch事件的dom元素
		 * @param {object} event 原生事件对象
		 */
		_trigger: function (eventType, el, event) {
			var target = event.target,
				currentTarget = el || event.currentTarget || event.target;

			if (!target || !currentTarget._m_touch_events.hasOwnProperty(eventType)) {
				return ;
			}

			var _events = currentTarget._m_touch_events,
				handlerList = _events[eventType];	//事件回调数组

			//开始冒泡循环
			while (1) {
				if (!target) { return ;}

				//已冒泡至顶，检测是否需要执行回调
				if (target === currentTarget) {
					//处理（执行）事件回调列表
					this._execHandler(handlerList, eventType, target, event, true);

					return ;	//已冒泡至顶，无需再冒泡
				}

				//存放临时回调数组
				var tempHandlerList = handlerList;
				// //清空事件回调数组
				// handlerList = [];
				//处理（执行）事件回调列表，并返回未执行的回调列表重新赋值给handlerList继续冒泡
				handlerList = this._execHandler(tempHandlerList, eventType, target, event, false);
				//如果执行结果返回false，则跳出冒泡及后续事件
				if (handlerList === false) {
					return ;
				}

				//向上冒泡
				target = target.parentNode;
			}
		},

		/**
		 * 执行事件回调
		 * @param {array} handlerList 事件回调列表
		 * @param {string} eventType 事件类型
		 * @param {object} target 当前目标dom节点
		 * @param {object} event 原生事件对象
		 * @param {boolean} isBubbleTop 是否冒泡至顶了
		 * @return {array} unExecHandlerList 返回不符合执行条件的未执行回调列表
		 */
		_execHandler: function (handlerList, eventType, target, event, isBubbleTop) {
			var i, len, handlerObj, proxyStr,
				execList = [], unExecHandlerList = [];

			for (i = 0, len = handlerList.length; i < len; i++) {
				handlerObj = handlerList[i];
				proxyStr = handlerObj.proxyStr;

				//如果冒泡至顶
				if (isBubbleTop) {
					//将符合执行条件的（没有事件委托）推进执行列表
					!proxyStr && execList.push(handlerObj);
				//未冒泡至顶
				} else {
					//将符合执行条件的（有事件委托且是委托目标）推进执行列表
					if (proxyStr && this.isProxyTarget(target, proxyStr)) {
						execList.push(handlerObj);
					} else {
						unExecHandlerList.push(handlerObj);
					}
				}
			}

			var handler;

			if (execList.length) {
				//执行符合条件的回调
				for (i = 0, len = execList.length; i < len; i++) {
					handler = execList[i].handler;
					//如果回调执行后返回false，则跳出冒泡及后续事件
					if (this._callback(eventType, handler, target, event) === false) {
						return false;
					}
				}
			}

			return unExecHandlerList;
		},

		/**
		 * 事件回调的最终处理函数
		 * @param {string} eventType 事件类型
		 * @param {function} handler 回调函数
		 * @param {object} el 目标dom节点
		 * @param {object} event 原生事件对象
		 */
		_callback: function (eventType, handler, el, event) {
			var	touch = this.hasTouch ? (event.touches.length ? event.touches[0] : event.changedTouches[0]) : event;

			//构建新的事件对象
			var mTouchEvent = {
				'type': eventType,
				'target': event.target,
				'pageX': touch.pageX || 0,
				'pageY': touch.pageY || 0
			};

			//如果是滑动事件则添加初始位置及滑动距离
			if (/^swip/.test(eventType) && event.startPosition) {
				mTouchEvent.startX = event.startPosition.pageX;
				mTouchEvent.startY = event.startPosition.pageY;
				mTouchEvent.moveX = mTouchEvent.pageX - mTouchEvent.startX;
				mTouchEvent.moveY = mTouchEvent.pageY - mTouchEvent.startY;
			}

			//将新的事件对象拓展到原生事件对象里
			event.mTouchEvent = mTouchEvent;

			var result = handler.call(el, event);
			//如果回调执行后返回false，则阻止默认行为和阻止冒泡
			if (result === false) {
				event.preventDefault();
				event.stopPropagation();
			}

			return result;
		}
	};

	//相关控制配置项
	var config = {
		tapMaxDistance: 5,		//单击事件允许的滑动距离
		doubleTapDelay: 200,	//双击事件的延时时长（两次单击的最大时间间隔）
		longTapDelay: 700,		//长按事件的最小时长
		swipeMinDistance: 5,	//触发方向滑动的最小距离
		swipeTime: 300			//触发方向滑动允许的最长时长
	};

	//事件类型列表
	var eventList = {
		TOUCH_START: util.hasTouch ? 'touchstart' : 'mousedown',
		TOUCH_MOVE: util.hasTouch ? 'touchmove' : 'mousemove',
		TOUCH_END: util.hasTouch ? 'touchend' : 'mouseup',
		TOUCH_CANCEL: 'touchcancel',
		TAP: 'tap',
		DOUBLE_TAP: 'doubletap',
		LONG_TAP: 'longtap',
		SWIPE_START: 'swipestart',
		SWIPING: 'swiping',
		SWIPE_END: 'swipeend',
		SWIPE_LEFT: 'swipeleft',
		SWIPE_RIGHT: 'swiperight',
		SWIPE_UP: 'swipeup',
		SWIPE_DOWN: 'swipedown'
	};

	/**
	 * touch相关主函数
	 * @param {object} el dom节点
	 */
	var Mtouch = function (elems) {
		//this._events = {};	//事件集合
		//this.el = el;

		[].forEach.call(elems, function (el, index) {
			//将事件结合保存在dom节点上，以达到共享的目的
			el._m_touch_events = el._m_touch_events || {};
			bindTouchEvents.call(null, el);
			this[index] = el;

		}.bind(this));

		this.length = elems.length;
	};

	Mtouch.prototype = {
		each: function (handler) {
			var i = 0, len = this.length;

			for (; i < len; i++) {
				handler.call(this[i], i);
			}

			return this;
		},
		/**
		 * 绑定事件函数，支持事件委托
		 * @param {string} eventType 事件类型，可同时绑定多个事件，用空格隔开
		 * @param [string] proxyStr 事件委托选择器（可选）
		 * @param {function} 事件监听回调函数
		 */
		on: function (eventType, proxyStr, handler) {
			//参数预处理
			if (typeof proxyStr === 'function') {
				handler = proxyStr;
				proxyStr = null;
			}
			if (typeof handler !== 'function' || !eventType || !eventType.length) {
				return this;
			}

			//拆分多个事件类型
			var eventTypesArr = eventType.split(/\s+/);

			this.each(function () {
				var _events = this._m_touch_events;

				eventTypesArr.forEach(function (type, key) {
					//如果未绑定过该事件，则创建一个
					if (!_events[type]) {
						_events[type] = [];
					}

					_events[type].push({
						'handler': handler,
						'proxyStr': proxyStr
					});
				});
			});

			return this;
		},

		/**
		 * 解绑事件
		 * @param {string} eventType 事件类型
		 * @param {string} proxyStr 事件委托选择器
		 */
		off: function (eventType, proxyStr, handler) {
			if (typeof proxyStr === 'function') {
				handler = proxyStr;
				proxyStr = null;
			}

			this.each(function () {
				var _events = this._m_touch_events;

				//没有传事件类型，则解绑所有事件
				if (!eventType) {
					this._m_touch_events = {};
					return ;
				}

				if (!_events.hasOwnProperty(eventType) || !_events[eventType].length) {
					return ;
				}

				//如果不需解绑代理及特定的回调，直接清空绑定的所有事件
				if (!proxyStr && !handler) {
					_events[eventType] = [];
					return ;
				}

				var handlerList = _events[eventType],
					len = handlerList.length - 1,
					handlerObj;
				
				//遍历事件数组，删除相应事件
				while (len >= 0) {
					handlerObj = handlerList[len];

					if (proxyStr && typeof handler === 'function') {
						if (handlerObj.proxyStr === proxyStr && handlerObj.handler === handler) {
							handlerList.splice(len, 1);
						}
					} else if (proxyStr && handlerObj.proxyStr === proxyStr) {
						handlerList.splice(len, 1);

					} else if (typeof handler === 'function' && handlerObj.handler === handler) {
						handlerList.splice(len, 1);
					}

					len--;
				}
			});
			
			return this;
		}
	};

	/**
	 * 绑定原生touch事件
	 * @param {object} el 对应的dom节点
	 */
	function bindTouchEvents(el) {
		if (el._m_touch_is_bind) {
			return ;
		}

		//触屏开始时间
		var touchStartTime = 0;

		//最后一次触屏时间
		var lastTouchTime = 0;

		//坐标位置
		var	x1, x2, x3, x4;

		//单击、长按定时器
		var tapTimer, longTapTimer;

		//记录是否触屏开始
		var isTouchStart = false;

		//重置所有定时器
		var resetTimer = function () {
			clearTimeout(tapTimer);
			clearTimeout(longTapTimer);
		};

		//触发单击事件
		var triggerSingleTap = function (event) {
			isTouchStart = false;
			resetTimer();
			util._trigger(eventList.TAP, el, event);
		};

		//开始触屏监听函数
		var touchstart = function (event) {
			var touch = util.hasTouch ? event.touches[0] : event;

			x1 = touch.pageX;
			y1 = touch.pageY;
			x2 = 0;
			y2 = 0;

			isTouchStart = true;
			touchStartTime = +new Date();

			//触发滑动开始事件
			util._trigger(eventList.SWIPE_START, el, event);

			clearTimeout(longTapTimer);
			//设置长按事件定时器
			longTapTimer = setTimeout(function () {
				isTouchStart = false;
				//清楚定时器
				resetTimer();
				util._trigger(eventList.LONG_TAP, el, event);
			}, config.longTapDelay);
		};

		//手指滑动监听函数
		var touchmove = function (event) {
			if (!isTouchStart) {
				return ;
			}

			var touch = util.hasTouch ? event.touches[0] : event,
				now   = +new Date();

			//记录滑动初始值，为swipe事件传递更多值
			event.startPosition = {
				'pageX': x1,
				'pageY': y1 
			};

			//触发滑动中事件
			util._trigger(eventList.SWIPING, el, event);

			x2 = touch.pageX;
			y2 = touch.pageY;

			var distanceX = Math.abs(x1 - x2),
				distanceY = Math.abs(y1 - y2);

			//如果滑动距离超过了单击允许的距离范围，则取消延时事件
			if (distanceX > config.tapMaxDistance || distanceY > config.tapMaxDistance) {
				resetTimer();
			}

			// if (distanceX > config.tapMaxDistance) {
			// 	event.preventDefault();
			// }
			
		};

		//触屏结束函数
		var touchend = function (event) {
			if (!isTouchStart) {
				return ;
			}

			var touch = util.hasTouch ? event.changedTouches[0] : event;	

			x2 = touch.pageX;
			y2 = touch.pageY;

			var now = +new Date();

			//触发滑动结束事件
			util._trigger(eventList.SWIPE_END, el, event);

			var distanceX = Math.abs(x1 - x2),
				distanceY = Math.abs(y1 - y2);

			//如果开始跟结束坐标距离在允许范围内则触发单击事件
			if (distanceX <= config.tapMaxDistance && distanceY <= config.tapMaxDistance) {
				//如果没有绑定双击事件，则立即出发单击事件
				if (!el._m_touch_events[eventList.DOUBLE_TAP] || !el._m_touch_events[eventList.DOUBLE_TAP].length ) {
					triggerSingleTap(event);
					lastTouchTime = now;

				//如果距离上一次触屏的时长大于双击延时时长，延迟触发单击事件
				} else if (now - lastTouchTime > config.doubleTapDelay){
					tapTimer = setTimeout(function () {
						triggerSingleTap(event);
					}, config.doubleTapDelay);

					lastTouchTime = now;

				//如果距离上一次触屏的时长在双击延时时长内
				//则清除单击事件计时器，并触发双击事件
				} else {
					resetTimer();
					util._trigger(eventList.DOUBLE_TAP, el, event);
					//双击后重置最后触屏时间为0，是为了从新开始计算下一次双击时长
					lastTouchTime = 0;
				}
			//触发方向滑动事件
			} else {
				//如果滑动时长在允许的范围内，且滑动距离超过了最小控制阀值，触发方向滑动事件
				if (now - touchStartTime <= config.swipeTime
					&& ( distanceX > config.swipeMinDistance  || distanceY > config.swipeMinDistance)
				) {
					//滑动方向LEFT, RIGHT, UP, DOWN
					var direction = util.swipeDirection(x1, y1, x2, y2);

					resetTimer();

					util._trigger(eventList['SWIPE_' + direction], el, event);
				}
			}

			isTouchStart = false;
		};

		//绑定触屏开始事件
		el.addEventListener(eventList.TOUCH_START, touchstart);
		//绑定触屏滑动事件
		el.addEventListener(eventList.TOUCH_MOVE, touchmove);
		//绑定触屏结束事件
		el.addEventListener(eventList.TOUCH_END, touchend);
		//绑定触屏取消事件
		el.addEventListener(eventList.TOUCH_CANCEL, resetTimer);

		el._m_touch_is_bind = true;	//标记该节点已经绑定过touch事件了
	}


	/** 
	 * 返回的辅助函数
	 * @param {string} selector 选择器字符串
	 */
	var mTouch = function (selector) {
		var elems;
		if (selector === doc || selector.nodeType === 1) {
			elems = [selector];
		} else {
			elems = doc.querySelectorAll(selector);
		}

		return new Mtouch(elems);
	};
	//配置touch事件相关控制的接口
	mTouch.config = function (cfg) {
		for (var k in cfg) {
			if (cfg.hasOwnProperty(k)) {
				config[k] = cfg[k];
			}
		}
	};

	return mTouch;
});
;(function (window, doc, factory) {
	factory = factory(window, doc);

	window.webUtil = window.webUtil || factory;

})(window, document, function (window, doc) {
	// 工具集
	var webUtil = {
		// 根据命名空间执行init方法
		run: function (namespaceStr) {
			if (!namespaceStr) {return ;}

			var namespaceArr = namespaceStr.split('.'),
				obj = window;

			for (var i = 0, len = namespaceArr.length; i < len; i++) {
				if (!obj[namespaceArr[i]]) {
					return ;
				}

				obj = obj[namespaceArr[i]];
			}

			typeof obj.init === 'function' && obj.init();
		},

		// 创建命名空间
		namespace: function (namespaceStr, newObj) {
			if (!namespaceStr) {return ;}

			var namespaceArr = namespaceStr.split('.'),
				obj = window;

			for (var i = 0, len = namespaceArr.length; i < len; i++) {
				if (!obj[namespaceArr[i]]) {
					if (newObj !== undefined && i == len - 1) {
						obj = obj[namespaceArr[i]] = newObj;
						break;
					}

					obj[namespaceArr[i]] = {};
				}

				obj = obj[namespaceArr[i]];
			}

			return obj;
		},

		// 获取url键值对
        getUrlKeyValObj: function () {
        	var url = window.location.search,
				arr, i, len,
				paramsObj = {};	
        	
        	if (this.getUrlKeyValObj.urlKeyValObj) {
        		return this.getUrlKeyValObj.urlKeyValObj;
        	}
	
			arr = url.substring(1).split('&');

			if (!arr.length) {
				return paramsObj;
			}
			
			for (i = 0, len = arr.length; i < len; i++) {
				var reg = /(.*)\=(.*)/g,
					match = reg.exec(arr[i]);
	
				if (match && match[1]) {
					paramsObj[decodeURIComponent(match[1])] = decodeURIComponent(match[2]);
				}
			}
			
			this.getUrlKeyValObj.urlKeyValObj = paramsObj;
			
			return paramsObj;
        },
        
        // 获取url的对应参数的值
		getUrlValue: function (param) {
			if (!param) {
				return '';
			}
			
			var paramsObj = this.getUrlKeyValObj();

			if (paramsObj.hasOwnProperty(param)) {
				return paramsObj[param];
			} else {
				return '';
			}
		},

		/*
		 * 函数节流
		 * @param {function} method	要进行节流的函数
		 * @param {number} delay 延时时间(ms)
		 * @param {number} duration 经过duration时间(ms)必须执行函数method
		 */
		throttle: function (method, delay, duration) {
			var timer = null,
				begin = null;
            return function () {
                var context = this,
                	args = arguments,
                	current = new Date();
                if (!begin) {
                	begin = current;
                }
                if (timer) {
                	window.clearTimeout(timer);
                }
                if (duration && current - begin >= duration) {
                     method.apply(context,args);
                     begin = null;
                }else {
                    timer = window.setTimeout(function () {
                        method.apply(context, args);
                        begin = null;
                    }, delay);
               }
            };
		},

		uuid: function (len, radix) {
		    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
		    var uuid = [], i;
		    radix = radix || chars.length;
		 
		    if (len) {
		      // Compact form
		      for (i = 0; i < len; i++) {
		      	uuid[i] = chars[0 | Math.random() * radix];
		      }
		    } else {
		      var r;
		 
		      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
		      uuid[14] = '4';

		      for (i = 0; i < 36; i++) {
		        if (!uuid[i]) {
		          r = 0 | Math.random() * 16;
		          uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
		        }
		      }
		    }
		 
		    return uuid.join('');
		},

		// 获取字节长度
		getByteLength: function(str){
            return str.replace(/[^\x00-\xff]/g, '**').length;
        },

        /**
		 * 根据字节长度截取字符串
		 * @param {string} str 目标字符串
		 * @param {number} num 需要截取的长度
		 * @return {string} 返回截取后的字符串
		 */
		getByteVal: function(str, num){
			var len = 0,
				returnValue = '';
			for (var i = 0, l = str.length; i < l; i++) {
				if (str[i].match(/[^\x00-\xff]/ig) != null) { //全角
		            len += 2;
		        } else {
		            len += 1;
				}
				if (len > num) {
					break;
				}
				returnValue += str[i];
			}
			return returnValue;
		},

		// 转义特殊字符<,>,",&
		escape: function (str) {
			if (!str) {
				return '';
			}

			var escape = {
				'<': '&lt;',
				'>': '&gt;',
				'\"': '&quot;',
				'&': '&amp;'
			};

			return str.replace(/[&<>"]/g, function (match) {
				return escape[match] || match;
			});
		},

		tab: function (panelWrap, activeClass) {
			var $wrap = $(panelWrap);

			$wrap.on('click', '[data-panel]', function () {
				activeClass && $wrap.find('[data-panel]').removeClass(activeClass);
				$wrap.find('[data-pindex]').removeClass(activeClass).hide();

				var index = $(this).addClass(activeClass).data('panel');
				$wrap.find('[data-pindex=' + index + ']').addClass(activeClass).show();
			});
		},

		tips: function (msg) {
			$('.ui-tips').remove();

			var $tips = $('<div class="ui-tips floatin"></div>');
			$tips.text(msg).appendTo('body');
		},

		animationEnd: function (el, cb, remove) {
			var eventType = whichAnimationEvent(el);

			el.addEventListener(eventType, handler, false);

			function handler() {
		    	if (remove !== false) {
		    		el.removeEventListener(eventType, handler, false);
		    	}

		    	var args = [].slice.call(arguments);
		    	cb.apply(null, args);
		    }

			function whichAnimationEvent(el) {
				var animations = {
					'animation':'animationend',
					'OAnimation':'oAnimationEnd',
					'MozAnimation':'animationend',
					'WebkitAnimation':'webkitAnimationEnd'
				}

				for(var a in animations){
				   if( el.style[a] !== undefined ){
				       return animations[a];
				   }
				}
		    }
		},

		delay: function (time) {
			var deferred = $.Deferred();

			setTimeout(function () {
				deferred.resolve();
			}, time || 0);

			return deferred.promise();
		},

		// 简单模板拼装
		Template: function () {
			this.arr = [];
			this._pushAll(arguments);
		},
        //截取URL键值对
        getRequest : function(){ 
            var url = window.location.search;
            var theRequest = new Object();
            if (url.indexOf("?") != -1) {
                var str = url.substr(1);
                strs = str.split("&");
                for (var i = 0; i < strs.length; i++) {
                    theRequest[strs[i].split("=")[0]] = decodeURIComponent(strs[i].split("=")[1]);
                }
            }
            return theRequest;
        }
	};
    

	webUtil.Template.prototype = {
		constructor: webUtil.Template,

		_: function () {
			this._pushAll(arguments);
			return this;
		},

		toString: function () {
			return this.arr.join('');
		},

		_pushAll: function (arguments) {
			var args = [].slice.call(arguments);
			this.arr = this.arr.concat(args);
		},

		clean: function () {
			this.arr = [];
			return this;
		}
	};

	return webUtil;
});