<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.kingdee.com/sns/core" prefix="s"%>
<c:set var="skinPath" value="${staticPath}/attendance/templates/n/skin-default/n" />
<c:set var="timestamp" value="?201510271753001" />

<%-- <c:if test="${debugMode}"> --%>
<link rel="stylesheet" href="${skinPath}/group.css${timestamp}" />

<%--基础 --%>
<link rel="stylesheet" href="${skinPath}/space.css${timestamp}" />
<link rel="stylesheet" href="${skinPath}/network.css${timestamp}" />

<%-- 统计报表 --%>
<link rel="stylesheet" href="${skinPath}/chart.css${timestamp}" />

<%-- 应用中心 --%>
<link rel="stylesheet" href="${skinPath}/application.css${timestamp}" />

<%-- 新的签到样式 --%>
<link rel="stylesheet" href="${skinPath}/n-attendance.css${timestamp}" />

<%-- </c:if> --%>

<%-- ============================== 合并压缩版本 ============================== --%>
<%-- <c:if test="${!debugMode}">
<s:signature src="${skinPath}/_attendance.css" type="css"/>
</c:if> --%>
<%-- ============================== 合并压缩版本[END] ============================== --%>
