<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
  <%@ taglib uri="http://www.kingdee.com/sns/core" prefix="s"%>
<c:set var="appJsPath" value="${staticPath}/attendance/js/n" />
<c:set var="timestamp" value="?201511161542" />

<%-- <c:if test="${debugMode}"> --%>
<script src="${appJsPath}/pages/department-tree.js${timestamp}"></script>
<script src="${appJsPath}/pages/attendance-takenotes.js${timestamp}"></script><%-- 埋点统计 --%>
<script src="${appJsPath}/pages/attendance-base.js${timestamp}"></script>
<script src="${appJsPath}/pages/attendance-export-dialog.js${timestamp}"></script><%-- 导出报表弹窗 --%>
<script src="${appJsPath}/pages/attendance-settings.js${timestamp}201602241418"></script><%--签到设置 --%>
<script src="${appJsPath}/pages/attendance-analysis.js${timestamp}"></script><%--签到分析 --%>
<script src="${appJsPath}/pages/attendance-companyReport.js${timestamp}"></script><%--公司报表 --%>
<script src="${appJsPath}/pages/attendance-report.js${timestamp}"></script><%--签到报表 --%>
<script src="${appJsPath}/pages/attendance-group.js${timestamp}"></script><%-- 小组签到 --%>
<script src="${appJsPath}/pages/attendance-holiday.js${timestamp}"></script><%-- 节假日签到设置 --%>
<script src="${appJsPath}/pages/attendance-department-statistics.js${timestamp}"></script><%-- 部门考勤统计 --%>
<script src="${appJsPath}/pages/attendance-company-analysis.js${timestamp}"></script><%-- 公司考勤统计分析 --%>
<%-- HR权限设置 --%>
<script src="${appJsPath}/pages/attendance-hr.js${timestamp}"></script>
<script src="${appJsPath}/pages/attendance-settings-admin.js${timestamp}201602261048"></script><%-- 管理员设置 --%>
<script src="${appJsPath}/pages/attendance-settings-workday.js${timestamp}"></script><%-- 工作日设置 --%>

<script src="${appJsPath}/pages/attendance-statistics.js${timestamp}"></script><%--签到统计 --%>
<%-- </c:if> --%>

<%-- ============================== 合并压缩版本 ============================== --%>
<%-- <c:if test="${!debugMode}">
<script src="${appJsPath}/_attendance.js?201402191434"></script>
  <s:signature src="${appJsPath}/_attendance.js" type="js"/>
</c:if> --%>
<%-- ============================== 合并压缩版本[END] ============================== --%>
