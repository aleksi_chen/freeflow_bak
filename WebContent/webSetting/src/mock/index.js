var Chance = require('chance')
var chance = new Chance()

chance.mixin({
    'detail': function () {
        return {
            "template": {
                "tenantId": "",
                "recordId": "b1af4895-f02a-40c0-8288-260aecca210f",
                "createTime": 1470971909179,
                "updateTime": 1472787235639,
                "status": "1",
                "isRecord": false,
                "globalId": "",
                "isPublic": false,
                "photo": "",
                "version": "1.0",
                "appName": "freeflow",
                "id": "57ad400560b2573480e9d42d",
                "isDelete": false,
                "title": "全控件测试",
                "num": 1,
                // "components": "[{\"id\":\"titlefield-44373613-8CD7-45CD-90EC-DFFDB150EC44\",\"type\":\"titlefield\",\"label\":\"标题\",\"placeholder\":\"请输入\",\"require\":true},{\"id\":\"textfield-T7CJB9Q2\",\"type\":\"textfield\",\"label\":\"单行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"textareafield-7A6R7IL4\",\"type\":\"textareafield\",\"label\":\"多行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"textareafield-4KJ3NGEV\",\"type\":\"textareafield\",\"label\":\"多行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"numberfield-6DL72IC9\",\"type\":\"numberfield\",\"label\":\"数字输入框\",\"placeholder\":\"请输入\",\"require\":false,\"decimal\":false},{\"id\":\"numberfield-N764SL4D\",\"type\":\"numberfield\",\"label\":\"数字输入框\",\"placeholder\":\"请输入\",\"require\":false,\"decimal\":false},{\"id\":\"moneyfield-7HQET61H\",\"type\":\"moneyfield\",\"label\":\"金额(元)\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"moneyfield-0RTVM2HD\",\"type\":\"moneyfield\",\"label\":\"金额(元)\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"radiofield-O0LU7BDI\",\"type\":\"radiofield\",\"label\":\"单选框\",\"placeholder\":\"请选择\",\"require\":false,\"options\":[\"选项1\",\"选项2\",\"选项3\"]},{\"id\":\"radiofield-OFS7EK8D\",\"type\":\"radiofield\",\"label\":\"单选框\",\"placeholder\":\"请选择\",\"require\":false,\"options\":[\"选项1\",\"选项2\",\"选项3\"]},{\"id\":\"datefield-NN2CCPS3\",\"type\":\"datefield\",\"label\":\"日期\",\"placeholder\":\"请选择\",\"require\":false,\"format\":\"yyyy/MM/dd\"},{\"id\":\"datefield-HGEQ8C8F\",\"type\":\"datefield\",\"label\":\"日期\",\"placeholder\":\"请选择\",\"require\":false,\"format\":\"yyyy/MM/dd\"},{\"id\":\"daterangefield-9858DB6M\",\"type\":\"daterangefield\",\"label\":\"日期区间\",\"placeholder\":\"请选择\",\"require\":false,\"labels\":[\"开始日期\",\"结束日期\"],\"format\":\"yyyy/MM/dd\"},{\"id\":\"daterangefield-B6D0C5B0\",\"type\":\"daterangefield\",\"label\":\"日期区间\",\"placeholder\":\"请选择\",\"require\":false,\"labels\":[\"开始日期\",\"结束日期\"],\"format\":\"yyyy/MM/dd\"},{\"id\":\"photofield-VUP2K611\",\"type\":\"photofield\",\"label\":\"图片\",\"placeholder\":\"请选择\",\"require\":false},{\"id\":\"textfield-QU6DAS08\",\"type\":\"textfield\",\"label\":\"单行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"filefield-NK6K5B5J\",\"type\":\"filefield\",\"label\":\"文件\",\"placeholder\":\"请选择\",\"require\":false}]",
                "components": "[{\"id\":\"titlefield-44373613-8CD7-45CD-90EC-DFFDB150EC44\",\"type\":\"titlefield\",\"label\":\"标题\",\"placeholder\":\"请输入\",\"require\":true},{\"id\":\"textfield-T7CJB9Q2\",\"type\":\"textfield\",\"label\":\"单行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"textareafield-7A6R7IL4\",\"type\":\"textareafield\",\"label\":\"多行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"textareafield-4KJ3NGEV\",\"type\":\"textareafield\",\"label\":\"多行输入框\",\"placeholder\":\"请输入\",\"require\":false}]",
                "user": {
                    "deptName": "codinghome",
                    "isDeptAdmin": false,
                    "orgInfoId": "572ffb1400b0cbb73d8f71d5",
                    "networkId": "572ffb1400b0cbb73d8f71d5",
                    "eid": "2703901",
                    "photo": "http://192.168.22.144/space/c/photo/load?id=5785eebf00b0a9f1266dc949",
                    "oid": "5785eebf00b0a9f1266dc93a",
                    "userId": "5785eebf00b0a9f1266dc922",
                    "name": "yuce_wang",
                    "personId": "5785eebf00b0a9f1266dc938",
                    "isAdmin": true,
                    "deptId": "b8caacc5-faef-4000-9c2b-4904c16755dc",
                    "openId": "5785eebf00b0a9f1266dc922"
                },
                "isGlobal": false
            },
            "approvalCndGroups": [],
            "approvalRule": {
                "tenantId": "",
                "templateId": "57ad400560b2573480e9d42d",
                "id": "57ad400560b2573480e9d42f",
                "content": "",
                "createTime": null,
                "cndSelectId": "titlefield-44373613-8CD7-45CD-90EC-DFFDB150EC44",
                "approvalType": "0",
                "cndContext": "{\"numberfield-ESCOTRSK\":[0,0,0],\"moneyfield-9PP964C5\":[0,0,0]}"
            }
        }
    }, create: function () {
        return {
            "template": {
                "tenantId": "",
                "recordId": "b1af4895-f02a-40c0-8288-260aecca210f",
                "createTime": 1470971909179,
                "updateTime": 1472787235639,
                "status": "1",
                "isRecord": false,
                "globalId": "",
                "isPublic": false,
                "photo": "",
                "version": "1.0",
                "appName": "freeflow",
                "id": "57ad400560b2573480e9d42d",
                "isDelete": false,
                "title": "全控件测试",
                "num": 1,
                // "components": "[{\"id\":\"titlefield-44373613-8CD7-45CD-90EC-DFFDB150EC44\",\"type\":\"titlefield\",\"label\":\"标题\",\"placeholder\":\"请输入\",\"require\":true},{\"id\":\"textfield-T7CJB9Q2\",\"type\":\"textfield\",\"label\":\"单行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"textareafield-7A6R7IL4\",\"type\":\"textareafield\",\"label\":\"多行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"textareafield-4KJ3NGEV\",\"type\":\"textareafield\",\"label\":\"多行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"numberfield-6DL72IC9\",\"type\":\"numberfield\",\"label\":\"数字输入框\",\"placeholder\":\"请输入\",\"require\":false,\"decimal\":false},{\"id\":\"numberfield-N764SL4D\",\"type\":\"numberfield\",\"label\":\"数字输入框\",\"placeholder\":\"请输入\",\"require\":false,\"decimal\":false},{\"id\":\"moneyfield-7HQET61H\",\"type\":\"moneyfield\",\"label\":\"金额(元)\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"moneyfield-0RTVM2HD\",\"type\":\"moneyfield\",\"label\":\"金额(元)\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"radiofield-O0LU7BDI\",\"type\":\"radiofield\",\"label\":\"单选框\",\"placeholder\":\"请选择\",\"require\":false,\"options\":[\"选项1\",\"选项2\",\"选项3\"]},{\"id\":\"radiofield-OFS7EK8D\",\"type\":\"radiofield\",\"label\":\"单选框\",\"placeholder\":\"请选择\",\"require\":false,\"options\":[\"选项1\",\"选项2\",\"选项3\"]},{\"id\":\"datefield-NN2CCPS3\",\"type\":\"datefield\",\"label\":\"日期\",\"placeholder\":\"请选择\",\"require\":false,\"format\":\"yyyy/MM/dd\"},{\"id\":\"datefield-HGEQ8C8F\",\"type\":\"datefield\",\"label\":\"日期\",\"placeholder\":\"请选择\",\"require\":false,\"format\":\"yyyy/MM/dd\"},{\"id\":\"daterangefield-9858DB6M\",\"type\":\"daterangefield\",\"label\":\"日期区间\",\"placeholder\":\"请选择\",\"require\":false,\"labels\":[\"开始日期\",\"结束日期\"],\"format\":\"yyyy/MM/dd\"},{\"id\":\"daterangefield-B6D0C5B0\",\"type\":\"daterangefield\",\"label\":\"日期区间\",\"placeholder\":\"请选择\",\"require\":false,\"labels\":[\"开始日期\",\"结束日期\"],\"format\":\"yyyy/MM/dd\"},{\"id\":\"photofield-VUP2K611\",\"type\":\"photofield\",\"label\":\"图片\",\"placeholder\":\"请选择\",\"require\":false},{\"id\":\"textfield-QU6DAS08\",\"type\":\"textfield\",\"label\":\"单行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"filefield-NK6K5B5J\",\"type\":\"filefield\",\"label\":\"文件\",\"placeholder\":\"请选择\",\"require\":false}]",
                "components": "[{\"id\":\"titlefield-44373613-8CD7-45CD-90EC-DFFDB150EC44\",\"type\":\"titlefield\",\"label\":\"标题\",\"placeholder\":\"请输入\",\"require\":true},{\"id\":\"textfield-T7CJB9Q2\",\"type\":\"textfield\",\"label\":\"单行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"textareafield-7A6R7IL4\",\"type\":\"textareafield\",\"label\":\"多行输入框\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"textareafield-4KJ3NGEV\",\"type\":\"textareafield\",\"label\":\"多行输入框\",\"placeholder\":\"请输入\",\"require\":false}]",
                "user": {
                    "deptName": "codinghome",
                    "isDeptAdmin": false,
                    "orgInfoId": "572ffb1400b0cbb73d8f71d5",
                    "networkId": "572ffb1400b0cbb73d8f71d5",
                    "eid": "2703901",
                    "photo": "http://192.168.22.144/space/c/photo/load?id=5785eebf00b0a9f1266dc949",
                    "oid": "5785eebf00b0a9f1266dc93a",
                    "userId": "5785eebf00b0a9f1266dc922",
                    "name": "yuce_wang",
                    "personId": "5785eebf00b0a9f1266dc938",
                    "isAdmin": true,
                    "deptId": "b8caacc5-faef-4000-9c2b-4904c16755dc",
                    "openId": "5785eebf00b0a9f1266dc922"
                },
                "isGlobal": false
            }

        }
    },
    approvalRuleDetail: function () {
        return {
            "template": {
                "tenantId": "",
                "recordId": "706e3eb50b3740b68d5ff7a30b281e87",
                "createTime": 1470646332586,
                "updateTime": 1473146473819,
                "status": "1",
                "isRecord": false,
                "globalId": "",
                "isPublic": false,
                "photo": "",
                "version": "1.0",
                "appName": "freeflow",
                "id": "57a8483c60b2b3727e8cd998",
                "isDelete": false,
                "title": "呜呜房东是个是",
                "num": 1,
                "components": "[{\"id\":\"titlefield-4AFE63DD-09BE-4C66-8B27-3AA7C2F7D9BC\",\"type\":\"titlefield\",\"label\":\"标题\",\"placeholder\":\"请输入\",\"require\":true},{\"id\":\"moneyfield-L6AAE2AI\",\"type\":\"moneyfield\",\"label\":\"金额(元)\",\"placeholder\":\"请输入\",\"require\":false},{\"id\":\"numberfield-V8ECMKTH\",\"type\":\"numberfield\",\"label\":\"数字输入框\",\"placeholder\":\"请输入\",\"require\":true},{\"id\":\"moneyfield-90JLLJ1A\",\"type\":\"moneyfield\",\"label\":\"金额(元)\",\"placeholder\":\"请输入\",\"require\":true}]",
                "user": {
                    "deptName": "codinghome",
                    "isDeptAdmin": false,
                    "orgInfoId": "572ffb1400b0cbb73d8f71d5",
                    "networkId": "572ffb1400b0cbb73d8f71d5",
                    "eid": "2703901",
                    "photo": "http://192.168.22.144/space/c/photo/load?id=5785eebf00b0a9f1266dc949",
                    "oid": "5785eebf00b0a9f1266dc93a",
                    "userId": "5785eebf00b0a9f1266dc922",
                    "name": "yuce_wang",
                    "personId": "5785eebf00b0a9f1266dc938",
                    "isAdmin": true,
                    "deptId": "b8caacc5-faef-4000-9c2b-4904c16755dc",
                    "openId": "5785eebf00b0a9f1266dc922"
                },
                "isGlobal": false
            },
            "approvalCndGroups": [],
            "approvalRule": {
                "tenantId": "",
                "templateId": "57a8483c60b2b3727e8cd998",
                "id": "57a8483c60b2b3727e8cd99a",
                "content": "",
                "createTime": null,
                "cndSelectId": "numberfield-V8ECMKTH",
                "approvalType": "0",
                "cndContext": "[0,0,0]"
            }
        }
    },
    approvalRuleSave: function () {
        return {
            "approvalCndGroups": [],
            "approvalRule": {
                "tenantId": "",
                "templateId": "57a8483c60b2b3727e8cd998",
                "id": "57a8483c60b2b3727e8cd99a",
                "content": "",
                "createTime": null,
                "cndSelectId": "moneyfield-90JLLJ1A",
                "approvalType": "0",
                "cndContext": "{\"0\":0,\"1\":0,\"2\":0,\"numberfield-V8ECMKTH\":[0,0,0],\"moneyfield-90JLLJ1A\":[0,0,0]}"
            }
        }
    },
    userInfo: function () {
        return {
            "deptName": "codinghome",
            "isDeptAdmin": false,
            "orgInfoId": "572ffb1400b0cbb73d8f71d5",
            "networkId": "572ffb1400b0cbb73d8f71d5",
            "eid": "2703901",
            "photo": "http://192.168.22.144/space/c/photo/load?id=5785eebf00b0a9f1266dc949",
            "oid": "5785eebf00b0a9f1266dc93a",
            "userId": "5785eebf00b0a9f1266dc922",
            "name": "yuce_wang",
            "personId": "5785eebf00b0a9f1266dc938",
            "isAdmin": true,
            "deptId": "b8caacc5-faef-4000-9c2b-4904c16755dc",
            "openId": "5785eebf00b0a9f1266dc922"
        }
    }
})
var meta = {
    code: 0,
    success: true,
    msg: "操作成功！"
}

module.exports = {
    detail: function (req, res) {
        res.json({meta: meta, data: chance.detail()})
    },
    create: function (req, res) {
        res.json({meta: meta, data: chance.create()})
    },
    approvalRuleDetail: function (req, res) {
        res.json({meta: meta, data: chance.approvalRuleDetail()})
    },
    approvalRuleSave: function (req, res) {
        res.json({meta: meta, data: chance.approvalRuleSave()})
    },
    userInfo:function (req, res) {
        res.json(chance.userInfo())
    }
}
