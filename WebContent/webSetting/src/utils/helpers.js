const helpers = {
    //截取URL键值对
    getRequest: function (search) {
        var url = window.location.search;
        var theRequest = new Object();
        var strs;
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            strs = str.split("&");
            for (let i = 0; i < strs.length; i++) {
                theRequest[strs[i].split("=")[0]] = decodeURIComponent(strs[i].split("=")[1]);
            }
        }
        return theRequest;
    },
    addRequest: function (url, params) {
        var arr =[],
            str=(url.indexOf("?") === -1)?'?':''
        const query = helpers.getRequest(url)
        for (let o in params) {
            if (!query[o]) {
                arr.push(o + '=' + params[o])
            }
        }
        str+=  arr.join('&')
        return url + str
    }
}
module.exports = helpers
