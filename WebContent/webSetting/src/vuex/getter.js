const getters = {
    global:function (state) {
        return state.global
    },
    templateId: function (state) {
        return state.templateId
    },
    templateName: function (state) {
        return state.templateName
    },
    approvalStep: function (state) {
        return state.approvalStep;
    },
    personRuleType: function (state) {
        return state.personRuleType;
    },
    personSelectList: function (state) {
        return state.personSelectList;
    },
    openDialog: function (state) {
        return state.openDialog;
    },
    conditionSelect: function (state) {
        return state.conditionSelect;
    },
    conditionList: function (state) {
        return state.conditionList;
    },
    conditionCount: function (state) {
        return state.conditionCount;
    },
    conditionNumberData: function (state) {
        return state.conditionNumberData;
    },
    conditionPersonSelect: function (state) {
        return state.conditionPersonSelect;
    },
    conditionPersonData: function (state) {
        return state.conditionPersonData;
    },
    conditionComplete: function (state) {
        return state.conditionComplete;
    },
    designSubmit:function (state) {
        return state.designSubmit;
    }

}
module.exports = getters
