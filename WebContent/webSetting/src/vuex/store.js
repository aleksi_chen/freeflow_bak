import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

var state = {
    global:{},//审批个人数据
    /* 审批规则设置 */
    templateId: "",//审批模板id
    templateName: "",//审批模板名称
    approvalStep: 0,//当前审批步骤
    personRuleType: 0,//当前审批规则配置类型
    personSelectList: {},//不限条件审批人列表对象
    openDialog: false,//悬浮框状态
    conditionSelect: 0,//按条件审批人选中组件id
    conditionList: [],//按条件审批人 条件数据列表
    conditionCount: 0,//按条件审批人 符合展现的条件数量
    conditionNumberData: {},//按条件审批人 数字输入条件区间数据
    conditionPersonSelect: false,//按条件审批人 是否展现 选择审批人
    conditionPersonData: {},//按条件审批人 条件区间对应选中审批人数据
    conditionComplete: {},//按条件审批人 对应条件是否已设置审批人
    /* 审批设计器 */
    designSubmit:function () {
    }
};

var mutations = {
    SETGLOBAL:function (state, data) {
        state.global = data;
    },
    SETTEMPLATEID: function (state, data) {
        state.templateId = data;
    },
    SETTEMPLATENAME: function (state, data) {
        state.templateName = data;
    },
    SETAPPROVALSTEP: function (state, data) {
        state.approvalStep = data;
    },
    SETPERSONRULETYPE: function (state, data) {
        state.personRuleType = data;
    },
    SETPERSONSELECTLIST: function (state, data) {
        state.personSelectList = JSON.parse(JSON.stringify(data))
    },
    TOGGLEOPENDIALOG: function (state) {
        state.openDialog = !state.openDialog;
    },
    SETCONDITIONSELECT: function (state, data) {
        state.conditionSelect = data;
    },
    SETCONDITIONLIST: function (state, data) {
        state.conditionList = data;
    },
    SETCONDITIONCOUNT: function (state, data) {
        state.conditionCount = data;
    },
    SETCONDITIONNUMBERDATA: function (state, data) {
        state.conditionNumberData = Vue.util.extend({}, data);
    },
    SETONECONDITIONNUMBER: function (state, id, data) {
        state.conditionNumberData[id] = data;
    },
    SETCONDITIONPERSONSELECT: function (state, data) {
        state.conditionPersonSelect = !state.conditionPersonSelect;
    },
    SETCONDITIONPERSONDATA: function (state, id, data) {
        state.conditionPersonData[id] = data.slice(0)
        state.conditionPersonData = Vue.util.extend({}, state.conditionPersonData)
    },
    SETCONDITIONCOMPLETE: function (state, id, data) {
        state.conditionComplete[id] = data
        state.conditionComplete = Vue.util.extend({}, state.conditionComplete)
    },
    SETDESIGINSUBMIT:function (state,data) {
        state.designSubmit=data
    }

}

var store = new Vuex.Store({
    state,
    mutations,
    strict: false
})

export default store
