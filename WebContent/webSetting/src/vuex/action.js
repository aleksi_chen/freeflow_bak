import vue from 'vue'
import http from '../api/http'
import helper from '../utils/helpers'
import LappPersonSelect from '../components/plugins/PersonSelect'
const actions = {
    getGlobal: function (store, data) {
        http.fetch(location.origin+ '/freeflow/web/userInfo.json',{
            urlAssemble:false
        }).then(function (rsp) {
            store.dispatch('SETGLOBAL', rsp.body)
        })
    },
    setTemplateId: function (store, data) {
        store.dispatch('SETTEMPLATEID', data)
    },
    setApprovalStep: function (store, data) {
        store.dispatch('SETAPPROVALSTEP', data)
    },
    setPersonRuleTypr: function (store, data) {
        store.dispatch('SETPERSONRULETYPE', data)
    },
    setPersonSelectList: function (store, data) {
        store.dispatch('SETPERSONSELECTLIST', data)
    },
    togglleOpenDialog: function (store) {
        store.dispatch('TOGGLEOPENDIALOG')
    },
    setConditionSelect: function (store, data) {
        store.dispatch('SETCONDITIONSELECT', data)
    },
    setConditionNumberData: function (store, data) {
        store.dispatch('SETCONDITIONNUMBERDATA', data)
    },
    setConditionPersonSelect: function (store) {
        store.dispatch('SETCONDITIONPERSONSELECT')
    },
    setConditionPersonData: function (store, data) {
        store.dispatch('SETCONDITIONPERSONDATA', data)
    },
    setOneConditionNumber: function (store, id, data) {
        store.dispatch('SETONECONDITIONNUMBER', id, data)
    },

    //检查按条件指定关键人是否完成
    checkconditionComplete: function (store, id, data) {
        var isComplete = false
        if (data && data.length > 0) {
            isComplete = data.some(function (e, i) {
                return e && e.usersContext && JSON.stringify(e.usersContext) !== '{}'
            })
        }
        store.dispatch('SETCONDITIONCOMPLETE', id, isComplete)
        return isComplete
    },

    //添加按条件指定关键人
    addConditionPersonList: function (store, id, index, conditionPerson,global) {
        var conditionPerson = conditionPerson.slice(0),
            selectedMembers = conditionPerson[index].usersContext || {}
        store.dispatch('TOGGLEOPENDIALOG')
        new LappPersonSelect({
            parentElement: 'body',
            eId: global.eid,
            selectedMembers: selectedMembers,//记录上次选了哪些人
            sureCallBack: function (rsp) {
                store.dispatch('TOGGLEOPENDIALOG')
                conditionPerson[index].usersContext = JSON.parse(JSON.stringify(rsp))
                actions.checkconditionComplete(store, id, conditionPerson)
                store.dispatch('SETCONDITIONPERSONDATA', id, conditionPerson)
            },
            cancelCallBack: function () {
                store.dispatch('TOGGLEOPENDIALOG')
            },
            existingSessionIsNeed: false
        })
    },

    //添加无条件指定关键人
    addPersonSelectList: function (store, personSelectList,global) {
        var selectedMembers = personSelectList || {}
        store.dispatch('TOGGLEOPENDIALOG')
        new LappPersonSelect({
            parentElement: 'body',
            eId: global.eid,
            selectedMembers: selectedMembers,
            sureCallBack: function (rsp) {
                store.dispatch('TOGGLEOPENDIALOG')
                store.dispatch('SETPERSONSELECTLIST', rsp)
            },
            cancelCallBack: function () {
                store.dispatch('TOGGLEOPENDIALOG')
            },
            existingSessionIsNeed: false
        })
    },
    //获取审批人设置数据
    getApprovalRule: function (store) {
        //已选择审批人信息 接口获取详情格式 转 选择审批人格式
        var data2ApprovalPerson = function (arr) {
            var obj = {};
            if (arr.length) {
                arr.forEach(function (val, i) {
                    obj[val.oId] = {
                        userId: val.oId,
                        photoUrl: val.photo,
                        userName: val.name
                    };
                })
            }
            return obj
        }
        //数据处理及初始化过程
        var approvalRule = function (data) {
            console.log(data)
            var approvalRule = data.approvalRule || {},
                template = data.template || {},
                templateName = template.title || '',
                approvalType = approvalRule.approvalType || 0,
                personSelect = approvalRule.content,
                cndSelectId = approvalRule.cndSelectId || '',
                cndContext = approvalRule.cndContext || '',
                components = template.components || [],
                approvalCndGroups = data.approvalCndGroups || [],
                conditionCount = 0,
                cndConData = {},
                conditionPersonData = [],
                label = '',
                isComplete
            personSelect = personSelect ? data2ApprovalPerson(JSON.parse(personSelect)) : {}
            components = components ? JSON.parse(components) : []
            if (cndContext) {
                cndConData = JSON.parse(cndContext)
            }

            components.forEach(function (e, i) {
                if (e.require && (e.type == "radiofield" || e.type == "numberfield" || e.type == "moneyfield")) {
                    conditionCount++
                    if (e.id == cndSelectId) {
                        store.dispatch('SETCONDITIONSELECT', i)
                        label = e.label
                    }
                    if ((e.type == "numberfield" || e.type == "moneyfield") && !cndConData[e.id]) {
                        cndConData[e.id] = [0, 0, 0]
                    }
                }
            })

            //对approvalCndGroups字段做处理，里面的cndContext 和usersContext
            if (approvalCndGroups.length !== 0) {
                approvalCndGroups.forEach(function (e, i) {
                    if (e.usersContext) {
                        e.usersContext = data2ApprovalPerson(e.usersContext)
                    }
                    e.showTitle = actions.cndContext2showTitle(label, e.cnds[0].cndContext)
                    if (e.cnds && e.cnds[0] && e.cnds[0].cndContext) {
                        e.cnds[0].cndContext = JSON.stringify(e.cnds[0].cndContext)
                    }
                    if (!conditionPersonData[e.boundId]) {
                        conditionPersonData[e.boundId] = []
                    }
                    conditionPersonData[e.boundId].push(e)
                })
                for (var personData in conditionPersonData) {
                    store.dispatch('SETCONDITIONPERSONDATA', personData, conditionPersonData[personData])
                    isComplete = actions.checkconditionComplete(store, personData, conditionPersonData[personData])
                    if (cndSelectId && cndSelectId == personData && isComplete) {
                        store.dispatch('SETCONDITIONPERSONSELECT')
                    }
                }
            }

            // if (cndSelectId) {
            //     var isComplete=actions.checkconditionComplete(store, cndSelectId, approvalCndGroups)
            //     if(isComplete){
            //         store.dispatch('SETCONDITIONPERSONSELECT')
            //     }
            // }
            store.dispatch('SETTEMPLATENAME', templateName)
            store.dispatch('SETPERSONRULETYPE', approvalType)
            store.dispatch('SETPERSONSELECTLIST', personSelect)
            store.dispatch('SETCONDITIONLIST', components)
            store.dispatch('SETCONDITIONCOUNT', conditionCount)
            store.dispatch('SETCONDITIONNUMBERDATA', cndConData)
        }


        var formId = helper.getRequest().formId,self=this
        if (!formId) {
            this.$alert({ title: '警告', content: "模板ID错误!" })
        } else {
            var templateId = formId
            store.dispatch('SETTEMPLATEID', templateId)
            http.fetch('approvalRule/detail/' + templateId,{type:'GET'}).then(function (rsp) {
                approvalRule(rsp.body.data)
            },function (rsp) {
                self.$alert({ title: '警告', content: rsp.body.meta.msg })
            })
        }
    },

    //保存审批规则设置
    submitApprovalRule: function (store, data, callback) {

        //已选择审批人信息 选择审批人格式 转 接口获取详情格式
        var approvalPerson2Data = function (obj) {
            var arr = [];
            if (obj) {
                for (var o in obj) {
                    arr.push({
                        oId: obj[o].userId,
                        photo: obj[o].photoUrl,
                        name: obj[o].userName
                    })
                }
            }
            return arr;
        }

        var content, cndSelectId, cndContext, approvalCndGroups = [], personRuleType = data.personRuleType,self=this
        content = JSON.stringify(approvalPerson2Data(data.personSelectList)) || ''
        cndSelectId = data.conditionList[data.conditionSelect] && data.conditionList[data.conditionSelect]['id']
        cndContext = JSON.stringify(data.conditionNumberData) || ''
        approvalCndGroups = []
        for (var approval in data.conditionPersonData) {
            data.conditionPersonData[approval].forEach(function (e, i) {
                var obj = vue.util.extend({}, e)
                obj.boundId = approval //绑定组件id
                obj.number = i
                obj.status = approval == cndSelectId ? 1 : 2//条件组状态    必填 1为启用 2为失效
                delete obj['showTitle']
                obj.usersContext = JSON.stringify(approvalPerson2Data(obj.usersContext))
                approvalCndGroups[approvalCndGroups.length] = obj
            })
        }

        //审批人为空
        if ((data.personRuleType == '1' && !data.conditionComplete[cndSelectId]) || (data.personRuleType == '2' && (!content || content == '[]'))) {
            return new Promise(function (resolve, reject) {
                if (data.isLast) {
                    self.$alert({
                        title: '警告',
                        content: '确定放弃当前审批规则设置,返回审批编辑器'
                    }).then(function (data) {
                        resolve({success: true})
                    },function () {
                        reject()
                    })
                } else {
                    reject({msg: '审批人不能为空，请添加审批人再保存'})
                }
            })
        }

        var dataJson = {
            "approvalRule": {
                "templateId": data.templateId,                 //绑定模板id 必填
                "approvalType": personRuleType,                                      //审批规则类型 0自由流 2无条件审批人 1指定条件审批人
                "content": content,                                            //无条件审批人
                "cndSelectId": cndSelectId,                    //选中条件的对应组件Id 	String 可选
                "cndContext": cndContext                                        //选中条件的表达式     	String 可选 JSON字符串
            },
            "approvalCndGroups": approvalCndGroups
        }
        // console.log(dataJson)
        return http.fetch('approvalRule/save',{
            type:'POST'
            ,data:JSON.stringify(dataJson)
        })
    },

    //按条件审批数字输入检测并排序
    numberConditionSort: function (store, data, type) {
        var msg = "", errArr = [], success, arr = [], obj = {}, err = [], errCode = ['审批条件不能为空', '审批条件必须为数字', '审批条件区间，不能重叠', '审批条件不能为负数']
        data.forEach(function (e, i) {
            e = +e
            if (typeof e !== 'number') {
                err[1] = true
                errArr.push(i)
            } else if (obj[e]) {
                if (e == 0) {
                    err[0] = true
                    errArr.push(i)
                } else {
                    err[2] = true
                    errArr.push(i)
                }
            } else if (e < 0) {
                err[3] = true
                errArr.push(i)
            }
            obj[e] = true
            arr.push(+e)
        })
        err.forEach(function (e, i) {
            e && (msg += errCode[i] + '\n')
        })
        success = err.length == 0
        return {
            success: success,//是否成功
            arr: arr.sort(function (a, b) {
                return a - b;
            }),//排序后的数组
            errArr: errArr,//验证错误的数组下标
            msg: msg//验证错误信息
        }
    },
    //条件区间转义为显示标题内容
    cndContext2showTitle: function (label, cndContext) {
        var str = '', str1 = '', str2 = '', style = 1
        for (var o in cndContext) {
            switch (o) {
                case 'gt':
                    str1 = cndContext[o] + '<'
                    break
                case 'gte':
                    str1 = cndContext[o] + '≤'
                    break
                case 'lt':
                    str2 = '< ' + cndContext[o]
                    break
                case 'lte':
                    str2 = '≤ ' + cndContext[o]
                    break
                case 'eq':
                    str1 = cndContext[o]
                    style = 0
                    break
                default:
                    break
            }
        }
        str = ['当“' + label + '”为<span class="ff-leave-explainr">“' + str1 + '”</span>', '<span class="ff-leave-explainr">当 ' + str1 + '“' + label + '”' + str2 + '</span>'][style]
        return str
    },
    //按条件审批进入选择审批人前数据处理与保存
    conditionPersonSet: function (store, data) {
        var condition = data.conditionList[data.conditionSelect],
            label = condition.label,
            type = condition.type,
            id = condition.id,
            conditionPersonData = data.conditionPersonData,
            conditionData = [],
            options = [],
            obj = {},
            cndObj = {},
            usersContext

        //是否设置过对应的条件审查人数据 返回查找结果
        var checkCondition = function (cndObj) {
            var txt = ''
            if (conditionPersonData && conditionPersonData[id]) {
                conditionPersonData[id].forEach(function (e, i) {
                    if (e.cnds && e.cnds[0] && e.cnds[0].cndContext == cndObj.cndContext && e.cnds[0].uid == cndObj.uid) {
                        txt = e.usersContext || ''
                    }
                })
            }
            return txt
        }

        //条件区间转义为标题文字
        var cndContext2title = function (cndContext) {
            var str = '“' + label + '”'
            for (var o in cndContext) {
                switch (o) {
                    case 'gt':
                        str += '大于' + cndContext[o]
                        break
                    case 'gte':
                        str += '大于等于' + cndContext[o]
                        break
                    case 'lt':
                        str += '小于' + cndContext[o]
                        break
                    case 'lte':
                        str += '小于等于' + cndContext[o]
                        break
                    case 'eq':
                        str += '等于' + cndContext[o]
                        break
                    default:
                        break
                }
            }
            return str
        }

        if (type == "numberfield" || type == "moneyfield") {
            options = data.conditionNumberData[id].slice(0)
            if (options.indexOf(0) === -1) {//数字输入框类型
                options.unshift(0)
            }
            options.forEach(function (e, i) {
                obj = {}, cndObj = {}
                obj.status = 1
                cndObj.uid = id
                cndObj.cndContext = {"gte": e}
                cndObj.status = 1
                if (e == 0) {
                    cndObj.cndContext = options[i + 1] ? {"gt": e, "lt": options[i + 1]} : {"gt": e}
                } else if (options[i + 1]) {
                    cndObj.cndContext = {"gte": e, "lt": options[i + 1]}
                }
                obj.title = cndContext2title(cndObj.cndContext)
                obj.showTitle = actions.cndContext2showTitle(label, cndObj.cndContext)
                cndObj.cndContext = JSON.stringify(cndObj.cndContext)
                obj.cnds = [cndObj]
                obj.usersContext = checkCondition(cndObj)
                conditionData.push(obj)
            })
        } else {
            options = condition.options
            options.forEach(function (e, i) {
                obj = {}, cndObj = {}
                obj.status = 1
                cndObj.uid = id
                cndObj.cndContext = {"eq": e}
                cndObj.status = 1
                obj.title = cndContext2title(cndObj.cndContext)
                obj.showTitle = actions.cndContext2showTitle(label, cndObj.cndContext)
                cndObj.cndContext = JSON.stringify(cndObj.cndContext)
                obj.cnds = [cndObj]
                obj.usersContext = checkCondition(cndObj)
                conditionData.push(obj)
            })
        }
        actions.checkconditionComplete(store, id, conditionData)
        store.dispatch('SETCONDITIONPERSONDATA', id, conditionData)
        store.dispatch('SETCONDITIONPERSONSELECT')
    },
    setDesiginConfirm:function (store,data) {
        store.dispatch('SETDESIGINSUBMIT', data)
    }
}
module.exports = actions
