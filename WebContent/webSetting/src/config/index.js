var ENV = {
    routeRootPath: '/freeflow/web/manager',
    apiNameSpace: 'v1/rest',
    serverRootUrl: '/',
    hostEndpoint: 'freeflow',
    environment: process.env.NODE_ENV,
}
    ENV.baseURL = `${location.origin}/${ENV.hostEndpoint}/${ENV.apiNameSpace}`

export default ENV
