/**
 * Created by yuce_wang on 2016/8/23.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import config from './config'

import {
    Root,
    Design,
    Setting
} from './pages'

import {getRequest, addRequest} from './utils/helpers'

//本地开发环境下为每个请求添加webLappToken进行通信
if (process.env.NODE_ENV === 'develop') {
    const query = getRequest()
    if (query.lappName && query.webLappToken) {
        const authInfo = {
            lappName: query.lappName,
            webLappToken: query.webLappToken
        }
        localStorage.setItem('auth', JSON.stringify(authInfo))
    }
    Vue.config.debug = true
}

Vue.use(VueRouter)

const router = new VueRouter({
    history: true,
    root: config.routeRootPath
})

router.map({
    '/formdesign': {
        name: 'formdesign',
        component: Design
    },
    '/approvalrule': {
        name: 'approvalrule',
        component: Setting
    }
})

//root路由重定向
router.redirect({
    '/': '/formdesign'
})
router.beforeEach(function (transition) {
    var toQuery = transition.to.query
    const fromQuery = transition.from.query
    if (!toQuery.lappName||!toQuery.webLappToken) {
        if (fromQuery&&fromQuery.lappName && fromQuery.webLappToken) {
            transition.redirect(addRequest(transition.to.path, fromQuery))
            return
        }
    }
    transition.next()
})
router.start(Root, '#container')
