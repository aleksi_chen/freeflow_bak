import Designer from './designer'
import FormArea from './form-area'
import webUtil  from './web-util'
import $ from '../lib/jquery-2.1.4.min'

var controller = {
    data: {

        getFormUrl: '/freeflow/v1/rest/form-template/detail/formTemplate',	// 获取表单设置控件url
        // saveUrl: '/freeflow/v1/rest/form-template/create/formTemplate.json?' ,
        createApproval: '/freeflow/v1/rest/form-template/create/formTemplate',//保存模板
        saveUrl: '/freeflow/v1/rest/form-template/save/recordTemplate',
        reviseUrl: '/freeflow/v1/rest/form-template/update/formTemplate',
        formArea: null,
        selectApprover: null,
        approverData: {},
        addKeyApproverArr: [],//指定审批人信息
        addApproverArr: [],//指定审批人信息
        curApprType: 0  // 0为不指定审批人， 1为审批人， 2为普通审批人
    },

    init: function () {
        var me = this;
        this.initFormArea();	// 初始化控件拖放
        this.bindUI();			// 绑定事件
        this.setAuthToken();    //为每个请求的url添加授权token

        webUtil.tab('.fd-widgetpanel', 'curr');	// tab切换

        var formId = webUtil.getUrlValue('formId');

        //编辑将之前选择模板生成
        if (formId) {
            this.getFormDataPost(formId).done(function (resp) {
                // console.log(resp)
                var title = resp.data.template.title;
                var status = resp.data.template.status;
                // $('.fd-title').text(title);
                // console.log(resp.data)
                if (resp.data.template.components) {
                    me.renderForm(resp.data, status, title);
                } else {//空白模板
                    me.renderFormWithTitle(status, title);
                }
            });
        }
    },
    destroyed: function () {
        // $('body').off()
        this.data.formArea.destroy()
    },
    addArea: function (contentArr, title) {
        var arr = contentArr || []
        arr.unshift({
            "id": "namefield",
            "type": "namefield",
            "label": title || "审批名称",
            "require": true
        })
        return arr
    },
    renderForm: function (data, status, title) {
        //第一步内容加载
        var content = JSON.parse(data.template.components);
        var selectId = data.approvalRule.cndSelectId
        if (selectId && data.approvalRule.approvalType == '1') {//添加作为审批条件的控件选中的selected表示
            content.forEach(function (e) {
                if (e.id === selectId) {
                    e.selected = true
                }
            });
        }
        content = this.addArea(content, title)
        this.data.formArea.renderForm(content, status);
    },
    renderFormWithTitle: function (status, title) {
        var content = [{
            "id": "titlefield-" + webUtil.uuid(),
            "type": "titlefield",
            "label": "标题",
            "placeholder": "请输入",
            "require": true
        }];
        content = this.addArea(content, title)
        this.data.formArea.renderForm(content, status);
    },
    // 初始化控件拖放区域
    initFormArea: function () {
        var formArea = new FormArea({
            'target': '.fd-designer',
            'className': 'fd-designer-inner',
            'dragTarget': '.fd-widgetitem'	// 拖动元素的选择器
        })
            .on('dragStart', this._onDragStart.bind(this))
            .on('draging', this._onDraging.bind(this))
            .on('dragEnd', this._onDragEnd.bind(this))
            // 选择控件的监听
            .on('selectComp', this._onSelectComp.bind(this))
            // 删除控件的监听
            .on('delComp', this._onDelComp.bind(this))
            .on('changeParentComponent', this._checkDetail.bind(this))

        this.data.formArea = formArea;
    },

    _onSelectComp: function (component) {
        var props = component.props;
        var settingTpl = Designer.getSettingTpl(props);

        $('.fd-settingpanel .fd-panel-body').html(settingTpl);
        // 校验数据
        this.validate();
    },
    _checkDetail: function (parentComponent) {
        if (parentComponent.length == 1) {
            var components = parentComponent.find('.fd-component'),
                numbercomponents = parentComponent.find('.fd-component-numberfield'),
                moneycomponents = parentComponent.find('.fd-component-moneyfield'),
                addTip = parentComponent.find('.fd-detail-none'),
                numberTip = parentComponent.find('.number-tip'),
                moneyTip = parentComponent.find('.money-tip')

            components.length< 2 ? addTip.show() : addTip.hide()
            numbercomponents.length >0 ? numberTip.show() : numberTip.hide()
            moneycomponents.length >0 ? moneyTip.show() : moneyTip.hide()
        }
    },
    _onDelComp: function (component) {
        if (component.isActive) {
            $('.fd-settingpanel .fd-panel-body').html('');
        }
    },

    _onDragStart: function (e, component) {
        var type = component.props ? component.props.type : $(component.srcElem).data('type');
        $('html').addClass('fd-cursor-move');
        this.showDraingMask(e, type);
    },

    _onDraging: function (e, component) {
        var $mask = $('.fd-draging-mask'),
            mtouch = e.mTouchEvent;

        var translateStr = 'translate3d(' + mtouch.moveX + 'px,' + mtouch.moveY + 'px,0)'

        $mask.css({
            'transform': translateStr,
            '-webkit-transform': translateStr,
            '-moz-transform': translateStr,
            '-o-transform': translateStr,
            '-ms-transform': translateStr
        });
    },

    _onDragEnd: function (e) {
        $('html').removeClass('fd-cursor-move');
        this.hideDraginMask();
        $('.fd-component-detailfield').each(function (i, e) {

        })

    },

    // 绑定事件
    bindUI: function () {
        var me = this;
        // $('.back-manager').off('click').on('click', function () {
        //     window.location.href = "/freeflow/web/approval-manage.json?" + urlParam;
        // })
        // 标题输入
        $('body').on('input', '.fd-setting-label input', function (e) {
                me._onLabelChange('label', this.value, 20);
            })
            // 提示语输入
            .on('input', '.fd-setting-placeholder input', function (e) {
                me._onLabelChange('placeholder', this.value, 40);
            })
            // 时间区间的标题
            .on('input', '.fd-daterange-input1, .fd-daterange-input2', function () {
                var index = $(this).hasClass('fd-daterange-input1') ? 0 : 1;
                me._onDrLabelChange(index, this.value);
            })
            // 单选框选项
            .on('input', '.fd-setting-options input', function () {
                var index = $('.fd-setting-options input').index(this);
                me._onOptionsChange(index, this.value, 40);
            })
            // 添加选项
            .on('click', '.fd-option-add', function (e) {
                me._optionItemHandler(e, 'add');
            })
            // 删除选项
            .on('click', '.fd-option-del', function (e) {
                me._optionItemHandler(e, 'del');
            })
            // 日期格式
            .on('change', 'input[name="dateformat"]', function () {
                var component = me.getCurrComponent();	// 当前选中控件和属性

                component.props.format = this.value;
            })
            // require是否必填  decimal是否允许小数
            .on('change', '.fd-setting-require input', function () {
                var component = me.getCurrComponent();	// 当前选中控件和属性
                var $placeholder = component.$comp.find('.fd-component-placeholder');
                var oldVal = $placeholder.html();
                var name = this.className
                var obj = {
                    require: {
                        title: '(必填)',
                        regex: /\(必填\)(?=\(允许小数\)|$)/g
                    },
                    decimal: {
                        title: '(允许小数)',
                        regex: /\(允许小数\)(?=\(必填\)|$)/g
                    },
                }
                component.props[name] = this.checked;
                $placeholder.html(this.checked ? oldVal + obj[name].title : oldVal.replace(obj[name].regex, ''));
            })
        //下一步
        // .on('click', '.fd-btn-next', function (e) {
        //     // me._nextPage(e)
        //     me._save(e)
        // })
    },


    // 保存操作
    _save: function (e) {
        var deferred = $.Deferred();
        var me = this;
        var formArea = this.data.formArea;


        var allComponents = formArea.getAllComponents();

        var componentsArr = [], title
        for (var i = 0, len = allComponents.length; i < len; i++) {
            var component = allComponents[i];
            var errors = $(component.srcElem).data('errors');
            if (errors && errors.length) {
                // 选中有错的控件
                formArea.selectComp(component.srcElem);
                return deferred.reject()
            }
            if (component.props.type !== 'namefield') {
                componentsArr.push(component)
            } else {
                title = component.props.label
            }
        }

        if (!componentsArr.length) {
            $('.fd-widgetpanel [data-panel="1"]').click();
            webUtil.tips('请先添加表单控件');
            return deferred.reject();
        }

        // 提交数据
        var data = this._getData(componentsArr);
        var formId = webUtil.getRequestKey().formId,
        // common = webUtil.getRequestKey().common,
            params;
        //保存
        if (!title) {
            webUtil.tips('模板名称不能为空');
            return deferred.reject()
        }
        if (formId) {
            params = {
                template: {
                    id: formId,
                    appName: "freeflow",//应用名称 必传
                    title: title,//标题
                    components: JSON.stringify(data.components)
                }
            }
        } else {
            webUtil.tips('模板ID为空');
            return deferred.reject()
        }
        return this._createFormPost(params);
    },

    // 获取数据
    _getData: function (allComponents) {
        // var title = $('.fd-form-name').val().trim();
        // var needApprover = $('input[name="need-approver"]:checked').val();

        var components = [],
            $elem,
            subProps,
            self=this
        allComponents.forEach(function (item) {
            delete item.props.selected
            $elem=$(item.srcElem)
            if(item.props.subComponent){
                item.props.subComponent=[]
                $elem.find('.fd-component').each(function (i,e) {
                    subProps=self.data.formArea.getComponentProps($(e).data('id'))
                    subProps.parentId=item.props.id
                    item.props.subComponent.push(subProps)
                })
            }
            if($elem.parents('.fd-component').length==0){
                components.push(item.props);
            }

        });
        return {
            // title: title,
            // approverInfo: this.data.approverData,
            components: components
        };
    },

    // 创建表单配置
    _createFormPost: function (data) {
        var deferred = $.Deferred();
        var formId = webUtil.getUrlValue('formId');
        // var common = webUtil.getUrlValue('common');
        var url = this.data.createApproval;
        // if(formId && !common){
        // 	url = this.data.reviseUrl;
        // }else{
        // 	url = this.data.saveUrl;
        // }
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            timeout: 3000
        }).then(function (resp) {
            // console.log(resp, data);
            if (resp&&resp.meta&&resp.meta.success === true) {
                deferred.resolve(resp);
                webUtil.tips('保存成功');
                // window.location.href = '/freeflow/web/approval-rule.json?detail=' + formId + '&' + urlParam
            } else {
                deferred.reject(resp.errorMsg || '保存失败了');
                webUtil.tips(resp.meta.msg);
            }
        }, function (resp) {
            deferred.reject('保存失败了');
        });

        return deferred.promise();
    },
    // 输入内容改变监听
    _onLabelChange: function (type, value, valLengh) {
        var component = this.getCurrComponent();	// 当前选中控件和属性

        var placeholder = type == 'placeholder' && component.props.require ? '(必填)' : ''
        placeholder += type == 'placeholder' && component.props.decimal ? '(允许小数)' : '';

        component.props[type] = value;
        component.$comp.find('.fd-component-' + type)
            .html(webUtil.getByteVal(value, valLengh) + placeholder);

        var errors = component.$comp.data('errors') || [];

        this.validate(type).done(function () {
            if (errors.indexOf(type) > -1) {
                //重置状态
                var $elem = $('.fd-settings').find('.fd-setting-' + type);
                $elem.find('input').removeClass('error');

                var $itemInfo = $elem.find('.fd-setting-iteminfo');
                $itemInfo.removeClass('error').html($itemInfo.data('tips'));

                errors.splice(errors.indexOf(type), 1);
                component.$comp.data('errors', errors);
            }
        }).fail(function () {
            if (errors.indexOf(type) == -1) {
                errors.push(type);
                component.$comp.data('errors', errors);
            }
        });
    },
    // 输入内容改变监听
    _onOptionsChange: function (index, value, valLengh) {
        var component = this.getCurrComponent();	// 当前选中控件和属性
        component.props.options[index] = value;
        // webUtil.getByteVal(value, valLengh)
        var type = 'options' + index
        var errors = component.$comp.data('errors') || [];

        this.validate('options', index).done(function () {
            if (errors.indexOf(type) > -1) {
                //重置状态
                var $elem = $('.fd-setting-options').find('.fd-setting-itembody:eq(' + index + ')')
                $elem.find('input').removeClass('error');
                errors.splice(errors.indexOf(type), 1);
                component.$comp.data('errors', errors);
            }
        }).fail(function () {
            if (errors.indexOf(type) == -1) {
                errors.push(type);
                component.$comp.data('errors', errors);
            }
        });
    },
    // 时间区间标题改变
    _onDrLabelChange: function (index, value) {
        var component = this.getCurrComponent();	// 当前选中控件和属性
        var checkerType = index === 0 ? 'starttime' : 'endtime';

        component.props.labels[index] = value;
        component.$comp.find('.fd-component-label')
            .eq(index)
            .html(webUtil.getByteVal(value, 20));

        var errors = component.$comp.data('errors') || [];

        this.validate(checkerType).done(function () {
            if (errors.indexOf(checkerType) > -1) {
                //重置状态
                var $daterangeLabel = $('.fd-setting-daterangelabel');
                $daterangeLabel.find('input').eq(index).removeClass('error');

                var $itemInfo = $daterangeLabel.find('.fd-setting-iteminfo').eq(index)
                $itemInfo.removeClass('error').html($itemInfo.data('tips'));

                errors.splice(errors.indexOf(checkerType), 1);
                component.$comp.data('errors', errors);
            }
        }).fail(function () {
            if (errors.indexOf(checkerType) == -1) {
                errors.push(checkerType);
                component.$comp.data('errors', errors);
            }
        });
    },

    // 新增或删除选项
    _optionItemHandler: function (e, type) {
        var component = this.getCurrComponent();	// 当前选中控件和属性
        var options = component.props.options;
        var isAdd = type == 'add';
        var $target = $(e.target);
        var $settingOptions = $target.closest('.fd-setting-options');

        if (isAdd) {
            if (options.length >= 50) {
                return;
            }

            if (options.length == 49) {
                $settingOptions.addClass('nomore-add');
            }
            $settingOptions.removeClass('limit-del');
        } else {
            if (options.length <= 1) {
                return;
            }

            if (options.length == 2) {
                $settingOptions.addClass('limit-del');
            }
            $settingOptions.removeClass('nomore-add');
        }

        var $itembody = $target.closest('.fd-setting-itembody');
        var index = $itembody.index();

        // 添加选项
        if (isAdd) {
            // 确定新的选项名字
            for (var i = 1, len = options.length; i <= len; i++) {
                if (options.indexOf('选项' + i) == -1) {
                    break;
                }
            }
            var newOption = '选项' + i;

            var $newItemBody = $itembody.clone();
            $newItemBody.find('input').val(newOption);

            // 插入新的选项
            options.splice(index + 1, 0, newOption);
            $itembody.after($newItemBody);
        } else {
            options.splice(index, 1);
            $itembody.remove();
        }
    },

    // 数据校验
    validate: function (checkerType, args) {
        var component = this.getCurrComponent();
        var type = component.props.type;
        var promise = Designer[type].validator.handler(component, checkerType, args);

        return promise;
    },

    getCurrComponent: function () {
        var currComponent = this.data.formArea.get('currComponent');

        if (!currComponent) {
            return;
        }

        return {
            props: currComponent.props,
            $comp: $(currComponent.srcElem)
        }
    },

    // 显示拖动的朦层
    showDraingMask: function (e, type) {
        var $mask = $('.fd-draging-mask');

        $mask.html($('.fd-widgetitem[data-type=' + type + ']').html());

        var width = $mask.outerWidth(),
            height = $mask.outerHeight();

        $mask.css({
            'left': (e.mTouchEvent.startX - width / 2) + 'px',
            'top': (e.mTouchEvent.startY - height / 2) + 'px'
        }).show();
    },

    // 隐藏拖动的朦层
    hideDraginMask: function () {
        $('.fd-draging-mask').hide();
    },

    // 获取表单数据
    getFormDataPost: function (formId) {
        var deferred = $.Deferred();
        $.ajax({
                'url': this.data.getFormUrl + '/' + formId,
                'type': 'GET',
                'contentType': 'application/json;charset=UTF-8'
            })
            .always(function (resp) {

                if (resp &&resp.meta &&resp.meta.success === true) {
                    deferred.resolve(resp);
                } else {
                    deferred.reject(resp);
                }
            });

        return deferred.promise();
    },
    setAuthToken: function () {
        //全局请求设置-->为每个请求带上授权token
        //获取url query
        var req = webUtil.getRequest();
        if (req.lappName && req.webLappToken) {
            $.ajaxSetup({
                beforeSend: function (e, data) {
                    data.url += '?lappName=' + req.lappName + '&webLappToken=' + req.webLappToken;
                    return true;
                }
            });
            $(document).ajaxSuccess(function(event, xhr, settings) {
                if(typeof xhr.responseText==='string'&&(/^.+html(.|\n)+>$/g).test(xhr.responseText)){
                    document.write(xhr.responseText)
                }
            });
        }
    }
};

export default controller
