import _ from '../lib/underscore-min'
import $ from '../lib/jquery-2.1.4.min'
import webUtil  from './web-util'
// 获取控件属性
var getProps = function (type, label, placeholder, otherProps) {
    var obj = {
        'id': '',
        'type': type || '',
        'label': label || '',
        'placeholder': placeholder || '',
        'require': false
    };

    if (otherProps) {
        $.extend(obj, otherProps);
    }
    return obj;
};

// 生成每个控件的默认属性
var textfieldProps = getProps('textfield', '单行输入框', '请输入'),
    textareafieldProps = getProps('textareafield', '多行输入框', '请输入'),
    numberfieldProps = getProps('numberfield', '数字输入框', '请输入', {'decimal': false}),
    datefieldProps = getProps('datefield', '日期', '请选择', {'format': 'yyyy/MM/dd'}),
    photofieldProps = getProps('photofield', '图片', '请选择'),
    filefieldProps = getProps('filefield', '文件', '请选择'),
    radiofieldProps = getProps('radiofield', '单选框', '请选择', {
        'options': ['选项1', '选项2', '选项3']
    }),
    daterangefieldProps = getProps('daterangefield', '日期区间', '请选择', {
        'labels': ['开始日期', '结束日期'],
        'format': 'yyyy/MM/dd'
    }),
    moneyfieldProps = getProps('moneyfield', '金额(元)', '请输入'), //金额
    titlefieldProps = getProps('titlefield', '审批', '请输入'), //标题类型
    namefieldProps = getProps('namefield', '审批名称', '请输入审批名称'), //审批名称类型
    detailfieldProps=getProps('detailfield', '明细','', {'subComponent': []})
// 校验器的校验规则
var rules = {
    'notEmpty': function (val) {
        return /[^\s]+/g.test(val);
    },
    'nomoreTen': function (val) {
        return webUtil.getByteLength(val) <= 20;
    },
    'nomoreTwenty': function (val) {
        return webUtil.getByteLength(val) <= 40;
    }
};

var labelChecker = {
    'type': 'label',
    'rules': ['notEmpty', 'nomoreTen'],
    'tips': ['标题不能为空'],
    'value': 'label'
};

var placeholderChecker = {
    'type': 'placeholder',
    'rules': ['nomoreTwenty'],
    'value': 'placeholder'
};

var optionsChecker = {
    'type': 'options',
    'rules': ['notEmpty', 'nomoreTwenty'],
    'value': 'options'
}

var drfieldStartChecker = {
    'type': 'starttime',
    'rules': ['notEmpty', 'nomoreTen'],
    'tips': ['标题不能为空'],
    'value': 'labels'
};

var drfieldEndChecker = {
    'type': 'endtime',
    'rules': ['notEmpty', 'nomoreTen'],
    'tips': ['标题不能为空'],
    'value': 'labels'
};

// 控件集合
var designer = {
    'textfield': {
        props: textfieldProps,
        validator: {
            checkers: [labelChecker, placeholderChecker],
            handler: getHandler(errorHandler)
        }
    },

    'textareafield': {
        props: textareafieldProps,
        validator: {
            checkers: [labelChecker, placeholderChecker],
            handler: getHandler(errorHandler)
        }
    },

    'numberfield': {
        props: numberfieldProps,
        validator: {
            checkers: [labelChecker, placeholderChecker],
            handler: getHandler(errorHandler)
        }
    },

    'radiofield': {
        props: radiofieldProps,
        validator: {
            checkers: [labelChecker, optionsChecker],
            handler: getHandler(optionsErrorHandler)
        }
    },

    'datefield': {
        props: datefieldProps,
        validator: {
            checkers: [labelChecker],
            handler: getHandler(errorHandler)
        }
    },

    'daterangefield': {
        props: daterangefieldProps,
        validator: {
            checkers: [drfieldStartChecker, drfieldEndChecker],
            handler: getHandler(drfieldErrHandler)
        }
    },

    'photofield': {
        props: photofieldProps,
        getWidgetTpl: getWidgetTpl,
        validator: {
            checkers: [labelChecker],
            handler: getHandler(errorHandler)
        }
    },

    'filefield': {
        props: filefieldProps,
        getWidgetTpl: getWidgetTpl,
        validator: {
            checkers: [labelChecker],
            handler: getHandler(errorHandler)
        }
    },

    'moneyfield': {
        props: moneyfieldProps,
        validator: {
            checkers: [labelChecker, placeholderChecker],
            handler: getHandler(errorHandler)
        }
    },

    'titlefield': {
        props: titlefieldProps,
        validator: {
            checkers: [labelChecker, placeholderChecker],
            handler: getHandler(errorHandler)
        }
    },
    'namefield': {
        props: namefieldProps,
        validator: {
            checkers: [labelChecker],
            handler: getHandler(errorHandler)
        }
    },
    'detailfield':{
        props: detailfieldProps,
        validator: {
            checkers: [labelChecker],
            handler: getHandler(errorHandler)
        }
    },
    // 控件模板
    'getWidgetTpl': getWidgetTpl,

    // 设置模板
    'getSettingTpl': getSettingTpl
};

// 获取校验器处理函数
function getHandler(errorHandler) {
    return function (compProps, checkerType, args) {
        var deferred = $.Deferred();
        var checkers = this.checkers;
        var checker, result;
        // 遍历校验器

        for (var i = 0, len = checkers.length; i < len; i++) {
            checker = checkers[i];
            if (!checkerType || (checkerType && checkerType == checker.type)) {
                // 遍历校验规则
                for (var j = 0, l = checker.rules.length; j < l; j++) {
                    result = errorHandler(j, compProps, checker, args);
                    if (result === false) {
                        deferred.reject();
                        break
                    }
                }
            }
        }

        deferred.resolve();
        return deferred.promise();
    };
}

// 通用的错误处理
function errorHandler(index, compProps, checker) {
    if (!rules[checker.rules[index]].call(null, compProps.props[checker.value])) {
        var $elem = $('.fd-settings').find('.fd-setting-' + checker.type);
        $elem.find('input').addClass('error');
        $elem.find('.fd-setting-iteminfo')
            .addClass('error')
            .html(checker.tips ? checker.tips[index] : undefined);

        return false;
    }

    return true;
}

// 单选的错误处理
function optionsErrorHandler(index, compProps, checker, args) {
    var disComplete = false, $elem, arr = []

    if (checker.type === 'options') {
        if (args !== undefined) {
            disComplete = !rules[checker.rules[index]].call(null, compProps.props[checker.value][args])
            $elem = $('.fd-setting-options').find('.fd-setting-itembody:eq(' + args + ')')
        } else {
            compProps.props[checker.value].forEach(function (e, i) {
                if (!rules[checker.rules[index]].call(null, e)) {
                    arr.push($('.fd-setting-options').find('.fd-setting-itembody').get(i))
                    disComplete = true
                }
            })
            $elem = $(arr)
        }
    } else {
        disComplete = !rules[checker.rules[index]].call(null, compProps.props[checker.value])
        $elem = $('.fd-settings').find('.fd-setting-' + checker.type)
    }
    if (disComplete) {
        $elem.find('input').addClass('error');
        $elem.find('.fd-setting-iteminfo')
            .addClass('error')
            .html(checker.tips ? checker.tips[index] : undefined);

        return false;
    }

    return true;
}

// 时间区间的错误处理
function drfieldErrHandler(index, compProps, checker) {
    var labelIndex = checker.type == 'starttime' ? 0 : 1;

    if (!rules[checker.rules[index]].call(null, compProps.props.labels[labelIndex])) {
        var $daterangeLabel = $('.fd-setting-daterangelabel');
        $daterangeLabel.find('input').eq(labelIndex).addClass('error');
        $daterangeLabel.find('.fd-setting-iteminfo')
            .eq(labelIndex)
            .addClass('error')
            .html(checker.tips ? checker.tips[index] : undefined);

        return false;
    }

    return true;
}

// 控件基础html模板
function getWidgetTpl(data) {
    // 是否需要“进入”右箭头
    var needEnterIcon = {
        'radiofield': 1,
        'datefield': 1,
        'daterangefield': 1,
        'photofield': 1,
        'filefield': 1
    };

    data = $.extend({needEnterIcon: needEnterIcon, 'decimal': false}, data);
    var baseTpl = [
        '<% if (type == "namefield" ) { %>', //标题类型
        '<div class="disdrag-box">',
        '<div class="fd-component disdrag  fd-component-<%= type %>">',
        '<div class="fd-component-overlayer"></div>',
        '<div class="fd-component-group fd-title">',
        '<label class="fd-component-<%= type %>-label fd-component-label"><%= label %></label>',
        '</div>',
        '</div>',
        '</div>',
        '<% } else  if (type == "titlefield" ) { %>',
        '<div class="disdrag-box">',
        '<div class="fd-component disdrag fd-component-<%= type %>">',
        '<div class="fd-component-overlayer"></div>',
        '<div class="fd-component-group">',
        '<label class="fd-component-label">标题</label>',
        '<span class="fd-component-placeholder ellipsis<%= needEnterIcon[type] ? " blue" : "" %>">',
        '<%= placeholder + (require ? "(必填)" : "") %>',
        '</span>',
        '</div>',
        '</div>',
        '</div>',
        '<% } else  if (type == "detailfield" ) { %>',
        //明细组件
        '<div class="fd-component fd-component-detailfield">',
        '<span class="fd-component-remove"></span>',
        '<div class="fd-component-header">',
        '<label class="fd-component-label">明细</label>',
        '<span class="fd-copy-detail fr"></span></div>',
        '<div class="fd-detail-container"></div>',
        '<div class="fd-detail-none">可以拖入多个非明细控件</div>',
        '<div class="fd-detail-value">',
        '<div class="number-tip">总数字输入框：0</div>',
        '<div class="money-tip">总金额（元）：0</div>',
        '<div class="money-tip">大写：壹万元整（示例）</div>',
        '</div>',
        '</div>',
        '<% } else { %>',
        '<div class="fd-component fd-component-<%= type %>">',
        '<span class="fd-component-remove"></span>',
        '<div class="fd-component-overlayer"></div>',
        // 日期区间类型
        '<% if (type == "daterangefield") { %>',
        '<div class="fd-component-group">',
        '<label class="fd-component-label"><%= labels[0] %></label>',
        '<span class="fd-component-placeholder ellipsis blue">',
        '<%= placeholder + (require ? "(必填)" : "") %>',
        '</span>',
        '<i class="fd-component-entericon"></i>',
        '</div>',
        '<div class="fd-component-group">',
        '<label class="fd-component-label"><%= labels[1] %></label>',
        '<span class="fd-component-placeholder ellipsis blue">',
        '<%= placeholder + (require ? "(必填)" : "") %>',
        '</span>',
        '<i class="fd-component-entericon"></i>',
        '</div>',

        // 非日期期间类型
        '<% } else { %>',
        '<div class="fd-component-group">',
        '<label class="fd-component-label"><%= label %></label>',
        '<span class="fd-component-placeholder ellipsis<%= needEnterIcon[type] ? " blue" : "" %>">',
        '<%= placeholder + (require ? "(必填)" : "")+(decimal ? "(允许小数)" : "") %>',
        '</span>',
        '<% if (needEnterIcon[type]) { %>',
        '<i class="fd-component-entericon"></i>',
        '<% } %>',
        '</div>',
        '<% } %>',
        '</div>',
        '<% } %>'
    ].join('');

    return _.template(baseTpl)(data);
}

// 设置区域的html模板
function getSettingTpl(data) {
    // 需要提示语的类型
    var needPlaceholderType = {
        'textfield': 1,
        'textareafield': 1,
        'numberfield': 1,
        'moneyfield': 1,
        'titlefield': 1,
    };

    //需要必填的类型
    var needRequireType = {
        'textfield': 1,
        'textareafield': 1,
        'numberfield': 1,
        'moneyfield': 1,
        'radiofield': 1,
        'datefield': 1,
        'daterangefield': 1,
        'filefield': 1,
        'photofield': 1
    }

    data = $.extend({
        'needPlaceholderType': needPlaceholderType,
        'needRequireType': needRequireType,
        'selected': false,
        'decimal': false
    }, data);
    // console.log(data)
    var settingTpl = [
        '<div class="fd-settings">',
        '<% if (type !== "titlefield") { %>', //非titlefield
        '<%  if (type === "namefield") { %>', //非日期区间设置项的标题
        '<div class="fd-setting-item fd-setting-label">',
        '<div class="fd-setting-itemtitle">',
        '<span>审批名称</span>',
        '<span class="fd-setting-iteminfo" data-tips="最多10个字">最多10个字</span>',
        '</div>',
        '<div class="fd-setting-itembody">',
        '<input type="text" value="<%= label %>" placeholder="请输入审批名称"/>',
        '</div>',
        '</div>',
        '<% } else  if (type !== "daterangefield") { %>', //非日期区间设置项的标题
        '<div class="fd-setting-item fd-setting-label">',
        '<div class="fd-setting-itemtitle">',
        '<span>标题</span>',
        '<span class="fd-setting-iteminfo" data-tips="最多10个字">最多10个字</span>',
        '</div>',
        '<div class="fd-setting-itembody">',
        '<input type="text" value="<%= label %>"/>',
        '</div>',
        '</div>',
        '<% } else{ %>',	//日期区间
        '<div class="fd-setting-item fd-setting-daterangelabel">',
        '<div class="fd-setting-itemtitle">',
        '<span>标题1</span>',
        '<span class="fd-setting-iteminfo" data-tips="最多10个字">最多10个字</span>',
        '</div>',
        '<div class="fd-setting-itembody">',
        '<input type="text" class="fd-daterange-input1" value="<%= labels[0] %>"/>',
        '</div>',
        '<div class="fd-setting-itemtitle">',
        '<span>标题2</span>',
        '<span class="fd-setting-iteminfo" data-tips="最多10个字">最多10个字</span>',
        '</div>',
        '<div class="fd-setting-itembody">',
        '<input type="text" class="fd-daterange-input2" value="<%= labels[1] %>"/>',
        '</div>',
        '</div>',
        '<% } %>',
        '<% } %>',
        // 需要配置提示语
        '<% if (needPlaceholderType[type]) { %>',
        '<div class="fd-setting-item fd-setting-placeholder">',
        '<div class="fd-setting-itemtitle">',
        '<span>提示语</span>',
        '<span class="fd-setting-iteminfo" data-tips="最多20个字">最多20个字</span>',
        '</div>',
        '<div class="fd-setting-itembody">',
        '<input type="text" value="<%= placeholder %>"/>',
        '</div>',
        '</div>',
        '<% } %>',

        // 日期类型，需要选择日期格式
        '<% if (type == "datefield" || type == "daterangefield") { %>',
        '<div class="fd-setting-item fd-setting-dateformat">',
        '<div class="fd-setting-itemtitle">',
        '<span>日期类型</span>',
        '<span class="fd-setting-iteminfo"></span>',
        '</div>',
        '<label class="fd-setting-itembody">',
        '<input type="radio" name="dateformat" value="yyyy/MM/dd" <%= format == "yyyy/MM/dd" ? "checked" : "" %>/>',
        '<span>年/月/日</span>',
        '</label>',
        /*'<label class="fd-setting-itembody">',
         '<input type="radio" name="dateformat" value="yyyy/MM" <%= format == "yyyy/MM" ? "checked" : "" %>/>',
         '<span>月/日</span>',
         '</label>',*/
        '</div>',
        '<% } %>',

        // 单选框，提供选项
        '<% if (type == "radiofield") { %>',
        '<div class="fd-setting-item fd-setting-options">',
        '<div class="fd-setting-itemtitle">',
        '<span>选项</span>',
        '<span class="fd-setting-iteminfo" data-tips="最多50项，每项最多20个字">最多50项，每项最多20个字</span>',
        '</div>',
        '<div class="">',
        '<% _.each(options, function (option) { %>',
        '<div class="fd-setting-itembody">',
        '<input type="text" value="<%= option %>" />',
        '<span class="fd-option-del">&minus;</span>',
        '<span class="fd-option-add">+</span>',
        '</div>',
        '<% }); %>',
        '</div>',
        '</div>',
        '<% } %>',
        // 设置项是否必填部分
        '<% if ( needRequireType[type]) { %>', //需要必填类型
        '<div class="fd-setting-item fd-setting-require">',
        '<% if( type == "moneyfield" ) {%>',  //金额类型
        '<div class="fd-setting-labelinfo">默认允许小数后两位,并且不能为负数</div>',
        '<% } else if( type == "numberfield" ) {%>',  //数字类型
        '<div>',
        '<label class="fd-setting-itembody">',
        '<input type="checkbox" value="1" class="decimal" <%= decimal === true ? "checked" : "" %>/>',
        '<span>允许为小数</span>',
        '</label>',
        '</div>',
        '<% } %>',
        '<label class="fd-setting-itembody">',
        '<% if(selected==true) { %>',  //金额类型
        '<input type="checkbox" value="1"  class="require" disabled <%= require === true ? "checked" : "" %>/>',
        '<span class="fd-setting-labelinfo">必填</span><span class="fd-setting-extrainfo">(已被设置为审批条件,不可修改勾选)</span>',
        '<% } else{ %>',  //数字类型
        '<input type="checkbox" value="1"  class="require" <%= require === true ? "checked" : "" %>/>',
        '<span class="fd-setting-labelinfo">必填</span>',
        '<% } %>',
        '</label>',
        '</div>',
        '<% } %>',
        '</div>'
    ].join('');

    return _.template(settingTpl)(data);
};
export default designer;
