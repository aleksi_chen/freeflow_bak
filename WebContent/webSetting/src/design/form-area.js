import Designer from './designer'
import Widget from './widget'
import $ from '../lib/jquery-2.1.4.min'
import webUtil  from './web-util'
import _ from '../lib/underscore-min'
import mTouch from '../lib/mTouch'

function FormArea(cfg) {
    Widget.call(this);
    // var formArea=new Widget()
    return this.render(cfg);
}

FormArea.prototype = $.extend({}, Widget.prototype, {
    constructor: FormArea,
    initCfg: function (cfg) {
        this.cfg = $.extend({
            'dragTarget': '', // 拖动元素的选择器
            'onInAear': function () {
            }, // 拖动元素进入设计器区域的监听
            'onLeaveAear': function () {
            }, // 拖动元素离开设计器区域的监听
            'onDragStart': function () {
            }, // 拖动开始的监听
            'onDraging': function () {
            }, // 拖动中的监听
            'onDragEnd': function () {
            }, // 拖动开始的监听
            'onSelectComp': function () {
            }, // 选择控件的监听
            'onDelComp': function () {
            } // 删除控件的监听
        }, cfg);
    },

    ready: function () {
        var me = this;
        var occupyLine = document.createElement('div');
        occupyLine.className = 'fd-occupyline';
        // 占位线
        this.set('occupyLine', occupyLine);
    },

    // 根据表单数据初始化表单控件
    renderForm: function (components, status) {
        var me = this;
        var $components = $();
        var $subComponent=$(),parentComponent
        components.forEach(function (item,i) {
            $components = $components.add(me._tplComponent$(item.type, item));
            if(item.subComponent&&item.subComponent.length>0){
                $subComponent=$()
                parentComponent= $($components[i])
                item.subComponent.forEach(function (comp) {
                    $subComponent=$subComponent.add(me._tplComponent$(comp.type, comp));
                })
                //console.log(item,i,item.subComponent,$subComponent)
                parentComponent.find('.fd-detail-container').append($subComponent)
                me.fire('changeParentComponent',parentComponent);
            }
        });
        this.set('componentStatus', status);
        this._$elem.append($components);
        this.checkAreaLen()

        //默认显示第一个
        var currElem = this._$elem.find('.fd-component').eq(0);
        currElem.addClass('active');
        var currComponent = this._packageComponent(currElem);
        this.set('currComponent', currComponent);
        this.fire('selectComp', currComponent);
    },
    //检测条件数量来是否显示提示
    checkAreaLen: function () {
        var keys = Object.keys(this._components)
        if (keys.length > 2) {
            $('.fd-add-field').hide()
        } else {
            $('.fd-add-field').show()
        }
    },
    renderFormStep2: function (contentStr) {
        if (!contentStr.length) {
            return;
        }
        var me = this;
        var contentList = '';
        var content = contentStr;
        for (x in content) {
            contentList += '<div class="approval-person-photo person-photo" id="' + content[x].oId + '"><img src="' + content[x].photo + '"/><p>' + content[x].name + '</p></div>';
        }
        $('#addKeyApprover').before(contentList);
        $('.fd-appr-type li:nth-child(2)').click();
    },

    renderUI: function () {
        var tpl = '<div class="<%= className %>">';
        this._$elem = $(_.template(tpl)({
            'className': this.get('className')
        }))
    },

    // 绑定外部事件
    _bindHandler: function () {
        this.eachBind({
            'inAear': 'onInAear',
            'leaveAear': 'onLeaveAear',
            'dragStart': 'onDragStart',
            'draging': 'onDraging',
            'dragEnd': 'onDragEnd',
            'selectComp': 'onSelectComp',
            'delComp': 'onDelComp'
        });
    },

    bindUI: function () {
        var me = this;

        this._bindHandler();

        this._$elem.on('mouseenter', '.fd-component', function (e) {
                var parent = $(this).parents('.fd-component')
                if (parent.length > 0) {
                    parent.removeClass('hover');
                }
                $(this).addClass('hover');
            })
            .on('mouseleave', '.fd-component', function () {
                var parent = $(this).parents('.fd-component')
                if (parent.length > 0) {
                    parent.addClass('hover');
                }
                $(this).removeClass('hover');
            })
            .on('mousedown', '.fd-component-remove', function () {
                return false;
            })
            .on('click', '.fd-component-remove', function (e) {
                me._removeComponent(this);
            });
        $('body').on('click', '.cancle', function () {
            $('.q-bg').hide()
        }).on('click', '.q-alert-closebtn', function () {
            $('.q-bg').hide()
        })

        // 无限延长长按事件的触发
        mTouch.config({
            'longTapDelay': 10000000
        });

        // 如果有拖动的目标元素，绑定拖动事件
        if (this.get('dragTarget')) {
            mTouch('body').on('swipestart', this.get('dragTarget'), function (e) {
                me._swipeStart(e, this, true);
            });
        }

        mTouch('body').off('swipestart', '.fd-component').on('swipestart', '.fd-component', function (e) {
                me._swipeStart(e, this, false);
                me.selectComp(this);
            })
            .on('swiping', function (e) {
                if (!me.get('_start')) {
                    return;
                }

                if (Math.abs(e.mTouchEvent.moveX) > 2 || Math.abs(e.mTouchEvent.moveY) > 2) {
                    me.listenDraging(e);
                }

            })
            .on('swipeend', function (e) {
                // 拖动结束
                me._swipeEnd(e);
            });
    },

    // 删除控件
    _removeComponent: function (elem) {
        var $parent = $(elem).closest('.fd-component'),
            id = $parent.data('id');
        if (this._components[id].selected) {
            webUtil.tips('该控件已经作为审批条件，不可删除')
            return
        }
        var self = this

        // if(this.get('componentStatus')=='1'){ //删除已启用模板控件提示隐藏
        //     $('.q-bg').show()
        //     $('.q-bg').find('.confirm').off('click').on('click',function () {
        //         self._remove(elem);
        //         $('.q-bg').hide()
        //     })
        //     return
        // }
        this._remove(elem);
    },
    _remove: function (elem) {
        var $parent = $(elem).closest('.fd-component'),
            currElem = $parent[0],
            id = $parent.data('id');

        if ($parent.hasClass('active')) {
            if ($parent.nextAll('.fd-component').length) {
                this.selectComp($parent.nextAll('.fd-component')[0])
            } else if ($parent.prevAll('.fd-component').length) {
                this.selectComp($parent.prevAll('.fd-component')[0])
            } else {
                var firstComponent = this.find('.fd-component')[0];
                firstComponent ? this.selectComp(firstComponent) : this.set('currComponent', null);
            }
        }
        var parentComponent=$parent.parents('.fd-component')
        $parent.off().remove();
        if(parentComponent.length==1){//元素有父组件
            this.fire('changeParentComponent',parentComponent);
        }
        this.delComponentProps(id);
        this.checkAreaLen()
        this.fire('delComp', this._packageComponent(currElem));
    },
    // 监听控件在区域内拖动
    listenDraging: function (e) {
        var myPosition = $(this.get('target'))[0].getBoundingClientRect(),
            disdragBox = $('.disdrag-box'),
            mtouch = e.mTouchEvent,
            drag,
            disdragArr = [],
            position
        this._dragStart(e);
        drag = {
            top: myPosition.top,
            left: myPosition.left,
            width: myPosition.width,
            height: myPosition.height
        }
        disdragBox.each(function (i, e) {
            position = e.getBoundingClientRect()
            disdragArr[i] = {
                top: position.top,
                left: position.left,
                width: position.width,
                height: position.height
            }
        })
        var checkIn = function (touch, clientRect) {
            return touch.pageX >= clientRect.left && touch.pageX <= clientRect.left + clientRect.width && touch.pageY >= clientRect.top && touch.pageY <= clientRect.top + clientRect.height
        }

        var dragElem = this.get('_dragElem').elem;
        this.fire('draging', e, this._packageComponent(dragElem));

        // 判断元素是否拖动进入设计框区域
        if (checkIn(mtouch, drag)) {
            var disdrag = false
            $.each(disdragArr, function (i, e) {
                if (checkIn(mtouch, e)) {
                    disdrag = true
                    return false
                }
            });
            if (disdrag) {
                this._leaveAear(e);
            } else {
                this._inAear(e);
            }
        } else {
            this._leaveAear(e);
        }
    },

    // 进入区域
    _inAear: function (e) {
        if (!this.get('_isInAear')) {
            this.set('_isInAear', true);
            this.fire('inAear');
            this._showOccupyLine();
        }

        // 根据拖动坐标获取坐标最近的控件节点

        var comObj = this._getComponentByPos(e.clientX, e.clientY);

        var component = comObj.component

        var oldElem = this.get('_$curSubElem')

        this.set('_$curSubElem', null)

        if (comObj.curComponent) {
            var curProps = this.getComponentProps($(comObj.curComponent).data('id'))
            var type = this._getType(this.get('_dragElem'))
            if (curProps.subComponent && type !== 'detailfield' && type !== 'photofield' && type !== 'filefield') {//明细控件判断
                var parentElem=$(comObj.curComponent).find('.fd-detail-container')
                var subComObj = this._getComponentByPos(e.clientX, e.clientY, parentElem);
                component = subComObj.component
                this.set('_$curSubElem', parentElem)
            }
        }

        if (this.get('_component') === component && oldElem === this.get('_$curSubElem')) {
            return;
        }

        this._showOccupyLine(component);
        this.set('_component', component);
    },

    // 离开区域
    _leaveAear: function (e) {
        if (this.get('_isInAear')) {
            this.set('_isInAear', false);
            this._$elem.removeClass('dragin');
            this._hideOccupyLine();
            this.fire('leaveAear');
        }

        this.set('_component', null);
    },

    // 移动开始
    _dragStart: function (e) {
        if (!this.get('_dragStart')) {
            var dragElemObj = this.get('_dragElem');

            this.set('_dragStart', true);
            this.fire('dragStart', e, this._packageComponent(dragElemObj.elem));

            this._$elem.addClass('draging');
            if (dragElemObj.isNew === false) {
                $(dragElemObj.elem).addClass('draging');
            }
        }
    },

    // 按钮鼠标开始
    _swipeStart: function (e, elem, isNew) {
        // 不是鼠标左键
        if ($(elem).hasClass('disdrag')) {//namefield不能拖动
            return
        }
        if (e.button !== 0) {
            return;
        }
        e.preventDefault();
        this.set('_start', true);
        this.set('_dragElem', {
            elem: elem,
            isNew: isNew
        });
    },

    _swipeEnd: function (e) {
        if (!this.get('_start')) {
            return;
        }
        if (this.get('_isInAear')) {
            var dragElemObj = this.get('_dragElem');

            var designerElem = $('.fd-designer-inner')
            var dragElem = dragElemObj.elem;
            // 需要添加新的控件
            if (dragElemObj.isNew) {
                var dragElemType = dragElem.getAttribute('data-type');
                var filefieldLen = designerElem.find(".fd-component-filefield").length;
                var photofieldLen = designerElem.find(".fd-component-photofield").length;
                if (filefieldLen > 0 && dragElemType == 'filefield') {
                    webUtil.tips('只能设置一个文件上传控件');
                    this.fire('dragEnd', e);
                    this._hideOccupyLine();
                    this.resetStatus();
                    return;
                }
                if (photofieldLen > 0 && dragElemType == 'photofield') {
                    webUtil.tips('只能设置一个图片上传控件');
                    this.fire('dragEnd', e);
                    this._hideOccupyLine();
                    this.resetStatus();
                    return;
                }
                dragElem = this._tplComponent$(dragElemType)[0];
            }

            var component = this.get('_component');
            var $curElem =  this.get('_$curSubElem')||this._$elem
            var parentComponent=$(dragElem).parents('.fd-component')

            if (component === null) {
                $curElem.append(dragElem);
            } else {
                $(component).before(dragElem);
            }


            if(this.get('_$curSubElem')){//拖动元素目标为父组件
                this.fire('changeParentComponent',$curElem.parents('.fd-component'));
            }else if(parentComponent.length==1){//拖动元素有父组件
                this.fire('changeParentComponent',parentComponent);
            }
            this.selectComp(dragElem);
            this.checkAreaLen()
            this._hideOccupyLine();
        }

        this._$elem.removeClass('draging');
        $(this.get('currComponent').srcElem).removeClass('draging');

        this.resetStatus();
        this.fire('dragEnd', e);
    },
    _getType: function (elmobj) {
        var type, props
        if (elmobj.isNew) {
            type = elmobj.elem.getAttribute('data-type');
        } else {
            props = this.getComponentProps($(elmobj.elem).data('id'))
            type = props ? props.type : null
        }
        return type
    },
    resetStatus: function () {
        this.set('_start', false);
        this.set('_dragElem', null);
        this.set('_dragStart', false);
        this.set('_component', null);
        this.set('_isInAear', false);
    },

    // 选中控件
    selectComp: function (currElem) {
        this.find('.active').removeClass('active');
        $(currElem).addClass('active');
        var currComponent = this._packageComponent(currElem);
        this.set('currComponent', currComponent);
        this.fire('selectComp', currComponent);
    },

    // 显示占位线
    _showOccupyLine: function (component) {
        var occupyLine = this.get('occupyLine');
        occupyLine.style.display = 'block';
        var $curComponent = this.get('_$curSubElem') || this._$elem
        if (!component) {
            $curComponent.append(occupyLine);
        } else {
            component.parentNode.insertBefore(occupyLine, component);
        }
    },

    // 隐藏占位线
    _hideOccupyLine: function () {
        this.get('occupyLine').style.display = 'none';
    },

    // 根据拖动坐标获取坐标最近的控件节点信息 component 坐标最近的控件节点 curComponent 坐标当前处于的控件节点
    _getComponentByPos: function (x, y, $elem) {
        $elem = $elem || this._$elem
        var components = $elem.children('.fd-component'),
            obj = {
                component: null,
                curComponent: null//处于当前组件
            }
        if (!components.length) {
            return obj;
        }
        var firstPos = components[0].getBoundingClientRect()

        for (var i = 0, len = components.length; i < len; i++) {
            var pos = components[i].getBoundingClientRect()
            if (y >= pos.top && y <= (pos.top + pos.height )) {
                obj.curComponent = components[i]
            }
        }

        for (var i = 0, len = components.length; i < len; i++) {
            var pos = components[i].getBoundingClientRect()
            // 第一个
            if (y <= (firstPos.top + firstPos.height / 2)) {
                obj.component = components[0]
                return obj;
            }
            //最后一个
            if (i == len - 1) {
                return obj;
            }
            var nextPos = components[i + 1].getBoundingClientRect();

            if (y >= (pos.top + pos.height / 2) && y <= (nextPos.top + nextPos.height / 2)) {
                obj.component = components[i + 1]
                return obj;
            }
        }
    },

    // 拼装控件html
    _tplComponent$: function (componentType, props) {
        if (Designer[componentType]) {
            var props = $.extend(true, {}, props || Designer[componentType].props);
            var $component = $(Designer.getWidgetTpl(props));
            var id = webUtil.uuid(8, 32);

            if (!props.id) {
                /*id = props.id.replace(props.type + '-', '');
                 } else {*/
                props.id = id = props.type + '-' + id;
            }

            //添加不可删除属性
            // if(props){
            //     console.log(props)
            // }

            if ($component.hasClass('disdrag-box')) {
                $component.children('.fd-component').data('id', id)
            } else {
                $component.data('id', id);
            }
            this.addComponentProps(id, props);
        }

        return $component;
    },

    _packageComponent: function (elem) {
        var $elem = $(elem);
        return {
            srcElem: elem,
            props: this.getComponentProps($elem.data('id')),
            isActive: $elem.hasClass('active')
        }
    },

    addComponentProps: function (id, props) {
        if (!this._components) {
            this._components = {};
        }
        this._components[id] = props;
    },

    getComponentProps: function (id) {
        return (id && this._components[id]);
    },

    delComponentProps: function (id) {
        delete this._components[id];
    },

    getAllComponents: function () {
        var $components = this.find('.fd-component');
        var arr = [],
            me = this;

        $components.each(function () {
            arr.push(me._packageComponent(this));
        });

        return arr;
    }
});

export  default FormArea;
