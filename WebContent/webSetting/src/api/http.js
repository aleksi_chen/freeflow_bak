import $http from 'superagent'
import ENV from '../config'
import helper from '../utils/helpers'
/**
* 合并对象
* @private
* @method merge
* @param {Object} defaultOptions
* @param {Object} options
* @return {Object} 返回已合并对象
*/
function merge(defaultOptions, options) {
    if (typeof (options) !== 'object') {
        options = {};
    }
    for (var key in options) {
        if (options.hasOwnProperty(key)) {
            defaultOptions[key] = options[key];
        }
    }
    return defaultOptions;
}

var request = {
    /**
    * 传入资源名，返回拼接的完整路径
    * @private
    * @method makeUrl
    * @param {Object} endpoint
    * @return {String} url
    */
    makeUrl: function (endpoint) {
        return `${ENV.baseURL}/${endpoint}`
    },
    /**
    * 公用http请求方法，根据参数发起请求，返回promise对象
    * @private
    * @method makeRequest
    * @param {Object} url
    * @param {Object} options
    * @return {Object} description
    */
    makeRequest: function (url, options) {
        var devAuthInfo;
        var defaultOptions = {
            url: url,
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            headers: {
                'Accept': 'application/json'
            },
            needAuth:true
        }
        //本地开发环境下为每个请求添加webLappToken进行通信
        if(process.env.NODE_ENV !== 'production' && localStorage.getItem('auth')){
            devAuthInfo = JSON.parse(localStorage.getItem('auth'))
        }


        var params = merge(defaultOptions, options || {})
        //需要带上lappName和webLappToken
        if(options.needAuth){
            var req = helper.getRequest();
            if (req.lappName && req.webLappToken) {
                params.url += '?lappName=' + req.lappName + '&webLappToken=' + req.webLappToken;
            }
        }
        //如果需要传输数据
        if (options && options.data) {
            params.data = options.data
        }
        return new Promise(function (resolve, reject) {

            var httpClient = $http[params.type.toLowerCase()](params.url)
            httpClient.set('Content-Type',params.contentType)
            if(params.headers){
                httpClient.set(params.headers)
            }
            if(params.type.toLowerCase() == 'get' && params.data){
                httpClient.query(params.data)
            }
            if(params.type.toLowerCase() != 'get' && params.data){
                httpClient.send(params.data)
            }
            //如果是本地localhost环境则为每个请求加上webtoken等信息
            if(devAuthInfo){
                httpClient.query(devAuthInfo)
            }
            httpClient.end(function (err, res) {
                if(typeof res.body==='string'&&(/^.+html(.|\n)+>$/g).test(res.body)){
                    document.write(res.body)
                    reject(err)
                }
                if (err) {
                    reject(err)
                }
                resolve(res)
            })
        })
    },
    /**
    * 模块入口,组装url,发起请求
    * @private
    * @method fetch
    * @param {Object} endpoint 资源名
    * @param {Object} options  请求方法等
    * @return {Object} description
    */
    fetch: function (endpoint, options) {
        //如果urlAssemble为false, 则不组装url前缀
        if(options && options.urlAssemble === false){
            return this.makeRequest(endpoint, options)
        }
        return this.makeRequest(this.makeUrl(endpoint), options)
    }
}

export default request
