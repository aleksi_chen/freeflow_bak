import Vue from 'vue'
import Alert from './Alert'

var defaultOptions = {
    className: '',
    visible: true,
    showInput: false,
    onlyAlert:false,
    inputValue: '',
    inputPlaceholder: '',
    inputReg: '',
    cancelBtnTxt: '取消',
    confirmBtnTxt: '确定',
    confirmActive: true,
    needDismiss: true,
    title: '标题',
    content: '内容'
}

function cloneRegExp(reg) {
    var pattern = reg.valueOf()
    var flags = ''
    flags += pattern.global ? 'g' : ''
    flags += pattern.ignoreCase ? 'i' : ''
    flags += pattern.multiline ? 'm' : ''
    return new RegExp(pattern.source, flags)
}

function install(Vue) {
    var AlertConstructor = Vue.extend(Alert)
    var alertInstance = null

    Object.defineProperty(Vue.prototype, '$alert', {

        get: function () {

            return function (options) {

                return new Promise(function (resolve, reject) {
                    if (alertInstance) {
                        alertInstance.$destroy(true)
                    }
                    alertInstance = new AlertConstructor({
                        el: document.createElement('div'),
                        data: Vue.util.extend(Vue.util.extend({}, defaultOptions), options)
                    })
                    alertInstance.$on('cancel', function () {
                        reject('cancel')
                    })
                    alertInstance.$on('confirm', function (data) {
                        resolve(data)
                    })
                    alertInstance.$on('dismiss', function () {
                        reject('dismiss')
                    })
                    alertInstance.$appendTo(document.body)
                })
            }.bind(this)
        }
    })
}

export default Vue.use(install)
