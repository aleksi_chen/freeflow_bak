/**
 * Created by yuce_wang on 2016/9/6.
 */
import $ from '../../lib/jquery-2.1.4.min'
    function LappPersonSelect(cfg){
        this.selectedCount = 0;
        this.xhrList = [];
        this.selectedMembers = cfg.selectedMembers;
        this.selectedGroupId = '';
        this.userId = cfg.eId,    //暂时用的eid
            this.orgPath = [];
        this.orgCache = {};
        this._$root = null;
        this._$parentElement = null;
        this.contactList = [];				  // 会话列表数据
        this.sessionLoading = true;         // 是否需要显示会话loading
        this.loadContactsComplete = false;    // 判断会话列表是否加载完成
        this.historyLen = 0;                  // 选人组件的历史记录数
        this.scrollIndex = 1;
        this.hasSelected=false;    // 选人组件的是否已操作
        this.deptLevel=0        // 部门负责人等级
        this.cfg = {
            parentElement: 'body',
            appId: '',				// 公众号id
            lightAppId: '', 		// 轻应用id
            eId: '',				// 企业id
            selectedMembers: '',	//记录上次选了哪些人
            appName: '',            // 应用名称
            theme: '',			    // 主题，如传入，创建组时以此命名组名称
            title: '',			    // 分享弹窗时的标题，也是会话组消息的标题
            content: '',		    // 分享弹窗界面内容
            cellContent: '',	    // 聊天界面显示的内容
            thumbData : '',	    	// 分享的图标（base64）
            webpageUrl : '',		// 分享内容的跳转链接
            callbackUrl : '',      	// 创建会话组分享成功后的回调地址
            callbackParam: {},		// 回调地址传参
            successCallback: ''	,	// 回调成功后的处理函数
            existingSessionIsNeed: true, //是否需要已有会话
            cancelCallBack: function(resp){
                console.log(resp)
            }, //取消回调
            sureCallBack: function(resp){
                console.log(resp)
            } ,//确定回调
            removeCallBack: function(resp){

            }
        };

        this.init(cfg);
    }

    LappPersonSelect.prototype = {
        init: function(cfg){
            this.initCfg(cfg);
            this.renderUI();
            this.initContactList(0, 150);
            this.bindEvt();
            this.initFirstData(cfg.selectedMembers)
        },

        initCfg: function(cfg){
            $.extend(this.cfg, cfg);
            var keys = Object.keys(this.cfg.selectedMembers);
            this.selectedCount=keys.length
        },
        //上次如果选了人
        initFirstData: function(selectedMembers){
            //上次没有选择人并且没有初始化也没人
            if(this.storeParam().get('isSelectPersonEmpty') === 'true'){
                $('.lapp-person-select-wrap .contact-list-wrap .lapp-result-none').show();
            }
            else{
                $('.lapp-person-select-wrap .contact-list-wrap .lapp-result-none').hide();
            }

            var self = this;

            //将上次选择的人取出来
            if(this.storeParam().get(self.userId)){
                var obj = JSON.parse(this.storeParam().get(self.userId))[this.userId];
                //console.log(obj)
                $.each(obj, function(key, val){
                    var userId = key,
                        photoUrl = val.photoUrl,
                        userName = val.userName;

                    self.renderContactList(val);
                })
            }

            $.each(selectedMembers, function(key, val){
                var userId = key,
                    photoUrl = val.photoUrl,
                    userName = val.userName,
                    deptRoleType=val.deptRoleType,
                    deptLevel=val.deptLevel
                if(deptRoleType){
                    self.deptLevel++
                    $('.lapp-person-select-info ul').append(self.selectedDeptLevelTp(deptLevel));
                }else{
                    $('.lapp-person-select-info ul').append(self.selectedPersonTpl(userId, photoUrl, userName));
                }
                $('.lapp-person-select-wrap .contact-detail-ul').find('li[data-userid="' + userId + '"]').addClass('checked');
                $('.no-select').hide();
                //$('.no-need-session')
                //$('.lapp-person-select-wrap .no-need-session ul').append(self.selectedPersonTpl(userId, photoUrl, userName));
            })

            this.showSelectedCount()

        },

        //回到组织架构
        toBackOrganization: function(){
            $('body').on('click', '.person-select-organization-title', function(){
                /*alert(window.history.length);
                 console.log(window.history)
                 window.history.go(-1);*/
                window.location.hash = '#lapp-contact-list';
            })
        },

        //将经常选的人存储下来
        storeParam: function(){
            var obj = {}
            if (window.localStorage && typeof window.localStorage.setItem == 'function') {
                obj = {
                    set: function (key, value) {
                        var oldval = this.get(key);

                        //在setItem前先removeItem避免iPhone/ipad上偶尔的QUOTA_EXCEEDED_ERR错误
                        if (oldval !== '') {
                            this.remove(key);
                        }

                        window.localStorage.setItem(key, value);

                        return this;
                    },

                    get: function (key) {
                        var value = window.localStorage.getItem(key);
                        //如果为空统一返回null
                        return !value ? '' : value;
                    },

                    remove:  function (key) {
                        window.localStorage.removeItem(key);

                        return this;
                    }
                }


            } else {
                //操作cookie
                //expires传过期月数
                obj = {
                    set: function (name, value, expires, path, domain, secure) {
                        var cookieText = encodeURIComponent(name) + '=' + encodeURIComponent(value);

                        if (!expires) {
                            expires = 12;   //默认一年过期
                        }

                        var today = new Date();
                        expires *= 2592000000 ;
                        var expires_date = new Date(today.getTime() + expires) ;
                        cookieText += '; expires=' + expires_date.toGMTString();

                        if (path) {
                            cookieText += '; path=' + path;
                        }

                        if (domain) {
                            cookieText += '; domain=' + domain;
                        }

                        if (secure) {
                            cookieText += '; secure';
                        }

                        document.cookie = cookieText;

                        return this;
                    },

                    get: function (name) {
                        var reg = new RegExp(name + '\=([^;=]+)');
                        var match = reg.exec(document.cookie);

                        return match ? match[1] : '';
                    },

                    remove: function (name, path, domain, secure) {
                        this.set(name, '', new Date(0), path, domain, secure);

                        return this;
                    }
                }

            }

            return obj
        },

        renderUI: function(){
            this._$parentElement = $(this.cfg.parentElement);
            this.setParentEleOverflow();
            window.location.hash='#lapp-contact-list';

            var $tpl = $(this.tpl());
            this._$root = $tpl;
            this._$parentElement.append($tpl);
        },

        initContactList: function(offset, count){
            var self = this;

            var jqxhr = $.ajax({
                type: 'get',
                cache: false,
                url: '/im/web/updatelistGroup.do',
                data: {
                    offset: offset,
                    count: count
                },
                dataType: 'json',
                success: function(resp){
                    $('.lapp-person-select-wrap .contact-list-wrap .lapp-search-loading').hide();
                    self.sessionLoading = false;
                    $('.lapp-person-select-wrap .person-select-exist-sessions .lapp-search-loading').hide();

                    if(resp.list && resp.list.length > 0){
                        self.contactList = self.contactList.concat(resp.list);
                        //self.renderContactList(resp.list);
                        self.renderSessionList(resp.list);
                    }

                    if(resp.more){
                        self.initContactList(offset + count, count);
                    }
                    else{
                        self.loadContactsComplete = true;
                        // 联系人列表为空
                        if($('.lapp-person-select-wrap .contact-list-wrap .contact-detail-ul li').length === 0){
                            //$('.lapp-person-select-wrap .contact-list-wrap .lapp-result-none').show();
                        }
                    }
                }
            });

            this.xhrList.push(jqxhr);
        },

        // initSessionList: function(offset, count){
        // 	var self = this;

        //           var jqxhr = $.ajax({
        //           	type: 'get',
        //           	cache: false,
        //           	url: '/im/web/updatelistGroup.do',
        //           	data: {
        //           		offset: offset,
        //               	count: count
        //           	},
        //           	dataType: 'json',
        //           	success: function(resp){
        //           		$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-search-loading').hide();
        //           		if(resp.list && resp.list.length > 0){
        //                    self.renderSessionList(resp.list);
        //                }

        //                if(resp.more){
        //                	self.initSessionList(offset + count, count);
        //                }
        //                else{
        //                	// 已有会话为空
        //                	if($('.lapp-person-select-wrap .person-select-exist-sessions .contact-detail-ul li').length === 0){
        //                		$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-result-none').show();
        //                	}
        //                }
        //           	}
        //           });

        //           this.xhrList.push(jqxhr);
        // },

        bindEvt: function(){
            this.bindSearchEvt();
            this.bindSearchSession();
            this.bindContactListEvt();
            this.bindLinkEvt();
            this.bindHashChangeEvt();
            this.bindSelectedPersonsEvt();
            this.bindOrgTreeEvt();
            this.bindButtonEvt();
            this.bindScrollEvt();

            //清空选人
            this.clearEmpty();
            //回到组织架构
            this.toBackOrganization()
        },

        //清空选人
        clearEmpty: function(){
            var me = this;
            $('body').on('click', '.clear-empty', function(){
                $('.lapp-person-select-info ul').html('');
                $('.contact-detail-ul li').removeClass('checked');
                $('.no-select').show();
                me.selectedMembers = {};
            })
        },

        // 联系人列表点击事件
        bindContactListEvt: function(){
            var self = this;
            // 点击用户
            $('.lapp-person-select-wrap').on('click', '.contact-detail-ul li.contact-detail', function(e){
                var $li = $(e.target).closest('li'),
                    userId = $li.data('userid'),
                    photoUrl = $li.find('img').attr('src'),
                    groupId = $li.data('groupid'),
                    userName = $li.find('h3').text();

                if($li.hasClass('checked')){
                    //return;


                    $('.lapp-person-select-wrap .contact-detail-ul').find('li[data-userid="' + userId + '"]').removeClass('checked');
                    if(self.selectedMembers[userId]){
                        //console.log(self.selectedMembers);
                        delete self.selectedMembers[userId];
                        //self.cfg.removeCallBack(self.selectedMembers);
                    }

                    $('.lapp-person-select-wrap .selected-persons-wrap').find('li[data-userid="' + userId + '"]').remove();
                    $('.lapp-person-select-info li[data-userid="' + userId + '"]').remove();

                    if($('.lapp-person-select-info ul').html() == ''){
                        $('.no-select').show();
                    }
                    self.selectedCount--;
                    self.hasSelected=true

                    // 选择人员数目小于6，隐藏左右滚动箭头
                    if(self.selectedCount == 5){
                        $('.lapp-person-select-wrap .person-move-left').hide();
                        $('.lapp-person-select-wrap .person-move-right').hide();
                        $('.lapp-person-select-wrap .selected-persons-scroll-wrap').removeClass('selected-persons-four');
                        $('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': '0px'}, 300);
                        self.scrollIndex = 1;
                    }
                    // 选择删除最后一版元素
                    else if(self.selectedCount >= 6 && self.scrollIndex == self.selectedCount - 2){
                        var marginLeft = -(self.scrollIndex - 2) * 46 + 'px';
                        $('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': marginLeft}, 300);
                        self.scrollIndex--;
                    }
                }
                else{
                    $('.lapp-person-select-info p.no-select').hide();
                    $('.lapp-person-select-wrap .contact-detail-ul').find('li[data-userid="' + userId + '"]').addClass('checked');
                    if(self.selectedMembers[userId]){
                        return;
                    }
                    // 如果groupId为空，表明是在搜索列表或组织架构列表，则去常用联系人列表中尝试获取groupId
                    if(!groupId){
                        groupId = $('.lapp-person-select-wrap .contact-list-wrap .contact-detail-ul').find('li[data-userid="' + userId + '"]').data('groupid');
                    }

                    var obj = {
                        userId: userId,
                        photoUrl: photoUrl,
                        groupId: groupId,
                        userName: userName
                    };
                    self.selectedMembers[obj.userId] = obj;

                    $('.lapp-person-select-info ul').append(self.selectedPersonTpl(userId, photoUrl, userName));

                    self.selectedCount++;
                    self.hasSelected=true

                    // 如果选择的人数大于5，显示左右滚动箭头
                    if(self.selectedCount == 6){
                        $('.lapp-person-select-wrap .person-move-left').show();
                        $('.lapp-person-select-wrap .person-move-right').show();
                        $('.lapp-person-select-wrap .selected-persons-scroll-wrap').addClass('selected-persons-four');
                    }
                }
                self.showSelectedCount();

            });

            // 点击已有会话
            $('.lapp-person-select-wrap').on('click', '.contact-detail-ul li.group-detail', function(e){
                $(this).addClass('selected');
                $('.lapp-person-select-wrap .content-box').fadeIn();
                $('.lapp-person-select-wrap .person-select-overlay').fadeIn();
                // 标识为点击已有会话打开
                $('.lapp-person-select-wrap .content-box').attr('openSource', '1');
                self.selectedGroupId = $(this).data('groupid');
            });
        },

        // 已选择的人员列表点击事件
        bindSelectedPersonsEvt: function(){
            var self = this;
            $('.lapp-person-select-info').on('click', '.delete-selected-person', function(e){
                var $li = $(e.target).closest('li'),
                    userId = $li.data('userid'),
                    deptLevel= $li.data('deptlevel');
                if(userId){
                    $('.lapp-person-select-wrap li.contact-detail[data-userid="' + userId + '"]').removeClass('checked');
                    if(self.selectedMembers[userId]){
                        delete self.selectedMembers[userId];
                        //self.cfg.removeCallBack(self.selectedMembers);
                    }
                }else if(deptLevel){
                    //todo
                    self.deptLevel--
                    var $nextDeptLevel= $li.nextAll('.deptLevel')
                    $nextDeptLevel.each(function (i,e) {
                        $(e).replaceWith(self.selectedDeptLevelTp($(e).data('deptlevel')-1))
                    })
                }
                self.selectedCount--;
                self.hasSelected=true
                self.showSelectedCount();
                $li.remove();

                /*// 选择人员数目小于6，隐藏左右滚动箭头
                 if(self.selectedCount < 6){
                 $('.lapp-person-select-wrap .person-move-left').hide();
                 $('.lapp-person-select-wrap .person-move-right').hide();
                 $('.lapp-person-select-wrap .selected-persons-scroll-wrap').removeClass('selected-persons-four');
                 $('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': '0px'}, 300);
                 self.scrollIndex = 1;
                 }
                 // 选择删除最后一版元素
                 else if(self.scrollIndex == self.selectedCount - 2){
                 var marginLeft = -(self.scrollIndex - 2) * 46 + 'px';
                 $('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': marginLeft}, 300);
                 self.scrollIndex--;
                 }*/
                if($('.lapp-person-select-info ul').html() == ''){
                    $('.lapp-person-select-info p.no-select').show();
                }
            });
        },

        // 搜索用户
        bindSearchEvt: function(){
            var	self = this;
            $('.person-search-wrap .search-input').on('input propertychange', function(e){
                var word = htmlEntities($(e.target).val().trim()),
                    me = this,
                    hash = window.location.hash;

                clearTimeout(this.searchTimeout);
                this.searchTimeout = setTimeout(function(){
                    $('.lapp-person-select-wrap .search-list-wrap .contact-detail-ul').empty();
                    if(word === '') {
                        $('.lapp-person-select-wrap .contact-list-wrap').show();
                        $('.lapp-person-select-wrap .search-list-wrap').hide();
                        if(hash != '#lapp-contact-list'){
                            $('.person-select-contacts').hide();
                            $('.lapp-person-select-wrap .person-select-organization').show();
                        }

                    }
                    else {
                        $('.person-select-contacts').show();
                        $('.lapp-person-select-wrap .person-select-organization').hide();
                        $('.lapp-person-select-wrap .contact-list-wrap').hide();
                        $('.lapp-person-select-wrap .search-list-wrap').show();
                        $('.lapp-person-select-wrap .search-list-wrap .lapp-result-none').hide();
                        $('.lapp-person-select-wrap .search-list-wrap .lapp-search-loading').show();

                        var reqData = {
                            protocol: 'get',
                            url: '/im/web/searchUser.do',
                            param: { word: word }
                        };
                        self.commonRequest(reqData).done(function (resp) {
                            $('.lapp-person-select-wrap .search-list-wrap .lapp-search-loading').hide();
                            if(resp.data.list && resp.data.list.length > 0){
                                self.renderSearchList(resp.data.list, word);
                            }
                            else{
                                $('.lapp-person-select-wrap .search-list-wrap .lapp-result-none').show();
                            }
                        }).fail(function () {
                            $('.lapp-person-select-wrap .search-list-wrap .lapp-search-loading').hide();
                            $('.lapp-person-select-wrap .search-list-wrap .lapp-result-none').show();
                        }).always(function() {

                        });
                    }

                    clearTimeout(me.searchTimeout);
                    me.searchTimeout = null;
                }, 500);
            });
        },

        // 搜索会话组
        bindSearchSession: function () {
            var	self = this,
                timerId,
                $result = $('.person-select-exist-sessions .lapp-result-none');

            $('.session-search-wrap .search-input').on('input propertychange', function(){
                var $ul = $('.person-select-exist-sessions .contact-detail-ul'),
                    $li = $ul.find('li');

                if ($li.length == 0) {
                    return ;
                }

                var word = $(this).val().trim(),
                    me = this;

                clearTimeout(timerId);
                timerId = setTimeout(function() {

                    if (word === '') {
                        $ul.show();
                        $li.show();
                        $result.hide();
                    } else {
                        var $searchList = $ul.find('li[data-groupname*="' + word + '"]');

                        // 搜索无结果
                        if ($searchList.length == 0) {
                            $ul.hide();
                            $result.show();
                        } else {
                            $ul.show();
                            $result.hide();
                            $li.hide();
                            $searchList.show();
                        }
                    }
                }, 300);
            });
        },

        bindButtonEvt: function(){
            var self = this;

            /*// 取消
             $('.lapp-person-select-wrap .content-box-footer .content-box-cancel').click(function(){
             // 去掉选中会话组的效果
             var openSource = $('.lapp-person-select-wrap .content-box').attr('openSource');
             if(openSource === '1'){
             var groupId = self.selectedGroupId;
             $('.lapp-person-select-wrap .person-select-exist-sessions li[data-groupid="' + groupId + '"]').removeClass('selected');
             }

             $('.lapp-person-select-wrap .content-box').fadeOut();
             $('.lapp-person-select-wrap .person-select-overlay').fadeOut();
             });

             // 发送
             $('.lapp-person-select-wrap .content-box-footer .content-box-confirm').click(function(){
             $('.lapp-person-select-wrap .content-box').fadeOut();

             var openSource = $('.lapp-person-select-wrap .content-box').attr('openSource');
             if(openSource === '0'){
             var selectedMembers = self.selectedMembers;

             // 单人，执行回调后发送消息
             if(self.selectedCount == 1){
             var groupId = '', userId = '';
             for(var id in selectedMembers){
             userId = id;
             groupId = selectedMembers[id]['groupId'];
             break;
             }
             self.handleCallback(groupId, userId);
             }
             // 多人，创建新会话，执行回调，发送消息
             else{
             var userIds = [],
             groupName = self.cfg.theme;

             for(var id in selectedMembers){
             if(!selectedMembers.hasOwnProperty(id)){
             continue;
             }
             userIds.push(id);
             }

             $.ajax({
             type: 'post',
             traditional: true,
             url: '/im/web/createGroup.do',
             data: {
             groupName: groupName,
             userIds: userIds
             },
             dataType: 'json',
             success: function(resp){
             if(resp.success === true){
             var	userId = '',
             groupId = resp.data && resp.data.groupId;
             self.handleCallback(groupId, userId);
             }
             else{
             self.showTips('发送失败', 1000, function(){
             $('.lapp-person-select-wrap .person-select-overlay').fadeOut();
             });
             }
             }
             });
             }
             }
             // 选择已有会话
             else if(openSource === '1'){
             var groupId = self.selectedGroupId,
             userId = '';

             self.handleCallback(groupId, userId);
             }
             });*/

            // 点击开始按钮
            $('.select-organizational-container .select-persons-button-start').click(function(){
                /*if(!$(this).hasClass('selected')){
                 return;
                 }*/
                //存储常用选择人

                var obj = {};
                obj[self.userId] = self.selectedMembers;
                self.storeParam().set(self.userId, JSON.stringify(obj));

                if(self.isEmptyObject(self.selectedMembers)){
                    self.storeParam().set('isSelectPersonEmpty', 'true');
                }
                else{
                    self.storeParam().set('isSelectPersonEmpty', 'false');
                }
                //console.log(self.selectedMembers)

                //上次没有选择人并且没有初始化也没人
                if(self.storeParam().get('isSelectPersonEmpty') === 'true'){
                    $('.lapp-person-select-wrap .contact-list-wrap .lapp-result-none').show();
                }
                else{
                    $('.lapp-person-select-wrap .contact-list-wrap .lapp-result-none').hide();
                }
                //点击确定回调
                self.cfg.sureCallBack(self.selectedMembers);
                self.destroy();
                self.resetHistory();
                /*$('.lapp-person-select-wrap .content-box').fadeIn();
                 $('.lapp-person-select-wrap .person-select-overlay').fadeIn();*/
                // 标识为点击开始按钮打开
                $('.select-organizational-container .content-box').attr('openSource', '0');
            });

            // 关闭
            $('.select-organizational-container .select-persons-button-close').click(function(){
                self.cfg.cancelCallBack(self.selectedMembers);
                self.destroy();
                self.resetHistory();
            });
        },

        bindLinkEvt: function(){
            var self=this
            //点击组织架构
            $('.lapp-person-select-wrap .link-list .principal-link').click(function(){
                $('.lapp-person-select-info p.no-select').hide();
                self.deptLevel++
                self.selectedCount++;
                $('.lapp-person-select-info ul').append(self.selectedDeptLevelTp(self.deptLevel));
            });

            //点击组织架构
            $('.lapp-person-select-wrap .link-list .organization-link').click(function(){
                window.location.hash = '#lapp-organization';
            });

            // 点击已有会话
            $('.lapp-person-select-wrap .link-list .exist-sessions-link').click(function(){
                window.location.hash = '#lapp-exist-sessions';
            });

        },

        bindScrollEvt: function(){
            var self = this;
            // 已选择人员列表向右滚动
            $('.lapp-person-select-wrap .person-move-left').click(function(){
                var index = self.scrollIndex;
                if(index == 1){
                    return;
                }
                var marginLeft = -(index - 2) * 46 + 'px';
                $('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': marginLeft}, 300);
                self.scrollIndex--;
            });

            // 已选择人员列表向左滚动
            $('.lapp-person-select-wrap .person-move-right').click(function(){
                var index = self.scrollIndex;
                if(index == self.selectedCount - 3){
                    return;
                }
                var marginLeft = -index * 46 + 'px';
                $('.lapp-person-select-wrap .selected-persons-wrap').animate({'marginLeft': marginLeft}, 300);
                self.scrollIndex++;
            });
        },

        bindHashChangeEvt: function(){
            this.newHashChangeHandler = this.hashChangeHandler.bind(this);
            $(window).on('hashchange', this.newHashChangeHandler);
        },

        unbindHashChangEvt: function(){
            $(window).off('hashchange', this.newHashChangeHandler);
        },

        hashChangeHandler: function(){
            var hash = window.location.hash;

            if(hash === '#lapp-exist-sessions'){
                $('.lapp-person-select-wrap .person-select-container').hide();
                $('.lapp-person-select-wrap .person-select-exist-sessions').show();
                $('.lapp-person-select-wrap .selected-persons-toolbar-left').hide();
                $('.lapp-person-select-wrap .select-persons-button-start').hide();

                if (this.loadContactsComplete === true) {
                    // 已有会话为空
                    if($('.lapp-person-select-wrap .person-select-exist-sessions .contact-detail-ul li').length === 0){
                        $('.lapp-person-select-wrap .person-select-exist-sessions .lapp-result-none').show();
                    }
                }

                if (this.sessionLoading === true) {
                    $('.lapp-person-select-wrap .person-select-exist-sessions .lapp-search-loading').show();

                    this.sessionLoading = false;

                    // if(this.loadContactsComplete === true){
                    // 	this.renderSessionList(this.contactList);

                    // 	// 已有会话为空
                    //             	if($('.lapp-person-select-wrap .person-select-exist-sessions .contact-detail-ul li').length === 0){
                    //             		$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-result-none').show();
                    //             	}
                    // }
                    // else{
                    // 	$('.lapp-person-select-wrap .person-select-exist-sessions .lapp-search-loading').show();
                    //           this.initSessionList(0, 150);
                    // }
                } /*else {
                 $('.lapp-person-select-wrap .person-select-exist-sessions .lapp-search-loading').hide();
                 }*/

                this.historyLen = 2;
            } else {
                var $searchInput = $('.person-select-exist-sessions .search-input');
                if ($searchInput.val().trim() != '') {
                    $searchInput.val('').trigger('input');
                }

                if(hash === '#lapp-organization'){
                    $('.lapp-person-select-wrap .person-select-container').hide();
                    $('.lapp-person-select-wrap .person-select-organization').show();
                    $('.lapp-person-select-wrap .selected-persons-toolbar-left').show();
                    $('.lapp-person-select-wrap .select-persons-button-start').show();

                    this.renderOrgTree();
                    this.historyLen = 2;
                }
                else if(hash === '#lapp-contact-list'){
                    $('.lapp-person-select-wrap .person-select-container').hide();
                    $('.lapp-person-select-wrap .person-select-contacts').show();
                    $('.lapp-person-select-wrap .selected-persons-toolbar-left').show();
                    $('.lapp-person-select-wrap .select-persons-button-start').show();

                    this.historyLen = 1;
                }
                else{
                    this.destroy();
                    this.historyLen = 0;
                }
            }
        },

        bindOrgTreeEvt: function(){
            var self = this,
                timeout = null;

            // 进入下一级组织
            $('.lapp-person-select-wrap .person-select-organization').on('click', 'li.department', function(){
                var $ul = $(this).closest('ul'),
                    $li = $(this).closest('li'),
                    id = $li.data('id'),
                    name = $.trim($li.text()),
                    node = self.orgCache[id];

                if(timeout){
                    clearTimeout(timeout);
                }

                timeout = setTimeout(function(){
                    if(node) {
                        node.parent = name;
                        self.orgPath.push(node);

                        var	tpl = self.orgTreeTpl(node), $tpl = $(tpl);
                        $('.lapp-person-select-wrap .person-select-organization .contact-detail-wrap').append($tpl);

                        $ul.animate({
                            'left': '-100%'
                        }, 300);

                        $tpl.animate({
                            'left': 0
                        }, 300);
                    }
                    else{
                        var eid = self.cfg.eId || '',
                            orgId = id,
                            reqData = {
                                protocol: 'post',
                                url: '/im/web/treeOrg.do',
                                param: {
                                    eid: eid,
                                    orgId: orgId,
                                    begin: 0,
                                    count: 10000
                                }
                            };

                        self.commonRequest(reqData).done(function (resp){
                            self.orgCache[orgId] = resp.data;
                            node = { 'parent': name };
                            $.extend(node, resp.data);
                            self.orgPath.push(node);

                            var	tpl = self.orgTreeTpl(node), $tpl = $(tpl);
                            $('.lapp-person-select-wrap .person-select-organization .contact-detail-wrap').append($tpl);

                            $ul.animate({
                                'left': '-100%'
                            }, 300);

                            $tpl.animate({
                                'left': 0
                            }, 300);
                        });
                    };
                }, 400);
            });

            // 返回上一级组织
            $('.lapp-person-select-wrap .person-select-organization').on('click', 'li.parent-org', function(){
                var $ul = $(this).closest('ul'),
                    $prevUl = $ul.prev(),
                    last = self.orgPath.length - 1;

                $ul.animate({
                    'left': '100%'
                }, 300, function(){
                    self.orgPath.splice(last, 1);
                    $ul.remove();
                });

                $prevUl.animate({
                    'left': 0
                }, 300);
            });
        },

        tpl: function(){
            var t = new SimpleTemplate();

            t._('<div class="select-organizational-container clearfix">')
                ._('<div class="lapp-person-select-info bsizing fl">')
                ._('<div class="lapp-person-select-head1">')
                ._('已选择审批人')
                ._('<span class="clear-empty">清空</span>')
                ._('</div>')
                ._('<ul class="clearfix"></ul>')
                ._('<p class="no-select center">请从右边选择审批人</p>')
                ._('</div>')

                ._('<div class="lapp-person-select-wrap fl">')
                ._('<div class="lapp-person-select-head2">')
                ._('请选择审批人')
                ._('<span></span>')
                ._('</div>')
                ._('<div class="link-list">')
                ._('<div class="principal-link link">')
                ._('<h3><span class="principal"></span>部门负责人')
                ._('<span class="fd-help fr"><i class="fa-help-icon"></i>如何设置</span>')
                ._('</h3>')
                ._('</div>')
                ._('</div>')
                ._('<div class="person-search-div">')
                ._('<div class="person-search-wrap">')
                ._('<span class="search-icon"></span>')
                ._('<input class="search-input" autocomplete="off" placeholder="姓名/拼音/电话">')
                ._('</div>')
                ._('</div>')
                //._('<div class="lapp-person-select-title">所有员工</div>')
                ._('<div class="person-select-contacts person-select-container">')
                /*._('<div class="person-search-div">')
                 ._('<div class="person-search-wrap">')
                 ._('<span class="search-icon"></span>')
                 ._('<input class="search-input" autocomplete="off" placeholder="姓名/拼音/电话"></input>')
                 ._('</div>')
                 ._('</div>')*/
                ._('<div class="contact-list-wrap">')
                ._('<div class="link-list">')
                ._('<div class="organization-link link">')
                ._('<img src="/lightapp/images/organization-icon.png">')
                ._('<h3>组织架构</h3>')
                ._('</div>');
            //是否需要已有会话
            if(this.cfg.existingSessionIsNeed){
                t._('<div class="exist-sessions-link link clearfix">')
                    ._('<img src="/lightapp/images/exist-sessions-icon.png">')
                    ._('<h3>已有会话</h3>')
                    ._('</div>');
            }

            t._('</div>')
                ._('<h3>上次选择联系人</h3>');
            if(this.cfg.existingSessionIsNeed){
                t._('<div class="contact-list">');
            }
            else{
                t._('<div class="contact-list no-need-session">');
            };

            t._('<div class="contact-detail-wrap">')
                ._('<ul class="contact-detail-ul">')
                ._('</ul>')
                ._('</div>')
                /*._('<div class="lapp-search-loading"><img src="/lightapp/images/loading.gif"></div>')*/
                ._('<div class="lapp-result-none">')
                ._('<img src="/lightapp/images/result-none.png">')
                ._('<span>暂无联系人</span>')
                ._('</div>')
                ._('</div>')
                ._('</div>')
                ._('<div class="search-list-wrap">')
                ._('<div class="contact-detail-wrap">')
                ._('<ul class="contact-detail-ul">')
                ._('</ul>')
                ._('</div>')
                ._('<div class="lapp-search-loading"><img src="/lightapp/images/loading.gif"></div>')
                ._('<div class="lapp-result-none">')
                ._('<img src="/lightapp/images/result-none.png">')
                ._('<span>无结果</span>')
                ._('</div>')
                ._('</div>')
                ._('</div>')
                ._('<div class="person-select-exist-sessions person-select-container">')
                ._('<div class="contact-detail-wrap">')
                ._('<div class="session-search-wrap"><span class="search-icon">')
                ._('</span><input class="search-input" autocomplete="off" placeholder="会话组名">')
                ._('</div>')
                ._('<ul class="contact-detail-ul">')
                ._('</ul>')
                ._('</div>')
                ._('<div class="lapp-search-loading"><img src="/lightapp/images/loading.gif"></div>')
                ._('<div class="lapp-result-none">')
                ._('<img src="/lightapp/images/result-none.png">')
                ._('<span>暂无会话</span>')
                ._('</div>')
                ._('</div>')
                ._('<div class="person-select-organization person-select-container">')
                ._('<div class="person-select-organization-title"><span></span>组织架构</div>')
                ._('<div class="contact-detail-wrap clearfix">')
                ._('</div>')
                ._('<div class="lapp-search-loading"><img src="/lightapp/images/loading.gif"></div>')
                ._('<div class="lapp-result-none">')
                ._('<img src="/lightapp/images/result-none.png">')
                ._('<span>没有组织架构</span>')
                ._('</div>')
                ._('</div>')

                ._('<div class="person-select-overlay"></div>')
                ._('<div class="content-box" openSource="0">')
                ._('<h3 class="ellipsis">' + this.cfg.title + '</h3>')
                ._('<div class="content-box-wrap">')
                ._('<div class="content-box-main clearfix">')
                ._('<img src="' + this.cfg.thumbData + '">')
                ._('<p>' + escapeContent(this.cfg.content) + '</p>')
                ._('</div>')
                ._('<div class="content-box-source">')
                ._('<span>来自：</span>')
                ._('<span class="app_name">' + this.cfg.appName + '</span>')
                ._('</div>')
                ._('</div>')
                ._('<div class="content-box-footer clearfix">')
                ._('<button class="content-box-cancel">取消</button>')
                ._('<button class="content-box-confirm">发送</button>')
                ._('</div>')
                ._('</div>')
                ._('<div class="person-select-prompt-msg"></div>')
                ._('</div>')
                ._('<div class="selected-persons-toolbar clearfix">')
                ._('<div class="select-persons-button-list clearfix">')
                ._('<button class="select-persons-button-start">确定</button>')
                ._('<button class="select-persons-button-close">取消</button>')
                ._('</div>')
                ._('</div>')
                ._('</div>')

            return t.toString();
        },

        //显示开始按钮上的数字
        showSelectedCount: function(){
            var $confirmButton = $('.select-organizational-container .select-persons-button-start');
            // console.log(this.selectedCount)
            if(this.hasSelected||this.selectedCount > 0){
                $confirmButton.text('确定');
                if(!$confirmButton.hasClass('selected')){
                    $confirmButton.addClass('selected');
                }
            }
            else{
                $confirmButton.removeClass('selected').text('确定');
            }
        },

        selectedPersonTpl: function(userId, photoUrl, userName){
            var t = new SimpleTemplate();
            t._('<li class="bsizing" data-userid="' + userId + '">')
                ._('<div class="select-persons-info">')
                ._('<img src="' + photoUrl + '">')
                ._('<span class="delete-selected-person"></span>')
                ._('</div>')
                ._('<p class="select-persons-info-username">'+ userName +'</p>')
                ._('</li>');

            return t.toString();
        },

        selectedDeptLevelTp: function(deptLevel){
            var t = new SimpleTemplate();
            t._('<li class="bsizing deptLevel" data-deptlevel="'+deptLevel+'">')
                ._('<div class="select-persons-info">')
                ._('<span class="principal"></span>')
                ._('<span class="delete-selected-person"></span>')
                ._('</div>')
                ._('<p class="select-persons-info-username">'+ this.NumberToChinese(deptLevel) +'级部门负责人</p>')
                ._('</li>');

            return t.toString();
        },
        NumberToChinese: function (numberText) {
            var CHINESE_NEGATIVE = "负";
            var CHINESE_ZERO = "零";
            var CHINESE_DIGITS = ["", "一", "二", "三", "四", "五", "六", "七", "八", "九"];
            var CHINESE_UNITS = ["", "十", "百", "千"];
            var CHINESE_GROUP_UNITS = ["", "万", "亿", "兆", "京", "垓", "杼", "穰", "溝", "澗", "正", "載", "極"];
            numberText=numberText+''
            numberText = numberText.replace(/^0+/g, "");
            numberText = numberText.replace(/^-0+/g, "-");
            if (numberText === "" || numberText === "-") {
                return CHINESE_ZERO;
            }
            var result = "";
            if (numberText[0] === "-") {
                result += CHINESE_NEGATIVE;
                numberText = numberText.substring(1);
            }

            var groupIsZero = true;
            var needZero = false;
            for (var i = 0; i < numberText.length; ++i) {
                var position = numberText.length - 1 - i;
                var digit = parseInt(numberText[i]);
                var unit = position % CHINESE_UNITS.length;
                var group = (position - unit) / CHINESE_UNITS.length;

                if (digit !== 0) {
                    if (needZero) {
                        result += CHINESE_ZERO;
                    }

                    if (digit !== 1 || unit !== 1 || !groupIsZero || (group === 0 && needZero)) {
                        result += CHINESE_DIGITS[digit];
                    }

                    result += CHINESE_UNITS[unit];
                }

                groupIsZero = groupIsZero && (digit === 0);

                if (unit === 0 && !groupIsZero) {
                    result += CHINESE_GROUP_UNITS[group];
                }

                needZero = (digit === 0 && (unit !== 0 || groupIsZero));

                if (unit === 0) {
                    groupIsZero = true;
                }
            }
            return result;
        },
        //搜索框列表
        renderSearchList: function(data, keyword){
            var t = new SimpleTemplate(),
                photoUrl,
                additional_message,
                defaultUrl = 'http://kdweibo.com/space/c/photo/load?id=';
            for(var i = 0, len = data.length; i < len; i++){
                photoUrl = data[i].photoUrl || defaultUrl;
                isSelectedCls = this.selectedMembers[data[i].id] ? ' checked' : '';
                var fullPinyin = data[i].fullPinyin,
                    phone = data[i].defaultPhone;
                additional_message = phone.indexOf(keyword) > -1 ? phone : fullPinyin.indexOf(keyword) > -1 ? fullPinyin : phone;

                t._('<li class="contact-detail clearfix' + isSelectedCls + '" data-userid="' + data[i].id + '">')
                    ._('<div class="contact-detail-inner clearfix">')
                    ._('<span class="contact-checkbox"></span>')
                    ._('<img src="' + photoUrl + '">')
                    ._('<div class="contact-content">')
                    ._('<div class="clearfix">')
                    ._('<h3 class="search-name-text ellipsis">' + data[i].name + '</h3>')
                    ._('<p class="search-department-text ellipsis">' + data[i].department + '</p>')
                    ._('</div>')
                    ._('<p>' + additional_message + '</p>')
                    ._('</div>')
                    ._('</div>')
                    ._('</li>');
            }

            $('.lapp-person-select-wrap .search-list-wrap .contact-detail-ul').append(t.toString());
        },

        isEmptyObject: function(obj){
            for(var n in obj){return false}
            return true;
        },


        //联系人列表
        renderContactList: function(data){
            /*for(var i = 0, len = data.length; i < len; i++){
             if(data[i].type === 1){
             $('.lapp-person-select-wrap .contact-list-wrap .contact-detail-ul').append(this.contactDetailTpl(data[i]));
             }
             }*/
            var html = [
                '<li class="contact-detail" data-userid="'+ data.userId +'" data-groupid="'+ data.groupId +'">',
                '<div class="contact-detail-inner clearfix">',
                '<span class="contact-checkbox"></span>',
                '<img src="'+ data.photoUrl +'">',
                '<div class="contact-content">',
                '<h3>'+ data.userName +'</h3>',
                '<p>职员</p>',
                '</div>',
                '</div>',
                '</li>'
            ].join('');
            $('.lapp-person-select-wrap .contact-list-wrap .contact-detail-ul').append(html);
        },

        //会话列表
        renderSessionList: function(data){
            for(var i = 0, len = data.length; i < len; i++){
                if(data[i].type === 2){
                    $('.lapp-person-select-wrap .person-select-exist-sessions .contact-detail-ul').append(this.groupDetailTpl(data[i]));
                }
            }
        },

        contactDetailTpl: function(data){
            var t = new SimpleTemplate(),
                defaultUrl = 'http://kdweibo.com/space/c/photo/load?id=',
                jobTitle = data.participant[5][0] || '职员',
                userId = data.participant[0][0],
                userName = data.groupName,
                groupId = data.groupId,
                photoUrl = data.participant[2][0] || defaultUrl,
                isSelectedCls = this.selectedMembers[userId] ? ' checked' : '';

            t._('<li class="contact-detail' + isSelectedCls + '" data-userid="' + userId + '" data-groupid="' + groupId + '">')
                ._('<div class="contact-detail-inner clearfix">')
                ._('<span class="contact-checkbox"></span>')
                ._('<img src="' + photoUrl + '">')
                ._('<div class="contact-content">')
                ._('<h3>' + userName + '</h3>')
                ._('<p>' + jobTitle + '</p>')
                ._('</div>')
                ._('</div>')
                ._('</li>');

            return t.toString();
        },

        groupDetailTpl: function(data){
            var t = new SimpleTemplate(),
                photoUrl,
                photoCount,
                defaultUrl = 'http://kdweibo.com/xtweb/pub/img/default_man.png';

            var searchVal = $('.person-select-exist-sessions .search-input').val().trim();
            var style = '';

            if (searchVal && data.groupName.indexOf(searchVal) == -1) {
                style = 'style="display: none;"';
            }

            t._('<li class="group-detail" ' + style + ' data-groupid="' + data.groupId + '" data-groupname="' + data.groupName + '">')
                ._('<div class="contact-detail-inner clearfix">')
                ._('<div class="group-avatar clearfix">');

            photoCount = data.participant[2].length > 4 ? 4 : data.participant[2].length;
            for(var j = 0; j < photoCount; j++){
                photoUrl = data.participant[2][j] || defaultUrl;
                t._('<img src="' + photoUrl + '">');
            }

            t._('</div>')
                ._('<div class="group-content ellipsis">' + data.groupName + '(' + (parseInt(data.participant[2].length) + 1) + '人)</div></div>')
                ._('</li>');

            return t.toString();
        },

        renderOrgTree: function(){
            var orgPath = this.orgPath,
                self = this;
            if(orgPath && orgPath.length > 0){
                if(orgPath.length > 1){
                    orgPath.splice(1);
                }

                var tpl = this.orgTreeTpl(orgPath[0], 'org-index');
                $('.lapp-person-select-wrap .person-select-organization .contact-detail-wrap').empty().append(tpl);

                // 组织架构为空
                if($('.lapp-person-select-wrap .person-select-organization .contact-detail-ul li').length === 0){
                    $('.lapp-person-select-wrap .person-select-organization .lapp-result-none').show();
                }
            }
            else{
                var eid = this.cfg.eId || '',
                    reqData = {
                        protocol: 'post',
                        url: '/im/web/treeOrg.do',
                        param: {
                            eid: eid,
                            orgId: '',
                            begin: 0,
                            count: 10000
                        }
                    };

                this.commonRequest(reqData).done(function (resp){
                    $('.lapp-person-select-wrap .person-select-organization .lapp-search-loading').hide();
                    var key = 'root';
                    resp.data.id = key;
                    self.orgCache[key] = resp.data;
                    orgPath.push(resp.data);

                    var tpl = self.orgTreeTpl(orgPath[0], 'org-index');
                    $('.lapp-person-select-wrap .person-select-organization .contact-detail-wrap').empty().append(tpl);

                    // 组织架构为空
                    if($('.lapp-person-select-wrap .person-select-organization .contact-detail-ul li').length === 0){
                        $('.lapp-person-select-wrap .person-select-organization .lapp-result-none').show();
                    }
                });
            }
        },

        orgTreeTpl: function(data, type){
            var t = new SimpleTemplate();

            if(type === 'org-index'){
                t._('<ul class="contact-detail-ul contact-detail-ul-org-index" data-id="' + data.id + '">');
            }
            else{
                t._('<ul class="contact-detail-ul" data-id="' + data.id + '">');
            }

            if(data.parent){
                t._('<li class="parent-org">')
                    ._('<div class="contact-detail-inner">')
                    ._('<span class="prev-arrow"></span>')
                    ._('<a href="javascript:void(0)" class="parent-name ellipsis" title="' + data.parent + '">' + data.parent + '</a>')
                    ._('</div>')
                    ._('</li>');
            }

            for(var i = 0, len = data.children.length; i < len; i++){
                var department = data.children[i];
                t._('<li class="department" data-id="' + department.id + '">')
                    ._('<div class="contact-detail-inner">')
                    ._('<a class="dep-name ellipsis" href="javascript:void(0)" title="' + department.name + '">' + department.name + '</a>')
                    ._('<span class="next-arrow"></span>')
                    ._('</div>')
                    ._('</li>');
            }

            for(var j = 0, len = data.person.length; j < len; j++){
                var user = data.person[j],
                    defaultUrl = 'http://kdweibo.com/space/c/photo/load?id=',
                    photoUrl = user.photoUrl || defaultUrl,
                    userId = user.oId,//原来为id，统一改为oId
                    isSelectedCls = this.selectedMembers[userId] ? ' checked' : '';
                user.jobTitle = user.jobTitle || '职员';
                t._('<li class="contact-detail' + isSelectedCls + '" data-userid="' + userId + '">')
                    ._('<div class="contact-detail-inner clearfix">')
                    ._('<span class="contact-checkbox"></span>')
                    ._('<img src="' + photoUrl + '">')
                    ._('<div class="contact-content">')
                    ._('<h3>' + user.name + '</h3>')
                    ._('<p>' + user.jobTitle + '</p>')
                    ._('</div>')
                    ._('</div>')
                    ._('</li>');
            }

            t._('</ul>');

            return t.toString();
        },

        destroy: function(){
            this.clearXhr();
            this.unbindHashChangEvt();
            this.resetParentEleOverflow();

            if(this._$root){
                this._$root.remove();
                this._$root = null;
            }
        },

        clearXhr: function () {
            for (var i = 0, len = this.xhrList.length; i < len; i++) {
                this.xhrList[i].abort();
            }
        },

        commonRequest: function(data){
            var deferred = $.Deferred();
            $.ajax({
                type: data.protocol || 'get',
                cache: false,
                dataType: 'json',
                url: data.url,
                data: data.param
            }).done(function (resp) {
                if (resp.success === true) {
                    deferred.resolve(resp);
                }
                else {
                    deferred.reject(resp);
                }
            }).fail(function (resp) {
                deferred.reject(resp);
            });
            return deferred.promise();
        },

        addUrlParam: function(url, str){
            if(url.indexOf('?') <= 0){
                url = url + '?';
            }
            else{
                url = url + '&';
            }
            return url + str;
        },

        handleCallback: function(groupId, userId){
            var self = this,
                callbackUrl = self.cfg.callbackUrl;

            if(groupId == undefined){
                groupId = '';
            }

            // 执行回调
            if(callbackUrl != ''){
                var callbackParam = self.cfg.callbackParam;
                $.extend(callbackParam, {
                    'eId': self.cfg.eId,
                    'openId': self.cfg.openId,
                    'groupId': groupId || userId
                });

                $.ajax({
                    type: 'post',
                    url: callbackUrl,
                    data: callbackParam,
                    dataType: 'json',
                    success: function(resp){
                        if(resp.success === true){
                            var callbackData = resp.data,
                                param = {
                                    'appName': self.cfg.appName,
                                    'lightAppId': self.cfg.lightAppId,
                                    'pubAccId': self.cfg.appId,
                                    'thumbUrl': self.cfg.thumbData,
                                    'title': self.cfg.title,
                                    'content': self.cfg.content,
                                    'webpageUrl': callbackData ? self.addUrlParam(self.cfg.webpageUrl, callbackData) : self.cfg.webpageUrl
                                };

                            $.ajax({
                                type: 'post',
                                url: '/im/web/sendMessage.do',
                                data: {
                                    groupId: groupId,
                                    toUserId: userId,
                                    msgType: 7,
                                    content: self.cfg.title,
                                    param: JSON.stringify(param)
                                },
                                dataType: 'json',
                                success: function(resp){
                                    if(resp.success === true){
                                        self.showTips('发送成功', 1000, function(){
                                            $('.lapp-person-select-wrap .person-select-overlay').fadeOut();
                                            var callback = self.cfg.successCallback;
                                            if(callback && typeof callback === 'function'){
                                                callback.call(null, callbackData);
                                            }
                                            else{
                                                self.destroy();
                                                self.resetHistory();
                                            }
                                        });
                                    }
                                    else{
                                        self.showTips('发送失败', 1000, function(){
                                            $('.lapp-person-select-wrap .person-select-overlay').fadeOut();
                                        });
                                    }
                                }
                            });
                        }
                        else{
                            self.showTips('发送失败', 1000, function(){
                                $('.lapp-person-select-wrap .person-select-overlay').fadeOut();
                            });
                        }
                    }
                });
            }
            // 不执行回调，直接发送消息
            else{
                var param = {
                    'appName': self.cfg.appName,
                    'lightAppId': self.cfg.lightAppId,
                    'pubAccId': self.cfg.appId,
                    'thumbUrl': self.cfg.thumbData,
                    'title': self.cfg.title,
                    'content': self.cfg.content,
                    'webpageUrl': self.cfg.webpageUrl
                };

                $.ajax({
                    type: 'post',
                    url: '/im/web/sendMessage.do',
                    data: {
                        groupId: groupId,
                        toUserId: userId,
                        msgType: 7,
                        content: self.cfg.title,
                        param: JSON.stringify(param)
                    },
                    dataType: 'json',
                    success: function(resp){
                        if(resp.success === true){
                            self.showTips('发送成功', 1000, function(){
                                $('.lapp-person-select-wrap .person-select-overlay').fadeOut();

                                var callback = self.cfg.successCallback;
                                if(callback && typeof callback === 'function'){
                                    callback.call(null);
                                }

                                self.destroy();
                                self.resetHistory();
                            });
                        }
                        else{
                            self.showTips('发送失败', 1000, function(){
                                $('.lapp-person-select-wrap .person-select-overlay').fadeOut();
                            });
                        }
                    }
                });
            }
        },

        // 设置父元素overflow为hidden，使得选人组件铺满父窗口
        setParentEleOverflow: function(){
            this.parentEleOverflow = this._$parentElement.css('overflow');
            this._$parentElement.css('overflow', 'hidden');
        },

        resetParentEleOverflow: function(){
            this._$parentElement.css('overflow', this.parentEleOverflow);
        },

        resetHistory: function(){
            if(this.historyLen > 0){
                window.history.go(-this.historyLen );
            }
        },

        showTips: function(tips, time, callback){
            var $promptMsg = $(".lapp-person-select-wrap .person-select-prompt-msg"),
                fadeOutTime = time || 500;

            $promptMsg.html(tips).fadeIn('slow', function(){
                $promptMsg.fadeOut(fadeOutTime, function(){
                    if(callback){
                        setTimeout(callback, 300);
                    }
                });
            });
        }

    };

    function SimpleTemplate () {
        this.parts = [];
        this._pushAll(arguments);
    }

    SimpleTemplate.prototype = {
        _: function () {
            this._pushAll(arguments);
            return this;
        },

        toString: function () {
            return this.parts.join('');
        },

        _pushAll: function (arr) {
            var i, n = arr.length;
            for (i = 0; i < n; i++) {
                this.parts.push(arr[i]);
            }
        }
    };

    function htmlEntities(str) {
        if (typeof str === 'undefined') return '';
        return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }

    function escapeContent(content){
        return content.replace(/\n|\r/g, '<br>').replace(/ /g, '&nbsp;');
    }

   export  default LappPersonSelect
