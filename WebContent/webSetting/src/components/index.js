import MenuContainer from './Menu/MenuContainer'
import ApprovalRuleSet from './Approval/ApprovalRuleSet'
import FormDesignSet from './Design/FormDesignSet'

const Components = {
    MenuContainer,
    ApprovalRuleSet,
    FormDesignSet
}
module.exports = Components
