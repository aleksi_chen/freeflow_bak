import Root from './Root'
import Design from './Design'
import Setting from './Setting'
const Pages = {
    Root,
    Design,
    Setting
}

module.exports = Pages
