var path = require('path')

module.exports = {
    entry: {
        app: './src/main.js'
        // vendor: ['vue', './src/lib/qingjs.js', './src/lib/swiper.js', './src/lib/validator.js']
    },
    output: {
        path: path.resolve(__dirname, '../dist/static'),
        publicPath: '/freeflow/webSetting/dist/static/',
//    publicPath: '/static',
        filename: '[name].js'
    },
    resolve: {
        // root: path.join(__dirname, 'node_modules'),
        extensions: ['', '.js', '.vue'],
        alias: {
            'src': path.resolve(__dirname, '../src')
        }
    },
    resolveLoader: {
        root: path.join(__dirname, 'node_modules')
    },
    module: {
        preLoaders: [
            // {
            //   test: /\.vue$/,
            //   loader: 'eslint',
            //   exclude: /node_modules/
            // },
            // {
            //   test: /\.js$/,
            //   loader: 'eslint',
            //   exclude: /node_modules/
            // }
        ],
        loaders: [
            {
                test: /\.css$/,
                loader: 'style!css'
            },
            {
                test: /\.vue$/,
                loader: 'vue'
            },
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/
            },
            {
                test: /\.json$/,
                loader: 'json'
            },
            // {
            //   test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            //   loader: "url-loader?limit=10000&mimetype=application/font-woff"
            // },
//      {
//        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
//        loader: "file-loader"
//      },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader",
                query: {
                    limit: 10000,
                    name: '[name].[ext]?[hash:7]'
                }
            },
            {
                test: /\.(png|jpg|gif)$/,
                loader: 'url',
                query: {
                    limit: 4096,
                    name: '[name].[ext]?[hash:7]'
                }
            },
            {
                test: /\.(svg|ttf|eot)$/,
                loader: "file-loader",
                query: {
                    limit: 10000,
                    name: '[name].[ext]?[hash:7]'
                }
            },

        ]
    },
    // eslint: {
    //   formatter: require('eslint-friendly-formatter')
    // }
}
