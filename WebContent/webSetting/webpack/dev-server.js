var express = require('express')
var webpack = require('webpack')
var config = require('./webpack.dev.conf')
var opn = require('opn');
var app = express()
var compiler = webpack(config)

var api = require('../src/mock')
app.get('/freeflow/v1/rest/form-template/detail/formTemplate/:id', api.detail)
app.post('/freeflow/v1/rest/form-template/create/formTemplate', api.create)
app.get('/freeflow/v1/rest/approvalRule/detail/:id', api.approvalRuleDetail)
app.post('/freeflow/v1/rest/approvalRule/save', api.approvalRuleSave)
app.get('/freeflow/web/userInfo.json', api.userInfo)

var getToken = require('./token')
//mock api

var devMiddleware = require('webpack-dev-middleware')(compiler, {
    publicPath: config.output.publicPath,
    stats: {
        colors: true,
        chunks: false
    }
})

var hotMiddleware = require('webpack-hot-middleware')(compiler)
// force page reload when html-webpack-plugin template changes
compiler.plugin('compilation', function (compilation) {
    compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
        hotMiddleware.publish({action: 'reload'})
        cb()
    })
})

// handle fallback for HTML5 history API
app.use(require('connect-history-api-fallback')())
// serve webpack bundle output
app.use(devMiddleware)
// enable hot-reload and state-preserving
// compilation error display
app.use(hotMiddleware)
// serve pure static assets
app.use('/static', express.static('./static'))

//获取token

var port = 8081
new Promise(function (resolve, reject) {
    app.listen(port, function (err) {
        if (err) {
            console.log(err)
            return reject(err)
        }
        resolve()
    })
}).then(getToken)
    .then(token => {
        console.log(`Listening at http://localhost:${port}/freeflow/web/manager/?lappName=freeflow&webLappToken=${token}`)
        opn(`http://localhost:${port}/freeflow/web/manager/?lappName=freeflow&webLappToken=${token}`);
    })
    .catch(err => {
        console.log(`Listening at http://localhost:${port}/freeflow/web/manager/`)
        opn(`http://localhost:${port}/freeflow/web/manager/`);
    })
