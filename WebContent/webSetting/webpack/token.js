const request = require('superagent')

const headers = {
    "postman-token": "5222e536-bcb9-f8fb-d257-4a7a9dcd5a3f",
    "cache-control": "no-cache",
    "accept-language": "zh-CN,zh;q=0.8,en;q=0.6,pt;q=0.4,zh-TW;q=0.2,it;q=0.2",
    "accept-encoding": "gzip, deflate, sdch",
    "referer": "http://yunzhijia.com/home/?m=open&a=login",
    "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
    "upgrade-insecure-requests": "1",
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
}

function login() {
    return new Promise(function (resolve, reject) {
        request
            .post('http://192.168.22.144/space/c/rest/user/login')
            .set(headers)
            .type('form')
            .send({
                email: 15820436267,
                forceToNetwork: false,
                password: 123456,
                redirectUrl: 'http://192.168.22.144/im/xiaoxi',
                remember: false
            })
            .redirects(0)
            .end(function (err, res) {
                if (err) return reject(err)
                const cookie = res.headers['set-cookie'][0]
                resolve(cookie)
            })
    })
}

function getToken(cookie) {
    return login().then(cookie => {
        return new Promise(function (resolve, reject) {
            request.get('http://192.168.22.144/im/xiaoxi')
                .set('Cookie', cookie)
                .end(function (err, res) {
                    console.log(cookie)
                    if (err) return reject(err)
                    const webLappToken = (/webLappToken:\s*\'[A-Za-z0-9%]+\'/g).exec(res.text)
                    const token = webLappToken ? webLappToken[0].replace(/(webLappToken:\s*|\')/g, '') : 'RTXcirUwxXfvnJssnlAY8dG1d%2BJBh3CxkGnXzgZjJd%2BbDqK7MPWtjRE559o86X3k73K5c5TbfKiDcF16ln4PLXuFbeuyLUOfg3LfusOr0OM%3D'
                    resolve(token)
                })
        })
    })

}

module.exports = getToken
