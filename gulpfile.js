var gulp = require('gulp'),
    clean = require('gulp-clean')
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat');
//默认任务
gulp.task('default', ['clean'], function () {
	//基础任务
	gulp.start('style', 'script');
});

gulp.task('style', function () {
	var concatStyleLists = [
	    'WebContent/web/css/normalize.css',
	    'WebContent/web/css/common.css',
	    'WebContent/web/css/form-design.css',
	    'WebContent/web/css/approval-manage.css',
	   	'WebContent/web/css/person-select.css'
	];

	var cssDest = 'WebContent/dist/css';

	return gulp.src(concatStyleLists)
		//补全浏览器前缀
		.pipe(autoprefixer({ browsers: ['last 2 version', 'safari 5', 'ios 6', 'android 4', 'Firefox <= 20'], cascade: false }))
		//合并
		.pipe(concat('web-all.css'))
		//输出到目录
		.pipe(gulp.dest(cssDest))
		//重命名新的文件
		.pipe(rename({suffix: '-min'}))
		//压缩
		.pipe(minifycss())
		//输出到目录
		.pipe(gulp.dest(cssDest));
});


//js合并压缩
gulp.task('basescript', function () {
	//base.js合集
	var commonScriptsList = [
		'WebContent/js/core/underscore-min.js',
		'WebContent/js/core/mTouch.js',
		'WebContent/web/js/web-util.js'
	];

	var jsDest = 'WebContent/dist/js';

	return gulp.src(commonScriptsList)
		//校验js是否报错
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(concat('base.js'))
		.pipe(gulp.dest(jsDest))
		.pipe(rename({suffix: '-min'}))
		.pipe(uglify())
		.pipe(gulp.dest(jsDest));
});


//js合并压缩
gulp.task('script', ['basescript'],  function () {
	//除了common.js外的js合集
	var allScriptsList = [
		'WebContent/web/js/web-util.js',
		'WebContent/web/js/widget.js',
		'WebContent/web/js/paginator.js',
		'WebContent/web/js/autocomplete.js',
		'WebContent/web/js/org-tree.js',
		'WebContent/web/js/select-approver.js',
		'WebContent/web/js/designer.js',
		'WebContent/web/js/form-area.js',
		'WebContent/web/js/form-controller.js',
		'WebContent/web/js/approval-manage.js',
		'WebContent/web/js/person-select.js'
	];

	var jsDest = 'WebContent/dist/js';

	return gulp.src(allScriptsList)
		//校验js是否报错
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(concat('web-all.js'))
		.pipe(gulp.dest(jsDest))
		.pipe(rename({suffix: '-min'}))
		.pipe(uglify())
		.pipe(gulp.dest(jsDest));
});


//删除源文件
gulp.task('clean', function () {
	var cleanLists = [
	    'WebContent/dist/css/*.*',
	    'WebContent/dist/js/*.*'
	];

	return gulp.src(cleanLists)
			   .pipe(clean());
});

//监听任务
gulp.task('watch', function () {
	gulp.watch(['WebContent/web/css/**/*.css'], ['style']);
	gulp.watch(['WebContent/web/js/**/*.js'], ['script']);
});
